![](https://git.flavorwiki.info/app/frontend/badges/develop/pipeline.svg)
![](https://git.flavorwiki.info/app/frontend/badges/develop/coverage.svg)

**Coding guidelines**

1. Code linting and formatting

If Visual Studio Code is used as a code editor, then the following 2 plugins should be installed:
- https://marketplace.visualstudio.com/items?itemName=numso.prettier-standard-vscode 
- https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint

Other prettifiers should be disabled.

2. Package Manager

The npm package manger should be used

3. CSS

For the css part the https://www.styled-components.com/ should be used instead of classical CSS / SCSS

4. React

As a general rule, the function components with hooks are used

5. Architecture

As a general rule, the app architecture is composed by pages, containers, and components. 

- Pages - are supposed to be very readable, so we try to make them just a set of high-level containers or components that showcase the layout of the page.
- Containers - encapsulate all the logic regarding the state of the app, and data fetching. They then pass down the data and callbacks down to the component.
- Components -  should only be concerned with presentational logic.

This architecture allows for the same component (similar or equal style) to have a different role in the app, just by wrapping it in a different container. We also use many “inner components”, just to make the base component more readable and simple.

**Text guidelines**

1. Use "." or punctuation at the end of a sentence but not a phrase

2. Capitalize first word of sentence and phrase. Phrase is not a full sentence:  Example "Minimum number of products to taste"

3. Capitalize all words in title except "of", "the". Title is something like "Continue Button" or "Start Survey Button"

4. Sentence means a full sentence with a subject, verb.

