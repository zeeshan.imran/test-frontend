import React from 'react'
import Button from '../Button'
import { Container, InnerContainer } from './styles'
import { isUserAuthenticatedAsAnalytics } from '../../utils/userAuthentication'

const CreationButtonStatsContainer = ({
  hasProductSelection,
  productButtonClick,
  productButtonText,
  filterButtonClick,
  filterButtonText,
  ...rest
}) => {
  const isAnalytics = isUserAuthenticatedAsAnalytics()
  
  return (
    <Container>
      {hasProductSelection && (
        <InnerContainer>
          <Button {...rest} onClick={productButtonClick}>
            {productButtonText}
          </Button>
        </InnerContainer>
      )}

      {!isAnalytics && (
        <InnerContainer>
          <Button {...rest} onClick={filterButtonClick}>
            {filterButtonText}
          </Button>
        </InnerContainer>
      )}
    </Container>
  )
}

export default CreationButtonStatsContainer
