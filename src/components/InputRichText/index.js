// TinyMCE
import 'tinymce/tinymce'
import 'tinymce/themes/silver'
import 'tinymce/plugins/autoresize'
import 'tinymce/plugins/imagetools'
import 'tinymce/plugins/table'
import 'tinymce/plugins/autolink'
import 'tinymce/plugins/link'
import 'tinymce/plugins/anchor'
import 'tinymce/plugins/media'
import 'tinymce/plugins/preview'
import 'tinymce/plugins/print'
import 'tinymce/plugins/pagebreak'
import 'tinymce/plugins/charmap'
import 'tinymce/plugins/lists'
import 'tinymce/plugins/fullscreen'
import 'tinymce/plugins/directionality'
//
import React, { useCallback } from 'react'
import PropTypes from 'prop-types'
import { Editor } from '@tinymce/tinymce-react'
import FieldLabel from '../FieldLabel'
import { useTranslation } from 'react-i18next'
require('../../utils/tinymce/image')

const MAX_FILE_SIZE = 2 * 1024 * 1024
const { REACT_APP_THEME } = process.env
const isHansen = REACT_APP_THEME === 'chrHansen'

const InputRichText = ({
  label,
  required,
  value,
  ownerCss,
  noUpload = false,
  outerAlign = 'center',
  tooltip,
  tooltipPlacement,
  onChange
}) => {
  const { t } = useTranslation()
  const handleFilePicker = useCallback(
    (cb, value, meta) => {
      var input = document.createElement('input')
      input.setAttribute('type', 'file')
      input.setAttribute('accept', 'image/*')
      input.onchange = function () {
        var file = this.files[0]
        if (file.size > MAX_FILE_SIZE) {
          global.tinyMCE.activeEditor.windowManager.alert(
            t('errors.pictureTooLarge')
          )
          return
        }
        var reader = new FileReader()
        reader.onload = function () {
          var id = 'blobid' + new Date().getTime()
          var blobCache = global.tinymce.activeEditor.editorUpload.blobCache
          var base64 = reader.result.split(',')[1]
          var blobInfo = blobCache.create(id, file, base64)
          blobCache.add(blobInfo)
          cb(blobInfo.blobUri(), { title: file.name })
        }
        reader.readAsDataURL(file)
      }
      input.click()
    },
    [t]
  )

  return (
    <FieldLabel
      required={required}
      label={label}
      tooltip={tooltip}
      tooltipPlacement={tooltipPlacement}
    >
      <Editor
        value={value}
        onEditorChange={onChange}
        apiKey='e0f8pyqc2l99ghypfxgarpymbr8q25dnl0lj78km00m3c9st'
        plugins={[
          'autoresize',
          'image',
          'imagetools',
          'table',
          'link',
          'autolink',
          'pagebreak',
          'charmap',
          'lists',
          'fullscreen',
          'preview',
          'media',
          'print',
          'anchor',
          'directionality'
        ]}
        toolbar={[
          'undo redo | bold italic underline strikethrough | fontselect fontsizeselect | ltr rtl | preview fullscreen | print',
          'alignleft aligncenter alignright alignjustify | outdent indent  | forecolor backcolor removeformat| image media link table | numlist bullist | pagebreak | charmap emoticons archor'
        ]}
        init={{
          min_height: '300px',
          file_picker_types: 'image',
          file_picker_callback: noUpload || handleFilePicker,
          autoresize_bottom_margin: 30,
          content_css: [
            'https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,700i|Roboto:400i,500,500i,700,700i&display=swap',
            'https://fonts.googleapis.com/css?family=Noto+Sans+KR&display=swap&subset=korean',
            'https://fonts.googleapis.com/css?family=Nunito:400,700,900&display=swap'
          ],
          content_style: `p { font-family: ${
            isHansen ? 'Calibri' : 'Noto Sans KR'
          }; font-size: 13.5pt; font-weight: 300; margin-top: 0; margin-bottom: 1rem; text-align: ${outerAlign}; } ${ownerCss}`,
          fontsize_formats: '8pt 10pt 12pt 13.5pt 16pt 20pt 24pt 32pt',
          font_formats: `Noto=Noto Sans KR;Nunito=Nunito;Lato=Lato;Calibri=Calibri;Andale Mono=andale mono,times; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats`,
          toolbar_drawer: false,
          menubar: false,
          quickbars_selection_toolbar:
            'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
          remove_script_host: false,
          relative_urls: false,
          convert_urls: false
        }}
      />
    </FieldLabel>
  )
}

InputRichText.defaultProps = {
  label: ''
}

InputRichText.propTypes = {
  label: PropTypes.string
}

export default InputRichText
