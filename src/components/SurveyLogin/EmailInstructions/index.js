import React from 'react'
import { SimpleText, MainText } from './styles'
import { withTranslation } from 'react-i18next'

const EmailInstructions = ({ t, requireRegistration, loginText }) => (
  <React.Fragment>
    {!requireRegistration ? (
      <SimpleText dangerouslySetInnerHTML={{ __html: loginText }} />
    ) : (
      <MainText>{t('components.surveyLogIn.askForAccount')}</MainText>
    )}
  </React.Fragment>
)

export default withTranslation()(EmailInstructions)
