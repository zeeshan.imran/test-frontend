import React from 'react'
import { mount } from 'enzyme'
import Title from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('Title', () => {
  let testRender
  let title
  let fullWidth

  beforeEach(() => {
    title = 'Survey'
    fullWidth = true
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Title', async () => {
    testRender = mount(<Title title={title} fullWidth={fullWidth} />)
    expect(testRender.find(Title)).toHaveLength(1)
  })
})
