import styled from 'styled-components'

export const Container = styled.div`
  text-align: ${({ fullWidth }) => (fullWidth ? 'center' : 'left')};
`
