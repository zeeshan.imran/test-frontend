import React from 'react'
import styled from 'styled-components'
import Text from '../../Text'

export const LeftText = styled(Text)`

  p {
    text-align: left !important;
  }
`

const OpenInstructions = ({ text = '' }) => {

  return (
    <LeftText>
      <span dangerouslySetInnerHTML={{ __html: text }} />
    </LeftText>
  ) 
}

export default OpenInstructions
