import React from 'react'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import Text from '../../Text'
import LinkText from '../../LinkText'
import { SocialButtonsContainer, DasboardButton, LeftText } from './styles'
import getQueryParams from '../../../utils/getQueryParams'

let LoggedOut = ({ history }) => {
  const { t } = useTranslation()
  return (
    <React.Fragment>
      <Text>{t('components.surveyLogIn.surveyAvailable')}</Text>
      <Text>{t('components.surveyLogIn.accessLogin')}</Text>
      <LinkText onClick={() => history.push('/onboarding')}>
        {t('components.surveyLogIn.access')}
      </LinkText>
    </React.Fragment>
  )
}
LoggedOut = withRouter(LoggedOut)

let OnlyLoggedIn = ({ history, location }) => {
  const { t } = useTranslation()
  const { referral } = getQueryParams(location)
  let params = ''
  if (referral) {
    params = '?referral=' + referral
  }
  return (
    <React.Fragment>
      <Text>{t('components.surveyLogIn.onlyWithAccount')}</Text>
      <LinkText onClick={() => history.push('/onboarding/login' + params)}>
        {t('components.surveyLogIn.access')}
      </LinkText>
      <Text>{t('or')}</Text>
      <LinkText
        onClick={() => history.push('/create-account/taster/welcome' + params)}
      >
        {t('requestTasterAccount')}
      </LinkText>
    </React.Fragment>
  )
}
OnlyLoggedIn = withRouter(OnlyLoggedIn)

const Valid = ({ text }) => {
  return (
    <LeftText>
      <span dangerouslySetInnerHTML={{ __html: text }} />
    </LeftText>
  )
}

const Invalid = () => {
  const { t } = useTranslation()
  return (
    <React.Fragment>
      <Text>{t('components.surveyLogIn.surveyAvailable')}</Text>
      <Text>{t('components.surveyLogIn.notSelectedToParticipate')}</Text>
    </React.Fragment>
  )
}

const Retake = () => {
  const { t } = useTranslation()
  const { REACT_APP_THEME } = process.env
  return (
    <React.Fragment>
      <Text>{t('survey.already_taken')}</Text>
      {/* <Text>{t('components.retakes.header')}</Text>
      <Text>{t('components.retakes.subHeader')}</Text> */}
      {REACT_APP_THEME === 'default' && (
        <SocialButtonsContainer>
          <DasboardButton href={`/`}>
            {t('components.surveyComplete.backToDashboard')}
          </DasboardButton>
        </SocialButtonsContainer>
      )}
    </React.Fragment>
  )
}

const Expired = () => {
  const { t } = useTranslation()
  const { REACT_APP_THEME } = process.env
  return (
    <React.Fragment>
      <Text>{t('components.surveyLogIn.surveyExpired')}</Text>
      {REACT_APP_THEME === 'default' && (
        <SocialButtonsContainer>
          <DasboardButton href={`/`}>
            {t('components.surveyComplete.backToDashboard')}
          </DasboardButton>
        </SocialButtonsContainer>
      )}
    </React.Fragment>
  )
}

const Suspended = pauseText => {
  const { t } = useTranslation()
  const { REACT_APP_THEME } = process.env
  return (
    <React.Fragment>
      <Text>
        <span
          dangerouslySetInnerHTML={{ __html: pauseText && pauseText.pauseText }}
        />
      </Text>
      {REACT_APP_THEME === 'default' && (
        <SocialButtonsContainer>
          <DasboardButton href={`/`}>
            {t('components.surveyComplete.backToDashboard')}
          </DasboardButton>
        </SocialButtonsContainer>
      )}
    </React.Fragment>
  )
}

let NotAvailableIn = () => {
  const { t } = useTranslation()
  const { REACT_APP_THEME } = process.env
  return (
    <React.Fragment>
      <Text>{t('components.surveyLogIn.notAvailableInCountry')}</Text>
      {REACT_APP_THEME === 'default' && (
        <SocialButtonsContainer>
          <DasboardButton href={`/`}>
            {t('components.surveyComplete.backToDashboard')}
          </DasboardButton>
        </SocialButtonsContainer>
      )}
    </React.Fragment>
  )
}

export {
  LoggedOut,
  Valid,
  Invalid,
  Retake,
  Expired,
  Suspended,
  OnlyLoggedIn,
  NotAvailableIn
}
