import React from 'react'
import { mount } from 'enzyme'
import { act } from 'react-dom/test-utils'
import { LoggedOut } from './index'
import LinkText from '../../LinkText'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('component SurveyLogin > SelectedInstructions > LoggedOut', () => {
  let testRender

  afterEach(() => {
    testRender.unmount()
  })
 
  test('should render without crash', () => {
    testRender = mount(<LoggedOut.WrappedComponent />)
  })

  test('should redirect to /onboarding after clicking on the login link', () => {
    const history = { push: jest.fn() }
    testRender = mount(<LoggedOut.WrappedComponent history={history} />)

    expect(testRender.find(LinkText)).toHaveLength(1)
    expect(
      testRender
        .find(LinkText)
        .first()
        .text()
    ).toBe('components.surveyLogIn.access')

    act(() => {
      testRender.find(LinkText).simulate('click')
    })

    expect(history.push).toHaveBeenCalledWith('/onboarding')
  })
})
