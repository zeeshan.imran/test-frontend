import styled from 'styled-components'
import colors from '../../../utils/Colors'
import Text from '../../Text'

export const SocialButtonsContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`
const SocialButton = styled.a`
  padding: 1.5rem;
  margin: 4rem 0 1rem 0;
  max-width: 30rem;
  width: 80%;
  border-radius: 0.4rem;
  color: white !important;
  line-height: 1;
  text-decoration: none;
  cursor: pointer;
  text-align: center;
`

export const DasboardButton = styled(SocialButton)`
  background-color: ${colors.DASHBOARD_BUTTON_COLOR} !important;
`

export const LeftText = styled(Text)`

  p {
    text-align: left !important;
  }
`
