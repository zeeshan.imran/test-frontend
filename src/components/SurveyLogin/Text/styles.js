import styled from 'styled-components'
import BaseText from '../../Text'
import { family } from '../../../utils/Fonts'
import colors from '../../../utils/Colors'

export const Text = styled(BaseText)`
  font-family: ${family.primaryLight};
  font-size: 1.4rem;
  color: ${colors.SLATE_GREY};
  text-align: ${({ centered }) => (centered ? 'center' : 'left')};
`

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: ${({ centered }) => (centered ? 'center' : 'flex-start')};
`
