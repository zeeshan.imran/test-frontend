import React from 'react'
import { storiesOf } from '@storybook/react'
import SurveyCompleted from './'

storiesOf('SurveyCompleted', module).add('Story', () => (
  <SurveyCompleted text='Example of a Thank You Text' />
))
