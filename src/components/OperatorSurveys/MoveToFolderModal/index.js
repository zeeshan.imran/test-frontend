import React, { useState, useEffect } from 'react'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import { Modal, Tree, Row } from 'antd'
import {
  dataFormatForTree,
  updateTreeData,
  filterChildren
} from './dataMappers'
import { useTranslation } from 'react-i18next'
import Loader from '../../../components/Loader'
import { LoaderWrapper } from './styles'

const MoveToFolderModal = ({
  visible,
  toggleModal,
  actionType,
  parentFolderId,
  moveSurveyOrFolderToFolder,
  selectedId
}) => {
  const { DirectoryTree } = Tree
  const { t } = useTranslation()
  const [treeData, setTreeData] = useState([])
  const [fetchFolderId, setFetchFolderId] = useState(null)
  const [selectedFolderId, setSelectedFolderId] = useState(null)

  const { data: { listFolders: folders } = {}, fetchMore, loading } = useQuery(
    LIST_FOLDERS,
    {
      fetchPolicy: 'network-only',
      variables: { parentId: null }
    }
  )

  useEffect(() => {
    if (!loading && !fetchFolderId && folders.length) {
      if (actionType === 'folder') {
        setTreeData(dataFormatForTree(folders, parentFolderId))
      } else {
        setTreeData(dataFormatForTree(folders))
      }
    }
  }, [loading, folders, fetchFolderId, parentFolderId, actionType])

  const onLoadData = ({ props }) => {
    const { eventKey } = props
    return new Promise(resolve => {
      setFetchFolderId(eventKey)
      fetchMore({
        variables: {
          parentId: eventKey
        },
        updateQuery: (prev, { fetchMoreResult }) => {
          if (!fetchMoreResult) return prev
          if (
            fetchMoreResult.listFolders &&
            fetchMoreResult.listFolders.length
          ) {
            setTreeData(origin =>
              updateTreeData(
                filterChildren(origin, selectedId),
                eventKey,
                dataFormatForTree(
                  filterChildren(fetchMoreResult.listFolders, selectedId)
                ),
                selectedId
              )
            )
            resolve()
          } else {
            resolve()
          }
        }
      })
    })
  }
  const selectedFolder = id => id.length && setSelectedFolderId(id[0])
  return (
    <Modal
      title={
        actionType === 'survey'
          ? t('components.operatorSurveys.moveSurvey')
          : t('components.operatorSurveys.moveFolder')
      }
      visible={visible}
      onOk={() => {
        toggleModal(false)
        if (actionType === 'folder') {
          moveSurveyOrFolderToFolder(selectedFolderId, parentFolderId, null)
        } else {
          moveSurveyOrFolderToFolder(selectedFolderId, null, parentFolderId)
        }
      }}
      onCancel={() => toggleModal(false)}
      okButtonProps={{ disabled: !selectedFolderId }}
    >
      {loading ? (
        <LoaderWrapper>
          <Loader />
        </LoaderWrapper>
      ) : treeData.length ? (
        <DirectoryTree
          loadData={onLoadData}
          treeData={{
            title: t('components.operatorSurveys.home'),
            key: null,
            id: null,
            children: treeData
          }}
          onSelect={key => selectedFolder(key)}
        />
      ) : (
        <Row type='flex' justify='center'>
          <span>{t('components.operatorSurveys.moveNotAvailable')}</span>
        </Row>
      )}
    </Modal>
  )
}

const LIST_FOLDERS = gql`
  query listFolders($parentId: ID) {
    listFolders(parentId: $parentId) {
      id
      name
      parent
    }
  }
`

export default MoveToFolderModal
