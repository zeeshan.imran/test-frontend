export const dataFormatForTree = (
  folders = [],
  selectedParentFolder = null
) => {
  const foldersList = !folders.length
    ? []
    : folders.map(folder => {
        return selectedParentFolder
          ? folder.id === selectedParentFolder ||
            folder.key === selectedParentFolder
            ? null
            : {
                title: folder.name || folder.title,
                key: folder.id || folder.key
              }
          : {
              title: folder.name || folder.title,
              key: folder.id || folder.key
            }
      })
  return foldersList.filter(n => n)
}

export const filterChildren = (childrens, selectedId) =>
  childrens.filter(child =>
    child.key
      ? child.key !== selectedId
      : child.id
      ? child.id !== selectedId
      : true
  )

export const updateTreeData = (list, key, children, selectedId) =>
  list.map(node => {
    return node.key === key
      ? { ...node, children }
      : node.children
      ? {
          ...node,
          children: updateTreeData(
            filterChildren(node.children, selectedId),
            key,
            dataFormatForTree(filterChildren(children, selectedId)),
            selectedId
          )
        }
      : node
  })
