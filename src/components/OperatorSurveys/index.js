import React, { useState } from 'react'
import Button from '../Button'
import OperatorSurveysList from '../OperatorSurveysList'
import Select from '../Select'
import { Row, Col, Dropdown, Menu, Icon } from 'antd'
import AlertModal from '../AlertModal'
import { useTranslation } from 'react-i18next'
import IconButton from '../../components/IconButton'
import BreadcrumbFolder from '../../containers/BreadcrumbFolder'
import CopySurveyUrlButton from '../../containers/CopySurveyUrl'
import EditSurveyButton from '../../containers/EditSurveyButton'
import EditDraftButton from '../../containers/EditDraftButton'

import {
  isUserAuthenticatedAsSuperAdmin,
  isUserAuthenticatedAsOperator,
  isUserAuthenticatedAsPowerUser
} from '../../utils/userAuthentication'
import {
  Container,
  CreationDropdownContainer,
  QuickAccessCard,
  ActionsContainer,
  Item,
  QuickAccessTitle,
  QuickAccessDetails,
  Divider,
  SearchBarContainer,
  BreadcrumbsRow
} from './styles'
import TooltipWrapper from '../TooltipWrapper'
import MoveToFolderModal from './MoveToFolderModal'
import CreateFolderModal from './CreateFolderModal'
import Loader from '../../components/Loader'
import SearchBar from '../SearchBar'

const surveySearchStateOptions = ['All', 'Draft', 'Published', 'Suspended']

const getSurveyStateSuffix = state => {
  switch (state) {
    case 'draft':
      return ' (Draft)'
    case 'suspended':
      return ' (Suspended)'
    case 'deprecated':
      return ' (Deleted)'
    default:
      return ''
  }
}

const getDropdownMenu = (survey, t, openDropdownId, setOpenDropdownId) => {
  const closeDropdown = () => setOpenDropdownId(false)

  const menu = (
    <React.Fragment>
      <Menu onClick={closeDropdown}>
        <Item
          onItemHover={() => {}}
          onClick={closeDropdown}
          rootPrefixCls='ant-menu'
          key='0'
        >
          <CopySurveyUrlButton uniqueName={survey.uniqueName} label noActive />
        </Item>
        <Item
          onItemHover={() => {}}
          onClick={closeDropdown}
          rootPrefixCls='ant-menu'
          key='1'
        >
          {survey.state === 'draft' ? (
            <EditDraftButton
              surveyId={survey._id || survey.id}
              label
              noActive
            />
          ) : (
            <EditSurveyButton
              surveyId={survey._id || survey.id}
              surveyState={survey.state}
              label
              noActive
            />
          )}
        </Item>
      </Menu>
    </React.Fragment>
  )

  return (
    <ActionsContainer>
      <Dropdown
        placement='bottomRight'
        overlay={menu}
        trigger={['click']}
        visible={survey.id === openDropdownId}
        data-testid='quick-access-dropdown'
        onVisibleChange={change => setOpenDropdownId(change && survey.id)}
      >
        <IconButton tooltip={t('tooltips.moreActions')} type='more' />
      </Dropdown>
    </ActionsContainer>
  )
}

const OperatorSurveys = ({
  loading,
  createNew,
  organizations,
  surveyState,
  setSurveyState,
  createFolder,
  updateFolder,
  deleteFolder,
  surveysList = [],
  changeFolder,
  folderId,
  currentFolder = { pathId: [], pathName: [] },
  moveSurveyOrFolderToFolder,
  quickAccessSurveys = [],
  searchTerm,
  onSearch
}) => {
  const { t } = useTranslation()
  const [openDropdownId, setOpenDropdownId] = useState(false)
  const [openCreateDropdown, setOpenCreateDropdown] = useState(false)
  const [openFolderNameModal, setOpenFolderNameModal] = useState(false)
  const [folderName, setFolderName] = useState('')
  const [isEditFolder, setIsEditFolder] = useState(false)
  const [openMoveToFolderModal, setOpenMoveToFolderModal] = useState(false)
  const [moveToFolder, setMoveToFolder] = useState({
    id: null,
    actionType: ''
  })
 
  const canCreateSurvey =
    isUserAuthenticatedAsOperator() ||
    isUserAuthenticatedAsSuperAdmin() ||
    isUserAuthenticatedAsPowerUser()

  const submitFolderName = name => {
    if (isEditFolder === true) {
      createFolder(name)
    } else {
      updateFolder(name, isEditFolder)
    }
    setFolderName('')
    setOpenFolderNameModal(false)
  }

  const createDropdownMenu = (
    <React.Fragment>
      <Menu onClick={() => setOpenCreateDropdown(false)}>
        <Menu.ItemGroup>
          <Item
            onItemHover={() => {}}
            onClick={() => {
              setFolderName('')
              setOpenCreateDropdown(false)
              setOpenFolderNameModal(true)
              setIsEditFolder(true)
            }}
            rootPrefixCls='ant-menu'
            key='0'
          >
            <Icon type='folder' style={{ fontSize: '1.2em' }} />
            {t('components.operatorSurveys.createNewFolder')}
          </Item>
        </Menu.ItemGroup>
        <Menu.ItemGroup>
          <Item
            onItemHover={() => {}}
            onClick={() => {
              setOpenCreateDropdown(false)
              createNew()
            }}
            rootPrefixCls='ant-menu'
            key='1'
            data-testid='create-survey'
          >
            <Icon type='border' style={{ fontSize: '1.2em' }} />
            {t('components.operatorSurveys.createNewSurvey')}
          </Item>
        </Menu.ItemGroup>
      </Menu>
    </React.Fragment>
  )

  return (
    <Container>
      <BreadcrumbsRow type='flex' align='middle' gutter={12}>
        <Col span={18}>
          <BreadcrumbFolder folderId={folderId} />
        </Col>
        <Col span={4}>
          <Select
            name='surveySearchState'
            size='large'
            value={surveyState}
            options={surveySearchStateOptions}
            onChange={newValue => {
              setSurveyState(newValue)
            }}
            placeholder={t(
              `components.createTasterAccount.forms.third.foodAllergies.placeholder`
            )}
          />
        </Col>
        <Col span={2}>
          {canCreateSurvey && (
            <CreationDropdownContainer>
              <Dropdown
                overlay={createDropdownMenu}
                trigger={['click']}
                visible={openCreateDropdown}
                onVisibleChange={change =>
                  setOpenCreateDropdown(!openCreateDropdown)
                }
              >
                <Button
                  size='large'
                  onClick={() => setOpenCreateDropdown(!openCreateDropdown)}
                  data-testid='open-dropdown'
                >
                  {t('components.operatorSurveys.createNew')}
                </Button>
              </Dropdown>
            </CreationDropdownContainer>
          )}
        </Col>
      </BreadcrumbsRow>
      {/* <hr /> */}
      {(quickAccessSurveys && quickAccessSurveys.length) || loading ? (
        <Divider orientation='left'>
          {t('components.operatorSurveys.quickAccess')}
        </Divider>
      ) : null}
      <Row type='flex' gutter={8} align='middle' justify='space-around'>
        {loading ? (
          <Loader />
        ) : (
          quickAccessSurveys.map((survey, index) => {
            const {
              minimumProducts,
              products,
              isScreenerOnly,
              compulsorySurvey
            } = survey

            let typeOfProducts = t('components.operatorSurveys.singleProduct')
            if (products && survey.products.length > 1) {
              typeOfProducts = t('components.operatorSurveys.multipleProducts')
              if (minimumProducts > 1) {
                typeOfProducts = t(
                  'components.operatorSurveys.multipleTastings'
                )
              }
            }

            if (isScreenerOnly) {
              typeOfProducts = t('components.operatorSurveys.screenerOnly')
            }

            if (compulsorySurvey) {
              typeOfProducts = t('components.operatorSurveys.compulsory')
            }

            const surveyState = getSurveyStateSuffix(survey.state)
            return (
              <Col span={8} key={index}>
                <QuickAccessCard>
                  <Row type='flex' align='middle'>
                    <Col span={23}>
                      <TooltipWrapper
                        helperText={`${survey.name} ${surveyState}`}
                      >
                        <Col span={12}>
                          <QuickAccessTitle>
                            {`${survey.name} ${surveyState}`}
                          </QuickAccessTitle>
                          <QuickAccessDetails>
                            {typeOfProducts}
                          </QuickAccessDetails>
                        </Col>
                      </TooltipWrapper>
                    </Col>
                    <Col span={1}>
                      {getDropdownMenu(
                        survey,
                        t,
                        openDropdownId,
                        setOpenDropdownId
                      )}
                    </Col>
                  </Row>
                </QuickAccessCard>
              </Col>
            )
          })
        )}
      </Row>

      <Divider orientation='left'>
        {t('components.operatorSurveys.surveys')}
      </Divider>

      <Row type='flex' justify='end'>
        <SearchBarContainer>
          <SearchBar
            placeholder={t('placeholders.search')}
            withIcon
            handleChange={onSearch}
            value={searchTerm}
          />
        </SearchBarContainer>
      </Row>
      {loading ? (
        <Row type='flex' justify='center'>
          <Loader />
        </Row>
      ) : (
        <OperatorSurveysList
          loading={loading}
          surveys={surveysList}
          organizations={organizations}
          showHeader={false}
          showActions
          deleteFolder={id => {
            AlertModal({
              title: t('components.operatorSurveys.deleteFolderTitle'),
              description: t(
                'components.operatorSurveys.deleteFolderDescription'
              ),
              handleCancel: () => {},
              handleOk: () => deleteFolder(id)
            })
          }}
          editFolder={(id, name) => {
            setIsEditFolder(id)
            setOpenFolderNameModal(true)
            setFolderName(name)
          }}
          moveToFolder={(id, actionType) => {
            setOpenMoveToFolderModal(true)
            setMoveToFolder({
              id: id,
              actionType: actionType
            })
          }}
          onDoubleClickRow={changeFolder}
          currentFolder={currentFolder}
        />
      )}
      {openFolderNameModal && (
        <CreateFolderModal
          visible={openFolderNameModal}
          onCancel={() => {
            setFolderName('')
            setOpenFolderNameModal(false)
          }}
          onOk={submitFolderName}
          folderName={folderName}
        />
      )}
      {openMoveToFolderModal && (
        <MoveToFolderModal
          visible={openMoveToFolderModal}
          toggleModal={setOpenMoveToFolderModal}
          parentFolderId={moveToFolder && moveToFolder.id}
          actionType={moveToFolder && moveToFolder.actionType}
          selectedId={moveToFolder && moveToFolder.id}
          moveSurveyOrFolderToFolder={moveSurveyOrFolderToFolder}
        />
      )}
    </Container>
  )
}

export default OperatorSurveys
