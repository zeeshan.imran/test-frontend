import styled from 'styled-components'
import colors from '../../utils/Colors'
import {
  Menu as AntMenu,
  Button as AntButton,
  Divider as AntDivider,
  Row
} from 'antd'

export const Container = styled.div`
  background-color: ${colors.WHITE};
  padding: 2.4rem 3.2rem 3.2rem;
`

export const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 6.4rem;
`

export const SearchBarContainer = styled.div`
  width: 27rem;
  margin-bottom: 10px;
`
export const CreationDropdownContainer = styled.div`
  text-align: right;
`

export const Button = styled(AntButton)``

export const QuickAccessCard = styled.div`
  .ant-row-flex {
    border: 1px solid #ccc;
    border-radius: 5px;
    padding: 10px;
  }
`

export const ActionsContainer = styled.div`
  display: flex;
  justify-content: flex-end;
`

export const Item = styled(AntMenu.Item)`
  color: ${colors.MODAL_LINK_COLOR};
  &:hover {
    span {
      color: ${colors.MODAL_LINK_HOVER_COLOR};
    }
  }
`

export const QuickAccessTitle = styled.div`
  font-size: 16;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`

export const QuickAccessDetails = styled.div`
  font-size: 12;
  color: rgba(0, 0, 0, 0.45);
`

export const Divider = styled(AntDivider)`
  color: #333;
  font-weight: normal;
  margin: 10px 0px !important;
`

export const BreadcrumbsRow = styled(Row)`
  margin-right: 0px !important;
`
