import React, { useState } from 'react'
import { Modal, Form } from 'antd'
import Input from '../../../components/Input'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { useTranslation } from 'react-i18next'

const CreateFolderModal = ({ visible, onCancel, folderName, onOk }) => {
  const { t } = useTranslation()
  const [name, setName] = useState(folderName)
  const [isError, setIsError] = useState(false)

  return (
    <Modal
      title={t('components.operatorSurveys.folderName')}
      visible={visible}
      onOk={() => {
        onOk(name)
      }}
      onCancel={onCancel}
      okButtonProps={{ disabled: !name || isError }}
    >
      <Formik
        enableReinitialize
        validateOnBlur
        validationSchema={Yup.object().shape({
          name: Yup.string()
            .test(
              'not-empty',
              t('validation.folder.name.required'),
              value => !/^[ ]+$/.test(value)
            )
            .test(
              'not-empty',
              t('validation.folder.name.withoutSpaces'),
              value => /^(?! ).*/.test(value)
            )
            .required(t('validation.folder.name.required'))
        })}
        initialValues={{
          name
        }}
        render={({ values, errors, setFieldValue }) => {
          if (errors.name && !isError) {
            setIsError(true)
          }
          if (!errors.name && isError) {
            setIsError(false)
          }
          return (
            <Form.Item
              help={errors.name}
              validateStatus={errors.name ? 'error' : 'success'}
            >
              <Input
                value={values.name}
                onChange={event => {
                  const newName = event.target.value
                  if (newName.length <= 30) {
                    setFieldValue('name', newName)
                    setName(newName)
                  }
                }}
                size='default'
                required
              />
            </Form.Item>
          )
        }}
      />
    </Modal>
  )
}

export default CreateFolderModal
