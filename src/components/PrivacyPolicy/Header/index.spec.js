import React from 'react'
import { mount } from 'enzyme'
import Header from '.'

describe('Header', () => {
  let testRender
  let desktop

  beforeEach(() => {
    desktop = ['top', 'middle', 'bottom']
  })

  afterEach(() => {
    testRender.unmount()
  })
  test('should render Header', async () => {
    testRender = mount(<Header align={'top'} desktop={desktop} />)
    expect(testRender.find(Header)).toHaveLength(1)

    expect(testRender.find('img').prop('src')).toEqual(
      'FlavorWiki_Full_Logo.png'
    )
  })
})
