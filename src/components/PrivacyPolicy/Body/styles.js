import styled from 'styled-components'
import colors from '../../../utils/Colors'
import { family } from '../../../utils/Fonts'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 2.5rem;
  padding: 2.5rem 3.5rem 0.1rem;
  max-width: 950px;
  scroll-behavior: smooth;
`

export const Title = styled.span`
  width: 100%;
  font-family: ${family.primaryLight};
  font-size: 3.2rem;
  color: ${colors.LIGHT_OLIVE_GREEN};
  margin-bottom: 0.9rem;
  user-select: none;
`

export const Subtitle = styled.div`
  display: block;
  width: 100%;
  font-family: ${family.primaryLight};
  font-weight: bold;
  font-size: 2.5rem;
  margin-top: 3.2rem;
  user-select: none;
`

export const Footer = styled.div`
  width: 100%;
  border-top: 1px solid;
  margin-top: 3.2 rem;
`

export const MainText = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding-bottom: 3.2rem;
`

export const Paragraph = styled.span`
  display: block;
  margin-bottom: 1.7rem;
  user-select: none;
`

export const Bold = styled.span`
  user-select: none;
  font-weight: bold;
`

export const Link = styled.a`
  display: inline;
  color: ${colors.MID_BLUE};
  text-decoration: underline;
`

export const List = styled.ol``

export const Item = styled.li`
  display: block;
  margin-left: 0.9rem;
  margin-bottom: 1.7rem;
  user-select: none;
`

export const Index = styled.a`
  display: block;
  margin-left: 0.9rem;
  margin-bottom: 1.7rem;
  user-select: none;
`

export const AnchoredSubtitle = styled.a`
  display: block;
  width: 100%;
  font-family: ${family.primaryLight};
  font-weight: bold;
  font-size: 2.5rem;
  margin-top: 3.2rem;
  user-select: none;
`

export const Rectangle = styled.div`
  border: 2px solid;
  padding: 3.5rem;
  margin-top: 3.5rem;
  margin-bottom: 3.5rem;
`

export const Underline = styled.span`
  text-decoration: underline;
  user-select: none;
`
