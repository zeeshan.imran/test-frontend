import React from 'react'
import {
  Container,
  Title,
  Subtitle,
  Footer,
  MainText,
  Bold,
  Link,
  Paragraph,
  // Rectangle,
  List,
  Item,
  Underline as U,
  Index,
  AnchoredSubtitle
} from './styles.js'

const Body = () => {
  return (
    <Container>
      <MainText>
        <Title>FLAVORWIKI PRIVACY AND PERSONAL DATA PROTECTION POLICY</Title>
        <Subtitle>Index</Subtitle>
        <List>
          <Index href='#Privacy Statement'>• Privacy Statement</Index>
          <Index href='#Glossar'>• Glossary</Index>
          <Index href='#The Data Controller'>• The Data Controller</Index>
          <Index href='#FlavorWiki Core Activity – Service Catalog and Lawful Basis'>
            • FlavorWiki Core Activity – Service Catalog and Lawful Basis
          </Index>
          <Index href='#WHAT Personal Data is “treated” by FlavorWiki'>
            • WHAT Personal Data is “treated” by FlavorWiki
          </Index>
          <Index href='#WHAT “Treatment” occurs over Personal Data'>
            • WHAT “Treatment” occurs over Personal Data
          </Index>
          <Index href='#HOW is Personal Data Security, Privacy and Confidentiality assured'>
            • HOW is Personal Data Security, Privacy and Confidentiality assured
          </Index>
          <Index href='#WHY and with WHO is Personal Data Shared'>
            • WHY and with WHO is Personal Data Shared
          </Index>
          <Index href='#For HOW LONG is Personal Data maintained'>
            • For HOW LONG is Personal Data maintained
          </Index>
          <Index href='#HOW to exercise Data Subjects’ rights'>
            • HOW to exercise Data Subjects’ rights
          </Index>
          <Index href='#FlavorWiki’ DPO'>• FlavorWiki’ DPO</Index>
        </List>
        <AnchoredSubtitle name='Privacy Statement'>
          Privacy Statement
        </AnchoredSubtitle>
        <Paragraph>
          This Privacy Policy describes how FlavorWiki processes Personal Data
          pertaining to natural persons that interact with it as website/
          platform visitors/ users or Prospect Customers/ Customers (meaning how
          such Personal Data is: Collected; Stored; Accessed; Processed and
          Shared) both online and by other means, such as by phone while
          Customers order FlavorWiki products; as well as which are the Lawful
          Bases towards such Processing activities.
        </Paragraph>
        <Paragraph>
          This Privacy Policy is provided to you, in line with the following
          Personal Data Protection Legislation:
        </Paragraph>
        <List>
          <Item>
            <Paragraph>
              • The Regulation (EU) 2016/679 of the European Parliament and of
              the Council of 27 April 2016 also known as the General Data
              Protection Regulation (GDPR), which became enforceable across the
              EU and the EEA from May 25th, 2018 having replaced the previous
              Directive 95/46/EC; In Ireland, the national law, which amongst
              other things, gives further effect to the GDPR, is the Data
              Protection Act 2018 (‘the 2018 Act’).
            </Paragraph>
          </Item>
          <Item>
            <Paragraph>
              • The Directive 2009/136/EC of the European Parliament and of the
              Council of 25 November 2009 also known as the ePrivacy Directive,
              amending the Directive 2002/22/EC on universal service and users’
              rights relating to electronic communications networks and
              services, Directive 2002/58/EC concerning the processing of
              personal data and the protection of privacy in the electronic
              communications sector and Regulation (EC) No 2006/2004 on
              cooperation between national authorities responsible for the
              enforcement of consumer protection laws;
            </Paragraph>
          </Item>
          <Item>
            <Paragraph>
              • The California Consumer Privacy Act 2018, assembly Bill of the
              State of California United States of America No. 375, under
              CHAPTER 55, an act to add Title 1.81.5 (commencing with Section
              1798.100) to Part 4 of Division 3 of the Civil Code, relating to
              privacy and approved by Governor June 28, 2018. Filed with
              Secretary of State June 28, 2018 and enforceable from January 1st,
              2020 onwards.
            </Paragraph>
          </Item>
        </List>
        <Paragraph>
          The primary goal of Processing Personal Data is to identify authorized
          users (logged in users) of FlavorWiki platform, so they can benefit
          from its functionalities as per their user profile.
        </Paragraph>
        <Paragraph>
          FlavorWiki (both as an organization as each of its staff members) is
          perfectly aware of the fact that Personal Data may represent a risk
          towards you if accessed by unhautorized 3rd parties; and that is why a
          set of Policies, Operational Processes, and mechanisms (technological
          and human-based) has been developed to ensure that the Personal Data
          entrusted by you to FlavorWiki will be maintained, handled and shared
          in a manner that ensures its Security, Accuracy, Confidentiality, and
          Privacy, hence ensuring your Personal Data Protection.
        </Paragraph>
        <Paragraph>
          Personal Data is exclusively Processed under the scope and purpose of
          agreed Services between FlavorWiki and either the Data Subject’s
          employer via a Services Contract (where a Corporate Client of Metter)
          or the Data Subject him/ herself (natural person to whom such Data
          pertains to) via either a Contract or Explicit Consent towards
          required Personal Data Processing Activities.
        </Paragraph>
        <Paragraph>
          Regardless of which of the above applies, each and every Data Subject
          maintains full control over the Personal Data that pertains to him/
          her as well as the Personal Data Processing Activities undertaken by
          FlavorWiki (as defined under the European General Data Protection
          Regulation [GDPR] as Data Subject's Rights and also the rights
          described under the California Consumer Protection Act[CCPA]).
        </Paragraph>
        <Subtitle>Applicability</Subtitle>
        <Paragraph>
          FlavorWiki reserves the right to modify this Privacy Policy at all
          times by posting an updated version on its websites.
        </Paragraph>
        <AnchoredSubtitle name='The Data Controller'>
          The Data Controller
        </AnchoredSubtitle>
        <Paragraph>
          Smart Sensory Analytics LLC, registration number 82-1401303
          established in 140 Jonathans Drive, West End, NC 27376 U.S. (operating
          under the brand “FlavorWiki”) is the entity that acts as the Data
          Controller for the purpose of this Privacy Policy and all data
          processing practices herein contemplated. All questions or requests
          regarding the processing of the personal data under our control or
          processing may be addressed to{' '}
          <Link
            rel='noopener'
            href='mailto:privacy@FlavorWiki.com'
            target='blank'
          >
            privacy@FlavorWiki.com
          </Link>
        </Paragraph>
        <Paragraph>
          FlavorWiki DPO contacts
          <br />
          Mr. Rui Serrano
          <br />
          Country: Portugal, European Union
          <br />
          email -{' '}
          <Link rel='noopener' href='mailto:phrdpoaas@gmail.com' target='blank'>
            phrdpoaas@gmail.com
          </Link>
          <br />
          phone - +351932579434
        </Paragraph>
        <AnchoredSubtitle name='FlavorWiki Core Activity – Service Catalog and Lawful Basis'>
          FLAVORWIKI Core Activity – Service Catalog and “Lawful Basis”
        </AnchoredSubtitle>
        <Paragraph>
          FLAVORWIKI renders a set of services towards other companies as well
          as natural persons, which successful completion requires “Personal
          Data Processing Activities”.
        </Paragraph>
        <Paragraph>
          Under this scope, FLAVORWIKI’ Service Catalog comprehends the
          following services and applicable “Lawful Basis” for processing
          Personal Data (respectively), meaning how is FLAVORWIKI permitted by
          law to process “Personal Data” under such services scope:
        </Paragraph>
        <List>
          <Item>
            • <U>Reaching out to prospect Natural Persons</U>
            <Paragraph />
            <Paragraph>
              FlavorWiki, in general, does not directly reach out to natural
              persons with whom it has no established relationship (although it
              could do so if observing GDPR Article 14); the way in which
              FlavorWiki reaches out to prospects (individuals who may be
              interested in its Services Portfolio), consists of publishing
              information on Social Media platforms about its services. Those
              Prospects who are members of such platforms and become interested
              in the posted message may click on the links made available which
              will lead them to one of FlavorWiki’s website landing pages.
            </Paragraph>
            <Paragraph>
              Nevertheless, the “Lawful Basis” towards reaching out to
              prospects, including directly while strictly observing by GDPR
              article 14, consists of Legitimate Interest.
            </Paragraph>
          </Item>
          <Item>
            •{' '}
            <U>
              Flavor Experience - allowing Data Subjects to participate in
              flavor tests and compensating them for it
            </U>
            <Paragraph />
            <Paragraph>
              The Lawful Basis in the case of this service consists of “Explicit
              Consent”.
            </Paragraph>
            <Paragraph>
              The Data Subjects adhere to their own free will and having
              understood the context, purpose, and scope of this service (and
              inherent “Personal Data Processing Activities”) while providing
              their Consent to it.
            </Paragraph>
          </Item>
          <Item>
            •{' '}
            <U>
              Allowing Corporate Client companies to use FlavorWiki’ platform as
              a Service in order to conduct their own flavor tests
            </U>
            <Paragraph />
            <Paragraph>
              In this case, the applicable Lawful Basis for Processing by
              FlavorWiki consists of the fulfillment of contractual obligations,
              since it plays the role of a Processor, while its Corporate Client
              is the Controller.
            </Paragraph>
            <Paragraph>
              In the case of flavor tests as a Service, the Controller
              (Corporate Client) is the sole responsible entity for ensuring
              that the Data Subject is fully aware and informed of Personal Data
              Processing scope, context, and purpose including the service
              components enabled by FlavorWiki’ platform and that there is a
              documented Lawful Basis in place from its side towards the Data
              Subject.
            </Paragraph>
          </Item>
          <Item>
            • <U>Flavor Tests</U>
            <Paragraph />
            <Paragraph>
              Besides allowing its Corporate Clients to conduct their own flavor
              tests over FlavorWiki’ platform as a Service, the company also
              provides this service on its own towards Corporate Clients.
            </Paragraph>
            <Paragraph>
              The Corporate Client is also a Controller, for it is the one that
              established which tests shall run addressing which products or
              services and towards what type of Customers (Data Subjects).
            </Paragraph>
            <Paragraph>
              Notwithstanding what has been mentioned above, FlavorWiki is the
              party under a Services Contract with its Corporate Client which
              directly addresses the Data Subjects and Processes their Personal
              Data, because, under this Service scope, no Personal Data is
              shared by FlavorWiki with its Corporate Client, merely anonymized
              Data, hence the Lawful Basis for Processing Personal Data consists
              of Explicit Consent from the Data Subjects, making FlavorWiki also
              a Controller.
            </Paragraph>
          </Item>
          <Item>
            • <U>Generic Marketing information</U>
            <Paragraph />
            <Paragraph>
              FlavorWiki sends out information towards its registered Data
              Subjects (meaning those whom have participated or have enlisted to
              participate in flavor tests) under the Lawful Basis of Legitimate
              Interest that derives from the fact that those individuals have
              demonstrated interest to participate in flavor tests, hence are
              interested to know which flavor tests are available as well as
              related information.
            </Paragraph>
            <Paragraph>
              Nevertheless, those Data Subjects may opt-out from this
              information service at any time by requesting it towards
              FlavorWiki while exercising their Rights under GDPR.
            </Paragraph>
          </Item>
        </List>
        <AnchoredSubtitle name='WHAT Personal Data is “treated” by FlavorWiki'>
          WHAT “Personal Data” is subject to Processing by FlavorWiki
        </AnchoredSubtitle>
        <Paragraph>
          In the case of FlavorWiki Direct Flavor Test service the following
          categories of Personal Data will be directly processed:
        </Paragraph>
        <List>
          <Item>• Contact Data (e.g. Name; Email; Phone number)</Item>
          <Item>• Location Data (e.g. Country; City; State)</Item>
          <Item>
            • Personal Information (e.g. Gender; Birth Date; Children)
          </Item>
          <Item>
            • Financial Data (e.g. Annual income; electronic payment username)
          </Item>
          <Item>
            • Consuming habits (e.g. Consuming habits; usual purchasing place)
          </Item>
          <Item>• Flavor Test feedback (e.g. Flavor; Aroma)</Item>
        </List>
        <Paragraph>
          <U>
            The univocal identification and documentation of the “Data Subject”
          </U>
        </Paragraph>
        <Paragraph>
          FlavorWiki adherent flavor test subjects (Data Subjects who have
          decided to adhere to the Flavor Experience) will be registered on
          FlavorWiki digital platform, therefore having a Login which enables
          their univocal identification and authentication towards the Service.
        </Paragraph>
        <Paragraph>
          In those cases where “Data Subjects” who may interact with FlavorWiki
          cannot be identified through their Login, FlavorWiki will resort to a
          two-factor authentication consisting of sending/ receiving messages
          that imply some action or permission/ objection to “Personal Data
          Processing Activities” by e-mail and having it confirmed with a unique
          individual code that was conveyed to those “Data Subjects” by SMS to
          their mobile phones.
        </Paragraph>
        <AnchoredSubtitle name='WHAT “Treatment” occurs over Personal Data'>
          WHAT Treatment occurs over “Personal Data”
        </AnchoredSubtitle>
        <Paragraph>
          <Bold>
            <U>Gathering/ Collection</U>
          </Bold>
        </Paragraph>
        <Paragraph>
          As previously mentioned, although in general FlavorWiki exclusively
          gathers the Personal Data directly from the Data Subjects themselves,
          when they reach out to FlavorWiki via the Social Media published
          Links, it may happen that FlavorWiki gathers a minimum amount of
          “Personal Data” that enables to entice contact with a “Data Subject”
          from a 3rd party source.
        </Paragraph>
        <Paragraph>
          In such case the type of “Personal Data” collected consists of basic
          minimum Contact and Location Data as well as Personal Information that
          is relevant to decide whom will be contacted, namely (yet not
          exclusively):
        </Paragraph>
        <List>
          <Item>• Name</Item>
          <Item>• Email</Item>
          <Item>• Phone number</Item>
          <Item>• Country, City, State</Item>
          <Item>• Gender</Item>
          <Item>• Age</Item>
        </List>
        <Paragraph>
          Where “Personal Data” was collected from a 3rd party (including
          “public sources”), FlavorWiki will act as per “GDPR” Article 14
          ruling, meaning the “Data Subject” will be contacted and informed
          about which type of “Personal Data” was gathered by FLAVORWIKI, for
          which purpose and from which source and the “Data Subject” will be
          requested to provide his/ her Explicit Consent towards “Personal Data”
          Processing under the conveyed service scope.
        </Paragraph>
        <Paragraph>
          If the “Data Subject” either does not reply within 28 days or his/ her
          answer is of not consenting towards FlavorWiki Processing his/ her
          “Personal Data”, FLAVORWIKI shall erase the “Personal Data” which has
          been collected about that “Data Subject”.
        </Paragraph>
        <Paragraph>
          To prevent further contact within the same scope, the “Data Subject’s”
          Name and e-mail address will be “blacklisted” (therefore maintained by
          FlavorWiki) on a dedicated repository that is accessible to relevant
          internal Departments only.
        </Paragraph>
        <Paragraph>
          Detailed and extensive amounts of “Personal Data” that is vital for
          rendering FlavorWiki services will exclusively be collected either
          directly towards the “Data Subjects” themselves or through 3rd party
          entities having the “Data Subject” fully informed of such collection
          process and once he/ she has provided his/ her explicit consent to it.
        </Paragraph>
        <Paragraph>
          FlavorWiki does not profile “Data Subjects” without their knowledge
          and consent, least of all from public platforms such as Social Media
          or “Affiliate” entities’ information repositories.
        </Paragraph>
        <Paragraph>
          When a Data Subject visits FlavorWiki’ websites, session cookie files
          are either placed on his/ her browser device, or the website reads
          such already existing files.
        </Paragraph>
        <Paragraph>
          FlavorWiki exclusively uses those cookies that record information
          about the “IT architecture and Landscape” of the device being used by
          the visitor (e.g. browser; browsing preferences; other…) however,
          without identifying that visitor personally (as a Data Subject).
        </Paragraph>
        <Paragraph>
          This information, except for IP addresses, is never combined with the
          data pertaining to either “Prospect Customers” or “Customers”, thus
          not leading to the identification and habits “profiling” of any
          particular Data Subject.
        </Paragraph>
        <Paragraph>
          IP addresses are exclusively cross-referenced with other data for the
          purpose of safekeeping the company from fraud attempts plus with
          regards to “Customers” documenting operations by (1) verifying the
          identity of a person signing in, and (2) making records of your
          consent and other legally binding actions (Legitimate Interest).
        </Paragraph>
        <Paragraph>
          The IP address is also used (while segregated) for the purposes of web
          analytics (via Google Analytics).
        </Paragraph>
        <Paragraph>
          For detailed information about cookies in use and similar employed
          technologies please refer to the{' '}
          <U>
            <Bold>Cookies Policy</Bold>
          </U>
          .
        </Paragraph>

        <Paragraph>
          <Bold>
            <U>Storing</U>
          </Bold>
        </Paragraph>
        <Paragraph>
          FLAVORWIKI is a Digital company, which means that the overwhelming
          amount of Data and information the company requires to operate is
          exclusively maintained under Digital format on IT Systems.
        </Paragraph>
        <Paragraph>
          Paper is used exclusively either for short periods of time and once no
          longer required properly disposed of (shredders) or if mandatory under
          any accessory local legal requirement which implies having “Personal
          Data” printed and stored.
        </Paragraph>
        <Paragraph>
          FLAVORWIKI Service “IT Landscape” consists of a core dedicated Service
          Platform for flavor tests registry, that is segregated by FlavorWiki’
          direct Services and all the other Corporate Clients which use it as a
          Service. In the latter case, each Corporate Client “work area” is also
          fully segregated to ensure Data Security, Confidentiality and the Data
          Subject’s privacy.
        </Paragraph>
        <Paragraph>
          Personal Data Processing by FlavorWiki requires the contribution of
          some Partners that deliver part of the Service and with which only the
          minimum amount of Personal Data that is mandatory for those service
          components to be delivered shall be shared, namely:
        </Paragraph>
        <List>
          <Item>• Tableau</Item>
          <Item>• AWS</Item>
          <Item>• Hubspot</Item>
          <Item>• MongoDB</Item>
        </List>
        <Paragraph>
          FLAVORWIKI acts as the Controller and these “Partners” as
          “Processors”, meaning they will not undergo any “Personal Data
          Processing Activities” activities towards information registered,
          submitted or conveyed by FLAVORWIKI unless under the scope of
          contracted services and that is agreed and documented under an
          existing “DPA” between the parties.
        </Paragraph>
        <Paragraph>
          <Bold>
            <U>Processing</U>
          </Bold>
        </Paragraph>
        <Paragraph>
          “Personal Data Processing Activities”, in specifics Processing
          Consists of:
        </Paragraph>
        <List>
          <Item>
            • Gathering Corporate Client requirements towards a specific Flavor
            Test;
          </Item>
          <Item>
            • Identifying the registered Test Subjects (Data Subjects who have
            adhered to the Service) who are best-fit individuals for the test at
            hand;
          </Item>
          <Item>
            • Inform and Invite those Test Subjects to participate in the Flavor
            Test;
          </Item>
          <Item>
            • Gather and register the list of Test Subjects towards the specific
            Flavor Test;
          </Item>
          <Item>
            • Enable the Flavor Test to run and retrieve Test Feedback Data by
            Data Subject;
          </Item>
          <Item>
            • Prepare a Flavor Test report towards the Corporate Client where
            the Data has been anonymized.
          </Item>
        </List>
        <Paragraph>
          Processing activities (as well as storing) by FlavorWiki side occurs
          in the EU, on its partner Hetzner Data Center in Germany.
        </Paragraph>
        <Paragraph>
          <Bold>
            <U>Sharing</U>
          </Bold>
        </Paragraph>
        <Paragraph>
          A portion of FLAVORWIKI’ “IT Landscape” is Cloud-based, therefore
          tools and services are either hosted or enabled by 3rd parties
          (“Partners”) and “Personal Data” is shared with those entities, not in
          the sense that they will change it or process it but that they will
          either store it or have their software processing it with FlavorWiki
          users logged.
        </Paragraph>
        <Paragraph>
          The existing DPAs with these types of “Partners” rule that these
          companies may not copy, use or process “Personal Data” “submitted” by
          FlavorWiki unless to enable FlavorWiki with storage or processing
          results that derive from the services rendered by FlavorWiki under
          defined “Lawful Basis” towards the “Data Subjects”.
        </Paragraph>
        <Paragraph>
          Yet (as previously mentioned) another set of “Partners” actively
          processes “Personal Data” submitted by FlavorWiki within the scope of
          complementary specific services that are part of FlavorWiki’s overall
          Service Catalog. These companies have agreed under a DPA to
          exclusively process “Personal Data” “enabled” by FlavorWiki as per
          FlavorWiki definitions and not to share or make it available to any
          third parties which do not play the role of “Sub-processors” within
          the scope of those shared services.
        </Paragraph>
        <Paragraph>
          In both cases, the DPAs also gather mutual commitment (from FlavorWiki
          and its “Partners”) to ensure that their “Sub-processors” will act in
          strict observance of “GDPR”.
        </Paragraph>
        <Paragraph>
          If some products from our Corporate Clients have to be delivered to
          “test subjects” (natural persons whom will be performing the flavor
          test), FlavorWiki may share with logistics operators or the post
          office the minimum amount of Personal Data that allows the delivery of
          such products to those Data Subjects.
        </Paragraph>
        <Paragraph>
          Last, FlavorWiki (as any company) and under specific circumstances may
          be bound by local legislation to share or make available some
          “Personal Data” to legal/ government authorities (as an example we
          have the case of invoices).
        </Paragraph>
        <Subtitle>The Principle of Data Minimization</Subtitle>
        <Paragraph>
          FlavorWiki takes every reasonable step to ensure that Personal Data
          under its direct Processing activities (as the Controller) is
          absolutely limited to the amount and type that is necessary to deliver
          its Services towards its Customers and Corporate Clients as it has
          been agreed by those, either via Consent or a Contract not maintained
          over redundant repositories nor for any longer than required under the
          scope of agreed services.
        </Paragraph>
        <Paragraph>
          However, Customers and Corporate Clients alike will act also as Joint
          Controllers and the same is not “arguable” by FlavorWiki with regards
          to those for it solely depends on their Personal Data Processing
          “scope” and “purpose”.
        </Paragraph>
        <AnchoredSubtitle name='WHY and with WHO is Personal Data Shared'>
          International Data Transfers
        </AnchoredSubtitle>
        <Paragraph>
          Some of FlavorWiki’s partners (Processors or Controllers) are
          established on 3rd countries (meaning not the EU Member States nor
          within the European Economic Area), as well as FlavorWiki itself;
          therefore not enjoying an adequacy qualification by the European
          Commission pursuant to GDPR Article 45 ruling.
        </Paragraph>
        <Paragraph>
          To make such transfers fully compliant with the GDPR, the Data
          Processing Agreements with those partners include the EU Standard
          Contractual Clauses in accordance with Commission Decision of 5
          February 2010 on standard contractual clauses for the transfer of
          personal data to processors established in third countries under
          Directive 95/46/EC of the European Parliament and of the Council and
          the European Court of Justice decision of July 16th, 2020 that renders
          the Privacy Shield not applicable and rules instead to solely use the
          Standard Contractual Clauses.
        </Paragraph>
        <Paragraph>
          And, more relevant, FlavorWiki both ensures having internal Security
          Measures and Processes in place as performing a detailed assessment
          regarding such partners.
        </Paragraph>

        <AnchoredSubtitle name='HOW is Personal Data Security, Privacy and Confidentiality assured'>
          HOW is “Personal Data” Security, Privacy and Confidentiality assured
        </AnchoredSubtitle>
        <Paragraph>
          FlavorWiki has its “IT Landscape” configured and monitored under the
          strictest Security market standards and it has reviewed and adopted
          changes to its operational processes in a manner that ensures
          compliance with the requirements posed under “GDPR” towards “Personal
          Data” Protection. This means to assure its Confidentiality and Privacy
          while under “Personal Data Processing Activities” performed by itself
          and its “Partners” within the scope of FlavorWiki rendered services.
        </Paragraph>

        <AnchoredSubtitle name='For HOW LONG is Personal Data maintained'>
          For HOW LONG is “Personal Data” maintained
        </AnchoredSubtitle>
        <Paragraph>
          Data retention is one major potential risk generator towards “Personal
          Data”, since having the Data available means it may be accessed if a
          “Personal Data Breach” occurs.
        </Paragraph>
        <Paragraph>
          FlavorWiki has set the Data Retention periods according to its
          services’ lifecycle, so that in one hand, the company will not hold to
          “Personal Data” for any day longer that it is effectively necessary
          and on the other hand the risk of having needed information deleted
          prior to the end of its lifecycle within FlavorWiki Service Catalog
          scope and commitment is minimized.
        </Paragraph>
        <Paragraph>
          This means, in the case of FlavorWiki’ platform as a Service
          component, where its Corporate Clients will use the platform on their
          own, that in case the contract ends or the service comes to an end,
          FlavorWiki will allow a 1 month period for the Corporate Client to
          withdraw all of the Data and Information stored under its segregated
          repository and then FlavorWiki will erase the Data contained on such
          repository.
        </Paragraph>
        <Paragraph>
          In the case of FlavorWiki direct service, and besides the fact that
          each Data Subject is entitled to request the erasure of his/ her
          Personal Data as per defined under GDPR, where a Data Subject has not
          participated in the Flavor Experience for over 6 months and after
          FlavorWiki has attempted contact with him/ her for the period of two
          calendar weeks by the contacts available to FlavorWiki, the Data
          Subject’ Personal Data shall be erased.
        </Paragraph>

        <AnchoredSubtitle name='HOW to exercise Data Subjects’ rights'>
          HOW to exercise “Data Subjects’” rights
        </AnchoredSubtitle>
        <Paragraph>
          Those Data Subject who are individual Customers may exercise their
          Rights directly towards FlavorWiki however, those who are staff
          members from FlavorWiki Corporate Clients must address those companies
          to exercise their rights towards FlavorWiki.
        </Paragraph>
        <Paragraph>
          Under the GDPR, the Data Subject has the following set of established
          rights:
        </Paragraph>
        <List>
          <Item>
            <Paragraph>
              • <Bold>[GDPR] Right of access.</Bold> The right to obtain from
              the Controller confirmation as to whether his/ her personal data
              is being processed, and, where that is the case, access to such
              personal data as well as related information. FlavorWiki will
              share the Personal Data over a secure channel, and that (depending
              on the type of Data as well as volume) may imply the need to
              convey a “password” via an alternative communication channel to
              the Data Subject to ensure authorized secure access. Customers may
              exercise this right by reviewing information on FlavorWiki’s
              website user account area or by submitting a request as per herein
              defined ahead in this document which is the application process
              for those Data Subjects who are not FlavorWiki Customers
            </Paragraph>
          </Item>
          <Item>
            <Paragraph>
              •{' '}
              <Bold>
                [CCPA] Right to know and access your personal information
              </Bold>{' '}
              - similar to the Right of Access under the GDPR, California
              resident natural persons have the right to
            </Paragraph>
            <List>
              <Item>
                <Paragraph>
                  Know the categories of personal information we collect and the
                  categories of sources from which we got the information;
                </Paragraph>
              </Item>
              <Item>
                <Paragraph>
                  Know the business or commercial purposes for which we collect
                  and share personal information;
                </Paragraph>
              </Item>
              <Item>
                <Paragraph>
                  Know the categories of third parties and other entities with
                  whom we share personal information;
                </Paragraph>
              </Item>
              <Item>
                <Paragraph>
                  Access the specific pieces of personal information we have
                  collected about you.
                </Paragraph>
              </Item>
            </List>
          </Item>
          <Item>
            <Paragraph>
              • <Bold>[GDPR] Right to rectification.</Bold> The right to obtain
              the rectification of inaccurate Personal Data pertaining to that
              Data Subject. Customers may directly amend existing information on
              FlavorWiki’s website user account area or by submitting a request
              as per herein defined ahead in this document which is the
              application process for those Data Subjects who are not FlavorWiki
              Customers.
            </Paragraph>
          </Item>
          <Item>
            <Paragraph>
              • <Bold>[GDPR] Right to erasure.</Bold> The right to have Personal
              Data pertaining to him/ her that is under Processing by FlavorWiki
              erased and therefore Processing stopped, unless a legal duty or
              have a legitimate ground to retain certain data prevents
              FlavorWiki from observing such right, in which case the Data
              Subject shall be duly informed. This right may be exercised by
              submitting a request as defined in the procedure stated below in
              this section.
            </Paragraph>
          </Item>
          <Item>
            <Paragraph>
              • <Bold>[CCPA] Right to deletion</Bold> - again in a similar
              manner to what the GDPR rules, natural persons who reside in the
              state of California may, in some circumstances, ask us to delete
              their personal data/ information. We may refuse the exercise of
              such right if it prevents us from exercising legal defence, we
              cannot do it driven from a legal obligation or there is the risk
              of by doing so, not being able to fulfil any open contractual
              obligations.
            </Paragraph>
          </Item>
          <Item>
            <Paragraph>
              • <Bold>[GDPR] The right to restrict processing.</Bold> Under
              relevant conditions set out by the law, the right to request and
              have in place processing restrictions (in scope and purpose)
              towards Personal Data that pertains to him/ her. When exercising
              this right, the Data Subject must be specific about which
              processing activities are being requested to be restricted and the
              Controller shall provide feedback to the Data Subject on either
              the completion of the request or any potential collateral impact
              that may derive from implementing the requested objection to
              Processing, asking for additional confirmation prior to
              implementing the request. This right may be exercised by
              submitting a request as defined in the procedure stated below in
              this section.
            </Paragraph>
          </Item>
          <Item>
            <Paragraph>
              • <Bold>[GDPR] The right to object to processing.</Bold> The right
              to object to processing activities that have been qualified under
              this Privacy Policy has occurred under the Lawful Base of
              Legitimate Interest by the side of FlavorWiki. The exercise of
              this right may also occur where the Data Subject wishes to opt-out
              from an existing Service (and not necessarily canceling the
              Service). When exercising this right, the Data Subject must be
              specific about which processing activities are being requested to
              stop and the Controller shall provide feedback to the Data Subject
              on either the completion of the request or any potential
              collateral impact that may derive from implementing the requested
              objection to Processing, asking for additional confirmation prior
              to implementing the request. This right may be exercised by
              submitting a request as defined in the procedure stated below in
              this section.
            </Paragraph>
          </Item>
          <Item>
            <Paragraph>
              • <Bold>[CCPA] Right to opt out of sales</Bold> - We do not “sell
              “ your data
            </Paragraph>
          </Item>
          <Item>
            <Paragraph>
              • <Bold>[GDPR] Right to data portability.</Bold> The right to
              receive the Personal Data pertaining to that Data Subject, in a
              structured, commonly used and machine-readable format as well as
              the right to transmit such Personal Data to another controller
              without hindrance. FlavorWiki will share the Personal Data over a
              secure channel, and that (depending on the type of Data as well as
              volume) may imply the need to convey a “password” via an
              alternative communication channel to the Data Subject to ensure
              authorized secure access. Customers may directly amend existing
              information on FlavorWiki’s website user account area or by
              submitting a request as per herein defined ahead in this document
              which is the application process for those Data Subjects who are
              not FlavorWiki Customers.
            </Paragraph>
          </Item>
          <Item>
            <Paragraph>
              •{' '}
              <Bold>
                [GDPR] Right to be informed about a Personal Data Breach.
              </Bold>{' '}
              The Data Subject has the right (and it is the Controller’s
              obligation by law to ensure it) to be informed of any unauthorized
              disclosure or potential disclosure of his/ her Personal Data to
              unauthorized 3rd parties within 72 hours of its occurrence.
            </Paragraph>
          </Item>
          <Item>
            <Paragraph>
              •{' '}
              <Bold>
                [GDPR] Right to lodge a complaint with a supervisory authority.
              </Bold>{' '}
              The right to lodge a complaint regarding FlavorWiki’s Processing
              activities over his/ her Personal Data towards any of the EU
              Member States data protection Supervisory Authorities. FlavorWiki
              is however also available to provide any clarification towards
              those Data Subjects who may feel that it's Processing of the
              Personal Data that pertains to them has negatively impacted them
              or somehow breached their rights under GDPR and/ or the right to
              Privacy, having such Personal Data processed in a secure manner
              and Confidentiality assurance. Data Subject may submit a complaint
              via the request process as per herein defined ahead.
            </Paragraph>
          </Item>
          <Item>
            <Paragraph>
              • <Bold>[CCPA] Right to be free from discrimination</Bold> - You
              may exercise any of the above rights without fear of being
              discriminated against. We are, however, permitted to provide a
              different price or rate to you if the difference is directly
              related to the value provided to you by your data.
            </Paragraph>
            <Paragraph>
              For any of the above-mentioned CCPA related rights, you may
              designate an authorized agent to make a request on your behalf. In
              the request, you or your authorized agent must provide including
              information sufficient for us to confirm the identity of an
              authorized agent. We are required to verify that your agent has
              been properly authorized to request information on your behalf and
              this may take additional time to fulfil your request.
            </Paragraph>
            <Paragraph>
              We will use the information you provide to make your CCPA rights
              requests to verify your identity, identify the personal
              information we may hold about you and act upon your request.
            </Paragraph>
            <Paragraph>
              We strongly recommend that you submit the email and postal address
              that you used when you created accounts, ordered subscriptions or
              signed up for a newsletter. After you submit a CCPA rights
              requests you will be required to verify access to the email
              address you submitted. You will receive an email with a follow-up
              link to complete your email verification process. You are required
              to verify your email in order for us to proceed with your CCPA
              rights requests. Please check your spam or junk folder in case you
              can't see the verification email in your inbox.
            </Paragraph>
          </Item>
        </List>
        <Paragraph>
          Any “Data Subject” may exercise his/ her rights under “GDPR” by
          reaching out to FlavorWiki’ “DPO” through the e-mail address{' '}
          <Link rel='noopener' href='privacy@FlavorWiki.com.' target='blank'>
            privacy@FlavorWiki.com.
          </Link>
        </Paragraph>
        <Paragraph>
          If you have any questions, complaints or wish to exercise your rights
          under “GDPR”, please do make clear on your message:
        </Paragraph>
        <List>
          <Item>
            • Purpose: Question; Complaint; Exercise of the “Data Subject’s”
            rights under “GDPR”
          </Item>

          <Item>• WHAT triggered your need to contact us?</Item>

          <Item>
            • WHEN did the root cause which triggered the need to contact us
            took place?
          </Item>
          <Item>
            • If a Member, your Member ID or if not a mobile phone number or
            alternative personal e-mail address so we may proceed with a
            two-factor authentication process.
          </Item>
        </List>
        <Paragraph>
          Why the need to provide alternative personal contact?
        </Paragraph>
        <Paragraph>
          Under “GDPR” only the “Data Subject” may exercise his/ her rights,
          hence companies must ensure and document that the “Data Subject” or
          his/ her legal representatives are the ones interacting with the
          company while acting over his/ her “Personal Data”. The way to ensure
          such “authentication” with regards to “Data Subjects” who do not have
          digital credentials on any FLAVORWIKI web-based platforms is to
          forward a code to that “Data Subject” via an alternative communication
          channel to the standard e-mail address which served the purpose of the
          initial contact and have a code generated by FLAVORWIKI included on
          all messages that pertain the exercise of “Data Subjects’” rights or
          actions over such “Data Subject’” “Personal Data”.
        </Paragraph>
        <AnchoredSubtitle name='Glossar'>Glossary</AnchoredSubtitle>
        <Paragraph>
          ”<Bold>Affiliate</Bold>” means any entity that directly or indirectly
          controls, is controlled by or is under common control with each Party.
          Whereas “Control,” for purposes of this definition, means direct or
          indirect ownership or control of more than 50% of the voting interests
          of the Party.
        </Paragraph>
        <Paragraph>
          “<Bold>Controller</Bold>” means the “Party” which determines the
          “Personal Data” which is forward to the other “Party” under the
          “Services” scope, and the inherent “Personal Data” Treatment”
          purposes, processes and/ or workflows which must be observed by the
          other “Party” within the mutual relationship.
        </Paragraph>
        <Paragraph>
          “<Bold>Data Protection Officer</Bold>”/ “<Bold>DPO</Bold>” means the
          natural person within a company who bear the responsibility of
          ensuring corporate compliance towards “GDPR” (as per defined under
          this Regulation), both by means of monitoring compliance status as
          well as acting towards the organization and management structure
          informing those about existing non-conformity points and the need for
          the organization to act upon them in order to make them compliant with
          “GDPR” rules, guidelines and requirements.
        </Paragraph>
        <Paragraph>
          “<Bold>Data Subject</Bold>” means the identified or identifiable
          natural person to whom “Personal Data” relates. Both Parties
          understand that the “Data Subject” is the sole owner of “Personal
          Data” which pertains to him/ her.
        </Paragraph>
        <Paragraph>
          “<Bold>Data Subjects’ Rights</Bold>” means the rights established
          towards the “Data Subjects” under “GDPR”. Please check the item below
          under the title “HOW to exercise Data Subjects’ rights”
        </Paragraph>
        <Paragraph>
          “<Bold>GDPR</Bold>” means the Regulation (EU) 2016/679 of the European
          Parliament and of the Council of 27 April 2016 on the protection of
          natural persons with regards to the “Personal Data” Treatment” and on
          the free movement of such data, while repealing and replacing the
          Directive 95/46/EC from May 25th, 2018 onwards.
        </Paragraph>
        <Paragraph>
          “<Bold>IT Landscape</Bold>” means the set of IT assets and services of
          and at the disposal of each “Party” that enables their “Personal Data”
          Treatment” operation, meaning the communications infrastructure (LAN,
          WAN, Wi-Fi networks), Data Center and technical rooms, Cloud-based
          services, workstations, software systems and tools, mobile devices in
          use, peripheral IT devices, Firewalls and web-based resources.
        </Paragraph>
        <Paragraph>
          “<Bold>Lawful Basis</Bold>” means the enlisted lawful grounds that a
          company has to entice “Personal Data” Treatment” activities under
          “GDPR”, namely (but not limited to) having documented: the “Data
          Subject’” Explicit Consent towards “Personal Data” Treatment”
          activities; the company Legitimate Interest in proceeding with
          ““Personal Data” Treatment” activities; accessory legal obligations
          that the company must observe and which entitled it to proceed with
          “Personal Data Processing Activities” activities within the limits of
          such ruling and inherent obligations; other as per defined under
          “GDPR”.
        </Paragraph>
        <Paragraph>
          “<Bold>Partner</Bold>” means any 3rd party entity towards which each
          “Party” may resort in order to ensure “Personal Data Processing
          Activities” under a “Lawful Basis” (as established by “GDPR”) and
          within the scope of agreed “Services”.
        </Paragraph>
        <Paragraph>
          “<Bold>Personal Data</Bold>” means any data which by itself or when
          cross-referenced with other data enables one to univocally identify
          one given natural person, the “Data Subject”.
        </Paragraph>
        <Paragraph>
          “<Bold>Personal Data Processing Activities</Bold>” means any operation or set of
          operations which is performed upon “Personal Data”, whether or not by
          automated means, such as collection/ retrieval; accessing
          (consultation, use); processing (organization, structuring, adaptation
          or alteration); storage (recording, erasure or destruction); sharing
          (disclosure by transmission, dissemination or otherwise making
          available, publishing).
        </Paragraph>
        <Paragraph>
          “<Bold>Personal Data Breach</Bold>” means any “event” or “incident”
          (as per ITIL definition) which enables the accidental or unlawful
          destruction, loss, alteration, unauthorized disclosure of, or access
          to “Personal Data”.
        </Paragraph>
        <Paragraph>
          “<Bold>Processor</Bold>” means the entity which proceeds with
          authorized “Personal Data Processing Activities” (under this DPA and
          the “Agreement”) on behalf of the “Controller”.
        </Paragraph>
        <Paragraph>
          “<Bold>Service Catalog</Bold>” means the set of Services rendered by
          FLAVORWIKI that requires “Personal Data Processing Activities”.
        </Paragraph>
        <Paragraph>
          “<Bold>Sub-processor</Bold>” means any “Processor” engaged by any of
          the “Parties” which performs complimentary “Personal Data Processing
          Activities” within the scope of the “Services”.
        </Paragraph>
      </MainText>
      <Footer>
        <Paragraph>
          The Privacy Policy was last updated on November 25, 2019 and are
          effective immediately.
        </Paragraph>
        <Paragraph>
          Should You have questions or comments regarding the Policy Policy or
          suggestions regarding it's improvement, please contact our support
          team at{' '}
          <Link
            rel='noopener'
            href='mailto:support@flavorwiki.com'
            target='blank'
          >
            support@flavorwiki.com
          </Link>{' '}
          or by phone at +1 (910) 722 1560 of +41 79 137 2228.
        </Paragraph>
        <Paragraph>
          All rights on the Privacy Policy belong to it's author. Any
          reproduction, without prior license, is strictly forbidden.
        </Paragraph>
        <Paragraph>
          <Bold>Thank You for visiting FlavorWiki.</Bold>
        </Paragraph>
      </Footer>
    </Container>
  )
}

export default Body
