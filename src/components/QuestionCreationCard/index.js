import React, { memo } from 'react'
import Header from '../../containers/QuestionCreationCardHeader'
import SettingsSection from './QuestionCreationCardSettings'
import Body from '../../containers/QuestionCreationCardBody'
import { Container, HeaderContainer } from './styles'

const HIDDEN_QUESTIONS = new Set(['profile'])

const QuestionCreationCard = ({
  questionIndex,
  questionIndexInSection,
  question,
  mandatory,
  unsetMandatory,
  unsetDisplaySurveyNameOnAll,
  displaySurveyNameOnAll,
  unsetPicture,
  allToMandatory,
  allToPicture,
  compulsorySurvey,
  reduceRewardInTasting,
  showInPreferedLanguage,
  addDelayToSelectNextProductAndNextQuestion
}) => {
  if (HIDDEN_QUESTIONS.has(question.type)) {
    return null
  }
  return (
    <Container className='question-creation-card'>
      <HeaderContainer>
        <Header
          questionIndex={questionIndex}
          questionIndexInSection={questionIndexInSection}
          question={question}
          mandatory={mandatory}
          unsetMandatory={unsetMandatory}
          unsetPicture={unsetPicture}
          allToMandatory={allToMandatory}
          unsetDisplaySurveyNameOnAll={unsetDisplaySurveyNameOnAll}
          displaySurveyNameOnAll={displaySurveyNameOnAll}
          allToPicture={allToPicture}
          hasDemographic={question.hasDemographic}
        />
        <SettingsSection
          questionIndex={questionIndex}
          question={question}
          mandatory={mandatory}
          hasDemographic={question.hasDemographic}
          demographicField={question.demographicField ? question.demographicField : ''}
        />
      </HeaderContainer>
      <Body
        mandatory={mandatory}
        questionIndex={questionIndex}
        question={question}
        compulsorySurvey={compulsorySurvey}
        reduceRewardInTasting={reduceRewardInTasting}
        showInPreferedLanguage={showInPreferedLanguage}
        addDelayToSelectNextProductAndNextQuestion={addDelayToSelectNextProductAndNextQuestion}
      />
    </Container>
  )
}

export default memo(QuestionCreationCard)
