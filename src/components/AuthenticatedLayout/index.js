import React from 'react'
import Navbar from '../Navbar'
import Footer from '../Footer'
import PageContainer from '../PageContainer'

import { Page, Content } from './styles'
const { REACT_APP_THEME } = process.env
let footerFlag = false
if (REACT_APP_THEME === 'chrHansen' && REACT_APP_THEME !== undefined)
  footerFlag = true
const AuthenticatedLayout = ({
  children,
  withFooter,
  getEntryName,
  handleNavigate,
  navigationEntries,
  mergeNavbarToContent,
  setShowHamburger,
  showHamburger,
  user,
  showhelp=false
}) => (
  <Page>
    <Navbar
      user={user}
      showhelp={showhelp}
      entriesData={navigationEntries}
      getEntryName={getEntryName}
      onClickEntry={handleNavigate}
      mergeNavbarToContent={mergeNavbarToContent}
      setShowHamburger={setShowHamburger}
      showHamburger={showHamburger}
    />
    <Content>{children}</Content>
    <PageContainer>{footerFlag && <Footer />}</PageContainer>
  </Page>
)

export default AuthenticatedLayout
