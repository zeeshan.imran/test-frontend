import styled from 'styled-components'
import TextComponent from '../Text'
import Button from '../Button'
import { family } from '../../utils/Fonts'
import colors from '../../utils/Colors'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const BaseText = styled(TextComponent)`
  font-family: ${family.primaryLight};
  color: ${colors.SLATE_GREY};
  text-align: center;
`

export const Text = styled(BaseText)`
  -webkit-font-smoothing: antialiased;
  font-family: ${family.primaryLight};
  font-size: 1.8rem;
  color: ${colors.BLACK};
  opacity: 0.7;
  text-align: center;
`

export const StyledButton = styled(Button)`
  margin-top: 5rem;
  width: ${({ desktop }) => (desktop ? '30%' : '100%')};
`
