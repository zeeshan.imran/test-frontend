import styled from 'styled-components'
import { DEFAULT_COMPONENTS_MARGIN } from '../../utils/Metrics'

export const CardContainer = styled.div`
  margin-bottom: ${DEFAULT_COMPONENTS_MARGIN}rem;
`
export const Container = styled.div`
  display: flex;
  margin-bottom: 1.5rem;
  padding: 0 3rem;
`
