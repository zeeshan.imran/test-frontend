import React from 'react'
import { mount } from 'enzyme'
import OngoingSurveysGrid from '.';
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import UserSurveyCard from '../../containers/UserSurveyCard'

jest.mock('../../containers/UserSurveyCard', () => () => null)

const mockSurveyCreation = {
  surveyId: 'survey-1',
  products: [
    {
      id: 'product-1',
      name: 'product',
      brand: 'brand' 
    }
  ],
  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?'
    }
  ],
  mandatoryQuestions: [],
  uniqueQuestionsToCreate: []
}
describe('OngoingSurveysGrid', () => {

    let testRender;
    let surveys;
    let client

    beforeEach(() => {
        surveys = [{
            id: 'm59arWq35RGmeqDm7VRCZf',
            title: 'Survey Title 1',
            expirationTime: '2022-01-01T11:00:00',
            picture:
                'https://cdn-images-1.medium.com/max/1200/1*0nhNvVPD07JZeizhkqF8Mw.png',
            createdBy: {
                firstname: 'Marc',
                lastname: 'Muller'
            }
            }
        ]
        client = createApolloMockClient()
        client.cache.writeQuery({
            query: SURVEY_CREATION,
            data: {
                surveyCreation: mockSurveyCreation
            }
        })
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render OngoingSurveysGrid UserSurveyCard', async () => {
        window.matchMedia.setConfig({
            type: 'screen',
            width: 480,
            height: 640
        })
         
        testRender = mount(
            <ApolloProvider client={client}>
                <OngoingSurveysGrid surveys={surveys} />
            </ApolloProvider>
        )
        expect(testRender.find(UserSurveyCard).at(0)).toHaveLength(1)
    })

    test('should render OngoingSurveysGrid', async () => {

        const history = createBrowserHistory()
        testRender = mount(
            <ApolloProvider client={client}>
                <Router history={history}>
                    <OngoingSurveysGrid surveys={[]}/>
                </Router>
            </ApolloProvider>
        )
        expect(testRender.find(OngoingSurveysGrid)).toHaveLength(1)
    })
});