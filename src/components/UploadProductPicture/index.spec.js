import React from 'react'
import { mount,shallow } from 'enzyme'
import UploadProductPicture from '.'
import UploadButton from './UploadButton'
import { act } from 'react-dom/test-utils'
import { Upload } from 'antd'

describe('UploadProductPicture', () => {
  let testRender
  let loading
  let error
  let url
  let onFileUpload
  
  beforeEach(() => {
    loading = 'yes'
    error = {}
    url = 'http://localhost:3000'
    onFileUpload = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render UploadProductPicture', () => {
    testRender = shallow(
      <UploadProductPicture
        loading={loading}
        error={error}
        url={url}
        onFileUpload={onFileUpload}
      />
    )

    expect(testRender).toMatchSnapshot()
  })

  test('should render Upload before', () => {
    testRender = mount(
      <UploadProductPicture handleBeforeUpload={onFileUpload} />
    )

    testRender.find(UploadButton).simulate('change')
    expect(onFileUpload).not.toHaveBeenCalled()
  })

  test('should remove picture', () => {
    testRender = mount(
      <UploadProductPicture
        loading={loading}
        error={null}
        url={url}
        onFileUpload={onFileUpload}
      />
    )

    act(() => {
      testRender.find(Upload).prop('onRemove')()
    })
    expect(onFileUpload).toHaveBeenCalled()
  })

  test('should handle before upload', () => {
    testRender = mount(
      <UploadProductPicture
        loading={loading}
        error={null}
        url={url}
        onFileUpload={onFileUpload}
      />
    )

    act(() => {
      testRender.find(UploadButton).prop('handleBeforeUpload')()
    })
    expect(onFileUpload).toHaveBeenCalled()
  })
})
