import styled from 'styled-components'
import { Icon as AntIcon } from 'antd'
import colors from '../../utils/Colors'

export const Card = styled.div`
  cursor: ${({ lockedCursor, withoutBorder }) => (!withoutBorder ? lockedCursor? 'not-allowed' : 'pointer' : 'default')};
  position: relative;
  height: ${({ mobile }) => (mobile ? '15rem' : '20rem')};
  width: ${({ mobile }) => (mobile ? '15rem' : '20rem')};
  background-color: ${colors.WHITE};
  border-radius: 1rem;
  box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.14);
  margin-bottom: 1.2rem;
  padding-left: ${({ desktop }) => (desktop ? '2.8rem' : '1rem')};
  padding-right: ${({ desktop }) => (desktop ? '2.8rem' : '1rem')};
  padding-top: ${({ desktop }) => (desktop ? '2.8rem' : '1rem')};
  padding-bottom: ${({ desktop }) => (desktop ? '2.8rem' : '1rem')};
  border: solid 0.2rem transparent;
  :hover {
    border: ${({ mobile, withoutBorder }) =>
      !withoutBorder &&
      (mobile
        ? 'solid 0.2rem transparent'
        : `solid 0.2rem ${colors.LIGHT_OLIVE_GREEN}`)};
  }
  scroll-snap-align: center;
`

export const SelectionDot = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  margin-top: 1.3rem;
  margin-right: 1.3rem;
  width: 1.5rem;
  height: 1.5rem;
  border-radius: ${({ checkbox }) => (checkbox ? '25%' : '50%')};
  background-color: ${({ selected }) =>
    selected ? `${colors.LIGHT_OLIVE_GREEN}` : `${colors.WHITE}`};
  border: solid 0.1rem ${colors.BLACK};
`
export const CheckedIcon = styled(AntIcon)`
  color: ${colors.WHITE};
  left: 0;
  position: absolute;
  font-size: 1.2rem;
`
