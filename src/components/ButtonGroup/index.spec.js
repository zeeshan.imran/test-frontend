import React from 'react'
import { mount } from 'enzyme'
import { Button } from './styles'
import ButtonGroup from '.'

describe('ButtonGroup', () => {
  let testRender
  let handleChange
  let buttonLabels
  let defaultSelected

  beforeEach(() => {
    handleChange = jest.fn()
    buttonLabels = ['a']
    defaultSelected = ''
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ButtonGroup', async () => {
    testRender = mount(
      <ButtonGroup
        handleChange={handleChange}
        buttonLabels={buttonLabels}
        defaultSelected={defaultSelected}
      />
    )
    expect(testRender.find(Button)).toHaveLength(1)

    testRender = mount(
      <ButtonGroup
        handleChange={handleChange}
        buttonLabels={['buttonLabels', '2']}
        defaultSelected={defaultSelected}
      />
    )
    expect(testRender.find(Button)).toHaveLength(2)
  })
})
