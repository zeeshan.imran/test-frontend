import React from 'react'
import { mount } from 'enzyme'
import OperatorSurveyLogs from '.'

describe('Table', () => {
  let testRender
  let handleKeywordChange
  let handleTableChange

  beforeEach(() => {

    handleKeywordChange = jest.fn()
    handleTableChange = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Table', () => {
    testRender = mount(
      <OperatorSurveyLogs
        loading={false}
        keyword=''
        page={1}
        surveyLogs={[
          {
            action: 'create',
            by: {
              fullName: 'Operator Account',
              emailAddress: 'operator@flavorwiki.com'
            },
            collections: ['survey'],
            createdAt: '2020-08-20T06:07:41.560Z',
            type: 'create-survey',
            values: { name: 'survey-1' }
          },
          {
            action: 'create',
            by: {
              fullName: 'Operator Account',
              emailAddress: 'operator@flavorwiki.com'
            },
            collections: ['survey'],
            createdAt: '2020-08-20T06:07:41.560Z',
            type: 'create-survey',
            values: { name: 'survey-1' }
          },
        ]}
        countSurveyLogs={2}
        handleKeywordChange={handleKeywordChange}
        handleTableChange={handleTableChange}
      />
    )

    expect(testRender.find(OperatorSurveyLogs)).toHaveLength(1)
  })
})
