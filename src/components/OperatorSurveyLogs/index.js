import React from 'react'
import moment from 'moment'
import { Table } from 'antd'
import SearchBar from '../SearchBar'
import { Container, HeaderContainer, Title, SearchBarContainer, Image } from './styles'
import { useTranslation } from 'react-i18next'

import { DEFAULT_N_ELEMENTS_PER_PAGE } from '../../utils/Constants'

const OperatorSurveyLogs = ({
  loading,
  keyword,
  page,
  surveyLogs,
  countSurveyLogs,
  handleKeywordChange,
  handleTableChange
}) => {
  const { t } = useTranslation()

  const subColumns = [
    {
      title: t('page.surveyLogs.table.field'),
      dataIndex: 'field',
      width: '30%'
    },
    {
      title: t('page.surveyLogs.table.oldValue'),
      dataIndex: 'oldValue',
      width: '35%',
      render: (data, rowData, rowIndex) => {
        if (!data && data !== 0) {
          return ''
        }

        if (data.startswith && data.startswith('https://')) {
          return <Image src={data} alt='' />
        }

        return <span dangerouslySetInnerHTML={{ __html: data.toString() }} />
      }
    },
    {
      title: t('page.surveyLogs.table.newValue'),
      dataIndex: 'newValue',
      width: '35%',
      render: (data, rowData, rowIndex) => {
        if (!data && data !== 0) {
          return ''
        }

        if (data.startswith && data.startswith('https://')) {
          return <Image src={data} alt='' />
        }

        return <span dangerouslySetInnerHTML={{ __html: data.toString() }} />
      }
    }
  ]

  const columns = [
    {
      title: t('page.surveyLogs.table.type'),
      dataIndex: 'type',
      width: 150
    },
    {
      title: t('page.surveyLogs.table.values'),
      dataIndex: 'values',
      render: (data, rowData, rowIndex) => {

        return (
          <React.Fragment>
            {rowData.relatedProduct && (
              <div>
                {t('page.surveyLogs.table.productName')}:{' '}
                <strong>{rowData.relatedProduct.name}</strong>
              </div>
            )}

            {(rowData.type === 'create-question' || rowData.type === 'update-question') && (
              <div>
                {t('page.surveyLogs.table.questionPrompt')}:{' '}
                <strong>{(rowData.relatedQuestion || data).prompt || (rowData.relatedQuestion || data).typeOfQuestion}</strong>
              </div>
            )}

            {rowData.type === 'create-survey' && (
              <div>
                {t('page.surveyLogs.table.surveyName')}:{' '}
                <strong>{data.name}</strong>
              </div>
            )}

            {rowData.action === 'update' && Array.isArray(data) && (
              <Table
                size='small'
                columns={subColumns}
                dataSource={data}
                pagination={false}
                bordered
              />
            )}
          </React.Fragment>
        )
      }
    },
    {
      title: t('page.surveyLogs.table.by'),
      dataIndex: 'by',
      width: '20%',
      render: (data, rowData, rowIndex) => {
        return (
          <div>
            <div>{data && data.fullName}</div>
            <div>{data && data.emailAddress}</div>
          </div>
        )
      }
    },
    {
      title: t('page.surveyLogs.table.createdAt'),
      dataIndex: 'createdAt',
      width: '20%',
      sorter: () => false,
      render: (data, rowData, rowIndex) => {
        return moment(data).format('YYYY/MM/DD-HH:mm')
      }
    }
  ]

  return (
    <Container>
      <HeaderContainer>
        <Title>{t('page.surveyLogs.pageTitle')}</Title>
        <SearchBarContainer>
          <SearchBar
            withIcon
            value={keyword}
            placeholder={t('components.organizationsList.search')}
            handleChange={handleKeywordChange}
          />
        </SearchBarContainer>
      </HeaderContainer>
      <Table
        key='tableSurveyLogs'
        loading={loading}
        rowKey={record => record.id}
        columns={columns}
        dataSource={surveyLogs}
        pagination={
          countSurveyLogs > 10
            ? {
                pageSize: DEFAULT_N_ELEMENTS_PER_PAGE,
                total: countSurveyLogs,
                current: parseInt(page, 10) + 1
              }
            : false
        }
        onChange={(paginationConfig, filtersConfig, sortingConfig) => {
          handleTableChange(
            paginationConfig.current,
            sortingConfig.field,
            sortingConfig.order
          )
        }}
      />
    </Container>
  )
}

export default OperatorSurveyLogs
