import styled from 'styled-components'
import { family } from '../../utils/Fonts'
import colors from '../../utils/Colors'

export const Container = styled.div`
  background-color: ${colors.WHITE};
  padding: 2.4rem 3.2rem 3.2rem;
`

export const Title = styled.h2`
  color: rgba(0, 0, 0, 0.85);
  font-family: ${family.primaryBold};
  margin-bottom: 0;
`

export const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 6.4rem;
`

export const SearchBarContainer = styled.div`
  width: 27rem;
`

export const Image = styled.img`
  width: 100%;
`
