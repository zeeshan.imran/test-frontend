import React from 'react'
import { Row } from 'antd'
import Title from './Title'
import Picture from './Picture'
import ThankYouText from './ThankYouText'
import { useTranslation } from 'react-i18next'
import SurveySharing from '../../containers/SurveySharing'
import useResponsive from '../../utils/useResponsive'
import { SocialButtonsContainer, DasboardButton } from './styles'
// import { getShareLink } from '../../utils/shareUtils'
// import { SURVEY_AUTHORIZATION_TYPE } from '../../utils/Constants'

const SurveyLinkedSurveyComponent = ({
  sharingButtons,
  text,
  linkedSurveys,
  compulsorySurvey,
  history
}) => {
  const { mobile } = useResponsive()
  const { t } = useTranslation()

  const onClick = () => {
    history.replace(`/taster${compulsorySurvey ? '?showhelp=true' : ''}`)
  }

  // const availableLinkedSurveys = useMemo(() => {
  //   if (linkedSurveys.length > 0) {
  //     return linkedSurveys.filter(linkedSurvey => {
  //       return (
  //         linkedSurvey.authorizationType === SURVEY_AUTHORIZATION_TYPE.SELECTED
  //       )
  //     })
  //   }
  //   return []
  // }, [linkedSurveys])

  return (
    <React.Fragment>
      {compulsorySurvey ? (
        <React.Fragment>
          <Row>
            <Title />
            <Picture />
            <ThankYouText text={text} />
          </Row>
          <SocialButtonsContainer>
            <DasboardButton onClick={onClick}>
              {t('defaultValues.default.customButtons.continue')}
            </DasboardButton>
          </SocialButtonsContainer>
        </React.Fragment>
      ) : (
        <React.Fragment>
          <Row>
            <Title />
            <Picture />
            <ThankYouText text={text} />
          </Row>
          <SocialButtonsContainer>
            <SurveySharing isDesktop={!mobile} centerAlign />

            <DasboardButton onClick={onClick}>
              {t('defaultValues.default.customButtons.continue')}
            </DasboardButton>
          </SocialButtonsContainer>
        </React.Fragment>
      )}
    </React.Fragment>
  )
}

export default SurveyLinkedSurveyComponent

// {/* <SocialButtonsContainer>
            
//             {sharingButtons &&
//               availableLinkedSurveys.map((singleSurvey, index) => {
//                 return (
//                   <TwitterButton
//                     key={index}
//                     href={getShareLink(singleSurvey.uniqueName)}
//                     target='_blank'
//                   >
//                     {singleSurvey.title}
//                   </TwitterButton>
//                 )
//               })}
//           </SocialButtonsContainer> */}