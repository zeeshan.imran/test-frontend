import React from 'react'
import { mount } from 'enzyme'
import Modal from '.'
import wait from '../../utils/testUtils/waait'

describe('Modal', () => {
  let testRender
  let visible
  let toggleModal
  let children

  beforeEach(() => {
    visible = true
    toggleModal = jest.fn()
    children = 'Are you sure want to delete ?'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Modal', async () => {
    testRender = mount(
      <Modal visible={visible} toggleModal={toggleModal} children={children} />
    )
    expect(testRender.find(Modal)).toHaveLength(1)
  })

  test('should render Modal oncancel', async () => {
    testRender = mount(
      <Modal visible toggleModal={toggleModal} children={children}>
        Modal content
      </Modal>
    )

    await wait(500)

    global.document
      .querySelector('.ant-modal-wrap')
      .click(new Event('click', { target: null }))

    expect(toggleModal).toHaveBeenCalled()
  })
})
