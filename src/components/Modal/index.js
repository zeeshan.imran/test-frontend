import React from 'react'
import PropTypes from 'prop-types'
import { StyledModal } from './styles'

const Modal = ({ visible, toggleModal, children, closable = false }) => (
  <StyledModal
    maskStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}
    visible={visible}
    closable={closable}
    footer={null}
    maskClosable
    onCancel={toggleModal}
    centered
  >
    {children}
  </StyledModal>
)

Modal.propTypes = {
  visible: PropTypes.bool,
  children: PropTypes.node,
  toggleModal: PropTypes.func
}

export default Modal
