import styled from 'styled-components'
import { Alert } from 'antd'

export const Container = styled.div`
  background-color: #f3f3f3;
  border: 1px solid #efefef;
`

export const Item = styled.div`
  background-color: #ffffff;
  border-bottom: 1px solid #efefef;
  padding: 1rem 1.5rem;
`

export const Label = styled.label`
  display: block;
  margin-bottom: 8px;
`

export const AntAlert = styled(Alert)`
  margin-bottom: 16px;
`