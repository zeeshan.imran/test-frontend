import React, { useState, useEffect } from 'react'
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo-hooks'
import { Modal } from 'antd'
import { useTranslation } from 'react-i18next'
import { sortableContainer, sortableElement } from 'react-sortable-hoc'
import './index.css'
import { Container, Item, Label, AntAlert } from './styles'
import getQueryParams from '../../utils/getQueryParams'

const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      mandatoryQuestions
      questions
    }
  }
`

export const UPDATE_SURVEY_CREATION_MANDATORY_QUESTIONS = gql`
  mutation updateSurveyCreationMandatoryQuestions(
    $mandatoryQuestions: mandatoryQuestions
  ) {
    updateSurveyCreationMandatoryQuestions(
      mandatoryQuestions: $mandatoryQuestions
    ) @client
  }
`

export const UPDATE_SURVEY_CREATION_QUESTIONS = gql`
  mutation updateSurveyCreationQuestions($questions: questions) {
    updateSurveyCreationQuestions(questions: $questions) @client
  }
`

const SortableItem = sortableElement(({ item }) => {
  const { t } = useTranslation()

  return (
    <Item>
      <strong>{t(`dropDownOptions.${item.type}`)}:</strong> {item.prompt}
    </Item>
  )
})

const SortableContainer = sortableContainer(({ children }) => {
  return <Container>{children}</Container>
})

const ModalTabQuestions = ({ visible = false, setVisible }) => {
  const { t } = useTranslation()
  const { section } = getQueryParams(window.location)
  const updateMandatoryQuestions = useMutation(
    UPDATE_SURVEY_CREATION_MANDATORY_QUESTIONS
  )
  const updateQuestions = useMutation(UPDATE_SURVEY_CREATION_QUESTIONS)
  const [items, setItems] = useState([])

  const {
    data: { surveyCreation: { questions, mandatoryQuestions } = {} }
  } = useQuery(SURVEY_CREATION)

  useEffect(() => {
    deriveData()
  }, [section, questions])

  const deriveData = () => {
    if (section && questions.length > 0) {
      const newItems = questions.filter(question => {
        return question.displayOn === section
      })
      setItems([...newItems])
    }
  }

  const onOk = async e => {
    const questionIndex = questions.findIndex(question => {
      return question.displayOn === section
    })

    const newQuestions = [...questions]
    newQuestions.splice(questionIndex, items.length, ...items)

    for (let i = 0; i < newQuestions.length; i++) {
      const nextQuestion = newQuestions[i + 1] ? newQuestions[i + 1].id : null

      if (
        newQuestions[i].order !== i &&
        newQuestions[i].nextQuestion !== nextQuestion
      ) {
        newQuestions[i].reorder = true
      }

      newQuestions[i].order = i
      newQuestions[i].nextQuestion = nextQuestion

      if (newQuestions[i].skipFlow) {
        newQuestions[i].skipFlow.rules = []
      }
    }

    await updateMandatoryQuestions({
      variables: {
        mandatoryQuestions: mandatoryQuestions.map(mandatoryQuestion => {
          if (mandatoryQuestion.skipFlow) {
            mandatoryQuestion.skipFlow.rules = []
          }

          return {
            ...mandatoryQuestion
          }
        })
      }
    })

    await updateQuestions({
      variables: {
        questions: newQuestions
      }
    })

    setVisible(false)
  }

  const onCancel = () => {
    setVisible(false)
    deriveData()
  }

  const onSortEnd = ({ oldIndex, newIndex }) => {
    const newItems = [...items]
    const wantToMoveItem = newItems.splice(oldIndex, 1)[0]
    newItems.splice(newIndex, 0, wantToMoveItem)

    setItems([...newItems])
  }

  return (
    <Modal
      title={t('reorderQuestion.modalTitle')}
      visible={visible}
      onOk={onOk}
      onCancel={onCancel}
    >
      <AntAlert type='warning' message={t('reorderQuestion.warning')} />
      <Label>{t('reorderQuestion.modalLabel')}</Label>
      <SortableContainer helperClass='sortableHelper' onSortEnd={onSortEnd}>
        {items.map((item, index) => (
          <SortableItem key={`item-${index}`} index={index} item={item} />
        ))}
      </SortableContainer>
    </Modal>
  )
}

export default ModalTabQuestions
