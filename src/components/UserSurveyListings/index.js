import React from 'react'
import { Modal } from 'antd'
import Table from './Table'

const UserSurveyListingsComponent = ({
  t,
  modalVisibilty,
  handleCancel,
  userSurveyList,
  loading,
  numberOfDays,
  updateNumberOfDays,
  updateEnrollmentExpiry,
  expiryRequestLoader
}) => {
  return (
    <Modal
      width={Math.ceil((window.innerWidth * 85) / 100)}
      title={t('components.userSurveyListings.modalHeading')}
      visible={modalVisibilty}
      okButtonProps={{ style: { display: 'none' } }}
      onCancel={handleCancel}
    >
      <Table
        t={t}
        userSurveyListing={userSurveyList}
        loading={loading}
        numberOfDays={numberOfDays}
        updateNumberOfDays={updateNumberOfDays}
        updateEnrollmentExpiry={updateEnrollmentExpiry}
        expiryRequestLoader={expiryRequestLoader}
      />
    </Modal>
  )
}

export default UserSurveyListingsComponent
