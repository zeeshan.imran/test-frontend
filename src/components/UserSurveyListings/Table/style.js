import styled from 'styled-components'
import { family } from '../../../utils/Fonts'
import colors from '../../../utils/Colors'

export const ColumnContentAligner = styled.div`
  display: inline-flex;
  align-items: center;
`

export const Action = styled.span`
  cursor: pointer;
  color: ${colors.PRODUCT_TABLE_COLOR};
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  line-height: 1;
`
