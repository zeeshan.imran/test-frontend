import React, { useRef, useEffect, useState } from 'react'
import { Table as AntTable, Popconfirm, Row, Form, InputNumber } from 'antd'
import { Formik } from 'formik'
import { type } from 'ramda'
import { ColumnContentAligner, Action } from './style'
import validationsSchema from './validation'
import { Columns } from './columns'

const Table = ({
  t,
  loading,
  userSurveyListing,
  numberOfDays,
  updateNumberOfDays,
  updateEnrollmentExpiry,
  expiryRequestLoader
}) => {
  const form = useRef()
  const [isPopupOpen, setIsPopupOpen] = useState(false)
  useEffect(() => {
    if (form.current) {
      form.current.getFormikActions().validateForm()
    }
  })

  const numberCheck = value =>
    Number(value) && Number(value) > 0 ? false : true

  const tabelColumns = Columns(t)

  const confirmModalContent = (
    <React.Fragment>
      <Row>
        {t(
          'components.userSurveyListings.tableSurveyListing.popupModal.confirmText'
        )}
      </Row>
      {isPopupOpen && (
        <Formik
          enableReinitialize
          validateOnChange
          validateOnMount={true}
          ref={form}
          initialValues={{
            numberOfDays: numberOfDays
          }}
          validationSchema={validationsSchema}
          render={({ errors, setFieldValue, values }) => (
            <React.Fragment>
              <Form.Item
                help={
                  errors &&
                  errors.numberOfDays &&
                  type(errors.numberOfDays) === 'String'
                    ? errors.numberOfDays
                    : null
                }
                validateStatus={
                  errors &&
                  errors.numberOfDays &&
                  type(errors.numberOfDays) === 'String'
                    ? 'error'
                    : 'success'
                }
                style={{ width: '50%', marginTop: 20 }}
              >
                <InputNumber
                  min={0}
                  required
                  name='numberOfDays'
                  size='small'
                  placeholder={t(
                    'components.userSurveyListings.tableSurveyListing.popupModal.daysPlaceholder'
                  )}
                  value={values.numberOfDays}
                  onChange={value => {
                    updateNumberOfDays(value)
                    setFieldValue('numberOfDays', value)
                  }}
                />
              </Form.Item>
            </React.Fragment>
          )}
        />
      )}
    </React.Fragment>
  )

  const tableColumnAction = [
    {
      title: t(
        'components.userSurveyListings.tableSurveyListing.header.actions'
      ),
      dataIndex: '',
      render: (_, rowData) =>
        !['rejected', 'finished'].includes(rowData.state) && (
          <ColumnContentAligner>
            <Popconfirm
              placement='topRight'
              title={confirmModalContent}
              okText={t(
                'components.userSurveyListings.tableSurveyListing.popupModal.updateButton'
              )}
              okButtonProps={{
                disabled: numberCheck(numberOfDays),
                loading: expiryRequestLoader
              }}
              onConfirm={() =>
                updateEnrollmentExpiry(rowData.id, Number(numberOfDays))
              }
              cancelText={t(
                'components.userSurveyListings.tableSurveyListing.popupModal.cancelButton'
              )}
              visible={rowData.id === isPopupOpen}
              onVisibleChange={visible => {
                if (!visible) {
                  setIsPopupOpen(false)
                }
              }}
            >
              <Action
                onClick={() => {
                  updateNumberOfDays(0)
                  setIsPopupOpen(rowData.id)
                }}
              >
                {t(
                  'components.userSurveyListings.tableSurveyListing.expiryButton'
                )}
              </Action>
            </Popconfirm>
          </ColumnContentAligner>
        )
    }
  ]

  const columns = [...tabelColumns, ...tableColumnAction]

  return (
    <AntTable
      loading={loading}
      key='userSurveyListingTable'
      rowKey={record => record.id}
      columns={columns}
      dataSource={userSurveyListing}
    />
  )
}

export default Table
