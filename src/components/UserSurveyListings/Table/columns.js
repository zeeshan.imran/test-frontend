import React from 'react'
import moment from 'moment'
import { Tag } from 'antd'

const dateFormat = 'YYYY/MM/DD-HH:mm'

export const Columns = t => [
  {
    title: t(
      'components.userSurveyListings.tableSurveyListing.header.surveyName'
    ),
    dataIndex: 'survey',
    key: 'survey',
    align: 'left',
    render: survey => survey.name
  },
  {
    title: t(
      'components.userSurveyListings.tableSurveyListing.header.surveyState'
    ),
    dataIndex: 'state',
    key: 'state',
    render: state => <Tag> {state} </Tag>
  },
  {
    title: t(
      'components.userSurveyListings.tableSurveyListing.header.startedAt'
    ),
    dataIndex: 'createdAt',
    key: 'createdAt',
    render: createdAt => moment(createdAt).format(dateFormat)
  },
  {
    title: t(
      'components.userSurveyListings.tableSurveyListing.header.expiredAt'
    ),
    dataIndex: 'expiredAt',
    key: 'expiredAt',
    render: expiredAt => moment(expiredAt).format(dateFormat)
  }
]
