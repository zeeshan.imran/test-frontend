import * as Yup from 'yup'
import i18n from '../../../utils/internationalization/i18n'

const emptyStringToNull = (value, originalValue) => {
  if (typeof originalValue === 'string' && originalValue === '') {
    return null
  }
  return value
}

export default Yup.object().shape({
  numberOfDays: Yup.number()
    .typeError(i18n.t('components.userSurveyListings.validations.number'))
    .test(
      'not-empty',
      i18n.t('components.userSurveyListings.validations.withoutSpace'),
      value => /^(?! ).*/.test(value)
    )
    .test(
      'not-empty',
      i18n.t('components.userSurveyListings.validations.required'),
      value => !/^[ ]+$/.test(value)
    )
    .test(
      'is-positive',
      i18n.t('components.userSurveyListings.validations.positiveValue'),
      value => value && value !== '' ? parseInt(value, 10) > 0 : true
    )
    .transform(emptyStringToNull)
    .nullable()
    .required(i18n.t('components.userSurveyListings.validations.required'))
})
