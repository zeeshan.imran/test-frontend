import styled from 'styled-components'
import { Icon } from 'antd'
import colors from '../../utils/Colors'
import { COMPONENTS_DEFAULT_MARGIN } from '../../utils/Metrics'

export const ProfilePictureUpload = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: ${COMPONENTS_DEFAULT_MARGIN}rem;
  justify-content: ${({ desktop }) => (desktop ? 'left' : 'center')};
`

export const ProfilePicture = styled.div`
  width: 11rem;
  height: 11rem;
  border-radius: 50%;
  border: ${({ hasImageToDisplay }) =>
    hasImageToDisplay ? 'none' : `0.15rem dashed ${colors.LIGHT_OLIVE_GREEN}`};
  background: url(${props => props.src}) no-repeat center;
  background-size: cover;
  margin-right: 3rem;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const DefaultIcon = styled(Icon)`
  font-size: 5.5rem;
  color: ${colors.LIGHT_OLIVE_GREEN};
`
