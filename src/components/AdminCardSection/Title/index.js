import React from 'react'
import { Container, TitleText } from './styles'

const Title = ({ title }) => (
  <Container>
    <TitleText>{title}</TitleText>
  </Container>
)

export default Title
