import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import {
  isUserAuthenticatedAsTaster,
  isUserAuthenticatedAsOperator,
  isUserAuthenticatedAsPowerUser,
  isUserAuthenticatedAsAnalytics
} from '../../utils/userAuthentication'

const PrivateRoute = ({
  authenticateTaster,
  authenticateOperator,
  authenticateAnalytics,
  authenticatePowerUser,
  component: Component,
  ...otherProps
}) => {
  const authenticatedAsTaster =
    authenticateTaster && isUserAuthenticatedAsTaster()
  const authenticatedAsOperator =
    authenticateOperator && isUserAuthenticatedAsOperator()
  const authenticatedAsPowerUser =
    authenticatePowerUser && isUserAuthenticatedAsPowerUser()
  const authenticatedAsAnalytics =
    authenticateAnalytics && isUserAuthenticatedAsAnalytics()

  return (
    <Route
      {...otherProps}
      render={props => {
        if (
          authenticatedAsTaster ||
          authenticatedAsAnalytics ||
          authenticatedAsPowerUser ||
          authenticatedAsOperator
        ) {
          return <Component {...props} {...otherProps} />
        }
        return <Redirect to='/onboarding' />
      }}
    />
  )
}

export default PrivateRoute
