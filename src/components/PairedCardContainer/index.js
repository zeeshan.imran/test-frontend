import React from 'react'
import chunk from 'lodash.chunk'
import { PairContainer, Spacer } from './styles'

const PairedCardContainer = ({ children }) =>
  chunk(children, 2).map((pair, index) => (
    <PairContainer key={index}>
      {pair.map((card, pairIndex) => (
        <Spacer key={pairIndex}>{card}</Spacer>
      ))}
    </PairContainer>
  ))

export default PairedCardContainer
