import styled from 'styled-components'

export const PairContainer = styled.div`
  margin-right: 1.5rem;
  padding: 0 3rem;
`

export const Spacer = styled.div`
  margin-bottom: 1.5rem;
`
