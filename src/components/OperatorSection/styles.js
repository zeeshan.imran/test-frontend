import styled from 'styled-components'
import { Button } from 'antd'
import Text from '../Text'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  background-color: ${colors.WHITE};
  padding: 3rem 3.5rem;
  margin-bottom: 2.5rem;
`
export const ActionsContainer = styled.div`
  display: flex;
  align-items: center;
`

export const Title = styled(Text)`
  font-size: 2rem;
  font-family: ${family.primaryBold};
  color: rgba(0, 0, 0, 0.85);
`

export const AntButton = styled(Button)`
  margin-left: 1rem;
`
