import React, { useState, useMemo } from 'react'
import { Container, Title, ActionsContainer, AntButton } from './styles'
import { StyledCheckbox } from '../StyledCheckBox'
import { useTranslation } from 'react-i18next'
import ModalTabQuestions from '../ModalTabQuestions'

const OperatorSection = ({
  title,
  showMandatoryBox,
  showDisplayBox,
  allToMandatory,
  displaySurveyNameOnAll,
  settingAllToUnRequiredOnClick,
  settingAllToNotDisplayNameOnClick,
  showPictureBox,
  allToPicture,
  settingAllToPictureOnClick,
  section,
  questions = []
}) => {
  const { t } = useTranslation()
  const [modalVisible, setModalVisible] = useState(false)

  const tabQuestions = useMemo(() => {
    return questions.filter(question => {
      return question.displayOn === section
    })
  }, [section, questions])

  return (
    <Container>
      <Title>{title}</Title>

      <ActionsContainer>
        {showMandatoryBox && (
          <StyledCheckbox
            checked={allToMandatory}
            onChange={e => settingAllToUnRequiredOnClick(e.target.checked)}
          >
            {t('components.questionCreation.markAll')}
          </StyledCheckbox>
        )}

        {showDisplayBox && (
          <StyledCheckbox
            checked={displaySurveyNameOnAll}
            onChange={e => settingAllToNotDisplayNameOnClick(e.target.checked)}
          >
            {t('components.questionCreation.displayAll')}
          </StyledCheckbox>
        )}

        {showPictureBox && (
          <StyledCheckbox
            checked={allToPicture}
            onChange={e => settingAllToPictureOnClick(e.target.checked)}
          >
            {t('components.questionCreation.pictureOnAll')}
          </StyledCheckbox>
        )}

        {tabQuestions.length > 1 && (
          <AntButton type='primary' onClick={() => setModalVisible(true)}>
            Reorder
          </AntButton>
        )}
      </ActionsContainer>

      <ModalTabQuestions visible={modalVisible} setVisible={setModalVisible} />
    </Container>
  )
}

export default OperatorSection
