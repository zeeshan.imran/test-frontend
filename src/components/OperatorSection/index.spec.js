import React from 'react'
import { mount } from 'enzyme'
import { ApolloProvider } from 'react-apollo-hooks'
import OperatorSection from '.'
import { Title } from './styles'
import { createApolloMockClient } from '../../utils/createApolloMockClient'

describe('OperatorSection', () => {
  let testRender
  let title
  let client

  beforeEach(() => {
    title = 'Operator Title'
    client = createApolloMockClient()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render OperatorSection', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <OperatorSection title={title} />
      </ApolloProvider>
    )
    expect(testRender.find(OperatorSection)).toHaveLength(1)

    expect(testRender.find(Title).text()).toBe('Operator Title')
  })
})
