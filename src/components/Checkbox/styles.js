import styled from 'styled-components'
import { Checkbox } from 'antd'
import { family } from '../../utils/Fonts'
import colors from '../../utils/Colors'

export const Label = styled.span`
  max-width: 100%;
  font-family: ${({ bigger }) =>
    bigger ? family.primaryRegular : family.primaryLight};
  font-size: ${({ bigger }) => (bigger ? '1.6rem' : '1.4rem')};
  line-height: 1.57;
  letter-spacing: normal;
  color: ${({ bigger }) => (bigger ? colors.CHECKBOX_LABEL_COLOR : 'rgb(0, 0, 0)')};
  user-select: none;
  font-family: ${family.primaryRegular};
  opacity: ${({ bigger }) => (bigger ? '0.8' : '0.65')};
  :hover {
    opacity: 1;
  }
  min-width : 80px;
  word-break: break-all;
`

export const StyledCheckbox = styled(Checkbox)`
  max-width: 100%;

  .ant-checkbox-wrapper + span {
    max-width: 100%;
  }

  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner {
    border-color: ${colors.CHECKBOX_HOVER_COLOR};
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${colors.CHECKBOX_BACKGROUND_COLOR};
    border-color: ${colors.CHECKBOX_HOVER_COLOR};
  }
  .ant-checkbox-disabled.ant-checkbox-checked .ant-checkbox-inner::after{
    border-color: ${colors.CHECKBOX_HOVER_COLOR} !important;
  }
  .ant-checkbox-checked:after {
    border: 1px solid ${colors.CHECKBOX_DISABLE_COLOR};
  }

  .ant-checkbox-wrapper-checked ${Label} {
    opacity: 1;
  }

  ${({ bigger }) =>
    bigger &&
    `
    .ant-checkbox-inner {
      width: 2.9rem;
      height: 2.9rem;
      &:after {
        top: 49%;
        left: 31%;
        width: 7.714286px;
        height: 12.142857px;
      }
    }
    .ant-checkbox {
      width: 2.9rem;
      height: 2.9rem;
      .ant-checkbox-inner {
        border: ${`solid 0.2rem ${colors.PALE_LIGHT_GREY}`};
      }
      &.ant-checkbox-checked {
        .ant-checkbox-inner {
          border: none;
        }
      }
    }
  `};
`
