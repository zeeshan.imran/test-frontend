// index.stories.js
// Story for a Component, with a state and a setter
import React, { Component } from 'react'
import { storiesOf } from '@storybook/react'
import Checkbox from '.'

class StateWrapper extends Component {
  state = {
    checked: false
  }

  render () {
    return this.props.children({
      setChecked: checked => this.setState({ checked }),
      checked: this.state.checked
    })
  }
}

storiesOf('Checkbox', module).add('Story', () => (
  <StateWrapper>
    {({ setChecked, checked }) => {
      return (
        <Checkbox onChange={setChecked} checked={checked}>
          Checkbox
        </Checkbox>
      )
    }}
  </StateWrapper>
))
