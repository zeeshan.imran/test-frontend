import React from 'react'
import Responsive from 'react-responsive'
import {
  breakpoints,
  ONE_PIXEL_IN_REM
} from '../../utils/Metrics'


export const Desktop = props => (
  <Responsive {...props} query={`(min-width: ${breakpoints.LG}px)`} />
)

export const NonDesktop = props => (
  <Responsive
    {...props}
    query={`(max-width: ${breakpoints.LG - ONE_PIXEL_IN_REM}px)`}
  />
)

export const Tablet = props => (
  <Responsive
    {...props}
    query={`(min-width: ${breakpoints.MD}px) and (max-width: ${breakpoints.LG -
      ONE_PIXEL_IN_REM}px)`}
  />
)

export const Mobile = props => (
  <Responsive
    {...props}
    query={`(max-width: ${breakpoints.MD - ONE_PIXEL_IN_REM}px)`}
    style={{ padding: 0 }}
  />
)

export const Default = props => (
  <Responsive {...props} query={`(min-width: ${breakpoints.MD}px)`} />
)
