import React from 'react'
import { mount } from 'enzyme'
import CategoryCard from '.'
import PrivateRoute from '../PrivateRoute'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import { Container, Image, Label } from './styles'

describe('CategoryCard', () => {
  let testRender
  let name
  let picture
  let onClick
  let mobile

  window.localStorage = {
    getItem: () => '{}'
  }

  beforeEach(() => {
    name = 'Category 1'
    picture = ''
    onClick = jest.fn()
    mobile = false
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render CategoryCard', async () => {
    const history = createBrowserHistory()

    testRender = mount(
      <Router history={history}>
        <CategoryCard
          name={name}
          picture={picture}
          onClick={onClick}
          mobile={mobile}
        />
      </Router>
    )
    expect(testRender.find(CategoryCard)).toHaveLength(1)

    expect(testRender.find(Label).text()).toBe('Category 1')

    testRender.find(Container).simulate('click')
    
    expect(onClick).toHaveBeenCalled()
  })
})
