import React from 'react'
import PropTypes from 'prop-types'
import { Container, Image, Label } from './styles'

const CategoryCard = ({ name, picture, onClick, mobile }) => (
  <Container mobile={mobile} onClick={onClick}>
    <Image src={picture} />
    <Label>{name}</Label>
  </Container>
)

CategoryCard.propTypes = {
  name: PropTypes.string,
  picture: PropTypes.string,
  onClick: PropTypes.func
}

export default CategoryCard
