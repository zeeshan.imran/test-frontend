import React, { Component } from 'react'
import PropTypes from 'prop-types'
import FieldLabel from '../FieldLabel'
import { InputContainer, InputStyled } from './styles'

class InputArea extends Component {
  render () {
    const {
      label,
      required,
      size,
      fieldDecorator,
      tooltip,
      tooltipPlacement,
      ...rest
    } = this.props
    return (
      <FieldLabel 
        required={required} 
        label={label} 
        tooltip={tooltip}
        tooltipPlacement={tooltipPlacement}
      >
        <InputContainer>
          {fieldDecorator(
            <InputStyled size={size || 'large'} {...rest} />
          )}
        </InputContainer>
      </FieldLabel>
    )
  }
}

InputArea.defaultProps = {
  label: '',
  fieldDecorator: Component => Component
}

InputArea.propTypes = {
  label: PropTypes.string,
  required: PropTypes.bool,
  fieldDecorator: PropTypes.func
}

export default InputArea
