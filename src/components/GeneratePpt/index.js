import React, { useState, useCallback } from 'react'
import gql from 'graphql-tag'
import { useApolloClient } from 'react-apollo-hooks'
import { StyledButton } from './styles'

const { REACT_APP_THEME } = process.env
const GeneratePpt = ({ disabled, surveyId, jobGroupId }) => {
  const [loading, setLoading] = useState(false)
  const apollo = useApolloClient()
  const getPpt = useCallback(async () => {
    try {
      setLoading(true)
      const { data } = await apollo.query({
        query: SURVEYS_PPT,
        variables: {
          surveyId,
          jobGroupId,
          currentEnv: REACT_APP_THEME
        },
        fetchPolicy: 'network-only'
      })
      setLoading(false)

      const { surveysPpt } = data
      const url = `${process.env.REACT_APP_BACKEND_API_URL}/ppt/${surveyId}`
      if (surveysPpt === 'OK') {
        let a = document.createElement('a')
        a.href = url
        a.download = `${surveyId}.ppt`
        document.body.appendChild(a)
        a.click()
        document.body.removeChild(a)
      }
    } catch (error) {
      setLoading(false)
    }
  }, [apollo, jobGroupId, surveyId])
  return (
    <StyledButton
      disabled={disabled}
      type={'primary'}
      onClick={getPpt}
      loading={loading}
    >
      Generate PPT
    </StyledButton>
  )
}
const SURVEYS_PPT = gql`
  query surveysPpt($surveyId: ID!, $jobGroupId: ID!, $currentEnv: String!) {
    surveysPpt(
      surveyId: $surveyId
      jobGroupId: $jobGroupId
      currentEnv: $currentEnv
    )
  }
`

export default GeneratePpt
