import React from 'react'
import { mount } from 'enzyme'
import OperatorQrCodes from './index'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('OperatorQrCodes', () => {
  let testRender
  let loading
  let qrCodes
  let searchTerm
  let onSearch
  let onCreate
  let onCopy
  let onEdit
  let onDelete
  let onDownload

  beforeEach(() => {
    loading = false
    qrCodes = [
      {
        targetType: 'survey',
        survey: ''
      }
    ]
    searchTerm = ''
    onSearch = jest.fn()
    onCopy = jest.fn()
    onEdit = jest.fn()
    onDelete = jest.fn()
    onDownload = jest.fn()
    onCreate = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render OperatorQrCodes', async () => {
    testRender = mount(
      <OperatorQrCodes
        loading={loading}
        qrCodes={qrCodes}
        onCopy={onCopy}
        onEdit={onEdit}
        searchTerm={searchTerm}
        onSearch={onSearch}
        onCreate={onCreate}
        onDelete={onDelete}
        onDownload={onDownload}
      />
    )
    expect(testRender).toMatchSnapshot()
  })
})
