import React from 'react'
import { storiesOf } from '@storybook/react'
import BackNavigation from './'

storiesOf('BackNavigation', module).add(
  'Back Navigation with default title',
  () => <BackNavigation />
)
