import React from 'react'
import { Container, Label, Icon } from './styles'
import { withTranslation } from 'react-i18next'

const BackNavigation = ({ action, title, t }) => {
  title = title || t('components.backNavigation.back')
  return (
    <Container onClick={action}>
      <Icon type='left' />
      <Label>{title}</Label>
    </Container>
  )
}

BackNavigation.defaultProps = {
  action: () => {}
}
export default withTranslation()(BackNavigation)
