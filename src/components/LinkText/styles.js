import styled from 'styled-components'
import Text from '../Text'
import { family } from '../../utils/Fonts'

export const LinkStyledText = styled(Text)`
  cursor: pointer;
  color: rgb(24, 144, 255);
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  line-height: 1;
`
