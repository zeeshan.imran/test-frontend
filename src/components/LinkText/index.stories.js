import React from 'react'
import LinkText from './'
import { storiesOf } from '@storybook/react'

storiesOf('LinkText', module).add('LinkText', () => (
  <LinkText onClick={() => console.log('clicked')}>
    This is a link text. It's clickable
  </LinkText>
))
