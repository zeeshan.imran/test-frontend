import React from 'react'
import { storiesOf } from '@storybook/react'
import { Icon } from 'antd'
import Input from './'

storiesOf('Input', module)
  .add('Input with Placeholder(Default)', () => (
    <Input placeholder={'This is a placeholder'} />
  ))
  .add('Input with Placeholder and Label', () => (
    <Input placeholder={'This is a placeholder'} label={'Label Example'} />
  ))
  .add('Password Input with Placeholder', () => (
    <Input type={'password'} placeholder={'Password'} />
  ))
  .add('Input with Placeholder and Icon ', () => (
    <Input
      prefix={<Icon type='user' />}
      placeholder={'This is a placeholder'}
    />
  ))
  .add('Input with Field Decorator ', () => (
    <Input
      fieldDecorator={Cmp => console.log(Cmp) || Cmp}
      prefix={<Icon type='user' />}
      placeholder={'This is a placeholder'}
    />
  ))
