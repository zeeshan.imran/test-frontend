import React from 'react'
import { shallow } from 'enzyme'
import LabeledProgressBar from '.';

describe('LabeledProgressBar', () => {

    let testRender;
    let percentage;
    let label;


    beforeEach(() => {
        percentage = 100;
        label = "Answered"
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render LabeledProgressBar', async () => {
        testRender = shallow(
            <LabeledProgressBar
                percentage={percentage}
                label={label}
            />
        )
        expect(testRender).toMatchSnapshot()
    })
});