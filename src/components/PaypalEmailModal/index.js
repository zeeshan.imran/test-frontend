import React from 'react'
import { Form, Icon } from 'antd'
import Modal from '../Modal'
import Input from '../Input'
import Button from '../Button'
import PaypalLogo from '../SvgIcons/PaypalLogo'
import { Container, Title, Prompt, FormItem, ImageContainer } from './styles'
import { useTranslation } from 'react-i18next';

const handleSubmit = (e, validateFields, submit) => {
  e.preventDefault()
  validateFields((err, values) => {
    if (!err) {
      return submit(values.email)
    }
  })
}

const PaypalEmailModal = ({ visible, onClick, errors, form}) => {
  const { t } = useTranslation()
  const {
    getFieldDecorator,
    validateFields,
    getFieldError,
    isFieldTouched
  } = form
  const hasFieldErrors = !!getFieldError('email')
  const canSubmit = isFieldTouched('email') && !hasFieldErrors
  return (
    <Modal visible={visible}>
      <Form onSubmit={e => handleSubmit(e, validateFields, onClick)}>
        <Container>
          <ImageContainer>
            <PaypalLogo />
          </ImageContainer>

          <Title>{t('components.paypalEmailModal.title')}</Title>
          <Prompt> {t('components.paypalEmailModal.prompt')} </Prompt>
          <FormItem
            help={errors || getFieldError('email')}
            validateStatus={errors || hasFieldErrors ? 'error' : 'success'}
          >
            {getFieldDecorator('email', {
              rules: [
                {
                  type: 'email',
                  message: t('validation.email.email')
                },
                {
                  required: true,
                  message: t('validation.email.required')
                }
              ]
            })(
              <Input
                prefix={<Icon type='user' />}
                placeholder={t('placeholders.paypalEmail')}
              />
            )}
          </FormItem>
          <Button type={canSubmit ? 'primary' : 'disabled'} htmlType='submit'>
            Submit
          </Button>
        </Container>
      </Form>
    </Modal>
  )
}

export default Form.create()(PaypalEmailModal)
