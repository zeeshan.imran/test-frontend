import styled from 'styled-components'
import { Form } from 'antd'
import Text from '../Text'
import { family } from '../../utils/Fonts'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const Title = styled(Text)`
  font-size: 2rem;
  font-family: ${family.primaryRegular};
  color: rgba(0, 0, 0, 0.85);
  margin-bottom: 1.2rem;
  text-align: center;
`

export const Prompt = styled(Text)`
  font-size: 1.4rem;
  font-family: ${family.primaryRegular};
  color: rgba(0, 0, 0, 0.65);
  line-height: 2.2rem;
  margin-bottom: 2rem;
  text-align: center;
`

export const FormItem = styled(Form.Item)`
  width: 100%;
`

export const ImageContainer = styled.div`
  width: 80%;
  max-width: 27rem;
  margin-bottom: 3.5rem;
`
