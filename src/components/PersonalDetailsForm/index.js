import React, { Component } from 'react'
import { Form, Row, Col } from 'antd'
import UploadPictureWithButton from '../UploadPictureWithButton'
import Select from '../Select'
import DatePicker from '../DatePicker'
import Input from '../Input'
import {
  CustomForm,
  FormItem,
  Container,
  ButtonAligner,
  StyledButton
} from './styles'
import {
  getOptionValue,
  getOptionName
} from '../../utils/getters/OptionGetters'
import getBase64 from '../../utils/getBase64'
import { useTranslation } from 'react-i18next'

class PersonalDetailsForm extends Component {
  state = {
    selectedPicture: '',
    hasPictureToUpload: false
  }
  render () {
    const {
      form: {
        getFieldDecorator,
        isFieldsTouched,
        setFieldsValue,
        getFieldError
      },
      desktop,
      submitForm,
      firstname,
      lastname,
      email,
      birthday,
      imageUrl,
      language,
      gender,
      genders,
      languages
    } = this.props
    const { t } = useTranslation()
    const { selectedPicture, hasPictureToUpload } = this.state

    const fieldsChanged = hasPictureToUpload || isFieldsTouched()
    const hasFieldErrors =
      !!getFieldError('firstname') ||
      !!getFieldError('lastname') ||
      !!getFieldError('email') ||
      !!getFieldError('birthday')

    const shouldEnableSubmitButton = fieldsChanged && !hasFieldErrors

    return (
      <Container>
        <CustomForm onSubmit={e => submitForm(e, this.props.form)}>
          <Row type='flex' justify='center'>
            <Col xs={{ span: 24 }} lg={{ span: 16 }} xl={{ span: 12 }}>
              <Row>
                <Col xl={{ span: 24 }}>
                  <FormItem>
                    {getFieldDecorator('picture')(
                      <UploadPictureWithButton
                        desktop={desktop}
                        imageUrl={
                          selectedPicture !== '' ? selectedPicture : imageUrl
                        }
                        handleBeforeUpload={async file => {
                          const selectedPicture = await getBase64(file)
                          this.setState({
                            selectedPicture,
                            hasPictureToUpload: true
                          })
                          return setFieldsValue({ picture: file })
                        }}
                      />
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row gutter={24}>
                <Col xs={{ span: 24 }} xl={{ span: 12 }}>
                  <FormItem>
                    {getFieldDecorator('firstname', {
                      initialValue: firstname,
                      rules: [
                        {
                          required: true,
                          message: t('validation.firstname.required')
                        }
                      ]
                    })(
                      <Input
                        placeholder={t('placeholders.firstName')}
                        label={t('components.personalDetailsForm.firstName')}
                      />
                    )}
                  </FormItem>
                </Col>
                <Col xs={{ span: 24 }} xl={{ span: 12 }}>
                  <FormItem>
                    {getFieldDecorator('lastname', {
                      initialValue: lastname,
                      rules: [
                        {
                          required: true,
                          message: t('validation.lastname.required')
                        }
                      ]
                    })(
                      <Input
                        placeholder={t('placeholders.lastName')}
                        label={t('components.personalDetailsForm.lastName')}
                      />
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col xs={{ span: 24 }} xl={{ span: 24 }}>
                  <FormItem>
                    {getFieldDecorator('email', {
                      initialValue: email,
                      rules: [
                        {
                          type: 'email',
                          message: t('validation.email.email')
                        },
                        {
                          required: true,
                          message: t('validation.email.required')
                        }
                      ]
                    })(
                      <Input
                        placeholder={t('placeholders.email')}
                        label={t('components.personalDetailsForm.email')}
                      />
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row gutter={24}>
                <Col xs={{ span: 24 }} xl={{ span: 12 }}>
                  <FormItem>
                    {getFieldDecorator('birthday', {
                      initialValue: birthday,
                      rules: [
                        {
                          required: true,
                          message: t('validation.birthday.required')
                        }
                      ]
                    })(
                      <DatePicker
                        desktop={desktop}
                        type={'date'}
                        label={t('components.personalDetailsForm.birthday')}
                      />
                    )}
                  </FormItem>
                </Col>
                <Col xs={{ span: 24 }} xl={{ span: 12 }}>
                  <FormItem>
                    {getFieldDecorator('gender', {
                      initialValue: gender
                    })(
                      <Select
                        options={genders}
                        label={t('components.personalDetailsForm.genders')}
                        getOptionName={getOptionName}
                        getOptionValue={getOptionValue}
                        placeholder={t('placeholders.gender')}
                        desktop={desktop}
                      />
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row gutter={24}>
                <Col xs={{ span: 24 }} xl={{ span: 12 }}>
                  <FormItem>
                    {getFieldDecorator('language', {
                      initialValue: language
                    })(
                      <Select
                        options={languages}
                        getOptionName={getOptionName}
                        getOptionValue={getOptionValue}
                        label={t('components.personalDetailsForm.language')}
                        placeholder={t('placeholders.language')}
                        desktop={desktop}
                      />
                    )}
                  </FormItem>
                </Col>
              </Row>
              <Row type='flex' justify='center'>
                <Col xs={{ span: 24 }} xl={{ span: 12 }}>
                  <ButtonAligner desktop={desktop}>
                    <StyledButton
                      type={shouldEnableSubmitButton ? 'primary' : 'disabled'}
                      htmlType='submit'
                    >
                      {t('components.personalDetailsForm.submitButton')}
                    </StyledButton>
                  </ButtonAligner>
                </Col>
              </Row>
            </Col>
          </Row>
        </CustomForm>
      </Container>
    )
  }
}

export default Form.create()(PersonalDetailsForm)
