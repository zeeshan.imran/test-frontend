import React from 'react'
import { mount } from 'enzyme'
import SurveySharingComponent from '.'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'

describe('SurveySharingComponent', () => {
  let testRender
  let survey
  let surveyEnrollment
  let isDesktop
  let client

  beforeEach(() => {
    survey = { id: '1', uniqueName: 'unique-name' }
    surveyEnrollment = '1'
    isDesktop = true
    client = createApolloMockClient()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SurveySharingComponent', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <SurveySharingComponent
          survey={survey}
          surveyEnrollment={surveyEnrollment}
          isDesktop={isDesktop}
        />
      </ApolloProvider>
    )
    expect(testRender.find(SurveySharingComponent)).toHaveLength(1)
  })
})
