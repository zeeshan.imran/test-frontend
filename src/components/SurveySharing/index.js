import React from 'react'
import { useTranslation } from 'react-i18next'
import { Container, TitleText, CopyButton, SimpleText } from './styles'
import { CopyToClipboard as Copy } from 'react-copy-to-clipboard'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import defaults from '../../defaults'
import { getShareLink } from '../../utils/shareUtils'

const SurveySharingComponent = ({
  isDesktop,
  survey,
  surveyEnrollment,
  centerAlign,
  enrollmentReferralAmount
}) => {
  const { t } = useTranslation()

  const availableCountry = defaults.countries.find(
    c => c.code === survey.country
  )
  const rewardCurrency = availableCountry || {}
  const currencyPrefix =
    defaults.currencyPrefixes[rewardCurrency.currency] || ''
  const currency =
    defaults.currencySuffixes[rewardCurrency.currency] || currencyPrefix

  let shareSurvey = survey
  if (survey.screeners && survey.screeners.length > 0) {
    shareSurvey = survey.screeners[0]
  }
  const referralAmountFixed = enrollmentReferralAmount ? enrollmentReferralAmount : shareSurvey.referralAmount
  const amount = (currency || '') + referralAmountFixed


  let isPaymentEnabled =
    shareSurvey.isPaypalSelected || shareSurvey.isGiftCardSelected
  const formattedText = shareSurvey.customizeSharingMessage
    ? shareSurvey.customizeSharingMessage.replace('{{amount}}', amount)
    : ''
  return (
    <Container isDesktop={isDesktop} centerAlign={centerAlign}>
      {shareSurvey.showSharingLink && (
        <TitleText>
          {isPaymentEnabled && parseInt(referralAmountFixed, 10) ? (
            <SimpleText
              dangerouslySetInnerHTML={{
                __html: formattedText
              }}
            />
          ) : null}
          <Copy
            text={getShareLink(shareSurvey.uniqueName, surveyEnrollment)}
            onCopy={() => {
              displaySuccessMessage(t(`containers.surveySharing.copiedMessage`))
            }}
          >
            <CopyButton>
              {t(`containers.surveySharing.shareTextMiddle`)}
            </CopyButton>
          </Copy>
        </TitleText>
      )}
    </Container>
  )
}

export default SurveySharingComponent
