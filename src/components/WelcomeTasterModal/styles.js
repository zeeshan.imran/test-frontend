import styled from 'styled-components'
import { Modal, Button } from 'antd'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css"; 

export const AntModal = styled(Modal)`
  text-align: center;
  padding: 50px;
  top: 0 !important;
  
`

export const AntModalMain = styled.main`
  margin-top: 5rem;
  margin-bottom: 5rem;
`

export const Image = styled.img`
  max-width: 16rem;
  opacity: 0.5;
`

export const ContentAnchor = styled.a`
  font-weight: bold !important;
`

export const AntModalTitle = styled.h5`
  font-size: 20px;
  font-weight: 500;
  text-transform: capitalize;
  line-height: 1.2;
  margin-bottom: 8px;
`

export const AntModalContent = styled.div`
  white-space: pre-line;
`

export const AntButton = styled(Button)`
  padding: 0 40px;
`

export const AntCarousel = styled(Slider)`
  .slick-arrow {
    top: unset !important; 
    bottom: 0 !important;
  }
  .slick-arrow.slick-prev{
    left: calc(50% - 50px);
    width: auto;
    height: auto;
    z-index: 999;
  }
  .slick-arrow.slick-prev:before{
    content: 'prev' !important;
    color: #8aba5c;
  }
  .slick-arrow.slick-next{
    right: calc(50% - 50px);
    width: auto;
    height: auto;
  }
  .slick-arrow.slick-next:before{
    content: 'next' !important;
    color: #8aba5c;
  }
  .slick-dots li button {
    background: rgb(216, 216, 216) !important;
    height: 1rem !important;
    border-radius: 0.3rem !important;
  }
  .slick-dots li.slick-active button {
    background: #8aba5c !important;
  }
`
