import React from 'react'
import { useTranslation } from 'react-i18next'
import {
  AntModal,
  AntModalMain,
  AntModalTitle,
  AntModalContent,
  AntCarousel,
  Image
} from './styles'

import imageSrc from '../../assets/pictures/onboarding/problem-solving.png'

const WelcomeTasterModal = ({ visible, setVisible, showhelp = false }) => {
  const { t } = useTranslation()

  const welcomeTexts = [
    {
      title: t('components.welcomeTasterModal.step1.title'),
      content: t('components.welcomeTasterModal.step1.content')
    },
    {
      title: t('components.welcomeTasterModal.step2.title'),
      content: t('components.welcomeTasterModal.step2.content')
    },
    {
      title: t('components.welcomeTasterModal.step3.title'),
      content: t('components.welcomeTasterModal.step3.content'),
    },
    {
      title: t('components.welcomeTasterModal.step4.title'),
      content: t('components.welcomeTasterModal.step4.content'),
    },
    {
      title: t('components.welcomeTasterModal.step5.title'),
      content: t('components.welcomeTasterModal.step5.content'),
    }
  ]
  if (showhelp) {
    welcomeTexts.unshift({
      title: t('components.welcomeTasterModal.thankYouSlider.title'),
      content: t('components.welcomeTasterModal.thankYouSlider.content')
    })
  }
  return (
    <AntModal
      visible={!!visible}
      maskClosable={false}
      onCancel={() => {
        setVisible(false)
      }}
      footer={null}
    >
      <Image src={imageSrc} alt='Flavorwiki' />
      <AntCarousel
        infinite={false}
        arrows
        autoplay
        autoplaySpeed={10000}
        beforeChange={(current, next) => {
          if (next === welcomeTexts.length) {
            setVisible(false)
          }
        }}
      >
        {welcomeTexts.map((wt, index) => {
          return (
            <div key={index}>
              <AntModalMain>
                <AntModalTitle>{wt.title}</AntModalTitle>
                <AntModalContent
                  dangerouslySetInnerHTML={{
                    __html: wt.content
                  }}
                />
              </AntModalMain>
            </div>
          )
        })}
        <div>&nbsp;</div>
      </AntCarousel>
    </AntModal>
  )
}

export default WelcomeTasterModal
