import styled from 'styled-components'
import Button from '../Button'
import { DEFAULT_COMPONENTS_MARGIN } from '../../utils/Metrics'

export const ButtonContainer = styled.div`
  margin-top: ${DEFAULT_COMPONENTS_MARGIN}rem;
  text-align: center;
  display: flex;
  flex-direction: column;
`

export const StyledButton = styled(Button)`
  width: 100%;
`
