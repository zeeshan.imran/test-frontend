import React from 'react'
import { storiesOf } from '@storybook/react'
import RegisterLayout from './'

const STEPS = [
  {
    title: 'Title 1',
    description: `Description 1`,
    path: 'path-1'
  },
  {
    title: 'Title 2',
    description: `Description 2`,
    path: 'path-2'
  },
  {
    title: 'Title 3',
    description: `Description 3`,
    path: 'path-3'
  }
]

storiesOf('RegisterLayout', module).add('Welcome Form', () => (
  <RegisterLayout
    title='Register Layout Title'
    steps={STEPS}
    currentPath={STEPS[0].path}
    content={`Content`}
  />
))
