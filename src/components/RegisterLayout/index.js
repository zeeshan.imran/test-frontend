import React, { Component } from 'react'
import Composer from 'react-composer'
import { Query, Mutation } from 'react-apollo'
import { Row, Col } from 'antd'
import { take } from 'ramda'
import { Desktop } from '../Responsive'
import Navbar from './Navbar'
import BottomNavbar from '../BottomNavbar'
import history from '../../history'

import {
  getStepPath,
  getStepTitle,
  getStepDescription
} from '../../utils/getters/StepGetters'
import listToString from '../../utils/listToString'
import { Content, StepsText, Title, DescriptionText, Container } from './styles'
import SIGN_UP_FORM_QUERY from '../../queries/signUpForm'
import { CREATE_USER_MUTATION } from '../../mutations/createUser'
import { UPDATE_SIGN_UP_FORM_MUTATION } from '../../mutations/signUpForm'
import { CREATE_TESTER_INFO_MUTATION } from '../../mutations/createTesterInfo'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import { AUTHENTICATION_REDIRECT } from '../../utils/Constants'
import { withRouter } from 'react-router-dom'
import { RESET_SIGN_UP_FORM } from '../../mutations/resetSignUpForm'
import { UPDATE_TESTER_INFO_MUTATION } from '../../mutations/updateTesterInfo'

import { withTranslation } from 'react-i18next'


class RegisterLayout extends Component {
  renderPropsComponents = () => [
    ({ render }) => <Desktop children={render} />,
    ({ render }) => <Query query={SIGN_UP_FORM_QUERY} children={render} />,
    ({ render }) => (
      <Mutation mutation={CREATE_USER_MUTATION} children={render} />
    ),
    ({ render }) => (
      <Mutation mutation={UPDATE_SIGN_UP_FORM_MUTATION} children={render} />
    ),
    ({ render }) => (
      <Mutation mutation={CREATE_TESTER_INFO_MUTATION} children={render} />
    ),
    ({ render }) => (
      <Mutation mutation={RESET_SIGN_UP_FORM} children={render} />
    ),
    ({ render }) => (
      <Mutation mutation={UPDATE_TESTER_INFO_MUTATION} children={render} />
    )
  ]

  getCurrentStep () {
    const { steps, currentPath } = this.props
    return steps.findIndex(step => step.path === currentPath)
  }

  handleNavigation = async action => {
    const { steps } = this.props
    const currentStep = this.getCurrentStep()
    const increment = action === 'next' ? 1 : -1
    const nextPath = getStepPath(steps[currentStep + increment])
    return history.push(nextPath)
  }

  goToLogin = resetSignUpForm => {
    resetSignUpForm()
    this.props.history.push(AUTHENTICATION_REDIRECT)
  }

  handleUpdateTesterInfo = async (
    formData,
    updateTesterInfo,
    resetSignUpForm
  ) => {
    const {
      dietaryRestrictions,
      hasChildren,
      numberOfChildren,
      childrenAges,
      ethnicity,
      incomeRange,
      smokerKind,
      userId,
      productTypes
    } = formData

    const {t} = this.props;

    const normalizedChildrenAges =
      hasChildren === 'Yes' &&
      listToString(take(numberOfChildren, childrenAges))

    const createTesterInfoData = {
      ...(dietaryRestrictions.length > 0 && {
        dietaryPreferenceId: dietaryRestrictions
      }),
      ...(hasChildren === 'Yes' && { childrenAges: normalizedChildrenAges }),
      ...(ethnicity && { ethnicityId: ethnicity }),
      ...(incomeRange && { incomeRangeId: incomeRange }),
      ...(smokerKind && { smokerKindId: smokerKind }),
      ...(productTypes.length > 0 && {
        preferredProductCategoriesIds: productTypes
      }),
      id: userId
    }

    try {
      const {
        data: {
          updateTesterInfo: { ok, error: mutationError }
        }
      } = await updateTesterInfo({ variables: createTesterInfoData })

      if (!ok) {
        return displayErrorPopup(t('components.register.creatingError')+`: ${mutationError}.`)
      }
    } catch (error) {
      return displayErrorPopup( t('components.register.creatingErrorCheckAgain') )
    }

    return this.goToLogin(resetSignUpForm)
  }

  createUser = async (
    formData,
    createUser,
    updateSignUpForm,
    createTesterInfo
  ) => {
    const createUserData = {
      firstname: formData.firstname,
      lastname: formData.lastname,
      email: formData.email,
      password: formData.password,
      recaptchaResponseToken: formData.recaptchaResponseToken,
      ...(!!formData.language && { languageId: formData.language })
    }

    const { t } = this.props;

    const {
      data: {
        createUser: { ok, user }
      }
    } = await createUser({ variables: createUserData })
    if (!ok) {
      return displayErrorPopup(t('components.register.creatingError'))
    }

    await updateSignUpForm({ variables: { field: 'userId', value: user.id } })

    const createTesterInfoData = {
      birthday: formData.birthday,
      ...(formData.gender && { genderId: formData.gender }),
      userId: user.id
    }

    try {
      const {
        data: {
          createTesterInfo: { ok, error: mutationError }
        }
      } = await createTesterInfo({ variables: createTesterInfoData })

      if (!ok) {
        return displayErrorPopup(t('components.register.creatingError')+ `: ${mutationError}.`)
      }
    } catch (error) {
      return displayErrorPopup( t('components.register.creatingErrorCheckAgain') )
    }

    return this.handleNavigation('next')
  }

  getOkAction = (
    formData,
    createUser,
    updateSignUpForm,
    createTesterInfo,
    resetSignUpForm,
    updateTesterInfo
  ) => {
    const { steps } = this.props
    const currentStep = this.getCurrentStep()
    const totalSteps = steps.length
    const isLastStep = currentStep === totalSteps - 1
    if (currentStep === 1) {
      return this.createUser(
        formData,
        createUser,
        updateSignUpForm,
        createTesterInfo
      )
    }
    if (isLastStep) {
      return this.handleUpdateTesterInfo(
        formData,
        updateTesterInfo,
        resetSignUpForm
      )
    }
    return this.handleNavigation('next')
  }

  renderContent = desktop => {
    const { content, steps } = this.props
    const currentStep = this.getCurrentStep()
    const totalSteps = steps.length
    return (
      <Content desktop={desktop}>
        <Row>
          <Col
            xs={{ span: 0 }}
            lg={{ offset: 4, span: 16 }}
            xl={{ offset: 8, span: 8 }}
          >
            <StepsText>{`Step ${currentStep + 1} of ${totalSteps}`}</StepsText>
          </Col>
        </Row>
        <Row type='flex'>
          <Col
            xs={{ span: 24 }}
            md={{ offset: 2, span: 20 }}
            lg={{ offset: 4, span: 16 }}
            xl={{ offset: 8, span: 8 }}
          >
            <Title desktop={desktop}>{getStepTitle(steps[currentStep])}</Title>
          </Col>
        </Row>
        <Row type='flex'>
          <Col
            xs={{ span: 24 }}
            md={{ offset: 2, span: 20 }}
            lg={{ offset: 4, span: 16 }}
            xl={{ offset: 8, span: 8 }}
          >
            <DescriptionText desktop={desktop}>
              {getStepDescription(steps[currentStep])}
            </DescriptionText>
          </Col>
        </Row>
        <Row>
          <Col
            xs={{ span: 24 }}
            md={{ offset: 2, span: 20 }}
            lg={{ offset: 4, span: 16 }}
            xl={{ offset: 8, span: 8 }}
          >
            {content}
          </Col>
        </Row>
      </Content>
    )
  }

  shouldEnableOkButton = (
    isFirstStep,
    isLastStep,
    firstStepValid,
    secondStepValid,
    lastStepValid
  ) => {
    if (isFirstStep) {
      return !firstStepValid
    }
    if (isLastStep) {
      return !lastStepValid
    }
    return !secondStepValid
  }

  render () {
    const { steps, t } = this.props
    const currentStep = this.getCurrentStep()
    const totalSteps = steps.length
    const isFirstStep = currentStep === 0
    const isLastStep = currentStep === totalSteps - 1
    return (
      <Composer components={this.renderPropsComponents()}>
        {([
          desktop,
          {
            data: { signUpForm },
            client
          },
          createUser,
          updateSignUpForm,
          createTesterInfo,
          resetSignUpForm,
          updateTesterInfo
        ]) => {
          const { firstStepValid, secondStepValid, lastStepValid } = signUpForm
          return (
            <Container>
              <Navbar />
              {this.renderContent(desktop)}
              <BottomNavbar
                currentStep={desktop ? null : currentStep + 1}
                totalSteps={desktop ? null : totalSteps}
                okText={t('components.register.nextStep')}
                onOk={() =>
                  this.getOkAction(
                    signUpForm,
                    createUser,
                    updateSignUpForm,
                    createTesterInfo,
                    resetSignUpForm,
                    updateTesterInfo
                  )
                }
                okDisabled={this.shouldEnableOkButton(
                  isFirstStep,
                  isLastStep,
                  firstStepValid,
                  secondStepValid,
                  lastStepValid
                )}
                cancelText={t('components.register.back')}
                cancelDisabled={isFirstStep}
                onCancel={
                  isLastStep ? null : () => this.handleNavigation('previous')
                }
                extraAction={t('components.register.doLater') }
                onExtraAction={() => this.goToLogin(resetSignUpForm)}
                hasExtraAction={isLastStep}
              />
            </Container>
          )
        }}
      </Composer>
    )
  }
}

RegisterLayout.propTypes = {}

RegisterLayout.defaultProps = {}

export default withTranslation()(withRouter(RegisterLayout))
