import React from 'react'
import { Desktop } from '../../Responsive'
import NavbarButton from '../../BottomNavbar/NavbarButton'
import MobileNavbarButton from '../../BottomNavbar/MobileNavbarButton'
import { Container, Text } from './styles'

const BottomNavbar = ({
  loading,
  onOk,
  okText,
  okDisabled = false,
  onCancel,
  cancelText,
  cancelDisabled = false,
  totalSteps,
  currentStep,
  hasExtraAction,
  onExtraAction,
  extraAction
}) => (
  <Desktop>
    {desktop => {
      const shouldRenderSteps = totalSteps && currentStep
      const singleButton = !onCancel && !shouldRenderSteps
      const Button = desktop ? NavbarButton : MobileNavbarButton
      return (
        <Container singleButton={singleButton} desktop={desktop}>
          {shouldRenderSteps && !hasExtraAction && (
            <Text desktop={desktop}>{`${currentStep} of ${totalSteps}`}</Text>
          )}
          {onCancel && (
            <Button
              order={1}
              disabled={cancelDisabled || loading}
              onClick={onCancel}
              type={cancelDisabled || loading ? 'thirdDisabled' : 'third'}
            >
              {cancelText}
            </Button>
          )}
          {hasExtraAction && (
            <Button
              type='secondary'
              order={2}
              loading={loading}
              onClick={onExtraAction}
            >
              {extraAction}
            </Button>
          )}
          {onOk && (
            <Button
              order={4}
              disabled={okDisabled}
              onClick={onOk}
              loading={loading}
            >
              {okText}
            </Button>
          )}
        </Container>
      )
    }}
  </Desktop>
)

export default BottomNavbar
