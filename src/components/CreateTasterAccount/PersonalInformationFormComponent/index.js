import React from 'react'
import { Form } from 'antd'
import { useTranslation } from 'react-i18next'
import OnboardingForm from '../../OnboardingForm'
import Input from '../../Input'
import DatePicker from '../../DatePicker'
import Select from '../../Select'
import { GENDERS } from '../../../utils/Constants'
import useResponsive from '../../../utils/useResponsive/index'
import { FullWidthDiv, ItemLeft, ItemRight, ClearFloats } from './styles'
import moment from 'moment'

const baseLokalise = 'components.createTasterAccount.forms'

const PersonalInformationFormComponent = ({
  firstName,
  lastName,
  dateofbirth,
  gender,
  handleChange,
  handleBlur,
  dropdownChangeHandler,
  getDatePickerChangeHandler,
  handleSubmit,
  errors,
  touched,
  paypalEmailAddress,
  isProfileCompleted
}) => {
  const { t } = useTranslation()

  const getFormItemProps = name => {
    return {
      help: touched[name] && errors[name],
      validateStatus: touched[name] && errors[name] ? 'error' : 'success'
    }
  }

  const { desktop } = useResponsive()

  return (
    <OnboardingForm onSubmit={handleSubmit}>
      <FullWidthDiv>
        <ItemLeft desktop={desktop}>
          <Form.Item {...getFormItemProps('firstName')}>
            <Input
              required
              name='firstName'
              value={firstName}
              label={t(`${baseLokalise}.second.firstName.label`)}
              onBlur={handleBlur}
              onChange={handleChange}
              placeholder={t(`${baseLokalise}.second.firstName.placeholder`)}
            />
          </Form.Item>
        </ItemLeft>
        <ItemRight desktop={desktop}>
          <Form.Item {...getFormItemProps('lastName')}>
            <Input
              required
              name='lastName'
              value={lastName}
              label={t(`${baseLokalise}.second.lastName.label`)}
              onBlur={handleBlur}
              onChange={handleChange}
              placeholder={t(`${baseLokalise}.second.lastName.placeholder`)}
            />
          </Form.Item>
        </ItemRight>
        <ClearFloats />
      </FullWidthDiv>

      <FullWidthDiv>
        <ItemLeft desktop={desktop}>
          <Form.Item {...getFormItemProps('dateofbirth')}>
            <DatePicker
              required
              mode='date'
              name='dateofbirth'
              value={dateofbirth}
              defaultPickerValue={
                dateofbirth ? dateofbirth : moment().subtract(18, 'years')
              }
              label={t(`${baseLokalise}.second.dateofbirth.label`)}
              onChange={getDatePickerChangeHandler('dateofbirth')}
              showToday
              format={'DD-MMM-YYYY'}
              placeholder={t(`${baseLokalise}.second.dateofbirth.placeholder`)}
              disabledDate={currentDate =>
                currentDate > moment().subtract(18, 'years')
              }
            />
          </Form.Item>
        </ItemLeft>
        <ItemRight desktop={desktop}>
          <Form.Item>
            <Select
              showSearch={desktop ? true : false}
              required
              name='gender'
              value={gender}
              options={GENDERS}
              getOptionName={gender =>
                t(`${baseLokalise}.second.gender.${gender}`)
              }
              getOptionValue={gender => gender}
              onChange={dropdownChangeHandler('gender')}
              label={t(`${baseLokalise}.second.gender.label`)}
              placeholder={t(`${baseLokalise}.second.gender.placeholder`)}
            />
          </Form.Item>
        </ItemRight>
        <ClearFloats />
      </FullWidthDiv>

      <FullWidthDiv>
        <ItemLeft desktop={desktop}>
          <Form.Item {...getFormItemProps('paypalEmailAddress')}>
            <Input
              // required
              name='paypalEmailAddress'
              value={paypalEmailAddress}
              label={t(`${baseLokalise}.third.paypalEmailAddress.label`)}
              onBlur={handleBlur}
              onChange={handleChange}
              placeholder={t(
                `${baseLokalise}.third.paypalEmailAddress.placeholder`
              )}
            />
          </Form.Item>
        </ItemLeft>
        <ClearFloats />
      </FullWidthDiv>
    </OnboardingForm>
  )
}

export default PersonalInformationFormComponent
