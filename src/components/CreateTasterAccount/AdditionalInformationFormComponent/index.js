import React from 'react'
import { Form } from 'antd'
import { useTranslation } from 'react-i18next'
import OnboardingForm from '../../OnboardingForm'
import Input from '../../Input'
import Select from '../../Select'
import {
  // ETHNICITIES,
  CHILDREN_AGES,
  // INCOME_RANGES,
  MARKET_RESEARCH_PARTICIPATION
} from '../../../utils/Constants'
import { BINARY_RESPONSES } from '../../../utils/Data'
import useResponsive from '../../../utils/useResponsive/index'
import {
  FullWidthDiv,
  ItemLeft,
  ItemRight,
  ClearFloats,
  SelectedFloat
} from './styles'
 
const fieldsForChildrenAges = (
  fieldCount,
  fieldValues,
  onChange,
  desktop,
  t
) => {
  if (fieldCount) {
    if (fieldCount > 10) {
      fieldCount = 10
    }
    if (!fieldValues) {
      fieldValues = `{}`
    }
    const mainValues = JSON.parse(fieldValues)
    const fieldsToDisplay = []
    for (let counter = 1; counter <= fieldCount; counter++) {
      fieldsToDisplay.push(
        <SelectedFloat counter={counter} key={counter} desktop={desktop}>
          <Form.Item>
            <Select
              required
              value={
                mainValues[counter - 1]
                  ? parseInt(mainValues[counter - 1], 10)
                  : ''
              }
              label={t(
                `components.createTasterAccount.forms.third.childrenAges.label`,
                {
                  counter: counter
                }
              )}
              placeholder={t(
                `components.createTasterAccount.forms.third.childrenAges.placeholder`,
                {
                  counter: counter
                }
              )}
              onChange={onChange(counter)}
              options={CHILDREN_AGES}
              getOptionName={option => option.name}
              getOptionValue={option => option.code}
            />
          </Form.Item>
        </SelectedFloat>
      )
    }
    return fieldsToDisplay
  }
  return null
}

const AdditionalInformationFormComponent = ({
  completeForm,
  // ethnicity,
  // incomeRange,
  hasChildren,
  numberOfChildren,
  smokerKind,
  foodAllergies,
  marketResearchParticipation,
  paypalEmailAddress,
  childrenAges,
  handleChange,
  handleNumberOfChildrenChange,
  onChangeAges,
  handleBlur,
  dropdownChangeHandler,
  handleSubmit,
  errors,
  touched,
  removeNumberOfChildren
}) => {
  const { t } = useTranslation()
  // const getFormItemProps = name => {
  //   return {
  //     help: touched[name] && errors[name],
  //     validateStatus: touched[name] && errors[name] ? 'error' : 'success'
  //   }
  // }
  const shouldRenderChildrenAgesDropdown =
    hasChildren === 'Yes' && numberOfChildren > 0
  const { desktop } = useResponsive()

  return (
    <OnboardingForm onSubmit={handleSubmit}>
      <FullWidthDiv>
        {/* <ItemLeft desktop={desktop}>
          <Form.Item {...getFormItemProps('paypalEmailAddress')}>
            <Input
              // required
              name='paypalEmailAddress'
              value={paypalEmailAddress}
              label={t(
                `components.createTasterAccount.forms.third.paypalEmailAddress.label`
              )}
              onBlur={handleBlur}
              onChange={handleChange}
              placeholder={t(
                `components.createTasterAccount.forms.third.paypalEmailAddress.placeholder`
              )}
            />
          </Form.Item>
        </ItemLeft> */}
        <ItemLeft desktop={desktop}>
          <Form.Item>
            <Select
              required
              showSearch={!!desktop}
              name='smokerKind'
              value={smokerKind}
              label={t(
                `components.createTasterAccount.forms.third.smokerKind.label`
              )}
              options={BINARY_RESPONSES}
              getOptionName={eth =>
                t(`components.createTasterAccount.forms.${eth}`)
              }
              getOptionValue={eth => eth}
              onChange={dropdownChangeHandler('smokerKind')}
              placeholder={t(
                `components.createTasterAccount.forms.third.smokerKind.placeholder`
              )}
            />
          </Form.Item>
        </ItemLeft>
        <ItemRight desktop={desktop}>
          <Form.Item>
            <Select
              required
              showSearch={!!desktop}
              name='foodAllergies'
              value={foodAllergies}
              label={t(
                `components.createTasterAccount.forms.third.foodAllergies.label`
              )}
              options={BINARY_RESPONSES}
              getOptionName={eth =>
                t(`components.createTasterAccount.forms.${eth}`)
              }
              getOptionValue={eth => eth}
              onChange={dropdownChangeHandler('foodAllergies')}
              placeholder={t(
                `components.createTasterAccount.forms.third.foodAllergies.placeholder`
              )}
            />
          </Form.Item>
        </ItemRight>
        {/* <ItemRight desktop={desktop}>
          <Form.Item>
            <Select
              required
              showSearch={!!desktop}
              name='incomeRange'
              value={incomeRange}
              label={t(
                `components.createTasterAccount.forms.third.incomeRange.label`
              )}
              options={
                completeForm['country'] &&
                INCOME_RANGES[completeForm['country']]
                  ? INCOME_RANGES[completeForm['country']]
                  : INCOME_RANGES['others']
              }
              onChange={dropdownChangeHandler('incomeRange')}
              placeholder={t(
                `components.createTasterAccount.forms.third.incomeRange.placeholder`
              )}
            />
          </Form.Item>
        </ItemRight> */}
        <ClearFloats />
      </FullWidthDiv>
      <FullWidthDiv>
        <ItemLeft desktop={hasChildren === 'Yes' && desktop}>
          <Form.Item>
            <Select
              required
              showSearch={!!desktop}
              name='hasChildren'
              value={hasChildren}
              label={t(
                'components.questionCreation.templates.childrenStatus.prompt'
              )}
              options={BINARY_RESPONSES}
              getOptionName={eth =>
                t(`components.createTasterAccount.forms.${eth}`)
              }
              onChange={dropdownChangeHandler('hasChildren')}
              placeholder={t(
                'components.additionalInfo.childrenAtHomePlaceholder'
              )}
            />
          </Form.Item>
        </ItemLeft>
        <ItemRight desktop={desktop}>
          <Form.Item>
            {hasChildren === 'Yes' && (
              <Input
                type={`number`}
                min='1'
                max='15'
                name='numberOfChildren'
                onFocus={removeNumberOfChildren('numberOfChildren')}
                value={numberOfChildren}
                label={t(
                  `components.createTasterAccount.forms.third.numberOfChildren.label`
                )}
                onChange={handleNumberOfChildrenChange('numberOfChildren')}
                placeholder={t(
                  `components.createTasterAccount.forms.third.numberOfChildren.placeholder`
                )}
              />
            )}
          </Form.Item>
        </ItemRight>
      </FullWidthDiv>
      <Input type={`hidden`} value={childrenAges} name={`childrenAges`} />
      {shouldRenderChildrenAgesDropdown > 0 ? (
        <FullWidthDiv>
          {fieldsForChildrenAges(
            numberOfChildren,
            childrenAges,
            onChangeAges,
            desktop,
            t
          )}
          <ClearFloats />
        </FullWidthDiv>
      ) : null}
      <FullWidthDiv>
        <ItemLeft desktop={desktop}>
          <Form.Item>
            <Select
              required
              showSearch={!!desktop}
              name='marketResearchParticipation'
              value={marketResearchParticipation}
              label={t(
                `components.createTasterAccount.forms.third.marketResearchParticipation.label`
              )}
              options={MARKET_RESEARCH_PARTICIPATION}
              getOptionName={eth =>
                t(
                  `components.createTasterAccount.forms.third.marketResearchParticipation.${eth}`
                )
              }
              getOptionValue={eth => eth}
              onChange={dropdownChangeHandler('marketResearchParticipation')}
              placeholder={t(
                `components.createTasterAccount.forms.third.marketResearchParticipation.placeholder`
              )}
            />
          </Form.Item>
        </ItemLeft>
        <ClearFloats />
      </FullWidthDiv>
      {/* ethnicity removed for now*/}
      {/* <FullWidthDiv>
        <ItemLeft desktop={desktop}>
          <Form.Item>
            <Select
              required
              showSearch={!!desktop}
              name='ethnicity'
              value={ethnicity}
              label={t(
                `components.createTasterAccount.forms.third.ethnicity.label`
              )}
              options={ETHNICITIES}
              getOptionName={eth => eth.charAt(0).toUpperCase() + eth.slice(1)}
              onChange={dropdownChangeHandler('ethnicity')}
              placeholder={t(
                `components.createTasterAccount.forms.third.ethnicity.placeholder`
              )}
            />
          </Form.Item>
        </ItemLeft>
        <ClearFloats />
      </FullWidthDiv> */}
    </OnboardingForm>
  )
}

export default AdditionalInformationFormComponent
