import styled from 'styled-components'
import Button from '../../Button'
import { DEFAULT_COMPONENTS_MARGIN } from '../../../utils/Metrics'

export const ButtonContainer = styled.div`
  margin-top: ${DEFAULT_COMPONENTS_MARGIN}rem;
  text-align: center;
  display: flex;
  flex-direction: column;
`

export const StyledButton = styled(Button)`
  width: 100%;
`

export const FullWidthDiv = styled.div`
  width: 100%;
`

export const SelectedFloat = styled.div`
  ${({ counter, desktop }) =>
    counter % 2
      ? desktop
        ? 'width: 45%; float: left; margin-right: 10%;'
        : 'width: 100%;'
      : desktop
        ? 'width: 45%; float: left;'
        : 'width: 100%;'};
`

export const ItemRight = styled.div`
  ${({ desktop }) => (desktop ? 'width: 45%; float: left;' : 'width: 100%;')};
`
export const ItemLeft = styled(ItemRight)`
  ${({ desktop }) => (desktop ? 'margin-right: 10%;' : '')};
`
export const ClearFloats = styled.div`
  clear: both;
`
