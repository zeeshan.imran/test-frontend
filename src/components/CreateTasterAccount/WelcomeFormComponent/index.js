import React, { useEffect, useMemo } from 'react'
import { Row, Col, Form } from 'antd'
import { useTranslation } from 'react-i18next'
import OnboardingForm from '../../OnboardingForm'
import Input from '../../Input'
import Select from '../../Select'
import { ReCAPTCHAField } from './styles'
import useUpdateLocale from '../../../hooks/useUpdateSurveyLanguage'

import { options } from '../../../utils/internationalization/index'
import { getOptionValue } from '../../../utils/getters/OptionGetters'
import {
  DUMMY_COUNTRY_PHONE_CODE,
  COUNTRY_PHONE_CODES,
  CURRENT_STATES
} from '../../../utils/Constants'

const baseLokalise = 'components.createTasterAccount.forms'

const WelcomeFormComponent = ({
  emailAddress,
  password,
  confirmPassword,
  country,
  state,
  language,
  setIsHuman,
  handleChange,
  handleBlur,
  canSubmit,
  handleSubmit,
  errors,
  touched,
  loading
}) => {
  const { t } = useTranslation()
  const updateLocale = useUpdateLocale()
  const isDummyTaster = window.location.pathname === '/create-demo-account/taster/welcome'
  
  useEffect(() => {
    if(isDummyTaster) {
      const target = { name: 'country', value: DUMMY_COUNTRY_PHONE_CODE.code }
      handleChange({ target })
    }
  }, [isDummyTaster])

  const getFormItemProps = name => {
    return {
      help: touched[name] && errors[name],
      validateStatus: touched[name] && errors[name] ? 'error' : 'success'
    }
  }

  const isSingleStaging = useMemo(() => {
    const singleStagings = [
      'staging1.flavorwiki.com',
      'staging2.flavorwiki.com',
      'staging3.flavorwiki.com',
      'staging4.flavorwiki.com',
      'staging5.flavorwiki.com',
      'staging6.flavorwiki.com',
      'staging7.flavorwiki.com',
      'staging8.flavorwiki.com',
      'staging9.flavorwiki.com',
      'staging10.flavorwiki.com'
    ]

    return singleStagings.includes(window.location.host)
  }, [window.location])

  useEffect(() => {
    if(isSingleStaging) {
      setIsHuman(true)
    }
  }, [isSingleStaging])

  return (
    <OnboardingForm onSubmit={handleSubmit}>
      <Row gutter={[16, 16]}>
        <Col span={12}>
          <Form.Item {...getFormItemProps('emailAddress')}>
            <Input
              required
              name='emailAddress'
              value={emailAddress}
              label={t(`${baseLokalise}.first.email.label`)}
              onBlur={handleBlur}
              onChange={handleChange}
              placeholder={t(`${baseLokalise}.first.email.placeholder`)}
            />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item>
            <Select
              required
              name='language'
              label={t(`${baseLokalise}.second.langugage.label`)}
              value={language}
              onChange={value => {
                const target = { name: 'language', value }
                handleChange({ target })
                updateLocale(value)
              }}
              placeholder={t(`${baseLokalise}.second.langugage.placeholder`)}
              options={options}
              getOptionValue={getOptionValue}
              getOptionName={(option = {}) =>
                t(option.label) ? t(option.label) : option.label
              }
            />
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={[16, 16]}>
        <Col span={12}>
          <Form.Item>
            {isDummyTaster && (
              <Input
                name='country'
                label={t(`${baseLokalise}.second.country.label`)}
                value={DUMMY_COUNTRY_PHONE_CODE.name}
                disabledInput
              />
            )}

            {!isDummyTaster && (
              <Select
                required
                showSearch
                name='country'
                value={country}
                options={COUNTRY_PHONE_CODES}
                optionFilterProp='children'
                getOptionName={option => {
                  const translatedName = t(
                    `${baseLokalise}.second.country.${option.name}`
                  )
                  return translatedName ===
                    `${baseLokalise}.second.country.${option.name}`
                    ? option.name
                    : translatedName
                }}
                getOptionValue={option => option.code}
                onChange={value => {
                  const target = { name: 'country', value }
                  handleChange({ target })
                }}
                label={t(`${baseLokalise}.second.country.label`)}
                placeholder={t(`${baseLokalise}.second.country.placeholder`)}
              />
            )}
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item>
            <Select
              required
              showSearch
              name='state'
              value={state}
              options={
                CURRENT_STATES[country]
                  ? CURRENT_STATES[country]
                  : CURRENT_STATES['others']
              }
              getOptionName={option =>
                CURRENT_STATES[country]
                  ? option.name
                  : t(`${baseLokalise}.second.state.notAplicable`)
              }
              getOptionValue={option => option.name}
              onChange={value => {
                const target = { name: 'state', value }
                handleChange({ target })
              }}
              label={t(`${baseLokalise}.second.state.label`)}
              placeholder={t(`${baseLokalise}.second.state.placeholder`)}
            />
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={[16, 16]}>
        <Col span={12}>
          <Form.Item {...getFormItemProps('password')}>
            <Input
              required
              name='password'
              value={password}
              label={t(`${baseLokalise}.first.password.label`)}
              type={'password'}
              onBlur={handleBlur}
              onChange={handleChange}
              placeholder={t(`${baseLokalise}.first.password.placeholder`)}
            />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item {...getFormItemProps('confirmPassword')}>
            <Input
              required
              name='confirmPassword'
              value={confirmPassword}
              label={t(`${baseLokalise}.first.confirm.label`)}
              type={'password'}
              onBlur={handleBlur}
              onChange={handleChange}
              placeholder={t(`${baseLokalise}.first.confirm.placeholder`)}
            />
          </Form.Item>

          {!isSingleStaging && (
            <ReCAPTCHAField
              sitekey={process.env.REACT_APP_GOOGLE_RECAPTCHA_KEY}
              onChange={setIsHuman}
            />
          )}
        </Col>
      </Row>
    </OnboardingForm>
  )
}

export default WelcomeFormComponent
