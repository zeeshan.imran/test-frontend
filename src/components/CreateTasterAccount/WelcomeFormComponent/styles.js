import styled from 'styled-components'
import ReCAPTCHA from 'react-google-recaptcha'

export const ReCAPTCHAField = styled(ReCAPTCHA)`
  text-align: -webkit-right;
`
