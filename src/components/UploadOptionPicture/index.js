import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import TooltipWrapper from '../../components/TooltipWrapper'
import {
  Avatar,
  Icon,
  Upload,
  IconAligner,
  RemoveIconAligner,
  labelUploadStyle
} from './styles'
import FieldLabel from '../FieldLabel'

const UploadOptionsPicture = ({
  name,
  image,
  onAddImageUrl,
  uploadStyle = {},
  onRemoveImageUrl,
  removeButton = true,
  label = false
}) => {
  const { t } = useTranslation()
  const [loading, setLoading] = useState(false)
  const handleBeforeUpload = async file => {
    setLoading(true)
    await onAddImageUrl({ [name]: file })
    setLoading(false)
    return false
  }

  const handleRemove = async () => {
    await onAddImageUrl({ [name]: '' })
  }

  const content = (
    <React.Fragment>
      <Upload
        name='avatar'
        accept='.jpg,.jpeg,.png'
        showUploadList={false}
        listType='picture-card'
        className='avatar-uploader'
        onRemove={handleRemove}
        beforeUpload={handleBeforeUpload}
        customRequest={() => {}}
        uploadStyle={label ? labelUploadStyle : uploadStyle}
      >
        {image && !loading ? (
          <Avatar src={image} alt={'avatar'} />
        ) : (
          <IconAligner>
            <Icon type={loading ? 'loading' : 'plus'} />
          </IconAligner>
        )}
      </Upload>
      {image && removeButton && (
        <RemoveIconAligner
          uploadStyle={uploadStyle}
          isLabel={label ? true : false}
        >
          <TooltipWrapper
            helperText={t('tooltips.deletePicture')}
            placement='right'
          >
            <Icon
              type='delete'
              onClick={e => {
                onRemoveImageUrl()
                e.stopPropagation()
              }}
            />
          </TooltipWrapper>
        </RemoveIconAligner>
      )}
    </React.Fragment>
  )

  return label ? <FieldLabel label={label}>{content}</FieldLabel> : content
}

export default UploadOptionsPicture
