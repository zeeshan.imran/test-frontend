import React from 'react'
import { shallow } from 'enzyme'
import Label from '.'

describe('Label', () => {
  let testRender
  let label
  let required

  beforeEach(() => {
    label = 'Name'
    required = false
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Label', async () => {
    testRender = shallow(
      <Label label={label} required={required} />
    )
    expect(testRender).toMatchSnapshot()
  })
})
