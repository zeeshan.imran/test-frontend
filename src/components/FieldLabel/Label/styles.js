import styled from 'styled-components'
import { family } from '../../../utils/Fonts'

export const LabelText = styled.span`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  line-height: 1.57;
  letter-spacing: normal;
  color: rgba(0, 0, 0, 0.65);
  margin-bottom: 0.5rem;
  user-select: none;
`
