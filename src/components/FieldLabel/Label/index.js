import React from 'react'
import { LabelText } from './styles'

const Label = ({ required, label }) => (
  <LabelText>{`${required ? '* ' : ''}`}{label}</LabelText>
)

export default Label
