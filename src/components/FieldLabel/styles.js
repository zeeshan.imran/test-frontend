import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`

export const LabelWrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
`
