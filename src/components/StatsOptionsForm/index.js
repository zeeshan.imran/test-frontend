import React from 'react'
import FieldLabel from '../FieldLabel'
import Select from '../Select'
import { Content, Setting, Switcher, Divider } from './styles'
import { withTranslation } from 'react-i18next'
import ColorPicker from './ColorPicker'
import Grouping from './Grouping'
import {
  getAvailableChartSettings,
  getAvailableStatisticSettings
} from '../../utils/chartSettings'

const StatsOptionsForm = ({
  settings,
  setSettings,
  chartType,
  questionType,
  hasProducts,
  isStatisticsSettings = false,
  t
}) => {
  let availableSettings
  if (isStatisticsSettings) {
    availableSettings = getAvailableStatisticSettings(chartType)
  } else {
    availableSettings = getAvailableChartSettings({
      chartType,
      questionType,
      hasProducts
    })
  }

  return (
    <Content gutter={24}>
      {availableSettings.map((option, index) => {
        let label = t(option.label)

        if (option.showIf && !option.showIf(settings)) {
          return null
        }
        switch (option.type) {
          case 'dropdown':
            return (
              <Setting xs={12} key={index}>
                <FieldLabel
                  label={label}
                  tooltip={option.tooltip && t(option.tooltip)}
                >
                  <Select
                    value={
                      option.name in settings
                        ? settings[option.name]
                        : option.default || false
                    }
                    options={option.options}
                    placeholder={t(option.placeholder)}
                    onChange={status => {
                      setSettings({ [option.name]: status })
                    }}
                    getOptionName={key => t(key.name) || key.name}
                    getOptionValue={key => key.value}
                  />
                </FieldLabel>
              </Setting>
            )
          case 'switch':
            return (
              <Setting xs={12} key={index}>
                <FieldLabel
                  label={label}
                  tooltip={option.tooltip && t(option.tooltip)}
                >
                  <Switcher
                    checked={settings[option.name]}
                    onChange={status => {
                      setSettings({ [option.name]: status })
                    }}
                  />
                </FieldLabel>
              </Setting>
            )
          case 'divider':
            return <Divider xs={24} key={index} />
          case 'group-products':
            return (
              <Setting xs={24} key={index} isVariable>
                <Grouping
                  settings={settings[option.name] || []}
                  option={option}
                  setSettings={setSettings}
                  label={label}
                />
              </Setting>
            )
          case 'group-labels':
            return (
              <Setting xs={24} key={index} isVariable>
                <Grouping
                  settings={settings[option.name] || []}
                  option={option}
                  setSettings={setSettings}
                  label={label}
                />
              </Setting>
            )
          case 'color-picker':
            return (
              <Setting xs={24} key={index} isVariable>
                <ColorPicker
                  settings={settings}
                  option={option}
                  setSettings={setSettings}
                  label={label}
                />
              </Setting>
            )
          default:
            return null
        }
      })}
    </Content>
  )
}

export default withTranslation()(StatsOptionsForm)
