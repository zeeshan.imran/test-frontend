import styled from 'styled-components'
import Text from '../../Text'
import colors from '../../../utils/Colors'
import { family } from '../../../utils/Fonts'
import { COMPONENTS_DEFAULT_MARGIN } from '../../../utils/Metrics'

export const PairedQuestionLayout = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 5.5rem;
`

export const QuestionContainer = styled.div`
  display: flex;
  & > :first-child {
    padding-right: 2rem;
    padding-top: 0.5rem;
  }
`

export const QuestionLayout = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`

export const Prompt = styled(Text)`
  font-family: ${family.primaryLight};
  color: ${colors.SLATE_GREY};
  font-size: 2.2rem;
  margin-bottom: 1.5rem;
`

export const SecondaryPrompt = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.5rem;
  color: ${colors.SLATE_GREY};
  margin-bottom: ${COMPONENTS_DEFAULT_MARGIN}rem;
`
