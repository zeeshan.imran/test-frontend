import React from 'react'
import { mount } from 'enzyme'
import OptionButtons from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('OptionButtons', () => {
  let testRender
  let onSelect
  let pair
  let oneFlavorNotPresentEnabled
  let neitherFlavorPresentEnabled
  let bothFlavorsPresentEnabled
  let customFlavorEnabled

  beforeEach(() => {
    onSelect = jest.fn()
    pair = 2
    oneFlavorNotPresentEnabled = []
    neitherFlavorPresentEnabled = { minPairs: 0 }
    bothFlavorsPresentEnabled = { minPairs: 0 }
    customFlavorEnabled = { minPairs: 0 }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render OptionButtons', async () => {
    testRender = mount(
      <OptionButtons
        onSelect={onSelect}
        pair={pair}
        oneFlavorNotPresentEnabled={oneFlavorNotPresentEnabled}
        neitherFlavorPresentEnabled={neitherFlavorPresentEnabled}
        bothFlavorsPresentEnabled={bothFlavorsPresentEnabled}
        customFlavorEnabled={customFlavorEnabled}
      />
    )

    expect(testRender).toMatchSnapshot()
  })
})
