import styled from 'styled-components'
import Text from '../../../../Text'
import colors from '../../../../../utils/Colors'
import { family } from '../../../../../utils/Fonts'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  align-items: center;
`

export const Title = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  color: ${colors.SLATE_GREY};
  text-align: center;
  margin-top: 3.5rem;
  margin-bottom: 2rem;
`
