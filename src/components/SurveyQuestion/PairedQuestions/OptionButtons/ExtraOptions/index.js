import React from 'react'
import { Button } from 'antd'
import { Desktop } from '../../../../Responsive'
import ExtraOptionsModal from '../../ExtraOptionsModal'
import ButtonGroup from '../ButtonGroup'

import { Container, Title } from './styles'
import { withTranslation } from 'react-i18next'

class OtherOptions extends React.Component {
  state = {
    customFlavorModal: false,
    neitherFlavorModal: false
  }

  handleSelectAndCloseModal = value => {
    const { handleSelect } = this.props
    this.setState({ customFlavorModal: false, neitherFlavorModal: false }, () =>
      handleSelect(value)
    )
  }

  render () {
    const {
      t,
      options,
      oneFlavorNotPresent,
      neitherFlavorPresent,
      bothFlavorsPresent,
      customFlavor
    } = this.props
    const { customFlavorModal, neitherFlavorModal } = this.state
    const shouldRenderNeitherFlavorButton =
      oneFlavorNotPresent || neitherFlavorPresent || bothFlavorsPresent
    return (
      <React.Fragment>
        <ExtraOptionsModal
          options={options}
          customFlavorModal={customFlavorModal}
          neitherFlavorModal={neitherFlavorModal}
          oneFlavorNotPresent={oneFlavorNotPresent}
          neitherFlavorPresent={neitherFlavorPresent}
          bothFlavorsPresent={bothFlavorsPresent}
          handleSelection={this.handleSelectAndCloseModal}
          handleCancel={() =>
            this.setState({
              customFlavorModal: false,
              neitherFlavorModal: false
            })
          }
        />
        <Desktop>
          {desktop => (
            <Container>
              <Title>{t('components.surveyInfoTest.otherOptions')}</Title>
              <ButtonGroup>
                {customFlavor && (
                  <Button
                    block
                    onClick={() => this.setState({ customFlavorModal: true })}
                  >
                    {t('components.surveyInfoTest.customOption')}
                  </Button>
                )}
                {shouldRenderNeitherFlavorButton && (
                  <Button
                    block
                    onClick={() => this.setState({ neitherFlavorModal: true })}
                  >
                    {t('components.surveyInfoTest.cantDecide')}
                  </Button>
                )}
              </ButtonGroup>
            </Container>
          )}
        </Desktop>
      </React.Fragment>
    )
  }
}

export default withTranslation()(OtherOptions)
