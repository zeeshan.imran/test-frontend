import React from 'react'
import { mount } from 'enzyme'
import OtherOptions from '.'
import { Button } from 'antd'
import ExtraOptionsModal from '../../ExtraOptionsModal'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('OtherOptions', () => {
  let testRender
  let options
  let oneFlavorNotPresent
  let neitherFlavorPresent
  let bothFlavorsPresent

  beforeEach(() => {
    options = []
    oneFlavorNotPresent = ''
    neitherFlavorPresent = ''
    bothFlavorsPresent = ''
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render OtherOptions', async () => {
    testRender = mount(
      <OtherOptions
        options={options}
        oneFlavorNotPresent={oneFlavorNotPresent}
        neitherFlavorPresent={neitherFlavorPresent}
        bothFlavorsPresent={bothFlavorsPresent}
        customFlavor={'sweet'}
      />
    )

    expect(testRender.find(OtherOptions)).toHaveLength(1)

    testRender
      .find(ExtraOptionsModal)
      .props('handleCancel')
      .handleCancel({
        target: { customFlavorModal: false, neitherFlavorModal: false }
      })
  })

  test('should render OtherOptions onclick Button', async () => {
    testRender = mount(
      <OtherOptions options={options} customFlavor={'sweet'} />
    )

    testRender
      .find(Button)
      .at(0)
      .props()
      .onClick({ target: { customFlavorModal: true } })
  })

  test('should render OtherOptions onclick on last Button', async () => {
    testRender = mount(
      <OtherOptions
        options={options}
        customFlavor={'sweet'}
        neitherFlavorPresent={'yes'}
      />
    )

    testRender
      .find(Button)
      .at(1)
      .props()
      .onClick({ target: { neitherFlavorModal: true } })
  })

  test('test model select event', async () => {
    const handleSelect = jest.fn()
    testRender = mount(
      <OtherOptions
        options={options}
        customFlavor={'sweet'}
        neitherFlavorPresent={'yes'}
        handleSelect={handleSelect}
      />
    )

    testRender.find(ExtraOptionsModal).prop('handleSelection')('value')
    expect(handleSelect).toHaveBeenCalledWith('value')
  })
})
