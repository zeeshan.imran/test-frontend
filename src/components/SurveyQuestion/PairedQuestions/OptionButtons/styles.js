import styled from 'styled-components'
import Text from '../../../Text'
import colors from '../../../../utils/Colors'
import { family } from '../../../../utils/Fonts'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  align-self: flex-start;
  width: 100%;
  max-width: ${({ desktop }) => (desktop ? '32rem' : 'none')};
`

export const NoPairsText = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  color: ${colors.SLATE_GREY};
  margin-top: 1rem;
  align-self: flex-start;
`
