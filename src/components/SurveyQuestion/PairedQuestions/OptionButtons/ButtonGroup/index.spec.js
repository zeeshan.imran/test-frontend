import React from 'react'
import { mount } from 'enzyme'
import ButtonGroup from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('ButtonGroup', () => {
  let testRender
  
  beforeEach(() => {
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ButtonGroup', async () => {
    testRender = mount(
      <ButtonGroup
      />
    )

    expect(testRender.find(ButtonGroup)).toHaveLength(1)
  })

})
