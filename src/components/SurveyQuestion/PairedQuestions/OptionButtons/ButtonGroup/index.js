import React from 'react'
import { Desktop } from '../../../../Responsive'
import { ButtonGroup as StyledButtonGroup } from './styles'

const ButtonGroup = props => (
  <Desktop>
    {desktop => <StyledButtonGroup desktop={desktop} {...props} />}
  </Desktop>
)

export default ButtonGroup
