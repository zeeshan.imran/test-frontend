import styled from 'styled-components'
import { Button as AntdButton, Col } from 'antd'
import colors from "../../../../../utils/Colors"

export const Button = styled(AntdButton)`
  height: auto;
  hyphens: auto;
  min-height: ${({ mobile }) => (mobile ? '6rem' : '3.2rem')};
  font-size: ${({ mobile }) => (mobile ? '1.8rem' : 'inherit')};
  word-break: break-word;
  white-space: pre-line;
  
  :focus {
    color: rgba(0, 0, 0, 0.65);
    border-color: #d9d9d9;
  }
  :hover {
    color: ${colors.PAIRED_QUESTION_HOVER_COLOR};
    background-color: #fff;
    border-color: ${colors.PAIRED_QUESTION_HOVER_COLOR};
  }
  @media (max-width: 768px) {
    :hover {
      color: ${colors.BLACK};
      border-color: #d9d9d9;
      background-color : ${colors.WHITE}
    }
  }
`

export const ImageButtonWrapper = styled(Col)`
  flex-direction: column;
`

export const NoImage = styled.span`
  display: block;
  margin-top: 50%;
  text-align: center;
`