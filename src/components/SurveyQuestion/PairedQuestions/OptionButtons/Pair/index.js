import React, { useMemo } from 'react'
import { Tooltip } from 'antd'
import ButtonGroup from '../ButtonGroup'
import PairedOptionImage from '../PairedOptionImage'
import { Button, ImageButtonWrapper, NoImage } from './styles'
import BaseCard from '../../../../BaseCard'
import StaticUrlOrBase64 from '../../../../../utils/imagesStaticUrl'
import useResponsive from '../../../../../utils/useResponsive/index'

const PairOption = ({ type, data, onSelect }) => {
  const { mobile } = useResponsive()

  const { pair, optionId, image, tooltip } = useMemo(() => {
    return {
      pair: data.pair || '',
      optionId: data.optionId || '',
      image: data.image800,
      tooltip: data.tooltip || ''
    }
  }, [data])

  return (
    <div>
      {type !== 'labelOnly' && (
        <Tooltip title={tooltip}>
          <ImageButtonWrapper>
            <BaseCard onClick={() => onSelect(pair, optionId)}>
              {image ? (
                <PairedOptionImage srcImage={StaticUrlOrBase64(image)} />
              ) : (
                <NoImage>{pair}</NoImage>
              )}
            </BaseCard>
          </ImageButtonWrapper>
        </Tooltip>
      )}

      {type !== 'imageOnly' && (
        <Tooltip title={tooltip}>
          <Button
            block
            mobile={mobile}
            className='pairButtons'
            onClick={() => onSelect(pair, optionId)}
          >
            {pair}
          </Button>
        </Tooltip>
      )}
    </div>
  )
}

const Pair = ({
  optionDisplayType,
  leftAttribute = {},
  rightAttribute = {},
  onSelect
}) => {
  return (
    <ButtonGroup>
      <PairOption
        type={optionDisplayType}
        data={leftAttribute}
        onSelect={onSelect}
      />

      <PairOption
        type={optionDisplayType}
        data={rightAttribute}
        onSelect={onSelect}
      />
    </ButtonGroup>
  )
}

export default Pair
