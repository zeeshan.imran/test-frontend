import React from 'react'
import { mount } from 'enzyme'
import Pair from '.'
import { Button } from './styles'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('Pair', () => {
  let testRender
  let onSelect
  let leftAttribute
  let rightAttribute

  beforeEach(() => {
    onSelect = jest.fn()
    leftAttribute = {
      pair: 'prev',
      image800: null
    }
    rightAttribute = {
      pair: 'next',
      image800: null
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Pair', async () => {
    testRender = mount(
      <Pair
        optionDisplayType={`labelOnly`}
        onSelect={onSelect}
        leftAttribute={leftAttribute}
        rightAttribute={rightAttribute}
      />
    )

    expect(testRender.find(Pair)).toHaveLength(1)
  })

  test('should render Pair on click leftAttribute', async () => {
    testRender = mount(
      <Pair
        optionDisplayType={`labelOnly`}
        onSelect={onSelect}
        leftAttribute={leftAttribute}
      />
    )

    testRender
      .find(Button)
      .first()
      .simulate('click')
    expect(onSelect).toHaveBeenCalled()
  })

  test('should render Pair on click rightAttribute', async () => {
    testRender = mount(
      <Pair
        optionDisplayType={`labelOnly`}
        onSelect={onSelect}
        rightAttribute={rightAttribute}
      />
    )
    testRender
      .find(Button)
      .last()
      .simulate('click')
    expect(onSelect).toHaveBeenCalled()
  })
})
