import React from 'react'
import { Desktop } from '../../../Responsive'
import Pair from './Pair'
import ExtraOptions from './ExtraOptions'
import { withTranslation } from 'react-i18next'

import { Container, NoPairsText } from './styles'

const OptionButtons = ({
  onSelect,
  pair,
  oneFlavorNotPresentEnabled,
  neitherFlavorPresentEnabled,
  bothFlavorsPresentEnabled,
  customFlavorEnabled,
  t,
  optionDisplayType
}) => {
  const hasExtraOptions =
    oneFlavorNotPresentEnabled ||
    neitherFlavorPresentEnabled ||
    bothFlavorsPresentEnabled ||
    customFlavorEnabled
  return (
    <Desktop>
      {desktop => (
        <Container desktop={desktop}>
          {pair ? (
            <React.Fragment>
              <Pair
                onSelect={onSelect}
                {...pair}
                optionDisplayType={optionDisplayType}
                t={t}
              />
              {hasExtraOptions && (
                <ExtraOptions
                  options={pair}
                  oneFlavorNotPresent={oneFlavorNotPresentEnabled}
                  neitherFlavorPresent={neitherFlavorPresentEnabled}
                  bothFlavorsPresent={bothFlavorsPresentEnabled}
                  customFlavor={customFlavorEnabled}
                  handleSelect={onSelect}
                />
              )}
            </React.Fragment>
          ) : (
            <NoPairsText>
              {t('components.surveyInfoTest.noMorePairs')}
            </NoPairsText>
          )}
        </Container>
      )}
    </Desktop>
  )
}

export default withTranslation()(OptionButtons)
