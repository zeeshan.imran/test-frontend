import React from 'react'
import { Image } from './styles'

const PairedOptionImage = ({
  srcImage
}) => (
  <Image
    src={srcImage}
  />
)

export default PairedOptionImage