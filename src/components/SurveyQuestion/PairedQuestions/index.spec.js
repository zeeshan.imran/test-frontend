import React from 'react'
import { act } from 'react-dom/test-utils'
import { shallow } from 'enzyme'
import wait from '../../../utils/testUtils/waait'
import PairedQuestions from './index'
import OptionButtons from './OptionButtons'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('PairedQuestions', () => {
  let testRender
  let onChange
  let pairs
  let pairsOptions

  beforeEach(() => {
    onChange = jest.fn()
    pairs = [
      {
        id: 'pair-1',
        leftAttribute: 'value-1',
        rightAttribute: 'value-2'
      },
      {
        id: 'pair-2',
        leftAttribute: 'value-2',
        rightAttribute: 'value-3'
      }
    ]
    pairsOptions = { minPairs: 2 }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PairedQuestions', async () => {
    testRender = shallow(
      <PairedQuestions
        onChange={onChange}
        pairs={pairs}
        value={[]}
        pairsOptions={pairsOptions}
      />
    )

    expect(testRender).toMatchSnapshot()
  })

  test('trigger the option button', async () => {
    testRender = shallow(
      <PairedQuestions
        onChange={onChange}
        pairs={pairs}
        value={[]}
        pairsOptions={pairsOptions}
      />
    )

    act(() => {
      testRender.find(OptionButtons).prop('onSelect')('value-1')
    })

    await wait(0)
    expect(onChange).toHaveBeenCalled()

    const onChangeValue = onChange.mock.calls[0][0]
    expect(onChangeValue).toMatchObject({
      isValid: false
    })
    expect(onChangeValue.values).toHaveLength(1)
    expect(onChangeValue.values[0]).toMatchObject({
      pairQuestion: 'pair-1',
      value: 'value-1'
    })
  })

  test('trigger the option button (last question)', async () => {
    testRender = shallow(
      <PairedQuestions
        onChange={onChange}
        pairs={pairs}
        value={[{ pairQuestion: 'pair-1', timeToAnswer: 4, value: 'value-1' }]}
        pairsOptions={pairsOptions}
      />
    )

    act(() => {
      testRender.find(OptionButtons).prop('onSelect')('value-2')
    })

    await wait(0)
    expect(onChange).toHaveBeenCalled()

    const onChangeValue = onChange.mock.calls[0][0]
    expect(onChangeValue).toMatchObject({
      isValid: true
    })
    expect(onChangeValue.values).toHaveLength(2)
    expect(onChangeValue.values[0]).toMatchObject(
      {
        pairQuestion: 'pair-1',
        value: 'value-1'
      },
      {
        pairQuestion: 'pair-2',
        value: 'value-2'
      }
    )
  })
  
  test('should not raise onChange if the pairs is full filled', async () => {
    testRender = shallow(
      <PairedQuestions
        onChange={onChange}
        pairs={pairs}
        value={[
          { pairQuestion: 'pair-1', timeToAnswer: 4, value: 'value-1' },
          { pairQuestion: 'pair-2', timeToAnswer: 4, value: 'value-2' }
        ]}
        pairsOptions={pairsOptions}
      />
    )

    act(() => {
      testRender.find(OptionButtons).prop('onSelect')('value-2')
    })

    await wait(0)
    expect(onChange).not.toHaveBeenCalled()
  })
})
