import React, { Component } from 'react'
import Status from './Status'
import OptionButtons from './OptionButtons'
import ProgressBar from './ProgressBar'
import {
  PairedQuestionLayout,
  QuestionContainer,
  QuestionLayout
} from './styles'
import { Prompt, SecondaryPrompt } from '../BaseLayout/styles'

const getCurrentPair = (pairs, answers) => {
  if (answers.length < pairs.length) {
    return pairs[answers.length]
  }
  return null
}

const getQuestionFromProps = ({ type, pairs, pairsOptions }) => ({
  type,
  pairs,
  pairsOptions
})

class PairedQuestions extends Component {
  state = {
    startTime: Date.now()
  }

  handleChange = (value, optionId) => {
    const {
      onChange,
      pairs,
      value: answers = [],
      pairsOptions: { minPairs }
    } = this.props
    const { startTime } = this.state
    const currentQuestion = answers.length

    if (answers.length === pairs.length) {
      return
    }

    const responseTime = Date.now()
    const timeToAnswer = responseTime - startTime
    const pairAnswer = {
      pairQuestion: pairs[currentQuestion].id,
      value,
      optionId,
      timeToAnswer
    }
    const minimumPairsAnswered = answers.length + 1 >= minPairs

    onChange({
      values: [...answers, pairAnswer],
      isValid: minimumPairsAnswered,
      context: getQuestionFromProps(this.props)
    })
    this.setState({ startTime: Date.now() })
  }

  render () {
    const {
      value: answers = [],
      prompt,
      secondaryPrompt,
      pairs,
      pairsOptions: { minPairs, maxPairs, ...extraOptionsProps },
      optionDisplayType
    } = this.props

    return (
      <React.Fragment>
        <Status minimumPairs={minPairs} pairsAnswered={answers.length} />

        <PairedQuestionLayout>
          <QuestionContainer>
            <ProgressBar
              percentage={(answers.length / minPairs) * 100}
              showPercentage={false}
            />
            <QuestionLayout>
              <Prompt>{prompt}</Prompt>
              <SecondaryPrompt>{secondaryPrompt || ''}</SecondaryPrompt>
            </QuestionLayout>
          </QuestionContainer>
          <OptionButtons
            onSelect={this.handleChange}
            pair={answers.length < maxPairs && getCurrentPair(pairs, answers)}
            {...extraOptionsProps}
            optionDisplayType={optionDisplayType}
          />
        </PairedQuestionLayout>
      </React.Fragment>
    )
  }
}

export default PairedQuestions
