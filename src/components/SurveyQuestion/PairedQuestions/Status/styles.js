import styled from 'styled-components'
import Text from '../../../Text'
import colors from '../../../../utils/Colors'
import { family } from '../../../../utils/Fonts'

export const Container = styled.div`
  margin-bottom: 1rem;
`

export const MinimumPairsPrompt = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.5rem;
  color: ${colors.SLATE_GREY};
`

export const PromptAligner = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`
