import React from 'react'
import QuestionMarkModal from './'
import { shallow } from 'enzyme'
import PairedQuestionsInfoModal from '../../../../PairedQuestionsInfoModal'

describe('<QuestionMarkModal>', () => {
  let wrapper
  afterEach(() => {
    wrapper.unmount()
  })

  it('renders <Modal>', () => {
    wrapper = shallow(<QuestionMarkModal visible={false} />)
    expect(wrapper.find(PairedQuestionsInfoModal).length).toEqual(1)

    wrapper
      .find(PairedQuestionsInfoModal)
      .props()
      .onClick({ target: { modalVisible: false } })
  })
})
