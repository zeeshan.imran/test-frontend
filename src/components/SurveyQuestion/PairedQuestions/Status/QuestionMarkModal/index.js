import React from 'react'
import { Circle } from './styles'
import PairedQuestionsInfoModal from '../../../../PairedQuestionsInfoModal'

class QuestionMarkModal extends React.Component {
  state = {
    modalVisible: false
  }

  toggleModal = () => this.setState({ modalVisible: !this.state.modalVisible })

  render () {
    const { modalVisible } = this.state
    return (
      <React.Fragment>
        <PairedQuestionsInfoModal
          visible={modalVisible}
          onClick={this.toggleModal}
        />
        <Circle onClick={this.toggleModal}>?</Circle>
      </React.Fragment>
    )
  }
}

export default QuestionMarkModal
