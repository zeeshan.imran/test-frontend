import styled from 'styled-components'
import colors from '../../../../../utils/Colors'
import { family } from '../../../../../utils/Fonts'

export const Circle = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 3rem;
  height: 3rem;
  border-radius: 50%;
  background-color: ${colors.LIGHT_OLIVE_GREEN};
  color: ${colors.WHITE};
  font-size: 1.8rem;
  font-family: ${family.primaryRegular};
  cursor: pointer;
  user-select: none;
  margin-left: 1.5rem;
  transition: 0.2s ease background-color;
  :hover {
    background-color: ${colors.DARKER_OLIVE_GREEN};
  }
`
