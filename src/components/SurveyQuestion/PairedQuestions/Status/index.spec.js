import React from 'react'
import { mount } from 'enzyme'
import Status from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('Status', () => {
  let testRender
  let minimumPairs

  beforeEach(() => {
    minimumPairs = 2
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Status', () => {
    testRender = mount(<Status minimumPairs={minimumPairs} />)

    expect(testRender).toMatchSnapshot()
  })  
})
