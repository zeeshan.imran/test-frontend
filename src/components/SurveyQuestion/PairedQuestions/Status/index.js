import React from 'react'
import { Desktop } from '../../../Responsive'
import QuestionMarkModal from './QuestionMarkModal'
import { withTranslation } from 'react-i18next'
import {
  Container,
  MinimumPairsPrompt,
  PromptAligner
} from './styles'

const Status = ({ minimumPairs, t }) => (
  <Desktop>
    {desktop => (
      <Container>
        <PromptAligner>
          <MinimumPairsPrompt>{`${t(
            'components.surveyInfoTest.atLeast'
          )} ${minimumPairs} ${
            minimumPairs === 1
              ? t('components.surveyInfoTest.pair')
              : t('components.surveyInfoTest.pairs')
          }`}</MinimumPairsPrompt>
          <QuestionMarkModal />
        </PromptAligner>
      </Container>
    )}
  </Desktop>
)

export default withTranslation()(Status)
