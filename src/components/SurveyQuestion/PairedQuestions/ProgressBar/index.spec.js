import React from 'react'
import { mount } from 'enzyme'
import ProgressBar from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('ProgressBar', () => {
  let testRender
  let percentage
  let showPercentage

  beforeEach(() => {
    percentage = 80
    showPercentage = true
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ProgressBar', async () => {
    testRender = mount(
      <ProgressBar percentage={percentage} showPercentage={showPercentage} />
    )

    expect(testRender.find(ProgressBar)).toHaveLength(1)
  })
})
