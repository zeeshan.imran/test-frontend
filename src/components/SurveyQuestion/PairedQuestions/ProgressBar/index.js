import React from 'react'
import { Progress } from './styles'
import colors from '../../../../utils/Colors'

const ProgressBar = ({ percentage, showPercentage = true }) => (
  <Progress
    type='circle'
    strokeWidth={10}
    width={50}
    percent={percentage}
    showInfo={showPercentage}
    strokeColor={{
      '100%': colors.STROKE_COLOR,
    }}
  />
)

export default ProgressBar
