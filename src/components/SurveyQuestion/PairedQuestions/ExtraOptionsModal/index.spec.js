import React from 'react'
import { shallow } from 'enzyme'
import ExtraOptionsModal from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('ExtraOptionsModal', () => {
  let testRender
  let customFlavorModal
  let neitherFlavorModal
  let oneFlavorNotPresent
  let neitherFlavorPresent
  let bothFlavorsPresent
  let handleSelection
  let handleCancel
  let options

  beforeEach(() => {
    customFlavorModal = true
    neitherFlavorModal = jest.fn()
    oneFlavorNotPresent = 'sweet'
    neitherFlavorPresent = 'bitter'
    bothFlavorsPresent = ''
    handleSelection = jest.fn()
    handleCancel = jest.fn()
    options = []
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ExtraOptionsModal', async () => {
    testRender = shallow(
      <ExtraOptionsModal
        customFlavorModal={customFlavorModal}
        neitherFlavorModal={neitherFlavorModal}
        oneFlavorNotPresent={oneFlavorNotPresent}
        neitherFlavorPresent={neitherFlavorPresent}
        bothFlavorsPresent={bothFlavorsPresent}
        handleSelection={handleSelection}
        handleCancel={handleCancel}
        options={options}
      />
    )
    expect(testRender).toMatchSnapshot()
  })
})
