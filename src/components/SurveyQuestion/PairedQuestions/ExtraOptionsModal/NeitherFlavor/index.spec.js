import React from 'react'
import { mount } from 'enzyme'
import NeitherFlavor from '.'
import { StyledButton } from './styles'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('NeitherFlavor', () => {
  let testRender
  let handleSelect
  let handleCancel
  let oneFlavor
  let neitherFlavor
  let bothFlavors
  let options
  beforeEach(() => {
    handleSelect = jest.fn()
    handleCancel = jest.fn()
    oneFlavor = 'sweet'
    neitherFlavor = ''
    bothFlavors = ''
    options = {
      leftAttribute: {
        pair: '',
        image800: ''
      },
      rightAttribute: {
        pair: '',
        image800: ''
      }
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render NeitherFlavor', async () => {
    testRender = mount(
      <NeitherFlavor
        handleSelect={handleSelect}
        handleCancel={handleCancel}
        oneFlavor={oneFlavor}
        neitherFlavor={neitherFlavor}
        bothFlavors={bothFlavors}
        options={options}
      />
    )
    expect(testRender.find(NeitherFlavor)).toHaveLength(1)
  })

  test('should render NeitherFlavor onclick oneFlavor', async () => {
    testRender = mount(
      <NeitherFlavor
        options={options}
        oneFlavor={'Chocolate'}
        handleSelect={handleSelect}
      />
    )

    testRender
      .find(StyledButton)
      .at(0)
      .props()
      .onClick()

    expect(handleSelect).toHaveBeenCalled()

    testRender
      .find(StyledButton)
      .at(1)
      .props()
      .onClick()

    expect(handleSelect).toHaveBeenCalled()
  })

  test('should render NeitherFlavor onclick oneFlavor', async () => {
    testRender = mount(
      <NeitherFlavor
        options={options}
        neitherFlavor={'bitter'}
        handleSelect={handleSelect}
        handleCancel={handleCancel}
      />
    )

    testRender
      .find(StyledButton)
      .at(0)
      .props()
      .onClick()

    expect(handleSelect).toHaveBeenCalled()
  })
  test('should render NeitherFlavor onclick bothFlavors', async () => {
    testRender = mount(
      <NeitherFlavor
        options={options}
        bothFlavors={'bitter'}
        handleSelect={handleSelect}
        handleCancel={handleCancel}
      />
    )

    testRender
      .find(StyledButton)
      .at(0)
      .props()
      .onClick()

    expect(handleSelect).toHaveBeenCalled()

    testRender
      .find(StyledButton)
      .at(1)
      .props()
      .onClick()

    expect(handleCancel).toHaveBeenCalled()
  })
})
