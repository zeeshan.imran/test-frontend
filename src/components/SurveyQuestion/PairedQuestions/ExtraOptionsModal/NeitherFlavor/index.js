import React from 'react'
import { ButtonGroup, StyledButton } from './styles'
import { withTranslation } from 'react-i18next'

const NeitherFlavor = ({
  t,
  handleSelect,
  handleCancel,
  oneFlavor,
  neitherFlavor,
  bothFlavors,
  options
}) => {
  return (
    <ButtonGroup>
      {oneFlavor && (
        <React.Fragment>
          <StyledButton
            onClick={() => handleSelect(`${options.leftAttribute.pair} not present`)}
            block
          >
            {t('components.surveyInfoTest.isNotPresent', {
              value: options.leftAttribute.pair
            })}
          </StyledButton>
          <StyledButton
            onClick={() =>
              handleSelect(`${options.rightAttribute.pair} not present`)
            }
            block
          >
            {t('components.surveyInfoTest.isNotPresent', {
              value: options.rightAttribute.pair
            })}
          </StyledButton>
        </React.Fragment>
      )}
      {neitherFlavor && (
        <StyledButton onClick={() => handleSelect('neither')}>
          {t('components.surveyInfoTest.noDescriptorPresent')}
        </StyledButton>
      )}
      {bothFlavors && (
        <StyledButton onClick={() => handleSelect('both')}>
          {t('components.surveyInfoTest.bothOptionsPresent')}
        </StyledButton>
      )}
      <StyledButton type='secondary' onClick={() => handleCancel()}>
        {t('buttons.cancel')}
      </StyledButton>
    </ButtonGroup>
  )
}

export default withTranslation()(NeitherFlavor)
