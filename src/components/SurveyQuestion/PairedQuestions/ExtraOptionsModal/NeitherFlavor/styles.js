import styled from 'styled-components'
import Button from '../../../../Button'

export const ButtonGroup = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  align-items: center;
`

export const StyledButton = styled(Button)`
  margin-bottom: 1rem;
  width: 70%;
`
