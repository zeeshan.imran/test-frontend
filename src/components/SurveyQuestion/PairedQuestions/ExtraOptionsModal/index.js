import React from 'react'
import Modal from '../../../Modal'
import CustomFlavor from './CustomFlavor'
import NeitherFlavor from './NeitherFlavor'
import { ModalContent, Title, Prompt } from './styles'
import { withTranslation } from 'react-i18next'
const ExtraOptionsModal = ({
  t,
  customFlavorModal,
  neitherFlavorModal,
  oneFlavorNotPresent,
  neitherFlavorPresent,
  bothFlavorsPresent,
  handleSelection,
  handleCancel,
  options
}) => (
  <Modal visible={customFlavorModal || neitherFlavorModal}>
    <ModalContent>
      <Title>{t('components.surveyInfoTest.optionsModalTitle')}</Title>
      <Prompt>{t('components.surveyInfoTest.pickOption')}</Prompt>
      {customFlavorModal && (
        <CustomFlavor
          handleCancel={handleCancel}
          handleSubmit={handleSelection}
        />
      )}
      {neitherFlavorModal && (
        <NeitherFlavor
          options={options}
          oneFlavor={oneFlavorNotPresent}
          neitherFlavor={neitherFlavorPresent}
          bothFlavors={bothFlavorsPresent}
          handleCancel={handleCancel}
          handleSelect={handleSelection}
        />
      )}
    </ModalContent>
  </Modal>
)

export default withTranslation()(ExtraOptionsModal)
