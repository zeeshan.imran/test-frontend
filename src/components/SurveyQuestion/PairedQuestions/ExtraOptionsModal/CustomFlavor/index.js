import React from 'react'
import Input from '../../../../Input'
import { ButtonGroup, StyledButton } from './styles'
import { withTranslation } from 'react-i18next'

class CustomFlavor extends React.Component {
  state = {
    customFlavor: ''
  }

  render () {
    const { handleSubmit, handleCancel, t } = this.props
    const { customFlavor } = this.state
    return (
      <React.Fragment>
        <Input
          placeholder={t('placeholders.addDescriptor')}
          onChange={e => this.setState({ customFlavor: e.target.value })}
        />
        <ButtonGroup>
          <StyledButton onClick={() => handleCancel()} block type='secondary'>
            {t('buttons.cancel')}
          </StyledButton>
          <StyledButton
            block
            type={customFlavor === '' ? 'disabled' : 'primary'}
            onClick={() => handleSubmit(customFlavor)}
          >
            {t('buttons.submit')}
          </StyledButton>
        </ButtonGroup>
      </React.Fragment>
    )
  }
}

export default withTranslation()(CustomFlavor)
