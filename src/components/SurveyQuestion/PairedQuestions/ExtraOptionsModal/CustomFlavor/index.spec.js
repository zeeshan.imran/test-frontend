import React from 'react'
import { mount } from 'enzyme'
import CustomFlavor from '.'
import { StyledButton } from './styles'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('CustomFlavor', () => {
  let testRender
  let handleSubmit
  let handleCancel

  beforeEach(() => {
    handleSubmit = jest.fn()
    handleCancel = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render CustomFlavor', async () => {
    testRender = mount(
      <CustomFlavor handleSubmit={handleSubmit} handleCancel={handleCancel} />
    )
    expect(testRender.find(CustomFlavor)).toHaveLength(1)
  })

  test('should render CustomFlavor handleCancel', async () => {
    testRender = mount(<CustomFlavor handleCancel={handleCancel} />)
    testRender
      .find(StyledButton)
      .at(0)
      .props()
      .onClick()

    expect(handleCancel).toHaveBeenCalled()
  })

  test('should render CustomFlavor handleSubmit', async () => {
    testRender = mount(<CustomFlavor handleSubmit={handleSubmit} />)
    testRender
      .find(StyledButton)
      .at(1)
      .props()
      .onClick()

    expect(handleSubmit).toHaveBeenCalled()
  })

  test('should render CustomFlavor Input onclick', async () => {
    testRender = mount(
      <CustomFlavor handleSubmit={handleSubmit} handleCancel={handleCancel} />
    )

    testRender
      .findWhere(c => c.name() === 'Input')
      .first()
      .prop('onChange')({ target: { value: 1 } })

    testRender.update()
    expect(testRender.state().customFlavor).toEqual(1)
  })
})
