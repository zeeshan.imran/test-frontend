import styled from 'styled-components'
import Button from '../../../../Button'

export const ButtonGroup = styled.div`
  display: flex;
  margin-top: 2rem;
  width: 100%;
  justify-content: space-between;
`

export const StyledButton = styled(Button)`
  width: 48%;
`
