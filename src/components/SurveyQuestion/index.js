import moment from 'moment'
import gql from 'graphql-tag'
import { Row, Col } from 'antd'
import React, { useEffect } from 'react'
import { useQuery, useMutation } from 'react-apollo-hooks'
import BaseLayout from './BaseLayout'
import ProductImage from './ProductImage'
import useResponsive from '../../utils/useResponsive'
import SurveySharing from '../../containers/SurveySharing'
import SurveyNavigation from '../../containers/SurveyNavigation'
import kebabToComponentCase from '../../utils/kebabToComponentCase'
import { HIDE_SHARING_LINK_QUESTION_TYPES } from '../../utils/Constants'
import TermsAndPrivacyFooter from '../../containers/TermsAndPrivacyFooter'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
// import { useTranslation } from 'react-i18next'
const AUTO_ADVANCE_SUPPORTED = [
  'slider',
  'vertical-rating',
  'dropdown',
  'choose-one',
  'open-answer',
  'email',
  'numeric',
  'select-and-justify'
]
const SHOW_NEXT_WHEN_AUTO_ADVANCE = [
  'open-answer',
  'email',
  'numeric',
  'select-and-justify'
]
/**
 * @param {Object} props
 * @param {Func} props.onSubmitReady - Trigger after the question is answered & ready to submit. It can be called multiple times when the taster changes the answer's values.
 */
const SurveyQuestion = ({
  survey,
  type,
  onSubmit,
  onSubmitSuccess,
  onSubmitError,
  onSubmitReady,
  onSubmitNotReady,
  autoAdvanceSettings,
  country,
  productPictureUrl,
  hideProductPicture,
  productName,
  reward,
  prompt,
  secondaryPrompt,
  isBaseLayout = true,
  isSecondaryPrompt,
  optionDisplayType,
  showProductImage,
  reduceRewardInTasting,
  isRichText,
  surveyId,
  setIsTimerLocked,
  isTimerLocked = false,
  timeCompletedFor,
  setTimeCompletedFor,
  handleAdvance,
  image,
  ...otherQuestionProps
}) => {
  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])
  const saveQuestions = useMutation(SAVE_QUESTIONS)
  const saveSurveyParticipation = useMutation(SAVE_SURVEY_PARTICIPATION)
  const { data: currentSurveyParticipationData } = useQuery(
    SURVEY_PARTICIPATION_QUERY
  )
  const { mobile } = useResponsive()
  if (!type) {
    return null
  }
  const componentName = kebabToComponentCase(type)
  const QuestionComponent = require(`./${componentName}`).default
  const isSubmitHidden =
    autoAdvanceSettings.active &&
    AUTO_ADVANCE_SUPPORTED.includes(type) &&
    !SHOW_NEXT_WHEN_AUTO_ADVANCE.includes(type) &&
    autoAdvanceSettings.hideNextButton
  const {
    currentTime,
    id,
    displayOn,
    delayToNextQuestion,
    extraDelayToNextQuestion,
    t
  } = otherQuestionProps
  // const { t } = useTranslation()

  const { currentSurveyParticipation } = currentSurveyParticipationData
  let timeLimit = 0
  let timeTitle = t('timeDelay.nextQuestionIn')
  let findQuestion = {}
  let showTimer = false
  if (
    survey &&
    survey.addDelayToSelectNextProductAndNextQuestion &&
    currentSurveyParticipation &&
    type !== 'choose-product'
  ) {
    const {
      savedQuestions,
      surveyEnrollmentId,
      selectedProduct
    } = currentSurveyParticipation
    findQuestion = savedQuestions.find(q => {
      if (displayOn === 'middle') {
        return q.id === id && q.product === selectedProduct
      }
      return q.id === id
    })
    if (!findQuestion) {
      findQuestion = {
        id,
        startedAt: currentTime,
        product: displayOn === 'middle' ? selectedProduct : 0,
        delayToNextQuestion,
        extraDelayToNextQuestion
      }
      savedQuestions.push(findQuestion)
      saveSurveyParticipation({
        variables: {
          savedQuestions
        }
      })
      saveQuestions({
        variables: {
          questions: savedQuestions,
          surveyEnrollment: surveyEnrollmentId
        }
      })
    }
    if (
      findQuestion &&
      findQuestion.startedAt &&
      timeCompletedFor.indexOf(
        findQuestion.product
          ? findQuestion.id + '-' + findQuestion.product
          : findQuestion.id
      ) < 0
    ) {
      const timeStarted = findQuestion.startedAt
      const delayToNextQuestionCalc = findQuestion.delayToNextQuestion
      const nextQuestionTime = moment(timeStarted).add(delayToNextQuestionCalc)
      const timeDiff = nextQuestionTime.diff(currentTime)
      if (timeDiff > 0) {
        findQuestion.expiredValue = 'main'
        timeLimit = currentTime + timeDiff
        showTimer = true
        setIsTimerLocked(true)
      } else {
        const valuesCompleted = timeCompletedFor
        valuesCompleted.push(
          findQuestion.product
            ? findQuestion.id + '-' + findQuestion.product
            : findQuestion.id
        )
        setTimeCompletedFor(valuesCompleted)
        setIsTimerLocked(false)
      }
    } else if (
      findQuestion &&
      findQuestion.startedAt &&
      findQuestion.expiredValue &&
      timeCompletedFor.indexOf(
        findQuestion.product
          ? findQuestion.id + '-' + findQuestion.product
          : findQuestion.id
      ) >= 0
    ) {
      const timeStarted = findQuestion.startedAt
      const delayToNextQuestionCalc = findQuestion.delayToNextQuestion
      const extraDelayToNextQuestionCalc = findQuestion.extraDelayToNextQuestion
      const nextQuestionTime = moment(timeStarted).add(delayToNextQuestionCalc)
      const nextQuestionExtraTime = moment(timeStarted)
        .add(delayToNextQuestionCalc)
        .add(extraDelayToNextQuestionCalc)
      if (nextQuestionExtraTime > nextQuestionTime) {
        const timeDiff = nextQuestionExtraTime.diff(currentTime)
        if (timeDiff > 0) {
          findQuestion.expiredValue = 'delayed'
          timeTitle = t('timeDelay.rejectedIn')
          timeLimit = currentTime + timeDiff
          showTimer = true
        } else {
          handleAdvance.current({
            variables: { rejectUserEnrollment: true }
          })
        }
      } else {
        showTimer = false
      }
    }
  }
  const content = (
    <Row type='flex'>
      <Col
        xs={{ span: 24, order: 1 }}
        lg={{
          span: productPictureUrl
            ? hideProductPicture
              ? 24
              : 12
            : image
            ? 12
            : 24,
          order: 1
        }}
      >
        <QuestionComponent
          type={type}
          autoAdvance={autoAdvanceSettings.active}
          onSubmitReady={onSubmitReady}
          onSubmitNotReady={onSubmitNotReady}
          {...otherQuestionProps}
          country={country}
          prompt={prompt}
          secondaryPrompt={secondaryPrompt}
          optionDisplayType={optionDisplayType}
          reduceRewardInTasting={reduceRewardInTasting}
          surveyId={surveyId}
          isTimerLocked={isTimerLocked}
        />
      </Col>
      {(showProductImage || image) && (
        <ProductImage
          reward={showProductImage ? reward : 0}
          src={image || productPictureUrl}
          description={showProductImage ? productName : undefined}
          productPictureUrl={image || productPictureUrl}
          hideProductPicture={hideProductPicture && !image}
          showIncentives={
            otherQuestionProps && otherQuestionProps.showIncentives
          }
        />
      )}
    </Row>
  )
  return (
    <React.Fragment>
      {isBaseLayout ? (
        <BaseLayout
          prompt={prompt}
          secondaryPrompt={isSecondaryPrompt && secondaryPrompt}
          isRichText={isRichText}
          showTimer={showTimer}
          timeTitle={timeTitle}
          timeLimit={timeLimit}
          setIsTimerLocked={setIsTimerLocked}
          maxPoint={findQuestion}
          timeCompletedFor={timeCompletedFor}
          setTimeCompletedFor={setTimeCompletedFor}
          handleAdvance={handleAdvance}
        >
          {content}
        </BaseLayout>
      ) : (
        content
      )}
      {!isTimerLocked && (
        <SurveyNavigation
          withSkipButton={type === 'choose-product'}
          onSubmit={onSubmit}
          onSubmitSuccess={onSubmitSuccess}
          onSubmitError={onSubmitError}
          country={country}
          showSubmit={!isSubmitHidden}
          type={type}
        />
      )}
      {!HIDE_SHARING_LINK_QUESTION_TYPES.includes(type) && (
        <SurveySharing isDesktop={!mobile} />
      )}
      <TermsAndPrivacyFooter isSurvey={!mobile} showMessage />
    </React.Fragment>
  )
}
export default SurveyQuestion
const SAVE_SURVEY_PARTICIPATION = gql`
  mutation saveCurrentSurveyParticipation($savedQuestions: JSON) {
    saveCurrentSurveyParticipation(savedQuestions: $savedQuestions) @client
  }
`
const SAVE_QUESTIONS = gql`
  mutation saveQuestions($questions: JSON, $surveyEnrollment: ID) {
    saveQuestions(questions: $questions, surveyEnrollment: $surveyEnrollment)
  }
`
