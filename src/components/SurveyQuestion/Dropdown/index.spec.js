import React from 'react'
import { shallow } from 'enzyme'
import Dropdown from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('Dropdown', () => {
  let testRender
  let prompt
  let secondaryPrompt
  let onChange
  let options
  let value

  beforeEach(() => {
    prompt = ''
    secondaryPrompt = ''
    onChange = jest.fn()
    options = ['option-1', 'option-2']
    value = []
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Dropdown', async () => {
    testRender = shallow(
      <Dropdown
        prompt={prompt}
        secondaryPrompt={secondaryPrompt}
        onChange={onChange}
        options={options}
        value={value}
      />
    )
    expect(testRender).toMatchSnapshot()
  })

})
