import React from 'react'
import { sort } from 'ramda'
import mapSingleValueToAnswer from '../mapSingleValueToAnswer'
import Select from '../../Select'
import { withTranslation } from 'react-i18next'
import {
  getOptionValue,
  getOptionName
} from '../../../utils/getters/OptionGetters'

const Location = ({
  options,
  autoAdvance,
  onSubmitReady = () => {},
  value = [],
  onChange,
  t
}) => (
  <React.Fragment>
    <Select
      showSearch
      value={value[0]}
      onChange={selectedValue => {
        onChange({
          ...mapSingleValueToAnswer(selectedValue),
          isValid: true
        })
        autoAdvance && onSubmitReady()
      }}
      placeholder={t('placeholders.selectOne')}
      options={sort((a, b) => ('' + a.label).localeCompare(b.label), options)}
      getOptionValue={getOptionValue}
      getOptionName={getOptionName}
    />
  </React.Fragment>
)
export default withTranslation()(Location)
