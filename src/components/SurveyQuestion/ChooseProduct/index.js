import React from 'react'
import ChooseProductContainer from '../../../containers/ChooseProduct'
import getAnswerSingleValue from '../getAnswerSingleValue'
import mapSingleValueToAnswer from '../mapSingleValueToAnswer'

const ChooseProduct = ({
  value,
  onChange,
  country,
  productsSkip,
  autoAdvance,
  onSubmitReady = () => {},
  onSubmitNotReady = () => {},
  showIncentives,
  reduceRewardInTasting,
  isTimerLocked = false
}) => {
  const selectedProduct = getAnswerSingleValue(value)
  return (
    <ChooseProductContainer
      selectedProduct={selectedProduct}
      productsSkip={productsSkip}
      handleChange={v => {
        onChange({ ...mapSingleValueToAnswer(v), isValid: true })
        !v && onSubmitNotReady()
        v && autoAdvance && onSubmitReady()
      }}
      country={country}
      showIncentives={showIncentives}
      reduceRewardInTasting={reduceRewardInTasting}
      isTimerLocked={isTimerLocked}
    />
  )
}

export default ChooseProduct
