import styled from 'styled-components'
import Text from '../../Text'
import { family } from '../../../utils/Fonts'
import { Col } from 'antd'

export const Container = styled.div`
  display: flex;
  justify-content: ${({ desktop }) => (desktop ? 'flex-end' : 'center')};
  width: 100%;
`

export const Aligner = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  max-width: 42rem;
  margin-top: 2rem;
`

export const Image = styled.img`
  margin-bottom: ${({ desktop }) => (desktop ? '2.5rem' : '3.5rem')};
  height: 80%;
  width: 80%;
  object-fit: contain;
`

export const Description = styled(Text)`
  text-align: center;
  font-family: ${family.primaryLight};
  font-size: 1.4rem;
  color: rgba(0, 0, 0, 0.45);
`

export const ColIndex = styled(Col)`
  z-index: -1 !important;
  flex: auto;
`