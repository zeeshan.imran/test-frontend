import React from 'react'
import { Desktop } from '../../Responsive'
import { useTranslation } from 'react-i18next'

import { ColIndex, Container, Aligner, Image, Description } from './styles'
const { REACT_APP_THEME } = process.env
const themeBasedSettingsStatus = REACT_APP_THEME === 'default'

const ProductImage = ({
  src,
  description,
  reward,
  productPictureUrl,
  hideProductPicture,
  showIncentives
}) => {
  const { t } = useTranslation()
  if (!productPictureUrl || hideProductPicture) {
    return null
  }
  return (
    <ColIndex xs={{ span: 24, order: 2 }} lg={{ span: 12, order: 2 }}>
      <Desktop>
        {desktop => (
          <Container desktop={desktop}>
            <Aligner>
              <Image desktop={desktop} src={src} />
              {desktop && <Description>{description}</Description>}
              {desktop &&
                reward &&
                themeBasedSettingsStatus &&
                showIncentives && (
                  <Description>
                    {t('components.question.chooseProduct.reward') +
                      ' ' +
                      reward}
                  </Description>
                )}
            </Aligner>
          </Container>
        )}
      </Desktop>
    </ColIndex>
  )
}

export default ProductImage
