import React from 'react'
import colors from '../../../../utils/Colors'
import { StyledTag } from './styles'

const ClosableTag = ({ name, handleClose }) => (
  <StyledTag closable onClose={handleClose} color={colors.LIGHT_OLIVE_GREEN}>
    {name}
  </StyledTag>
)

export default ClosableTag
