import React from 'react'
import { create, act } from 'react-test-renderer'
import Slider from './index'

jest.doMock('./index.js')

describe('SurveyQuestion/Slider', () => {
  let testRenderer
  let data = {
    prompt: 'this is the prompt',
    sliderOptions: {
      sliders: [
        {
          label: 'this is the first slider'
        },
        {
          label: 'this is the secondsry slider'
        }
      ]
    },
    range: {
      min: 10,
      step: 2,
      max: 140,
      marks: []
    },
    secondaryPrompt: 'this is NOT the prompt',
    values: []
  }

  let mockonChange = jest.fn(changes => {
    data = {
      ...data,
      ...changes
    }
  })

  const update = () => {
    act(() => {
      testRenderer.update(
        <Slider {...data} onChange={mockonChange} value={data.values} />
      )
    })
  }

  beforeEach(() => {
    act(() => {
      testRenderer = create(<Slider {...data} onChange={mockonChange} />)
    })
  })

  afterEach(() => {
    testRenderer.unmount()
  })

  test('change the sliders value', () => {
    const firstSlider = testRenderer.root.findByProps({ name: '0-slider' })
    const secondSlider = testRenderer.root.findByProps({ name: '1-slider' })
    act(() => {
      secondSlider.props.onChange(101)
    })
    update()
    act(() => {
      firstSlider.props.onChange(21.54)
    })
    update()
    expect(firstSlider.props.value).toBe(20)
    expect(secondSlider.props.value).toBe(10)
  })

  test('change the input value', () => {
    const firstInput = testRenderer.root.findByProps({ name: '0-input-number' })
    const secondInput = testRenderer.root.findByProps({
      name: '1-input-number'
    })
    act(() => {
      secondInput.props.onChange(101)
    })
    update()
    act(() => {
      firstInput.props.onChange(21.54)
    })
    update()
    expect(firstInput.props.value).toBe(20)
    expect(secondInput.props.value).toBe(10)
  })
})
