import React, { useEffect, useState, useCallback, useMemo } from 'react'
import { InputNumber, Col, Tooltip } from 'antd'
import {
  LabelContainer,
  Label,
  SliderWraper,
  SliderContainer,
  AntSlider,
  NumberFieldWrapper,
  MinMaxLabel
} from './styles'

const multiplier = 10000

/**
 * @param {Object} props
 * @param {Func} props.onSubmitReady - Trigger after ready to submit. It can be called multiple times when the taster changes the answer's values.
 */
const Slider = ({
  prompt,
  sliderOptions,
  range,
  value = [],
  onChange,
  autoAdvance,
  onSubmitReady = () => {},
  selectedProduct
}) => {
  const sliders = (sliderOptions && sliderOptions.sliders) || []
  const [touchedSliders, setTouchedSliders] = useState([])
  const handleTouchSlider = useCallback(
    name => {
      const nextTouchedSliders = touchedSliders.includes(name)
        ? touchedSliders
        : [...touchedSliders, name]

      setTouchedSliders(nextTouchedSliders)
      if (autoAdvance && nextTouchedSliders.length === sliders.length) {
        onSubmitReady()
      }
    },
    [sliders, touchedSliders, autoAdvance, onSubmitReady]
  )

  useEffect(() => {
    if (value.length === 0 && sliderOptions && sliderOptions.sliders) {
      let newValue = value
      for (let index = 0; index < sliderOptions.sliders.length; index++) {
        newValue[index] = {
          label: sliderOptions.sliders[index].label || '',
          value: range.min
        }
      }
      onChange({
        values: newValue
      })
    }
  })

  const marks = useMemo(() => {
    if (range.marks) {
      let marksData = {}
      let filteredMarks = range.marks.sort(
        (first, second) => first.value - second.value
      )
      filteredMarks.forEach((mark, index) => {
        const top =
          range.isInOneRow || index % 2
            ? mark.isMajorUnit
              ? `0px`
              : `4px`
            : mark.isMajorUnit
            ? `-35px`
            : `-31px`
        marksData[mark.value] = {
          label: mark.label,
          style: {
            whiteSpace: 'noWrap',
            fontSize: mark.isMajorUnit ? '14px' : '10px',
            top
          }
        }
      })

      return marksData
    }

    if (range.min && range.max) {
      return {
        [range.min]: range.min,
        [range.max]: range.max
      }
    }

    return {}
  }, [range])

  const changeHandler = (selectedValue, index, label) => {
    const nextTouchedSliders = touchedSliders.includes(index)
      ? touchedSliders
      : [...touchedSliders, index]

    setTouchedSliders(nextTouchedSliders)

    const newValues = [
      ...value.slice(0, index),
      { label: label, value: selectedValue },
      ...value.slice(index + 1)
    ]

    onChange({
      values: newValues,
      isValid: nextTouchedSliders.length === sliders.length,
      context: {
        type: 'slider',
        range,
        sliderOptions,
        selectedProduct,
        prompt
      }
    })
  }

  const formatSliderValue = newValue =>
    (range.step *
      multiplier *
      Math.floor(
        (newValue * multiplier - range.min * multiplier) /
          (range.step * multiplier),
        10
      ) +
      range.min * multiplier) /
    multiplier

  return (
    <React.Fragment>
      {sliders.map((option, index) => (
        <SliderWraper key={index}>
          <NumberFieldWrapper>
            <Col md={20} sm={24}>
              <LabelContainer>
                <Tooltip title={option.tooltip || option.label}>
                  <Label>{option.label}</Label>
                </Tooltip>
              </LabelContainer>
            </Col>
            <Col md={4} sm={4}>
              <InputNumber
                name={`${index}-input-number`}
                min={range.min}
                max={range.max}
                step={range.step}
                value={
                  value &&
                  value[index] &&
                  (value[index].value || value[index].value === 0)
                    ? value[index].value
                    : range.min
                }
                onChange={change => {
                  const newValue = formatSliderValue(change)
                  changeHandler(newValue, index, option.label)
                  handleTouchSlider(index)
                }}
              />
            </Col>
          </NumberFieldWrapper>
          <SliderContainer
            hasLeft={option.leftLabel}
            hasRight={option.rightLabel}
          >
            {option.leftLabel && <MinMaxLabel>{option.leftLabel}</MinMaxLabel>}
            <AntSlider
              name={`${index}-slider`}
              className={
                (touchedSliders && touchedSliders.indexOf(index) > -1) ||
                (value[index] &&
                  (value[index].value || value[index].value === 0))
                  ? 'slider-value-selected'
                  : ''
              }
              min={range.min}
              max={range.max}
              step={range.step}
              tipFormatter={() => option.tooltip || option.label}
              marks={marks}
              value={
                value[index] && (value[index].value || value[index].value === 0)
                  ? value[index].value
                  : range.min
              }
              onChange={change => {
                if (change <= range.max && change >= range.min) {
                  const newValue = formatSliderValue(change)
                  changeHandler(newValue, index, option.label)
                }
              }}
              onAfterChange={e => {
                if (e === range.min) {
                  if (e <= range.max && e >= range.min) {
                    const newValue = formatSliderValue(e)
                    changeHandler(newValue, index, option.label)
                  }
                }
                handleTouchSlider(index)
              }}
            />
            {option.rightLabel && (
              <MinMaxLabel>{option.rightLabel}</MinMaxLabel>
            )}
          </SliderContainer>
        </SliderWraper>
      ))}
    </React.Fragment>
  )
}
export default Slider
