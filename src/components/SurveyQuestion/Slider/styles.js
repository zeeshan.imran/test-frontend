import styled from 'styled-components'
import { Form, Slider, Row } from 'antd'
import Text from '../../Text'
import { family } from '../../../utils/Fonts'

export const Label = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  color: rgba(0, 0, 0, 0.65);
`

export const LabelContainer = styled(Form.Item)`
  margin-bottom: 0;
`

export const SliderWraper = styled.div`
  padding: 0 12px;
  margin-top: 14px;
`

export const SliderContainer = styled.div`
  display: grid;
  grid-template-columns: ${props => props.hasLeft ? '50px' : ''} 1fr ${props => props.hasRight ? '50px' : ''};
  grid-gap: 16px;
`

export const AntSlider = styled(Slider)`
  
  @media (max-width: 900px) {
    .ant-slider-mark span:first-child {
      transform: translateX(-4%) !important;
      -webkit-transform: translateX(-4%) !important;
      -ms-transform: translateX(-4%) !important;
      -o-transform: translateX(-4%) !important;
    }
    .ant-slider-mark span:last-child {
      // left: 90% !important;
      transform: translateX(-10%) !important;
      -webkit-transform: translateX(-10%) !important;
      -ms-transform: translateX(-10%) !important;
      -o-transform: translateX(-10%) !important;
    }
    .ant-slider-mark span.ant-slider-mark-text:last-child{
      left: auto !important;
      right: 0 !important;
     }
  }
  @media (max-width: 500px) {
    .ant-slider-mark span:last-child {
      // left: 80% !important;
      transform: translateX(20%) !important;
      -webkit-transform: translateX(20%) !important;
      -ms-transform: translateX(20%) !important;
      -o-transform: translateX(20%) !important;
    }
    .ant-slider-mark span.ant-slider-mark-text:last-child{
      left: auto !important;
      right: 0 !important;
     }
  }
`

export const NumberFieldWrapper = styled(Row)`
  margin-bottom: 10px;
`

export const MinMaxLabel = styled.label`
  font-size: 12px;
  word-break: break-word;
  align-self: center;
  text-align: center;
`