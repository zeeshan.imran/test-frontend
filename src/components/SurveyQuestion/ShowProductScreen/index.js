import React from 'react'
import ShowProductScreenContainer from '../../../containers/ShowProductScreen'
import getAnswerSingleValue from '../getAnswerSingleValue'

const ShowProductScreen = ({
  value,
  country,
  productsSkip,
  showIncentives
}) => {
  const selectedProduct = getAnswerSingleValue(value)

  return (
    <ShowProductScreenContainer
      selectedProduct={selectedProduct}
      productsSkip={productsSkip}
      handleChange={v => {
      }}
      country={country}
      showIncentives={showIncentives}
    />
  )
}

export default ShowProductScreen
