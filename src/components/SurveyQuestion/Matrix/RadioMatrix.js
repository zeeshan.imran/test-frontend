import React, { useState, useEffect } from 'react'
import { OptionsContainer,StyledTable } from './styles'
import { withTranslation } from 'react-i18next'
import { Radio } from 'antd'

const RadioMatrix = ({
  selectedOptions = [],
  onChange,
  options,
  matrixOptions,
  t
}) => {
  const additionalQuestion = (matrixOptions && matrixOptions.question) || []
  const [selectionOption, setSelectOption] = useState({})
  const handleValueChange = values => {
    const selection = { ...selectionOption, ...values }
    setSelectOption(selection)
  }
  useEffect(() => {
    const keyLength = Object.keys(selectionOption).length
    const responseLength = additionalQuestion.length
    if (keyLength === responseLength) {
      onChange({
        values: [selectionOption],
        isValid: true
      })
    }
  }, [selectionOption, additionalQuestion, onChange])

  const tableColumns = []
  tableColumns.push({
    title: '',
    dataIndex: 'name',
    key: 'name',
    width: '45vw'
  })
  let rowHeaders = []
  additionalQuestion.forEach((extra, i) => {
    rowHeaders.push({ name: extra.label, id: extra.id })
  })
  const onRadioChange = (name, value) => {
    handleValueChange({ [name]: value })
  }
  options.forEach((option, i) => {
    tableColumns.push({
      title: option.label,
      key: option.value,
      render: row => {
        return (
          <Radio
            checked={selectionOption[row.id] === option.value}
            onChange={() => onRadioChange(row.id, option.value)}
            name={row.id}
            value={option.value}
          />
        )
      }
    })
  })

  return (
    <React.Fragment>
      <OptionsContainer>
        <React.Fragment>
          <StyledTable
            rowKey={record => record.id}
            columns={tableColumns}
            dataSource={rowHeaders}
            bordered
            pagination={false}
          />
        </React.Fragment>
      </OptionsContainer>
    </React.Fragment>
  )
}

export default withTranslation()(RadioMatrix)
