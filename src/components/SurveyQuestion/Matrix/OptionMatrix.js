import React, { useState, useEffect } from 'react'
import { OptionsContainer, InformationText, StyledTable } from './styles'
import { withTranslation } from 'react-i18next'
import Checkbox from '../../Checkbox'
const OptionMatrix = ({
  selectedOptions = [],
  onChange,
  options,
  matrixOptions,
  settings,
  t
}) => {
  const additionalQuestion = (matrixOptions && matrixOptions.question) || []
  const [needCheckMore, setNeedCheckMore] = useState(false)
  const [canCheckMore, setCanCheckMore] = useState(true)
  const [errorMessage, setErrorMessage] = useState('')
  const [selectionOption, setSelectOption] = useState({})
  const [selectedLength, setSelectedLength] = useState([])
  const handleValueChange = values => {
    const selection = { ...selectionOption, ...values }
    setSelectOption(selection)
    getErrorMessages(selection)
  }
  useEffect(() => {
    const keyLength = Object.keys(selectionOption).length
    const responseLength = additionalQuestion.length
    if (keyLength === responseLength) {
      const checkValidity = selectedLength.every(el => el > 0) && !needCheckMore
      onChange({
        values: [selectionOption],
        isValid: checkValidity
      })
    }
  }, [
    selectionOption,
    additionalQuestion,
    onChange,
    needCheckMore,
    canCheckMore,
    selectedLength
  ])
  const getErrorMessages = values => {
    const allKeys = Object.keys(values)
    let keysLength = []
    allKeys.forEach(item => {
      keysLength.push(values[item].length)
    })
    setSelectedLength(keysLength)
    if (
      settings !== null &&
      settings.minAnswerValues !== null &&
      settings.maxAnswerValues === null
    ) {
      setCanCheckMore(true)
      setNeedCheckMore(!keysLength.every(el => el >= settings.minAnswerValues))
      setErrorMessage(
        t('components.surveyQuestion.onlyMinAnswerValues', settings)
      )
    } else if (
      settings !== null &&
      settings.minAnswerValues == null &&
      settings.maxAnswerValues !== null
    ) {
      setCanCheckMore(keysLength.every(el => el < settings.maxAnswerValues))
      setNeedCheckMore(keysLength.every(el => el > settings.maxAnswerValues))
      setErrorMessage(
        t('components.surveyQuestion.onlyMaxAnswerValues', settings)
      )
    } else if (
      settings !== null &&
      settings.minAnswerValues !== null &&
      settings.maxAnswerValues !== null &&
      settings.minAnswerValues === settings.maxAnswerValues
    ) {
      setCanCheckMore(keysLength.every(el => el < settings.maxAnswerValues))
      setNeedCheckMore(keysLength.every(el => el !== settings.minAnswerValues))
      setErrorMessage(
        t('components.surveyQuestion.minMaxSameAnswerValues', settings)
      )
    } else if (
      settings !== null &&
      settings.minAnswerValues !== null &&
      settings.maxAnswerValues !== null &&
      settings.minAnswerValues !== settings.maxAnswerValues
    ) {
      setCanCheckMore(keysLength.every(el => el < settings.maxAnswerValues))
      setNeedCheckMore(keysLength.every(el => el < settings.minAnswerValues))
      setErrorMessage(
        t('components.surveyQuestion.minMaxDiffAnswerValues', settings)
      )
    } else {
      setCanCheckMore(true)
      setNeedCheckMore(false)
      setErrorMessage('')
    }
  }
  const disableCheckBox = (selections, options) => {
    if (!selections.includes(options)) {
      if (
        settings !== null &&
        settings.minAnswerValues !== null &&
        settings.maxAnswerValues === null
      ) {
        return false
      } else if (
        settings !== null &&
        settings.minAnswerValues == null &&
        settings.maxAnswerValues !== null
      ) {
        return selections.length >= settings.maxAnswerValues
      } else if (
        settings !== null &&
        settings.minAnswerValues !== null &&
        settings.maxAnswerValues !== null &&
        settings.minAnswerValues === settings.maxAnswerValues
      ) {
        return selections.length === settings.maxAnswerValues
      } else if (
        settings !== null &&
        settings.minAnswerValues !== null &&
        settings.maxAnswerValues !== null &&
        settings.minAnswerValues !== settings.maxAnswerValues
      ) {
        return selections.length >= settings.maxAnswerValues
      } else {
        return true
      }
    } else {
      return false
    }
  }
  const tableColumns = []
  tableColumns.push({
    title: '',
    dataIndex: 'name',
    key: 'name',
    width: '45vw'
  })
  let rowHeaders = []
  additionalQuestion.forEach((extra, i) => {
    rowHeaders.push({ name: extra.label, id: extra.id })
  })
  const onRadioChange = (name, value) => {
    let values = [value]
    if (selectionOption.hasOwnProperty(`${name}`)) {
      let selectedValues = selectionOption[name]
      if (selectedValues.includes(value)) {
        const optionIndex = selectedValues.indexOf(value)
        selectedValues.splice(optionIndex, 1)
        values = [...selectedValues]
      } else {
        values = [...selectedValues, value]
      }
    }
    handleValueChange({ [name]: values })
  }
  options.forEach((option, i) => {
    tableColumns.push({
      title: option.label,
      key: option.value,
      render: row => {
        return (
          <Checkbox
            checked={
              !!selectionOption[row.id] &&
              selectionOption[row.id].includes(option.value)
            }
            disabled={
              !!selectionOption[row.id] &&
              disableCheckBox(selectionOption[row.id], option.value)
            }
            onChange={() => onRadioChange(row.id, option.value)}
            showLabel={false}
          />
        )
      }
    })
  })
  return (
    <React.Fragment>
      <OptionsContainer>
        <React.Fragment>
          <StyledTable
            rowKey={record => record.id}
            columns={tableColumns}
            dataSource={rowHeaders}
            bordered
            pagination={false}
          />
        </React.Fragment>
      </OptionsContainer>
      {(needCheckMore || !canCheckMore) && (
        <InformationText>{errorMessage}</InformationText>
      )}
    </React.Fragment>
  )
}

export default withTranslation()(OptionMatrix)
