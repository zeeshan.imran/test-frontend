import React from 'react'
import RadioMatrix from './RadioMatrix'
import OptionMatrix from './OptionMatrix'
import { withTranslation } from 'react-i18next'

const Matrix = ({
  value: selectedOptions = [],
  onChange,
  options,
  settings,
  matrixOptions,
  t
}) => {
  switch (settings && settings.chooseMultiple) {
    case false:
      return (
        <RadioMatrix
          onChange={onChange}
          options={options}
          settings={settings}
          matrixOptions={matrixOptions}
          selectedOptions={selectedOptions}
          t={t}
        />
      )
    case true:
      return (
        <OptionMatrix
          onChange={onChange}
          options={options}
          settings={settings}
          matrixOptions={matrixOptions}
          selectedOptions={selectedOptions}
          t={t}
        />
      )
    default:
      return null
  }
}

export default withTranslation()(Matrix)
