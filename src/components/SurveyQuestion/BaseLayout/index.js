import React from 'react'
import { Row, Col } from 'antd'
import {
  Container,
  Prompt,
  SecondaryPrompt,
  CountdownContainer
} from './styles'
import TimerCountdown from '../../TimerCountdown'

const BaseLayout = ({
  prompt,
  secondaryPrompt,
  children,
  isRichText,
  showTimer = false,
  timeLimit = 0,
  timeTitle,
  setIsTimerLocked,
  maxPoint={},
  timeCompletedFor,
  setTimeCompletedFor,
  handleAdvance
}) => (
  <Container>
    <Row>
      <Col xs={{ span: 24 }} md={{ span: 24 }}>
        {isRichText ? (
          <Prompt dangerouslySetInnerHTML={{ __html: prompt }}></Prompt>
        ) : (
          <Prompt>{prompt}</Prompt>
        )}
        {showTimer && (
          <CountdownContainer>
            <TimerCountdown
              titleCountdown={timeTitle}
              timeCountDown={timeLimit}
              onFinishCountDown={async () => {
                if (maxPoint) {
                  if (maxPoint.expiredValue === 'main') {
                    const valuesCompleted = timeCompletedFor
                    valuesCompleted.push(maxPoint.product
                      ? maxPoint.id + '-' + maxPoint.product
                      : maxPoint.id)
                    setTimeCompletedFor(valuesCompleted)
                  }
                  if (maxPoint.expiredValue === 'delayed') {
                    await handleAdvance.current({
                      variables: { rejectUserEnrollment: true }
                    })
                  }
                }
                setIsTimerLocked(false)
              }}
            />
          </CountdownContainer>
        )}
      </Col>
    </Row>
    {secondaryPrompt && <SecondaryPrompt>{secondaryPrompt}</SecondaryPrompt>}
    {children}
  </Container>
)

export default BaseLayout
