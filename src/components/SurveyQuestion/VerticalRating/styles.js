import styled from 'styled-components'

export const Container = styled.div`
  min-height: ${({ max }) => (max ? (max * 3 + 'rem') : '33rem')};
  display: flex;
  justify-content: ${({ desktop }) => (desktop ? 'flex-start' : 'flex-start')};
`
