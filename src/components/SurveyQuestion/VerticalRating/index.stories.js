import React from 'react'
import { storiesOf } from '@storybook/react'
import SurveyVerticalSliderQuestion from './'

const verticalRatingQuestion = {
  type: 'vertical-rating',
  prompt: 'Example of a vertical rating question prompt',
  secondaryPrompt: 'Select one option',
  range: {
    min: 1,
    max: 10,
    step: 1,
    labels: ['Base Label', 'Middle Label', 'Last Label']
  }
}

storiesOf('SurveyVerticalSliderQuestion', module).add('Story', () => (
  <SurveyVerticalSliderQuestion
    question={verticalRatingQuestion}
    onChange={value => console.log('selected the following value', value)}
    value={2}
  />
))
