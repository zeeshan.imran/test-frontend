import React from 'react'
import { mount } from 'enzyme'
import SurveyVerticalSliderQuestion from '.'
import { act } from 'react-dom/test-utils'
import VerticalSlider from '../../VerticalSlider'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('SurveyVerticalSliderQuestion', () => {
  let testRender
  let value
  let onChange
  let prompt
  let secondaryPrompt
  let range

  beforeEach(() => {
    value = null
    onChange = jest.fn()
    prompt = ''
    secondaryPrompt = ''
    range = {}
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SurveyVerticalSliderQuestion', async () => {
    testRender = mount(
      <SurveyVerticalSliderQuestion
        value={value}
        onChange={onChange}
        prompt={prompt}
        secondaryPrompt={secondaryPrompt}
        range={range}
      />
    )

    expect(testRender.find(SurveyVerticalSliderQuestion)).toHaveLength(1)

    act(() => {
      testRender.find(VerticalSlider).prop('onChange')({
        isValid: true,
        values: ['[object Object]']
      })
    })

    testRender.update()
    expect(onChange).toHaveBeenCalledWith({
      isValid: true,
      values: ['[object Object]']
    })
  })

  test('should render SurveyVerticalSliderQuestion onChange', async () => {
    testRender = mount(
      <SurveyVerticalSliderQuestion
        value={undefined}
        onChange={onChange}
        prompt={prompt}
        secondaryPrompt={secondaryPrompt}
        range={{
          skipRangeMax : 3,
          skipRangeMin : 1
        }}
      />
    )

    act(() => {
      testRender
        .find(VerticalSlider)
        .first()
        .prop('onChange')({
          target: {
            value: 3
          }
        })
    })
    expect(onChange).toHaveBeenCalledWith({
      isValid: true,
      skippingTarget: undefined,
      values: ['[object Object]']
    })
  })
})
