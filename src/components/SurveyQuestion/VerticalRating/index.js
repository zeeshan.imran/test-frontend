import React from 'react'
import { Desktop } from '../../Responsive'
import VerticalSlider from '../../VerticalSlider'
import { Container } from './styles'
import mapSingleValueToAnswer from '../mapSingleValueToAnswer'

const SurveyVerticalSliderQuestion = ({
  value,
  onChange,
  range: { min, max, step, labels, skipRangeMax, skipRangeMin },
  autoAdvance,
  onSubmitReady = () => {}
}) => {
  const targetValue = (value || [])[0]
  const formattedValue = targetValue && Number.parseInt(targetValue, 10) // formatted value should be undefined if there is no value
  const handleChange = v => {
    onChange({
      ...mapSingleValueToAnswer(v),
      isValid: true
    })
  }

  return (
    <Desktop>
      {desktop => (
        <Container desktop={desktop} max={max}>
          <VerticalSlider
            min={min}
            max={max}
            step={step}
            onChange={handleChange}
            onAfterChange={() => {
              autoAdvance && onSubmitReady()
            }}
            value={formattedValue}
            labels={labels}
          />
        </Container>
      )}
    </Desktop>
  )
}

export default SurveyVerticalSliderQuestion
