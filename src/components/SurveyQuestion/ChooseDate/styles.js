import styled from 'styled-components'
import { DatePicker as AntDatePicker } from 'antd'

export const DatePicker = styled(AntDatePicker)`
  display: flex;
  flex-direction: column;
  margin-bottom: 5.5rem;
`