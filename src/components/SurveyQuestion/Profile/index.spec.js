import React from 'react'
import { mount } from 'enzyme'
import Profile from '.'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'
import gql from 'graphql-tag'
import SURVEY_PARTICIPATION_QUERY from '../../../queries/SurveyParticipationQuery'
import defaults from '../../../defaults'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import { surveyBasicInfo } from '../../../fragments/survey'

jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k,
  getFixedT: () => () => ''
}))
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

const QUESTION_ID = 'question-1'
const SURVEY_ID = 'survey-1'

const QUESTION_QUERY = gql`
  query question($id: ID) {
    question(id: $id) {
      type
    }
  }
`
const SURVEY_QUERY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      ...surveyBasicInfo
    }
  }

  ${surveyBasicInfo}
`

const mockQuestionQuery = isRequired => ({
  request: {
    query: QUESTION_QUERY,
    variables: { id: QUESTION_ID }
  },
  result: {
    data: {
      question: {
        id: QUESTION_ID,
        required: isRequired,
        products: []
      }
    }
  }
})

describe('Profile', () => {
  let testRender
  let prompt
  let relatedQuestions
  let mockClient

  beforeEach(() => {
    prompt = ''
    relatedQuestions = ''

    mockClient = createApolloMockClient({
      mocks: [mockQuestionQuery(false)]
    })
    mockClient.cache.writeQuery({
      query: SURVEY_PARTICIPATION_QUERY,
      data: {
        currentSurveyParticipation: {
          ...defaults.currentSurveyParticipation,
          surveyId: SURVEY_ID,
          products: ['product-1', 'product-2'],
          selectedProducts: ['product-1'],
          answers: [],
          productRewardsRule: {
            active: false,
            min: 0,
            max: 0,
            percentage: 0
          }
        }
      }
    })
    mockClient.cache.writeQuery({
      query: SURVEY_QUERY,
      variables: { id: SURVEY_ID },
      data: {
        survey: {
          id: SURVEY_ID,
          products: [],
          minimumProducts: 1,
          customButtons: {},
          state: 'draft'
        }
      }
    })
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Profile', async () => {
    const history = createBrowserHistory()

    testRender = mount(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <Profile prompt={prompt} relatedQuestions={relatedQuestions} />
        </Router>
      </ApolloProvider>
    )

    expect(testRender.find(Profile)).toHaveLength(1)
  })
})
