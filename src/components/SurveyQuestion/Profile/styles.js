import styled from 'styled-components'
import BaseText from '../../Text'
import { family } from '../../../utils/Fonts'
import colors from '../../../utils/Colors'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const Title = styled(BaseText)`
  font-family: ${family.primaryLight};
  color: ${colors.SLATE_GREY};
  text-align: center;
  font-size: 4.2rem;
  margin-bottom: 3.5rem;
  display:block;
`

export const ChartContainer = styled.div`
  display: flex;
  flex-direction: ${({ desktop }) => (desktop ? 'row' : 'column')};
  justify-content: space-around;
  width: 100%;
  align-items: center;
  margin-bottom: 5rem;
  flex-wrap: wrap;
`
