import React, { useMemo, useState } from 'react'
import { withTranslation } from 'react-i18next'
import { Form } from 'antd'
import * as Yup from 'yup'
import { Input } from 'antd'

import mapSingleValueToAnswer from '../mapSingleValueToAnswer'
const { TextArea } = Input
const TasterName = ({
  t,
  value,
  settings,
  onChange,
  autoAdvance,
  onSubmitReady = () => {}
}) => {
  const formattedValue = (value || [])[0] // we only use the first of the "array of values" standard
  const [touched, setTouched] = useState(false)
  const validationSchema = useMemo(() => {
    let schema = Yup.string()

    if (settings.answerFormat === 'digits') {
      schema = schema.matches(
        /^[0-9 ]+$/,
        t('validation.tasterName.digitsOnly')
      )
    }

    if (settings.answerFormat === 'letters-and-digits') {
      schema = schema.matches(
        /^[0-9a-zA-Z ]+$/,
        t('validation.tasterName.lettersDigitsOnly')
      )
    }

    if (!settings.allowSpaces) {
      schema = schema.test(
        'no-space',
        t('validation.tasterName.noSpaces'),
        value => !/ /.test(value)
      )
    }

    schema = schema.min(
      settings.minLength,
      t('validation.tasterName.minLength', { min: settings.minLength })
    )

    if (settings.maxLength !== null && settings.maxLength !== undefined) {
      schema = schema.max(
        settings.maxLength,
        t('validation.tasterName.maxLength', { max: settings.maxLength })
      )
    }

    return schema
  }, [settings, t])

  const handleChange = v => {
    onChange({
      ...mapSingleValueToAnswer(v),
      isValid: validationSchema.isValidSync(v)
    })
    setTouched(true)
  }

  let error = ''
  try {
    validationSchema.validateSync(value)
  } catch (ex) {
    error = ex.message
  }

  return (
    <Form.Item
      help={touched && error}
      validateStatus={touched && error ? 'error' : 'success'}
    >
      <TextArea
        defaultValue={formattedValue}
        rows={4}
        onChange={e => handleChange(e.target.value)}
        onKeyUp={e => {
          if (
            e.key === 'Enter' &&
            formattedValue &&
            validationSchema.isValidSync(formattedValue)
          ) {
            autoAdvance && onSubmitReady()
          }
        }}
        placeholder={t('placeholders.writeAnswer')}
      />
    </Form.Item>
  )
}

export default withTranslation()(TasterName)
