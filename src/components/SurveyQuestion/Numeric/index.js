import React, { useState, useCallback, useRef, useMemo } from 'react'
import * as Yup from 'yup'
import Input from '../../Input'
import mapSingleValueToAnswer from '../mapSingleValueToAnswer'
import { Form } from 'antd'
import { withTranslation } from 'react-i18next'

const Numeric = ({
  t,
  onChange,
  required,
  autoAdvance,
  onSubmitReady = () => {},
  numericOptions: { min, max, decimalNumbers }
}) => {
  const [inputValue, setInputValue] = useState('')
  const [error, setError] = useState(false)

  const validateSchema = useMemo(() => {
    let schema = Yup.number().typeError(t('validation.numeric.integer'))
    if (required) {
      schema = schema.required()
    }
    if (min !== null) {
      schema = schema.min(
        parseFloat(min),
        t('validation.numeric.greaterOrEqual', {
          min: min
        })
      )
    }

    if (max !== null) {
      schema = schema.max(
        parseFloat(max),
        t('validation.numeric.lessOrEqual', {
          max: max
        })
      )
    }

    if (decimalNumbers === 0) {
      schema = schema.test(
        'big-integer',
        t('validation.numeric.integer'),
        value => /^[1-9][0-9]*$/g.test(value)
      )
    }

    return schema
  }, [required, decimalNumbers, max, min])

  const handleQuestionValueChange = useRef(async value => {
    const { values } = mapSingleValueToAnswer(value)

    let isValid
    try {
      await validateSchema.validate(value)
      isValid = true
      setError(false)
    } catch (e) {
      isValid = false
      setError(e.message)
    }

    onChange({
      values,
      isValid,
      canSkipAnswer: isValid || !value
    })
  })

  const handleChange = useCallback(
    event => {
      const original = parseFloat(event.target.value)
      const factor = Math.pow(10, decimalNumbers)
      const truncated = Math.floor(original * factor) / factor
      const value =
        isNaN(original) || original === truncated
          ? event.target.value
          : truncated

      setInputValue(String(value))
      handleQuestionValueChange.current(value)
    },
    [handleQuestionValueChange, decimalNumbers]
  )

  return (
    <Form.Item help={error} validateStatus={error ? 'error' : 'success'}>
      <Input
        type='number'
        min={min}
        max={max}
        step={Math.pow(10, -decimalNumbers)}
        value={inputValue}
        onChange={handleChange}
        onKeyUp={e => {
          if (
            e.key === 'Enter' &&
            validateSchema.isValidSync(inputValue) &&
            autoAdvance
          ) {
            const { values } = mapSingleValueToAnswer(inputValue)
            onChange({
              values,
              isValid: true
            })
            onSubmitReady()
          }
        }}
      />
    </Form.Item>
  )
}

export default withTranslation()(Numeric)
