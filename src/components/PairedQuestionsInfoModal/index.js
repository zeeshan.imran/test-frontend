import React from 'react'
import Modal from '../Modal'
import InfoIcon from '../SvgIcons/InfoIcon'
import { Container, Title, Dialog, Button, Icon } from './styles'
import { withTranslation } from 'react-i18next'

const PairedQuestionsInfoModal = ({ visible, onClick, t }) => (
  <Modal visible={visible}>
    <Container>
      <Icon>
        <InfoIcon />
      </Icon>
      <Title>{t('components.surveyInfoTest.information')}</Title>
      <Dialog>{t('components.surveyInfoTest.pairedQuestionInfo')}</Dialog>
      <Button onClick={onClick}>{t('components.surveyInfoTest.close')}</Button>
    </Container>
  </Modal>
)

export default withTranslation()(PairedQuestionsInfoModal)
