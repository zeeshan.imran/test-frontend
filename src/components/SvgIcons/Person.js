import React from 'react'
import colors from '../../utils/Colors'

const Person = props => (
  <svg viewBox='0 0 20 22' xmlns='http://www.w3.org/2000/svg' {...props}>
    <defs>
      <rect id='a' width='26' height='26' rx='13' />
    </defs>
    <g transform='translate(-3 -4)' fill='none' fillRule='evenodd'>
      <mask id='b' fill='#fff'>
        <use xlinkHref='#a' />
      </mask>
      <g mask='url(#b)' fill={props.color}>
        <path d='M12.5 4c2.96 0 5.323 2.463 5.323 5.486 0 3.05-2.363 5.486-5.323 5.486-2.933 0-5.323-2.435-5.323-5.486C7.177 6.463 9.567 4 12.5 4zm.5 12c5.527 0 10 4.646 10 10.328v.056H3v-.056C3 20.646 7.473 16 13 16z' />
      </g>
    </g>
  </svg>
)

Person.defaultProps = {
  color: colors.SLATE_GREY
}

export default Person
