import React from 'react'
import { mount } from 'enzyme'
import Marketing from './Marketing.js'

describe('Marketing', () => {
  let testRender

  beforeAll(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Marketing', async () => {
    testRender = mount(<Marketing />)
    expect(testRender.find('svg')).toHaveLength(1)
    expect(testRender.find('svg[viewBox="0 0 32 32"]')).toHaveLength(1)
    expect(testRender.find('path')).toHaveLength(1)
  })
})
