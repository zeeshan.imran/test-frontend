import React from 'react'
import { mount } from 'enzyme'
import Bank from './Bank.js'

describe('Bank', () => {
  let testRender

  beforeAll(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Bank', async () => {
    testRender = mount(<Bank />)
    expect(testRender.find('svg')).toHaveLength(1)
    expect(testRender.find('svg[viewBox="0 0 23 23"]')).toHaveLength(1)
    expect(testRender.find('g')).toHaveLength(1)
    expect(testRender.find('path')).toHaveLength(3)
  })
})
