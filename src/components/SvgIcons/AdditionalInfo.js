import React from 'react'
import colors from '../../utils/Colors'

const AdditionalInfo = props => (
  <svg viewBox='0 0 16 19' xmlns='http://www.w3.org/2000/svg' {...props}>
    <g fill={props.color}>
      <path d='M16 1.188C16 .53 15.489 0 14.857 0H1.143C.512 0 0 .531 0 1.188v16.625C0 18.468.512 19 1.143 19h13.714C15.49 19 16 18.469 16 17.812V1.188zM4.429 3.563h1.428a1 1 0 0 1 1 1v2.75a1 1 0 0 1-1 1H4.43a1 1 0 0 1-1-1v-2.75a1 1 0 0 1 1-1zm7.549 11.874H4.022a.594.594 0 1 1 0-1.188l7.956.001a.594.594 0 1 1 0 1.188zm0-3.562H4.022a.594.594 0 1 1 0-1.188h7.956a.594.594 0 1 1 0 1.188zm0-3.563H9.737a.594.594 0 0 1 0-1.188l2.24.001a.594.594 0 1 1 0 1.188zm0-3.562H9.737a.594.594 0 0 1 0-1.188h2.24a.594.594 0 1 1 0 1.188z' />
    </g>
  </svg>
)

AdditionalInfo.defaultProps = {
  color: colors.SLATE_GREY
}

export default AdditionalInfo
