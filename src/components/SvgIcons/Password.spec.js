import React from 'react'
import { mount } from 'enzyme'
import Password from './Password.js'

describe('Password', () => {
  let testRender

  beforeAll(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Password', async () => {
    testRender = mount(<Password />)
    expect(testRender.find('svg')).toHaveLength(1)
    expect(testRender.find('svg[viewBox="0 0 20 20"]')).toHaveLength(1)
    expect(testRender.find('path')).toHaveLength(1)
  })
})
