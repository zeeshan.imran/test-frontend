import React from 'react'
import { mount } from 'enzyme'
import Reward from './Reward.js'

describe('Reward', () => {
  let testRender

  beforeAll(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Reward', async () => {
   
    testRender = mount(<Reward />)
    expect(testRender.find('svg')).toHaveLength(1)
    expect(testRender.find('g')).toHaveLength(1)
    expect(testRender.find('path')).toHaveLength(3)

  })
})
