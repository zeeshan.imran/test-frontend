import React from 'react'
import { Tooltip } from 'antd'
import { StyledIcon, TooltipContent } from './styles'
import colors from '../../utils/Colors'

const HelperIcon = ({
  isInLabel = false,
  placement = 'rightTop',
  helperText,
  style,
  ...props
}) => {
  return (
    <Tooltip
      title={
        <TooltipContent
          dangerouslySetInnerHTML={{
            __html: helperText
          }}
        />
      }
      placement={placement}
      arrowPointAtCenter
      trigger='click'
      {...props}
    >
      <StyledIcon
        type='question-circle'
        theme='twoTone'
        twoToneColor={colors.TOOL_TIP_COLOR}
        style={style || {}}
        isinlabel={`true`}
      />
    </Tooltip>
  )
}

export default HelperIcon
