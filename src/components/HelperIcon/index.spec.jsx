import React from 'react'
import { shallow } from 'enzyme'
import HelperIcon from '.'

describe('HelperIcon', () => {
    let testRender
    let placement
    let helperText
    
    beforeEach(() => {
        placement = 'placement'
        helperText = 'Click here'
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render HelperIcon', () => {
        testRender = shallow(            
            <HelperIcon
                placement={placement}
                helperText={helperText}
            />
        )
        expect(testRender).toMatchSnapshot()
    })
})
