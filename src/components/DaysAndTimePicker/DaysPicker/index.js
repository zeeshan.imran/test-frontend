import React from 'react'
import { InputNumber } from 'antd'

const DaysPicker = ({ onChangeDays, daysDelay }) => {
  const daysFormatter = value => value && value.replace(/[^0-9]/g, '')
  return (
    <InputNumber
      placeholder='Days'
      onChange={onChangeDays}
      value={daysDelay}
      formatter={days => daysFormatter(days)}
      size='middle'
      style={{
        marginRight: '1rem'
      }}
    />
  )
}

export default DaysPicker
