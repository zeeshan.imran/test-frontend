import React, { useState, useEffect } from 'react'
import { Icon, Col, Descriptions, Row } from 'antd'
import CustomButton from '../Button'
import DaysPicker from './DaysPicker'
import TimePicker from './TimePicker'
import {
  daysAndTimeConversion,
  convertHoursToDaysAndTime
} from './DaysAndTimeConversion'

const DaysAndTimePicker = ({
  buttonText,
  delayToNext,
  extraDelayToNext,
  onChangeDelayToNext,
  onChangeExtraDelayToNext
}) => {
  const [toggelButton, setToggelButton] = useState(!!delayToNext)

  const { days: daysDelay, time: timeDelay } = convertHoursToDaysAndTime(
    delayToNext
  )

  const {
    days: daysExtraDelay,
    time: timeExtraDelay
  } = convertHoursToDaysAndTime(extraDelayToNext)

  const [time, setTime] = useState(timeDelay)
  const [days, setDays] = useState(daysDelay)
  const [extraTime, setExtraTime] = useState(timeExtraDelay)
  const [extraDays, setExtraDays] = useState(daysExtraDelay)

  useEffect(() => {
    if (days || time) {
      const delayResponse = daysAndTimeConversion(days, time)
      onChangeDelayToNext(delayResponse)
    }
  }, [time, days])

  useEffect(() => {
    if (extraTime || extraDays) {
      const extraDelayResponse = daysAndTimeConversion(extraDays, extraTime)
      onChangeExtraDelayToNext(extraDelayResponse)
    }
  }, [extraTime, extraDays])

  const onChangeDays = value => {
    const days = value && `${value}`.replace(/[^0-9]/g, '')
    setDays(days || '0')
  }

  const onChangeExtraDays = value => {
    const days = value && `${value}`.replace(/[^0-9]/g, '')
    setExtraDays(days || '0')
  }

  const onChangeTime = (timeString, hours) =>
    hours ? setTime(hours) : setTime('00:00:00')

  const onChangeExtraTime = (timeString, hours) =>
    hours ? setExtraTime(hours) : setTime('00:00:00')

  return (
    <React.Fragment>
      {!toggelButton && (
        <CustomButton onClick={() => setToggelButton(true)}>
          {buttonText}
        </CustomButton>
      )}
      {toggelButton && (
        <Row>
          <Col span={8}>
            <Descriptions title='Time Delay' colon={false}>
              <Descriptions.Item>
                <DaysPicker daysDelay={days} onChangeDays={onChangeDays} />
                <TimePicker timeDelay={time} onChangeTime={onChangeTime} allowClear={false} />
              </Descriptions.Item>
            </Descriptions>
          </Col>
          <Col span={10}>
            <Descriptions title='Extra Time Delay' colon={false}>
              <Descriptions.Item>
                <DaysPicker
                  daysDelay={extraDays}
                  onChangeDays={onChangeExtraDays}
                />
                <TimePicker
                  timeDelay={extraTime}
                  onChangeTime={onChangeExtraTime}
                  allowClear={false}
                />
                <CustomButton
                  size='default'
                  type='red'
                  onClick={() => {
                    setToggelButton(false)
                    setDays('')
                    setExtraDays('')
                    setTime('00:00:00')
                    setExtraTime('00:00:00')
                  }}
                >
                  <Icon type='close' />
                </CustomButton>
              </Descriptions.Item>
            </Descriptions>
          </Col>
        </Row>
      )}
    </React.Fragment>
  )
}

export default DaysAndTimePicker
