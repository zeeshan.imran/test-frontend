const getDays = hours => `${Math.trunc(hours / 24)}`
const getHours = hours => `${hours % 24}`

export const daysAndTimeConversion = (days, time) => {
  if (Number(days)) {
    const hoursCount = Number(days) * 24
    const timeSpilt = time.split(':')
    const getHours = timeSpilt[0]
    if (Number(getHours)) {
      return `${hoursCount + Number(getHours)}:${timeSpilt[1]}:${timeSpilt[2]}`
    } else {
      return `${hoursCount + Number(getHours)}:${timeSpilt[1]}:${timeSpilt[2]}`
    }
  } else {
    return time
  }
}

export const convertHoursToDaysAndTime = hours => {
  if (hours && hours.length) {
    const hoursSplit = hours && hours.split(':')
    if (Number(hoursSplit[0]) >= 24) {
      return {
        days: getDays(Number(hoursSplit[0])),
        time: `${getHours(Number(hoursSplit[0]))}:${hoursSplit[1]}:${
          hoursSplit[2]
        }`
      }
    } else {
      return {
        days: '',
        time: hours
      }
    }
  } else {
    return {
      days: '',
      time: '00:00:00'
    }
  }
}
