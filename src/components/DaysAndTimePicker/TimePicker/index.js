import React from 'react'
import moment from 'moment'
import { TimePicker as Time } from 'antd'

const TimePicker = ({ onChangeTime, timeDelay, allowClear }) => (
  <Time
    onChange={onChangeTime}
    value={moment(timeDelay, 'HH:mm:ss')}
    defaultOpenValue={moment('00:00:00', 'HH:mm:ss')}
    style={{
      marginRight: '1rem'
    }}
    allowClear={allowClear}
  />
)

export default TimePicker
