import React from 'react'
import { Form, Icon } from 'antd'
import Input from '../Input'
import OnboardingForm from '../OnboardingForm'
import Error from '../Error'
import SuccessMessage from '../SuccessMessage'

import { InfoContainer, CustomButton } from './styles'
import Loader from '../Loader'
import { useTranslation } from 'react-i18next';

const FormItem = Form.Item

const NewPasswordForm = ({
  password,
  confirmPassword,
  errors,
  touched,
  handleChange,
  handleSubmit,
  isValid,
  infoMessage,
  errorSubmitting,
  loading,
  submitted,
  disabled
}) => {
  const { t } = useTranslation();
  return (
    <OnboardingForm>
      <FormItem
        validateStatus={errors.password ? 'error' : 'success'}
        help={errors.password}
      >
        <Input
          type='password'
          name='password'
          value={password}
          onChange={handleChange}
          prefix={<Icon type='lock' />}
          disabled={disabled}
        />
      </FormItem>
      <FormItem
        validateStatus={
          errors.confirmPassword && touched.confirmPassword ? 'error' : 'success'
        }
        help={touched.confirmPassword && errors.confirmPassword}
      >
        <Input
          type='password'
          name='confirmPassword'
          value={confirmPassword}
          onChange={handleChange}
          prefix={<Icon type='lock' />}
          disabled={disabled}
        />
      </FormItem>
      {!!infoMessage && (
        <InfoContainer>
          {errorSubmitting ? (
            <Error big={disabled} dangerous>{infoMessage}</Error>
          ) : (
            <SuccessMessage>{infoMessage}</SuccessMessage>
          )}
        </InfoContainer>
      )}
      {!submitted && (
        <Loader loading={loading}>
          <CustomButton type='primary' disabled={!isValid} onClick={handleSubmit}>
            {t('changePassword')}
          </CustomButton>
        </Loader>
      )}
    </OnboardingForm>
  )
}

export default NewPasswordForm
