import React from 'react'
import { shallow } from 'enzyme'
import Input from '.';

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('Input', () => {
    let testRender
    let password
    let confirmPassword
    let errors
    let touched
    let handleChange
    let handleSubmit
    let isValid
    let infoMessage
    let errorSubmitting
    let loading
    let submitted

    beforeEach(() => {
        password='test@123'
        confirmPassword ='test@123'
        errors='Please enter password'
        touched=true
        handleChange=jest.fn()
        handleSubmit=jest.fn()
        isValid=false
        infoMessage='Password should be 6 digit'
        errorSubmitting=jest.fn()
        loading=false
        submitted=true
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render Input', async () => {
        testRender = shallow(

            <Input
                password={password}
                confirmPassword={confirmPassword}
                errors={errors}
                touched={touched}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                isValid={isValid}
                infoMessage={infoMessage}
                errorSubmitting={errorSubmitting}
                loading={loading}
                submitted={submitted}
            />
        )
        expect(testRender).toMatchSnapshot()
    })
});