import styled from 'styled-components'
import Text from '../Text'
import { family } from '../../utils/Fonts'
import colors from '../../utils/Colors'

export const Container = styled.div`
  background-color: ${colors.WHITE};
  padding: 2.4rem 3.2rem 3.2rem;
`

export const Title = styled(Text)`
  font-family: ${family.primaryBold};
  font-size: 2rem;
  color: rgba(0, 0, 0, 0.85);
  display: flex;
`

export const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 6.4rem;
`

export const SearchBarContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 1rem;
  width: 50rem;
`
