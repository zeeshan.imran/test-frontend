import React from 'react'
import { useTranslation } from 'react-i18next'
import Select from '../Select'
import SearchBar from '../SearchBar'
import OperatorSurveysList from '../OperatorSurveysList'

import { Container, Title, HeaderContainer, SearchBarContainer } from './styles'

import { COUNTRY_PHONE_CODES } from '../../utils/Constants'

const OperatorActiveSurveys = ({
  pageTitle = '',
  keyword,
  country,
  onFilter = () => {},
  surveys,
  organizations,
  total,
  page,
  onChangePage,
  loading
}) => {
  const { t } = useTranslation()
  return (
    <Container>
      <HeaderContainer>
        <Title>{pageTitle}</Title>
        <SearchBarContainer>
          <Select
            name='country'
            size='default'
            options={COUNTRY_PHONE_CODES}
            getOptionName={option => option.name}
            getOptionValue={option => option.code}
            showSearch
            allowClear
            value={country}
            placeholder={t('placeholders.country')}
            onChange={value => {
              onFilter('country', value)
            }}
          />

          <SearchBar
            withIcon
            value={keyword}
            placeholder={t('placeholders.search')}
            handleChange={value => {
              onFilter('keyword', value)
            }}
          />
        </SearchBarContainer>
      </HeaderContainer>
      <OperatorSurveysList
        isDashboard
        loading={loading}
        surveys={surveys}
        organizations={organizations}
        page={page}
        total={total}
        showHeader={false}
        onChangePage={onChangePage}
        showActions
      />
    </Container>
  )
}

export default OperatorActiveSurveys
