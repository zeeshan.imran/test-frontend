import React from 'react'

import { Container } from './styles'

const HorizontalScroll = ({ children }) => <Container>{children}</Container>

export default HorizontalScroll
