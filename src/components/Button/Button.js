import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Button } from 'antd'
import { family } from '../../utils/Fonts'
import colors from '../../utils/Colors'

export const CustomButtonStyle = styled(Button)`
  width: 100%;
  background-color: ${colors.LIGHT_OLIVE_GREEN};
  border-radius: 0.65rem;
  font-family: ${family.primaryLight};
  font-size: 1.4rem;
  letter-spacing: 0.04rem;

  /* Color that is applied when its hovered */
  &:hover {
    background-color: ${colors.DARKER_OLIVE_GREEN};
    color: ${colors.WHITE};
  }

  /* Color that stays when it is clicked */
  &:focus {
    background-color: ${colors.DARKER_OLIVE_GREEN};
    color: ${colors.WHITE};
  }

  /* Color that flashes when is clicked */
  &:active {
    background-color: ${colors.LIGHT_OLIVE_GREEN};
    color: ${colors.WHITE};
  }

  &[ant-click-animating-without-extra-node]:after {
    border-color: ${colors.SOFT_GREEN} !important;
  }
`

export const FacebookButton = styled(CustomButtonStyle)`
  background-color: ${colors.FACEBOOK_BLUE};
  color: ${colors.WHITE};
  border: none;

  /* Color that is applied when its hovered */
  &:hover {
    background-color: ${colors.DARKER_FACEBOOK_BLUE};
    color: ${colors.WHITE};
  }

  /* Color that stays when it is clicked */
  &:focus {
    background-color: ${colors.DARKER_FACEBOOK_BLUE};
    color: ${colors.WHITE};
  }

  /* Color that flashes when is clicked */
  &:active {
    background-color: ${colors.FACEBOOK_BLUE};
    color: ${colors.WHITE};
  }

  &[ant-click-animating-without-extra-node]:after {
    border-color: ${colors.FACEBOOK_BLUE} !important;
  }
`

export const DisabledButton = styled(CustomButtonStyle)`
  background-color: ${colors.LIGHT_OLIVE_GREEN} !important;
  color: ${colors.WHITE} !important;
  opacity: 0.6;
  border: none;
`

export const SecondaryButton = styled(CustomButtonStyle)`
  background-color: ${colors.WHITE};
  color: ${colors.SLATE_GREY};
  border-color: ${colors.LIGHT_GREY};

  /* Color that is applied when its hovered */
  &:hover {
    background-color: ${colors.LIGHT_GREY};
    color: ${colors.SLATE_GREY};
    border-color: ${colors.LIGHT_GREY};
  }

  /* Color that stays when it is clicked */
  &:focus {
    background-color: ${colors.LIGHT_GREY};
    color: ${colors.SLATE_GREY};
    border-color: ${colors.LIGHT_GREY};
  }

  /* Color that flashes when is clicked */
  &:active {
    background-color: ${colors.LIGHT_OLIVE_GREEN};
    color: ${colors.SLATE_GREY};
    border-color: ${colors.LIGHT_GREY};
  }

  &[ant-click-animating-without-extra-node]:after {
    border-color: ${colors.LIGHT_GREY} !important;
  }
`

export const PrimaryButton = styled(CustomButtonStyle)`
  border: none;
`

class CustomButton extends Component {
  render () {
    const { type } = this.props

    if (type === 'disabled') {
      return <DisabledButton disabled {...this.props} />
    }
    if (type === 'facebook') {
      return <FacebookButton {...this.props} />
    }
    if (type === 'secondary') {
      return <SecondaryButton {...this.props} />
    }
    return <PrimaryButton {...this.props} />
  }
}

CustomButton.defaultProps = {
  type: 'primary',
  size: 'default'
}

CustomButton.propTypes = {
  onClick: PropTypes.func,
  type: PropTypes.string,
  size: PropTypes.string
}

export default CustomButton
