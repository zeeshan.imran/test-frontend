import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import Button from './'

storiesOf('Buttons', module)
  .add('Primary Type, Default Size', () => (
    <Button type='primary' onClick={action('pressed')}>
      Primary Button
    </Button>
  ))
  .add('Secondary Type', () => (
    <Button type='secondary' onClick={action('pressed')}>
      Secondary Button
    </Button>
  ))
  .add('Facebook Type', () => (
    <Button type='facebook' onClick={action('pressed')}>
      Facebook Button
    </Button>
  ))
  .add('Disabled', () => (
    <Button type='disabled' onClick={action('pressed')}>
      Disabled Button
    </Button>
  ))
