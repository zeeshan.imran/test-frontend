import styled from 'styled-components'
import colors from '../../utils/Colors'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${colors.WHITE};
  border-radius: 1rem;
  box-shadow: 0 4px 16px 0 rgba(0, 0, 0, 0.14);
  height: ${({ mobile }) => (mobile ? '15rem' : '17.2rem')};
  width: ${({ mobile }) => (mobile ? '32.5rem' : '100%')};
  margin-right: ${({ mobile }) => (mobile ? '1.5rem' : 0)};
  padding: 2.3rem 3.3rem 2.9rem;
  justify-content: space-between;
  cursor: pointer;
  user-select: none;
  scroll-snap-align: center;
`
