import styled from 'styled-components'
import colors from '../../../utils/Colors'
import { family } from '../../../utils/Fonts'

export const Container = styled.div`
  display: flex;
  align-items: center;
  min-height: 3.2rem;
`

export const Icon = styled.div`
  background: url(${props => props.src}) no-repeat center;
  background-size: contain;
  width: 2.7rem;
  height: 3.2rem;
  margin-right: 1.7rem;
`

export const Text = styled.span`
  font-family: ${family.primaryLight};
  font-size: 1.4rem;
  letter-spacing: 0.04rem;
  font-weight: 300;
  font-style: normal;
  font-stretch: normal;
  color: ${colors.SLATE_GREY};
`

export const Link = styled.span`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  letter-spacing: 0.04rem;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  color: ${colors.SLATE_GREY};
  margin-left: 1.2rem;
`
