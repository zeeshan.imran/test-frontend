import React from 'react'
import PropTypes from 'prop-types'
import Status from './Status'
import Products from './Products'
import TimeLeft from './TimeLeft'
import { Container } from './styles'

const UserSurveyCard = ({
  onClick,
  inProgress,
  numberOfProducts,
  expirationDate,
  mobile
}) => (
  <Container mobile={mobile} onClick={onClick}>
    <Status inProgress={inProgress} />
    <Products numberOfProducts={numberOfProducts} inProgress={inProgress} />
    <TimeLeft expirationDate={expirationDate} inProgress={inProgress} />
  </Container>
)

UserSurveyCard.propTypes = {
  inProgress: PropTypes.bool,
  numberOfProducts: PropTypes.number,
  expirationDate: PropTypes.instanceOf(Date),
  onClick: PropTypes.func
}

export default UserSurveyCard
