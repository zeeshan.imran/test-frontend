import React from 'react'
import { mount } from 'enzyme'
import UserSurveyCard from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('UserSurveyCard', () => {
  let testRender
  let onClick
  let inProgress
  let numberOfProducts
  let mobile
  let expirationDate

  beforeEach(() => {
    onClick = jest.fn()
    inProgress = true
    numberOfProducts = 2
    mobile = null
    expirationDate = new Date('2019-06-13T04:41:20')
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render UserSurveyCard', async () => {
    testRender = mount(
      <UserSurveyCard
        onClick={onClick}
        inProgress={inProgress}
        numberOfProducts={numberOfProducts}
        mobile={mobile}
        expirationDate={expirationDate}
      />
    )

    expect(testRender.find(UserSurveyCard)).toHaveLength(1)
  })
})
