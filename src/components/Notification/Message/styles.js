import styled from 'styled-components'
import Text from '../../Text'
import colors from '../../../utils/Colors'
import { family } from '../../../utils/Fonts'

export const MessageText = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  line-height: 1.14;
  letter-spacing: 0.05rem;
  color: ${colors.PALE_GREY};
  margin-bottom: 1rem;
`
