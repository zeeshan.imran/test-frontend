import React from 'react'
import { mount } from 'enzyme'
import Icon from '.'

describe('Icon', () => {
  let testRender
  let type

  beforeEach(() => {
    type = 'survey'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Icon for survey', async () => {
    testRender = mount(<Icon type={type} />)
    expect(testRender.find(Icon)).toHaveLength(1)
  })

  test('should render Icon for completed', async () => {
    testRender = mount(<Icon type={'completed'} />)
    expect(testRender.find(Icon)).toHaveLength(1)
  })
})
