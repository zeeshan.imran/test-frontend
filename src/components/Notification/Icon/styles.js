import styled from 'styled-components'
import { Icon as AntIcon } from 'antd'
import colors from '../../../utils/Colors'

export const NotificationIcon = styled.div`
  background: url(${props => props.src}) no-repeat center;
  background-size: contain;
  width: 1.5rem;
  height: 1.8rem;
`

export const CompletedNotificationIcon = styled(AntIcon)`
  color: ${colors.WHITE};
`
