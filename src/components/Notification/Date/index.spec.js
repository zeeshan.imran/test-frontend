import React from 'react'
import { shallow } from 'enzyme'
import Date from '.'

describe('Date', () => {
  let testRender
  let children

  beforeEach(() => {
    children = 'date'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Date', async () => {
    testRender = shallow(<Date children={children} />)
    expect(testRender).toMatchSnapshot()
  })
})
