import React from 'react'
import { Container, Label } from './styles'

const RightLabels = ({ labels }) => {
  return (
    <Container>
      {labels.map((label, index) => (
        <Label key={index}>{label}</Label>
      ))}
    </Container>
  )
}

export default RightLabels
