import styled from 'styled-components'
import { Tabs as AntTabs } from 'antd'
import colors from '../../utils/Colors'

export const TabPane = AntTabs.TabPane

export const ContentContainer = styled.div`
  display: flex;
  justify-content: center;
  padding-top: 3.5rem;
`

export const StyledTabs = styled(AntTabs)`
  color: ${colors.SLATE_GREY};

  /* Uncomment to remove the arrows shown in responsive */
  /* .ant-tabs-tab-prev {
    display: none;
  }
  .ant-tabs-tab-next {
    display: none;
  } */
  .ant-tabs-bar {
    border-bottom: none;
    box-shadow: inset 0 -1px 0 0 rgba(0, 0, 0, 0.09);
  }
  .ant-tabs-nav .ant-tabs-tab-active,
  .ant-tabs-tab:hover,
  .ant-tabs-tab:active {
    color: ${colors.LIGHT_OLIVE_GREEN};
    svg {
      g {
        fill: ${colors.LIGHT_OLIVE_GREEN};
      }
    }
  }
  svg {
    g {
      transition: fill 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
    }
  }
  .ant-tabs-ink-bar {
    z-index: 0;
    background-color: ${colors.LIGHT_OLIVE_GREEN};
  }
`
