import React from 'react'
import { mount } from 'enzyme'
import Tabs from './index'
import Person from '../SvgIcons/Person'
import AdditionalInfo from '../SvgIcons/AdditionalInfo'
import Password from '../SvgIcons/Password'
import Privacy from '../PrivacyForm'
import { TabPane, ContentContainer } from './styles'

const TABS = {
  1: {
    icon: <Person />,
    title: 'Personal Detail',
    content: <div>Personal Detail Content</div>
  },
  2: {
    icon: <AdditionalInfo />,
    title: 'Additional Information',
    content: <div>Additional Information Content</div>
  },
  3: {
    icon: <Password />,
    title: 'Change Password',
    content: <div>Change Password Content</div>
  },
  4: {
    icon: <Privacy />,
    title: 'Privacy',
    content: <div>Privacy Content</div>
  }
}

describe('Tabs', () => {
  let testRender

  afterEach(() => {
    testRender.unmount()
  })

  test('should render without crash', () => {
    testRender = mount(
      <Tabs
        tabs={[1, 2, 3, 4]}
        renderTab={id => {
          const { title } = TABS[id]
          return <div>{title}</div>
        }}
        renderTabContent={id => {
          return TABS[id].content
        }}
      />
    )

    //
    expect(testRender.find(TabPane)).toHaveLength(4)

    // showing content of first tab
    expect(testRender.find(ContentContainer)).toHaveLength(1)
    expect(testRender.find(ContentContainer).text()).toBe('Personal Detail Content')
  })
})
