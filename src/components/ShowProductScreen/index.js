import React, { useState } from 'react'
import { Row } from 'antd'
import Select from '../Select/index'
import ProductCard from '../ProductCard'
import useResponsive from '../../utils/useResponsive'
import { Col, Container } from './styles'
import { withTranslation } from 'react-i18next'

const ShowProductScreen = ({
  brands,
  products,
  takenProducts,
  selectedProduct,
  t,
  savedRewards = [],
  prefix,
  suffix,
  isRule,
  showIncentives
}) => {
  const [selectedBrand, setSelectedBrand] = useState('')
  const { mobile } = useResponsive()
  const filteredProducts = products.filter(
    product => product.brand === selectedBrand
  )

  const multipleBrands = brands.length !== 1

  return (
    <React.Fragment>
      {multipleBrands && (
        <Row>
          <Col xs={{ span: 12 }} md={{ span: 12 }}>
            <Select
              options={brands}
              getOptionValue={brand => brand.id}
              getOptionName={brand => brand.name}
              value={selectedBrand}
              placeholder={t('placeholders.selectBrand')}
              onChange={brandId => {
                setSelectedBrand(brandId)
              }}
              size={`large`}
            />
          </Col>
        </Row>
      )}
      <Row gutter={16}>
        <Container>
          {(multipleBrands ? filteredProducts : products).map(
            (product, index) => {
              const reward =
                savedRewards.find(reward => reward.id === product.id) || {}

              return (
                <Col
                  mobile={mobile}
                  key={index}
                  xs={{ span: 12 }}
                  md={{ span: 8 }}
                  lg={{ span: 6 }}
                >
                  <ProductCard
                    selected={selectedProduct === product.id}
                    name={product.name}
                    reward={
                      showIncentives && !!(reward.reward || product.reward) &&
                      `${prefix}${reward.reward || product.reward}${suffix}`
                    }
                    image={product.photo}
                    onClick={() => {}}
                    withSelection={false}
                    withoutBorder={true}
                    isRule={isRule}
                  />
                </Col>
              )
            }
          )}
        </Container>
      </Row>
    </React.Fragment>
  )
}

export default withTranslation()(ShowProductScreen)
