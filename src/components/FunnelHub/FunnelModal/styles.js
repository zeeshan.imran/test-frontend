import styled from 'styled-components'
import Text from '../../Text'
import { Table as AntTable } from 'antd'

export const FieldText = styled(Text)`
  display: block;
  color: #8aba5c;
  font-weight: 800;
  cursor: pointer;
  &:hover {
    color: #52c41a;
    text-decoration: underline;
  }
`

export const ModalImage = styled.img`
  width: 120px;
  height: 120px;
  background: #ddd;
  border-radius: 3px;
  transition: transform 0.25s, visibility 0.25s ease-in;
  &:hover {
    transform: scale(1.5);
  }
`
export const ModalTable = styled(AntTable)`
  .ant-table-thead > tr > th,
  .ant-table-tbody > tr > td {
    padding: 6px 5px 5px 6px;
    font-size: 11px;
    text-align: center;
    &:first-child {
      text-align: left;
    }
  }
  .ant-table-thead > tr > th.ant-table-column-has-actions {
    width: 16% !important;
  }
`
export const TableTitle = styled.span`
  display: flex;
  justify-content: center;
  font-weight: 800;
`
export const ImageContainer = styled.div`
  width: 200px;
  height: 200px;
  background: #ddd;
  border-radius: 3px;
 
`