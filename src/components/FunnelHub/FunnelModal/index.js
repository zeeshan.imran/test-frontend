import React, { useState } from 'react'
import { Modal as AntModal } from 'antd'
import { FieldText } from './styles'
import ModalData from './ModalData'
const FunnelModal = ({ value, prod }) => {
  const [isModalVisible, setIsModalVisible] = useState(false)
  return (
    <React.Fragment>
      <FieldText
        onClick={() => {
          setIsModalVisible(true)
        }}
      >
        {prod.responses + ' / ' + value.responses}
      </FieldText>
      {isModalVisible ? (
        <AntModal
          visible={isModalVisible}
          keyboard={false}
          closable={false}
          width={700}
          onCancel={() => {
            setIsModalVisible(false)
          }}
          onOk={() => {
            setIsModalVisible(false)
          }}
          footer={null}
        >
          <ModalData
            productId={prod.id}
            questionId={value.question.id}
            productName={prod.name}
          />
        </AntModal>
      ) : null}
    </React.Fragment>
  )
}

export default FunnelModal
