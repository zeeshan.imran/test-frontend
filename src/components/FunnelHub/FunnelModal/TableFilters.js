const StateFilters = [
  {
    text: 'Waiting For Product',
    value: 'waiting-for-product'
  },
  {
    text: 'Waiting For Screening',
    value: 'waiting-for-screening'
  },
  {
    text: 'Rejected',
    value: 'rejected'
  },
  {
    text: 'Finished',
    value: 'finished'
  },
]

const ValidationFilters = [
  {
    text: 'Valid',
    value: 'valid'
  },
  {
    text: 'Invalid',
    value: 'invalid'
  },
  {
    text: 'Not Processed',
    value: 'notProcessed'
  }
]
export { StateFilters, ValidationFilters }
