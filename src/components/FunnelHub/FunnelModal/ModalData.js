import React from 'react'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import Zoom from 'react-img-zoom'
import { useTranslation } from 'react-i18next'
import { StateFilters, ValidationFilters } from './TableFilters'
import { SearchTable } from '../../SearchTable'
import { ModalTable, TableTitle } from './styles'
const moment = require('moment')

const ModalData = ({ productId, questionId, productName }) => {
  const { t } = useTranslation()
  const {
    data: { surveyFunnelValidation },
    loading
  } = useQuery(SURVEY_FUNNEL_VALIDATION, {
    variables: {
      questionId,
      productId
    },
    fetchPolicy: 'network-only'
  })
  const columns = [
    {
      title: t(`containers.page.welcomeForm.emailLabel`),
      dataIndex: 'emailAddress',
      key: 'responseEmailAddress',
      width: 200,
      ...SearchTable('emailAddress')
    },
    {
      title: t(`components.surveyBasicInfoForm.name`),
      dataIndex: 'name',
      key: 'responseName',
      width: 250,
      sorter: (a, b) => a.name.length - b.name.length,
      sortDirections: ['descend', 'ascend'],
      ...SearchTable('name')
    },
    {
      title: 'State',
      dataIndex: 'state',
      key: 'state',
      width: 250,
      filters: StateFilters,
      onFilter: (value, record) => record.state.indexOf(value) === 0,
      sorter: (a, b) => a.state.length - b.state.length,
      sortDirections: ['descend', 'ascend']
    },
    {
      title: 'Validation',
      dataIndex: 'validation',
      key: 'validation',
      width: 300,
      filters: ValidationFilters,
      onFilter: (value, record) => {
        return value === 'notProcessed'
          ? record.validation === ''
          : record.validation.indexOf(value) === 0
      },
      sorter: (a, b) => a.validation.length - b.validation.length,
      sortDirections: ['descend', 'ascend']
    },
    {
      title: 'Expired On',
      dataIndex: 'expiredAt',
      key: 'expiredAt',
      width: 200
    },
    {
      title: t(`components.surveyFunnel.columns.responses`),
      dataIndex: 'responses',
      key: 'responseImage',
      render: userImage => {
        return (
          <Zoom
            img={userImage}
            zoomScale={4.5}
            width={120}
            height={120}
            transitionTime={0.3}
          />
        )
      },
      width: 200
    }
  ]
  const expireCheck = (allowedDays, createdAt, state) => {
    if (allowedDays && createdAt) {
      const response = moment(createdAt).add(allowedDays, 'days') < moment()
      if (response && state !== 'finished') {
        return `${moment(createdAt)
          .add(allowedDays, 'days')
          .format('YYYY-MM-DD')}`
      } else {
          return '-'
      }
    }
    else return ''
  }
  const funnelAnswer =
    surveyFunnelValidation && surveyFunnelValidation.funnelAnswer
  const dataSource =
    funnelAnswer &&
    funnelAnswer.map((iterator, index) => {
      return {
        key: `${index}-${iterator.enrollment}`,
        responses: iterator.value[0],
        name: iterator.userInfo.length ? iterator.userInfo[0].fullName : '',
        emailAddress: iterator.userInfo.length
          ? iterator.userInfo[0].emailAddress
          : '',
        expiredAt: expireCheck(
          iterator.enrollmentInfo.allowedDaysToFillTheTasting || 5,
          iterator.enrollmentInfo.createdAt,
          iterator.enrollmentInfo.state
        ),
        state: iterator.enrollmentInfo.state,
        validation: iterator.enrollmentInfo.validation
      }
    })
  return (
    <div>
      <ModalTable
        dataSource={dataSource}
        columns={columns}
        loading={loading}
        title={() => <TableTitle>{productName}</TableTitle>}
      />
    </div>
  )
}

export const SURVEY_FUNNEL_VALIDATION = gql`
  query getSurveyFunnelValidation($questionId: ID, $productId: ID) {
    surveyFunnelValidation(questionId: $questionId, productId: $productId) {
      funnelAnswer {
        enrollment
        product
        question
        value
        userInfo
        enrollmentInfo {
          validation
          state
          processed
          escalation
          allowedDaysToFillTheTasting
          createdAt
        }
      }
    }
  }
`
export default ModalData
