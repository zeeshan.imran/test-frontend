import React, { useState } from 'react'
import gql from 'graphql-tag'
import { useTranslation } from 'react-i18next'
import { Table } from 'antd'
import { useQuery } from 'react-apollo-hooks'
import { Container, UploadPicture } from './styles'
import FunnelModal from './FunnelModal'
const FunnelHub = ({ survey }) => {
  const { t } = useTranslation()
  const [currentPage, setCurrentPage] = useState(1)
  const [perPage] = useState(20)

  const columns = [
    {
      title: t(`components.surveyFunnel.columns.responses`),
      dataIndex: 'responses',
      key: 'responses',
      width: 200
    },
    {
      title: 'User Responded',
      dataIndex: 'userAnswer',
      key: 'userAnswer',
      width: 200
    },
    {
      title: t(`components.surveyFunnel.columns.product`),
      dataIndex: 'product',
      key: 'product',
      width: 200
    },
    {
      title: t(`components.surveyFunnel.columns.question`),
      dataIndex: 'question',
      key: 'question'
    }
  ]

  const {
    data: { surveyFunnel },
    loading
  } = useQuery(SURVEY_FUNNEL, {
    variables: {
      surveyId: survey.id,
      currentPage,
      perPage
    },
    fetchPolicy: 'no-cache'
  })

  const funnelData = surveyFunnel && surveyFunnel.funnel

  const dataSource =
    funnelData &&
    funnelData.map((value, index) => {
      const children = value.product
        ? value.product.map((prod, internalIndex) => {
            if (
              value &&
              value.question &&
              value.question.type === 'upload-picture'
            ) {
              return {
                key: index + '-' + internalIndex,
                responses: <FunnelModal prod={prod} value={value} />,
                question: value.question ? value.question.prompt : '',
                product: prod.name
              }
            } else
              return {
                key: index + '-' + internalIndex,
                responses: prod.responses + ' / ' + value.responses,
                question: value.question ? value.question.prompt : '',
                product: prod.name
              }
          })
        : false
      return {
        key: index,
        responses:
          value.question.type === 'upload-picture' ? (
            <UploadPicture>{value.responses}</UploadPicture>
          ) : (
            value.responses
          ),
        product: value.product
          ? 'Total Products: ' + value.product.length
          : '-',
        question: value.question ? value.question.prompt : '',
        userAnswer: value.userAnswer,
        children: children
      }
    })

  return (
    <Container>
      <React.Fragment>
        <Table
          dataSource={dataSource}
          columns={columns}
          loading={loading}
          pagination={{
            hideOnSinglePage: true,
            current: currentPage,
            total: surveyFunnel && surveyFunnel.total,
            pageSize: perPage
          }}
          onChange={newPage => setCurrentPage(newPage.current)}
        />
      </React.Fragment>
    </Container>
  )
}

export const SURVEY_FUNNEL = gql`
  query surveyFunnel($surveyId: ID, $currentPage: Int, $perPage: Int) {
    surveyFunnel(
      surveyId: $surveyId
      currentPage: $currentPage
      perPage: $perPage
    ) {
      total
      funnel {
        responses
        userAnswer
        product {
          id
          name
          responses
        }
        question {
          id
          prompt
          type
        }
      }
    }
  }
`

export default FunnelHub
