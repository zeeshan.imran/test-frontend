import React from 'react'
import { shallow } from 'enzyme'
import TabBar from '.'

describe('TabBar', () => {
  let testRender
  let activeTab
  let onTabSelection
  let tabs
  let action

  beforeEach(() => {
    activeTab = 'tab1'
    onTabSelection = jest.fn()
    tabs = [{ key: '1', value: 'tab1' }]
    action = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render TabBar', async () => {
    testRender = shallow(
      <TabBar
        activeTab={activeTab}
        onTabSelection={onTabSelection}
        tabs={tabs}
        action={action}
      />
    )
    expect(testRender).toMatchSnapshot()
  })
})
