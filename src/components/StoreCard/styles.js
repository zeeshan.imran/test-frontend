import styled from 'styled-components'

export const Container = styled.div`
  user-select: none;
`

export const Logo = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  border-radius: 1rem;
  background: url(${props => props.src}) no-repeat center;
  background-size: contain;
`
