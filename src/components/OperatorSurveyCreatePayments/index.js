import React from 'react'
import { useTranslation } from 'react-i18next'
import { Formik } from 'formik'
import {
  Container,
  CheckboxContainer,
  PrefixedInput,
  BottomRow,
  BottomCol
} from './styles'
import { Row, Col, Form } from 'antd'
import FieldLabel from '../FieldLabel'
import Input from '../Input'
import { validateSurveyBasics } from '../../validates'
import MandatoryQuestionsSection from '../../containers/MandatoryQuestionsToCreate'
import { MAX_LIMIT_REFERRAL } from '../../utils/Constants'
import defaults from '../../defaults'
import { StyledCheckbox } from '../StyledCheckBox'

const OperatorSurveyPayments = ({ onChange, data, stricted }) => {
  const availableCountry = defaults.countries.find(c => c.code === data.country)
  const rewardCurrency = availableCountry || {}
  const currencyPrefix =
    defaults.currencyPrefixes[rewardCurrency.currency] || ''
  const currencySuffix =
    defaults.currencySuffixes[rewardCurrency.currency] || ''
  const { t } = useTranslation()
  return (
    <React.Fragment>
      <Formik
        enableReinitialize
        validationSchema={validateSurveyBasics()}
        initialValues={{
          referralAmount: data.referralAmount,
          isPaypalSelected: data.isPaypalSelected,
          isGiftCardSelected: data.isGiftCardSelected,
          referralAmountInDollar: data.referralAmountInDollar,
          dualCurrency: data.dualCurrency
        }}
        render={({ errors, values, setValues }) => {
          const { referralAmount: referralAmountError } = errors

          const handleChange = newValues => {
            setValues({ ...values, ...newValues })
            onChange(newValues)
          }

          const handleReferralAmountChange = ({ referralAmount, name }) => {
            let numberValue = parseInt(referralAmount, 10)

            if (numberValue > MAX_LIMIT_REFERRAL) {
              numberValue = Number(numberValue.toString().slice(0, 7))
            }

            handleChange({ [name]: numberValue })
          }

          const handlePaymentMethodsChange = newValues => {
            const { isPaypalSelected, isGiftCardSelected } = {
              ...values,
              ...newValues
            }

            if (!isPaypalSelected && !isGiftCardSelected) {
              handleChange({
                referralAmount: 0,
                referralAmountInDollar: 0,
                ...newValues
              })
              return
            }

            if (
              !values.referralAmount &&
              (isPaypalSelected || isGiftCardSelected)
            ) {
              handleChange({
                referralAmount: 1,
                ...newValues
              })
              return
            }

            handleChange(newValues)
          }

          return (
            <Container>
              <Row gutter={24}>
                <Col xs={{ span: 8 }}>
                  <Form.Item>
                    <FieldLabel label={t('surveyPayments.paymentTypes.paypal')}>
                      <CheckboxContainer>
                        <StyledCheckbox
                          checked={data.isPaypalSelected}
                          disabled={stricted}
                          onChange={e => {
                            handlePaymentMethodsChange({
                              isPaypalSelected: e.target.checked
                            })
                          }}
                        >
                          {t('surveyPayments.active')}
                        </StyledCheckbox>
                      </CheckboxContainer>
                    </FieldLabel>
                  </Form.Item>
                </Col>
                <Col xs={{ span: 8 }}>
                  <Form.Item>
                    <FieldLabel
                      label={t('surveyPayments.paymentTypes.giftCard')}
                    >
                      <CheckboxContainer>
                        <StyledCheckbox
                          checked={data.isGiftCardSelected}
                          disabled={stricted}
                          onChange={e => {
                            data.referralAmount = isNaN(data.referralAmount)
                              ? 0
                              : data.referralAmount
                            handlePaymentMethodsChange({
                              isGiftCardSelected: e.target.checked
                            })
                          }}
                        >
                          {t('surveyPayments.active')}
                        </StyledCheckbox>
                      </CheckboxContainer>
                    </FieldLabel>
                  </Form.Item>
                </Col>
                {(data.isPaypalSelected || data.isGiftCardSelected) && (
                  <Col xs={{ span: 8 }}>
                    <Form.Item
                      help={referralAmountError}
                      validateStatus={referralAmountError ? 'error' : 'success'}
                    >
                      <PrefixedInput
                        largePrefix={
                          (currencyPrefix && currencyPrefix.length > 2) ||
                          (currencySuffix && currencySuffix.length > 2)
                        }
                      >
                        <Input
                          min={0}
                          max={MAX_LIMIT_REFERRAL}
                          type={`number`}
                          prefix={currencyPrefix || currencySuffix}
                          label={t(
                            'containers.surveyBasicInfoForm.referralAmount'
                          )}
                          name='referralAmount'
                          value={data.referralAmount}
                          placeholder={t(
                            'containers.surveyBasicInfoForm.referralAmount'
                          )}
                          onChange={event => {
                            handleReferralAmountChange({
                              referralAmount: event.target.value,
                              name: 'referralAmount'
                            })
                          }}
                          size='default'
                        />
                      </PrefixedInput>
                    </Form.Item>
                  </Col>
                )}
              </Row>
              <BottomRow>
                {(data.isPaypalSelected || data.isGiftCardSelected) &&
                  data.dualCurrency && (
                    <BottomCol xs={{ span: 8 }}>
                      <Form.Item
                        help={referralAmountError}
                        validateStatus={
                          referralAmountError ? 'error' : 'success'
                        }
                      >
                        <PrefixedInput
                          largePrefix={
                            (currencyPrefix && currencyPrefix.length > 2) ||
                            (currencySuffix && currencySuffix.length > 2)
                          }
                        >
                          <Input
                            min={0}
                            max={MAX_LIMIT_REFERRAL}
                            type={`number`}
                            prefix={'$'}
                            label={`${t(
                              'containers.surveyBasicInfoForm.referralAmount'
                            )}($)`}
                            name='referralAmountInDollar'
                            value={data.referralAmountInDollar}
                            placeholder={`${t(
                              'containers.surveyBasicInfoForm.referralAmount'
                            )}($)`}
                            onChange={event => {
                              handleReferralAmountChange({
                                referralAmount: event.target.value,
                                name: 'referralAmountInDollar'
                              })
                            }}
                            size='default'
                          />
                        </PrefixedInput>
                      </Form.Item>
                    </BottomCol>
                  )}
              </BottomRow>
            </Container>
          )
        }}
      />
      <MandatoryQuestionsSection />
    </React.Fragment>
  )
}

export default OperatorSurveyPayments
