import styled from 'styled-components'
import colors from '../../utils/Colors'
import { Row, Col } from 'antd'
export const Container = styled.div`
  background-color: ${colors.WHITE};
  padding: 2.5rem 3.5rem;
  margin-bottom: 2.5rem;
`
export const CheckboxContainer = styled.div`
  line-height: 32px;
`

export const TooltipContent = styled.div`
  ul {
    margin: 0.5rem 0 0 0;
    padding-left: 2rem;
    min-width: 180px;
    li {
      padding-bottom: 0.5rem;
    }
  }
`

export const PrefixedInput = styled.div`
  .ant-input-affix-wrapper .ant-input {
    ${props => (props.largePrefix ? 'padding-left: 46px' : '')}
  }
`
export const BottomRow = styled(Row)`
  display: flex;
  flex-direction: row-reverse;
`
export const BottomCol = styled(Col)`
  padding-left: 15px;
`
