import styled from 'styled-components'
import colors from '../../utils/Colors'

export const Line = styled.div`
  height: 0.1rem;
  background-color: ${colors.BLACK};
  opacity: 0.2;
  margin-bottom: 4rem;
`
