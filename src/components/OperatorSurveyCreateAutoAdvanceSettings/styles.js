import styled from 'styled-components'
import { Row, Form } from 'antd'
import Text from '../Text'
import Input from '../Input'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'

export const Container = styled.div`
  background-color: ${colors.WHITE};
  padding: 2.5rem 3.5rem;
  margin-bottom: 2.5rem;
`
export const CheckboxContainer = styled.div`
  line-height: 32px;
`

export const TooltipContent = styled.div`
  ul {
    margin: 0.5rem 0 0 0;
    padding-left: 2rem;
    min-width: 180px;
    li {
      padding-bottom: 0.5rem;
    }
  }
`

export const AddOptionButtonContainer = styled(Row)`
  margin-bottom: 1.5rem;
`

export const Label = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  color: rgba(0, 0, 0, 0.65);
`

export const LabelContainer = styled(Form.Item)`
  margin-bottom: 0;
`

export const ProductUrlField = styled(Input)`

  .ant-input {
    padding-left: 65px !important;
  }
`
