import React, { useEffect, useRef } from 'react'
import { useTranslation } from 'react-i18next'
import { Formik } from 'formik'
import { Container, CheckboxContainer } from './styles'
import { Row, Col, Form } from 'antd'
import { StyledCheckbox } from '../StyledCheckBox'
import FieldLabel from '../FieldLabel'
import Input from '../Input'
import settingsSchema from '../../validates/settings'
import PromotionOptions from './PromotionOptions'
import PubSub from 'pubsub-js'
import { PUBSUB } from '../../utils/Constants'

const { REACT_APP_THEME } = process.env
const isFlavorwiki = REACT_APP_THEME === 'default'

const AutoAdvanceSettings = ({
  autoAdvanceSettings,
  onSettingsChange,
  sharingButtons,
  onSharingButtonsChange,
  screenerOnly,
  editing = false,
  handleValidatedDataChange,
  validatedData,
  pdfFooterSettings,
  handlePdfSetting,
  showGeneratePdf,
  handleGeneratePdfButton,
  surveyAuthorizationType,
  showOnTasterDashboard,
  handleShowOnTasterDashboard,
  handleShowProductScreen,
  productRewardsRule = {},
  handleProductRewardsRule,
  handleCompulsorySurvey,
  compulsorySurvey,
  handleShowSurveyScore,
  showSurveyScore,
  showSurveyProductScreen = false,
  screenOut,
  handleScreenOut,
  handleScreenOutSettings,
  screenOutSettings,
  onChangePromotionSettings,
  promotionSettings,
  showSharingLink,
  onSharingLinkChange,
  stricted,
  handleShowInternalNameInReports,
  showInternalNameInReports,
  disableAllEmails,
  handleDisableAllEmails,
  handleIncludeCompulsorySurveyDataInStats,
  includeCompulsorySurveyDataInStats,
  handleShowInPreferedLanguage,
  showInPreferedLanguage,
  handleAddDelayToSelectNextProductAndNextQuestion,
  addDelayToSelectNextProductAndNextQuestion,
  showIncentives = false,
  handleShowIncentives,
  showUserProfileDemographics,
  handleShowUserProfileDemographics,
  dualCurrency,
  handleCurrencySurvey,
  handleReduceRewardInTasting,
  reduceRewardInTasting,
  isFlavorwikiOperator = false,
  promotionOptions,
  onChangePromotionOptions
}) => {
  const { t } = useTranslation()
  const formRef = useRef()

  useEffect(() => {
    const token = PubSub.subscribe(PUBSUB.VALIDATE_ADVANCE_SETTINGS, () => {
      if (formRef.current) {
        formRef.current.validateForm()
      }
    })
    return () => {
      PubSub.unsubscribe(token)
    }
  }, [])

  return (
    <Formik
      ref={formRef}
      enableReinitialize
      validationSchema={settingsSchema}
      initialValues={{
        ...autoAdvanceSettings,
        promotionActive: promotionSettings.active,
        promotionCode: promotionSettings.code,
        promotionOptions:
          promotionOptions && promotionOptions.promoProducts
            ? promotionOptions
            : { promoProducts: [{ label: null, url: null }] }
      }}
    >
      {({ errors, values, setFieldValue }) => (
        <Container>
          {isFlavorwiki && (
            <React.Fragment>
              {isFlavorwikiOperator && (
                <Row gutter={24}>
                  <Col xs={{ span: 8 }}>
                    <Form.Item>
                      <FieldLabel
                        tooltip={t('tooltips.screenerOnly')}
                        label={t('surveySettings.screenerOnly.title')}
                      >
                        {editing ? (
                          <span>- {screenerOnly ? `Yes` : `No`}</span>
                        ) : (
                          <CheckboxContainer>
                            <StyledCheckbox
                              disabled={editing || compulsorySurvey}
                              checked={screenerOnly}
                              onChange={(e) => {
                                const checked = e.target.checked
                                onSettingsChange(values, checked)
                                if (checked) {
                                  handleProductRewardsRule(false)
                                }
                              }}
                            >
                              {t('surveySettings.screenerOnly.active')}
                            </StyledCheckbox>
                          </CheckboxContainer>
                        )}
                      </FieldLabel>
                    </Form.Item>
                  </Col>
                </Row>
              )}
              {isFlavorwikiOperator && (
                <Row gutter={24}>
                  <Col xs={{ span: 12 }}>
                    <Form.Item>
                      <FieldLabel
                        label='Add delay to select next product and next question'
                      >
                        <CheckboxContainer>
                          <StyledCheckbox
                            checked={addDelayToSelectNextProductAndNextQuestion}
                            onChange={e => {
                              handleAddDelayToSelectNextProductAndNextQuestion(e.target.checked)
                            }}
                          >
                            {`Yes`}
                          </StyledCheckbox>
                        </CheckboxContainer>
                      </FieldLabel>
                    </Form.Item>
                  </Col>
                </Row>
              )}
              <Row gutter={24}>
                <Col xs={{ span: 8 }}>
                  <Form.Item>
                    <FieldLabel
                      label={t(
                        'surveySettings.autoAdvanceSettings.autoAdvance'
                      )}
                    >
                      <CheckboxContainer>
                        <StyledCheckbox
                          checked={values.active}
                          onChange={(e) => {
                            onSettingsChange(
                              {
                                ...values,
                                active: e.target.checked
                              },
                              screenerOnly
                            )
                          }}
                        >
                          {t('surveySettings.autoAdvanceSettings.active')}
                        </StyledCheckbox>
                      </CheckboxContainer>
                    </FieldLabel>
                  </Form.Item>
                </Col>
                {!!values.active && (
                  <React.Fragment>
                    <Col xs={{ span: 8 }}>
                      <Form.Item>
                        <FieldLabel
                          label={t(
                            'surveySettings.autoAdvanceSettings.nextButton'
                          )}
                          tooltip={t('tooltips.hideNextButtonTooltip')}
                        >
                          <CheckboxContainer>
                            <StyledCheckbox
                              checked={values.hideNextButton}
                              onChange={(e) => {
                                onSettingsChange({
                                  ...values,
                                  hideNextButton: e.target.checked
                                })
                              }}
                            >
                              {t(
                                'surveySettings.autoAdvanceSettings.hideNextButton'
                              )}
                            </StyledCheckbox>
                          </CheckboxContainer>
                        </FieldLabel>
                      </Form.Item>
                    </Col>
                    <Col xs={{ span: 8 }}>
                      <Form.Item
                        help={errors.debounce}
                        validateStatus={errors.debounce ? 'error' : 'success'}
                      >
                        <Input
                          label={t(
                            'surveySettings.autoAdvanceSettings.debounce'
                          )}
                          tooltip={t('tooltips.debounceTooltip')}
                          type="number"
                          size="default"
                          value={values.debounce}
                          onChange={(e) => {
                            const original = parseFloat(e.target.value)
                            const truncated = Math.floor(original * 10) / 10
                            const value =
                              isNaN(original) || original === truncated
                                ? e.target.value
                                : truncated

                            if (value > 1e4) {
                              return
                            }

                            setFieldValue('debounce', value)
                            onSettingsChange({
                              ...values,
                              debounce: value
                            })
                          }}
                        />
                      </Form.Item>
                    </Col>
                  </React.Fragment>
                )}
              </Row>
              <Row gutter={24}>
                <Col xs={{ span: 12 }}>
                  <Form.Item>
                    <FieldLabel
                      label={t('surveySettings.sharingButtons.label')}
                    >
                      <CheckboxContainer>
                        <StyledCheckbox
                          checked={sharingButtons}
                          onChange={(e) => {
                            onSharingButtonsChange(e.target.checked)
                          }}
                        >
                          {t('surveySettings.sharingButtons.yes')}
                        </StyledCheckbox>
                      </CheckboxContainer>
                    </FieldLabel>
                  </Form.Item>
                </Col>
              </Row>
              {isFlavorwikiOperator && (
                <Row gutter={24}>
                  <Col xs={{ span: 12 }}>
                    <Form.Item>
                      <FieldLabel
                        label={t('surveySettings.showSharingLink.label')}
                      >
                        <CheckboxContainer>
                          <StyledCheckbox
                            checked={showSharingLink}
                            onChange={(e) => {
                              onSharingLinkChange(e.target.checked)
                            }}
                          >
                            {t('surveySettings.sharingButtons.yes')}
                          </StyledCheckbox>
                        </CheckboxContainer>
                      </FieldLabel>
                    </Form.Item>
                  </Col>
                  <Col xs={{ span: 12 }}>
                    <Form.Item>
                      <FieldLabel label={t('surveySettings.disableAllEmails.label')}>
                        <CheckboxContainer>
                          <StyledCheckbox
                            checked={disableAllEmails}
                            onChange={(e) => {
                              handleDisableAllEmails(e.target.checked)
                            }}
                          >
                            {t('surveySettings.disableAllEmails.yes')}
                          </StyledCheckbox>
                        </CheckboxContainer>
                      </FieldLabel>
                    </Form.Item>
                  </Col>
                </Row>
              )}
              {!screenerOnly && (
                <Row gutter={24}>
                  <Col xs={{ span: 12 }}>
                    <Form.Item>
                      <FieldLabel
                        label={t('surveySettings.valdatedChartData.label')}
                      >
                        <CheckboxContainer>
                          <StyledCheckbox
                            checked={validatedData}
                            onChange={e => {
                              handleValidatedDataChange(e.target.checked)
                            }}
                          >
                            {t('surveySettings.valdatedChartData.yes')}
                          </StyledCheckbox>
                        </CheckboxContainer>
                      </FieldLabel>
                    </Form.Item>
                  </Col>
                </Row>
              )}
              
              {isFlavorwikiOperator && (
                <Row gutter={24}>
                  <Col xs={{ span: 12 }}>
                    <Form.Item>
                      <FieldLabel
                        label={t('surveySettings.showOnTasterDashboard.label')}
                      >
                        <CheckboxContainer>
                          <StyledCheckbox
                            checked={showOnTasterDashboard}
                            onChange={(e) => {
                              handleShowOnTasterDashboard(e.target.checked)
                            }}
                          >
                            {t('surveySettings.showOnTasterDashboard.yes')}
                          </StyledCheckbox>
                        </CheckboxContainer>
                      </FieldLabel>
                    </Form.Item>
                  </Col>
                </Row>
              )}

              {isFlavorwikiOperator && (
                <Row gutter={24}>
                  <Col xs={{ span: 12 }}>
                    <Form.Item>
                      <FieldLabel
                        label={t('surveySettings.showIncentives.label')}
                      >
                        <CheckboxContainer>
                          <StyledCheckbox
                            checked={showIncentives}
                            onChange={(e) => {
                              handleShowIncentives(e.target.checked)
                            }}
                          >
                            {t('surveySettings.showIncentives.active')}
                          </StyledCheckbox>
                        </CheckboxContainer>
                      </FieldLabel>
                    </Form.Item>
                  </Col>
                </Row>
              )}

              {!includeCompulsorySurveyDataInStats && isFlavorwikiOperator && (
                <Row gutter={24}>
                  <Col xs={{ span: 12 }}>
                    <Form.Item>
                      <FieldLabel
                        label={t('surveySettings.compulsorySurvey.label')}
                      >
                        <CheckboxContainer>
                          <StyledCheckbox
                            disabled={editing || screenerOnly}
                            checked={compulsorySurvey}
                            onChange={(e) => {
                              handleCompulsorySurvey(e.target.checked)
                            }}
                          >
                            {t('surveySettings.compulsorySurvey.yes')}
                          </StyledCheckbox>
                        </CheckboxContainer>
                      </FieldLabel>
                    </Form.Item>
                  </Col>
                  {compulsorySurvey && (
                    <Col xs={{ span: 12 }}>
                      <Form.Item>
                        <FieldLabel
                          label={t(
                            'surveySettings.compulsorySurvey.showSurveyScore.label'
                          )}
                        >
                          <CheckboxContainer>
                            <StyledCheckbox
                              checked={showSurveyScore}
                              onChange={(e) => {
                                handleShowSurveyScore(e.target.checked)
                              }}
                            >
                              {t(
                                'surveySettings.compulsorySurvey.showSurveyScore.yes'
                              )}
                            </StyledCheckbox>
                          </CheckboxContainer>
                        </FieldLabel>
                      </Form.Item>
                    </Col>
                  )}
                </Row>
              )}
              {!compulsorySurvey && isFlavorwikiOperator && (
                <Row gutter={24}>
                  <Col xs={{ span: 12 }}>
                    <Form.Item>
                      <FieldLabel label="Include compulsory survey data in stats and reports">
                        <CheckboxContainer>
                          <StyledCheckbox
                            checked={includeCompulsorySurveyDataInStats}
                            onChange={(e) => {
                              handleIncludeCompulsorySurveyDataInStats(
                                e.target.checked
                              )
                            }}
                          >
                            {t('surveySettings.compulsorySurvey.yes')}
                          </StyledCheckbox>
                        </CheckboxContainer>
                      </FieldLabel>
                    </Form.Item>
                  </Col>
                </Row>
              )}
              {isFlavorwikiOperator && (
                <Row gutter={24}>
                  <Col xs={{ span: 12 }}>
                    <Form.Item>
                      <FieldLabel label="Show survey in prefered language">
                        <CheckboxContainer>
                          <StyledCheckbox
                            checked={showInPreferedLanguage}
                            onChange={(e) => {
                              handleShowInPreferedLanguage(e.target.checked)
                            }}
                          >
                            {t('surveySettings.compulsorySurvey.yes')}
                          </StyledCheckbox>
                        </CheckboxContainer>
                      </FieldLabel>
                    </Form.Item>
                  </Col>
                </Row>
              )}
            </React.Fragment>
          )}
          {!screenerOnly && (
            <React.Fragment>
              <Row gutter={24}>
                <Col xs={{ span: 16 }}>
                  <Form.Item>
                    <FieldLabel
                      tooltip={t('tooltips.showGeneratePdf')}
                      label={t('surveySettings.showGeneratePdf.title')}
                    >
                      <CheckboxContainer>
                        <StyledCheckbox
                          checked={showGeneratePdf}
                          onChange={(e) => {
                            handleGeneratePdfButton(e.target.checked)
                          }}
                        >
                          {t('surveySettings.showGeneratePdf.active')}
                        </StyledCheckbox>
                      </CheckboxContainer>
                    </FieldLabel>
                  </Form.Item>
                </Col>
              </Row>
              <Row gutter={24}>
                <Col xs={{ span: 16 }}>
                  <Form.Item>
                    <FieldLabel
                      tooltip={t('tooltips.showSurveyProductScreen')}
                      label={t('surveySettings.showSurveyProductScreen.title')}
                    >
                      <CheckboxContainer>
                        <StyledCheckbox
                          checked={showSurveyProductScreen}
                          disabled={stricted}
                          onChange={(e) => {
                            handleShowProductScreen(e.target.checked)
                          }}
                        >
                          {t('surveySettings.showSurveyProductScreen.active')}
                        </StyledCheckbox>
                      </CheckboxContainer>
                    </FieldLabel>
                  </Form.Item>
                </Col>
              </Row>
              {!compulsorySurvey &&
                !screenOut &&
                isFlavorwiki &&
                isFlavorwikiOperator && (
                  <Row gutter={24}>
                    <Col xs={{ span: 16 }}>
                      <Form.Item>
                        <FieldLabel
                          tooltip={t('tooltips.productRewardsRule')}
                          label={t('surveySettings.productRewardsRule.title')}
                        >
                          <CheckboxContainer>
                            <StyledCheckbox
                              checked={productRewardsRule.active}
                              onChange={(e) => {
                                handleProductRewardsRule(e.target.checked)
                              }}
                            >
                              {t('surveySettings.productRewardsRule.active')}
                            </StyledCheckbox>
                          </CheckboxContainer>
                        </FieldLabel>
                      </Form.Item>
                    </Col>
                  </Row>
                )}
              {['selected', 'enrollment'].includes(surveyAuthorizationType) &&
                isFlavorwiki && (
                  <Row gutter={24}>
                    <Col xs={{ span: 16 }}>
                      <Form.Item>
                        <FieldLabel
                          label={t(
                            'components.surveyBasicInfoForm.showUserProfileDemographics'
                          )}
                        >
                          <CheckboxContainer>
                            <StyledCheckbox
                              checked={showUserProfileDemographics}
                              onChange={(e) => {
                                handleShowUserProfileDemographics(
                                  e.target.checked
                                )
                              }}
                            >
                              {t(
                                'components.surveyBasicInfoForm.showUserProfileDemographics'
                              )}
                            </StyledCheckbox>
                          </CheckboxContainer>
                        </FieldLabel>
                      </Form.Item>
                    </Col>
                  </Row>
                )}
              {!compulsorySurvey && isFlavorwiki && (
                <Row gutter={24}>
                  <Col xs={{ span: 12 }}>
                    <Form.Item>
                      <FieldLabel label="Reduce reward in tasting">
                        <CheckboxContainer>
                          <StyledCheckbox
                            checked={reduceRewardInTasting}
                            onChange={(e) => {
                              handleReduceRewardInTasting(e.target.checked)
                            }}
                          >
                            {t('surveySettings.compulsorySurvey.yes')}
                          </StyledCheckbox>
                        </CheckboxContainer>
                      </FieldLabel>
                    </Form.Item>
                  </Col>
                </Row>
              )}
            </React.Fragment>
          )}
          <Row gutter={24}>
            <Col xs={{ span: 8 }}>
              <Form.Item>
                <FieldLabel label={t('surveySettings.pdfFooterData.label')}>
                  <CheckboxContainer>
                    <StyledCheckbox
                      checked={pdfFooterSettings.active}
                      onChange={(e) => {
                        handlePdfSetting({
                          ...pdfFooterSettings,
                          active: e.target.checked
                        })
                      }}
                    >
                      {t('surveySettings.valdatedChartData.yes')}
                    </StyledCheckbox>
                  </CheckboxContainer>
                </FieldLabel>
              </Form.Item>
            </Col>
            {!!pdfFooterSettings.active && (
              <Col xs={{ span: 16 }}>
                <Form.Item
                  help={errors.debounce}
                  validateStatus={errors.debounce ? 'error' : 'success'}
                >
                  <Input
                    label={t('surveySettings.pdfFooterSettings.note')}
                    type="TextArea"
                    size="default"
                    value={pdfFooterSettings.footerNote}
                    onChange={(e) => {
                      handlePdfSetting({
                        ...pdfFooterSettings,
                        footerNote: e.target.value
                      })
                    }}
                  />
                </Form.Item>
              </Col>
            )}
          </Row>
          {!productRewardsRule.active && isFlavorwiki && isFlavorwikiOperator && (
            <Row>
              {/* New field Entry */}
              <Col xs={{ span: 8 }}>
                <Form.Item>
                  <FieldLabel label={"Don't Screen Out Anyone"}>
                    <CheckboxContainer>
                      <StyledCheckbox
                        checked={screenOut}
                        onChange={(e) => {
                          handleScreenOut(e.target.checked)
                        }}
                      >
                        {t('surveySettings.valdatedChartData.yes')}
                      </StyledCheckbox>
                    </CheckboxContainer>
                  </FieldLabel>
                </Form.Item>
              </Col>
              {!!screenOut && (
                <React.Fragment>
                  <Col xs={{ span: 8 }}>
                    <Form.Item>
                      <FieldLabel label={'Reject after'}>
                        <CheckboxContainer>
                          <StyledCheckbox
                            checked={screenOutSettings.reject}
                            onChange={(e) => {
                              handleScreenOutSettings({
                                ...screenOutSettings,
                                reject: e.target.checked
                              })
                            }}
                          >
                            {t('surveySettings.valdatedChartData.yes')}
                          </StyledCheckbox>
                        </CheckboxContainer>
                      </FieldLabel>
                    </Form.Item>
                  </Col>
                  {screenOutSettings && screenOutSettings.reject && (
                    <Col xs={{ span: 8 }}>
                      <Form.Item
                        help={errors.debounce}
                        validateStatus={errors.debounce ? 'error' : 'success'}
                      >
                        <Input
                          label={'Reject after step'}
                          type="number"
                          size="default"
                          value={screenOutSettings.rejectAfterStep}
                          onChange={(e) => {
                            handleScreenOutSettings({
                              ...screenOutSettings,
                              rejectAfterStep: e.target.value
                            })
                          }}
                        />
                      </Form.Item>
                    </Col>
                  )}
                </React.Fragment>
              )}
            </Row>
          )}
          <Row gutter={24}>
            <Col xs={{ span: 12 }}>
              <Form.Item>
                <FieldLabel label="Show internal name in reports">
                  <CheckboxContainer>
                    <StyledCheckbox
                      checked={showInternalNameInReports}
                      onChange={(e) => {
                        handleShowInternalNameInReports(e.target.checked)
                      }}
                    >
                      {t('surveySettings.valdatedChartData.yes')}
                    </StyledCheckbox>
                  </CheckboxContainer>
                </FieldLabel>
              </Form.Item>
            </Col>
            {isFlavorwiki && isFlavorwikiOperator && (
              <Col xs={{ span: 12 }}>
                <Form.Item>
                  <FieldLabel label={'Dual currency'}>
                    <CheckboxContainer>
                      <StyledCheckbox
                        checked={dualCurrency}
                        onChange={(e) => {
                          handleCurrencySurvey(e.target.checked)
                        }}
                      >
                        {t('surveySettings.valdatedChartData.yes')}
                      </StyledCheckbox>
                    </CheckboxContainer>
                  </FieldLabel>
                </Form.Item>
              </Col>
            )}
          </Row>
          <Row gutter={24}>
            <Col xs={{ span: 8 }}>
              <Form.Item>
                <FieldLabel
                  label={t('surveySettings.promotionSettings.active')}
                >
                  <CheckboxContainer>
                    <StyledCheckbox
                      checked={promotionSettings.active}
                      onChange={(e) => {
                        setFieldValue('promotionActive', e.target.checked)
                        onChangePromotionSettings({
                          ...promotionSettings,
                          active: e.target.checked
                        })
                      }}
                    >
                      {t('surveySettings.promotionSettings.enable')}
                    </StyledCheckbox>
                  </CheckboxContainer>
                </FieldLabel>
              </Form.Item>
            </Col>

            {promotionSettings.active && (
              <React.Fragment>
                <Col xs={{ span: 8 }}>
                  <Form.Item>
                    <FieldLabel
                      label={t(
                        'surveySettings.promotionSettings.showAfterTaster'
                      )}
                    >
                      <CheckboxContainer>
                        <StyledCheckbox
                          checked={promotionSettings.showAfterTaster}
                          onChange={(e) => {
                            onChangePromotionSettings({
                              ...promotionSettings,
                              showAfterTaster: e.target.checked
                            })
                          }}
                        >
                          {t('surveySettings.promotionSettings.yes')}
                        </StyledCheckbox>
                      </CheckboxContainer>
                    </FieldLabel>
                  </Form.Item>
                </Col>
                <Col xs={{ span: 8 }}>
                  <Form.Item
                    help={errors.promotionCode}
                    validateStatus={errors.promotionCode ? 'error' : 'success'}
                  >
                    <Input
                      label={t('surveySettings.promotionSettings.code')}
                      size="default"
                      value={promotionSettings.code}
                      onChange={(e) => {
                        setFieldValue('promotionCode', e.target.value)
                        onChangePromotionSettings({
                          ...promotionSettings,
                          code: e.target.value
                        })
                      }}
                    />
                  </Form.Item>
                </Col>
              </React.Fragment>
            )}
          </Row>
          {promotionSettings.active && (
            <Row>
              <PromotionOptions
                minOptions={1}
                maxOptions={3}
                label={'Promotion Products'}
                options={
                  values.promotionOptions && values.promotionOptions.promoProducts
                }
                onChangeOptions={(updatedOptions) => {
                  const change = {
                    promotionOptions: {
                      ...promotionOptions,
                      promoProducts: updatedOptions
                    }
                  }
                  setFieldValue('promotionOptions', change.promotionOptions)
                  onChangePromotionOptions(change.promotionOptions)
                }}
                errors={
                  errors.promotionOptions && errors.promotionOptions.promoProducts
                }
                addAnswerLabel={'Add Product'}
              />
            </Row>
          )}
        </Container>
      )}
    </Formik>
  )
}

export default AutoAdvanceSettings
