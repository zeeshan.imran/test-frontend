import React, { useMemo } from 'react'
import { type, isNil, insert } from 'ramda'
import { Icon, Row, Col, Form } from 'antd'
import Input from '../Input'
import Button from '../Button'
import { AddOptionButtonContainer, LabelContainer, Label, ProductUrlField } from './styles'
import { useTranslation } from 'react-i18next'
import HelperIcon from '../HelperIcon'

const PromotionOptions = ({
  label,
  options = [],
  onChangeOptions,
  errors,
  minOptions = 1,
  maxOptions = null,
  addAnswerLabel
}) => {
  const { t } = useTranslation()
  const { canAdd, canRemove } = useMemo(() => {
    return {
      canAdd: maxOptions && options.length < maxOptions,
      canRemove: options.length > minOptions
    }
  }, [options, minOptions, maxOptions])
  return (
    <React.Fragment>
      <Row gutter={24}>
        <Col lg={16} md={24}>
          <LabelContainer
            help={type(errors) === 'String' && errors}
            validateStatus={
              type(errors) === 'String' && errors ? 'error' : 'success'
            }
          >
            <Label>{label}</Label>
          </LabelContainer>
        </Col>
      </Row>

      {options.map((option, index) => {
        if (option.isOpenAnswer) {
          return null
        }
        const isStrictMode = option.editMode === 'strict'
        const url = option.url ? option.url.replace('https://', '') : ''
        return (
          <React.Fragment key={index}>
            <Row gutter={24}>
              <Col xs={10} xl={8}>
                <Form.Item
                  help={
                    type(errors) === 'Array' && !isNil(errors[index])
                      ? errors[index].label
                      : null
                  }
                  validateStatus={
                    (errors && errors[index] && errors[index].label) ||
                    (errors && type(errors) === 'String')
                      ? 'error'
                      : 'success'
                  }
                >
                  <Input
                    required
                    name={`${index}-label`}
                    size="default"
                    placeholder={'Product name'}
                    value={option.label}
                    onChange={(event) => {
                      const updatedOptions = [
                        ...options.slice(0, index),
                        { ...options[index], label: event.target.value },
                        ...options.slice(index + 1)
                      ]
                      onChangeOptions(updatedOptions, event.target.value)
                    }}
                  />
                </Form.Item>
              </Col>

              <Col xs={10} xl={8}>
                <Form.Item
                  help={
                    type(errors) === 'Array' && !isNil(errors[index])
                      ? errors[index].url
                      : null
                  }
                  validateStatus={
                    (errors && errors[index] && errors[index].url) ||
                    (errors && type(errors) === 'String')
                      ? 'error'
                      : 'success'
                  }
                >
                  <ProductUrlField
                    required
                    name={`${index}-url`}
                    size="default"
                    prefix="https://"
                    placeholder="example.com/product"
                    value={url}
                    onChange={(event) => {
                      const newUrl = `https://${event.target.value}`
                      const updatedOptions = [
                        ...options.slice(0, index),
                        {
                          ...options[index],
                          url: newUrl
                        },
                        ...options.slice(index + 1)
                      ]
                      onChangeOptions(updatedOptions, newUrl)
                    }}
                  />
                </Form.Item>
              </Col>

              {isStrictMode && (
                <Col xs={5} xl={4} flex="auto">
                  <HelperIcon
                    placement="bottomRight"
                    helperText={t('tooltips.strictOptions')}
                  />
                </Col>
              )}

              {canRemove && (
                <Col xs={5} xl={4} flex="auto">
                  <Button
                    size="default"
                    type="red"
                    onClick={() => {
                      onChangeOptions([
                        ...options.slice(0, index),
                        ...options.slice(index + 1)
                      ])
                    }}
                  >
                    <Icon type="close" />
                  </Button>
                </Col>
              )}
            </Row>
          </React.Fragment>
        )
      })}

      <AddOptionButtonContainer>
        <Row>
          <Col lg={8} md={8}>
            <Button
              disabled={!canAdd}
              onClick={() => {
                onChangeOptions(
                  insert(
                    options.length -
                      (options[options.length - 1] &&
                      options[options.length - 1].isOpenAnswer
                        ? 1
                        : 0),
                    { label: null, url: null, __typename: 'PromotionProduct' },
                    options
                  )
                )
              }}
            >
              {addAnswerLabel ||
                t('components.questionCreation.optionsSection.addAnswer')}
            </Button>
          </Col>
        </Row>
      </AddOptionButtonContainer>
    </React.Fragment>
  )
}

export default PromotionOptions
