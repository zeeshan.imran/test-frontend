import React from 'react'
import { Card, Col } from 'antd'
import IconButton from '../IconButton'
import {
  ProductsRow,
  Container,
  Gallery,
  PhotoSection,
  IconContainer,
  Image,
  FieldText
} from './styles'

const StatsProductSection = ({
  products,
  selectedProducts,
  toggleProductSeletion
}) => {
  return (
    <React.Fragment>
      <ProductsRow>
        <Col span={24}>
          <Card>
            <Container>
              <Gallery>
                {products.map((singleProduct, index) => {
                  return (
                    <PhotoSection
                      key={index}
                      onClick={() => toggleProductSeletion(singleProduct.id)}
                    >
                      {selectedProducts.indexOf(singleProduct.id) >= 0 && (
                        <IconContainer>
                          <IconButton
                            tooltip={singleProduct.name}
                            type={`check-circle`}
                            size={`2.2rem`}
                          />
                        </IconContainer>
                      )}
                      {singleProduct.photo ? (
                        <React.Fragment>
                          <Image src={singleProduct.photo} title={singleProduct.name} />
                          <FieldText>{singleProduct.name}</FieldText>
                          <FieldText>({singleProduct.id})</FieldText>
                        </React.Fragment>
                      ) : (
                        <FieldText>{singleProduct.name}</FieldText>
                      )}
                    </PhotoSection>
                  )
                })}
              </Gallery>
            </Container>
          </Card>
        </Col>
      </ProductsRow>
    </React.Fragment>
  )
}

export default StatsProductSection
