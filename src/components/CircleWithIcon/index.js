import React from 'react'
import { Shape } from './styles'

const CircleWithIcon = ({ backgroundColor, children }) => (
  <Shape backgroundColor={backgroundColor}>{children}</Shape>
)

export default CircleWithIcon
