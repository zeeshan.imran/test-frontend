import styled from 'styled-components'

export const Shape = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 3.5rem;
  height: 3.5rem;
  border-radius: 50%;
  margin-right: 1.5rem;
  background-color: ${props => props.backgroundColor};
`
