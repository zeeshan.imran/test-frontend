import React, { useState } from 'react'
import { Modal } from 'antd'
import Select from '../Select'
import { useTranslation } from 'react-i18next'
import { options } from '../../utils/internationalization/index'
import { getOptionValue } from '../../utils/getters/OptionGetters'
import Button from '../LoginForm/Button/index'
import useUpdateLocale from '../../hooks/useUpdateSurveyLanguage'

const SelectLanguage = ({ showModal, setShowModal }) => {
  const { t } = useTranslation()
  const updateLocale = useUpdateLocale()
  const [language, setLanguage] = useState('')
  const setLanguageChange = value => {
    setLanguage(value)
  }
  const closeModal = () => {
    if (language !== '') {
      setShowModal(false)
      updateLocale(language)
    }
  }
  return (
    <Modal
      title={t(`components.createTasterAccount.forms.second.langugage.placeholder`)}
      visible={showModal}
      onOk={closeModal}
      okButtonProps={{ disabled: language === '' }}
      maskClosable={false}
      closable={false}
      centered
      footer={[
        <Button
          key='submit'
          type='primary'
          onClick={closeModal}
          size={'default'}
          disabled={language === ''}
        >
          {t('components.tasterProfile.submitButton')}
        </Button>
      ]}
    >
      <Select
        name='Language'
        label={t(`components.createTasterAccount.forms.second.langugage.label`)}
        size={`default`}
        value={language}
        onChange={type => {
          setLanguageChange(type)
        }}
        placeholder={t(
          `components.createTasterAccount.forms.second.langugage.placeholder`
        )}
        options={options}
        getOptionValue={getOptionValue}
        getOptionName={(option = {}) =>
          t(option.label) ? t(option.label) : option.label
        }
      />
    </Modal>
  )
}
export default SelectLanguage
