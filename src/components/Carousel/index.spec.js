import React from 'react'
import { shallow } from 'enzyme'
import Carousel from '.'
import firstCarouselImage from '../../assets/pictures/onboarding/1.png'
import thirdCarouselImage from '../../assets/pictures/onboarding/3.png'

describe('Carousel', () => {
  let testRender
    let slides
    let renderSlideBottomContent
    let renderSlideTopContent

  beforeEach(() => {
    slides = [firstCarouselImage, thirdCarouselImage, 'withText']
    renderSlideBottomContent = jest.fn()
    renderSlideTopContent =  jest.fn()

  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Carousel', async () => {
    testRender = shallow(
      <Carousel
        slides={slides}
        renderSlideBottomContent={renderSlideBottomContent}
        renderSlideTopContent={renderSlideTopContent}
      />
    )
    expect(testRender).toMatchSnapshot()
  })
})
