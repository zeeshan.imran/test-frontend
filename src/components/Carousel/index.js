import React from 'react'
import LibCarousel from 'nuka-carousel'
import { CAROUSEL_AUTOPLAY_INTERVAL } from '../../utils/Constants'
import Dots from './Dots'
import { SlideContainer, TopLayer, Overlay } from './styles'

const Carousel = ({
  slides,
  renderSlideBottomContent,
  renderSlideTopContent,
  afterSlide
}) => (
  <LibCarousel
    afterSlide={afterSlide}
    wrapAround
    autoplay
    autoplayInterval={CAROUSEL_AUTOPLAY_INTERVAL}
    renderBottomCenterControls={({ currentSlide, goToSlide, slideCount }) => (
      <Dots
        currentSlide={currentSlide}
        goToSlide={goToSlide}
        slideCount={slideCount}
      />
    )}
    renderCenterLeftControls={() => null}
    renderCenterRightControls={() => null}
  >
    {slides.map((slide, slideIndex) => {
      return (
        <SlideContainer key={slideIndex}>
          {renderSlideTopContent && (
            <TopLayer>{renderSlideTopContent(slide, slideIndex)}</TopLayer>
          )}
          <Overlay />
          {renderSlideBottomContent(slide)}
        </SlideContainer>
      )
    })}
  </LibCarousel>
)

export default Carousel
