import styled from 'styled-components'

export const SlideContainer = styled.div`
  position: relative;
  z-index: 0;
`

export const TopLayer = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  z-index: 1;
`

export const Overlay = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.2);
`
