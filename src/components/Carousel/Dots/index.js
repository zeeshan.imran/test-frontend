import React from 'react'
import { Container, Dot } from './styles'

const Dots = ({ currentSlide, goToSlide, slideCount }) => {
  return (
    <Container>
      {Array.from(Array(slideCount), (slide, index) => (
        <Dot
          key={index}
          last={index === slideCount - 1}
          active={index === currentSlide}
          onClick={() => goToSlide(index)}
        />
      ))}
    </Container>
  )
}

export default Dots
