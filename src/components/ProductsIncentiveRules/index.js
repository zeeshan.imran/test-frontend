import React, { useEffect, useRef } from 'react'
import { useTranslation } from 'react-i18next'
import { Container, PrefixedInput } from './styles'
import Input from '../Input'
import { Form, Col } from 'antd'
import { Formik } from 'formik'
import { PUBSUB } from '../../utils/Constants'
import PubSub from 'pubsub-js'
import validationSchema from '../../validates/productsIncentiveRule'
import defaults from '../../defaults'

const ProductsIncentiveRules = ({
  min,
  max,
  percentage,
  onChange,
  prefix,
  suffix,
  active
}) => {
  const formRef = useRef()
  const { t } = useTranslation()

  useEffect(() => {
    const token = PubSub.subscribe(PUBSUB.VALIDATE_SURVEY_PRODUCTS, () => {
      if (formRef.current) {
        formRef.current.validateForm()
      }
    })
    return () => {
      PubSub.unsubscribe(token)
    }
  }, [])

  return (
    <Formik
      ref={formRef}
      validationSchema={validationSchema(active)}
      initialValues={{
        min,
        max,
        percentage
      }}
      render={({ values, errors, setFieldValue }) => {
        const {
          min: minError,
          max: maxError,
          percentage: percentageError
        } = errors
        return (
          <Container gutter={24} style={{margin: '0px', marginBottom: '2.5rem'}}>
            <Col xs={8}>
              <Form.Item
                help={minError}
                validateStatus={minError ? 'error' : 'success'}
              >
                <PrefixedInput
                  largePrefix={
                    (prefix && prefix.length > 2) ||
                    (suffix && suffix.length > 2)
                  }
                >
                  <Input
                    min={0}
                    max={values.max}
                    precision={2}
                    required
                    name='min'
                    value={values.min}
                    onChange={event => {
                      const newMin = parseFloat(event.target.value, 10)
                      setFieldValue('min', newMin)
                      onChange({ min: newMin })
                    }}
                    label={t('components.product.incentiveRule.min')}
                    tooltip={t('tooltips.product.incentiveRule.min')}
                    size='default'
                    prefix={prefix || suffix}
                    type={`number`}
                  />
                </PrefixedInput>
              </Form.Item>
            </Col>
            <Col xs={8}>
              <Form.Item
                help={percentageError}
                validateStatus={percentageError ? 'error' : 'success'}
              >
                <PrefixedInput largePrefix='%'>
                  <Input
                    min={0}
                    max={100}
                    precision={2}
                    required
                    name='brand'
                    value={values.percentage}
                    onChange={event => {
                      const newPercentage = parseFloat(event.target.value, 10)
                      setFieldValue('percentage', newPercentage)
                      onChange({ percentage: newPercentage })
                    }}
                    label={t('components.product.incentiveRule.percentage')}
                    tooltip={t('tooltips.product.incentiveRule.percentage')}
                    size='default'
                    prefix='%'
                    type={`number`}
                  />
                </PrefixedInput>
              </Form.Item>
            </Col>
            <Col xs={8}>
              <Form.Item
                help={maxError}
                validateStatus={maxError ? 'error' : 'success'}
              >
                <PrefixedInput
                  largePrefix={
                    (prefix && prefix.length > 2) ||
                    (suffix && suffix.length > 2)
                  }
                >
                  <Input
                    min={0}
                    max={defaults.MAX_LIMIT_REFERRAL}
                    precision={2}
                    required
                    name='max'
                    value={values.max}
                    onChange={event => {
                      const newMax = parseFloat(event.target.value, 10)
                      setFieldValue('max', newMax)
                      onChange({ max: newMax })
                    }}
                    label={t('components.product.incentiveRule.max')}
                    tooltip={t('tooltips.product.incentiveRule.max')}
                    size='default'
                    prefix={prefix || suffix}
                    type={`number`}
                  />
                </PrefixedInput>
              </Form.Item>
            </Col>
          </Container>
        )
      }}
    />
  )
}

export default ProductsIncentiveRules
