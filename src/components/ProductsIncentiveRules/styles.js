import styled from 'styled-components'
import colors from '../../utils/Colors'
import { Row } from 'antd'

export const PrefixedInput = styled.div`
  .ant-input-number-input {
    ${props => (props.largePrefix ? 'padding-left: 46px' : '')}
  }
`

export const Container = styled(Row)`
  background-color: ${colors.WHITE};
  padding: 2.5rem 3.5rem;
  margin-bottom: 2.5rem;
`
