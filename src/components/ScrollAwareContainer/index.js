import { Component } from 'react'
import PropTypes from 'prop-types'

class ScrollAwareContainer extends Component {
  state = {
    scrollY: 0
  }

  handleScroll = () => {
    const { threshold } = this.props
    const currentScrollY = window.scrollY

    if (currentScrollY > threshold) return this.setState({ scrollY: threshold })
    this.setState({ scrollY: currentScrollY })
  }

  componentDidMount () {
    window.addEventListener('scroll', this.handleScroll)
  }

  componentWillUnmount () {
    window.removeEventListener('scroll', this.handleScroll)
  }

  scrollToTop = () => {
    window.scrollTo(0, 0)
  }

  render () {
    return this.props.children({
      scrollY: this.state.scrollY,
      scrollToTop: this.scrollToTop
    })
  }
}

ScrollAwareContainer.propTypes = {
  threshold: PropTypes.number
}

export default ScrollAwareContainer
