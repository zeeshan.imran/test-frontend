import kebabCase from 'lodash.kebabcase'
import React, { useState, useEffect } from 'react'
import { Row, Col, Form, Radio,Input as AntInput, Select, Icon } from 'antd'
import * as Yup from 'yup'
import { CopyToClipboard as Copy } from 'react-copy-to-clipboard'
import {
  Container,
  QrCodeSection,
  FormHeader,
  FormWrapper,
  Title,
  AddOnFieldLabel,
  NotFound,
  PlaceHolder,
  CopyButton,
  SurveyStatus,
  Visibility,
  RadioButton
} from './styles'
import Button from '../../components/Button'
import Input from '../../components/Input'
import { Formik } from 'formik'
import Label from '../../components/FieldLabel/Label'
import { useTranslation } from 'react-i18next'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import AlertModal from '../AlertModal'
import HelperIcon from '../HelperIcon'

const OperatorQrCodeForm = ({
  isNewQrCode,
  i18nPrefix,
  saving,
  loadingSurveys,
  title,
  surveys,
  values: initialValues = {
    name: '',
    uniqueName: '',
    targetType: 'link',
    qrCodePhoto: '',
    survey: '',
    targetLink: ''
  },
  onSearch,
  onDownload,
  onSubmit,
  onCancel,
  organizationUniqueName,
  uniqueNameDisable
}) => {
  const { t } = useTranslation()
  const [isUniqueNameTouched, setIsUniqueNameTouched] = useState(false)

  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .trim()
      .required(t('components.operatorQrCodeForm.validate.name.required')),
    uniqueName: Yup.string()
      .required(t('components.operatorQrCodeForm.validate.uniqueName.required'))
      .test(
        'not-empty',
        t('components.operatorQrCodeForm.validate.uniqueName.required'),
        value => !/^[ \t\r\n]+$/.test(value)
      )
      .max(128, t('components.operatorQrCodeForm.validate.uniqueName.max'))
      .matches(
        /^[0-9a-zA-Z-]+$/,
        t('components.operatorQrCodeForm.validate.uniqueName.matches')
      ),
    survey: Yup.string()
      .nullable()
      .when('targetType', {
        is: 'survey',
        then: Yup.string().required(
          t('components.operatorQrCodeForm.validate.survey.required')
        )
      }),
    targetLink: Yup.string()
      .nullable()
      .when('targetType', {
        is: 'link',
        then: Yup.string()
          .url(t('components.operatorQrCodeForm.validate.targetLink.urlValid'))
          .required(
            t('components.operatorQrCodeForm.validate.targetLink.required')
          )
      })
  })

  const STATE_ELEMENTS = {
    active: null,
    draft: (
      <SurveyStatus color='rgba(0,0,0,0.45)'>
        {t('components.operatorQrCodeForm.draft')}
      </SurveyStatus>
    ),
    suspended: (
      <SurveyStatus color='#faad14'>
        {t('components.operatorQrCodeForm.draft')}
      </SurveyStatus>
    )
  }

  useEffect(() => {
    setIsUniqueNameTouched(false)
  }, [initialValues, initialValues.uniqueName])

  const confirmUniqueName = onChangeUniqueName => {
    if (!isNewQrCode && !isUniqueNameTouched) {
      AlertModal({
        title: t('components.operatorQrCodeForm.alert.title'),
        description: t('components.operatorQrCodeForm.alert.description'),
        okText: t('components.operatorQrCodeForm.alert.okText'),
        handleOk: onChangeUniqueName
      })
    } else {
      onChangeUniqueName()
    }
  }

  return (
    <Formik
      validationSchema={validationSchema}
      initialValues={initialValues}
      onSubmit={onSubmit}
    >
      {({
        errors,
        touched,
        values,
        setFieldValue,
        handleChange,
        handleBlur,
        handleSubmit
      }) => {
        const getItemProps = name => {
          const error = touched[name] && errors[name]
          return {
            help: error,
            validateStatus: error ? 'error' : 'success'
          }
        }

        const redirectLink = `${window.location.origin}/qr-code/${organizationUniqueName}/${values.uniqueName}`

        const isLinkChanged = !(
          initialValues.uniqueName &&
          initialValues.uniqueName === values.uniqueName
        )

        const isLinkValid = values.uniqueName && !errors.uniqueName

        return (
          <Container>
            <FormHeader>
              <Title>{title}</Title>
              <Button type='secondary' size='default' onClick={onCancel}>
                {t('components.operatorQrCodeForm.cancel')}
              </Button>
              <Button
                data-testid='save'
                loading={saving}
                size='default'
                onClick={handleSubmit}
              >
                {t(`${i18nPrefix}.save`)}
              </Button>
            </FormHeader>
            <FormWrapper>
              <QrCodeSection
                className={isLinkChanged ? 'outdated' : ''}
                data-testid='qr-code'
              >
                {initialValues.qrCodePhoto ? (
                  <div
                    className='qr-code'
                    style={{
                      backgroundImage: `url(${initialValues.qrCodePhoto})`
                    }}
                    onClick={() => onDownload(initialValues)}
                  >
                    <span className='download'>
                      {t('components.operatorQrCodeForm.download')}
                    </span>
                  </div>
                ) : (
                  <PlaceHolder>
                    <Icon type='qrcode' />
                  </PlaceHolder>
                )}
                <span className='outdated-note'>
                  {t('components.operatorQrCodeForm.generateQrCodeNotice')}
                </span>
              </QrCodeSection>
              <Row gutter={24}>
                <Col xs={{ span: 12 }}>
                  <Form.Item {...getItemProps('name')}>
                    <Input
                      autoComplete='off'
                      size='default'
                      label={t('components.operatorQrCodeForm.name')}
                      name='name'
                      required
                      value={values.name}
                      onBlur={handleBlur}
                      onChange={e => {
                        if (isNewQrCode) {
                          setFieldValue('uniqueName', kebabCase(e.target.value))
                        }

                        handleChange(e)
                      }}
                    />
                  </Form.Item>
                </Col>
                <Col xs={{ span: 12 }}>
                  <Form.Item {...getItemProps('uniqueName')}>
                    <Input
                      disabled={uniqueNameDisable}
                      suffix={
                        uniqueNameDisable && (
                          <HelperIcon
                            helperText={t('components.operatorQrCodeForm.uniqueNameHelper')}
                          />
                        )
                      }
                      size='default'
                      label={t('components.operatorQrCodeForm.uniqueName')}
                      name='uniqueName'
                      required
                      value={values.uniqueName}
                      onBlur={handleBlur}
                      onChange={e => {
                        const newUniqueName = e.target.value

                        confirmUniqueName(() => {
                          setFieldValue('uniqueName', newUniqueName)
                          setIsUniqueNameTouched(true)
                        })
                      }}
                    />
                  </Form.Item>
                </Col>
                <Col xs={{ span: 24 }}>
                  <Visibility isVisible={isLinkValid}>
                    <Form.Item
                      label='The redirect link:'
                      help={
                        isLinkChanged &&
                        t('components.operatorQrCodeForm.validate.redirectLink')
                      }
                      validateStatus='validating'
                    >
                      <Input
                        disabled
                        size='default'
                        data-testid='redirect-link'
                        value={redirectLink}
                        addonAfter={
                          !isLinkChanged && (
                            <Copy
                              text={redirectLink}
                              onCopy={() => {
                                displaySuccessMessage('Copied')
                              }}
                            >
                              <CopyButton>
                                <Icon type='copy' />{' '}
                                {t('components.operatorQrCodeForm.copy')}
                              </CopyButton>
                            </Copy>
                          )
                        }
                      />
                    </Form.Item>
                  </Visibility>
                </Col>
                <Col xs={{ span: 24 }}>
                  <AddOnFieldLabel>
                    <Label required label='Target Options' />
                    <Radio.Group
                      name='targetType'
                      value={values.targetType}
                      onBlur={handleBlur}
                      onChange={e => {
                        handleChange(e)
                        if (e.target.value === 'survey') {
                          setFieldValue('targetLink', null)
                        }
                        if (e.target.value === 'link') {
                          setFieldValue('survey', null)
                        }
                      }}
                    >
                      <RadioButton value='survey'>
                        {t('components.operatorQrCodeForm.linkToSurvey')}
                      </RadioButton>
                      <RadioButton value='link'>
                        {t('components.operatorQrCodeForm.linkToUrl')}
                      </RadioButton>
                    </Radio.Group>
                  </AddOnFieldLabel>
                  {values.targetType === 'survey' && (
                    <Form.Item {...getItemProps('survey')}>
                      <Select
                        name='survey'
                        autoClearSearchValue={false}
                        loading={loadingSurveys}
                        style={{ width: '100%' }}
                        showSearch
                        value={values.survey}
                        defaultActiveFirstOption={false}
                        showArrow
                        filterOption={false}
                        onSearch={onSearch}
                        onBlur={e => {
                          handleBlur(e)
                          onSearch('')
                        }}
                        onChange={value => {
                          setFieldValue('survey', value)
                          onSearch('')
                        }}
                        notFoundContent={
                          <NotFound>
                            {t('components.operatorQrCodeForm.notFound')}
                          </NotFound>
                        }
                      >
                        {surveys.map(s => (
                          <Select.Option
                            key={s.uniqueName}
                            value={s.uniqueName}
                          >
                            {s.name} ({s.uniqueName}) {STATE_ELEMENTS[s.state]}
                          </Select.Option>
                        ))}
                      </Select>
                    </Form.Item>
                  )}
                  {values.targetType === 'link' && (
                    <Form.Item {...getItemProps('targetLink')}>
                      <AntInput
                        size='default'
                        name='targetLink'
                        value={values.targetLink}
                        onBlur={handleBlur}
                        onChange={handleChange}
                      />
                    </Form.Item>
                  )}
                </Col>
              </Row>
            </FormWrapper>
          </Container>
        )
      }}
    </Formik>
  )
}

export default OperatorQrCodeForm
