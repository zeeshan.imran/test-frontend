import React, { Component } from 'react'
import { Modal } from './styles'
import SelectCategory from './SelectCategory'
import SearchBar from './SearchBar'
import ProductsTable from './ProductsTable'

import { withTranslation } from 'react-i18next'

class ProductsSelectModal extends Component {
  state = {
    selectedCategory: '',
    selectedProducts: []
  }

  render () {
    const {
      visible,
      handleOk,
      handleCancel,
      search,
      handleSearch,
      categories,
      products,
      handleItemAction
    } = this.props
    const { selectedCategory } = this.state
    const { t } = this.props

    return (
      <Modal
        title={t('components.products.addProducts')} 
        visible={visible}
        onOk={handleOk}
        okText={t('components.products.apply')} 
        onCancel={handleCancel}
      >
        <SelectCategory
          categories={categories}
          selectedCategory={selectedCategory}
          handleSelectCategory={selectedCategory =>
            this.setState({ selectedCategory })
          }
        />
        <SearchBar value={search} handleChange={handleSearch} />
        <ProductsTable
          products={products}
          handleSelection={selectedProducts =>
            this.setState({ selectedProducts })
          }
          handleItemAction={handleItemAction}
        />
      </Modal>
    )
  }
}

export default withTranslation()(ProductsSelectModal)
