import React from 'react'
import { Row, Col, Form } from 'antd'
import Input from '../Input'
import {
  Container,
  CurrentPassword,
  ButtonAligner,
  CustomCol,
  StyledButton
} from './styles'
import { useTranslation } from 'react-i18next'

const ChangeUserPassword = ({
  currentPassword,
  newPassword,
  confirmNewPassword,
  handleSubmit,
  fieldChangeHandler,
  errors,
  touched,
  isValid
}) => {
  const { t } = useTranslation()
  const getFormItemProps = name => {
    return {
      help: touched[name] && errors[name],
      validateStatus: touched[name] && errors[name] ? 'error' : 'success'
    }
  }
  return (
    <Container>
      <Row type='flex' justify='center'>
        <Col xs={{ span: 24 }} lg={{ span: 16 }}>
          <Row type='flex' justify='center'>
            <Col xs={{ span: 24 }}>
              <CurrentPassword>
                <Form.Item {...getFormItemProps('currentPassword')}>
                  <Input
                    type={'password'}
                    placeholder={t('placeholders.currentPassword')}
                    label={t('components.changePassword.currentPassword')}
                    name={`currentPassword`}
                    value={currentPassword}
                    onChange={fieldChangeHandler('currentPassword')}
                  />
                </Form.Item>
              </CurrentPassword>

              <Form.Item {...getFormItemProps('newPassword')}>
                <Input
                  type={'password'}
                  placeholder={t('placeholders.newPassword')}
                  label={t('components.changePassword.newPassword')}
                  name={`newPassword`}
                  value={newPassword}
                  onChange={fieldChangeHandler('newPassword')}
                />
              </Form.Item>

              <Form.Item {...getFormItemProps('confirmNewPassword')}>
                <Input
                  type={'password'}
                  placeholder={t('placeholders.confirmPassword')}
                  label={t('components.changePassword.confirmPassword')}
                  name={`confirmNewPassword`}
                  value={confirmNewPassword}
                  onChange={fieldChangeHandler('confirmNewPassword')}
                />
              </Form.Item>

              <ButtonAligner>
                <Row gutter={24} type='flex' justify='center'>
                  <CustomCol desktop xs={{ span: 24 }} xl={{ span: 12 }}>
                    <StyledButton
                      htmlType='button'
                      type={isValid ? 'primary' : 'disabled'}
                      onClick={async () => {
                        await handleSubmit()
                      }}
                    >
                      {t('components.changePassword.savePasswordButton')}
                    </StyledButton>
                  </CustomCol>
                  <Col xs={{ span: 24 }} xl={{ span: 12 }}>
                    <StyledButton type='secondary'>
                      {t('components.changePassword.forgotPasswordButton')}
                    </StyledButton>
                  </Col>
                </Row>
              </ButtonAligner>
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  )
}

export default ChangeUserPassword
