import React from 'react'
import { mount } from 'enzyme'
import QuestionCreationCardTypeSelect from '.'
import PubSub from 'pubsub-js'
import { PUBSUB } from '../../utils/Constants'

jest.mock('pubsub-js', () => ({
  subscribe: (e, fn) => fn({}, { code: 'de', messages: 'someMessages' }),
  unsubscribe: jest.fn()
}))

jest.mock('i18next', () => ({
  exists: val => !!val,
  getFixedT: () => () => ''
})) 

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('QuestionCreationCardTypeSelect', () => {
  let testRender
  let disabled
  let types
  let type
  let existingUniqueTypes
  let onSelect
  const setState = jest.fn()
  const useStateSpy = jest.spyOn(React, 'useState')
  useStateSpy.mockImplementation(init => [init || 'error', setState])

  beforeEach(() => {
    disabled = false
    types = ['sm-card', 'lg-card']
    type = 'sm-card'
    existingUniqueTypes = ''
    onSelect = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render QuestionCreationCardTypeSelect', async () => {
    testRender = mount(
      <QuestionCreationCardTypeSelect
        disabled={disabled}
        types={types}
        type={type}
        existingUniqueTypes={existingUniqueTypes}
        onSelect={onSelect}
      />
    )
    expect(testRender.find(QuestionCreationCardTypeSelect)).toHaveLength(1)
  })

  test('should render QuestionCreationCardTypeSelect onclick', async () => {
    testRender = mount(
      <QuestionCreationCardTypeSelect
        disabled={false}
        options={['text', 'paired']}
        onSelect={onSelect}
      />
    )

    testRender
      .findWhere(c => c.name() === 'Select')
      .first()
      .prop('onChange')({ target: 'paired' })

    expect(onSelect).toHaveBeenCalledWith({ target: 'paired' })
  })

  test('should render QuestionCreationCardTypeSelect onclick', async () => {
    testRender = mount(
      <QuestionCreationCardTypeSelect
        disabled={false}
        options={['text', 'paired']}
        onSelect={onSelect}
      />
    )

    testRender
      .findWhere(c => c.name() === 'Select')
      .first()
      .prop('onChange')({ target: '' })

    expect(onSelect).toHaveBeenCalled()
  })

  test('should unset error when selecting the type', () => {
    testRender = mount(
      <QuestionCreationCardTypeSelect
        disabled={false}
        options={['text', 'paired']}
        onSelect={onSelect}
      />
    )
    setState.mockClear()
    testRender
      .find('Select')
      .first()
      .prop('onChange')('change')
    expect(onSelect).toHaveBeenCalledWith('change')
    expect(setState).toHaveBeenCalled()
  })

  test('should set error on PubSub event', () => {
    testRender = mount(
      <QuestionCreationCardTypeSelect
        disabled={false}
        options={['text', 'paired']}
        onSelect={onSelect}
      />
    )
    PubSub.unsubscribe(PUBSUB.VALIDATE_SURVEY_QUESTIONS)
    expect(setState).toHaveBeenCalled()
  })
})
