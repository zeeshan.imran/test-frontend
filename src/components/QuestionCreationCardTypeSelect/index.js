import React, { memo, useState, useEffect, useCallback } from 'react'
import { Form, Row, Col } from 'antd'
import startCase from 'lodash.startcase'
import capitalize from 'lodash.capitalize'
import PubSub from 'pubsub-js'
import Select from '../Select'
import { PUBSUB } from '../../utils/Constants'
import { exists } from 'i18next'
import { useTranslation } from 'react-i18next'

const QuestionCreationCardTypeSelect = ({
  disabled,
  types,
  type,
  existingUniqueTypes,
  onSelect,
  hasDemographic,
  demographicField,
  onSelectTemplate,
  templates
}) => {
  const [uniqueTypesSet, setSelectedTypesSet] = useState(
    new Set(existingUniqueTypes)
  )
  const [error, setError] = useState(null)
  const { t } = useTranslation()

  const [focusedOption, setFocusedOption] = useState(false)

  useEffect(() => {
    const token = PubSub.subscribe(PUBSUB.VALIDATE_SURVEY_QUESTIONS, () => {
      setError(
        type ? null : t('validation.questionCreation.questionType.required')
      )
    })

    return () => {
      PubSub.unsubscribe(token)
    }
  }, [setError, type])

  useEffect(() => {
    setSelectedTypesSet(new Set(existingUniqueTypes))
  }, [existingUniqueTypes])

  const isDisabledType = useCallback(type => uniqueTypesSet.has(type), [
    uniqueTypesSet
  ])

  return (
    <Row gutter={24}>
      <Col lg={14} xs={24}>
        <Form.Item help={error} validateStatus={error ? 'error' : 'success'}>
          <Select
            focusedOption={focusedOption}
            setFocusedOption={setFocusedOption}
            disabled={disabled}
            data-testid={'question-type-select'}
            name={'questionType'}
            label={t('components.questionCreation.questionType')}
            value={type}
            onChange={change => {
              onSelect(change)
              if (error) {
                setError(false)
              }
            }}
            placeholder={t('placeholders.questionType')}
            options={types}
            isDisabledOption={isDisabledType}
            getOptionName={type =>
              exists(`dropDownOptions.${type}`)
                ? t(`dropDownOptions.${type}`)
                : capitalize(startCase(type))
            }
            size='default'
            sideDetails={true}
          />
        </Form.Item>
      </Col> 
      <Col xs={10}>
        {hasDemographic && (
          <Form.Item help={error} validateStatus={error ? 'error' : 'success'}>
            <Select
              disabled={disabled}
              name={'template'}
              label={t('components.questionCreation.template')}
              value={demographicField}
              onChange={template => {
                onSelectTemplate(template)
              }}
              placeholder={t('components.questionCreation.template')}
              options={templates}
              isDisabledOption={isDisabledType}
              getOptionName={template =>
                template && template.data && template.data.prompt
              }
              getOptionValue={template => template && template.name}
              size='default'
              sideDetails
            />
          </Form.Item>
        )}
      </Col>
    </Row>
  )
}

export default memo(QuestionCreationCardTypeSelect)
