import React, { Component } from 'react'
import { storiesOf } from '@storybook/react'
import UsersTable from '.'

class TableWithState extends Component {
  state = {
    selected: []
  }

  render () {
    return (
      <UsersTable
        users={[
          {
            id: 'a',
            firstName: 'hello',
            lastName: 'last',
            email: 'hum@hum.com',
            gender: 'helicopter',
            surveys: [{ id: 'a' }, { id: 'b' }],
            createdAt: '2018-10-01'
          },
          {
            id: 'b',
            firstName: 'another',
            lastName: 'last',
            email: 'test@test.com',
            gender: 'male',
            surveys: [{ id: 'ab' }, { id: 'cd' }],
            createdAt: '2018-11-01'
          }
        ]}
        selected={this.state.selected}
        handleChangeSelection={selected => this.setState({ selected })}
        handleEdit={targetUserId =>
          console.log('would navigate to edit page for:', targetUserId)
        }
        handleRemove={targetUser => console.log('would remove:', targetUser)}
      />
    )
  }
}

storiesOf('UsersTable', module).add('UsersTable', () => <TableWithState />)
