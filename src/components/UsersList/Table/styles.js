import styled from 'styled-components'
import { DEFAULT_COMPONENTS_MARGIN } from '../../../utils/Metrics'
import { family } from '../../../utils/Fonts'
import { Menu as AntMenu, Icon as AntIcon } from 'antd'
import colors from '../../../utils/Colors'

export const Picture = styled.img`
  height: 3.6rem;
  width: 3.6rem;
  border-radius: 50%;
  margin-right: ${DEFAULT_COMPONENTS_MARGIN}rem;
`

export const ColumnContentAligner = styled.div`
  display: inline-flex;
  align-items: center;
  min-width: ${({ fixed }) => `${fixed ? fixed : 'auto'}`};
`

export const Action = styled.span`
  cursor: pointer;
  color: rgb(24, 144, 255);
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  line-height: 1;
`

export const Separator = styled.div`
  display: inline-block;
  height: 1.4rem;
  width: 0.1rem;
  margin: 0 0.8rem;
  background-color: #e9e9e9;
`
export const Icon = styled(AntIcon)`
  height: 32px;
  width: 32px;
  margin-right: 16px;
  color: rgba(0, 0, 0, 0.65);
`
export const Item = styled(AntMenu.Item)`
  color: ${colors.MODAL_LINK_COLOR};
  &:hover {
    span {
      color: ${colors.MODAL_LINK_HOVER_COLOR};
    }
  }
`
export const ActionsContainer = styled.div`
  display: flex;
  justify-content: flex-end;
`