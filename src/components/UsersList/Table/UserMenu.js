import React, { useState } from 'react'
import IconButton from '../../IconButton'
import { Dropdown, Menu } from 'antd'
import { ActionsContainer, Item } from './styles'
import {
  isUserAuthenticatedAsSuperAdmin,
  isUserAuthenticatedAsPowerUser
} from '../../../utils/userAuthentication'

const UserMenu = ({
  currentUserId,
  handleRemove,
  setModalType,
  toggleEditUserForm,
  rowData,
  blockUser,
  userSurveyEnrollments,
  t
}) => {
  const { REACT_APP_THEME } = process.env
  const showOnFlavorWiki = REACT_APP_THEME === 'default'
  const isSuperAdmin = isUserAuthenticatedAsSuperAdmin()
  const isPowerUser = isUserAuthenticatedAsPowerUser()
  const [openDropdownId, setOpenDropdownId] = useState(false)
  const closeDropdown = () => setOpenDropdownId(false)
  const blackListText = rowData.blackListed
    ? 'Remove from blacklist'
    : 'Add to blacklist'
  let menu
  menu = (
    <React.Fragment>
      <Menu onClick={closeDropdown}>
        {currentUserId !== rowData.id && (
          <Menu.ItemGroup>
            <Item
              onItemHover={() => {}}
              onClick={() => {
                closeDropdown()
                handleRemove(rowData.id)
              }}
              rootPrefixCls='ant-menu'
              key='0'
            >
              {t('components.usersList.table.delete')}
            </Item>
          </Menu.ItemGroup>
        )}

        <Menu.ItemGroup>
          <Menu.Divider key='33' />
          <Item
            onItemHover={() => {}}
            onClick={() => {
              closeDropdown()
              setModalType('edit')
              toggleEditUserForm(rowData.id)
            }}
            rootPrefixCls='ant-menu'
            key='8'
          >
            {t('components.usersList.table.edit')}
          </Item>
        </Menu.ItemGroup>

        {currentUserId !== rowData.id &&
          ['taster'].includes(rowData.type) &&
          showOnFlavorWiki && (
            <Menu.ItemGroup>
              <Menu.Divider key='44' />
              <Item
                onItemHover={() => {}}
                onClick={() => {
                  closeDropdown()
                  setModalType('edit')
                  blockUser(rowData.id, rowData.blackListed)
                }}
                rootPrefixCls='ant-menu'
                key='9'
              >
                {blackListText}
              </Item>
            </Menu.ItemGroup>
          )}

        {currentUserId !== rowData.id && showOnFlavorWiki && (
          <Menu.ItemGroup>
            <Menu.Divider key='55' />
            <Item
              onItemHover={() => {}}
              onClick={() => {
                closeDropdown()
                userSurveyEnrollments(rowData.id)
              }}
              rootPrefixCls='ant-menu'
              key='11'
            >
              {t('components.usersList.table.surveyListings')}
            </Item>
          </Menu.ItemGroup>
        )}
      </Menu>
    </React.Fragment>
  )

  return (
    <ActionsContainer>
      {(isSuperAdmin || (isPowerUser && rowData.type === 'analytics')) && (
        <Dropdown
          overlay={menu}
          trigger={['click']}
          visible={
            rowData._id === openDropdownId || rowData.id === openDropdownId
          }
          onVisibleChange={change =>
            setOpenDropdownId(change && (rowData._id || rowData.id))
          }
        >
          <IconButton tooltip={t('tooltips.moreActions')} type='more' />
        </Dropdown>
      )}
    </ActionsContainer>
  )
}

export default UserMenu
