import React from 'react'
import { Table as AntTable } from 'antd'
import { DEFAULT_N_ELEMENTS_PER_PAGE } from '../../../utils/Constants'
import './index.css'
import { withTranslation } from 'react-i18next'
import UserMenu from './UserMenu'
import { ColumnContentAligner, Action, Separator } from './styles'

const Table = ({
  isArchivedList,
  users,
  handleChangeSelection,
  handleRemove,
  onUnarchivedUser,
  toggleEditUserForm,
  setModalType,
  page,
  userCount,
  onTableChange,
  currentUserId,
  t,
  orderBy,
  orderDirection,
  isAdmin,
  history,
  location,
  loading,
  blockUser,
  userSurveyEnrollments
}) => {
  const KEEP_ROUTERS = [
    '/operator/dashboard',
    '/operator/sharedWithMe',
    '/operator/surveys',
    '/operator/terms-of-use',
    '/operator/privacy-policy'
  ]
  const getImpersonatePath = (impersonateUserId, pathname = '/') => {
    const shouldKeepUrl = KEEP_ROUTERS.includes(pathname)
    return `/impersonate/${impersonateUserId}?redirect=${
      shouldKeepUrl ? pathname : '/'
    }`
  }

  const nameColumn = {
    title: t('components.usersList.table.name'),
    dataIndex: 'fullName',
    width: '20%',
    render: (_, rowData) => (
      <span>
        {rowData.firstName && rowData.lastName
          ? `${rowData.firstName} ${rowData.lastName}`
          : rowData.fullName}
      </span>
    ),
    sorter: () => false,
    className: 'column'
  }

  const emailColumn = {
    title: t('components.usersList.table.email'),
    dataIndex: 'emailAddress',
    width: '20%',
    sorter: () => false,
    className: 'column'
  }

  const countryColumn = {
    title: t('components.usersList.table.country'),
    dataIndex: 'country',
    width: '20%'
  }

  const organizationColumn = {
    title: t('components.usersList.table.organization'),
    dataIndex: 'organization.name',
    sorter: () => false,
    width: '20%',
    className: 'column'
  }

  const userTypeColumn = {
    title: t('components.usersList.table.type'),
    dataIndex: 'type',
    width: '20%',
    render: (_, rowData) => (
      <ColumnContentAligner>
        {rowData && rowData.isSuperAdmin ? 'super-admin' : rowData.type}
      </ColumnContentAligner>
    )
  }

  let columns = [nameColumn, emailColumn, countryColumn]
  if (isAdmin) {
    columns.push(organizationColumn)
  }

  columns = [
    ...columns,
    userTypeColumn,
    {
      title: t('components.usersList.table.actions'),
      dataIndex: '',
      render: (_, rowData) => {
        const isOrganizationInactive =
          rowData.organization && rowData.organization.inactive

        return (
          <ColumnContentAligner fixed='210px'>
            {isArchivedList && !isOrganizationInactive && (
              <Action
                onClick={() => {
                  onUnarchivedUser(rowData.id)
                }}
              >
                {t('unarchivedUser.unarchived')}
              </Action>
            )}

            {isArchivedList &&
              isOrganizationInactive &&
              t('unarchivedUser.organizationInactive')}

            {isAdmin &&
              !isArchivedList &&
              currentUserId !== rowData.id &&
              ['operator', 'taster', 'power-user', 'analytics'].includes(
                rowData.type
              ) && (
                <React.Fragment>
                  <Action
                    onClick={async () => {
                      history.replace(
                        getImpersonatePath(rowData.id, location.pathname)
                      )
                    }}
                  >
                    {t(`components.impersonate.impersonate`, { user: '' })}
                  </Action>
                  <Separator />
                </React.Fragment>
              )}

            {!isArchivedList && (
              <UserMenu
                currentUserId={currentUserId}
                handleRemove={handleRemove}
                setModalType={setModalType}
                toggleEditUserForm={toggleEditUserForm}
                t={t}
                rowData={rowData}
                blockUser={blockUser}
                userSurveyEnrollments={userSurveyEnrollments}
              />
            )}
          </ColumnContentAligner>
        )
      }
    }
  ]

  columns = columns.map(col => ({
    ...col,
    sortOrder: col.dataIndex === orderBy ? orderDirection : undefined
  }))

  return (
    <AntTable
      key='usersTable'
      loading={loading}
      rowKey={record => record.id}
      columns={columns}
      pagination={
        userCount > 10
          ? {
              pageSize: DEFAULT_N_ELEMENTS_PER_PAGE,
              total: userCount,
              current: parseInt(page, 10)
            }
          : false
      }
      onChange={(paginationConfig, filtersConfig, sortingConfig) => {
        onTableChange(
          paginationConfig.current,
          sortingConfig.field,
          sortingConfig.order
        )
      }}
      dataSource={users}
    />
  )
}

export default withTranslation()(Table)
