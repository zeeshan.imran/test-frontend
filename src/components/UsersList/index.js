import React from 'react'
import Table from './Table'
import SearchBar from '../SearchBar'
import Button from '../Button'
import { Container, HeaderContainer, PageTitle, SearchBarContainer } from './styles'
import UserForm from './UserForm'
import { getAuthenticatedUser } from '../../utils/userAuthentication'
import { useTranslation } from 'react-i18next'

/**
 * @param {Object} props
 * @param {number} props.page - Current page, start from 1
 */
const UsersListComponent = ({
  isArchivedList = false,
  isEditUser,
  toggleEditUserForm,
  selected,
  handelAdd,
  newUserData,
  setnewUserData,
  userTypes,
  organizationsList,
  handleEdit,
  handleSearch,
  searchBy,
  isAdmin,
  setEnableSubmit,
  enableSubmit,
  emailAlreadyInUse,
  modalType,
  setModalType,
  setUserData,
  location,
  history,
  loading,
  userSurveyEnrollments,
  ...tableProps
}) => {
  const { t } = useTranslation()
  const currentUserId = getAuthenticatedUser().id
  
  return (
    <Container>
      <HeaderContainer>
        {isArchivedList && (
          <PageTitle>{t('components.operatorSurveys.archivedUsers')}</PageTitle>
        )}

        {!isArchivedList && (
          <Button
            onClick={() => {
              setModalType('add')
              setUserData({})
              toggleEditUserForm(true)
            }}
          >
            {t('components.usersList.add')}
          </Button>
        )}

        <SearchBarContainer>
          <SearchBar
            placeholder='Search'
            withIcon
            handleChange={handleSearch}
            value={searchBy}
          />
        </SearchBarContainer>
      </HeaderContainer>
      <Table
        {...tableProps}
        isArchivedList={isArchivedList}
        toggleEditUserForm={toggleEditUserForm}
        currentUserId={currentUserId}
        isAdmin={isAdmin}
        setModalType={setModalType}
        location={location}
        history={history}
        loading={loading}
        userSurveyEnrollments={userSurveyEnrollments}
      />
      {!!isEditUser && (
        <UserForm
          isAdmin={isAdmin}
          id='user form'
          isEditUser={isEditUser}
          handleEdit={handleEdit}
          toggleEditUserForm={toggleEditUserForm}
          handelAdd={handelAdd}
          newUserData={newUserData}
          setnewUserData={setnewUserData}
          userTypes={userTypes}
          organizationsList={organizationsList}
          currentUserId={currentUserId}
          // Enabling Submit Button
          setEnableSubmit={setEnableSubmit}
          enableSubmit={enableSubmit}
          emailAlreadyInUse={emailAlreadyInUse}
          modalType={modalType}
        />
      )}
    </Container>
  )
}

export default UsersListComponent
