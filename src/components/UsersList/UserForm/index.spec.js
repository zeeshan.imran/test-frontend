import React from 'react'
import { mount, shallow } from 'enzyme'
import UserForm from '.'
import Input from '../../Input'
import { Modal, Form, Row, Checkbox, Col } from 'antd'

describe('UserForm', () => {
  let testRender
  let isEditUser
  let handelAdd
  let toggleEditUserForm
  let newUserData
  let setnewUserData
  let userTypes
  let isAdmin
  let organizationsList
  let setEnableSubmit
  let emailAlreadyInUse

  beforeEach(() => {
    isEditUser = true
    handelAdd = jest.fn()
    toggleEditUserForm = jest.fn()
    newUserData = {
      id: 1,
      type: 'operator',
      emailAddress: 'test@flavorwiki.com',
      isDummyTaster: false
    }
    setnewUserData = jest.fn()
    userTypes = ['isAdmin', 'taster']
    isAdmin = true
    organizationsList = [
      {
        name: 'organization one',
        id: 1
      },
      {
        name: 'organization two',
        id: 2
      }
    ]
    setEnableSubmit = jest.fn()
    emailAlreadyInUse = true
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render UserForm Email Address input', () => {
    testRender = mount(
      <UserForm
        isEditUser={isEditUser}
        handelAdd={handelAdd}
        toggleEditUserForm={toggleEditUserForm}
        setnewUserData={setnewUserData}
        setEnableSubmit={setEnableSubmit}
        emailAlreadyInUse={emailAlreadyInUse}
      />
    )

    testRender.findWhere(c => c.name() === 'Modal').prop('onCancel')()

    testRender
      .findWhere(
        c => c.name() === 'Input' && c.prop('name') === 'Email Address'
      )
      .first()
      .prop('onChange')({ target: { value: 'test@flawor.com' } })

    expect(setEnableSubmit).toHaveBeenCalled()

    expect(setnewUserData).toHaveBeenCalledWith({
      emailAddress: 'test@flawor.com'
    })
  })

  test('should render UserForm Full name input', () => {
    testRender = mount(
      <UserForm
        isEditUser={isEditUser}
        handelAdd={handelAdd}
        toggleEditUserForm={toggleEditUserForm}
        setnewUserData={setnewUserData}
        setEnableSubmit={setEnableSubmit}
        emailAlreadyInUse={emailAlreadyInUse}
      />
    )

    testRender
      .findWhere(c => c.name() === 'Input' && c.prop('name') === 'Full name')
      .first()
      .prop('onChange')({ target: { value: 'Tom xender' } })

    expect(setEnableSubmit).toHaveBeenCalled()

    expect(setnewUserData).toHaveBeenCalledWith({
      fullName: 'Tom xender'
    })
  })

  test('should render UserForm isAdmin and usersList selectOrganization', () => {
    testRender = mount(
      <UserForm
        isEditUser={isEditUser}
        handelAdd={handelAdd}
        toggleEditUserForm={toggleEditUserForm}
        setnewUserData={setnewUserData}
        setEnableSubmit={setEnableSubmit}
        emailAlreadyInUse={emailAlreadyInUse}
        isAdmin={isAdmin}
        organizationsList={organizationsList}
      />
    )

    testRender
      .findWhere(c => c.name() === 'Select')
      .first()
      .prop('onChange')(1)
  })
  test('should render UserForm isAdmin and usersList selectType', () => {
    testRender = mount(
      <UserForm
        isEditUser={isEditUser}
        newUserData={newUserData}
        setnewUserData={setnewUserData}
        setEnableSubmit={setEnableSubmit}
        isAdmin={isAdmin}
        userTypes={userTypes}
        currentUserId={2}
      />
    )

    testRender
      .findWhere(c => c.name() === 'Select')
      .last()
      .prop('onChange')()
  })

  test('should render UserForm isAdmin and usersList isTaster', () => {
    testRender = mount(
      <UserForm
        isEditUser={isEditUser}
        newUserData={newUserData}
        setnewUserData={setnewUserData}
        setEnableSubmit={setEnableSubmit}
        isAdmin={isAdmin}
        organizationsList={organizationsList}
        currentUserId={1}
      />
    )

    testRender
      .findWhere(c => c.name() === 'Checkbox' && c.prop('name') === 'isTaster')
      .first()
      .prop('onChange')({
      target: {
        value: true
      }
    })
  })

  test('should render UserForm isAdmin and user type is isSuperAdmin', () => {
    testRender = mount(
      <UserForm
        isEditUser={isEditUser}
        newUserData={newUserData}
        setnewUserData={setnewUserData}
        setEnableSubmit={setEnableSubmit}
        isAdmin={isAdmin}
        organizationsList={organizationsList}
        currentUserId={2}
      />
    )

    testRender
      .findWhere(
        c => c.name() === 'Checkbox' && c.prop('name') === 'isSuperAdmin'
      )
      .first()
      .prop('onChange')({
      target: {
        value: true
      }
    })
  })

  test('should render UserForm is setnewUserData', () => {
    const newUserDatamock = {}
    const organizationsListMock = [
      {
        name: 'organization one',
        id: 1
      }
    ]
    testRender = mount(
      <UserForm
        newUserData={newUserDatamock}
        setnewUserData={setnewUserData}
        organizationsList={organizationsListMock}
      />
    )
    expect(testRender.find(UserForm)).toHaveLength(1)
  })
})
