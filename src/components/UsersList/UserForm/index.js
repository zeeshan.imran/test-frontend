import React, { useMemo, useEffect, useRef, useState } from 'react'
import { Formik } from 'formik'
import { Modal, Form, Row, Col } from 'antd'
import { useTranslation } from 'react-i18next'
import { Title, StyledCheckbox } from './styles.js'
import Input from '../../Input'
import Select from '../../Select'
import validationSchema from '../../../validates/user'
import { options } from '../../../utils/internationalization/index'
import { getOptionValue } from '../../../utils/getters/OptionGetters'

import {
  GENDERS,
  DUMMY_COUNTRY_PHONE_CODE,
  COUNTRY_PHONE_CODES,
  CURRENT_STATES
} from '../../../utils/Constants'

const UserForm = ({
  isEditUser,
  handelAdd,
  toggleEditUserForm,
  newUserData,
  setnewUserData,
  handleEdit,
  userTypes,
  isAdmin,
  organizationsList,
  currentUserId,
  setEnableSubmit,
  enableSubmit,
  emailAlreadyInUse,
  modalType
}) => {
  const capitalize = text => text.charAt(0).toUpperCase() + text.slice(1)
  const { t } = useTranslation()

  if (
    organizationsList &&
    organizationsList.length === 1 &&
    !newUserData.organization
  ) {
    setnewUserData({
      ...newUserData,
      organization: organizationsList[0].id
    })
  }

  const formRef = useRef()
  const [isMounted, setIsMounted] = useState()
  const [isTouched, setIsTouched] = useState(modalType !== 'add')
  const [enableEmail] = useState(isEditUser !== true)

  const setData = ({ ...newData }) => {
    if (!isTouched) {
      setIsTouched(true)
    }
    setnewUserData(newData)
  }

  useEffect(() => {
    if (!isMounted && formRef.current && formRef.current.validateForm) {
      setIsMounted(true)
      formRef.current.validateForm()
    } else if (!isMounted) {
      setIsMounted(false)
    }
  }, [isMounted])

  const canCreateTaster = useMemo(() => {
    if (newUserData) {
      const { emailAddress = '', organization = '' } = newUserData

      if (emailAddress.endsWith('@flavorwiki.com')) {
        return true
      }

      if (organizationsList) {
        const organizationItem = organizationsList.find(organizationItem => {
          return (
            organizationItem.id === organization &&
            (organizationItem.name === 'Demo Company' ||
              organizationItem.name === 'FlavorWiki')
          )
        })

        if (organizationItem) {
          return true
        }
      }
    }

    return false
  }, [organizationsList, newUserData])

  return (
    <Modal
      title={
        modalType === 'add' ? (
          <Title>{t('components.userList.addFormTitle')}</Title>
        ) : (
          <Title>{t('components.userList.editFormTitle')}</Title>
        )
      }
      width={1016}
      visible={!!isEditUser}
      onOk={isEditUser === true ? handelAdd : handleEdit}
      okButtonProps={{ disabled: !enableSubmit }}
      onCancel={() => toggleEditUserForm(false)}
    >
      <Formik
        ref={formRef}
        enableReinitialize
        validationSchema={validationSchema}
        initialValues={{
          ...newUserData
        }}
        render={({ errors, setFieldValue, values }) => {
          if (emailAlreadyInUse) {
            errors['emailAddress'] = emailAlreadyInUse
          }
          return (
            <Row gutter={24}>
              <Col xl={12}>
                <Form.Item
                  help={isTouched ? errors.emailAddress : null}
                  validateStatus={
                    isTouched && errors.emailAddress ? 'error' : 'success'
                  }
                >
                  <Input
                    name='Email Address'
                    size='default'
                    label={t('components.usersList.emailAddress')}
                    value={values.emailAddress}
                    placeholder={t('placeholders.emailAddress')}
                    required
                    disabled={enableEmail}
                    onChange={e => {
                      const formData = {
                        ...newUserData,
                        emailAddress: e.target.value
                      }

                      setFieldValue('emailAddress', e.target.value)
                      setEnableSubmit(errors, formData)
                      setData(formData)
                    }}
                  />
                </Form.Item>
                <Form.Item
                  help={isTouched ? errors.fullName : null}
                  validateStatus={
                    isTouched && errors.fullName ? 'error' : 'success'
                  }
                >
                  <Input
                    name='Full name'
                    size='default'
                    label={t('components.usersList.fullName')}
                    value={values.fullName}
                    placeholder={t('placeholders.fullName')}
                    required
                    onChange={e => {
                      const formData = {
                        ...newUserData,
                        fullName: e.target.value
                      }

                      setFieldValue('fullName', e.target.value)
                      setEnableSubmit(errors, formData)
                      setData(formData)
                    }}
                  />
                </Form.Item>
                <Form.Item>
                  <Select
                    name='gender'
                    size='default'
                    label={t(
                      `components.createTasterAccount.forms.second.gender.label`
                    )}
                    options={GENDERS}
                    getOptionName={option =>
                      t(
                        `components.createTasterAccount.forms.second.gender.${option}`
                      )
                    }
                    getOptionValue={option => option}
                    value={values.gender}
                    placeholder={t(
                      `components.createTasterAccount.forms.second.gender.placeholder`
                    )}
                    showSearch
                    onChange={value => {
                      const formData = {
                        ...newUserData,
                        gender: value
                      }

                      setFieldValue('gender', value)
                      setEnableSubmit(errors, formData)
                      setData(formData)
                    }}
                  />
                </Form.Item>
                <Form.Item>
                  {newUserData && newUserData.isDummyTaster && (
                    <Input
                      label={t(
                        `components.createTasterAccount.forms.second.country.label`
                      )}
                      value={DUMMY_COUNTRY_PHONE_CODE.name}
                      disabledInput
                    />
                  )}

                  {newUserData && !newUserData.isDummyTaster && (
                    <Select
                      name='country'
                      size='default'
                      label={t(
                        'components.createTasterAccount.forms.second.country.label'
                      )}
                      value={values.country}
                      options={COUNTRY_PHONE_CODES}
                      getOptionName={option => option.name}
                      getOptionValue={option => option.code}
                      placeholder={t(
                        'components.createTasterAccount.forms.second.country.placeholder'
                      )}
                      showSearch
                      onChange={value => {
                        const formData = {
                          ...newUserData,
                          country: value,
                          state: ''
                        }

                        setFieldValue('country', value)
                        setFieldValue('state', '')
                        setEnableSubmit(errors, formData)
                        setData(formData)
                      }}
                    />
                  )}
                </Form.Item>
                <Form.Item>
                  <Select
                    name='state'
                    size='default'
                    label={t(
                      'components.createTasterAccount.forms.second.state.label'
                    )}
                    value={values.state}
                    placeholder={t(
                      'components.createTasterAccount.forms.second.state.placeholder'
                    )}
                    options={
                      CURRENT_STATES[values.country]
                        ? CURRENT_STATES[values.country]
                        : CURRENT_STATES['others']
                    }
                    getOptionName={option => option.name}
                    getOptionValue={option => option.name}
                    showSearch
                    onChange={value => {
                      const formData = {
                        ...newUserData,
                        state: value
                      }

                      setFieldValue('state', value)
                      setEnableSubmit(errors, formData)
                      setData(formData)
                    }}
                  />
                </Form.Item>
              </Col>
              <Col xl={12}>
                <Form.Item>
                  <Select
                    name='language'
                    size={`default`}
                    label={t(
                      `components.createTasterAccount.forms.second.langugage.label`
                    )}
                    value={values.language}
                    placeholder={t(
                      `components.createTasterAccount.forms.second.langugage.placeholder`
                    )}
                    options={options}
                    getOptionValue={getOptionValue}
                    getOptionName={(option = {}) =>
                      t(option.label) ? t(option.label) : option.label
                    }
                    showSearch
                    onChange={value => {
                      const formData = {
                        ...newUserData,
                        language: value
                      }

                      setFieldValue('language', value)
                      setEnableSubmit(errors, formData)
                      setData(formData)
                    }}
                  />
                </Form.Item>
                <Form.Item
                  help={isTouched ? errors.paypalEmailAddress : null}
                  validateStatus={
                    isTouched && errors.paypalEmailAddress ? 'error' : 'success'
                  }
                >
                  <Input
                    name='paypalEmailAddress'
                    size='default'
                    label={t(
                      'components.createTasterAccount.forms.third.paypalEmailAddress.label'
                    )}
                    value={values.paypalEmailAddress}
                    placeholder={t(
                      'components.createTasterAccount.forms.third.paypalEmailAddress.placeholder'
                    )}
                    onChange={event => {
                      const formData = {
                        ...newUserData,
                        paypalEmailAddress: event.target.value
                      }

                      setFieldValue('paypalEmailAddress', event.target.value)
                      setEnableSubmit(errors, formData)
                      setData(formData)
                    }}
                  />
                </Form.Item>
                {isAdmin && (
                  <Form.Item>
                    <Select
                      label={t('components.usersList.selectOrganization')}
                      value={values.organization}
                      onChange={value => {
                        const formData = {
                          ...newUserData,
                          organization: value
                        }

                        setFieldValue('organization', value)
                        setEnableSubmit(errors, formData)
                        setData(formData)
                      }}
                      placeholder={t('placeholders.selectOrganization')}
                      options={organizationsList}
                      getOptionName={organization => organization.name}
                      getOptionValue={organization => organization.id}
                      showSearch
                      optionFilterProp={`children`}
                      size='default'
                    />
                  </Form.Item>
                )}
                {isAdmin && currentUserId !== values.id && (
                  <Form.Item
                    help={isTouched ? errors.type : null}
                    validateStatus={
                      isTouched && errors.type ? 'error' : 'success'
                    }
                  >
                    <Select
                      label={t('components.usersList.selectType')}
                      value={values.type}
                      onChange={value => {
                        const formData = {
                          ...newUserData,
                          type: value
                        }

                        setFieldValue('type', value)
                        setEnableSubmit(errors, formData)
                        setData(formData)
                      }}
                      placeholder={t('placeholders.selectType')}
                      options={userTypes}
                      getOptionName={type => capitalize(type)}
                      getOptionValue={type => type}
                      size='default'
                    />
                  </Form.Item>
                )}
                {isAdmin && values.type === 'operator' && canCreateTaster && (
                  <Row>
                    <Col xs={{ span: 12 }}>
                      <StyledCheckbox
                        name='isTaster'
                        checked={values.isTaster}
                        onChange={e => {
                          const formData = {
                            ...newUserData,
                            isTaster: e.target.checked
                          }

                          setFieldValue('isTaster', e.target.checked)
                          setEnableSubmit(errors, formData)
                          setData(formData)
                        }}
                      >
                        {t('components.userForm.isTaster')}
                      </StyledCheckbox>
                    </Col>
                    {currentUserId !== values.id &&
                      /[^@]+@flavorwiki.com$/i.test(values.emailAddress) && (
                        <Col xs={{ span: 12 }}>
                          <StyledCheckbox
                            name='isSuperAdmin'
                            checked={values.isSuperAdmin}
                            onChange={e => {
                              const formData = {
                                ...newUserData,
                                isSuperAdmin: e.target.checked
                              }

                              setFieldValue('isSuperAdmin', e.target.checked)
                              setEnableSubmit(errors, formData)
                              setData(formData)
                            }}
                          >
                            {t('components.userForm.isSuperAdmin')}
                          </StyledCheckbox>
                        </Col>
                      )}
                  </Row>
                )}
              </Col>
            </Row>
          )
        }}
      />
    </Modal>
  )
}

export default UserForm
