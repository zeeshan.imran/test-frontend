import React from 'react'
import { ContentWrapper } from '../styles'
import { Table } from 'antd'

const Csv = ({ content }) => {
  const { columns, data } = content
  const tableColumns = [
    { title: '', dataIndex: 0 },
    ...columns.map((col, index) => ({
      dataIndex: index + 1,
      title: col
    }))
  ]
  const dataSource = Object.keys(data).map(key => [key, ...data[key]])
  return (
    <ContentWrapper>
      <Table
        size='middle'
        scroll={{ x: '100vw' }}
        pagination={{ pageSize: 6 }}
        bordered
        columns={tableColumns}
        dataSource={dataSource}
      />
    </ContentWrapper>
  )
}

export default Csv
