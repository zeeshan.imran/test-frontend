import React from 'react'

const NotSupport = ({ file }) => (
  <div>No viewer is available for {file.name}.</div>
)

export default NotSupport
