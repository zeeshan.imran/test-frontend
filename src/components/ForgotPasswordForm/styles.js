import styled from 'styled-components'
import Button from '../Button'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'
import { DEFAULT_COMPONENTS_MARGIN } from '../../utils/Metrics'

export const FormInfoContainer = styled.div`
  margin: ${DEFAULT_COMPONENTS_MARGIN}rem 0;
`

export const MessagePositioner = styled.div`
  position: absolute;
`

export const FormLinkContainer = styled.div`
  text-align: center;
  margin: ${DEFAULT_COMPONENTS_MARGIN}rem 0;
`

export const LoginLink = styled.span`
  font-family: ${family.primaryLight};
  color: ${colors.LIGHT_OLIVE_GREEN};
  cursor: pointer;
  margin-bottom: ${DEFAULT_COMPONENTS_MARGIN}rem;
`

export const StyledButton = styled(Button)`
  width: 100%;
`
