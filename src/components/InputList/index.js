import React from 'react'
import { Col, Row, Form } from 'antd'
import FieldLabel from '../FieldLabel'
import Input from '../Input'
import Button from '../Button'
import { InstructionIndex, MarginRow, BtnCol } from './styles'
import { remove } from 'ramda'
import { useTranslation } from 'react-i18next'

const InputList = ({ label, value = [], onChange, tooltip, tooltipPlacement, errors = [], ...other }) => {
  const { t } = useTranslation()
  return (
    <FieldLabel label={label} tooltip={tooltip} tooltipPlacement={tooltipPlacement}>
      {value &&
        value.map((instruction, index) => (
          <MarginRow key={index}>
            <Col xs={{ span: 1 }}>
              <InstructionIndex>{index + 1}</InstructionIndex>
            </Col>
            <Col xs={{ span: 22 }}>
              <Form.Item
                validateStatus={errors[index] ? 'error' : 'success'}
                help={errors[index]}
              >
                <Input
                  value={instruction}
                  size='default'
                  onChange={event => {
                    let updValues = [...value]
                    updValues[index] = event.target.value
                    onChange(updValues)
                  }}
                />
              </Form.Item>
            </Col>
            <BtnCol xs={{ span: 1 }}>
              <Button
                type='red'
                size='default'
                onClick={() => {
                  onChange(remove(index, 1, value))
                }}
              >
                X
              </Button>
            </BtnCol>
          </MarginRow>
        ))}
      <Row>
        <Col lg={8} md={8}>
          <Button size='default' onClick={() => onChange([...value, ''])}>
            {t('components.inputList.addStep')}
          </Button>
        </Col>
      </Row>
    </FieldLabel>
  )
}

export default InputList
