import React from 'react'
import { CopyToClipboard as Copy } from 'react-copy-to-clipboard'
import IconButton from '../IconButton'
import { ButtonWrapper } from './styles'

const CopyToClipboard = ({ target, tooltip, label, noActive }) => (
  <Copy text={target}>
    <ButtonWrapper>
      <IconButton type='link' tooltip={tooltip} noActive={noActive} />
      {label}
    </ButtonWrapper>
  </Copy>
)

export default CopyToClipboard
