import React from 'react'
import moment from 'moment'
import { Row, Col } from 'antd'
import SurveyProgressBar from '../../containers/SurveyProgressBar'
import SectionTitle from '../SectionTitle'
import { Desktop } from '../Responsive'

import { Container, ProgressBarContainer, CountdownContainer } from './styles'
import TimerCountdown from '../TimerCountdown'

const SurveyQuestionLayout = ({
  title,
  children,
  question,
  currentSurveyParticipation = {},
  setIsTimerLocked,
  timeCompletedFor,
  setTimeCompletedFor,
  handleAdvance,
  survey
}) => {
  let timeLimit = 0
  let timeTitle = 'Next product in: '
  let maxPoint = {}
  let showTimer = false
  if (
    survey &&
    survey.addDelayToSelectNextProductAndNextQuestion &&
    question &&
    question.type === 'choose-product' &&
    currentSurveyParticipation
  ) {
    if (currentSurveyParticipation && currentSurveyParticipation.savedRewards) {
      const { savedRewards } = currentSurveyParticipation
      maxPoint = savedRewards.reduce((main, second) => {
        const mainStartedAt = main.startedAt || 0
        const secondStartedAt = second.startedAt || 0
        return mainStartedAt > secondStartedAt ? main : second
      })
      if (
        maxPoint &&
        maxPoint.startedAt &&
        timeCompletedFor.indexOf(maxPoint.id) < 0
      ) {
        const timeStarted = maxPoint.startedAt
        const delayToNextProduct = maxPoint.delayToNextProduct
        const nextProductTime = moment(timeStarted).add(delayToNextProduct)
        const timeDiff = nextProductTime.diff(question.currentTime)

        if (timeDiff > 0) {
          maxPoint.expiredValue = 'main'
          timeLimit = question.currentTime + timeDiff
          showTimer = true
          setIsTimerLocked(true)
        } else {
          const valuesCompleted = timeCompletedFor
          valuesCompleted.push(maxPoint.id)
          setTimeCompletedFor(valuesCompleted)
          setIsTimerLocked(false)
        }
      } else if (
        maxPoint &&
        maxPoint.startedAt &&
        maxPoint.expiredValue &&
        timeCompletedFor.indexOf(maxPoint.id) >= 0
      ) {
        const timeStarted = maxPoint.startedAt
        const delayToNextProduct = maxPoint.delayToNextProduct
        const extraDelayToNextProduct = maxPoint.extraDelayToNextProduct

        const nextProductTime = moment(timeStarted).add(delayToNextProduct)
        const nextProductExtraTime = moment(timeStarted)
          .add(delayToNextProduct)
          .add(extraDelayToNextProduct)

        if (nextProductExtraTime > nextProductTime) {
          const timeDiff = nextProductExtraTime.diff(question.currentTime)
          if (timeDiff > 0) {
            maxPoint.expiredValue = 'delayed'
            timeTitle = `You will be rejected in: `
            timeLimit = question.currentTime + timeDiff
            showTimer = true
          } else {
            handleAdvance.current({
              variables: { rejectUserEnrollment: true }
            })
          }
        }
      }
    }
  }

  return (
    <Desktop>
      {desktop => (
        <Container desktop={desktop}>
          {question &&
            [true, undefined, null].includes(question.displaySurveyName) && (
              <SectionTitle title={title} />
            )}
          <Row>
            <Col xs={{ span: 24 }} md={{ span: 24 }}>
              {showTimer && (
                <CountdownContainer>
                  <TimerCountdown
                    titleCountdown={timeTitle}
                    timeCountDown={timeLimit}
                    onFinishCountDown={async () => {
                      if (maxPoint) {
                        if (maxPoint.expiredValue === 'main') {
                          const valuesCompleted = timeCompletedFor
                          valuesCompleted.push(maxPoint.id)
                          setTimeCompletedFor(valuesCompleted)
                        }
                        if (maxPoint.expiredValue === 'delayed') {
                          await handleAdvance.current({
                            variables: { rejectUserEnrollment: true }
                          })
                        }
                      }
                      setIsTimerLocked(false)
                    }}
                  />
                </CountdownContainer>
              )}

              <ProgressBarContainer>
                <SurveyProgressBar />
              </ProgressBarContainer>
            </Col>
          </Row>
          {children}
        </Container>
      )}
    </Desktop>
  )
}

export default SurveyQuestionLayout
