import React from 'react'
import { createBrowserHistory } from 'history'
import { Router } from 'react-router-dom'
import { mount } from 'enzyme'
import OperatorSurveysList from './index'
import { Table as AntTable } from 'antd'
import wait from '../../utils/testUtils/waait'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { FieldContainer, ActionsContainer } from './styles'
import { getAuthenticatedUser } from '../../utils/userAuthentication'

jest.mock('../../containers/CloneSurvey', () => () => null)
jest.mock('../../containers/EditSurveyButton', () => () => null)
jest.mock('../../containers/DeleteSurveyButton', () => () => null)
jest.mock('../../utils/userAuthentication')

describe('OperatorSurveyList component', () => {
  let testRender
  let client

  beforeAll(() => {
    client = createApolloMockClient()
    getAuthenticatedUser.mockImplementation(() => ({
      emailAddress: 'operator@flavorwiki.com'
    }))
  })

  afterEach(() => {
    testRender.unmount()
  })

  test(`test enable/disable actions`, async () => {
    testRender = mount(<OperatorSurveysList />)
    await wait(0)

    expect(testRender.find(AntTable).props().columns.length).toBe(2)
    testRender.setProps({ showActions: true })
    expect(testRender.find(AntTable).props().columns.length).toBe(3)
  })

  test(`test amount of rows`, async () => {
    const surveyMock = [
      {
        id: '1',
        products: [],
        state: 'draft'
      },
      {
        id: '2',
        products: [],
        state: 'suspended'
      },
      {
        id: '3',
        products: [],
        state: 'deprecated'
      },
      {
        id: '4',
        products: [],
        state: 'active'
      }
    ]

    const history = createBrowserHistory()
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <OperatorSurveysList surveys={surveyMock} showActions />
        </Router>
      </ApolloProvider>
    )
    await wait(0)
 
    expect(testRender.find(FieldContainer).length).toBe(8)
    expect(testRender.find(ActionsContainer).length).toBe(4)
  })
})
