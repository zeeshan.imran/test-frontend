import React, { useState, useEffect } from 'react'
import gql from 'graphql-tag'
import { useMutation } from 'react-apollo-hooks'
import Text from '../../Text'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { StyledModal } from '../styles'
import ShareStatsForm from '../../../containers/ShareStatsForm'
import { displaySuccessMessage } from '../../../utils/displaySuccessMessage'

const ShareStatsButton = ({
  survey,
  organizations,
  onMenuItemClick,
  screeners,
  compulsorySurvey,
  includeCompulsorySurveyDataInStats
}) => {
  const { t } = useTranslation()
  const shareStatsScreen = useMutation(SHARE_STATS_SCREEN)
  const [sharedStatsUsers, setSharedStatsUsers] = useState({})
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [newShares, setNewShares] = useState([])
  const [isScreenerShownInShare, setIsScreenerShownInShare] = useState(
    !!survey.isScreenerShownInShare
  )
  const [isOkDisabled, setIsOkDisabled] = useState()
  const [
    isCompulsorySurveyShownInShare,
    setIsCompulsorySurveyShownInShare
  ] = useState(!!survey.isCompulsorySurveyShownInShare)
  useEffect(() => {
    if (
      survey.id &&
      survey.sharedStatsUsers &&
      survey.sharedStatsUsers.length > 0
    ) {
      setSharedStatsUsers([...survey.sharedStatsUsers])
    }
  }, [survey.id])

  const shareSurvey = async () => {
    const {
      data: { shareSurveyStatsScreen }
    } = await shareStatsScreen({
      variables: {
        surveyId: survey.id || survey._id,
        users: newShares,
        isScreenerShownInShare,
        isCompulsorySurveyShownInShare
      }
    })
    setSharedStatsUsers([...shareSurveyStatsScreen.sharedStatsUsers])
    displaySuccessMessage(
      t(`components.dasboard.survey.actions.shareStats.success.message`, {
        name: survey.name,
        num: newShares.length
      })
    )
  }
  return (
    <React.Fragment>
      <Text
        onClick={() => {
          onMenuItemClick()
          setIsModalVisible(true)
        }}
      >
        {t(`tooltips.ShareSurveyStats`)}
      </Text>
      <StyledModal
        visible={isModalVisible}
        title={t(`components.dasboard.survey.actions.shareStats.modal.title`, {
          name: survey.name
        })}
        keyboard={false}
        closable={false}
        onCancel={() => {
          setIsModalVisible(false)
        }}
        onOk={() => {
          shareSurvey()
          setIsModalVisible(false)
        }}
        okButtonProps={{
          disabled: isOkDisabled
        }}
      >
        <ShareStatsForm
          survey={survey}
          surveyId={survey.id}
          sharedStatsUsers={sharedStatsUsers}
          organizations={organizations}
          setNewShares={setNewShares}
          isScreenerShownInShare={isScreenerShownInShare}
          setIsScreenerShownInShare={setIsScreenerShownInShare}
          setIsOkDisabled={setIsOkDisabled}
          isOkDisabled={isOkDisabled}
          showScreenerOption={screeners && screeners.length > 0}
          isCompulsorySurveyShownInShare={isCompulsorySurveyShownInShare}
          setIsCompulsorySurveyShownInShare={setIsCompulsorySurveyShownInShare}
          compulsorySurvey={compulsorySurvey}
          includeCompulsorySurveyDataInStats={
            includeCompulsorySurveyDataInStats
          }
        />
      </StyledModal>
    </React.Fragment>
  )
}

export const SHARE_STATS_SCREEN = gql`
  mutation shareSurveyStatsScreen(
    $surveyId: ID
    $users: [String]
    $isScreenerShownInShare: Boolean
    $isCompulsorySurveyShownInShare: Boolean
  ) {
    shareSurveyStatsScreen(
      surveyId: $surveyId
      users: $users
      isScreenerShownInShare: $isScreenerShownInShare
      isCompulsorySurveyShownInShare: $isCompulsorySurveyShownInShare
    ) {
      id
      sharedStatsUsers {
        id
        organization {
          id
        }
      }
    }
  }
`

export default withRouter(ShareStatsButton)
