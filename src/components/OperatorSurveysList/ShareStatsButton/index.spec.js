import React from 'react'
import { mount } from 'enzyme'
import ShareStatsButton from '.'
import { Router } from 'react-router-dom'
import IconButton from '../../../components/IconButton'
import wait from '../../../utils/testUtils/waait'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'

describe('ShareStatsButton', () => {
  let testRender
  let survey
  let client

  beforeEach(() => {
    survey = {
      id: 'survey-1'
    }
    client = createApolloMockClient()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ShareStatsButton', async () => {
    const history = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }

    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <ShareStatsButton survey={survey} />
        </Router>
      </ApolloProvider>
    )
    expect(testRender.find(ShareStatsButton)).toHaveLength(1)
  })
})
