import styled from 'styled-components'
import Text from '../Text'
import { family } from '../../utils/Fonts'
import { Modal as AntModal, Menu as AntMenu, Icon as AntIcon } from 'antd'
import colors from '../../utils/Colors'

export const ActionsContainer = styled.div`
  display: flex;
  justify-content: flex-end;
`

export const FieldText = styled(Text)`
  font-size: ${({ small }) => (small ? `1.2rem` : `1.4rem`)};
  font-family: ${family.primaryRegular};
  line-height: 2.2rem;
  color: rgba(0, 0, 0, 0.45);
  user-select: none;
  white-space: pre-wrap;
  justify-content: center;
`

export const FieldContainer = styled.div`
  display: flex;
  align-items: center;
  ${({ center }) => (center ? `justify-content: center;` : ``)}
`

export const TitleContainer = styled.div`
  display: flex;
  flex-direction: column;
`

export const SurveyNameContainer = styled.div`
  display: inline-flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`

export const SurveyName = styled(FieldText)`
  font-family: ${family.primaryBold};
  color: rgba(0, 0, 0, 0.65);
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  @media screen and (max-device-width: 1400px) {
    max-width: 40vw;
  }
  @media screen and (min-device-width: 1401px) {
    max-width: 500px;
  }
`

export const StyledModal = styled(AntModal)`
  .ant-modal-body {
    padding: 2.5rem 4rem 3.5rem 4rem;
    min-width: 600px;
  }
  .ant-modal-content {
    min-width: 550px;
  }
  .ant-transfer-list {
    width: 210px;
  }
`

export const Item = styled(AntMenu.Item)`
  color: ${colors.MODAL_LINK_COLOR};
  &:hover {
    span {
      color: ${colors.MODAL_LINK_HOVER_COLOR};
    }
  }
`

export const SurveyImage = styled.img`
  min-height: 32px;
  min-width: 32px;
  height: 32px;
  width: 32px;
  max-height: 32px;
  max-width: 32px;
  margin-right: 16px;
  border-radius: 4px;
  border: 0.1rem solid rgba(0, 0, 0, 0.65);
`

export const Icon = styled(AntIcon)`
  height: 32px;
  width: 32px;
  margin-right: 16px;
  color: rgba(0, 0, 0, 0.65);
`
