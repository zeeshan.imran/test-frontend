import React from 'react'
import { storiesOf } from '@storybook/react'
import AdminSurveysList from './'

const surveys = [
  {
    id: 'm59arWq35RGmeqDm7VRCZf',
    title: 'Survey Title 1',
    expirationTime: '2022-01-01T11:00:00',
    picture:
      'https://cdn-images-1.medium.com/max/1200/1*0nhNvVPD07JZeizhkqF8Mw.png',
    createdBy: {
      firstname: 'Marc',
      lastname: 'Muller'
    },
    participants: [
      {
        state: 'SUBSCRIBED'
      },
      {
        state: 'SUBSCRIBED'
      },
      {
        state: 'STARTED'
      },
      {
        state: 'STARTED'
      },
      {
        state: 'COMPLETED'
      },
      {
        state: 'COMPLETED'
      },
      {
        state: 'COMPLETED'
      },
      {
        state: 'COMPLETED'
      },
      {
        state: 'COMPLETED'
      },
      {
        state: 'COMPLETED'
      },
      {
        state: 'ABORTED'
      },
      {
        state: 'ABORTED'
      }
    ]
  },
  {
    id: 'vYDLpjsktUzkWnvjaC4h6c',
    title: 'Survey Title 2',
    expirationTime: '2020-01-01T11:00:00',
    picture:
      'https://cdn-images-1.medium.com/max/1200/1*0nhNvVPD07JZeizhkqF8Mw.png',
    createdBy: {
      firstname: 'Marc',
      lastname: 'Muller'
    },
    participants: [
      {
        state: 'SUBSCRIBED'
      },
      {
        state: 'SUBSCRIBED'
      },
      {
        state: 'STARTED'
      },
      {
        state: 'STARTED'
      },
      {
        state: 'PRODUCTS_SELECTED'
      },
      {
        state: 'PRODUCTS_SELECTED'
      },
      {
        state: 'COMPLETED'
      },
      {
        state: 'COMPLETED'
      },
      {
        state: 'COMPLETED'
      },
      {
        state: 'COMPLETED'
      },
      {
        state: 'ABORTED'
      },
      {
        state: 'ABORTED'
      }
    ]
  },
  {
    id: 'vYDLpjsktUzkWnvjaC4h6H',
    title: 'Survey Title 3',
    expirationTime: '2010-01-01T11:00:00',
    picture:
      'https://cdn-images-1.medium.com/max/1200/1*0nhNvVPD07JZeizhkqF8Mw.png',
    createdBy: {
      firstname: 'Marc',
      lastname: 'Muller'
    },
    participants: [
      {
        state: 'COMPLETED'
      },
      {
        state: 'COMPLETED'
      }
    ]
  },
  {
    id: 'vYDLpjsktUzkWnvjaC4h7H',
    title: 'Survey Title 4',
    expirationTime: '2030-01-01T11:00:00',
    picture:
      'https://cdn-images-1.medium.com/max/1200/1*0nhNvVPD07JZeizhkqF8Mw.png',
    createdBy: {
      firstname: 'Marc',
      lastname: 'Muller'
    },
    participants: [
      {
        state: 'ABORTED'
      }
    ]
  }
]

storiesOf('AdminSurveysList', module).add('Story', () => (
  <AdminSurveysList
    surveys={surveys}
    handleDeleteSurvey={id => console.log('...deleting survey with the id', id)}
    showHeader
    showActions
  />
))
