import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { Table as AntTable, Tooltip, Dropdown, Menu } from 'antd'
import CopySurveyUrlButton from '../../containers/CopySurveyUrl'
import EditSurveyButton from '../../containers/EditSurveyButton'
import PauseSurveyButton from '../../containers/PauseSurveyButton'
import PhotosSurveyButton from '../../containers/PhotosSurveyButton'
import DeleteSurveyButton from '../../containers/DeleteSurveyButton'
import {
  getAuthenticatedOrganization,
  isUserAuthenticatedAsPowerUser,
  isUserAuthenticatedAsSuperAdmin,
  isUserAuthenticatedAsOperator,
  isUserAuthenticatedAsAnalytics
} from '../../utils/userAuthentication'
import IconButton from '../../components/IconButton'
import {
  FieldText,
  FieldContainer,
  TitleContainer,
  SurveyName,
  ActionsContainer,
  SurveyNameContainer,
  Item,
  SurveyImage,
  Icon
} from './styles'
import EditDraftButton from '../../containers/EditDraftButton'
import CloneSurvey from '../../containers/CloneSurvey'
import ExportSurveySharing from '../../containers/ExportSurveySharing'
import ImportProductIncentives from '../../containers/ImportProductIncentives'
import ExportProductIncentives from '../../containers/ExportProductIncentives'
import ImportSurveyShares from '../../containers/ImportSurveyShares'
import ImportGiftCardIncentives from '../../containers/ImportGiftCardIncentives'
import ExportGiftCardIncentives from '../../containers/ExportGiftCardIncentives'
import ImportGiftCardShares from '../../containers/ImportGiftCardShares'
import ExportGiftCardShares from '../../containers/ExportGiftCardShares'
import StatsButton from './StatsButton'
import ShareStatsButton from './ShareStatsButton'
import FunnelButton from './FunnelButton'
import { useTranslation } from 'react-i18next'
import ExportSurveyButton from '../../containers/ExportSurveyButton'
import ExportSurveyPhotosButton from '../../containers/ExportSurveyPhotosButton'
import moment from 'moment'
import SurveyAnalysisButton from '../../containers/SurveyAnalysisButton'
import { DEFAULT_N_ELEMENTS_PER_PAGE } from '../../utils/Constants'
import { imagesCollection } from '../../assets/png/index.js'
const { REACT_APP_THEME } = process.env
const themeBasedSettingsStatus = REACT_APP_THEME === 'default'
const getSurveyStateSuffix = state => {
  switch (state) {
    case 'draft':
      return ' (Draft)'
    case 'suspended':
      return ' (Suspended)'
    case 'deprecated':
      return ' (Deleted)'
    default:
      return ''
  }
}

const checkSpecialVisibility = () => {
  return (
    isUserAuthenticatedAsSuperAdmin() ||
    isUserAuthenticatedAsPowerUser() ||
    isUserAuthenticatedAsOperator()
  )
}

const OperatorSurveysList = ({
  surveys,
  organizations,
  total,
  page,
  onChangePage,
  showActions,
  loading,
  onClickRow = () => {},
  deleteFolder,
  editFolder,
  moveToFolder,
  currentFolder,
  isDashboard = false,
  onDoubleClickRow = () => {}
}) => {
  const isSuperAdmin = isUserAuthenticatedAsSuperAdmin()
  const isAnalytics = isUserAuthenticatedAsAnalytics()
  const mobileImage = process.env.REACT_APP_THEME
    ? imagesCollection[`${process.env.REACT_APP_THEME}Mobile`]
    : imagesCollection['defaultMobile']

  const [openDropdownId, setOpenDropdownId] = useState(false)
  const { t } = useTranslation()
  const dateFormat = 'YYYY/MM/DD-HH:mm'
  const columns = [
    {
      key: 'column_name',
      title: t('components.operatorSurveys.surveyName'),
      width: '20%',
      render: (_, survey) => {
        const isFolder = survey['type'] === 'folder'

        let typeOfProducts = t('components.operatorSurveys.singleProduct')
        if (!isFolder) {
          const {
            minimumProducts,
            products,
            isScreenerOnly,
            compulsorySurvey
          } = survey
          if (products && survey.products.length > 1) {
            typeOfProducts = t('components.operatorSurveys.multipleProducts')
            if (minimumProducts > 1) {
              typeOfProducts = t('components.operatorSurveys.multipleTastings')
            }
          }
          if (isScreenerOnly) {
            typeOfProducts = t('components.operatorSurveys.screenerOnly')
          }
          if (compulsorySurvey) {
            typeOfProducts = t('components.operatorSurveys.compulsory')
          }
        }

        return (
          <FieldContainer>
            {isFolder ? (
              <Icon type='folder' style={{ fontSize: '32px', color: '#555' }} />
            ) : survey.coverPhoto ? (
              <SurveyImage src={survey.coverPhoto} />
            ) : (
              <SurveyImage src={mobileImage} />
            )}
            <TitleContainer>
              <SurveyNameContainer>
                <Tooltip
                  placement='topLeft'
                  title={survey.name}
                  mouseEnterDelay={0.25}
                  overlayStyle={{ maxWidth: '40vw' }}
                >
                  <SurveyName>{survey.name}</SurveyName>
                </Tooltip>
                {!isFolder && (
                  <FieldText>{getSurveyStateSuffix(survey.state)}</FieldText>
                )}
              </SurveyNameContainer>
              {!isFolder && <FieldText>{typeOfProducts}</FieldText>}
            </TitleContainer>
          </FieldContainer>
        )
      }
    },
    {
      key: 'column_date',
      title: t('components.operatorSurveys.updatedAt'),
      width: '20%',
      align: 'center',
      render: (_, survey) => {
        const updated = moment(survey.openedAt).format(dateFormat)
        return (
          <FieldContainer center>
            <TitleContainer>
              <FieldText small>{`${updated}`}</FieldText>
            </TitleContainer>
          </FieldContainer>
        )
      }
    }
  ]

  const actionsColumn = {
    key: 'column_action',
    title: t('components.operatorSurveys.actions'),
    width: '20%',
    align: 'right',
    render: (_, survey, __) => {
      const isFolder = survey['type'] === 'folder'

      const closeDropdown = () => setOpenDropdownId(false)
      let menu = []

      if (isAnalytics) {
        if (!isFolder) {
          return (
            <ActionsContainer>
              <StatsButton surveyId={survey._id || survey.id} />
            </ActionsContainer>
          )
        }

        return null
      }

      if (isFolder) {
        return (
          <ActionsContainer>
            <IconButton
              tooltip={t(`tooltips.editFolder`)}
              onClick={() => editFolder(survey._id || survey.id, survey.name)}
              type='edit'
            />
            <IconButton
              tooltip={t(`tooltips.moveFolder`)}
              onClick={() => moveToFolder(survey._id || survey.id, 'folder')}
              type='folder-add'
            />
            <IconButton
              tooltip={t(`tooltips.deleteFolder`)}
              onClick={() => deleteFolder(survey._id || survey.id)}
              type='delete'
            />
          </ActionsContainer>
        )
      } else {
        const showAll = survey.owner === getAuthenticatedOrganization()

        if (!showAll) {
          return (
            <ActionsContainer>
              {['draft', 'active', 'suspended'].includes(survey.state) && (
                <StatsButton surveyId={survey._id || survey.id} />
              )}
            </ActionsContainer>
          )
        }
        const exportEligible = () => {
          if (survey && survey.screeners && survey.screeners.length) {
            return false
          } else {
            return true
          }
        }

        const hasGiftCard = () => {
          if (survey && survey.screeners && survey.screeners.length) {
            return survey.screeners[0].isGiftCardSelected
          } else {
            return survey.isGiftCardSelected
          }
        }

        const hasPayPalSelected = () => {
          if (survey && survey.screeners && survey.screeners.length) {
            return survey.screeners[0].isPaypalSelected
          } else {
            return survey.isPaypalSelected
          }
        }

        menu = (
          <React.Fragment>
            <Menu onClick={closeDropdown}>
              <Menu.ItemGroup>
                <Item
                  onItemHover={() => {}}
                  onClick={closeDropdown}
                  rootPrefixCls='ant-menu'
                  key='0'
                >
                  <CloneSurvey
                    state={survey.state}
                    surveyId={survey._id || survey.id}
                    surveyName={survey.name}
                    onMenuItemClick={closeDropdown}
                  />
                </Item>
                {checkSpecialVisibility() && (
                  <Item
                    onItemHover={() => {}}
                    onClick={closeDropdown}
                    rootPrefixCls='ant-menu'
                    key='1'
                  >
                    <ShareStatsButton
                      organizations={organizations}
                      survey={{ ...survey, id: survey.id || survey._id }}
                      onMenuItemClick={closeDropdown}
                      screeners={survey.screeners ? survey.screeners : []}
                      compulsorySurvey={survey.compulsorySurvey}
                      includeCompulsorySurveyDataInStats={
                        survey.includeCompulsorySurveyDataInStats
                      }
                    />
                  </Item>
                )}
                <Item
                  onItemHover={() => {}}
                  onClick={closeDropdown}
                  rootPrefixCls='ant-menu'
                  key='2'
                >
                  <ExportSurveyButton
                    surveyId={survey._id || survey.id}
                    surveyName={survey.name}
                    onMenuItemClick={closeDropdown}
                    screeners={survey.screeners ? survey.screeners : []}
                    compulsorySurvey={survey.compulsorySurvey}
                    includeCompulsorySurveyDataInStats={
                      survey.includeCompulsorySurveyDataInStats
                    }
                  />
                </Item>
                <Item
                  onItemHover={() => {}}
                  onClick={closeDropdown}
                  rootPrefixCls='ant-menu'
                  key='9'
                >
                  <ExportSurveyPhotosButton
                    state={survey.state}
                    surveyId={survey._id || survey.id}
                    surveyName={survey.name}
                    onMenuItemClick={closeDropdown}
                  />
                </Item>

                {isSuperAdmin && (
                  <Item
                    onItemHover={() => {}}
                    onClick={closeDropdown}
                    rootPrefixCls='ant-menu'
                    key='10'
                  >
                    <Link
                      to={`/operator/survey/${survey._id || survey.id}/logs`}
                    >
                      Survey Logs
                    </Link>
                  </Item>
                )}
              </Menu.ItemGroup>
              {checkSpecialVisibility() && (
                <Menu.ItemGroup>
                  <Menu.Divider key='22' rootPrefixCls='ant-menu' />
                  {themeBasedSettingsStatus && (
                    <Menu.Item
                      onItemHover={() => {}}
                      onClick={closeDropdown}
                      rootPrefixCls='ant-menu'
                      key='3'
                    >
                      <PhotosSurveyButton
                        surveyId={survey._id || survey.id}
                        onMenuItemClick={closeDropdown}
                      />
                    </Menu.Item>
                  )}
                  <Menu.Item
                    onItemHover={() => {}}
                    onClick={closeDropdown}
                    rootPrefixCls='ant-menu'
                    key='4'
                  >
                    <SurveyAnalysisButton
                      surveyId={survey._id || survey.id}
                      onMenuItemClick={closeDropdown}
                    />
                  </Menu.Item>
                  {exportEligible() &&
                    hasPayPalSelected() &&
                    themeBasedSettingsStatus &&
                    survey &&
                    parseInt(survey.referralAmount, 10) && (
                      <Item>
                        <ExportProductIncentives
                          state={survey.state}
                          survey={{ ...survey, id: survey.id || survey._id }}
                          onMenuItemClick={closeDropdown}
                        />
                      </Item>
                    )}
                  {exportEligible() &&
                    hasPayPalSelected() &&
                    themeBasedSettingsStatus && (
                      <Item
                        onItemHover={() => {}}
                        onClick={closeDropdown}
                        rootPrefixCls='ant-menu'
                        key='5'
                      >
                        <ImportProductIncentives
                          state={survey.state}
                          survey={{ ...survey, id: survey.id || survey._id }}
                          onMenuItemClick={closeDropdown}
                        />
                      </Item>
                    )}
                  {exportEligible() &&
                    themeBasedSettingsStatus &&
                    survey &&
                    parseInt(survey.referralAmount, 10) && (
                      <Item
                        onItemHover={() => {}}
                        onClick={closeDropdown}
                        rootPrefixCls='ant-menu'
                        key='6'
                      >
                        <ExportSurveySharing
                          state={survey.state}
                          survey={{ ...survey, id: survey.id || survey._id }}
                          onMenuItemClick={closeDropdown}
                        />
                      </Item>
                    )}
                  {exportEligible() && themeBasedSettingsStatus && (
                    <Item
                      onItemHover={() => {}}
                      onClick={closeDropdown}
                      rootPrefixCls='ant-menu'
                      key='7'
                    >
                      <ImportSurveyShares
                        state={survey.state}
                        survey={{ ...survey, id: survey.id || survey._id }}
                        onMenuItemClick={closeDropdown}
                      />
                    </Item>
                  )}
                  {exportEligible() &&
                    hasGiftCard() &&
                    themeBasedSettingsStatus && (
                      <React.Fragment>
                        {survey && parseInt(survey.referralAmount, 10) && (
                          <Item
                            onItemHover={() => {}}
                            onClick={closeDropdown}
                            rootPrefixCls='ant-menu'
                            key='13'
                          >
                            <ExportGiftCardIncentives
                              state={survey.state}
                              survey={{
                                ...survey,
                                id: survey.id || survey._id
                              }}
                              onMenuItemClick={closeDropdown}
                            />
                          </Item>
                        )}

                        <Item
                          onItemHover={() => {}}
                          onClick={closeDropdown}
                          rootPrefixCls='ant-menu'
                          key='14'
                        >
                          <ImportGiftCardIncentives
                            state={survey.state}
                            survey={{ ...survey, id: survey.id || survey._id }}
                            onMenuItemClick={closeDropdown}
                          />
                        </Item>

                        {survey && parseInt(survey.referralAmount, 10) && (
                          <Item
                            onItemHover={() => {}}
                            onClick={closeDropdown}
                            rootPrefixCls='ant-menu'
                            key='15'
                          >
                            <ExportGiftCardShares
                              state={survey.state}
                              survey={{
                                ...survey,
                                id: survey.id || survey._id
                              }}
                              onMenuItemClick={closeDropdown}
                            />
                          </Item>
                        )}

                        <Item
                          onItemHover={() => {}}
                          onClick={closeDropdown}
                          rootPrefixCls='ant-menu'
                          key='16'
                        >
                          <ImportGiftCardShares
                            state={survey.state}
                            survey={{ ...survey, id: survey.id || survey._id }}
                            onMenuItemClick={closeDropdown}
                          />
                        </Item>
                      </React.Fragment>
                    )}
                </Menu.ItemGroup>
              )}
              <Menu.ItemGroup>
                <Menu.Divider key='33' />
                <Item
                  onItemHover={() => {}}
                  onClick={closeDropdown}
                  rootPrefixCls='ant-menu'
                  key='8'
                >
                  <DeleteSurveyButton
                    state={survey.state}
                    surveyId={survey._id || survey.id}
                    surveyName={survey.name}
                    onMenuItemClick={closeDropdown}
                  />
                </Item>
              </Menu.ItemGroup>
            </Menu>
          </React.Fragment>
        )
      }

      return (
        <ActionsContainer>
          {['draft', 'active', 'suspended'].includes(survey.state) && (
            <React.Fragment>
              <StatsButton surveyId={survey._id || survey.id} />
              <FunnelButton surveyId={survey._id || survey.id} />
              {survey.state === 'draft' ? (
                <React.Fragment>
                  <CopySurveyUrlButton uniqueName={survey.uniqueName} />
                  <EditDraftButton surveyId={survey._id || survey.id} />
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <PauseSurveyButton
                    survey={{ ...survey, id: survey.id || survey._id }}
                  />
                  <CopySurveyUrlButton uniqueName={survey.uniqueName} />
                  <EditSurveyButton
                    surveyId={survey._id || survey.id}
                    surveyState={survey.state}
                  />
                </React.Fragment>
              )}
              {!isDashboard && (
                <IconButton
                  tooltip={t(`tooltips.moveFolder`)}
                  onClick={() =>
                    moveToFolder(survey.id || survey._id, 'survey')
                  }
                  type='folder-add'
                />
              )}
            </React.Fragment>
          )}
          <Dropdown
            overlay={menu}
            trigger={['click']}
            visible={
              survey._id === openDropdownId || survey.id === openDropdownId
            }
            onVisibleChange={change =>
              setOpenDropdownId(change && (survey._id || survey.id))
            }
          >
            <IconButton tooltip={t('tooltips.moreActions')} type='more' />
          </Dropdown>
        </ActionsContainer>
      )
    }
  }

  let tableProps = {}

  if (total || total === 0) {
    tableProps = {
      pagination: {
        pageSize: DEFAULT_N_ELEMENTS_PER_PAGE,
        total,
        current: page + 1
      },
      onChange: paginationConfig => {
        onChangePage(paginationConfig.current - 1)
      }
    }
  }

  return (
    <AntTable
      rowKey={record => record._id || record.id}
      columns={showActions ? [...columns, actionsColumn] : columns}
      dataSource={surveys}
      loading={loading}
      onRow={record => ({
        onClick: () => onClickRow(record._id),
        onDoubleClick: () => {
          if (record['type'] === 'folder') {
            onDoubleClickRow({
              pathName: [...currentFolder.pathName, record.name],
              pathId: [...currentFolder.pathId, record._id]
            })
          }
        }
      })}
      {...tableProps}
    />
  )
}

export default OperatorSurveysList
