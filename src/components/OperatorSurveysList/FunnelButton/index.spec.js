import React from 'react'
import { mount } from 'enzyme'
import FunnelButton from '.'
import { Router } from 'react-router-dom'
import IconButton from '../../../components/IconButton'
import wait from '../../../utils/testUtils/waait'

describe('FunnelButton', () => {
  let testRender
  let surveyId

  beforeEach(() => {
    surveyId = 1
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render FunnelButton', async () => {
    const history = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }

    testRender = mount(
      <Router history={history}>
        <FunnelButton surveyId={surveyId} />
      </Router>
    )
    expect(testRender.find(FunnelButton)).toHaveLength(1)

    testRender.find(IconButton).simulate('click')

    await wait(0)

    expect(history.push).toHaveBeenCalledWith('/operator/survey/funnel/1')
  })
})
