import React, { useState, useEffect } from 'react'
import { Formik } from 'formik'
import { Row, Col, Form, Icon } from 'antd'
import Input from '../Input'
import Select from '../Select'
import InputRichText from '../InputRichText'
import InputList from '../InputList'
import Checkbox from '../Checkbox'
import FieldLabel from '../FieldLabel'
import toKebabCase from 'lodash.kebabcase'
import { getOptionValue } from '../../utils/getters/OptionGetters'
import { options } from '../../utils/internationalization/index'
import { useTranslation } from 'react-i18next'
import defaults from '../../defaults'
import UploadSurveyCoverPicture from '../../containers/UploadSurveyCoverPicture'

import { Container, CheckboxContainer, Visibility, CopyButton } from './styles'
import { validateSurveyBasics } from '../../validates'
import rowSize from '../../utils/rowSize'
import InputEmails from '../InputEmails'
import capitalize from 'lodash.capitalize'
import { CopyToClipboard as Copy } from 'react-copy-to-clipboard'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'

const isFlavorwiki = process.env.REACT_APP_THEME === 'default'

const SurveyBasicInfoForm = ({
  authUser,
  isNewSurvey,
  coverPhoto,
  onChange,
  name,
  title,
  instructionsText,
  instructionSteps,
  thankYouText,
  rejectionText,
  screeningText,
  uniqueName,
  authorizationType,
  exclusiveTasters,
  allowRetakes,
  // forcedAccount,
  // forcedAccountLocation,
  recaptcha,
  surveyLanguage,
  customButtons = {},
  handleCustomButtonChange,
  handleLanguageChange,
  isUniqueNameDuplicated,
  customizeSharingMessage,
  loginText,
  pauseText,
  allowedDaysToFillTheTasting,
  country,
  isScreenerOnly,
  optionDisplayType,
  isDraft,
  retakeAfter,
  maxRetake,
  maximumReward,
  isFlavorwikiOrganization = false
}) => {
  const { t } = useTranslation()
  const [isValidate, setisValidate] = useState(false)
  const [isUniqueNameTouched, setisUniqueNameTouched] = useState(false)
  const [isLinkValid, setIsLinkValid] = useState(
    uniqueName && uniqueName.length ? true : false
  )
  const { REACT_APP_THEME } = process.env
  const { spanValue, fieldValue } = rowSize()
  const [isTitleTouched, setisTitleTouched] = useState(false)

  const defaultTheme = REACT_APP_THEME === 'default'
  useEffect(() => {
    if (!uniqueName && isLinkValid) {
      setIsLinkValid(false)
    }
    if (uniqueName && uniqueName.length && !isLinkValid) {
      setIsLinkValid(true)
    }
  }, [uniqueName])

  useEffect(() => {
    if (isScreenerOnly) {
      onChange({ allowRetakes: false })
    }
  }, [isScreenerOnly])

  return (
    <Formik
      enableReinitialize
      validationSchema={validateSurveyBasics(isValidate)}
      initialValues={{
        name,
        title,
        uniqueName,
        authorizationType,
        exclusiveTasters,
        instructionsText,
        instructionSteps,
        thankYouText,
        rejectionText,
        screeningText,
        surveyLanguage,
        customizeSharingMessage,
        loginText,
        pauseText,
        customButtonsContinue: customButtons.continue,
        customButtonsStart: customButtons.start,
        customButtonsNext: customButtons.next,
        customButtonsSkip: customButtons.skip,
        allowedDaysToFillTheTasting,
        country,
        coverPhoto,
        retakeAfter,
        maxRetake,
        maximumReward
      }}
      render={({ values, errors, setFieldValue, handleBlur }) => {
        const {
          name: nameError,
          title: titleError,
          uniqueName: uniqueNameError,
          allowedDaysToFillTheTasting: allowedDaysToFillTheTastingErrors,
          instructionSteps: instructionStepsError,
          exclusiveTasters: exclusiveTastersError,
          retakeAfter: retakeAfterError,
          maxRetake: maxRetakeError,
          maximumReward: maximumRewardError
        } = errors
        return (
          <Container>
            <Row gutter={24}>
              <Col xs={{ span: 16 }}>
                <Row gutter={spanValue}>
                  <Col xs={{ span: fieldValue }}>
                    <Form.Item
                      help={nameError}
                      validateStatus={nameError ? 'error' : 'success'}
                    >
                      <Input
                        name='name'
                        value={values.name}
                        onChange={event => {
                          setFieldValue('name', event.target.value)
                          const changes = { name: event.target.value }

                          if (isNewSurvey) {
                            const date = new Date()
                            const uniqueName = !isUniqueNameTouched
                              ? event.target.value
                                ? `${toKebabCase(
                                    event.target.value
                                  )}-${date.getDate()}${date.getHours()}${date.getMinutes()}`
                                : ''
                              : values.uniqueName
                            setFieldValue('uniqueName', uniqueName)
                            setIsLinkValid(
                              uniqueName && uniqueName.length ? true : false
                            )
                            changes.uniqueName = uniqueName
                            const title = event.target.value
                            if (title.length <= 60 && !isTitleTouched) {
                              setFieldValue('title', title)
                              changes.title = title
                            }
                          }
                          onChange(changes)
                          setisValidate(true)
                        }}
                        label={t('components.surveyBasicInfoForm.name')}
                        tooltip={t('tooltips.surveyBasicInfoForm.name')}
                        size='default'
                        required
                      />
                    </Form.Item>
                  </Col>
                  <Col xs={{ span: fieldValue }}>
                    <Form.Item
                      help={
                        uniqueNameError ||
                        (isUniqueNameDuplicated &&
                          t('components.surveyBasicInfoForm.uniqueNameExists'))
                      }
                      validateStatus={
                        isUniqueNameDuplicated || uniqueNameError
                          ? 'error'
                          : 'success'
                      }
                    >
                      <Input
                        disabled={!isNewSurvey}
                        tooltip={
                          isNewSurvey
                            ? t('tooltips.surveyBasicInfoForm.uniqueName')
                            : t('tooltips.surveyBasicInfoForm.staticUniqueName')
                        }
                        name='uniqueName'
                        value={values.uniqueName}
                        onChange={event => {
                          const newUniqueName = event.target.value
                          setFieldValue('uniqueName', newUniqueName)
                          setisUniqueNameTouched(!!newUniqueName)
                          onChange({ uniqueName: newUniqueName })
                        }}
                        label={t('components.surveyBasicInfoForm.uniqueName')}
                        size='default'
                        required
                      />
                    </Form.Item>
                  </Col>
                  {isFlavorwiki && isFlavorwikiOrganization && (
                    <Col xs={{ span: 8 }} justify='center'>
                      <Select
                        data-testid='authorization-type-select'
                        name='authorizationType'
                        value={authorizationType}
                        onChange={type => {
                          onChange({ authorizationType: type })
                        }}
                        label={t(
                          'components.surveyBasicInfoForm.authorizationType'
                        )}
                        tooltip={t(
                          'tooltips.surveyBasicInfoForm.authorizationType'
                        )}
                        size='default'
                        options={['public', 'enrollment', 'selected']}
                        getOptionName={option => {
                          return capitalize(option)
                        }}
                      />
                    </Col>
                  )}
                </Row>
                {isFlavorwiki && isFlavorwikiOrganization && (
                  <Row gutter={24}>
                    <Col xs={{ span: 8 }}>
                      <Form.Item
                        help={maximumRewardError}
                        validateStatus={
                          maximumRewardError ? 'error' : 'success'
                        }
                      >
                        <Input
                          type='number'
                          min={0}
                          step={1}
                          name='maximumReward'
                          value={values.maximumReward}
                          onChange={event => {
                            const value =
                              event.target.value &&
                              parseInt(event.target.value, 10)
                            setFieldValue('maximumReward', title)
                            onChange({ maximumReward: value })
                          }}
                          label={t(
                            'components.surveyBasicInfoForm.maximumReward'
                          )}
                          size='default'
                        />
                      </Form.Item>
                    </Col>
                  </Row>
                )}
              </Col>
              <Col xs={{ span: 8 }}>
                <UploadSurveyCoverPicture
                  isSurvey
                  onChange={photo => {
                    onChange({ coverPhoto: `${photo}` })
                  }}
                  value={values.coverPhoto}
                />
              </Col>
            </Row>

            <Row gutter={24}>
              <Col xs={{ span: 8 }}>
                <Form.Item
                  help={titleError}
                  validateStatus={titleError ? 'error' : 'success'}
                >
                  <Input
                    name='tite'
                    value={values.title}
                    onChange={event => {
                      const title = event.target.value
                      if (title.length <= 30) {
                        setisTitleTouched(!!title)
                        setFieldValue('title', title)
                        onChange({ title: title })
                      }
                    }}
                    label={t('components.surveyBasicInfoForm.title')}
                    tooltip={t('tooltips.surveyBasicInfoForm.title')}
                    size='default'
                    required
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={{ span: 8 }}>
                <Visibility isVisible={isLinkValid}>
                  <Form.Item>
                    <Input
                      disabled
                      label={t('components.surveyBasicInfoForm.surveyUrl')}
                      size='default'
                      data-testid='redirect-link'
                      value={`${window.location.origin}/survey/${values.uniqueName}`}
                      addonAfter={
                        <Copy
                          text={`${window.location.origin}/survey/${values.uniqueName}`}
                          onCopy={() => {
                            displaySuccessMessage(
                              t(
                                'components.surveyBasicInfoForm.copySuccessfull'
                              )
                            )
                          }}
                        >
                          <CopyButton>
                            <Icon type='copy' />{' '}
                            {t('components.surveyBasicInfoForm.copy')}
                          </CopyButton>
                        </Copy>
                      }
                    />
                  </Form.Item>
                </Visibility>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={{ span: 24 }}>
                <Form.Item>
                  <FieldLabel
                    label={t('components.surveyBasicInfoForm.securitySettings')}
                    tooltip={t('tooltips.surveyBasicInfoForm.securitySettings')}
                  >
                    {authorizationType !== 'selected' && (
                      <CheckboxContainer>
                        <Checkbox
                          checked={recaptcha}
                          onChange={value => {
                            onChange({ recaptcha: value })
                          }}
                        >
                          {t('components.surveyBasicInfoForm.requireRecaptcha')}
                        </Checkbox>
                      </CheckboxContainer>
                    )}
                    {(authorizationType === 'selected' ||
                      authorizationType === 'enrollment') &&
                      !isScreenerOnly && (
                        <CheckboxContainer>
                          <Checkbox
                            checked={allowRetakes}
                            onChange={value => {
                              onChange({ allowRetakes: value })
                            }}
                          >
                            {t(
                              'components.surveyBasicInfoForm.takeSurveyMultipleTimes'
                            )}
                          </Checkbox>
                        </CheckboxContainer>
                      )}
                  </FieldLabel>
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              {(authorizationType === 'selected' ||
                authorizationType === 'enrollment') &&
                allowRetakes && (
                  <Col xs={{ span: 8 }}>
                    <Form.Item
                      help={retakeAfterError}
                      validateStatus={retakeAfterError ? 'error' : 'success'}
                    >
                      <Input
                        name='retakeAfter'
                        value={values.retakeAfter}
                        onChange={event => {
                          const retakeAfter = parseInt(event.target.value, 10)
                          setFieldValue('retakeAfter', retakeAfter)
                          onChange({ retakeAfter })
                        }}
                        type={`number`}
                        label={t('components.surveyBasicInfoForm.retakeAfter')}
                        size='default'
                        min={0}
                        required
                      />
                    </Form.Item>
                  </Col>
                )}
              {(authorizationType === 'selected' ||
                authorizationType === 'enrollment') &&
                allowRetakes && (
                  <Col xs={{ span: 8 }}>
                    <Form.Item
                      help={maxRetakeError}
                      validateStatus={maxRetakeError ? 'error' : 'success'}
                    >
                      <Input
                        name='maxRetake'
                        value={values.maxRetake}
                        onChange={event => {
                          const maxRetake = parseInt(event.target.value, 10)
                          setFieldValue('maxRetake', maxRetake)
                          onChange({ maxRetake })
                        }}
                        type={`number`}
                        min={1}
                        max={365}
                        label={t(
                          'components.surveyBasicInfoForm.maximumRetake'
                        )}
                        size='default'
                        required
                      />
                    </Form.Item>
                  </Col>
                )}
            </Row>
            {authorizationType === 'selected' && isDraft && (
              <Row gutter={24}>
                <Col xs={{ span: 24 }}>
                  <Form.Item
                    help={exclusiveTastersError}
                    validateStatus={exclusiveTastersError ? 'error' : 'success'}
                  >
                    <InputEmails
                      label={t('components.surveyBasicInfoForm.allowedUsers')}
                      name='exclusiveTasters'
                      value={values.exclusiveTasters}
                      onBlur={handleBlur}
                      onChange={emails => {
                        setFieldValue('exclusiveTasters', emails)
                        onChange({
                          exclusiveTasters: emails
                        })
                      }}
                    />
                  </Form.Item>
                </Col>
              </Row>
            )}
            {(authorizationType === 'enrollment' ||
              authorizationType === 'selected') && (
              <Row gutter={8}>
                <Col xs={{ span: 8 }}>
                  <Form.Item
                    help={allowedDaysToFillTheTastingErrors}
                    validateStatus={
                      allowedDaysToFillTheTastingErrors ? 'error' : 'success'
                    }
                  >
                    <Input
                      label={t(
                        'components.surveyBasicInfoForm.allowedDaysToFillTheTasting'
                      )}
                      name='allowedDaysToFillTheTasting'
                      value={values.allowedDaysToFillTheTasting}
                      onBlur={handleBlur}
                      type={`number`}
                      onChange={event => {
                        const newAllowedDaysToFillTheTasting = parseInt(
                          event.target.value,
                          10
                        )
                        if (
                          newAllowedDaysToFillTheTasting.toString().length < 10
                        ) {
                          setFieldValue(
                            'allowedDaysToFillTheTasting',
                            newAllowedDaysToFillTheTasting
                          )
                          onChange({
                            allowedDaysToFillTheTasting: newAllowedDaysToFillTheTasting
                          })
                        }
                      }}
                      required
                      min={1}
                      max={712}
                      size={`default`}
                    />
                  </Form.Item>
                </Col>
              </Row>
            )}
            <Row gutter={24}>
              <Col xs={{ span: 24 }}>
                <Form.Item>
                  <InputList
                    errors={instructionStepsError}
                    label={t('components.surveyBasicInfoForm.instructionSteps')}
                    tooltip={t('tooltips.surveyBasicInfoForm.instructionSteps')}
                    name='instructionSteps'
                    value={values.instructionSteps}
                    onChange={value => {
                      setFieldValue('instructionSteps', value)
                      onChange({ instructionSteps: value })
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={{ span: 24 }}>
                <Form.Item>
                  <InputRichText
                    label={t('components.surveyBasicInfoForm.instruction')}
                    tooltip={t('tooltips.surveyBasicInfoForm.instruction')}
                    name='instructionsText'
                    value={values.instructionsText}
                    onChange={value => {
                      setFieldValue('instructionsText', value)
                      onChange({ instructionsText: value })
                    }}
                    size='300'
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={{ span: 24 }}>
                <Form.Item>
                  <InputRichText
                    label={t('components.surveyBasicInfoForm.thankYou')}
                    tooltip={t('tooltips.surveyBasicInfoForm.thankYou')}
                    name='thankYouText'
                    value={values.thankYouText}
                    onChange={value => {
                      setFieldValue('thankYouText', value)
                      onChange({ thankYouText: value })
                    }}
                    size='300'
                  />
                </Form.Item>
              </Col>
            </Row>
            {defaultTheme && (
              <Row gutter={24}>
                <Col xs={{ span: 24 }}>
                  <Form.Item>
                    <InputRichText
                      label={t('components.surveyBasicInfoForm.rejection')}
                      tooltip={t('tooltips.surveyBasicInfoForm.rejection')}
                      name='rejectionText'
                      value={values.rejectionText || ''}
                      onChange={value => {
                        setFieldValue('rejectionText', value)
                        onChange({ rejectionText: value })
                      }}
                      size='300'
                    />
                  </Form.Item>
                </Col>
              </Row>
            )}
            <Row gutter={24}>
              <Col xs={{ span: 24 }}>
                <Form.Item>
                  <InputRichText
                    label={t('components.surveyBasicInfoForm.screening')}
                    tooltip={t('tooltips.surveyBasicInfoForm.screening')}
                    name='screeningText'
                    value={values.screeningText || ''}
                    onChange={value => {
                      setFieldValue('screeningText', value)
                      onChange({ screeningText: value })
                    }}
                    size='300'
                  />
                </Form.Item>
              </Col>
            </Row>
            {defaultTheme && isFlavorwikiOrganization && (
              <Row gutter={24}>
                <Col xs={{ span: 24 }}>
                  <Form.Item>
                    <InputRichText
                      label={t(
                        'components.surveyBasicInfoForm.customizeSharing.title'
                      )}
                      tooltip={t(
                        'tooltips.surveyBasicInfoForm.customizeSharing'
                      )}
                      name='customizeSharingMessage'
                      value={values.customizeSharingMessage || ''}
                      onChange={value => {
                        setFieldValue('customizeSharingMessage', value)
                        onChange({ customizeSharingMessage: value })
                      }}
                      size='300'
                    />
                  </Form.Item>
                </Col>
              </Row>
            )}
            {isFlavorwikiOrganization && (
              <Row gutter={24}>
                <Col xs={{ span: 24 }}>
                  <Form.Item>
                    <InputRichText
                      label={t('components.surveyBasicInfoForm.loginText')}
                      tooltip={t('tooltips.surveyBasicInfoForm.loginText')}
                      name='loginText'
                      value={values.loginText || ''}
                      onChange={value => {
                        setFieldValue('loginText', value)
                        onChange({ loginText: value })
                      }}
                      size='300'
                    />
                  </Form.Item>
                </Col>
              </Row>
            )}
            <Row gutter={24}>
              <Col xs={{ span: 24 }}>
                <Form.Item>
                  <InputRichText
                    label={t('components.surveyBasicInfoForm.pauseText')}
                    tooltip={t('tooltips.surveyBasicInfoForm.pauseText')}
                    name='pauseText'
                    value={values.pauseText || ''}
                    onChange={value => {
                      setFieldValue('pauseText', value)
                      onChange({ pauseText: value })
                    }}
                    size='300'
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={{ span: 6 }}>
                <Form.Item>
                  <Select
                    name='surveyLanguage'
                    label={t('components.surveyBasicInfoForm.selectLanguage')}
                    tooltip={t('tooltips.surveyBasicInfoForm.selectLanguage')}
                    size={`default`}
                    value={surveyLanguage}
                    onChange={type => {
                      handleLanguageChange(
                        type,
                        setFieldValue,
                        'surveyLanguage'
                      )
                    }}
                    placeholder={t('components.surveyBasicInfoForm.language')}
                    options={options}
                    getOptionValue={getOptionValue}
                    getOptionName={(option = {}) =>
                      t(option.label) ? t(option.label) : option.label
                    }
                  />
                </Form.Item>
              </Col>
              <Col xs={{ span: 6 }}>
                <Form.Item>
                  <Select
                    options={defaults.countries}
                    name='country'
                    value={values.country}
                    onChange={value => {
                      setFieldValue('country', value || '$')
                      onChange({ country: value || '$' })
                    }}
                    disabled={authUser && authUser.isDummyTaster}
                    getOptionName={option => option.name}
                    getOptionValue={option => option.code}
                    label={t('components.surveyBasicInfoForm.country')}
                    tooltip={t('tooltips.surveyBasicInfoForm.country')}
                    placeholder={t('placeholders.availableInCountry')}
                    size={`default`}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={{ span: 6 }}>
                <Form.Item
                  help={errors && errors.customButtonsContinue}
                  validateStatus={
                    errors && errors.customButtonsContinue ? 'error' : 'success'
                  }
                >
                  <Input
                    label={t('components.surveyBasicInfoForm.continueButton')}
                    tooltip={t('tooltips.surveyBasicInfoForm.continueButton')}
                    name='customButtons.continue'
                    value={values.customButtonsContinue}
                    placeholder={t(
                      'defaultValues.default.customButtons.continue'
                    )}
                    size={`default`}
                    onChange={event => {
                      setFieldValue('customButtonsContinue', event.target.value)
                      handleCustomButtonChange('continue', event.target.value)
                    }}
                  />
                </Form.Item>
              </Col>
              <Col xs={{ span: 6 }}>
                <Form.Item
                  help={errors && errors.customButtonsStart}
                  validateStatus={
                    errors && errors.customButtonsStart ? 'error' : 'success'
                  }
                >
                  <Input
                    label={t(
                      'components.surveyBasicInfoForm.startSurveyButton'
                    )}
                    tooltip={t(
                      'tooltips.surveyBasicInfoForm.startSurveyButton'
                    )}
                    name='customButtons.start'
                    value={values.customButtonsStart}
                    placeholder={t('defaultValues.default.customButtons.start')}
                    size={`default`}
                    onChange={event => {
                      setFieldValue('customButtonsStart', event.target.value)
                      handleCustomButtonChange('start', event.target.value)
                    }}
                  />
                </Form.Item>
              </Col>
              <Col xs={{ span: 6 }}>
                <Form.Item
                  help={errors && errors.customButtonsNext}
                  validateStatus={
                    errors && errors.customButtonsNext ? 'error' : 'success'
                  }
                >
                  <Input
                    label={t('components.surveyBasicInfoForm.nextButton')}
                    tooltip={t('tooltips.surveyBasicInfoForm.nextButton')}
                    name='customButtons.next'
                    value={values.customButtonsNext}
                    placeholder={t('defaultValues.default.customButtons.next')}
                    size={`default`}
                    onChange={event => {
                      setFieldValue('customButtonsNext', event.target.value)
                      handleCustomButtonChange('next', event.target.value)
                    }}
                  />
                </Form.Item>
              </Col>
              <Col xs={{ span: 6 }}>
                <Form.Item
                  help={errors && errors.customButtonsSkip}
                  validateStatus={
                    errors && errors.customButtonsSkip ? 'error' : 'success'
                  }
                >
                  <Input
                    label={t('components.surveyBasicInfoForm.skipButton')}
                    tooltip={t('tooltips.surveyBasicInfoForm.skipButton')}
                    name='customButtons.skip'
                    value={values.customButtonsSkip}
                    placeholder={t('defaultValues.default.customButtons.skip')}
                    size={`default`}
                    onChange={event => {
                      setFieldValue('customButtonsSkip', event.target.value)
                      handleCustomButtonChange('skip', event.target.value)
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Container>
        )
      }}
    />
  )
}

export default SurveyBasicInfoForm
