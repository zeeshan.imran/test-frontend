import React from 'react'
import { mount } from 'enzyme'
import StoresTable, { columns, showDeleteStoreAlert } from '.'
import { Action } from './styles'
import AlertModal from '../../AlertModal'

jest.mock('../../AlertModal')
jest.mock('../../../utils/internationalization/i18n')

const mockAlertModal = ({ handleOk, handleCancel }) => {
  handleOk()
  handleCancel()
}
AlertModal.mockImplementation(mockAlertModal)

describe('StoresTable', () => {
  let testRender
  let stores
  let handleChange
  let handleEditStore
  let handleDeleteStore

  beforeEach(() => {
    stores = [
      {
        id: 'store-1',
        availableInCountry: 'us',
        date: null
      }
    ]
    handleChange = jest.fn()
    handleEditStore = jest.fn()
    handleDeleteStore = jest.fn()
  })
  test('should render StoresTable', async () => {
    testRender = mount(
      <StoresTable
        stores={stores}
        handleChange={handleChange}
        handleEditStore={handleEditStore}
        handleDeleteStore={handleDeleteStore}
      />
    )
    expect(testRender.find(StoresTable)).toHaveLength(1)
    expect(
      testRender
        .find(Action)
        .last()
        .text()
    ).toBe('')

    testRender.unmount()
  })
  test('should call on click when edit', async () => {
    testRender = mount(
      <StoresTable stores={stores} handleEditStore={handleEditStore} />
    )

    testRender
      .find(Action)
      .first()
      .simulate('click')

    expect(handleEditStore).toHaveBeenCalledWith('store-1')
    testRender.unmount()
  })
  test('should call on click when remove', async () => {
    testRender = mount(
      <StoresTable stores={stores} handleDeleteStore={handleDeleteStore} />
    )
    testRender
      .find(Action)
      .last()
      .simulate('click')

    expect(handleDeleteStore).toHaveBeenCalledWith('store-1')
    testRender.unmount()
  })

  test('should sort', () => {
    const now = new Date()
    const cols = columns(jest.fn(), jest.fn())
    const sorter = cols[3]['sorter']
    expect(sorter({ date: now }, { date: now })).toBe(0)
  })

  test('should handle ok and cancel', () => {
    showDeleteStoreAlert(handleDeleteStore, 'store-id')
    expect(AlertModal).toHaveBeenCalled()
    expect(handleDeleteStore).toHaveBeenCalledWith('store-id')
  })
})
