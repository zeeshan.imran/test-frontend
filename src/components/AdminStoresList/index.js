import React, { Fragment } from 'react'
import SelectedItemsBanner from '../SelectedItemsBanner'
import StoresTable from './StoresTable'

const AdminStoresList = ({ stores, selectedStores, ...tableProps }) => (
  <Fragment>
    <SelectedItemsBanner selectedItems={selectedStores.length} />
    <StoresTable stores={stores} {...tableProps} />
  </Fragment>
)

export default AdminStoresList
