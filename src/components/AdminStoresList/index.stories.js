import React, { Component } from 'react'
import { storiesOf } from '@storybook/react'
import AdminStoresList from './'

const stores = [
  {
    name: 'Migros',
    id: 'wL25wwMTw4SxvGqxdBseqT',
    picture:
      'https://www.turkeycentral.com/uploads/company_cover/monthly_2016_06/migros-logo.png.205d0be987b7040287fbf488a8067540.png',
    date: '2018-12-16',
    availableInCountry: {
      key: 'DE',
      value: 'Germany',
      state: 'Frankfurt'
    }
  },
  {
    name: 'Migros',
    id: 'Ytx2Tsyo4yvJgRGdjdzTJn',
    picture:
      'https://www.turkeycentral.com/uploads/company_cover/monthly_2016_06/migros-logo.png.205d0be987b7040287fbf488a8067540.png',
    date: '2018-12-10',
    availableInCountry: {
      key: 'CH',
      value: 'Switzerland',
      state: 'TI'
    }
  }
]

class StateWrapper extends Component {
  state = {
    selectedStores: []
  }

  render () {
    return this.props.children({
      setSelectedStores: selectedStores => this.setState({ selectedStores }),
      selectedStores: this.state.selectedStores
    })
  }
}

storiesOf('AdminStoresList', module).add('Story', () => (
  <StateWrapper>
    {({ setSelectedStores, selectedStores }) => {
      return (
        <AdminStoresList
          stores={stores}
          selectedStores={selectedStores}
          handleChange={setSelectedStores}
          handleDeleteStore={id =>
            console.log('...deleting store with the id', id)
          }
          handleEditStore={id =>
            console.log('...editing store with the id', id)
          }
        />
      )
    }}
  </StateWrapper>
))
