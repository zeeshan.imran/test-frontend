import React from 'react'
import { mount } from 'enzyme'
import SearchBar from '.'
import { Search } from './styles'
import wait from '../../utils/testUtils/waait'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'

describe('SearchBar', () => {
  let testRender
  let search
  let client

  beforeEach(() => {
    search = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render without crash', () => {
    client = createApolloMockClient()

    testRender = mount(
      <ApolloProvider client={client}>
        <SearchBar handleChange={search} />
      </ApolloProvider>
    )

    expect(testRender.find(Search)).toHaveLength(1)
  })

  test('should render with search icon without crash', () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <SearchBar withIcon handleChange={search} />
      </ApolloProvider>
    )

    expect(testRender.find(Search)).toHaveLength(1)
  })

  test('should debounce the keyword when the user is typing', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <SearchBar handleChange={search} />
      </ApolloProvider>
    )

    expect(testRender.find(Search)).toHaveLength(1)

    testRender.find(Search).simulate('change', { target: { value: 'key' } })
    await wait(200)
    expect(search).not.toHaveBeenCalled()

    testRender.find(Search).simulate('change', { target: { value: 'keywo' } })
    await wait(200)
    expect(search).not.toHaveBeenCalled()

    // longtime after releasing the key, the search will be trigger
    testRender.find(Search).simulate('change', { target: { value: 'keyword' } })
    await wait(1000)
    expect(search).toHaveBeenCalledTimes(1)
    expect(search).toHaveBeenCalledWith('keyword')
  })
})
