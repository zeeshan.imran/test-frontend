import styled from 'styled-components'
import { Input as AntInput } from 'antd'
import { family } from '../../utils/Fonts'
import colors from '../../utils/Colors'

export const Search = styled(AntInput)`
  .ant-input {
    font-family: ${family.primaryRegular};
    font-size: 1.4rem;
    &:focus {
      border-color: ${colors.INPUT_BORDER_COLOR} !important;
    }
    &:hover {
      border-color: ${colors.INPUT_BORDER_COLOR} !important;
    }
  }
  .ant-input-suffix {
    color: rgba(0, 0, 0, 0.25);
  }
  .ant-input::-moz-placeholder {
    font-family: ${family.primaryRegular};
    font-size: 1.4rem;
    color: rgba(0, 0, 0, 0.25);
    opacity: 1;
  }
  .ant-input:-ms-input-placeholder {
    font-family: ${family.primaryRegular};
    font-size: 1.4rem;
    color: rgba(0, 0, 0, 0.25);
  }
  .ant-input::-webkit-input-placeholder {
    font-family: ${family.primaryRegular};
    font-size: 1.4rem;
    color: rgba(0, 0, 0, 0.25);
  }
`
