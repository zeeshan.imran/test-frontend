import React, { useState, useEffect } from 'react'
import { Formik } from 'formik'
import { Switch, DatePicker } from 'antd'
import { map, when, any, propEq, find } from 'ramda'
import Button from '../Button'
import {
  displayErrorPopup,
  distroyErrorPopup
} from '../../utils/displayErrorPopup'
import CreationButtonStatsContainer from '../CreationButtonStatsContainer'
import { useTranslation } from 'react-i18next'
import {
  RowLabel,
  RowHeader,
  RowContent,
  RowStarter,
  SwitchArea,
  StyledModal
} from './styles'
import { PRODUCT_LIMIT_MIN_STATS } from '../../utils/Constants'
import { StyledCheckbox } from '../StyledCheckBox'
const { REACT_APP_THEME } = process.env
const StatsFilterModal = ({
  finalQuestionsSelected,
  finalDateFilter,
  modalHeaderTitle,
  questionList,
  submitText,
  resetText,
  onSubmit,
  onReset,
  hasProductSelection,
  shouldGenerateCharts,
  setShouldGenerateCharts,
  productsSelected,
  data
}) => {
  const [questionsSelected, setQuestionsSelected] = useState(
    finalQuestionsSelected
  )
  const [isDateFilterEnabled, setIsDateFilterEnabled] = useState(false)
  const [dateFilter, setDateFilter] = useState(finalDateFilter)
  const [showFilterModal, setShowFilterModal] = useState(false)
  const { t } = useTranslation()
  useEffect(() => {
    setQuestionsSelected(finalQuestionsSelected)
    setDateFilter(finalDateFilter)
    setIsDateFilterEnabled(finalDateFilter.length > 0)
  }, [showFilterModal, finalQuestionsSelected, finalDateFilter])

  return (
    <React.Fragment>
      <CreationButtonStatsContainer
        hasProductSelection={hasProductSelection}
        shouldGenerateCharts={shouldGenerateCharts}
        productButtonClick={() => {
          if (shouldGenerateCharts) {
            setShouldGenerateCharts(false)
          } else {
            if (
              productsSelected.length < PRODUCT_LIMIT_MIN_STATS ||
              productsSelected.length > data.survey.maxProductStatCount
            ) {
              return displayErrorPopup(
                t('containers.surveyStats.productsTab.maxNumberError', {
                  minNumber: PRODUCT_LIMIT_MIN_STATS,
                  maxNumber: data.survey.maxProductStatCount,
                  currentNumber: productsSelected.length
                })
              )
            } else {
              distroyErrorPopup()
            }
            setShouldGenerateCharts(true)
          }
        }}
        productButtonText={
          shouldGenerateCharts
            ? t('containers.surveyStats.productsTab.backToProductSelection')
            : t('containers.surveyStats.productsTab.buttonText')
        }
        filterButtonClick={() => {
          setShowFilterModal(true)
        }}
        filterButtonText={t('containers.surveyStats.otherFilters.buttonText')}
      />
      {showFilterModal && (
        <StyledModal
          title={modalHeaderTitle}
          visible
          width={800}
          footer={[
            <Button
              key={`reset`}
              type={`secondary`}
              onClick={() => {
                onReset()
                setShowFilterModal(false)
              }}
            >
              {resetText}
            </Button>,
            <Button
              key={`submit`}
              onClick={() => {
                onSubmit(
                  questionsSelected,
                  isDateFilterEnabled ? dateFilter : []
                )
                setShowFilterModal(false)
              }}
            >
              {submitText}
            </Button>
          ]}
          onCancel={() => {
            setShowFilterModal(false)
          }}
        >
          <Formik
            render={() => {
              return (
                <React.Fragment>
                  {REACT_APP_THEME === 'default' ? (
                    <RowStarter>
                      <StyledCheckbox
                        name='isDateFilterEnabled'
                        checked={isDateFilterEnabled}
                        onChange={e => {
                          const { checked: value } = e.target
                          setIsDateFilterEnabled(value)
                        }}
                      >
                        {'Date filter'}
                      </StyledCheckbox>
                      {isDateFilterEnabled && (
                        <DatePicker.RangePicker
                          value={dateFilter}
                          onChange={dateRange => setDateFilter(dateRange)}
                        />
                      )}
                    </RowStarter>
                  ) : null}
                  {questionList.map((question, index) => {
                    return (
                      <RowStarter key={index}>
                        <RowHeader>{question.chartTitle}</RowHeader>
                        <RowContent>
                          {question.options.map((option, optionIndex) => {
                            const matchQuestion = propEq(
                              'question',
                              question.id
                            )

                            const questionFilter = find(
                              matchQuestion,
                              questionsSelected
                            )

                            const checked = questionFilter
                              ? questionFilter.options.includes(option.value)
                              : false

                            return (
                              <SwitchArea key={optionIndex}>
                                <Switch
                                  checked={checked}
                                  size={`small`}
                                  onChange={checked => {
                                    const isAlreadyAdded = any(
                                      matchQuestion,
                                      questionsSelected
                                    )

                                    if (!isAlreadyAdded) {
                                      setQuestionsSelected([
                                        ...questionsSelected,
                                        {
                                          question: question.id,
                                          options: [option.value]
                                        }
                                      ])
                                      return
                                    }

                                    let updater
                                    if (checked) {
                                      updater = question => ({
                                        ...question,
                                        options: [
                                          ...question.options,
                                          option.value
                                        ]
                                      })
                                    } else {
                                      updater = question => ({
                                        ...question,
                                        options: question.options.filter(
                                          o => o !== option.value
                                        )
                                      })
                                    }

                                    setQuestionsSelected(
                                      map(when(matchQuestion, updater))(
                                        questionsSelected
                                      )
                                    )
                                  }}
                                />
                                <RowLabel>{option.label}</RowLabel>
                              </SwitchArea>
                            )
                          })}
                        </RowContent>
                      </RowStarter>
                    )
                  })}
                </React.Fragment>
              )
            }}
          />
        </StyledModal>
      )}
    </React.Fragment>
  )
}

export default StatsFilterModal
