import styled from 'styled-components'
import Text from '../Text'
import { Modal as AntModal } from 'antd'

export const RowStarter = styled.div`
  margin-top: 3rem;
  &:first-child {
    margin-top: 0;
  }
  .ant-checkbox-wrapper {
    margin-right: 16px;
    padding: 7px 0;
  }
  .ant-calendar-picker-input.ant-input {
    line-height: 1.8;
  }
  .ant-calendar-picker {
    width: 320px;
  }
`
export const RowContent = styled.div``
export const SwitchArea = styled.div`
  margin-left: 1.5rem;
  display: inline-block;
`
export const StyledModal = styled(AntModal)``
export const RowHeader = styled(Text)`
  display: block;
  color: #aaa;
`
export const RowLabel = styled(Text)`
  margin-left: 0.5rem;
`
