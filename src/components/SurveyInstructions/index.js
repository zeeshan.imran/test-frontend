import React from 'react'
import { Desktop } from '../Responsive'
import Header from './Header'
import Body from './Body'
import Footer from './Footer'

import { Container } from './styles'

const SurveyInstructions = ({
  title,
  description,
  instructionSteps,
  onClick,
  buttonLabel
}) => (
  <Desktop>
    {desktop => (
      <Container desktop={desktop}>
        <Header title={title} showInstructionTitle={instructionSteps && instructionSteps.length > 0} />
        <Body
          extraInstructions={description}
          instructionSteps={instructionSteps}
        />
        <Footer onClick={onClick} buttonLabel={buttonLabel} />
      </Container>
    )}
  </Desktop>
)

export default SurveyInstructions
