import styled from 'styled-components'
import { Col } from 'antd'
import Button from '../../Button'
import Text from '../../Text'
import { family } from '../../../utils/Fonts'
import colors from '../../../utils/Colors'

export const CustomCol = styled(Col)`
  display: flex;
  justify-content: center;
  flex-direction: column;
`

export const HappyTastingText = styled(Text)`
  margin-top: 8.4rem;
  margin-bottom: 2.5rem;
  font-family: ${family.primaryLight};
  font-size: 4.2rem;
  color: ${colors.BLACK};
  text-align: center;
`

export const StartSurveyButton = styled(Button)`
  width: 100%;
`
