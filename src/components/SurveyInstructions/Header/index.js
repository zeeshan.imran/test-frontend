import React from 'react'
import { Row } from 'antd'
import { withTranslation } from 'react-i18next'

import { CustomCol, Title, InstructionsTitle } from './styles'

const Header = ({ title, showInstructionTitle, t }) => (
  <Row type='flex' justify='center'>
    <CustomCol xs={{ span: 24 }} lg={{ span: 8 }}>
      <Title>{title}</Title>
      {showInstructionTitle && (
        <InstructionsTitle>
          {t('components.surveyInstructions.title')}
        </InstructionsTitle>
      )}
    </CustomCol>
  </Row>
)

export default withTranslation()(Header)
