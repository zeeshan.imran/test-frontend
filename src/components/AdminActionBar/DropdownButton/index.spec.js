import React from 'react'
import { shallow } from 'enzyme'
import DropdownButton from '.'

describe('DropdownButton', () => {
  let testRender
  let actions

  beforeEach(() => {
    actions = ['edit', 'delete']
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render DropdownButton', async () => {
    testRender = shallow(<DropdownButton actions={actions} />)
    expect(testRender).toMatchSnapshot()
  })

  test('should render DropdownButton action null', async () => {
    testRender = shallow(<DropdownButton actions={[]} />)
    //
    expect(testRender.html()).toBeNull()
  })
})
