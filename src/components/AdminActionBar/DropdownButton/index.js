import React from 'react'
import { Dropdown, Icon, Menu } from 'antd'
import Button from '../../Button'

const dropdownMenu = actions => (
  <Menu>
    {actions.map((action, index) => (
      <Menu.Item key={index} onClick={action.onClick}>
        {action.label}
      </Menu.Item>
    ))}
  </Menu>
)

const DropdownButton = ({ actions = [] }) => {
  if (actions.length === 0) return null
  return (
    <Dropdown overlay={dropdownMenu(actions)} trigger={['click']}>
      <Button type='secondary' size='default'>
        Actions <Icon type='down' />
      </Button>
    </Dropdown>
  )
}

export default DropdownButton
