import React from 'react'
import { shallow } from 'enzyme'
import AdminActionBar from '.'

describe('AdminActionBar', () => {
  let testRender
  let buttons
  let dropdownActions

  beforeEach(() => {
    buttons = ['Back', 'Next', 'previous']
    dropdownActions = ['edit', 'delete']
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render AdminActionBar', async () => {
    testRender = shallow(
      <AdminActionBar buttons={buttons} dropdownActions={dropdownActions} />
    )
    expect(testRender).toMatchSnapshot()
  })
})
