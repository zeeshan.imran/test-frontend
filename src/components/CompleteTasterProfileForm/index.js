import React from 'react'
import { Form } from 'antd'
import Input from '../Input'
import Select from '../Select'
import { COUNTRY_PHONE_CODES } from '../../utils/Constants'

import { SubmitButton, ErrorMessage } from './styles'
import { useTranslation } from 'react-i18next';

const GENDERS = ['male', 'female', 'other']

const CompleteTasterProfileForm = ({
  fullName,
  birthYear,
  gender,
  nationality,
  errors,
  touched,
  fieldChangeHandler,
  dropdownChangeHandler,
  handleSubmit,
  setSubmitting,
  loading,
  isValid
}) => {
  const { t } = useTranslation();
  const { submission: submissionError } = errors

  return (
    <React.Fragment>
      <Form.Item
        help={touched.fullName && errors.fullName}
        validateStatus={
          touched.fullName && errors.fullName ? 'error' : 'success'
        }
      >
        <Input
          required
          name='fullName'
          value={fullName}
          onChange={fieldChangeHandler('fullName')}
          label={t('components.tasterProfile.fullName')}
        />
      </Form.Item>
      <Form.Item
        help={touched.birthYear && errors.birthYear}
        validateStatus={
          touched.birthYear && errors.birthYear ? 'error' : 'success'
        }
      >
        <Input
          required
          name='birthYear'
          value={birthYear}
          onChange={fieldChangeHandler('birthYear')}
          label={t('components.tasterProfile.birthYear')}
          type='number'
        />
      </Form.Item>
      <Form.Item
        help={touched.gender && errors.gender}
        validateStatus={touched.gender && errors.gender ? 'error' : 'success'}
      >
        <Select
          required
          value={gender}
          onChange={dropdownChangeHandler('gender')}
          options={GENDERS}
          getOptionName={gender =>
            gender.charAt(0).toUpperCase() + gender.slice(1)
          }
          label={t('components.tasterProfile.gender')}
          placeholder={t('placeholders.questionSectionSelect')}
        />
      </Form.Item>
      <Form.Item
        help={touched.nationality && errors.nationality}
        validateStatus={
          touched.nationality && errors.nationality ? 'error' : 'success'
        }
      >
        <Select
          showSearch
          required
          label={t('components.tasterProfile.nationality')}
          name='nationality'
          value={nationality}
          placeholder={t('placeholders.nationality')}
          optionFilterProp='children'
          options={COUNTRY_PHONE_CODES}
          onChange={dropdownChangeHandler('nationality')}
          getOptionName={option => option.name}
          getOptionValue={option => option.name}
          autocomplete='off'
        />
      </Form.Item>
      <SubmitButton
        loading={loading}
        type={isValid ? 'primary' : 'disabled'}
        onClick={async () => {
          await handleSubmit()
          setSubmitting(false)
        }}
      >
        {t('components.tasterProfile.submitButton')}
      </SubmitButton>
      {submissionError ? <ErrorMessage>{submissionError}</ErrorMessage> : null}
    </React.Fragment>
  )
}

export default CompleteTasterProfileForm
