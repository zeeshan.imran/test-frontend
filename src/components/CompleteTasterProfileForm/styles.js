import styled from 'styled-components'
import Button from '../Button'
import Error from '../Error'

export const SubmitButton = styled(Button)`
  width: 100%;
`

export const ErrorMessage = styled(Error)`
  margin-top: 1.5rem;
  width: 100%;
`
