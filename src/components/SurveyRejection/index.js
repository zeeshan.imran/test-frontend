import React from 'react'
import { Row } from 'antd'
import RejectionText from './RejectionText'
import { withTranslation } from 'react-i18next'
import {
  SocialButtonsContainer,
  FacebookButton,
  TwitterButton,
  LinkedInButton,
  DasboardButton
} from './styles'
import { linkedInShare } from '../../utils/shareUtils'

const SurveyRejection = ({ sharingButtons, text, shareLink, t }) => {
  const encText = encodeURIComponent(t('components.surveyComplete.text'))
  const encLink = encodeURIComponent(shareLink)
  const via = 'FlavorWiki'
  const { REACT_APP_THEME } = process.env
  return (
    <React.Fragment>
      <Row>
        <RejectionText text={text || t('defaultValues.rejectionText')} />
      </Row>
      <SocialButtonsContainer>
        {sharingButtons && (
          <React.Fragment>
            <FacebookButton
              target='_blank'
              href={`http://www.facebook.com/share.php?display=page&u=${shareLink}`}
            >
              {t('components.socialMedia.shareFB')}
            </FacebookButton>
            <TwitterButton
              target='_blank'
              href={`https://twitter.com/intent/tweet?text=${encText}&via=${via}&url=${encLink}`}
            >
              {t('components.socialMedia.shareTW')}
            </TwitterButton>
            <LinkedInButton href='#' onClick={e => linkedInShare(e, encLink)}>
              {t('components.socialMedia.shareLI')}
            </LinkedInButton>
          </React.Fragment>
        )}
        {REACT_APP_THEME === 'default' && (
          <DasboardButton href={`/`}>
            {t('components.surveyComplete.backToDashboard')}
          </DasboardButton>
        )}
      </SocialButtonsContainer>
    </React.Fragment>
  )
}

export default withTranslation()(SurveyRejection)
