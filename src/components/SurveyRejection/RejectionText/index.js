import React from 'react'
import { CustomCol, Prompt } from './styles'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import SURVEY_PARTICIPATION_QUERY from '../../../queries/SurveyParticipationQuery'
import { surveyBasicInfo } from '../../../fragments/survey'
import { getShareLink } from '../../../utils/shareUtils'

const RejectionText = ({ text }) => {
  const {
    data: { currentSurveyParticipation: { surveyId, surveyEnrollmentId } = {} }
  } = useQuery(SURVEY_PARTICIPATION_QUERY)

  const {
    data: { survey = {} }
  } = useQuery(SURVEY_QUERY, { variables: { id: surveyId } })

  const formatedText = text
    .replace(
      '{{sharing_link}}',
      getShareLink(survey.uniqueName, surveyEnrollmentId)
    )
    .replace('{{amount}}', survey.referralAmount)

  return (
    <CustomCol lg={{ span: 8, offset: 8 }}>
      <Prompt dangerouslySetInnerHTML={{ __html: formatedText }} />
    </CustomCol>
  )
}

const SURVEY_QUERY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      ...surveyBasicInfo
      screeners {
        id
        uniqueName
        isPaypalSelected
        isGiftCardSelected
        customizeSharingMessage
        referralAmount
      }
    }
  }

  ${surveyBasicInfo}
`

export default RejectionText
