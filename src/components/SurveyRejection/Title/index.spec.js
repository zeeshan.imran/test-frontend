import React from 'react'
import { shallow } from 'enzyme'
import Title from '.';

jest.mock('react-i18next', () => ({
    withTranslation: () => Component => {
        Component.defaultProps = { ...Component.defaultProps, t: () => "" };
        return Component;
    },
}));

describe('Title', () => {

    let testRender;

    beforeEach(() => {

    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render Title', async () => {
        testRender = shallow(
            <Title
                t={key => key}
            />
        )
        expect(testRender).toMatchSnapshot()
    })
});