import styled from 'styled-components'
import colors from '../../utils/Colors'

const SocialButton = styled.a`
  padding: 1.5rem;
  margin-bottom: 1rem;
  max-width: 30rem;
  width: 80%;
  border-radius: 0.4rem;
  color: white !important;
  line-height: 1;
  text-decoration: none;
  cursor: pointer;
  text-align: center;
`

export const FacebookButton = styled(SocialButton)`
  background-color: #3b5a97 !important;
`

export const TwitterButton = styled(SocialButton)`
  background-color: #38a1f3 !important;
`

export const LinkedInButton = styled(SocialButton)`
  background-color: #0277b5 !important;
`

export const DasboardButton = styled(SocialButton)`
  background-color: ${colors.DASHBOARD_BUTTON_COLOR} !important;
`

export const SocialButtonsContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
`
