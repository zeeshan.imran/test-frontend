import styled from 'styled-components'
import colors from '../../utils/Colors'

export const ClickableContainer = styled.div`
  cursor: pointer;
  padding: 0.5rem 1rem;
  border-radius: 0.5rem;

  &:focus {
    background-color: ${colors.ICON_BUTTON_COLOR_FOCUS};
    color: ${colors.WHITE};
  }

  &:active {
    background-color: ${({noActive}) => !noActive && colors.ICON_BUTTON_COLOR_ACTIVE};
    color: ${({noActive}) => !noActive && colors.WHITE};
  }
`

export const Ghost = styled.div``
