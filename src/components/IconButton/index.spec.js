import React from 'react'
import { mount } from 'enzyme'
import IconButton from '.'
import { ClickableContainer } from './styles'

describe('IconButton', () => {
  let testRender
  let type
  let size
  let tooltip
  let clickedTooltip
  let onClick

  beforeEach(() => {
    type = 'primary'
    size = 'large'
    tooltip = 'submit here'
    clickedTooltip = false
    onClick = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render IconButton', async () => {
    testRender = mount(
      <IconButton
        type={type}
        size={size}
        tooltip={tooltip}
        clickedTooltip={clickedTooltip}
        onClick={onClick}
      />
    )
    expect(testRender.find(IconButton)).toHaveLength(1)
  })

  test('should render IconButton on click', async () => {
    testRender = mount(
      <IconButton
        type={type}
        size={size}
        tooltip={tooltip}
        clickedTooltip={clickedTooltip}
        onClick={onClick}
      />
    )
    testRender
      .find(ClickableContainer)
      .props()
      .onClick()

    expect(onClick).toHaveBeenCalled()
  })
})
