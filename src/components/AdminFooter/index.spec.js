import React from 'react'
import { shallow } from 'enzyme'
import AdminFooter from '.'

describe('AdminFooter', () => {
  let testRender
  let onOk
  let onCancel
  let okText
  let cancelText
  let disabled
  let cancelDisabled

  beforeEach(() => {
    onOk = jest.fn()
    onCancel = jest.fn()
    okText = 'ok'
    cancelText = 'Cancel'
    disabled = 'primary'
    cancelDisabled = 'not disabled'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render AdminFooter', () => {
    testRender = shallow(
      <AdminFooter
        onOk={onOk}
        onCancel={onCancel}
        okText={okText}
        cancelText={cancelText}
        okDisabled={disabled}
        cancelDisabled={cancelDisabled}
      />
    )

    expect(testRender).toMatchSnapshot()
  })
})
