import React from 'react'

import { StyledFooter, OkButton, CancelButton } from './styles'

const AdminFooter = ({
  onOk,
  onCancel,
  okText,
  cancelText,
  okDisabled,
  cancelDisabled
}) => (
  <StyledFooter>
    {cancelText && (
      <CancelButton
        size='default'
        type='secondary'
        disabled={cancelDisabled}
        onClick={onCancel}
      >
        {cancelText}
      </CancelButton>
    )}
    {okText && (
      <OkButton
        size='default'
        type={okDisabled ? 'disabled' : 'primary'}
        onClick={onOk}
      >
        {okText}
      </OkButton>
    )}
  </StyledFooter>
)

export default AdminFooter
