import React, { useMemo, useState, useEffect } from 'react'
import { Formik } from 'formik'
import { Popover } from 'antd'
import { useTranslation } from 'react-i18next'
import { uniqBy, prop, reject, isNil } from 'ramda'
import './styles.css'
import InputArea from '../../InputArea'
import RadioButton from '../../RadioButton'
import Checkbox from '../../Checkbox'
import Carousel from '../../../components/Carousel'
import PaypalUsers from './PaypalUsers'
import PhotoValidationLogs from './PhotoValidationLogs'
import { isUserAuthenticatedAsSuperAdmin } from '../../../utils/userAuthentication'
import ProductList from './ProductTable'

import { ValidText, InvalidText } from './styles'
import {
  FieldText,
  FieldTextImage,
  FieldTextImageContainer,
  TextContainer,
  CarouselImage,
  FlexBetweenContainer,
  InfoBox,
  Link,
  AntCollapse,
  AntPanel
} from '../styles'

const characterLimit = 200

const PhotoValidationForm = ({
  loading,
  survey,
  paypalEmailAddress,
  paypalUsers = [],
  photoValidationLogs,
  userValidations = {},
  enrollment = {},
  setEnrollment,
  canModify,
  onAddToBlackList
}) => {
  const { t } = useTranslation()
  const isSuperAdmin = isUserAuthenticatedAsSuperAdmin()
  const [isEscalated, setIsEscalated] = useState(false)
  const [isHiddenFromCharts, setIsHiddenFromCharts] = useState(false)
  const [invalidComment, setInvalidComment] = useState('')
  const [charactersRemaining, setCharactersRemaining] = useState(characterLimit)
  const [step, setStep] = useState(0)

  const { answers, savedRewards, validation, user = {} } = enrollment
  const {
    validCount = 0,
    validAnswers = [],
    invalidCount = 0,
    invalidAnswers = []
  } = userValidations

  const productsList =
    answers && answers.length && answers.map(ans => ans && ans.product)

  const uniqProductList =
    productsList &&
    productsList.length &&
    reject(isNil, uniqBy(prop('id'), productsList))

  useEffect(() => {
    if (enrollment) {
      const { escalation, hiddenFromCharts, comment } = enrollment

      setIsEscalated(escalation)
      setIsHiddenFromCharts(hiddenFromCharts)
      setInvalidComment(comment)
      setCharactersRemaining(
        comment ? characterLimit - comment.length : characterLimit
      )
    }
  }, [enrollment])

  useEffect(() => {
    return () => {
      setStep(0)
    }
  }, [answers])

  const validContent = useMemo(() => {
    return (
      <React.Fragment>
        {validAnswers.map(answer => {
          return <img src={answer.value[0]} alt='' />
        })}
      </React.Fragment>
    )
  }, [validAnswers])

  const invalidContent = useMemo(() => {
    return (
      <React.Fragment>
        {invalidAnswers.map(answer => {
          return <img src={answer.value[0]} alt='' />
        })}
      </React.Fragment>
    )
  }, [invalidAnswers])

  const paypalUsersContent = useMemo(() => {
    return (
      <PaypalUsers
        dataSource={paypalUsers}
        onAddToBlackList={onAddToBlackList}
      />
    )
  }, [paypalUsers])

  const { images, products, timeToAnswer } = useMemo(() => {
    const data = {
      images: [],
      products: [],
      timeToAnswer: []
    }

    if (answers && answers.length > 0) {
      answers.forEach(answer => {
        if (answer.product) {
          data.products.push(answer.product.name)
        }

        if (answer.timeToAnswer) {
          data.timeToAnswer.push(answer.timeToAnswer)
        }

        if (answer.value && answer.value[0]) {
          data.images.push(answer.value[0])
        }
      })
    }

    return data
  }, [answers])

  const canShowLogs = useMemo(() => {
    if (
      isSuperAdmin &&
      enrollment &&
      enrollment.id &&
      photoValidationLogs &&
      photoValidationLogs.length > 0
    ) {
      return true
    }
    return false
  }, [isSuperAdmin, enrollment, photoValidationLogs])

  const activeData = useMemo(() => {
    if (images[step] && products[step] && timeToAnswer[step]) {
      return {
        imageUrl: images[step],
        productName: products[step],
        secondsToAnswer: `${t(
          'components.photoValidation.responseTime'
        )} ${new Date(timeToAnswer[step]).getSeconds()}s`
      }
    }

    return {}
  }, [answers, step, images, products, timeToAnswer])

  return (
    <Formik
      render={() => {
        return (
          <React.Fragment>
            <TextContainer>
              <FieldText>Enrollment Id: {enrollment.id}</FieldText>
              <FieldText>User: {(user && user.fullName) || '-'}</FieldText>
              <FieldText>Email: {(user && user.emailAddress) || '-'}</FieldText>
              <FieldText>Paypal: {paypalEmailAddress || '-'}</FieldText>

              {paypalUsers.length > 1 && (
                <Popover content={paypalUsersContent} placement='bottomLeft'>
                  <InvalidText>
                    User with same PayPal: {paypalUsers.length}
                  </InvalidText>
                </Popover>
              )}
            </TextContainer>

            {images.length > 0 ? (
              <React.Fragment>
                <Carousel
                  afterSlide={currentIndex => {
                    setStep(currentIndex)
                  }}
                  slides={images}
                  renderSlideBottomContent={image => {
                    return <CarouselImage key={image} src={image} />
                  }}
                />
                <TextContainer>
                  <FieldTextImageContainer>
                    {activeData.productName && (
                      <FieldTextImage>{activeData.productName}</FieldTextImage>
                    )}
                    {activeData.secondsToAnswer && (
                      <FieldTextImage>
                        {activeData.secondsToAnswer}
                      </FieldTextImage>
                    )}
                    {activeData.imageUrl && (
                      <FieldTextImage>
                        <Link href={activeData.imageUrl} target='_blank'>
                          {t('components.photoValidation.newTab')}
                        </Link>
                      </FieldTextImage>
                    )}
                  </FieldTextImageContainer>
                </TextContainer>
              </React.Fragment>
            ) : (
              <TextContainer>
                <FieldText>Images:{'-'}</FieldText>
              </TextContainer>
            )}

            {uniqProductList && uniqProductList.length > 0 && (
              <ProductList
                enrollement={enrollment}
                setEnrollment={setEnrollment}
                savedRewards={savedRewards}
                products={uniqProductList}
                t={t}
                survey={survey}
              />
            )}

            {canModify && (
              <React.Fragment>
                <FlexBetweenContainer>
                  <Checkbox
                    bigger
                    checked={isHiddenFromCharts}
                    onChange={value => {
                      setIsHiddenFromCharts(value)
                      setEnrollment({
                        ...enrollment,
                        hiddenFromCharts: value
                      })
                    }}
                  >
                    Hide enrollment from charts
                  </Checkbox>
                </FlexBetweenContainer>
                <FlexBetweenContainer>
                  <Checkbox
                    bigger
                    checked={isEscalated}
                    onChange={value => {
                      setIsEscalated(value)
                      setEnrollment({
                        ...enrollment,
                        escalation: value
                      })
                    }}
                  >
                    {t('components.photoValidation.addEscalation')}
                  </Checkbox>
                  {user && user.id && (
                    <Checkbox
                      bigger
                      checked={user.blackListed}
                      onChange={value => {
                        setEnrollment({
                          ...enrollment,
                          processed: true,
                          validation: 'invalid',
                          user: {
                            ...enrollment.user,
                            blackListed: value
                          }
                        })
                      }}
                    >
                      {'Blacklist User'}
                    </Checkbox>
                  )}
                </FlexBetweenContainer>
                <FlexBetweenContainer>
                  <RadioButton
                    checked={validation === 'valid'}
                    onClick={() => {
                      setEnrollment({
                        ...enrollment,
                        validation: 'valid',
                        processed: true,
                        user: {
                          ...enrollment.user,
                          blackListed: false
                        }
                      })
                    }}
                  >
                    {t('components.photoValidation.valid')}
                  </RadioButton>
                  <RadioButton
                    checked={validation === 'invalid'}
                    onClick={() => {
                      setEnrollment({
                        ...enrollment,
                        validation: 'invalid',
                        processed: true
                      })
                    }}
                  >
                    {t('components.photoValidation.invalid')}
                  </RadioButton>
                </FlexBetweenContainer>
              </React.Fragment>
            )}

            <FlexBetweenContainer>
              {validCount === 0 && (
                <ValidText>Valid Count: {validCount}</ValidText>
              )}

              {validCount > 0 && (
                <Popover
                  overlayClassName='validation-popup-overlay'
                  title='Images'
                  content={validContent}
                  trigger='click'
                  placement='top'
                >
                  <ValidText>Valid Count: {validCount}</ValidText>
                </Popover>
              )}

              {invalidCount === 0 && (
                <InvalidText>Invalid Count: {invalidCount}</InvalidText>
              )}

              {invalidCount > 0 && (
                <Popover
                  overlayClassName='validation-popup-overlay'
                  title='Images'
                  content={invalidContent}
                  trigger='click'
                  placement='top'
                >
                  <InvalidText>Invalid Count: {invalidCount}</InvalidText>
                </Popover>
              )}
            </FlexBetweenContainer>

            <InputArea
              name='comment'
              value={invalidComment}
              onChange={event => {
                setInvalidComment(event.target.value)
                enrollment['comment'] = event.target.value
                setEnrollment(enrollment)
                setCharactersRemaining(
                  characterLimit - event.target.value.length
                )
              }}
              label={t('components.photoValidation.comment')}
              size='small'
              maxLength={`200`}
            />

            <InfoBox>
              {t('components.photoValidation.commentRemaining')}{' '}
              {charactersRemaining}
            </InfoBox>

            {canShowLogs && (
              <AntCollapse expandIconPosition='right'>
                <AntPanel key='1' header={t('components.photoValidation.logs')}>
                  <PhotoValidationLogs
                    loading={loading}
                    dataSource={photoValidationLogs}
                  />
                </AntPanel>
              </AntCollapse>
            )}
          </React.Fragment>
        )
      }}
    />
  )
}

export default PhotoValidationForm
