import React from 'react'
import { Table, Button } from 'antd'
import { useTranslation } from 'react-i18next'
import { ErrorText, InformationCell } from './styles'

const baseLokalise = 'components.photoValidation.paypalUsersTable'

const PaypalUsers = ({ dataSource, onAddToBlackList }) => {
  const { t } = useTranslation()

  const columns = [
    {
      title: t(`${baseLokalise}.paypalEmailAddress`),
      dataIndex: 'paypalEmailAddress',
      render: (_, rowData) => {
        return (
          <React.Fragment>
            <div>{rowData.paypalEmailAddress}</div>
            {rowData.blackListed ? (
              <ErrorText>{t(`${baseLokalise}.inBlackList`)}</ErrorText>
            ) : (
              <Button
                type='danger'
                size='small'
                onClick={() => onAddToBlackList(rowData.id)}
              >
                {t(`${baseLokalise}.addToBlackList`)}
              </Button>
            )}
          </React.Fragment>
        )
      }
    },
    {
      title: t(`${baseLokalise}.user`),
      dataIndex: 'user',
      render: (_, rowData) => {
        return (
          <InformationCell>
            <h6>{rowData.fullname}</h6>
            <small>{rowData.emailAddress}</small>
          </InformationCell>
        )
      }
    },
    {
      title: t(`${baseLokalise}.country`),
      dataIndex: 'country',
      render: (_, rowData) => {
        return (
          <InformationCell>
            <h6>{rowData.country}</h6>
            <small>{rowData.state}</small>
          </InformationCell>
        )
      }
    },
    {
      title: t(`${baseLokalise}.enrollments`),
      dataIndex: 'enrollments',
      render: (_, rowData) => {
        return rowData.surveyEnrollments ? rowData.surveyEnrollments.length : 0
      }
    }
  ]

  return (
    <React.Fragment>
      <Table
        key='paypalUser'
        size='small'
        rowKey={record => record.id}
        columns={columns}
        dataSource={dataSource}
      />
    </React.Fragment>
  )
}

export default PaypalUsers
