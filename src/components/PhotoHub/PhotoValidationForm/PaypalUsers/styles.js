import styled from 'styled-components'
import colors from '../../../../utils/Colors'

export const ErrorText = styled.div`
  color: ${colors.RED};
  font-size: 12px;
`

export const InformationCell = styled.div`
  
  h6 {
    font-size: 16px;
    margin-bottom: 0;
  }

  small {
    display: block;
    font-size: 12px;
  }
`
