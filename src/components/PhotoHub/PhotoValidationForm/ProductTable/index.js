import React from 'react'
import { reject, isNil, flatten } from 'ramda'
import { Table as AntdTable, Switch } from 'antd'

const productValidation = (products, savedRewards) =>
  reject(
    isNil,
    flatten(
      products.map(product => {
        if (savedRewards && savedRewards.length) {
          return savedRewards.map(reward => {
            if (reward.id === product.id && reward.answered) {
              return {
                ...product,
                reward: reward.reward || 'N/A',
                showOnCharts: reward.showOnCharts,
                payable: reward.payable === null ? true : reward.payable
              }
            } else {
              return null
            }
          })
        } else {
          return null
        }
      })
    )
  )

const ProductList = ({
  enrollement,
  products,
  savedRewards,
  setEnrollment,
  t,
  survey
}) => {
  const updatedProducts = productValidation(
    survey && survey.products ? survey.products : products,
    savedRewards
  )
  const validateProduct = (productId, field, value) => {
    const updatedSavedRewards = savedRewards.map(reward => {
      if (reward.id === productId) {
        return {
          ...reward,
          [field]: value
        }
      } else {
        return reward
      }
    })

    setEnrollment({
      ...enrollement,
      savedRewards: updatedSavedRewards
    })
  }

  const columns = [
    {
      title: t('components.photoValidation.product'),
      dataIndex: 'name',
      key: 'name',
      align: 'left',
      render: (data, rowData, rowIndex) => {
        return (
          <div>
            <div>{data}</div>
            <small>
              {t('components.photoValidation.productReward')}: {rowData.reward}
            </small>
          </div>
        )
      }
    },
    {
      title: t('components.photoValidation.productValidate'),
      dataIndex: '',
      align: 'center',
      render: (_, rowData) => (
        <Switch
          checked={rowData.showOnCharts}
          size='small'
          onChange={vaule => validateProduct(rowData.id, 'showOnCharts', vaule)}
        />
      )
    },
    {
      title: t('components.photoValidation.productPayable'),
      dataIndex: '',
      align: 'center',
      render: (_, rowData) => (
        <Switch
          checked={rowData.payable}
          size='small'
          onChange={vaule => validateProduct(rowData.id, 'payable', vaule)}
        />
      )
    }
  ]

  return (
    <AntdTable
      bordered
      key='userSurveyListingTable'
      rowKey={record => record.id}
      columns={columns}
      dataSource={updatedProducts}
      pagination={false}
    />
  )
}

export default ProductList
