import React from 'react'
import moment from 'moment'
import { useTranslation } from 'react-i18next'
import {
  FieldText,
  FieldTextSimple,
  FieldTextSimpleHeader,
  AntTable
} from '../../styles'

const PhotoValidationLogs = ({ loading, dataSource }) => {
  const { t } = useTranslation()
  let columns = [
    {
      title: t('components.photoValidation.logTable.action'),
      dataIndex: 'action'
    },
    {
      title: t('components.photoValidation.logTable.values'),
      dataIndex: 'values',
      render: (_, rowData) => {
        if (rowData.values) {
          const mainValues = Object.keys(rowData.values)
          if (mainValues) {
            return mainValues.map((current, index) => {
              const fieldValue = rowData.values[current].toString()
              return (
                <FieldTextSimple key={`${index}-key`}>
                  <FieldTextSimpleHeader>{current}</FieldTextSimpleHeader>:{' '}
                  {fieldValue}
                </FieldTextSimple>
              )
            })
          }
        }
      }
    },
    {
      title: t('components.photoValidation.logTable.by'),
      dataIndex: 'by',
      render: (_, rowData) => {
        let updatedBy = ''
        if (rowData.by) {
          updatedBy = (
            <React.Fragment>
              <FieldTextSimple>
                {rowData.by.fullName
                  ? rowData.by.fullName
                  : rowData.by.emailAddress}
              </FieldTextSimple>
              {rowData.impersonatedBy ? (
                <FieldText>
                  (
                  {rowData.impersonatedBy.fullName
                    ? rowData.impersonatedBy.fullName
                    : rowData.impersonatedBy.emailAddress}
                  )
                </FieldText>
              ) : null}
            </React.Fragment>
          )
        }
        return updatedBy
      }
    },
    {
      title: t('components.photoValidation.logTable.date'),
      dataIndex: 'createdAt',
      render: (_, rowData) => {
        return moment(rowData.createdAt).format('YYYY/MM/DD-HH:mm')
      }
    }
  ]
  return (
    <React.Fragment>
      <AntTable
        key={`photoValidationLogs`}
        loading={loading}
        size={`small`}
        rowKey={record => record.id}
        columns={columns}
        dataSource={dataSource}
        pagination={true}
      />
    </React.Fragment>
  )
}

export default PhotoValidationLogs
