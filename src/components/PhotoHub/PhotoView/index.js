import React, { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import {
  PhotoSection,
  TextContainer,
  IconContainer,
  FieldText,
  FieldTextHeader
} from '../styles'
import IconButton from '../../IconButton'

const PhotoView = ({ enrollment, setIsModalVisible, setSelectedAnswer }) => {
  const { t } = useTranslation()
  let { id, paypalEmail, validation, user, answers, escalation, hiddenFromCharts } = enrollment

  const { blackListed } = user || false
  let iconType = 'exclamation-circle'
  let toolTip = t('tooltips.notProcessedSurveyPhotos')

  if (validation === 'valid') {
    iconType = 'check-circle'
    toolTip = t('tooltips.validSurveyPhotos')
  }

  if (validation === 'invalid') {
    iconType = 'close-circle'
    toolTip = t('tooltips.invalidSurveyPhotos')
  }

  const paypalEmailAddress = useMemo(() => {
    if (paypalEmail) {
      return paypalEmail
    }

    if (user && user.paypalEmailAddress) {
      return user.paypalEmailAddress
    }

    return '-'
  }, [paypalEmail, user])

  return (
    <PhotoSection
      onClick={() => {
        setSelectedAnswer(enrollment)
        setIsModalVisible(true)
      }}
    >
      <IconContainer state={validation}>
        {hiddenFromCharts && (
          <IconButton
            tooltip={'Enrollment is hidden from charts'}
            type={`eye-invisible`}
            size='2rem'
          />
        )}
        {escalation && (
          <IconButton
            tooltip={t('tooltips.photoHasEscalation')}
            type={`up-circle`}
            size='2rem'
          />
        )}
        {answers && answers.length > 0 && (
          <IconButton
            tooltip={t('tooltips.photoHasImages')}
            type={`sketch`}
            size='2rem'
          />
        )}
        {blackListed && (
          <IconButton
            tooltip={'User is blacklisted'}
            type={`user-delete`}
            size='2rem'
          />
        )}
        <IconButton tooltip={toolTip} type={iconType} size='2rem' />
      </IconContainer>
      <TextContainer>
        <FieldTextHeader>{id}</FieldTextHeader>
        <FieldText>{user && user.emailAddress}</FieldText>
        <FieldText>{paypalEmailAddress}</FieldText>
      </TextContainer>
    </PhotoSection>
  )
}

export default PhotoView
