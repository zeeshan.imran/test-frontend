import React, { useEffect, useState } from 'react'
import gql from 'graphql-tag'
import { useTranslation } from 'react-i18next'
import { useMutation } from 'react-apollo-hooks'
import { omit } from 'ramda'
import { Pagination } from 'antd'
import PhotoView from './PhotoView'
import NoPhotosAvailable from './NoPhotosAvailable'
import ModalPhotoValidationForm from '../../containers/ModalPhotoValidationForm'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import LoadingModal from '../LoadingModal'
import usePrevious from '../../hooks/usePrevious'
import {
  isUserAuthenticatedAsPowerUser,
  isUserAuthenticatedAsOperator
} from '../../utils/userAuthentication'
import { Container, Gallery, PaginationContainer } from './styles'

const PhotoHub = ({
  refetch,
  refetchCount,
  page,
  survey,
  loading,
  surveyEnrollmentValidations,
  countSurveyEnrollmentValidations,
  updateUrlSearchParams
}) => {
  const { t } = useTranslation()
  const [isUpdating, setIsUpdating] = useState(false)
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [surveyEnrollments, setSurveyEnrollments] = useState([])
  const [selectedAnswer, setSelectedAnswer] = useState({})
  const canModify =
    isUserAuthenticatedAsPowerUser() || isUserAuthenticatedAsOperator()

  const previousAnswer = usePrevious(selectedAnswer)
  const updateAnswer = useMutation(UPDATE_VALIDATE_ANSWER)

  useEffect(() => {
    if (surveyEnrollmentValidations) {
      setSurveyEnrollments([...surveyEnrollmentValidations])
    }
  }, [surveyEnrollmentValidations])

  const setAnswerProcessed = async updatedAnswer => {
    let {
      id,
      processed,
      hiddenFromCharts,
      validation,
      comment,
      escalation,
      user,
      savedRewards
    } = updatedAnswer
    let newUser = null
    if (user) {
      const { __typename, ...rest } = user
      newUser = rest
    }
    let updatedSavedRewards = {}
    if (savedRewards && savedRewards.length) {
      updatedSavedRewards = {
        savedRewards: savedRewards.map(reward => omit(['__typename'], reward))
      }
    }
    try {
      setIsUpdating(true)

      await updateAnswer({
        variables: {
          input: {
            id,
            surveyId: survey.id,
            processed,
            hiddenFromCharts: hiddenFromCharts ? hiddenFromCharts : false,
            validation,
            comment,
            escalation,
            user: newUser,
            ...updatedSavedRewards
          }
        }
      })

      displaySuccessMessage(
        t(`components.surveyPhotos.successPopup`, {
          state: validation
        })
      )

      refetch()
      refetchCount()
    } catch (error) {
      return displayErrorPopup(`Error while updating validation .`)
    } finally {
      setIsUpdating(false)
      setIsModalVisible(false)
      setSelectedAnswer({})
    }
  }

  return (
    <Container>
      {loading && <LoadingModal visible />}

      {surveyEnrollments.length > 0 ? (
        <React.Fragment>
          <Gallery>
            {surveyEnrollments.map((surveyEnrollment, index) => {
              return (
                <PhotoView
                  key={index}
                  enrollment={surveyEnrollment}
                  setIsModalVisible={setIsModalVisible}
                  setSelectedAnswer={setSelectedAnswer}
                />
              )
            })}
          </Gallery>

          <PaginationContainer>
            <Pagination
              defaultCurrent={page}
              defaultPageSize={20}
              total={countSurveyEnrollmentValidations}
              hideOnSinglePage
              onChange={value => updateUrlSearchParams('page', value)}
            />
          </PaginationContainer>

          <ModalPhotoValidationForm
            refetchEnrollments={refetch}
            isUpdating={isUpdating}
            canModify={canModify}
            selectedAnswer={selectedAnswer}
            setSelectedAnswer={setSelectedAnswer}
            isVisible={isModalVisible}
            onCancel={() => {
              setSelectedAnswer(previousAnswer)
              setIsModalVisible(false)
            }}
            onOk={() => {
              setAnswerProcessed(selectedAnswer)
            }}
            survey={survey}
          />
        </React.Fragment>
      ) : (
        <NoPhotosAvailable loading={loading} />
      )}
    </Container>
  )
}

export const UPDATE_VALIDATE_ANSWER = gql`
  mutation updateValidateSurveyEnrollement(
    $input: EditValidateSurveyEnrollement
  ) {
    updateValidateSurveyEnrollement(input: $input) {
      id
    }
  }
`

export default PhotoHub
