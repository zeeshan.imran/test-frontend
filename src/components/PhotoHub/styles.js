import styled from 'styled-components'
import colors from '../../utils/Colors'
import Text from '../Text'
import { Collapse, Table } from 'antd'

export const Container = styled.div`
  background-color: ${colors.WHITE};
  padding: 3rem 3.5rem;
  margin-bottom: 2.5rem;
`
export const TextContainer = styled.div`
  margin-bottom: 2rem;
`

export const Section = styled.div`
  padding: 3rem 3.5rem;
  width: 100%;
`

export const PaginationContainer = styled.div`
  text-align: right;
  margin-top: 5rem;
`
export const InfoBox = styled.div`
  font-size: 1rem;
`

export const ClearFix = styled.div`
  clear: both;
`

export const Gallery = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: stretch;
`
export const PhotoSection = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  margin: 10px 2%;
  border-bottom: 2px solid #8cbb5c;
  justify-content: center;
  cursor: pointer;
  width: 21%;
  padding: 5rem 0 2rem 0;
  :hover {
    border-bottom: 2px solid #3a581b;
  }
`
export const Image = styled.img`
  width: 150px;
  height: 150px;
`
export const FullImage = styled.img`
  width: 100%;
  max-width: 600px;
  max-height: 800px;
`
export const IconContainer = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  background: #fff;
  display: inline-flex;
  svg {
    fill: ${({ state }) =>
      state
        ? state === 'valid'
          ? colors.SOFT_GREEN
          : colors.RED
        : colors.BLACK};
  }
`

export const FlexBetweenContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 3rem;
  margin-bottom: 3rem;
`

export const FieldText = styled(Text)`
  display: block;
  color: #aaa;
`
export const FieldTextSimple = styled(Text)`
  display: block;
`
export const FieldTextSimpleHeader = styled.span`
  color: #aaa;
`

export const FieldTextImageContainer = styled.div`
  display: block;
  color: rgba(0, 0, 0, 0.65);
  bottom: 0;
  text-align: center;
  width: 100%;
  font-weight: bolder;
  font-size: 1.5rem;
  background: #fff;
`
export const FieldTextImage = styled.div`
  color: rgba(0, 0, 0, 0.65);
`

export const FieldTextHeader = styled(Text)`
  display: block;
  color: rgba(0, 0, 0, 0.65);
  font-weight: bolder;
`

export const CarouselImage = styled.div`
  background: url(${props => props.src}) no-repeat center;
  background-size: contain;
  height: 50vh;
  width: 100%;
  image-orientation: from-image;
`
export const Link = styled.a`
  display: inline;
  color: ${colors.MID_BLUE};
  text-decoration: underline;
`
export const AntTable = styled(Table)`
  .ant-table-small {
    font-size: 1.2rem;
  }
`
export const ValidText = styled(Text)`
  display: block;
  color: ${colors.TOOL_TIP_COLOR};
`
export const InvalidText = styled(Text)`
  display: block;
  color: ${colors.RED};
`

export const AntCollapse = styled(Collapse)`
  border: unset;
  margin-top: 16px;

  > .ant-collapse-item {
    border-bottom: unset;

    .ant-collapse-content {
      border-top: unset;
    }
  }
`

export const AntPanel = styled(Collapse.Panel)`
  .ant-collapse-content-box {
    border-top: unset;
    padding: 12px 0 0;
  }
`
