import React from 'react'
import { Text, Divider, StatisticHeader } from '../styles'
import Fisher from '../Fisher'
import Anova from '../Anova'
import PcaChart from '../PcaChart'
import PenaltyChart from '../PenaltyChart'
import Tukey from '../Tukey'
import DataTable from '../DataTable'
import StatsOptions from '../../../containers/StatsOptions'
import formatAnalysisTableData from '../../../utils/formatAnalysisTableData'

const StatisticSwitch = ({
  data,
  chartSettings,
  statisticsSettings,
  isOwner,
  saveChartsSettings,
  t
}) => {
  const fullCardWidth = '880px'

  return (
    <React.Fragment>
      {data.mean_stddev && !data.mean_stddev.loading && (
        <DataTable
          {...formatAnalysisTableData(data.mean_stddev, chartSettings)}
          chartSettings={chartSettings}
        />
      )}
      {data.pca && !data.pca.loading && (
        <React.Fragment>
          <Divider />
          <StatisticHeader>
            <Text>{t(`charts.analysis.chartTitle.pca3d`)}</Text>
            <StatsOptions
              chartData={{
                title: t(`charts.analysis.chartTitle.pca3d`),
                chart_type: 'pca',
                question: data.questionId
              }}
              chartSettings={statisticsSettings[`${data.questionId}-pca`]}
              saveChartsSettings={saveChartsSettings}
              isOwner={isOwner}
              isStatisticsSettings
            />
          </StatisticHeader>
          <PcaChart
            inputData={{ ...data.pca, chart_id: data.chart_id }}
            cardWidth={fullCardWidth}
            chartSettings={chartSettings}
            statisticsSettings={statisticsSettings[`${data.questionId}-pca`]}
          />
        </React.Fragment>
      )}
      {data.penalty && !data.penalty.loading && (
        <React.Fragment>
          <Divider />
          <StatisticHeader>
            <Text>{t(`charts.analysis.chartTitle.penalty`)}</Text>
            <StatsOptions
              chartData={{
                title: t(`charts.analysis.chartTitle.penalty`),
                chart_type: 'penalty',
                question: data.questionId
              }}
              chartSettings={statisticsSettings[`${data.questionId}-penalty`]}
              saveChartsSettings={saveChartsSettings}
              isOwner={isOwner}
              isStatisticsSettings
            />
          </StatisticHeader>
          <PenaltyChart
            inputData={{ ...data.penalty, chart_id: data.chart_id }}
            cardWidth={fullCardWidth}
            statisticsSettings={statisticsSettings[`${data.questionId}-penalty`]}
          />
        </React.Fragment>
      )}
      {data.fisher && !data.fisher.loading && (
        <Fisher data={data.fisher} chartSettings={chartSettings} />
      )}
      {data.anova && !data.anova.loading && (
        <Anova data={data.anova} chartSettings={chartSettings} />
      )}
      {data.tukey && !data.tukey.loading && (
        <Tukey data={data.tukey} chartSettings={chartSettings} />
      )}
    </React.Fragment>
  )
}

export default StatisticSwitch
