import React from 'react'

const withPptDump = (
  type,
  pickPropsFn = ({ inputData }) => inputData,
  pickSettingsFn = ({ colorsArr }) => ({ colorsArr }),
  pickAllSettingFn = ({ chartSettings }) => ({ chartSettings })
) => Component => {
  const isPDF = /^\/preview-operator-ppt\//.test(window.location.pathname)
  if (isPDF) {
    return props => (
      <div
        data-ppt-type={type}
        data-ppt-raw={JSON.stringify(
          typeof pickSettingsFn === 'function'
            ? {
                ...pickPropsFn(props),
                ...pickSettingsFn(props.chartSettings || {}),
                ...pickAllSettingFn(props || {})
              }
            : pickPropsFn(props)
        )}
      >
        <Component {...props} />
      </div>
    )
  }

  return Component
}

export default withPptDump
