import styled from 'styled-components'
import { DefaultChart } from '../styles'
import { family } from '../../../utils/Fonts'

export const Container = styled(DefaultChart)`
  .penaltychart-container {
    position: relative;
  }
  .shapeLegendContainer {
    background: #fff;
    border: #e4e4e5 1px solid;
    padding: 8px 0;
    .legend {
      display: block;
      cursor: default;
      padding: 0 4px 0 0;
      svg {
        position: relative;
        margin: 0px 4px;
        top: 1px;
      }
    }
  }
  .chart-legend {
    margin-bottom: 24px;
  }
  .axisY text {
    font-weight: 700;
    font-size: 10px;
    font-family: ${family.primaryBold};
    color: #7f7f7f;
    fill: #7f7f7f;
    line-height: 1.2;
  }
  .axisX .tick:last-child {
    display: block;
  }
  .sliderdesc {
    text-align: right;
    font-weight: 700;
    font-size: 12px;
    font-family: ${family.primaryBold};
    color: #7f7f7f;
    line-height: 38px;
    padding-right: 4px;
  }
  .ant-input-number,
  .ant-input-disabled {
    margin-top: 4px;
    padding-top: 2px;
    margin-left: 12px;
    width: 90px;
  }
  .ant-input-disabled {
    cursor: default;
    background-color: transparent;
    color: inherit;
    padding-top: 5px;
  }
`
