import React, { useState, useEffect, useRef, useMemo } from 'react'
import * as d3 from 'd3'
import { Container } from './styles'
import { defaultChartSettings } from '../../../defaults/chartSettings'
import { Row, Col, Slider, InputNumber, Input } from 'antd'
import {
  tooltipLayer,
  appendChartLegend
} from '../../../utils/ChartsHelperFunctions'
import { useTranslation } from 'react-i18next'
import withPptDump from '../withPptDump'

const PenaltyChart = ({ cardWidth, inputData, statisticsSettings }) => {
  const { t } = useTranslation()
  const [selectedLabels, setSelectedLabels] = useState(
    inputData.labels.map((el, i) => ({ name: el, index: i, isSelected: true }))
  )

  const chartName = 'pcachart_3d_' + inputData.chart_id
  const chartMargin = { top: 40, right: 100, bottom: 60, left: 150 }
  const svgWidth = parseInt(cardWidth, 10)
  const chartWidth = svgWidth - chartMargin.left - chartMargin.right
  const svgHeight = chartWidth * 0.7
  const chartHeight = svgHeight - chartMargin.top - chartMargin.bottom

  const maxX = useMemo(
    () =>
      d3.max(
        inputData.high_plots
          .concat(inputData.low_plots)
          .filter(el => el)
          .map(el => el[1])
      ) || 1,
    [inputData]
  )

  const maxY = useMemo(
    () =>
      d3.max(
        inputData.high_plots
          .concat(inputData.low_plots)
          .filter(el => el && el[1] !== 0)
          .map(el => el[0])
      ) || 5,
    [inputData]
  )
  const minY = useMemo(
    () =>
      d3.min(
        inputData.high_plots
          .concat(inputData.low_plots)
          .filter(el => el && el[1] !== 0)
          .map(el => el[0])
      ) || 0,
    [inputData]
  )

  const [penalty, setPenalty] = useState(inputData.penalty)
  const [percentage, setPercentage] = useState(inputData.percentage)
  const [penaltyScale, setPenaltyScale] = useState([
    Math.min(minY, 0) * 1.25,
    maxY * 1.25
  ])
  const [percentageScale, setPercentageScale] = useState([0, maxX * 1.2])

  const xScale = d3
    .scaleLinear()
    .rangeRound([0, chartWidth])
    .domain(percentageScale)
  const yScale = d3
    .scaleLinear()
    .rangeRound([chartHeight, 0])
    .domain(penaltyScale)

  const colorsScale = d3
    .scaleOrdinal()
    .range(defaultChartSettings.colorsArr)
    .domain(inputData.labels.map((_el, i) => i))

  const mounted = useRef()

  useEffect(() => {
    if (!mounted.current) {
      mounted.current = true
      initChart(inputData)
    } else {
      updateChart()
    }
  }, [percentage, penalty, penaltyScale, percentageScale, selectedLabels, statisticsSettings.fontFamily_legend, statisticsSettings.fontSize_legend])

  const initChart = () => {
    d3.select('.' + chartName).remove()

    const chartsContainer = d3
      .select('#' + chartName)
      .append('div')
      .attr('class', chartName)
      .append('div')
      .attr('class', 'penaltychart-container')

    const chartSvg = chartsContainer.append('svg')

    const g = chartSvg
      .attr('width', svgWidth)
      .attr('height', svgHeight)
      .append('g')
      .attr('class', 'chart-container')
      .attr(
        'transform',
        'translate(' + chartMargin.left + ',' + chartMargin.top + ')'
      )

    g.append('rect')
      .attr('class', 'clicklistener')
      .style('fill', 'transparent')
      .attr('width', chartWidth)
      .attr('height', chartHeight)

    g.append('g').attr('class', 'axisY')

    g.append('g')
      .attr('class', 'axisX')
      .attr('transform', 'translate(0,' + chartHeight + ')')

    g.append('line')
      .attr('class', 'basic-line')
      .style('stroke', '#999')
      .style('stroke-width', 1)
      .attr('x1', xScale(0))
      .attr('x2', xScale(xScale.domain()[1]))
      .attr('y1', yScale(0))
      .attr('y2', yScale(0))

    g.append('line')
      .attr('class', 'basic-line')
      .style('stroke', '#999')
      .style('stroke-width', 1)
      .attr('x1', xScale(0))
      .attr('x2', xScale(0))
      .attr('y1', yScale(yScale.domain()[0]))
      .attr('y2', yScale(yScale.domain()[1]))

    g.append('line')
      .attr('class', 'percentage-line')
      .style('stroke', '#8aba5c')
      .style('stroke-width', 2)
      .attr('x1', xScale(0))
      .attr('x2', xScale(0))
      .attr('y1', yScale(yScale.domain()[0]))
      .attr('y2', yScale(yScale.domain()[1]))

    g.append('line')
      .attr('class', 'penalty-line')
      .style('stroke', '#8aba5c')
      .style('stroke-width', 2)
      .attr('x1', xScale(0))
      .attr('x2', xScale(xScale.domain()[1]))
      .attr('y1', yScale(0))
      .attr('y2', yScale(0))

    g.append('g').attr('class', 'scatterplot-dots-high')
    g.append('g').attr('class', 'scatterplot-dots-low')

    appendChartLegend(
      chartsContainer,
      selectedLabels,
      colorsScale,
      setSelectedLabels
    )

    initShapeLegend()

    updateChart()
  }

  const updateChart = () => {
    const chartsContainer = d3
      .select('#' + chartName)
      .select('.penaltychart-container')

    chartsContainer
      .select('.axisX')
      .transition()
      .duration(100)
      .call(
        d3
          .axisBottom(xScale)
          .tickFormat(d3.format('.0%'))
          .ticks(10)
          .tickSize(-chartHeight)
          .tickPadding(10)
      )
    chartsContainer
      .select('.axisY')
      .transition()
      .duration(100)
      .call(
        d3
          .axisLeft(yScale)
          .ticks(8)
          .tickSize(-chartWidth)
          .tickPadding(10)
      )

    updateDots(chartsContainer, 'high')
    updateDots(chartsContainer, 'low')

    chartsContainer
      .select('.percentage-line')
      .attr('x1', xScale(percentage))
      .attr('x2', xScale(percentage))
    chartsContainer
      .select('.penalty-line')
      .attr('y1', yScale(penalty))
      .attr('y2', yScale(penalty))

    chartsContainer.select('.clicklistener').on('click', function () {
      const position = d3.mouse(this)
      setPenalty(yScale.invert(position[1]).toFixed(2))
      setPercentage(xScale.invert(position[0]).toFixed(3))
    })

    chartsContainer
      .selectAll('.chart-legend .legend')
      .style('font-size', `${statisticsSettings.fontSize_legend || 1.1}rem`)
      .style('font-family', statisticsSettings.fontFamily_legend || 'Lato')
  }

  const updateDots = (container, plotName) => {
    const dotsSelection = container
      .select(`.scatterplot-dots-${plotName}`)
      .selectAll('.scatterplot-dot')
      .data(
        inputData[`${plotName}_plots`]
          .map((data, i) => ({ data, i }))
          .filter((d, i) => d.data && selectedLabels[i].isSelected),
        d => d.i
      )

    const selectionEnter = dotsSelection.enter()
    let appendElements

    if (plotName === 'high') {
      appendElements = selectionEnter
        .append('circle')
        .attr('class', 'scatterplot-dot')
        .attr('r', 8)
    } else {
      appendElements = selectionEnter
        .append('g')
        .attr('class', 'scatterplot-dot')
      appendElements
        .append('rect')
        .attr('height', 12)
        .attr('width', 12)
        .style('transform', 'translate(0px, -9px) rotate(45deg)')
    }

    const mergedElements = appendElements
      .style('fill', d => colorsScale(d.i))
      .attr('stroke', d => d3.color(colorsScale(d.i)).darker(3))
      .on('mouseover', d => {
        tooltipLayer.showTooltip({
          title: `${inputData.labels[d.i]} - ${t(
            `charts.penaltyChart.too${plotName}`
          )}`,
          results: [
            {
              label: t('charts.penaltyChart.penalty'),
              value: inputData[`${plotName}_plots`][d.i][0]
            },
            {
              label: t('charts.penaltyChart.percentage'),
              value: inputData[`${plotName}_plots`][d.i][1]
            }
          ]
        })
      })
      .on('mousemove', tooltipLayer.updateTooltip)
      .on('mouseout', tooltipLayer.clearTooltip)
      .style('display', d => (d.data[1] === 0 ? 'none' : ''))
      .merge(dotsSelection)

    if (plotName === 'high') {
      mergedElements
        .attr('cx', d => xScale(d.data[1]))
        .attr('cy', d => yScale(d.data[0]))
    } else {
      mergedElements.style(
        'transform',
        d => `translate(${xScale(d.data[1])}px, ${yScale(d.data[0])}px)`
      )
    }

    dotsSelection.exit().remove()
  }

  const initShapeLegend = () => {
    const chartsContainer = d3
      .select('#' + chartName)
      .select('.penaltychart-container')

    const shapeLegendContainer = chartsContainer
      .append('div')
      .attr('class', 'shapeLegendContainer chart-legend')
      .style('position', 'absolute')
      .style('right', `${chartMargin.right}px`)
      .style('top', `${chartMargin.top}px`)

    const toohighCont = shapeLegendContainer
      .append('div')
      .attr('class', 'legend')

    toohighCont
      .append('svg')
      .style('width', 12)
      .style('height', 12)
      .append('circle')
      .attr('r', 4.5)
      .attr('cx', 7)
      .attr('cy', 6)
      .style('stroke', '#555')
      .style('fill', '#eee')

    toohighCont.append('span').text(t(`charts.penaltyChart.toohigh`))

    const toolowCont = shapeLegendContainer
      .append('div')
      .attr('class', 'legend')

    toolowCont
      .append('svg')
      .style('width', 12)
      .style('height', 12)
      .append('rect')
      .attr('width', 7)
      .attr('height', 7)
      .attr('x', 2)
      .attr('y', 2)
      .style('stroke', '#555')
      .style('fill', '#eee')
      .style('transform', 'rotate(45deg) translate(3px, -5px)')

    toolowCont.append('span').text(t(`charts.penaltyChart.toolow`))

    chartsContainer
      .append('div')
      .attr('class', 'chart-legend')
      .style('position', 'absolute')
      .style('bottom', '18px')
      .style('left', '48%')
      .text(t('charts.analysis.penalty.x-axis'))

    chartsContainer
      .append('div')
      .attr('class', 'chart-legend')
      .style('position', 'absolute')
      .style('left', '20px')
      .style('top', '40%')
      .style('transform', 'rotate(-90deg)')
      .text(t('charts.analysis.penalty.y-axis'))

  }

  return (
    <Container>
      <div id={chartName} />
      <Row>
        <Col span={6} className={'sliderdesc'}>
          {t('charts.penaltyChart.penaltyLevel')}:
        </Col>
        <Col span={12}>
          <Slider
            step={0.1}
            value={penalty}
            min={yScale.domain()[0]}
            max={yScale.domain()[1]}
            onChange={setPenalty}
          />
        </Col>
        <Col span={3}>
          <InputNumber
            min={yScale.domain()[0]}
            max={yScale.domain()[1]}
            value={penalty}
            onChange={setPenalty}
          />
        </Col>
      </Row>
      <Row>
        <Col span={6} className={'sliderdesc'}>
          {t('charts.penaltyChart.percentageLevel')}:
        </Col>
        <Col span={12}>
          <Slider
            value={percentage * 100}
            min={xScale.domain()[0] * 100}
            max={xScale.domain()[1] * 100}
            onChange={val => setPercentage(val / 100)}
          />
        </Col>
        <Col span={3}>
          <InputNumber
            min={xScale.domain()[0] * 100}
            max={xScale.domain()[1] * 100}
            value={Math.floor(percentage * 100)}
            onChange={val => setPercentage(val / 100)}
          />
        </Col>
      </Row>
      <Row>
        <Col span={6} className={'sliderdesc'}>
          {t('charts.penaltyChart.penaltyScale')}:
        </Col>
        <Col span={12}>
          <Slider
            range
            step={0.1}
            value={penaltyScale}
            min={-10}
            max={10}
            onChange={val => {
              setPenaltyScale([Math.min(minY, val[0]), Math.max(maxY, val[1])])
            }}
          />
        </Col>
        <Col span={3}>
          <Input disabled value={`${penaltyScale[0]} / ${penaltyScale[1]}`} />
        </Col>
      </Row>
      <Row>
        <Col span={6} className={'sliderdesc'}>
          {t('charts.penaltyChart.percentageScale')}:
        </Col>
        <Col span={12}>
          <Slider
            step={1}
            value={percentageScale[1] * 100}
            min={maxX * 100}
            max={100}
            onChange={val => setPercentageScale([0, val / 100])}
          />
        </Col>
        <Col span={3}>
          <InputNumber
            min={maxX * 100}
            max={100}
            value={Math.floor(percentageScale[1] * 100)}
            onChange={val => setPercentageScale([0, val / 100])}
          />
        </Col>
      </Row>
    </Container>
  )
}

export default withPptDump('penalty-chart')(PenaltyChart)
