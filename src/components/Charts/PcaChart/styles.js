import styled from 'styled-components'

export const Charts2d = styled.div`
  display: flex;
  justify-content: space-around;
  flex-wrap: wrap;
  margin-bottom: 12px;

  > div {
    flex-basis: 50%;
    width: 50%;
  }

  .xLabel {
    font-size: 1.1rem;
  }
`
