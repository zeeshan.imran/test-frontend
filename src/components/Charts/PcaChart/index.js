import React, { useState, useEffect } from 'react'
import * as d3 from 'd3'
import { Table } from 'antd'
import { useTranslation } from 'react-i18next'
import ScatterPlot3d from './ScatterPlot3d'
import { Charts2d } from './styles'
import { StatisticsTableContainer, DefaultChart } from '../styles'
import { defaultChartSettings } from '../../../defaults/chartSettings'
import {
  appendChartLegend,
  updateChartLegendColorScheme
} from '../../../utils/ChartsHelperFunctions'
import withPptDump from '../withPptDump'

const PcaChart = ({
  cardWidth,
  inputData,
  chartSettings,
  statisticsSettings
}) => {
  const { t } = useTranslation()
  const chartName = 'pcachart_all_' + inputData.chart_id
  const [products, setProducts] = useState(
    inputData.products.map((el, i) => ({
      name: el,
      index: i,
      isSelected: true
    }))
  )

  useEffect(() => {
    initChart()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const columnsAxes = inputData.labels.map((label, i) => ({
    title: `${label}: ${inputData.explained_percentage[i].toFixed(2)}`,
    dataIndex: label,
    key: label.toLowerCase()
  }))
  const productRows = inputData.result_products.map((el, i) => {
    let axesValues = {}
    inputData.labels.forEach((l, l_i) => {
      axesValues[l] = el[l_i].toFixed(3)
    })
    return {
      product: inputData.products[i],
      key: i,
      ...axesValues
    }
  })
  const attributesRows = inputData.result_attributes.map((el, i) => {
    let axesValues = {}
    inputData.labels.forEach((l, l_i) => {
      axesValues[l] = el[l_i].toFixed(3)
    })
    return {
      attribute: inputData.attributes[i],
      key: i,
      ...axesValues
    }
  })

  const colorsScale = d3
    .scaleOrdinal()
    .range(chartSettings.colorsArr || defaultChartSettings.colorsArr)
    .domain(
      inputData.products.length >= inputData.attributes.length
        ? inputData.products.map((_el, i) => i)
        : inputData.attributes.map((_el, i) => i)
    )

  useEffect(() => {
    updateChartLegendColorScheme(
      d3.select('#' + chartName),
      colorsScale.copy().domain(products.map(el => el.index))
    )
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [chartSettings.colorsArr])

  const initChart = () => {
    const chartsContainer = d3.select('#' + chartName)
    appendChartLegend(
      chartsContainer,
      products,
      colorsScale.copy().domain(products.map(el => el.index)),
      setProducts,
      true
    )
  }
  return (
    <React.Fragment>
      <DefaultChart id={chartName}>
        <ScatterPlot3d
          cardWidth={cardWidth}
          inputData={inputData}
          chartSettings={chartSettings}
          statisticsSettings={statisticsSettings}
          colorsScale={colorsScale}
          products={products}
        />
        <Charts2d>
          <ScatterPlot3d
            cardWidth={`${parseInt(cardWidth, 10) / 1.8}px`}
            inputData={{ ...inputData, chart_id: `${inputData.chart_id}-xz` }}
            params={{ isFixed: true, rotationX: 0, rotationY: 0 }}
            chartSettings={chartSettings}
            statisticsSettings={statisticsSettings}
            colorsScale={colorsScale}
            products={products}
          />
          <ScatterPlot3d
            cardWidth={`${parseInt(cardWidth, 10) / 1.8}px`}
            inputData={{ ...inputData, chart_id: `${inputData.chart_id}-yz` }}
            params={{
              isFixed: true,
              rotationX: 0,
              rotationY: Math.PI / 2
            }}
            chartSettings={chartSettings}
            statisticsSettings={statisticsSettings}
            colorsScale={colorsScale}
            products={products}
          />
          <ScatterPlot3d
            cardWidth={`${parseInt(cardWidth, 10) / 1.8}px`}
            inputData={{ ...inputData, chart_id: `${inputData.chart_id}-xy` }}
            params={{
              isFixed: true,
              rotationX: Math.PI / 2,
              rotationY: 0
            }}
            chartSettings={chartSettings}
            statisticsSettings={statisticsSettings}
            colorsScale={colorsScale}
            products={products}
          />
        </Charts2d>
      </DefaultChart>
      <StatisticsTableContainer>
        <Table
          dataSource={productRows}
          pagination={false}
          columns={[
            {
              title: t('charts.analysis.tableHeaders.product'),
              dataIndex: 'product',
              key: 'product'
            },
            ...columnsAxes
          ]}
          scroll={{ x: 880 }}
        />
        <Table
          dataSource={attributesRows}
          pagination={false}
          columns={[
            {
              title: t('charts.analysis.tableHeaders.attribute'),
              dataIndex: 'attribute',
              key: 'attribute'
            },
            ...columnsAxes
          ]}
          scroll={{ x: 880 }}
        />
      </StatisticsTableContainer>
    </React.Fragment>
  )
}

export default withPptDump('pca', () => ({}))(PcaChart)
