import styled from 'styled-components'
import { DefaultChart } from '../../styles'

export const Container = styled(DefaultChart)`
  text-align: center;

  .ant-btn {
    margin: 20px auto;
  }

  .pca-3d-label {
    font-size: 10px;
    font-weight: 700;
  }
`
