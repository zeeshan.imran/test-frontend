import React, { useState, useEffect, useRef } from 'react'
import * as d3 from 'd3'
import { flatten, concat, sortBy, prop, uniqBy } from 'ramda'
import { _3d as d3_3d } from 'd3-3d'
import { Container } from './styles'
import {
  tooltipLayer,
  hasCollision
} from '../../../../utils/ChartsHelperFunctions'
import withPptDump from '../../withPptDump'

const extentPadding = 0.2
const vectorEndingRadius = 3
const gridFill = '#E6ECF6'

// Based on https://bl.ocks.org/niekes/1c15016ae5b5f11508f92852057136b5
const ScatterPlot3d = ({
  cardWidth,
  inputData,
  params = {},
  chartSettings,
  statisticsSettings,
  colorsScale,
  products
}) => {
  const chartName = 'pcachart_3d_' + inputData.chart_id
  const svgWidth = parseInt(cardWidth, 10) - 48 + 'px'
  const svgHeight = parseInt(cardWidth, 10) * 0.75 + 'px'
  const mounted = useRef()
  const startAngleY = isNaN(params.rotationY) ? Math.PI / 3.5 : params.rotationY
  const startAngleX = isNaN(params.rotationX) ? Math.PI / 7 : params.rotationX

  let highligtedLines = []

  const appendPointToHighlights = d => {
    highligtedLines.push(
      [
        { x: d.x, y: d.y, z: d.z },
        {
          x: d.x,
          y: chartState.maxValue * (params.isFixed ? 1 : -1),
          z: d.z,
          id: !isNaN(d.productIndex) ? d.productIndex : d.attributeIndex
        }
      ],
      [
        { x: d.x, y: d.y, z: d.z },
        {
          x: chartState.maxValue * (params.isFixed ? 1 : -1),
          y: d.y,
          z: d.z,
          id: !isNaN(d.productIndex) ? d.productIndex : d.attributeIndex
        }
      ],
      [
        { x: d.x, y: d.y, z: d.z },
        {
          x: d.x,
          y: d.y,
          z: chartState.maxValue * (params.isFixed ? 1 : -1),
          id: !isNaN(d.productIndex) ? d.productIndex : d.attributeIndex
        }
      ]
    )
  }

  const [chartState] = useState(() => {
    const maxValue = Math.ceil(
      d3.max(
        concat(
          flatten(inputData.result_attributes),
          flatten(inputData.result_products)
        )
      ) *
        (1 + extentPadding)
    )

    const scale = (params.isFixed ? 150 : 190) / maxValue
    const ticks = d3
      .scaleLinear([-maxValue, maxValue], [0, svgHeight])
      .ticks(10)
    const origin = [parseInt(svgWidth, 10) / 2, parseInt(svgHeight, 10) / 2]

    const grid3d = d3_3d()
      .shape('GRID', ticks.length)
      .origin(origin)
      .rotateY(startAngleY)
      .rotateX(-startAngleX)
      .scale(scale)

    const point3d = d3_3d()
      .x(d => -d.x)
      .y(d => -d.y)
      .z(d => d.z)
      .origin(origin)
      .rotateY(startAngleY)
      .rotateX(-startAngleX)
      .scale(scale)

    const line3d = d3_3d()
      .shape('LINE', ticks.length)
      .x(d => -d.x)
      .y(d => -d.y)
      .z(d => d.z)
      .origin(origin)
      .rotateY(startAngleY)
      .rotateX(-startAngleX)
      .scale(scale)

    let productsData = inputData.result_products.map((el, i) => {
      // distance to plane 0=-x+y+z
      // division by Math.sqrt(A^2 + B^2 + C^2) omitted to improve performance. This does not change the order
      const distanceToOrigin = el[0] + el[1] + el[2]
      return {
        data: el,
        product: inputData.products[i],
        productIndex: i,
        distanceToOrigin
      }
    })
    // Sort points based on distance to plane 0=-x+y+z
    productsData = sortBy(prop('distanceToOrigin'), productsData)

    let attributesData = inputData.result_attributes.map((el, i) => {
      const distanceToOrigin = el[0] + el[1] + el[2]
      return {
        data: el,
        attribute: inputData.attributes[i],
        attributeIndex: i,
        distanceToOrigin
      }
    })
    attributesData = sortBy(prop('distanceToOrigin'), attributesData)

    // Grid
    const gridLines_y = []
    ticks.forEach(z => {
      ticks.forEach(x => {
        gridLines_y.push([x, maxValue, z])
      })
    })
    const gridLines_x = []
    ticks.forEach(z => {
      ticks.forEach(y => {
        gridLines_x.push([maxValue, y, z])
      })
    })
    const gridLines_z = []
    ticks.forEach(x => {
      ticks.forEach(y => {
        gridLines_z.push([x, y, -maxValue])
      })
    })

    const axesLines = params.isFixed
      ? [
          [
            { x: 0, y: 0, z: -maxValue },
            { x: 0, y: 0, z: maxValue, id: `axes-x` }
          ],
          [
            { x: -maxValue, y: 0, z: 0 },
            { x: maxValue, y: 0, z: 0, id: `axes-y` }
          ],
          [
            { x: 0, y: maxValue, z: 0 },
            { x: 0, y: -maxValue, z: 0, id: `axes-z` }
          ]
        ]
      : [
          [
            { x: -maxValue, y: -maxValue, z: -maxValue },
            { x: -maxValue, y: -maxValue, z: maxValue, id: `axes-x` }
          ],
          [
            { x: -maxValue, y: -maxValue, z: -maxValue },
            { x: -maxValue, y: maxValue, z: -maxValue, id: `axes-y` }
          ],
          [
            { x: -maxValue, y: -maxValue, z: -maxValue },
            { x: maxValue, y: -maxValue, z: -maxValue, id: `axes-z` }
          ]
        ]

    const axisTicks = d3
      .scaleLinear()
      .domain([-maxValue, maxValue])
      .ticks(5)

    const axisPoints = {
      z_bottom: [
        {
          z: 0 + maxValue / 64,
          x: maxValue * (params.isFixed ? 1.25 : 1.2),
          y: -maxValue * 1.2,
          id: 'z_bottom',
          label: `${
            inputData.labels[2]
          }: ${inputData.explained_percentage[2].toFixed(2)}`
        }
      ],
      z_top: [
        {
          z: 0,
          x: -maxValue * 1.16,
          y: maxValue * 1.16,
          id: 'z_top',
          label: `${
            inputData.labels[2]
          }: ${inputData.explained_percentage[2].toFixed(2)}`
        }
      ],
      x_bottom: [
        {
          z: maxValue * 1.2,
          x: 0,
          y: -maxValue * 1.2,
          id: 'x_bottom',
          label: `${
            inputData.labels[0]
          }: ${inputData.explained_percentage[0].toFixed(2)}`
        }
      ],
      x_top: [
        {
          z: -maxValue * 1.16,
          x: 0,
          y: maxValue * 1.16,
          id: 'x_top',
          label: `${
            inputData.labels[0]
          }: ${inputData.explained_percentage[0].toFixed(2)}`
        }
      ],
      y_left: [
        {
          z: -maxValue * 1.2,
          x: maxValue * (params.isFixed ? 1.25 : 1.2),
          y: 0 - maxValue / 64,
          id: 'y_left',
          label: `${
            inputData.labels[1]
          }: ${inputData.explained_percentage[1].toFixed(2)}`
        }
      ],
      y_right: [
        {
          z: maxValue * (params.isFixed ? 1.25 : 1.2),
          x: -maxValue * 1.2,
          y: 0 - maxValue / 64,
          id: 'y_right',
          label: `${
            inputData.labels[1]
          }: ${inputData.explained_percentage[1].toFixed(2)}`
        }
      ]
    }

    axisTicks.forEach((tick, i) => {
      axisPoints.z_bottom.push({
        z:
          i !== axisTicks.length - 1
            ? tick + maxValue / 64
            : tick + maxValue / 32,
        x: maxValue * 1.04,
        y: -maxValue * 1.04,
        id: `z_bottom${tick}`,
        label: tick
      })
      axisPoints.z_top.push({
        z: tick,
        x: -maxValue * 1.03,
        y: maxValue * 1.03,
        id:
          i === axisTicks.length - 1
            ? `y_right${tick}`
            : i === 0
            ? `x_top${tick}`
            : `z_top${tick}`,
        label: tick
      })
      axisPoints.x_bottom.push({
        z: maxValue * 1.06,
        x: tick,
        y: -maxValue * 1.04,
        id: i !== axisTicks.length - 1 ? `x_bottom${tick}` : `z_bottom${tick}`,
        label: tick
      })
      axisPoints.x_top.push({
        z: -maxValue * 1.03,
        x: tick,
        y: maxValue * 1.03,
        id: i !== axisTicks.length - 1 ? `x_top${tick}` : `y_left${tick}`,
        label: tick
      })
      axisPoints.y_left.push({
        z: -maxValue * 1.04,
        x: maxValue * 1.04,
        y: tick - maxValue / 64,
        id: i !== 0 ? `y_left${tick}` : `z_bottom${tick}`,
        label: tick
      })
      axisPoints.y_right.push({
        z: maxValue * 1.04,
        x: -maxValue * 1.04,
        y: tick - maxValue / 64,
        id: i !== 0 ? `y_right${tick}` : `x_bottom${tick}`,
        label: tick
      })
    })

    // Data points
    const scatter = []
    productsData.forEach((prData, i) => {
      // Assumed that order is the same with labels
      scatter.push({
        x: prData.data[0],
        y: prData.data[1],
        z: prData.data[2],
        productIndex: prData.productIndex,
        label: prData.product,
        id: `product-point-${i}`
      })
    })

    // Data vectors
    const vectors = []
    attributesData.forEach((atData, i) => {
      vectors.push([
        { x: 0, y: 0, z: 0 },
        {
          x: atData.data[0],
          y: atData.data[1],
          z: atData.data[2],
          id: `attribute-point-${i}`,
          attributeIndex: i,
          label: atData.attribute,
        }
      ])
    })
    return {
      grid3d,
      line3d,
      point3d,
      gridLines_y,
      gridLines_x,
      gridLines_z,
      axesLines,
      scatter,
      vectors,
      axisPoints,
      maxValue
    }
  })

  let mx, my, mouseX, mouseY
  const dragStart = () => {
    d3.select('#' + chartName)
      .select('.pca-3d-chart--labels')
      .remove()
    mx = d3.event.x
    my = d3.event.y
  }

  const dragged = () => {
    mouseX = mouseX || 0
    mouseY = mouseY || 0
    const beta = ((d3.event.x - mx + mouseX) * Math.PI) / 230
    const alpha = (((d3.event.y - my + mouseY) * Math.PI) / 230) * -1
    // Keep rotation in boundaries [0,Pi/2] [-Pi/2,0]
    const rotationY = Math.min(Math.max(0, beta + startAngleY), Math.PI / 2)
    const rotationX = Math.max(Math.min(0, alpha - startAngleX), -Math.PI / 2)
    if (rotationY !== beta + startAngleY) {
      mx = d3.event.x + mouseX - ((rotationY - startAngleY) * 230) / Math.PI
    }
    if (rotationX !== alpha - startAngleX) {
      my = d3.event.y + mouseY - ((rotationX + startAngleX) * -230) / Math.PI
    }

    chartState.grid3d.rotateY(rotationY).rotateX(rotationX)
    chartState.point3d.rotateY(rotationY).rotateX(rotationX)
    chartState.line3d.rotateY(rotationY).rotateX(rotationX)
    updateChart()
  }

  const dragEnd = () => {
    mouseX = d3.event.x - mx + mouseX
    mouseY = d3.event.y - my + mouseY
    renderLabels()
  }

  useEffect(() => {
    if (!mounted.current) {
      mounted.current = true
      initChart(inputData)
    } else {
      updateChart()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    applySettings()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    statisticsSettings.isLabelsShown,
    statisticsSettings.isProductsShown,
    statisticsSettings.isProductLabelsShown,
    statisticsSettings.isVectorsShown,
    statisticsSettings.isAttributeLabelsShown,
    statisticsSettings.isVectorsShown_size
  ])

  useEffect(() => {
    updateChart()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [products])

  useEffect(() => {
    initChart()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [chartSettings.colorsArr])

  const initChart = async inputData => {
    d3.select('.' + chartName).remove()

    const chartsContainer = d3
      .select('#' + chartName)
      .append('div')
      .attr('class', chartName)
      .append('div')
      .attr('class', 'pca-3d-container')

    let chartSvg = chartsContainer
      .append('svg')
      .style('width', svgWidth)
      .style('height', svgHeight)

    if (!params.isFixed) {
      chartSvg.call(
        d3
          .drag()
          .on('start', dragStart)
          .on('drag', dragged)
          .on('end', dragEnd)
      )
    } else {
      chartSvg.on('click', renderLabels)
    }
    chartSvg = chartSvg.append('g').classed('pca-3d-chart', true)

    chartSvg.append('g').classed('pca-3d-container-grid-y', true)
    chartSvg.append('g').classed('pca-3d-container-grid-x', true)
    chartSvg.append('g').classed('pca-3d-container-grid-z', true)
    chartSvg.append('g').classed('pca-3d-container-axis', true)
    chartSvg.append('g').classed('pca-3d-container-axes-lines', true)
    chartSvg.append('g').classed('pca-3d-container-highlight', true)
    chartSvg.append('g').classed('pca-3d-container-vectors', true)
    chartSvg.append('g').classed('pca-3d-container-points', true)

    updateChart()
    renderLabels()
  }

  const updateChart = () => {
    /* ----------- GRID ----------- */

    const chartSvg = d3
      .select('#' + chartName)
      .select('.pca-3d-container svg .pca-3d-chart')
    const yGrid = chartSvg
      .select('.pca-3d-container-grid-y')
      .selectAll('path.grid')
      .data(chartState.grid3d(chartState.gridLines_y), d => d.id)

    yGrid
      .enter()
      .append('path')
      .attr('class', '_3d grid')
      .merge(yGrid)
      .attr('stroke', '#fff')
      .attr('stroke-width', 0.6)
      .attr('fill', gridFill)
      .attr('fill-opacity', 0.8)
      .attr('d', chartState.grid3d.draw)

    yGrid.exit().remove()

    const xGrid = chartSvg
      .select('.pca-3d-container-grid-x')
      .selectAll('path.grid')
      .data(chartState.grid3d(chartState.gridLines_x), d => d.id)

    xGrid
      .enter()
      .append('path')
      .attr('class', '_3d grid')
      .merge(xGrid)
      .attr('stroke', '#fff')
      .attr('stroke-width', 0.6)
      .attr('fill', gridFill)
      .attr('fill-opacity', 0.8)
      .attr('d', chartState.grid3d.draw)

    xGrid.exit().remove()

    const zGrid = chartSvg
      .select('.pca-3d-container-grid-z')
      .selectAll('path.grid')
      .data(chartState.grid3d(chartState.gridLines_z), d => d.id)

    zGrid
      .enter()
      .append('path')
      .attr('class', '_3d grid')
      .merge(zGrid)
      .attr('stroke', '#fff')
      .attr('stroke-width', 0.6)
      .attr('fill', gridFill)
      .attr('fill-opacity', 0.8)
      .attr('d', chartState.grid3d.draw)

    zGrid.exit().remove()

    /* ----------- axis -----------*/
    let axisPoints = []
    const rotateX = chartState.point3d.rotateX()
    const rotateY = chartState.point3d.rotateY()

    // Determine what axis labels to show based on rotation angle
    // if (!(rotateX > -Math.PI / 26 && rotateY < Math.PI / 26)) {
    if (rotateX < -Math.PI / 22) {
      axisPoints = axisPoints.concat(chartState.axisPoints.z_bottom)
    } else if (rotateY > Math.PI / 26) {
      axisPoints = axisPoints.concat(chartState.axisPoints.z_top)
    }
    if (rotateX < -Math.PI / 22) {
      axisPoints = axisPoints.concat(chartState.axisPoints.x_bottom)
    } else if (rotateY < (Math.PI * 12) / 26) {
      axisPoints = axisPoints.concat(chartState.axisPoints.x_top)
    }
    if (rotateY < Math.PI / 4 && rotateX > (-Math.PI * 12) / 26) {
      axisPoints = axisPoints.concat(chartState.axisPoints.y_left)
    } else if (rotateX > (-Math.PI * 12) / 26) {
      axisPoints = axisPoints.concat(chartState.axisPoints.y_right)
    }
    // Removes repeated axis labels (appears on axis intersections)
    axisPoints = uniqBy(a => a.id, axisPoints)

    const axisLabels = chartSvg
      .select('.pca-3d-container-axis')
      .selectAll('.xLabel')
      .data(chartState.point3d(axisPoints), d => d.id)

    axisLabels
      .enter()
      .append('text')
      .attr('class', 'xLabel')
      .style('text-anchor', 'middle')
      .attr('data-id', d => d.id)
      .merge(axisLabels)
      .text(d => d.label)
      .attr('x', d => d.projected.x)
      .attr('y', d => d.projected.y)

    axisLabels.exit().remove()

    /* --------- axes lines ---------- */

    const axesLines = chartSvg
      .select('.pca-3d-container-axes-lines')
      .selectAll('.axis-line')
      .data(chartState.line3d(chartState.axesLines), d => d.id)

    axesLines
      .enter()
      .append('line')
      .attr('class', 'axis-line')
      .attr('stroke-width', 1)
      .attr('stroke', '#ADADAD')
      .merge(axesLines)
      .attr('x1', d => d[0].projected.x)
      .attr('y1', d => d[0].projected.y)
      .attr('x2', d => d[1].projected.x)
      .attr('y2', d => d[1].projected.y)

    axesLines.exit().remove()

    /* ----------- vectors ----------- */

    const lines = chartSvg
      .select('.pca-3d-container-vectors')
      .selectAll('.vector-group')
      .data(chartState.line3d(chartState.vectors), d => d.id)

    lines
      .enter()
      .append('g')
      .attr('class', 'vector-group')
      .style('opacity', statisticsSettings.isVectorsShown ? 1 : 0)
      .style(
        'pointer-events',
        statisticsSettings.isVectorsShown ? 'auto' : 'none'
      )
      .each(function (_d, i) {
        d3.select(this)
          .append('line')
          .attr('stroke-width', 1)
          .attr('stroke', d => colorsScale(d[1].attributeIndex))
        d3.select(this)
          .append('circle')
          .attr(
            'r',
            vectorEndingRadius * statisticsSettings.isVectorsShown_size || 1
          )
          .attr('fill', d =>
            d3.color(colorsScale(d[1].attributeIndex)).darker(1)
          )
          .on('mouseover', d => {
            highligtedLines = []
            appendPointToHighlights(d[1])
            tooltipLayer.showTooltip({
              title: inputData.attributes[i],
              results: [
                {
                  label: inputData.labels[0],
                  value: d[1].x.toFixed(3)
                },
                {
                  label: inputData.labels[1],
                  value: d[1].y.toFixed(3)
                },
                {
                  label: inputData.labels[2],
                  value: d[1].z.toFixed(3)
                }
              ]
            })
            updateHighlight()
          })
          .on('mousemove', tooltipLayer.updateTooltip)
          .on('mouseout', d => {
            highligtedLines = []
            tooltipLayer.clearTooltip()
            updateHighlight()
          })
      })
      .merge(lines)
      .each(function (d) {
        d3.select(this)
          .select('line')
          .attr('x1', d => d[0].projected.x)
          .attr('y1', d => d[0].projected.y)
          .attr('x2', d => d[1].projected.x)
          .attr('y2', d => d[1].projected.y)
        d3.select(this)
          .select('circle')
          .attr('cx', d => d[1].projected.x)
          .attr('cy', d => d[1].projected.y)
      })

    lines.exit().remove()

    /* ----------- Highlight handle ---------*/
    const updateHighlight = () => {
      const hLinesSelection = chartSvg
        .select('.pca-3d-container-highlight')
        .selectAll('line')
        .data(chartState.line3d(highligtedLines), (d, i) => i)

      hLinesSelection
        .enter()
        .append('line')
        .attr('stroke-width', 1)
        .attr('stroke', d => colorsScale(d[1].id))
        .merge(hLinesSelection)
        .attr('x1', d => d[0].projected.x)
        .attr('y1', d => d[0].projected.y)
        .attr('x2', d => d[1].projected.x)
        .attr('y2', d => d[1].projected.y)

      hLinesSelection.exit().remove()
    }

    /* ----------- Product bubbles ----------- */

    const points = chartSvg
      .select('.pca-3d-container-points')
      .selectAll('circle')
      .data(chartState.point3d(chartState.scatter), d => d.id)

    points
      .enter()
      .append('circle')
      .attr('class', '_3d_product')
      .merge(points)
      .attr('r', 10)
      .attr('stroke', d => d3.color(colorsScale(d.productIndex)).darker(3))
      .attr('fill', d => colorsScale(d.productIndex))
      .attr('cx', d => d.projected.x)
      .attr('cy', d => d.projected.y)
      .style('opacity', d =>
        products[d.productIndex].isSelected &&
        statisticsSettings.isProductsShown
          ? 1
          : 0
      )
      .style(
        'pointer-events',
        statisticsSettings.isProductsShown ? 'auto' : 'none'
      )
      .on('click', d => console.log('clicked: ', d))
      .on('mouseover', d => {
        highligtedLines = []
        appendPointToHighlights(d)
        tooltipLayer.showTooltip({
          title: d.label,
          results: [
            {
              label: inputData.labels[0],
              value: d.x.toFixed(3)
            },
            {
              label: inputData.labels[1],
              value: d.y.toFixed(3)
            },
            {
              label: inputData.labels[2],
              value: d.z.toFixed(3)
            }
          ]
        })
        updateHighlight()
      })
      .on('mousemove', tooltipLayer.updateTooltip)
      .on('mouseleave', d => {
        highligtedLines = []
        updateHighlight()
        tooltipLayer.clearTooltip()
      })

    points.exit().remove()

    if (highligtedLines.length > 0) {
      updateHighlight()
    }
  }

  const renderLabels = () => {
    const chartContainer = d3.select('#' + chartName).select('svg')

    chartContainer.select('.pca-3d-chart--labels').remove()

    const labelsContainer = chartContainer
      .append('g')
      .attr('class', 'pca-3d-chart--labels')

    // Place and label location
    const labels = [],
      pivots = [],
      links = []

    chartContainer
      .selectAll('._3d_product, .vector-group circle')
      .data()
      .forEach((el, i) => {
        const maxLabelLength = 15
        const abbrText = '..'
        const point = Array.isArray(el) ? el[1] : el
        let label = point.label
        
        if (label.length > maxLabelLength) {
          label = label.slice(0, maxLabelLength - abbrText.length) + abbrText
        }

        labels.push({
          x: point.projected.x,
          y: point.projected.y,
          productIndex: el.productIndex,
          attributeIndex: el.attributeIndex,
          id: i,
          label
        })
        pivots.push({
          x: point.projected.x,
          y: point.projected.y,
          fx: point.projected.x,
          fy: point.projected.y,
          id: `${i}-pivot`,
          fixed: true,
          label
        })
        links.push({
          source: i,
          target: `${i}-pivot`,
          productIndex: el.productIndex,
          attributeIndex: el.attributeIndex
        })
      })

    const placeLabels = labelsContainer
      .selectAll('.pca-3d-label')
      .data(labels)
      .enter()
      .append('text')
      .attr('class', 'pca-3d-label')
      .attr('x', d => d.x)
      .attr('y', d => d.y)
      .attr('text-anchor', 'middle')
      .text(d => d.label)
      .style('cursor', 'default')
      .style(
        'opacity',
        d => {
          if (d.productIndex !== undefined) {
            return statisticsSettings.isProductsShown && statisticsSettings.isProductLabelsShown ? 1 : 0
          } else {
            return statisticsSettings.isVectorsShown && statisticsSettings.isAttributeLabelsShown ? 1 : 0
          }
        }
      )

    const link = labelsContainer
      .append('g')
      .attr('class', 'links')
      .selectAll('line')
      .data(links)
      .enter()
      .append('line')
      .attr('stroke-width', 0.6)
      .attr('stroke', d => colorsScale(d.productIndex))
      .style(
        'opacity',
        d => {
          if (d.productIndex !== undefined) {
            return statisticsSettings.isProductsShown && statisticsSettings.isProductLabelsShown ? 1 : 0
          } else {
            return statisticsSettings.isVectorsShown && statisticsSettings.isAttributeLabelsShown ? 1 : 0
          }
        }
      )

    // Create the force layout with a slightly weak charge
    const repelForce = d3
      .forceManyBody()
      .strength(-20)
      .distanceMax(svgWidth / 8)
    const collideForce = d3.forceCollide(10)
    const linksForce = d3
      .forceLink(links)
      .id(d => d.id)
      .distance(25)
    const force = d3
      .forceSimulation()
      .nodes(labels.concat(pivots))
      .force('repelForce', repelForce)
      .force('linksForce', linksForce)
      .force('collideForce', collideForce)

    let tryCounter = 25

    force.on('tick', () => {
      // Update the position of the text element
      placeLabels
        .attr('x', d => d.x)
        .attr('y', (d, i) => (pivots[i].y < d.y ? d.y + 6 : d.y))
        .attr('text-anchor', (d, i) =>
          pivots[i].x > d.x ? 'end' : pivots[i].x < d.x ? 'start' : 'middle'
        )
      link
        .attr('x1', d => d.source.x)
        .attr('y1', d => d.source.y)
        .attr('x2', d => d.target.x)
        .attr('y2', d => d.target.y)

      if (--tryCounter === 0 || !hasOverlap()) {
        force.stop()
      } else {
        force.tick()
      }
    })

    force.tick()

    const hasOverlap = () => {
      let overlap = false
      placeLabels.each(function (d) {
        const bbox1 = d3
          .select(this)
          .node()
          .getBoundingClientRect()
        if (!overlap) {
          placeLabels.each(function (d2) {
            if (d.id === d2.id) return
            const bbox2 = d3
              .select(this)
              .node()
              .getBoundingClientRect()
            if (hasCollision(bbox1, bbox2)) {
              overlap = true
            }
          })
          chartContainer.selectAll('._3d_product').each(function (d2) {
            const bbox2 = d3
              .select(this)
              .node()
              .getBoundingClientRect()
            if (hasCollision(bbox1, bbox2)) {
              overlap = true
            }
          })
        }
      })
      return overlap
    }
  }

  const applySettings = () => {
    const chartSvg = d3.select('#' + chartName).select('svg')

    if (!params.isFixed) {
      chartSvg.call(
        d3
          .drag()
          .on('start', dragStart)
          .on('drag', dragged)
          .on('end', dragEnd)
      )
      renderLabels()
    } else {
      chartSvg.on('click', renderLabels)
      renderLabels()
    }

    chartSvg
      .selectAll('._3d_product')
      .style('opacity', statisticsSettings.isProductsShown ? 1 : 0)
      .style(
        'pointer-events',
        statisticsSettings.isProductsShown ? 'auto' : 'none'
      )
    chartSvg
      .selectAll('.vector-group')
      .style('opacity', statisticsSettings.isVectorsShown ? 1 : 0)
      .style(
        'pointer-events',
        statisticsSettings.isVectorsShown ? 'auto' : 'none'
      )
    chartSvg
      .selectAll('.vector-group circle')
      .attr(
        'r',
        vectorEndingRadius * statisticsSettings.isVectorsShown_size || 1
      )
  }

  return (
    <Container>
      <div id={chartName} />
    </Container>
  )
}

export default withPptDump('pca-plot', () => ({}))(ScatterPlot3d)
