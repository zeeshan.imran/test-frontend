import React, { useState, useEffect, useRef, useMemo } from 'react'
import * as d3 from 'd3'
import { Container } from './styles'
import { getSelectedData } from '../../../containers/Charts/ColumnsChart/GetData'
import {
  getProducts,
  getMaxSelectedValue,
  getTotalSum
} from '../../../containers/Charts/GetData'
import {
  appendChartLegend,
  updateChartLegendColorScheme,
  tooltipLayer
} from '../../../utils/ChartsHelperFunctions'
import { defaultChartSettings } from '../../../defaults/chartSettings'
import { NOT_PRODUCT } from '../../../utils/chartUtils'
import { findIndex, propEq } from 'ramda'
import withPptDump from '../withPptDump'

const getInitialProducts = inputData =>
  getProducts(inputData)
    ? getProducts(inputData).map((el, i) => ({
        name: el,
        index: i,
        isSelected: true
      }))
    : [{ name: NOT_PRODUCT, index: 0, isSelected: true }]

const BarChart = ({ cardWidth, inputData, chartSettings = {}, t }) => {
  const chartName = 'barchart_' + inputData.chart_id
  const chartMargin = { top: 16, right: 110, bottom: 25, left: 100 }

  const labels = inputData.labels
  const [products, setProducts] = useState(getInitialProducts(inputData))

  const minBarHeight = 16
  const minGroupHeight = 60
  const groupHeight = useMemo(() => {
    const prodsCount = getInitialProducts(inputData).length
    return Math.max(prodsCount * minBarHeight, minGroupHeight)
  }, [inputData])
  const svgWidth = parseInt(cardWidth, 10) * 0.95 + 'px'
  const chartWidth =
    parseInt(svgWidth, 10) - chartMargin.left - chartMargin.right
  const chartHeight = groupHeight * labels.length

  const yScale = d3
    .scaleBand()
    .rangeRound([0, chartHeight])
    .paddingInner(0.2)
    .paddingOuter(0.1)
    .domain(labels)

  const colorsScale = d3
    .scaleOrdinal()
    .range(chartSettings.colorsArr || defaultChartSettings.colorsArr)
    .domain(
      products && products.length > 1 ? products.map(el => el.index) : labels
    )

  const mounted = useRef()

  useEffect(() => {
    if (!mounted.current) {
      initChart(inputData)
      mounted.current = true
    } else {
      updateChart()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    products,
    chartSettings.isCountProGroupShown,
    chartSettings.isCountProLabelShown,
    chartSettings.hasOutline,
    chartSettings.hasOutline_color,
    chartSettings.hasOutline_width,
    chartSettings.valueType,
    chartSettings.howManyDecimals,
    chartSettings.percentageGrouping,
    chartSettings.isChartGridHidden,
    chartSettings.fontSize_axis,
    chartSettings.fontFamily_axis,
    chartSettings.fontSize_legend,
    chartSettings.fontFamily_legend
  ])

  useEffect(() => {
    if (mounted.current) {
      // Products obj is invalid - reinit
      setProducts(getInitialProducts(inputData))
      mounted.current = false
    }
  }, [chartSettings.isProductLabelsSwapped, inputData])

  useEffect(() => {
    if (mounted.current) {
      updateChart()
      updateChartLegendColorScheme(
        d3.select('#' + chartName).select('.barchart'),
        colorsScale.copy().domain(products.map(el => el.index))
      )
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [chartSettings.colorsArr])

  useEffect(() => {
    if (mounted.current) {
      updateLabelsAbbreviation()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [chartSettings.fontSize_labels, chartSettings.fontFamily_labels])

  const totalSum = useMemo(() => getTotalSum(inputData), [inputData])

  const initChart = async inputData => {
    d3.select('.' + chartName).remove()

    const chartsContainer = d3
      .select('#' + chartName)
      .append('div')
      .attr('class', chartName)
      .append('div')
      .attr('class', 'barchart-container')

    const chartSvg = chartsContainer
      .append('div')
      .attr('class', 'barchart')
      .style('width', svgWidth)
      .append('svg')

    const g = chartSvg
      .attr('width', chartWidth + chartMargin.left + chartMargin.right)
      .attr('height', chartHeight + chartMargin.top + chartMargin.bottom)
      .append('g')
      .attr('class', 'stacked')
      .attr(
        'transform',
        'translate(' + chartMargin.left + ',' + chartMargin.top + ')'
      )

    g.append('g').attr('class', 'axisY')

    g.append('g').attr('class', 'axisX allTicks')

    g.append('g').attr('class', 'barchart-g-cont')

    chartsContainer
      .select('.barchart')
      .append('div')
      .attr('class', 'yLabels')
      .style('width', chartMargin.left - 5 + 'px')
      .style('position', 'absolute')
      .style('top', 0)
      .selectAll('.yLabel')
      .data(labels)
      .enter()
      .append('div')
      .attr('class', 'yLabel')
      .style('position', 'absolute')
      .style('width', chartMargin.left - 5 + 'px')

    const legendContainer = chartsContainer.select('.barchart')

    if (!inputData.results.data && products.length > 1) {
      appendChartLegend(
        legendContainer,
        products,
        colorsScale.copy().domain(products.map(el => el.index)),
        setProducts,
        true
      )
    }

    updateChart()
    updateLabelsAbbreviation()
  }

  const updateChart = () => {
    updateColumnsChart()
  }

  const updateColumnsChart = () => {
    if (getInitialProducts(inputData).length !== products.length) {
      // Hardfix for useEffect race conditions
      return
    }
    const decimals = !isNaN(chartSettings.howManyDecimals)
      ? parseInt(chartSettings.howManyDecimals, 10)
      : 2

    const chartsContainer = d3
      .select('#' + chartName)
      .select('.barchart-container')

    const yInner = d3
      .scaleBand()
      .rangeRound([0, yScale.bandwidth()])
      .paddingInner(0.4)
      .paddingOuter(0.2)
      .domain(products.filter(el => el.isSelected).map(el => el.index))

    let xDomain
    if (chartSettings.valueType === 'percentage') {
      xDomain = [
        0,
        Math.min(
          Math.max(
            getMaxSelectedValue(inputData, products) / Math.max(totalSum, 1)
          ) * 1.2,
          1
        )
      ]
      if (xDomain[1] === 0) {
        xDomain[1] = 1
      }
    } else if (inputData.range) {
      xDomain = [inputData.range[0], inputData.range[1]]
    } else {
      xDomain = [0, Math.max(getMaxSelectedValue(inputData, products) * 1.2, 1)]
    }

    const x = d3
      .scaleLinear()
      .rangeRound([0, chartWidth])
      .domain(xDomain)
      .nice()

    const xAxis = d3
      .axisBottom(x)
      .ticks(5)
      .tickSizeOuter(0)
      .tickSizeInner(chartHeight)
      .tickFormat(
        chartSettings.valueType === 'percentage' ? d3.format('.0%') : null
      )

    const customXAxis = g => {
      const s = g.selection ? g.selection() : g
      g.call(xAxis)
      s.select('.domain').remove()
      s.selectAll('.tick line').attr('y2', chartHeight)
    }

    chartsContainer
      .select('.axisY')
      .call(d3.axisLeft(yScale))
      .each(function () {
        d3.select(this)
          .selectAll('line')
          .attr('transform', 'translate(0, ' + yScale.step() / 2 + ')')

        d3.select(this)
          .selectAll('text')
          .remove()
      })

    chartsContainer
      .select('.axisX')
      .transition()
      .duration(300)
      .call(customXAxis)
      .on('end', function () {
        d3.select(this)
          .selectAll('.tick line')
          .attr('y2', chartHeight)
      })

    const selectedData = getSelectedData(inputData, products)

    const labelsSelection = chartsContainer
      .select('.barchart-g-cont')
      .selectAll('.outerContainer')
      .data(selectedData)

    const groupEnter = labelsSelection
      .enter()
      .append('g')
      .classed('outerContainer', true)

    const bars = groupEnter
      .merge(labelsSelection)
      .selectAll('.innerContainer')
      .data(
        function (d) {
          return d.data
        },
        d => d.name
      )

    const barsToUpdate = bars
      .enter()
      .append('g')
      .classed('innerContainer', true)
      .on('mousemove', tooltipLayer.updateTooltip)
      .on('mouseleave', tooltipLayer.clearTooltip)
      .each(function (d) {
        const yStart =
          yScale(d.label) + yInner(findIndex(propEq('name', d.name), products))
        d3.select(this)
          .append('rect')
          .attr('x', 0)
          .attr('y', yStart)
          .attr('width', yInner.bandwidth())
          .attr('height', 0)
          .style('opacity', 0)
          .transition(300)
          .style('opacity', 1)

        d3.select(this)
          .append('text')
          .attr('text-anchor', 'start')
          .style('font-size', '10px')
          .style('fill', '#595959')
          .attr('x', chartWidth - 4)
          .attr('y', yStart + yInner.bandwidth() / 2 - 1)
          .style('opacity', chartSettings.isCountProLabelShown ? 1 : 0)
      })
      .merge(bars)

    barsToUpdate
      .on('mouseover', d => {
        const title = ['paired-questions', 'slider'].includes(
          inputData.question_type
        )
          ? t('charts.table.attribute')
          : t('charts.table.answer')

        let results = []

        if (['paired-questions', 'slider'].includes(inputData.question_type)) {
          results.push({
            label: t('charts.table.score'),
            value: d.value % 1 === 0 ? d.value : d.value.toFixed(decimals)
          })
        } else {
          results.push(
            {
              label: t('charts.tooltip.total'),
              value: d.value % 1 === 0 ? d.value : d.value.toFixed(decimals)
            },
            {
              label: t('charts.tooltip.percentageOfTotal'),
              value:
                ((100 * d.value) / Math.max(totalSum, 1)).toFixed(decimals) +
                '%'
            }
          )
        }

        if (
          inputData.question_with_products ||
          inputData.question_type === 'matrix'
        ) {
          results.unshift({
            label: t('charts.table.answer'),
            value: d.label
          })
          const prodSum = d3.sum(
            inputData.results[d.productIndex].data,
            el => el.value
          )
          results.push({
            label: chartSettings.isProductLabelsSwapped
              ? t('charts.tooltip.percentageOfTehGroup')
              : t('charts.tooltip.percentageOfProductGroup'),
            value: `${((100 * d.value) / prodSum).toFixed(decimals)}%`
          })
          const labelRes = inputData.results.map(prodRes =>
            prodRes.data.find(el => el.label === d.label)
          )
          results.push({
            label: chartSettings.isProductLabelsSwapped
              ? t('charts.tooltip.percentageOfProductGroup')
              : t('charts.tooltip.percentageOfTehGroup'),
            value: `${(
              (100 * d.value) /
              d3.sum(labelRes, el => el.value)
            ).toFixed(decimals)}%`
          })
        }

        return tooltipLayer.showTooltip({
          title:
            inputData.question_with_products ||
            inputData.question_type === 'matrix'
              ? d.name
              : `${title}: ${d.label}`,
          results
        })
      })
      .each(function (d) {
        const yStart =
          yScale(d.label) + yInner(findIndex(propEq('name', d.name), products))
        const width = x(
          chartSettings.valueType === 'percentage'
            ? d.value / Math.max(totalSum, 1)
            : d.value
        )

        d3.select(this)
          .select('rect')
          .attr('fill', d =>
            colorsScale(
              products && products.length > 1 ? d.productIndex : d.label
            )
          )
          .attr(
            'stroke',
            (chartSettings.hasOutline_color &&
              chartSettings.hasOutline_color[0]) ||
              defaultChartSettings.outlineColor
          )
          .attr(
            'stroke-width',
            chartSettings.hasOutline ? chartSettings.hasOutline_width || 1 : 0
          )
          .transition()
          .duration(mounted.current ? 400 : 0)
          .attr('x', 0)
          .attr('y', yStart)
          .attr('width', width)
          .attr('height', yInner.bandwidth())

        if (chartSettings.isCountProLabelShown) {
          let text
          if (chartSettings.valueType === 'value' || !chartSettings.valueType) {
            // Check if has decimals
            text = ['slider', 'paired-questions'].includes(
              inputData.question_type
            )
              ? d.value.toFixed(decimals)
              : d.value
          } else if (
            inputData.question_with_products ||
            inputData.question_type === 'matrix'
          ) {
            if (
              (chartSettings.percentageGrouping === 'outer_group' &&
                !chartSettings.isProductLabelsSwapped) ||
              (chartSettings.percentageGrouping === 'inner_group' &&
                chartSettings.isProductLabelsSwapped)
            ) {
              const prodSum = d3.sum(
                inputData.results[d.productIndex].data,
                el => el.value
              )
              text = `${((100 * d.value) / Math.max(prodSum, 1)).toFixed(
                decimals
              )}%`
            } else {
              const labelRes = inputData.results.map(prodRes =>
                prodRes.data.find(el => el.label === d.label)
              )
              text = `${(
                (100 * d.value) /
                Math.max(
                  d3.sum(labelRes, el => el.value),
                  1
                )
              ).toFixed(decimals)}%`
            }
          } else {
            text = `${((100 * d.value) / Math.max(totalSum, 1)).toFixed(
              decimals
            )}%`
          }

          const valuesFontSize = chartSettings.fontSize_values
            ? chartSettings.fontSize_values
            : 1
          const valuesFontFamily = chartSettings.fontFamily_values
          ? chartSettings.fontFamily_values
            : 'Lato'
          d3.select(this)
            .select('text')
            .text(text)
            .style('font-size', `${valuesFontSize}rem`)
            .style('font-family', valuesFontFamily)
            .style('opacity', 1)
            .transition()
            .duration(mounted.current ? 400 : 0)
            .attr('x', width + 4 * valuesFontSize)
            .attr('y', yStart + yInner.bandwidth() / 2 + 3 * valuesFontSize)
        } else {
          d3.select(this)
            .select('text')
            .style('opacity', 0)
        }
      })

    bars.exit().remove()

    chartsContainer
      .selectAll('.allTicks .tick text')
      .style('font-size', `${chartSettings.fontSize_axis || 1}rem`)
      .style('font-family', chartSettings.fontFamily_axis || 'Lato')
    chartsContainer
      .selectAll('.chart-legend .legend')
      .style('font-size', `${chartSettings.fontSize_legend || 1.1}rem`)
      .style('font-family', chartSettings.fontFamily_legend || 'Lato')
    chartsContainer
      .selectAll('.axisX .tick line')
      .style('display', chartSettings.isChartGridHidden ? 'none' : 'block')
  }

  const updateLabelsAbbreviation = () => {
    const chartsContainer = d3
      .select('#' + chartName)
      .select('.barchart-container')
    chartsContainer
      .selectAll('.yLabel')
      .style('font-size', `${chartSettings.fontSize_labels || 1}rem`)
      .style('font-family', chartSettings.fontFamily_labels || 'Lato')
      .each(function (d, i) {
        d3.select(this)
          .select('span')
          .remove()
        const txtSpan = d3.select(this).append('span')
        let txt = d
        const abbrText = '..'
        for (let i = 0; i <= 30; i++) {
          if (i === 1) {
            d3.select(this)
              .on('mouseover', () => tooltipLayer.showTooltip({ title: d }))
              .on('mousemove', tooltipLayer.updateTooltip)
              .on('mouseleave', tooltipLayer.clearTooltip)
          }
          const txtBB = txtSpan
            .text(txt)
            .node()
            .getBoundingClientRect()
          if (
            txt.length > 1 + abbrText.length &&
            (txtBB.height > yScale.step() - 10 ||
              txtBB.width > chartMargin.left - 5)
          ) {
            txt = txt.substring(0, (txt.length * 3) / 4 - 1) + abbrText
          } else {
            break
          }
        }
        const h = txtSpan.node().getBoundingClientRect().height
        d3.select(this).style(
          'top',
          d =>
            (yScale.step() - h) / 2 + yScale.step() * i + chartMargin.top + 'px'
        )
      })
  }

  return (
    <Container>
      <div id={chartName} />
    </Container>
  )
}

export default withPptDump('bar-chart')(BarChart)
