import React from 'react'
import { Table as TableCore } from 'antd'
import { Text, Divider, StatisticsTableContainer } from '../styles'
import formatAnalysisTableData from '../../../utils/formatAnalysisTableData'
import { useTranslation } from 'react-i18next'
import withPptDump from '../withPptDump'

const Table = withPptDump('table', ({ columns, dataSource }) => ({
  columns,
  dataSource
}))(TableCore)

const Anova = ({ data, showAll = false, chartSettings }) => {
  const pageSize = showAll ? (data && data.length) || 0 : undefined
  const { t } = useTranslation()

  const { columns, rows } = formatAnalysisTableData(data, chartSettings)

  return (
    <StatisticsTableContainer>
      <Divider />
      <Text>{t(`charts.analysis.chartTitle.anova`)}</Text>
      <Table
        dataSource={rows}
        columns={columns}
        useFixedHeader={showAll}
        pagination={{
          hideOnSinglePage: true,
          pageSize
        }}
        scroll={{ x: 880 }}
      />
    </StatisticsTableContainer>
  )
}

export default withPptDump('anova-chart', () => ({}))(Anova)
