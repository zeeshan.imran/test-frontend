import React from 'react'
import { ChartsRow } from './styles'

import ChartSwitch from './ChartSwitch'
import { ChartContextProvider } from '../../contexts/ChartContext'

const Charts = ({
  charts,
  chartsSettings,
  saveChartsSettings,
  resetSimilarSettings,
  hideFilter = false,
  isOwner,
  statsApi,
  t,
  surveyId
}) => {
  const fullCardWidth = '930px'
  const halfCardWidth = '445px'
  const cardMargin = '15px auto'

  return (
    <ChartContextProvider>
      <ChartsRow>
        {charts.map((el, idx) => {
          return (
            <ChartSwitch
              key={idx}
              el={el}
              idx={idx}
              fullCardWidth={fullCardWidth}
              halfCardWidth={halfCardWidth}
              cardMargin={cardMargin}
              chartsSettings={chartsSettings}
              saveChartsSettings={saveChartsSettings}
              resetSimilarSettings={resetSimilarSettings}
              hideFilter={hideFilter}
              isOwner={isOwner}
              statsApi={statsApi}
              t={t}
              surveyId={surveyId}
            />
          )
        })}
      </ChartsRow>
    </ChartContextProvider>
  )
}

export default Charts
