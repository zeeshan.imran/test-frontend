import styled from 'styled-components'
import { DefaultChart } from '../styles'

export const Container = styled(DefaultChart)``

export const ProductName = styled.div`
  text-align: center;
  font-weight: 500;
  font-size: 1.2em;
  margin: 10px 0 20px;
`

export const ProductSection = styled.div`
  margin-bottom: 32px;
`
