import React from 'react'
import { Container, ProductName, ProductSection } from './styles'
import DataTable from '../DataTable'
import {
  getTableData,
  groupResultsByLabels
} from '../../../containers/Charts/GetData'
import { getChartComponent } from '../../../utils/chartConfigs'
import { useChartContext } from '../../../contexts/ChartContext'
import withPptDump from '../withPptDump'
// Stacked or horizontal bar chart divided in product sections
const MultiChart = ({
  cardWidth,
  inputData,
  chartSettings,
  t,
  shouldPrintChart,
  shouldPrintTables
}) => {
  const ctx = useChartContext()
  const isBarChart = inputData.chart_type === 'bar' // Legacy: bar chart is currently uses more complex grouped version to respresent charts
  return (
    <Container>
      {inputData.results.map((productResult, i) => {
        const productInputData = {
          ...inputData,
          labels: inputData.labels,
          chart_id: inputData.chart_id + '_' + i,
          results: isBarChart ? productResult : productResult.attributes,
          avg: Array.isArray(inputData.avg) && inputData.avg[i]
        }
        const groupedData = groupResultsByLabels(
          productInputData,
          chartSettings.groupLabels,
          chartSettings.sortOrder
        )
        const ChartComponent = getChartComponent(groupedData, true)

        return (
          <ProductSection key={inputData.chart_id + '_' + i}>
            <ProductName className='product-name'>
              {isBarChart ? productResult.name : productResult.productName}
            </ProductName>
            {shouldPrintChart && (
              <ChartComponent
                {...{
                  cardWidth,
                  inputData: groupedData,
                  labels: groupedData.labels,
                  chartSettings,
                  t
                }}
              />
            )}
            {shouldPrintTables && chartSettings.isDataTableShown && (
              <DataTable
                showAll={ctx.showAll}
                {...getTableData(groupedData, chartSettings, t)}
              />
            )}
          </ProductSection>
        )
      })}
    </Container>
  )
}
export default withPptDump('multi-chart', () => ({}))(MultiChart)
