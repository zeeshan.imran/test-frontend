import styled from 'styled-components'
import { DefaultChart } from '../styles'
import { family } from '../../../utils/Fonts'

export const Container = styled(DefaultChart)`
  .labelName tspan {
    font-style: normal;
    font-weight: 700;
  }

  .labelName {
    font-size: 0.9em;
    font-style: italic;
  }

  .tooltips-layer .tooltip-container .tooltip-title {
    font-weight: 700;
    font-family: ${family.primaryBold};
  }
  .msgInner {
    top: 43.9%;
    position: absolute;
    text-align: center;
    width: 100%;
    bottom: 22px;
    margin-left: 4px;
  }
  .ant-card-body {
    padding: 0;
  }
  .pieChartCont {
    padding: 0px;
    position: relative;
  }
  .labels {
    font-family: ${family.primaryBold};
    font-size: 12px;
  }
`
