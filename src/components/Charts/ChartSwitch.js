import React, { useMemo } from 'react'
import { Col } from 'antd'
import DataGuard from './DataGuard'
import {
  CHART_SIZES,
  CARD_WIDTH,
  getChartSchema,
  getChartComponent
} from '../../utils/chartConfigs'

const ChartSwitch = ({
  el,
  idx,
  chartsSettings,
  saveChartsSettings,
  resetSimilarSettings,
  hideFilter,
  halfCardWidth,
  fullCardWidth,
  cardMargin,
  isOwner,
  statsApi,
  t,
  surveyId
}) => {
  const colSpan = CHART_SIZES[el.chart_type] || CHART_SIZES.DEFAULT
  const fullOrHalf = CARD_WIDTH[el.chart_type] || CARD_WIDTH.DEFAULT
  const cardWidth = { halfCardWidth, fullCardWidth }[`${fullOrHalf}CardWidth`]
  const chartProps = useMemo(
    () => ({
      t,
      chartType: el.chart_type,
      chartId: el.chart_id,
      questionId: el.question,
      inputData: el,
      cardWidth,
      cardMargin,
      validationSchema: getChartSchema(el),
      component: getChartComponent(el),
      saveChartsSettings,
      resetSimilarSettings,
      hideFilter,
      chartsSettings,
      isOwner,
      statsApi
    }),
    [
      t,
      el,
      chartsSettings,
      saveChartsSettings,
      resetSimilarSettings,
      hideFilter,
      cardWidth,
      cardMargin,
      isOwner,
      statsApi
    ]
  )
  switch (el.chart_type) {
    case '':
    case 'pie':
    case 'stacked-column-horizontal-bars':
    case 'stacked-column':
    case 'map':
    case 'column':
    case 'columns-mean':
    case 'polar':
    case 'line':
    case 'table':
    case 'spearmann-table':
    case 'pearson-table':
    case 'tags-list':
    case 'stacked-bar':
    case 'horizontal-bars-mean':
    case 'bar':
      return (
        <Col span={colSpan} key={el.title.replace(/[^a-z0-9]/gi, '') + idx}>
          <DataGuard {...chartProps} surveyId={surveyId} />
        </Col>
      )
    default:
      return (
        <Col span={12} key={el.title.replace(/[^a-z0-9]/gi, '') + idx}>
          {el.title}
        </Col>
      )
  }
}

export default ChartSwitch
