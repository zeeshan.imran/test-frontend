import React, { useState } from 'react'
import { Table as TableCore } from 'antd'
import { useTranslation } from 'react-i18next'
import formatSelectedTableData from '../../../utils/formatSelectedTableData'
import Select from '../../Select'
import withPptDump from '../withPptDump'

const Table = withPptDump('table', ({ columns, dataSource }) => ({
  columns,
  dataSource
}))(TableCore)

const parseTableData = (t, data, table) => {
  const selectedTableIndex =
    data &&
    data.tables &&
    data.tables.length &&
    data.tables.findIndex(t => t.name === table)

  return selectedTableIndex >= 0 &&
    data &&
    data.tables &&
    data.tables[selectedTableIndex]
    ? formatSelectedTableData(data.tables[selectedTableIndex], t)
    : { columns: [], rows: [] }
}

const SecondaryTables = ({ data, showAll = false }) => {
  const pageSize = showAll ? data.length || 0 : undefined
  const { t } = useTranslation()
  const [selectedTable, setSelectedTable] = useState(null)

  const {
    columns: selectedTableColumns,
    rows: selectedTableRows
  } = parseTableData(t, data, selectedTable)

  const isPDF = /^\/preview-operator-ppt\//.test(window.location.pathname)
  if (isPDF) {
    return data.tables.map(table => {
      const { columns, rows } = parseTableData(t, data, table.name)

      return (
        <Table
          dataSource={rows}
          columns={columns}
          useFixedHeader={showAll}
          pagination={{
            hideOnSinglePage: true,
            pageSize
          }}
          scroll={{ x: 880 }}
        />
      )
    })
  }

  return (
    <React.Fragment>
      <Select
        label={t('charts.analysis.parameters.tukeyTableSelect')}
        value={selectedTable}
        options={data.tables}
        size='small'
        onChange={value => setSelectedTable(value)}
        getOptionValue={table => table.name}
        getOptionName={table =>
          t(`charts.analysis.tables.${table.name}`) !==
          `charts.analysis.tables.${table.name}`
            ? t(`charts.analysis.tables.${table.name}`)
            : table.name
        }
      />
      {selectedTable && (
        <Table
          dataSource={selectedTableRows}
          columns={selectedTableColumns}
          useFixedHeader={showAll}
          pagination={{
            hideOnSinglePage: true,
            pageSize
          }}
          scroll={{ x: 880 }}
        />
      )}
    </React.Fragment>
  )
}

export default SecondaryTables
