import React from 'react'
import { Table as TableCore } from 'antd'
import { Text, Divider, StatisticsTableContainer, Spacer } from '../styles'
import formatAnalysisTableData from '../../../utils/formatAnalysisTableData'
import { useTranslation } from 'react-i18next'
import withPptDump from '../withPptDump'
import SecondaryTables from './SecondaryTables'
const Table = withPptDump('table', ({ columns, dataSource }) => ({
  columns,
  dataSource
}))(TableCore)
const Tukey = ({ data, showAll = false, chartSettings = {} }) => {
  const pageSize = showAll ? data.length || 0 : undefined
  const { t } = useTranslation()
  const { columns, rows } = data
    ? formatAnalysisTableData(data, chartSettings)
    : { columns: [], rows: [] }

  return (
    <StatisticsTableContainer>
      <Divider />
      <Text>{t(`charts.analysis.chartTitle.tukey`)}</Text>
      <Table
        dataSource={rows}
        columns={columns}
        useFixedHeader={showAll}
        pagination={{
          hideOnSinglePage: true,
          pageSize
        }}
        scroll={{ x: 880 }}
      />
      <Spacer />
      <SecondaryTables data={data} showAll={showAll} />
    </StatisticsTableContainer>
  )
}

export default withPptDump('tukey-chart', () => ({}))(Tukey)
