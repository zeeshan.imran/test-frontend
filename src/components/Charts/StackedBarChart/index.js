import React, { useState, useEffect, useRef, useMemo } from 'react'
import * as d3 from 'd3'
import { Container } from './styles'
import {
  appendChartLegend,
  updateChartLegendColorScheme,
  tooltipLayer,
  getStackedTooltip,
  sortResultByAnalyticalValue
} from '../../../utils/ChartsHelperFunctions'
import {
  getProducts,
  getAnalyticValuesExtent,
  getSelectedStackedData,
  getMaxSelectedValue
} from '../../../containers/Charts/GetData'
import { defaultChartSettings } from '../../../defaults/chartSettings'
import { NOT_PRODUCT } from '../../../utils/chartUtils'
import withPptDump from '../withPptDump'

// Stacked or horizontal bar chart
const StackedBarChart = ({
  cardWidth,
  inputData: inputDataMain,
  chartSettings,
  t
}) => {
  // Sorting labels by analytics
  const inputData = sortResultByAnalyticalValue(inputDataMain)

  const chartName = 'stackedbarchart_' + inputData.chart_id
  const isStacked = inputData.chart_type === 'stacked-bar'
  const isAvg = inputData.chart_type === 'horizontal-bars-mean'
  const [labels, setLabels] = useState(
    inputData.labels.map((el, i) => ({ name: el, index: i, isSelected: true }))
  )

  let products
  if (isStacked || isAvg) {
    if (Array.isArray(inputData.results)) {
      // In-loop chart
      products = getProducts(inputData).map((el, i) => ({
        name: el,
        index: i,
        isSelected: true
      }))
    } else {
      // non-loop chart
      products = [{ name: NOT_PRODUCT, index: 0, isSelected: true }]
    }
  } else {
    products = inputData.labels.map((el, i) => ({
      name: el,
      index: i,
      isSelected: true
    }))
  }

  const valuesExtent = useMemo(() => getAnalyticValuesExtent(inputData), [
    inputData
  ])
  const mounted = useRef()

  const barHeight = 40
  const stackedChartMargin = { top: 10, right: 110, bottom: 15, left: 100 }
  const stackedBarChartWidth = parseInt(cardWidth, 10) * 0.95 + 'px'
  const stackedChartDims = [
    parseInt(stackedBarChartWidth, 10) -
      stackedChartMargin.left -
      stackedChartMargin.right,
    barHeight * products.length
  ]

  const domain = isStacked
    ? [0, 100] // Domain for stacked chart (%)
    : isAvg
    ? [valuesExtent[0], valuesExtent[1]] // Domain for mean value
    : inputData.range
    ? [inputData.range[0], inputData.range[1]] // Domain for sliderQ / pairedQ
    : [0, Math.max(getMaxSelectedValue(inputData), 1)]

  const stackedChartScaleX = d3
    .scaleLinear()
    .rangeRound([0, stackedChartDims[0]])
    .domain(domain)
    .nice()
  const stackedChartScaleY = d3
    .scaleBand()
    .rangeRound([0, stackedChartDims[1]])
    .paddingInner(0.3)
    .paddingOuter(0.15)
    .domain(products.map(el => el.index))
  const colorsStackedChart = d3
    .scaleOrdinal()
    .range(chartSettings.colorsArr || defaultChartSettings.colorsArr)
    .domain(isAvg ? products.map(el => el.index) : labels.map(el => el.name))

  useEffect(() => {
    if (!mounted.current) {
      initCharts()
      mounted.current = true
    } else {
      updateCharts()
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [labels, chartSettings])

  useEffect(() => {
    if (mounted.current) {
      updateLabelsAbbreviation()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [chartSettings.fontSize_labels, chartSettings.fontFamily_labels])

  useEffect(() => {
    if (mounted.current) {
      updateCharts()
      updateChartLegendColorScheme(
        d3
          .select('#' + chartName)
          .select('.stacked-barchart-container .stacked-barchart'),
        colorsStackedChart.copy().domain(labels.map(el => el.index))
      )
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [chartSettings.colorsArr])

  useEffect(() => {
    if (mounted.current) {
      // Selected labels obj is invalid - reinit
      setLabels(
        inputData.labels.map((el, i) => ({
          name: el,
          index: i,
          isSelected: true
        }))
      )
      mounted.current = false
    }
  }, [inputData])

  useEffect(() => {
    if (mounted.current) {
      // Selected labels obj is invalid - reinit
      mounted.current = false
      initCharts()
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [chartSettings.sortOrder])

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const initCharts = () => {
    d3.select('.' + chartName).remove()

    const chartsContainer = d3
      .select('#' + chartName)
      .append('div')
      .attr('class', chartName)
      .append('div')
      .attr('class', 'stacked-barchart-container')

    const stackedSvg = chartsContainer
      .append('div')
      .attr('class', 'stacked-barchart')
      .style('width', stackedBarChartWidth)
      .append('svg')

    initStackedChart()
    updateCharts()
    updateLabelsAbbreviation()

    function initStackedChart () {
      const g = stackedSvg
        .attr(
          'width',
          stackedChartDims[0] +
            stackedChartMargin.left +
            stackedChartMargin.right
        )
        .attr(
          'height',
          stackedChartDims[1] +
            stackedChartMargin.top +
            stackedChartMargin.bottom
        )
        .append('g')
        .attr('class', 'stacked')
        .attr('transform', 'translate(' + stackedChartMargin.left + ',0)')

      const axisBottom = d3
        .axisBottom(stackedChartScaleX)
        .tickSizeOuter(0)
        .tickSizeInner(-stackedChartDims[1])

      if (
        inputData.chart_type === 'horizontal-bars-mean' &&
        inputData.question_type === 'vertical-rating'
      ) {
        // Use label's analytic values as ticks on x-axis
        axisBottom.tickValues(
          inputData.question_with_products
            ? inputData.results[0].data.map(el => +el.analytics_value)
            : inputData.results.data.map(el => +el.analytics_value)
        )
      } else {
        axisBottom.ticks(20)
      }

      g.append('g')
        .attr('class', 'axisX allTicks')
        .call(axisBottom)
        .each(function () {
          d3.select(this)
            .select('.domain')
            .remove()
        })
        .attr('transform', 'translate(0,' + stackedChartDims[1] + ')')

      g.append('g')
        .attr('class', 'axisY')
        .call(d3.axisLeft(stackedChartScaleY))
        .each(function () {
          d3.select(this)
            .selectAll('line')
            .attr(
              'transform',
              'translate(0, ' + stackedChartScaleY.step() / 2 + ')'
            )
          d3.select(this)
            .selectAll('text')
            .remove()
        })

      g.append('g').attr('class', 'stacked-barchart-g')

      const yLabelsData = isAvg
        ? inputData.avg
        : isStacked
        ? products
        : inputData.results.data

      chartsContainer
        .select('.stacked-barchart')
        .append('div')
        .attr('class', 'yLabels')
        .style('width', stackedChartMargin.left - 5 + 'px')
        .style('position', 'absolute')
        .style('top', 0)
        .selectAll('.yLabel')
        .data(yLabelsData)
        .enter()
        .append('div')
        .attr('class', 'yLabel')
        .style('position', 'absolute')
        .style('width', stackedChartMargin.left - 5 + 'px')

      const legendContainer = chartsContainer.select('.stacked-barchart')

      if (isStacked) {
        chartsContainer
          .select('.stacked-barchart')
          .append('div')
          .attr('class', 'yLabelsSum')
          .style('width', stackedChartMargin.right - 5 + 'px')
          .style('position', 'absolute')
          .style('top', 0)
          .style('right', 0)
          .selectAll('.yLabelSum')
          .data(yLabelsData)
          .enter()
          .append('div')
          .attr('class', 'yLabelSum')
          .style('position', 'absolute')
          .style('width', stackedChartMargin.right - 5 + 'px')
          .text(d => {
            const prodCount = inputData.question_with_products
              ? d3.sum(inputData.results[d.index].data.map(el => el.value))
              : inputData && inputData.results && inputData.results.data
              ? d3.sum(inputData.results.data.map(el => el.value))
              : 0
            return `(N=${prodCount})`
          })
          .style(
            'top',
            (d, i) => stackedChartScaleY.step() * i + 'px'
          )

        appendChartLegend(
          legendContainer,
          labels,
          colorsStackedChart.copy().domain(labels.map(el => el.index)),
          setLabels
        )
      }
    }
  }
  
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const updateCharts = () => {
    isStacked ? updateStackedChart() : updateBarChart()

    d3.select('#' + chartName)
      .select('.stacked-barchart-container')
      .selectAll('.axisX .tick line')
      .style('display', chartSettings.isChartGridHidden ? 'none' : 'block')
  }

  const updateStackedChart = () => {
    const decimals = !isNaN(chartSettings.howManyDecimals)
      ? chartSettings.howManyDecimals
      : 2
    const chartsContainer = d3
      .select('#' + chartName)
      .select('.stacked-barchart-container')

    const stackedData = getSelectedStackedData(inputData, products, labels)

    const stack = d3
      .stack()
      .keys(labels.map(el => el.name))
      .order(
        chartSettings.isStackedLabelsReversed
          ? d3.stackOrderReverse
          : d3.stackOrderNone
      )
      .offset(d3.stackOffsetNone)

    const seriesData = stack(stackedData)

    const uniColoredFragments = chartsContainer
      .select('.stacked-barchart-g')
      .selectAll('.outerBars')
      .data(seriesData)

    const bars = uniColoredFragments
      .enter()
      .append('g')
      .classed('outerBars', true)
      .merge(uniColoredFragments)
      .attr('fill', function (d) {
        return colorsStackedChart(d.key)
      })
      .selectAll('.innerBars')
      .data(
        function (d) {
          return d.map(el => ({ ...el, key: d.key }))
        },
        (d, i) => {
          return d.data._metadata.productIndex + '/' + d.key
        }
      )

    const barsToUpdate = bars
      .enter()
      .append('g')
      .classed('innerBars', true)
      .on('mousemove', tooltipLayer.updateTooltip)
      .on('mouseleave', tooltipLayer.clearTooltip)
      .each(function (d) {
        d3.select(this).append('rect')
        d3.select(this)
          .append('text')
          .attr('fill', '#fff')
          .attr('text-anchor', 'middle')
          .attr('font-weight', 'bold')
          .attr('font-size', '10px')
      })
      .merge(bars)

    barsToUpdate
      .on('mouseover', d => {
        if (isStacked) {
          tooltipLayer.showTooltip(
            getStackedTooltip({ inputData, d, colorsStackedChart, decimals, t })
          )
        }
      })
      .each(function (d) {
        const xStart = stackedChartScaleX((100 * d[0]) / d.data._metadata.summ)
        const width =
          stackedChartScaleX((100 * d[1]) / d.data._metadata.summ) -
            stackedChartScaleX((100 * d[0]) / d.data._metadata.summ) || 0
        const yStart = stackedChartScaleY(d.data._metadata.productIndex)
        d3.select(this)
          .select('rect')
          .transition()
          .duration(mounted.current ? 400 : 0)
          .attr('y', yStart)
          .attr('height', stackedChartScaleY.bandwidth())
          .attr('x', xStart)
          .attr('width', width)
          .attr(
            'stroke',
            chartSettings.hasOutline
              ? chartSettings.hasOutline_color[0] ||
                  defaultChartSettings.outlineColor
              : '#fff'
          )
          .attr(
            'stroke-width',
            chartSettings.hasOutline ? chartSettings.hasOutline_width || 1 : 0.5
          )

        if (chartSettings.isCountProLabelShown) {
          const valueText = d3.select(this).select('text')
          valueText.text(
            d =>
              ((100 * (d[1] - d[0])) / d.data._metadata.summ).toFixed(
                decimals
              ) + '%'
          )

          const valuesFontSize = chartSettings.fontSize_values
            ? chartSettings.fontSize_values
            : 1
          const valuesFontFamily = chartSettings.fontFamily_values
            ? chartSettings.fontFamily_values
            : 'Lato'
          const textBbox = valueText.node().getBoundingClientRect()
          valueText

            .style('font-size', `${valuesFontSize}rem`)
            .style('font-family', valuesFontFamily)
            .transition()
            .duration(mounted.current ? 400 : 0)
            .attr('x', xStart + width / 2)
            .attr(
              'y',
              yStart + stackedChartScaleY.bandwidth() / 2 + 3 * valuesFontSize
            )
            .style(
              'opacity',
              textBbox.width <= width &&
                textBbox.height <= stackedChartScaleY.bandwidth()
                ? 1
                : 0
            )
        } else {
          d3.select(this)
            .selectAll('text')
            .style('opacity', 0)
        }
      })

    chartsContainer
      .selectAll('.chart-legend .legend')
      .style('font-size', `${chartSettings.fontSize_legend || 1.1}rem`)
      .style('font-family', chartSettings.fontFamily_legend || 'Lato')

    chartsContainer
      .selectAll('.axisX .tick text')
      .style('font-size', `${chartSettings.fontSize_axis || 1}rem`)
      .style('font-family', chartSettings.fontFamily_axis || 'Lato')
  }

  const updateBarChart = () => {
    const decimals = !isNaN(chartSettings.howManyDecimals)
      ? chartSettings.howManyDecimals
      : 2

    const chartsContainer = d3
      .select('#' + chartName)
      .select('.stacked-barchart-container')

    const bars = chartsContainer
      .select('.stacked-barchart-g')
      .selectAll('.rect-cont')
      .data(isAvg ? inputData.avg : inputData.results.data)

    bars
      .enter()
      .append('g')
      .classed('rect-cont', true)
      .on('mousemove', tooltipLayer.updateTooltip)
      .on('mouseleave', tooltipLayer.clearTooltip)
      .each(function (d) {
        d3.select(this).append('rect')
        d3.select(this)
          .append('text')
          .classed('resultsText', true)
          .style('text-anchor', 'left')
      })
      .merge(bars)
      .on('mouseover', (d, i) => {
        tooltipLayer.showTooltip({
          title: !inputData.results.data ? d.label : null,
          titleColor: colorsStackedChart(i),
          results: [
            {
              label: isAvg ? t('charts.table.mean') : d.label,
              value: parseFloat(d.value).toFixed(decimals)
            }
          ]
        })
      })
      .each(function (d, i) {
        d3.select(this)
          .select('rect')
          .attr('y', stackedChartScaleY(i))
          .attr('height', stackedChartScaleY.bandwidth())
          .transition()
          .duration(400)
          .attr('x', 0)
          .attr('width', stackedChartScaleX(d.value))
          .attr('fill', colorsStackedChart(i))
          .attr(
            'stroke',
            (chartSettings.hasOutline_color &&
              chartSettings.hasOutline_color[0]) ||
              defaultChartSettings.outlineColor
          )
          .attr(
            'stroke-width',
            chartSettings.hasOutline ? chartSettings.hasOutline_width || 1 : 0
          )

        const valuesFontSize = chartSettings.fontSize_values
          ? chartSettings.fontSize_values
          : 1
        const valuesFontFamily = chartSettings.fontFamily_values
          ? chartSettings.fontFamily_values
          : 'Lato'
        d3.select(this)
          .select('text')
          .style('font-size', `${valuesFontSize}rem`)
          .style('font-family', valuesFontFamily)
          .text(d.value % 1 === 0 ? d.value : d.value.toFixed(decimals))
          .attr(
            'y',
            stackedChartScaleY(i) +
              stackedChartScaleY.bandwidth() / 2 +
              3 * valuesFontSize
          )
          .attr('x', stackedChartScaleX(d.value) + 5)
          .style('opacity', chartSettings.isCountProLabelShown ? 1 : 0)
      })

    chartsContainer
      .selectAll('.axisX .tick text')
      .style('font-size', `${chartSettings.fontSize_axis || 1}rem`)
      .style('font-family', chartSettings.fontFamily_axis || 'Lato')
  }

  const updateLabelsAbbreviation = () => {
    const chartsContainer = d3
      .select('#' + chartName)
      .select('.stacked-barchart-container')
    chartsContainer
      .selectAll('.yLabel')
      .style('font-size', `${chartSettings.fontSize_labels || 1}rem`)
      .style('font-family', chartSettings.fontFamily_labels || 'Lato')
      .each(function (d, i) {
        d3.select(this)
          .select('span')
          .remove()
        const txtSpan = d3.select(this).append('span')
        let txt
        if ((isStacked || isAvg) && !Array.isArray(inputData.results)) {
          txt = t('charts.axis.stackedAnswerName')
        } else {
          txt = isAvg || !isStacked ? d.label : d.name
        }
        const abbrText = '..'
        for (let i = 0; i <= 30; i++) {
          if (i === 1) {
            d3.select(this)
              .on('mouseover', () =>
                tooltipLayer.showTooltip({ title: d.name })
              )
              .on('mousemove', tooltipLayer.updateTooltip)
              .on('mouseleave', tooltipLayer.clearTooltip)
          }
          const txtBB = txtSpan
            .text(txt)
            .node()
            .getBoundingClientRect()
          if (
            txt.length > 1 + abbrText.length &&
            (txtBB.height > stackedChartScaleY.step() - 10 ||
              txtBB.width > stackedChartMargin.left - 5)
          ) {
            txt = txt.substring(0, (txt.length * 3) / 4 - 1) + abbrText
          } else {
            break
          }
        }
        const h = txtSpan.node().getBoundingClientRect().height
        d3.select(this).style(
          'top',
          d =>
            (stackedChartScaleY.step() - h) / 2 +
            stackedChartScaleY.step() * i +
            'px'
        )
      })
    chartsContainer
      .selectAll('.yLabelSum')
      .style('margin-top', `${3 + 9 * (2 - (chartSettings.fontSize_labels || 1))}px`)
      .style('font-size', `${chartSettings.fontSize_labels || 1}rem`)
      .style('font-family', chartSettings.fontFamily_labels || 'Lato')
      .style('display', chartSettings.isTotalRespondentsProProductShown ? 'block' : 'none')
   }

  return (
    <Container>
      <div id={chartName} />
    </Container>
  )
}

export default withPptDump('stacked-bar-chart')(StackedBarChart)
