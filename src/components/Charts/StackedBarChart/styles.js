import styled from 'styled-components'
import { DefaultChart } from '../styles'

export const Container = styled(DefaultChart)`
  .stacked-barchart {
    position: relative;
  }
  .stacked-barchart-g rect.faded-rect {
    opacity: 0;
  }
  .tick line {
    stroke: #e4e4e5;
  }
`
