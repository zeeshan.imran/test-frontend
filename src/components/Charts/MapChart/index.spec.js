import React from 'react'
import { mount } from 'enzyme'
import MapChart from '.'
import wait from '../../../utils/testUtils/waait'

describe('MapChart', () => {
  let testRender
  let chartIndex
  let inputData
  let container

  beforeEach(() => {
    container = global.document.createElement('div')
    global.document.body.appendChild(container)

    chartIndex = 1
    inputData = {
      chart_id: '121',
      chart_type: 'map',
      title: 'What state do you live in?',
      trial: '',
      results: {
        data: [
          {
            label: 'Alabama',
            value: 2
          },
          {
            label: 'Alaska',
            value: 2
          },
          {
            label: 'Arizona',
            value: 9
          }
        ]
      },
      labels: ['Alabama', 'Alaska', 'Arizona'],
      country_geo_data: {
        features: [
          {
            properties: {
              value: 2
            }
          },
          {
            properties: {
              value: 3
            }
          },
          {
            properties: {
              value: 5
            }
          },
          {
            properties: {
              value: 1
            }
          }
        ]
      }
    }
  })

  afterEach(() => {
    testRender.detach()
    testRender.unmount()
    global.document.body.removeChild(container)
    container = null
  })

  test('should render MapChart', async () => {
    testRender = mount(
      <MapChart chartIndex={chartIndex} inputData={inputData} />,
      { attachTo: container }
    )

    expect(testRender).toMatchSnapshot()

    expect(testRender.find('#geochart_121').html()).toMatchSnapshot()

    await wait(500)
    expect(testRender.find('#geochart_121').html()).toMatchSnapshot()
  })
})
