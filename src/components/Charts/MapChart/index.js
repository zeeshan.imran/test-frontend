import React, { useEffect, useRef, useMemo } from 'react'
import * as d3 from 'd3'
import { Container } from './styles'
import {
  tooltipLayer,
  hasCollision
} from '../../../utils/ChartsHelperFunctions'
import { mergeResultsWithGeojson } from '../../../containers/Charts/MapChart/GetData'
import { getTotalSum } from '../../../containers/Charts/GetData'
import { defaultChartSettings } from '../../../defaults/chartSettings'
import withPptDump from '../withPptDump'

const MapChart = ({ cardWidth, inputData, chartSettings = {}, t }) => {
  const chartName = 'geochart_' + inputData.chart_id
  const chartMargin = { top: 10, right: 20, bottom: 25, left: 26 }
  const mounted = useRef()

  const colorsArr =
    chartSettings.colorsArr_map || defaultChartSettings.mapChartColorsArr
  const totalSum = useMemo(() => getTotalSum(inputData), [inputData])

  useEffect(() => {
    mounted.current = true
    initChart()
    updateColors()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [chartSettings.valueType, chartSettings.howManyDecimals])

  useEffect(() => {
    if (mounted.current) {
      updateColors()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [chartSettings.colorsArr_map])

  useEffect(() => {
    if (mounted.current) {
      updateFonts()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    chartSettings.fontSize_labels,
    chartSettings.fontFamily_labels,
    chartSettings.isLabelsShown
  ])

  const getProjection = (width, height, geoJson) => {
    const [bottomLeft, topRight] = d3.geoBounds(geoJson)
    /* https://bl.ocks.org/mbostock/4282586 */
    const lambda = -(topRight[0] + bottomLeft[0]) / 2
    const center = [
      (topRight[0] + bottomLeft[0]) / 2 + lambda,
      (topRight[1] + bottomLeft[1]) / 2
    ]
    const projection =
      inputData.region === 'us'
        ? d3.geoAlbersUsa().translate([width / 2, height / 2])
        : d3
            .geoAlbers()
            .parallels([bottomLeft[1], topRight[1]])
            .translate([width / 2, height / 2])
            .rotate([lambda, 0, 0])
            .center(center)
    switch (inputData.region) {
      case 'br':
      case 'ca':
        projection.scale(600)
        break
      case 'au':
        projection.scale(700)
        break
      case 'ar':
        projection.scale(750)
        break
      case 'us':
        projection.scale(900)
        break
      case 'mx':
        projection.scale(1300)
        break
      case 'ph':
        projection.scale(1860)
        break
      case 'it':
        projection.scale(2280)
        break
      case 'fr':
      case 'fr-dep':
      case 'uk':
        projection.scale(2500)
        break
      case 'de':
        projection.scale(3000)
        break
      case 'pl':
        projection.scale(4500)
        break
      default:
        projection.scale(40)
    }
    return projection
  }

  const initChart = () => {
    const decimals = !isNaN(chartSettings.howManyDecimals)
      ? parseInt(chartSettings.howManyDecimals, 10)
      : 2
    d3.select('.' + chartName).remove()
    const width = parseInt(cardWidth, 10) - chartMargin.left - chartMargin.right
    const height = (width * 500) / 960

    const chartsContainer = d3
      .select('#' + chartName)
      .append('div')
      .attr('class', chartName)

    const svg = chartsContainer
      .append('svg')
      .attr('width', width)
      .attr('height', height)

    const projection = getProjection(width, height, inputData.country_geo_data)

    const path = d3.geoPath(projection)

    const geoData = mergeResultsWithGeojson(
      inputData.results.data,
      inputData.country_geo_data
    ).sort((a, b) => (a.properties.value >= b.properties.value ? -1 : 1))

    svg.append('g').attr('class', 'countryShapes')
    svg.append('g').attr('class', 'mapLabels')
    svg.append('g').attr('class', 'mouseListeners')

    svg
      .select('.countryShapes')
      .selectAll('.mapShape')
      .data(geoData)
      .enter()
      .append('path')
      .classed('mapShape', true)
      .attr('d', path)
      .style('stroke', '#eee')
      .style('stroke-width', '0.5')
      .filter(d => d.properties.name)
      .each(d => {
        const center = path.centroid(d)
        if (d.properties.label_fix) {
          center[0] += d.properties.label_fix.left
            ? d.properties.label_fix.left
            : 0
          center[0] -= d.properties.label_fix.right
            ? d.properties.label_fix.right
            : 0
          center[1] += d.properties.label_fix.top
            ? d.properties.label_fix.top
            : 0
          center[1] -= d.properties.label_fix.bottom
            ? d.properties.label_fix.bottom
            : 0
        }

        let bbox
        let overlap = false
        const textSvg = svg.select('.mapLabels').append('g')
        const nameField = d.properties.displayName ? 'displayName' : 'name'

        let textWords = d.properties[nameField].split(' ')
        let isDashSeparated = false
        if (textWords.length === 1) {
          textWords = d.properties[nameField].split('-')
          isDashSeparated = textWords.length > 1
        }
        const textLines =
          d.properties[nameField].length > 10
            ? [
                textWords
                  .slice(0, Math.floor(textWords.length / 2))
                  .join(isDashSeparated ? '-' : ' ') +
                  (isDashSeparated ? '-' : ''),
                textWords
                  .slice(Math.floor(textWords.length / 2))
                  .join(isDashSeparated ? '-' : ' ')
              ]
            : [textWords.join(' ')]
        textLines.forEach((textLine, i) => {
          textSvg
            .append('text')
            .datum(d)
            .text(textLine)
            .attr('text-anchor', 'middle')
            .attr('x', center[0])
            .attr(
              'y',
              textLines.length === 2 ? center[1] - 6 + 12 * i : center[1]
            )
        })
        textSvg.call(d => {
          bbox = d.node().getBoundingClientRect()
        })
        svg.selectAll('.mapLabels text').each(function (d2) {
          if (d.properties[nameField] === d2.properties[nameField]) return
          const bbox2 = d3
            .select(this)
            .node()
            .getBoundingClientRect()
          if (hasCollision(bbox, bbox2)) {
            overlap = true
            return true
          }
        })
        if (overlap) textSvg.remove()
      })

    svg
      .select('.mouseListeners')
      .selectAll('path')
      .data(geoData)
      .enter()
      .append('path')
      .attr('d', path)
      .style('opacity', 0)
      .filter(d => d.properties.name)
      .on('mouseover', d =>
        tooltipLayer.showTooltip({
          title: d.properties.name,
          results: [
            {
              label: t('charts.tooltip.total'),
              value:
                d.properties.value % 1 === 0
                  ? d.properties.value
                  : d.properties.value.toFixed(decimals)
            },
            {
              label: t('charts.tooltip.percentageOfTotal'),
              value:
                ((100 * d.properties.value) / Math.max(totalSum, 1)).toFixed(
                  decimals
                ) + '%'
            }
          ]
        })
      )
      .on('mousemove', tooltipLayer.updateTooltip)
      .on('mouseleave', tooltipLayer.clearTooltip)

    updateFonts()
  }

  const updateColors = () => {
    const values = inputData.results.data.map(el => el.value)
    const minVal = d3.min(values)
    const maxVal = Math.max(d3.max(values), 1)
    const scale = d3
      .scaleLinear()
      .domain(
        colorsArr.length > 1
          ? d3
              .range(colorsArr.length)
              .map(d => ((maxVal - minVal) * d) / (colorsArr.length - 1))
          : [minVal, maxVal]
      )
      .range(
        colorsArr.length === 1
          ? [colorsArr[0], d3.color(colorsArr[0]).brighter(0.5)]
          : colorsArr
      )

    d3.select('#' + chartName)
      .select('.' + chartName)
      .selectAll('.mapShape')
      .style('fill', d =>
        d.properties.name ? scale(d.properties.value) : '#eee'
      )
  }

  const updateFonts = () => {
    const chartsContainer = d3.select('#' + chartName).select('.' + chartName)

    chartsContainer
      .selectAll('.mapLabels text')
      .style('font-size', `${chartSettings.fontSize_labels || 1}rem`)
      .style('font-family', chartSettings.fontFamily_labels || 'Lato')
      .style('opacity', chartSettings.isLabelsShown ? 1 : 0)
  }

  return (
    <Container>
      <div id={chartName} />
    </Container>
  )
}

export default withPptDump("map", () => ({}))(MapChart)
