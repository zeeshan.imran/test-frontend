import React, { useMemo } from 'react'
import { Table as TableCore } from 'antd'
import { Container } from './styles'
import withPptDump from '../withPptDump'

const Table = withPptDump('table', ({ columns, dataSource }) => ({
  columns,
  dataSource
}))(TableCore)

const TableChart = ({
  inputData,
  chartSettings = {},
  showAllInTable = false
}) => {
  const { labels, results, chart_id } = inputData
  const chartName = 'tablechart_' + chart_id
  const decimals = !isNaN(chartSettings.howManyDecimals)
    ? parseInt(chartSettings.howManyDecimals, 10)
    : 2

  const columns = []
  labels.map(value => {
    columns.push({
      title: value,
      dataIndex: value.toLowerCase(),
      key: value.toLowerCase(),
      width: '50%'
    })
    return value
  })
  const dataSource = useMemo(() => {
    return results && results.length
      ? results.map((rowValues, index) => {
          let rowResult = {
            key: index
          }
          rowValues.forEach((value, i) => {
            rowResult[columns[i].dataIndex] =
              parseFloat(value) || parseFloat(value) === 0
                ? parseFloat(value).toFixed(decimals)
                : value
          })
          return rowResult
        })
      : []
  }, [chartSettings.howManyDecimals])
  const pageSize = showAllInTable
    ? (dataSource && dataSource.length) || 0
    : undefined
  return (
    <Container>
      <div id={chartName} className='tableChartCont' />
      <Table
        useFixedHeader={showAllInTable}
        pagination={{
          hideOnSinglePage: true,
          pageSize
        }}
        dataSource={dataSource}
        columns={columns}
      />
    </Container>
  )
}

export default withPptDump('table-chart', () => ({}))(TableChart)
