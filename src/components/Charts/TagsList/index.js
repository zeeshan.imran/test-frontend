import React from 'react'
import { Col, Row, Tooltip } from 'antd'
import { Container, StyledTag, StyledSpan, SimpleTag } from './styles'
import withPptDump from '../withPptDump'

const TagsList = ({ inputData }) => {
  const tags = inputData.results
  return (
    <Container>
      <Row gutter={[16, 16]}>
        {tags.map(tag => (
          <Col span={4} key={tag.value}>
            <Tooltip
              title={<span dangerouslySetInnerHTML={{ __html: tag.label }} />}
              placement='top'
            >
              {tag.label.length > 20 ? (
                <StyledTag>
                  <StyledSpan>{tag.label}</StyledSpan>
                </StyledTag>
              ) : (
                <SimpleTag>
                  <StyledSpan>{tag.label}</StyledSpan>
                </SimpleTag>
              )}
            </Tooltip>
          </Col>
        ))}
      </Row>
    </Container>
  )
}

export default withPptDump('tags-list')(TagsList)
