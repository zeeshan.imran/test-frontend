import React from 'react'
import { storiesOf } from '@storybook/react'
import PolarChart from './index'

const data_m3_to_15 = {
  title: 'some PolarChart',
  labels: ['gjq label 1', 'gjq label 2', 'gjq label 3', 'gjq label 4', 'gjq label 5', 'gjq label 6'],
  series: [
    {
      data: [-3, -2, -1, 0, 1, 2]
    },
    {
      data: [-2, -1, 0, 1, 2, 3]
    },
    {
      data: [-1, 0, 1, 2, 3, 4]
    },
    {
      data: [0, 1, 2, 3, 4, 5]
    },
    {
      data: [1, 2, 3, 4, 5, 6]
    },
    {
      data: [2, 3, 4, 5, 6, 7]
    },
    {
      data: [3, 4, 5, 6, 7, 8]
    },
    {
      data: [4, 5, 6, 7, 8, 9]
    },
    {
      data: [5, 6, 7, 8, 9, 10]
    },
    {
      data: [6, 7, 8, 9, 10, 11]
    },
    {
      data: [7, 8, 9, 10, 11, 12]
    },
    {
      data: [8, 9, 10, 11, 12, 13]
    },
    {
      data: [9, 10, 11, 12, 13, 14]
    },
    {
      data: [10, 11, 12, 13, 14, 15]
    }
  ]
}

const data_0_to_10 = {
  title: 'some PolarChart',
  labels: ['gjq label 1', 'gjq label 2', 'gjq label 3', 'gjq label 4', 'gjq label 5', 'gjq label 6'],
  series: [
    {
      data: [0, 1, 2, 3, 4, 5]
    },
    {
      data: [1, 2, 3, 4, 5, 6]
    },
    {
      data: [2, 3, 4, 5, 6, 7]
    },
    {
      data: [3, 4, 5, 6, 7, 8]
    },
    {
      data: [4, 5, 6, 7, 8, 9]
    },
    {
      data: [5, 6, 7, 8, 9, 10]
    }
  ]
}

const data_m10_to_0 = {
  title: 'some PolarChart',
  labels: ['gjq label 1', 'gjq label 2', 'gjq label 3', 'gjq label 4', 'gjq label 5', 'gjq label 6', 'gjq label 7'],
  series: [
    {
      data: [-10, -9, -8, -7, -6, -5, -4]
    },
    {
      data: [-9, -8, -7, -6, -5, -4, -3]
    },
    {
      data: [-8, -7, -6, -5, -4, -3, -2]
    },
    {
      data: [-7, -6, -5, -4, -3, -2, -1]
    },
    {
      data: [-6, -5, -4, -3, -2, -1, 0]
    },
    {
      data: [-10, -6, -2, 0, -9, -5, -7]
    }
  ]
}

storiesOf('PolarChart 1- 15', module)
  .add('Polar Chart -3 to 15', () => (
    <PolarChart chartIndex={1} inputData={data_m3_to_15} />
  ))
  .add('Polar Chart 1 to 10', () => (
    <PolarChart chartIndex={1} inputData={data_0_to_10} />
  ))
  .add('Polar Chart -10 to 0', () => (
    <PolarChart chartIndex={1} inputData={data_m10_to_0} />
  ))
