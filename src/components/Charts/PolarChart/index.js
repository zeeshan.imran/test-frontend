import React, { useEffect, useState, useRef } from 'react'
import { find, propEq } from 'ramda'
import * as d3 from 'd3'
import { initChart, updateChart } from './Chart'
import {
  appendChartLegend,
  updateChartLegendColorScheme
} from '../../../utils/ChartsHelperFunctions'
import { parsePolarData } from '../../../containers/Charts/PolarChart/GetData'
import { getProducts } from '../../../containers/Charts/GetData'
import { DefaultChart } from '../styles'
import { defaultChartSettings } from '../../../defaults/chartSettings'
import { NOT_PRODUCT } from '../../../utils/chartUtils'
import withPptDump from '../withPptDump'

const PolarChart = ({ inputData, chartSettings = {} }) => {
  const chartName = 'polarchart_' + inputData.chart_id

  const [products, setProducts] = useState(
    getProducts(inputData)
      ? getProducts(inputData).map((el, i) => ({
          name: el,
          index: i,
          isSelected: true
        }))
      : [{ name: NOT_PRODUCT, index: 0, isSelected: true }]
  )
  const [data] = useState(parsePolarData(inputData))
  const mounted = useRef()

  const colorsScale = d3
    .scaleOrdinal()
    .range(chartSettings.colorsArr || defaultChartSettings.colorsArr)
    .domain(
      inputData.series.data ? [NOT_PRODUCT] : products.map(el => el.index)
    )

  useEffect(() => {
    if (!mounted.current) {
      mounted.current = true
      initPolarChart()
    } else {
      updatePolarChart()
    }
  }, [
    products,
    chartSettings.howManyDecimals,
    chartSettings.fontSize_legend,
    chartSettings.fontFamily_legend,
    chartSettings.fontSize_labels,
    chartSettings.fontFamily_labels,
    chartSettings.fontSize_axis,
    chartSettings.fontFamily_axis
  ])

  useEffect(() => {
    if (mounted.current) {
      updateChartLegendColorScheme(
        d3.select('#' + chartName).select('.' + chartName),
        colorsScale.copy().domain(products.map(el => el.index))
      )
      updatePolarChart()
    }
  }, [chartSettings.colorsArr])

  const getSelectedData = () => {
    return data
      .map((el, i) => ({
        ...el,
        color: inputData.series.data
          ? colorsScale(NOT_PRODUCT)
          : colorsScale(i),
        i,
        productIndex: find(propEq('name', el['name']), products).index
      }))
      .filter(
        element => find(propEq('name', element['name']), products).isSelected
      )
  }

  const initPolarChart = () => {
    d3.select('.' + chartName).remove()

    const selectedData = getSelectedData()

    const chartsContainer = d3
      .select('#' + chartName)
      .append('div')
      .attr('class', chartName)

    initChart('.' + chartName, selectedData, inputData.range, chartSettings)

    if (!inputData.series.data && products.length > 1) {
      appendChartLegend(
        chartsContainer,
        products,
        colorsScale.copy().domain(products.map(el => el.index)),
        setProducts,
        true
      )
    }
  }

  const updatePolarChart = () => {
    const selectedData = getSelectedData()
    updateChart('.' + chartName, selectedData, chartSettings)
  }

  return (
    <DefaultChart>
      <div id={chartName} />
    </DefaultChart>
  )
}

export default withPptDump('polar', ({ inputData }) => ({ inputData }))(
  PolarChart
)
