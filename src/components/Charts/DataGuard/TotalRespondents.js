import React from 'react'
import { StyledTotalRespondents } from './styles'
import withPptDump from '../withPptDump'

const TotalRespondents = ({ children }) => {
  return <StyledTotalRespondents>{children}</StyledTotalRespondents>
}

export default withPptDump(
  'total-respondents',
  ({ children }) => (children.join ? children.join('') : children),
  false
)(TotalRespondents)
