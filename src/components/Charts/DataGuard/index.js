// this file need console.log, it will help us to print the root cause of errors
/* eslint-disable no-console */
import React, { useMemo } from 'react'
import { Card, Icon, Spin } from 'antd'
import { uniq } from 'ramda'
import { DefaultChart } from '../styles'
import Title from 'antd/lib/typography/Title'
import { ErrorWrapper, SpinContainer } from './styles'
import StatsOptions from '../../../containers/StatsOptions'
import DataTable from '../DataTable'
import {
  getTableData,
  swapProductsWithLabels,
  groupResultsByLabels
} from '../../../containers/Charts/GetData'
import { getAvailableChartSettings } from '../../../utils/chartSettings'
import { isMultiChart } from '../../../utils/chartConfigs'
import { useChartContext } from '../../../contexts/ChartContext'
import StatsAnalysisForm from '../../../containers/StatsAnalysisForm'
import StatisticSwitch from '../StatisticSwitch'
import { getSingleChartSettings } from '../../../utils/ChartSettingsUtils'
import TotalRespondents from './TotalRespondents'

const DataGuard = ({ component, t, ...props }) => {
  const ctx = useChartContext()
  const {
    cardWidth,
    cardMargin,
    bodyStyle,
    validationSchema,
    inputData,
    chartsSettings = {},
    saveChartsSettings,
    resetSimilarSettings,
    hideFilter = false,
    isOwner,
    statsApi,
    chartId,
    surveyId,
    questionId,
    printFormat = 'all'
  } = props

  if (inputData.loading) {
    return (
      <Card
        title={inputData.title}
        style={{ width: cardWidth, margin: cardMargin, padding: 0 }}
        bodyStyle={bodyStyle}
      >
        <SpinContainer>
          <Spin size='default' />
        </SpinContainer>
      </Card>
    )
  }

  const filteredSettings = useMemo(() => {
    const chartSettings = getSingleChartSettings(chartsSettings, {
      question: questionId,
      chart_type: inputData.chart_type
    })
    const availableSettings = getAvailableChartSettings({
      chartType: inputData.chart_type,
      questionType: inputData.question_type,
      hasProducts: inputData.question_with_products
    }).map(el => el.name)

    const toRet = Object.keys(chartSettings)
      .filter(key => availableSettings.includes(key))
      .reduce((obj, key) => {
        obj[key] = chartSettings[key]
        return obj
      }, {})

    return toRet
  }, [chartsSettings])

  const groupedData = useMemo(() => {
    return !isMultiChart(inputData)
      ? groupResultsByLabels(
          inputData,
          filteredSettings.groupLabels,
          filteredSettings.sortOrder
        )
      : inputData
  }, [filteredSettings.groupLabels, filteredSettings.sortOrder, inputData])

  const swappedInput = useMemo(() => {
    return Array.isArray(groupedData.results) && !groupedData.attributes // Special case for multiple stacked bars
      ? swapProductsWithLabels(groupedData)
      : groupedData
  }, [groupedData, filteredSettings.isProductLabelsSwapped])

  try {
    if (validationSchema)
      validationSchema.validateSync(groupedData, { abortEarly: false })

    const ChartComponent = component
    const shouldPrintChart = ['chart', 'all'].includes(printFormat)
    const shouldPrintTables = ['tables', 'all'].includes(printFormat)
    return (
      <Card
        title={groupedData.title}
        className={`svg-chart-${chartId}`}
        extra={
          !hideFilter && (
            <StatsOptions
              chartData={inputData}
              chartSettings={filteredSettings}
              saveChartsSettings={saveChartsSettings}
              resetSimilarSettings={resetSimilarSettings}
              chartId={`svg-chart-${chartId}`}
              isOwner={isOwner}
              isAdditionalMenuShown
            />
          )
        }
        style={{ width: cardWidth, margin: cardMargin, padding: 0 }}
        bodyStyle={bodyStyle}
      >
        {filteredSettings.isTotalRespondentsShown && (
          <TotalRespondents>
            {t('charts.totalRespondents')}: {inputData.totalRespondents}
          </TotalRespondents>
        )}
        {(shouldPrintChart || isMultiChart) && (
          <ChartComponent
            {...{
              ...props,
              chartSettings: filteredSettings,
              t,
              inputData: filteredSettings.isProductLabelsSwapped
                ? swappedInput
                : groupedData,
              showAllInTable: ctx.showAll,
              shouldPrintChart,
              shouldPrintTables
            }}
          />
        )}
        {shouldPrintTables &&
        filteredSettings.isDataTableShown &&
        !groupedData.attributes && ( // Special case for multiple stacked bars
          <DataTable
            showAll={
              ctx.showAll ||
              (!filteredSettings.isDataTableShown_isPaginationEnabled &&
                groupedData.chart_type !== 'map')
            }
            {...getTableData(groupedData, filteredSettings, t)}
          />
        )}
        {inputData.availableStatistics &&
          inputData.availableStatistics.length > 0 &&
          inputData.statistic && (
            <StatisticSwitch
              data={{
                ...inputData.statistic,
                chart_id: inputData.chart_id,
                questionId
              }}
              saveChartsSettings={saveChartsSettings}
              resetSimilarSettings={resetSimilarSettings}
              isOwner={isOwner}
              chartSettings={filteredSettings}
              statisticsSettings={chartsSettings}
              t={t}
            />
          )}
        {!ctx.printView && (
          <StatsAnalysisForm
            isOwner={isOwner}
            question={inputData.question}
            availableStatistics={inputData.availableStatistics}
            surveyId={surveyId}
            loading={inputData.statistic && inputData.statistic.loading}
            statsApi={statsApi}
          />
        )}
      </Card>
    )
  } catch (ex) {
    console.groupCollapsed('Chart data input is invalid')
    console.log({ groupedData, errors: (ex.errors && uniq(ex.errors)) || ex })
    console.groupEnd('Chart data input is invalid')

    return (
      <DefaultChart>
        <Card
          title={groupedData.title}
          style={{ width: cardWidth, margin: cardMargin }}
        >
          <ErrorWrapper>
            <Title level={3}>
              <Icon type='api' /> Sorry, we can not render this chart.
            </Title>
          </ErrorWrapper>
        </Card>
      </DefaultChart>
    )
  }
}

export default DataGuard
