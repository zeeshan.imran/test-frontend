import React, { useState, useEffect, useRef, useMemo } from 'react'
import * as d3 from 'd3'
import { findIndex, propEq } from 'ramda'
import { Container } from './styles'
import {
  appendChartLegend,
  updateChartLegendColorScheme,
  tooltipLayer,
  getStackedTooltip
} from '../../../utils/ChartsHelperFunctions'
import {
  getProducts,
  getAnalyticValuesExtent,
  getSelectedStackedData
} from '../../../containers/Charts/GetData'
import { defaultChartSettings } from '../../../defaults/chartSettings'
import { NOT_PRODUCT } from '../../../utils/chartUtils'
import withPptDump from '../withPptDump'

const StackedColumnChart = ({ cardWidth, inputData, chartSettings, t }) => {
  const chartName = 'stackedchart_' + inputData.chart_id
  const hasAvgChart = !!inputData.chart_avg

  const products = getProducts(inputData)
    ? getProducts(inputData).map((el, i) => ({
        name: el,
        index: i,
        isSelected: true
      }))
    : [{ name: NOT_PRODUCT, index: 0, isSelected: true }]
  const [labels, setLabels] = useState(
    inputData.labels.map((el, i) => ({ name: el, index: i, isSelected: true }))
  )
  const valuesExtent = useMemo(() => getAnalyticValuesExtent(inputData), [
    inputData
  ])
  const mounted = useRef()
  const marginOuter = d3
    .scaleLinear()
    .range([26, hasAvgChart ? 260 : 400])
    .domain([1, 0.2])(chartSettings.chartWidth ? chartSettings.chartWidth : 1)

  const stackedChartMargin = {
    top: 20,
    right: marginOuter,
    bottom: 25,
    left: marginOuter
  }
  const stackedBarChartWidth = hasAvgChart
    ? parseInt(cardWidth, 10) * 0.67 + 'px'
    : parseInt(cardWidth, 10) * 0.95 + 'px'
  const stackedBarChartHeight = hasAvgChart
    ? parseInt(cardWidth, 10) * 0.25 + 'px'
    : parseInt(cardWidth, 10) * 0.3 + 'px'
  const stackedChartDims = [
    products.length *
      Math.round(
        (parseInt(stackedBarChartWidth, 10) -
          stackedChartMargin.left -
          stackedChartMargin.right) /
          products.length
      ),
    parseInt(stackedBarChartHeight, 10) -
      stackedChartMargin.top -
      stackedChartMargin.bottom
  ]
  const stackedChartScaleX = d3
    .scaleBand()
    .rangeRound([0, stackedChartDims[0]])
    .paddingInner(0.4)
    .paddingOuter(0.2)
    .domain(products.filter(el => el.isSelected).map(el => el.index))
  const stackedChartScaleY = d3
    .scaleLinear()
    .rangeRound([stackedChartDims[1], 0])
    .domain([0, 100])
    .nice()

  const avgChartMargin = { top: 12, right: 30, bottom: 25, left: 100 }
  const avgChartBarHeight = 30
  const avgChartWidth =
    parseInt(cardWidth, 10) -
    parseInt(stackedBarChartWidth, 10) -
    avgChartMargin.left -
    avgChartMargin.right -
    40
  const avgChartHeight = hasAvgChart ? avgChartBarHeight * products.length : 0
  const avgChartDims = [avgChartWidth - 5, avgChartHeight]
  const avgChartScaleX = d3
    .scaleLinear()
    .rangeRound([0, avgChartDims[0]])
    .domain(valuesExtent)
    .nice()

  const colorsStackedChart = d3
    .scaleOrdinal()
    .range(chartSettings.colorsArr || defaultChartSettings.colorsArr)
    .domain(labels.map(el => el.name))

  useEffect(() => {
    if (!mounted.current) {
      initCharts()
      mounted.current = true
    } else {
      updateCharts()
    }
  }, [products, labels, chartSettings])

  useEffect(() => {
    if (mounted.current) {
      // Selected labels obj is invalid - reinit
      setLabels(
        inputData.labels.map((el, i) => ({
          name: el,
          index: i,
          isSelected: true
        }))
      )
      mounted.current = false
    }
  }, [inputData])

  useEffect(() => {
    if (mounted.current) {
      updateChartLegendColorScheme(
        d3
          .select('#' + chartName)
          .select('.stacked-barchart-container .stacked-barchart'),
        colorsStackedChart.copy().domain(labels.map(el => el.index))
      )
    }
  }, [chartSettings.colorsArr])

  const initCharts = () => {
    d3.select('.' + chartName).remove()

    const chartsContainer = d3
      .select('#' + chartName)
      .append('div')
      .attr('class', chartName)
      .append('div')
      .attr('class', 'stacked-barchart-container')

    const stackedSvg = chartsContainer
      .append('div')
      .attr('class', 'stacked-barchart')
      .style('width', stackedBarChartWidth)
      .append('svg')

    initStackedChart()
    if (hasAvgChart) initHorizontalChart()

    updateCharts()

    function initStackedChart () {
      const g = stackedSvg
        .attr(
          'width',
          stackedChartDims[0] +
            stackedChartMargin.left +
            stackedChartMargin.right
        )
        .attr(
          'height',
          stackedChartDims[1] +
            stackedChartMargin.top +
            stackedChartMargin.bottom
        )
        .append('g')
        .attr('class', 'stacked')
        .attr(
          'transform',
          'translate(' +
            stackedChartMargin.left +
            ',' +
            stackedChartMargin.top +
            ')'
        )

      g.append('g')
        .attr('class', 'axisY')
        .call(
          d3
            .axisLeft(stackedChartScaleY)
            .ticks(5)
            .tickSizeOuter(0)
            .tickSizeInner(-stackedChartDims[0])
        )
        .each(function () {
          d3.select(this)
            .select('.domain')
            .remove()
        })

      g.append('g')
        .attr('class', 'axisX')
        .attr('transform', 'translate(0,' + stackedChartDims[1] + ')')

      g.append('g').attr('class', 'stacked-barchart-g')

      chartsContainer
        .select('.stacked-barchart')
        .append('div')
        .attr('class', 'xLabels')
        .style('position', 'relative')
        .style('left', stackedChartMargin.left + 'px')
        .style('top', -stackedChartMargin.bottom + 'px')

      if (
        inputData.results.length === inputData.avg.length ||
        (!Array.isArray(inputData.results) && inputData.avg.length === 1)
      ) {
        chartsContainer
          .select('.stacked-barchart-g')
          .append('g')
          .classed('stacked-barchart-means', true)
          .selectAll('text')
          .data(inputData.avg)
          .enter()
          .append('text')
          .style('text-anchor', 'middle')
          .attr('y', -10)
      }

      const legendContainer = chartsContainer.select('.stacked-barchart')

      appendChartLegend(
        legendContainer,
        labels,
        colorsStackedChart.copy().domain(labels.map(el => el.index)),
        setLabels,
        !hasAvgChart
      )

      // Products legend
      // if (!inputData.results.data && products.length > 1) {
      //   appendChartLegend(
      //     legendContainer,
      //     products,
      //     colorsStackedChart,
      //     setProducts,
      //     true
      //   )
      // }
    }

    function initHorizontalChart () {
      const horizontalSvg = chartsContainer
        .append('div')
        .attr('class', 'horizontal-barchart')
        .append('svg')
        .attr('class', 'horizontal-barchart-svg')
      const g = horizontalSvg
        .attr(
          'width',
          avgChartWidth + avgChartMargin.left + avgChartMargin.right
        )
        .attr(
          'height',
          avgChartHeight + avgChartMargin.top + avgChartMargin.bottom
        )
        .append('g')
        .attr('class', 'horizontal-barchart-g')
        .attr(
          'transform',
          'translate(' + avgChartMargin.left + ',' + avgChartMargin.top + ')'
        )

      g.append('g').attr('class', 'axisX allTicks')

      g.append('g').attr('class', 'axisY')

      chartsContainer
        .select('.horizontal-barchart')
        .append('div')
        .attr('class', 'yLabels')
        .style('width', avgChartMargin.left - 5 + 'px')
        .style('position', 'absolute')
        .style('top', avgChartMargin.top + 'px')
    }
  }

  const updateCharts = () => {
    updateStackedChart()
    hasAvgChart && updateHorizontalChart()
    if (hasAvgChart) {
      const chartsContainer = d3
        .select('#' + chartName)
        .select('.stacked-barchart-container')

      const stackedChartH = chartsContainer
        .select('.stacked-barchart')
        .node()
        .getBoundingClientRect().height
      const avgChartWidth = chartsContainer
        .select('.horizontal-barchart')
        .node()
        .getBoundingClientRect().height
      chartsContainer.style(
        'min-height',
        `${Math.max(stackedChartH, avgChartWidth)}px`
      )
    }
  }

  const updateStackedChart = () => {
    const decimals = !isNaN(chartSettings.howManyDecimals)
      ? chartSettings.howManyDecimals
      : 2

    const maximalStackedWidth = 100

    const chartsContainer = d3
      .select('#' + chartName)
      .select('.stacked-barchart-container')

    const stackedData = getSelectedStackedData(inputData, products, labels)

    const columnWidth = Math.min(
      stackedChartScaleX.bandwidth(),
      maximalStackedWidth
    )

    stackedChartScaleX.domain(
      products.filter(el => el.isSelected).map(el => el.index)
    )

    chartsContainer
      .select('.axisX')
      .call(d3.axisBottom(stackedChartScaleX))
      .each(function () {
        d3.select(this)
          .selectAll('line')
          .attr(
            'transform',
            'translate(' + stackedChartScaleX.step() / 2 + ', 0)'
          )

        d3.select(this)
          .selectAll('text')
          .remove()
      })

    const stack = d3
      .stack()
      .keys(labels.map(el => el.name))
      .order(
        chartSettings.isStackedLabelsReversed
          ? d3.stackOrderReverse
          : d3.stackOrderNone
      )
      .offset(d3.stackOffsetNone)

    const seriesData = stack(stackedData)

    const uniColoredFragments = chartsContainer
      .select('.stacked-barchart-g')
      .selectAll('.outerBars')
      .data(seriesData)

    const bars = uniColoredFragments
      .enter()
      .append('g')
      .classed('outerBars', true)
      .merge(uniColoredFragments)
      .attr('fill', function (d) {
        return colorsStackedChart(d.key)
      })
      .selectAll('.innerBars')
      .data(
        function (d) {
          return d.map(el => ({ ...el, key: d.key }))
        },
        (d, i) => {
          return d.data._metadata.productIndex + '/' + d.key
        }
      )

    const barsToUpdate = bars
      .enter()
      .append('g')
      .classed('innerBars', true)
      .attr('data-id', (d, i) => d.data._metadata.productIndex + '/' + i)
      .on('mousemove', tooltipLayer.updateTooltip)
      .on('mouseleave', tooltipLayer.clearTooltip)
      .each(function (d) {
        d3.select(this).append('rect')
        d3.select(this)
          .append('text')
          .attr('fill', '#fff')
          .attr('text-anchor', 'middle')
          .attr('font-weight', 'bold')
          .attr('font-size', '10px')
      })
      .merge(bars)

    barsToUpdate
      .on('mouseover', d => {
        tooltipLayer.showTooltip(
          getStackedTooltip({ inputData, d, colorsStackedChart, decimals, t })
        )
      })
      .each(function (d) {
        const productIndex = findIndex(
          propEq('name', d.data._metadata.name),
          products
        )
        const xStart =
          stackedChartScaleX.bandwidth() > maximalStackedWidth
            ? stackedChartScaleX(productIndex) +
              (stackedChartScaleX.bandwidth() - maximalStackedWidth) / 2
            : stackedChartScaleX(productIndex)
        const sectionHeight = dataPoint =>
          (100 * d[dataPoint]) / d.data._metadata.summ
        const yStart = stackedChartScaleY(sectionHeight(1)) || 0
        const height =
          stackedChartScaleY(sectionHeight(0)) -
            stackedChartScaleY(sectionHeight(1)) || 0
        d3.select(this)
          .select('rect')
          .transition()
          .duration(mounted.current ? 400 : 0)
          .attr('x', xStart)
          .attr('width', columnWidth)
          .attr('y', yStart)
          .attr('height', height)
          .attr(
            'stroke',
            chartSettings.hasOutline
              ? chartSettings.hasOutline_color[0] ||
                  defaultChartSettings.outlineColor
              : '#fff'
          )
          .attr(
            'stroke-width',
            chartSettings.hasOutline ? chartSettings.hasOutline_width || 1 : 0.5
          )
        if (chartSettings.isCountProLabelShown) {
          const valueText = d3.select(this).select('text')
          valueText.text(
            ((100 * (d[1] - d[0])) / d.data._metadata.summ).toFixed(decimals) +
              '%'
          )
          const textBbox = valueText.node().getBoundingClientRect()
          valueText
            .style('font-size', `${chartSettings.fontSize_values || 1}rem`)
            .style('font-family', chartSettings.fontFamily_values || 'Lato')
            .transition()
            .duration(mounted.current ? 400 : 0)
            .attr('x', xStart + columnWidth / 2)
            .attr(
              'y',
              yStart + height / 2 + 4 * (chartSettings.fontSize_values || 1)
            )
            .style(
              'opacity',
              textBbox.width + 2 <= columnWidth && textBbox.height + 2 <= height
                ? 1
                : 0
            )
        } else {
          d3.select(this)
            .selectAll('text')
            .style('opacity', 0)
        }

        if (chartSettings.isMeanProProductShown) {
          const meanDecimals = !isNaN(chartSettings.howManyDecimals)
            ? chartSettings.isMeanProProductShown_howManyDecimals
            : 2
          chartsContainer
            .select('.stacked-barchart-means')
            .selectAll('text')
            .style('opacity', 1)
            .text(
              d =>
                `${t('charts.analysis.options.mean')}: ${d.value.toFixed(
                  meanDecimals
                )}`
            )
            .attr('x', d => {
              const productIndex = Array.isArray(inputData.results)
                ? findIndex(propEq('name', d.label), products)
                : 0
              const xStart =
                stackedChartScaleX.bandwidth() > maximalStackedWidth
                  ? stackedChartScaleX(productIndex) +
                    (stackedChartScaleX.bandwidth() - maximalStackedWidth) / 2
                  : stackedChartScaleX(productIndex)
              return xStart + columnWidth / 2
            })
            .style('font-size', `${chartSettings.fontSize_values || 1}rem`)
            .style('font-family', chartSettings.fontFamily_values || 'Lato')
        } else {
          chartsContainer
            .selectAll('.stacked-barchart-means text')
            .style('opacity', 0)
        }
      })

    bars.exit().remove()

    chartsContainer
      .select('.stacked-barchart .xLabels')
      .selectAll('.xLabel')
      .remove()

    chartsContainer
      .select('.stacked-barchart .xLabels')
      .selectAll('.xLabel')
      .data(products.filter(el => el.isSelected), d => d.index)
      .enter()
      .append('div')
      .attr('class', 'xLabel')
      .style('position', 'absolute')
      .style('left', (d, i) => stackedChartScaleX.step() * i + 'px')
      .style('width', stackedChartScaleX.step() + 'px')
      .style('transform', function (d) {
        return (
          'translate(' +
          Math.floor(
            (stackedChartScaleX.step() -
              parseInt(d3.select(this).style('width'), 10)) /
              2
          ) +
          'px, 0)'
        )
      })
      .each(function (d) {
        const txtSpan = d3.select(this).append('span')
        let txt = Array.isArray(inputData.results)
          ? d.name
          : t('charts.axis.stackedAnswerName')
        const abbrText = '..'
        for (let i = 0; i <= 10; i++) {
          if (i === 1) {
            d3.select(this)
              .on('mouseover', () =>
                tooltipLayer.showTooltip({ title: d.name })
              )
              .on('mousemove', tooltipLayer.updateTooltip)
              .on('mouseleave', tooltipLayer.clearTooltip)
          }
          const txtW = txtSpan
            .text(txt)
            .node()
            .getBoundingClientRect().width
          if (
            txt.length > 1 + abbrText.length &&
            txtW > stackedChartScaleX.step() - 10
          ) {
            txt = txt.substring(0, txt.length / 2 - 1) + abbrText
          } else {
            break
          }
        }
        if (chartSettings.isTotalRespondentsProProductShown) {
          const respondentsSpan = d3.select(this).append('span')
          const prodCount = inputData.question_with_products
            ? d3.sum(inputData.results[d.index].data.map(el => el.value))
            : inputData && inputData.results && inputData.results.data
            ? d3.sum(inputData.results.data.map(el => el.value))
            : 0
          respondentsSpan.text(`(N=${prodCount})`).style('display', 'block')
        }
      })

    // Add margin to xLabels
    let maxH = 0
    chartsContainer
      .select('.stacked-barchart')
      .selectAll('.xLabel')
      .each(function (d) {
        const h = d3
          .select(this)
          .node()
          .getBoundingClientRect().height
        maxH = Math.max(h, maxH)
      })
    chartsContainer
      .select('.stacked-barchart .xLabels')
      .style('height', maxH - stackedChartMargin.bottom + 'px')
    chartsContainer
      .selectAll('.xLabel')
      .style('font-size', `${chartSettings.fontSize_labels || 1}rem`)
      .style('font-family', chartSettings.fontFamily_labels || 'Lato')

    chartsContainer
      .selectAll('.chart-legend .legend')
      .style('font-size', `${chartSettings.fontSize_legend || 1.1}rem`)
      .style('font-family', chartSettings.fontFamily_legend || 'Lato')

    chartsContainer
      .selectAll('.axisY .tick text')
      .style('font-size', `${chartSettings.fontSize_axis || 1}rem`)
      .style('font-family', chartSettings.fontFamily_axis || 'Lato')

    chartsContainer
      .selectAll('.axisY .tick line')
      .style('display', chartSettings.isChartGridHidden ? 'none' : 'block')
  }

  const updateHorizontalChart = () => {
    const decimals = !isNaN(chartSettings.howManyDecimals)
      ? chartSettings.howManyDecimals
      : 2

    const chartsContainer = d3
      .select('#' + chartName)
      .select('.horizontal-barchart')

    const filteredData = inputData.avg
      .map((el, i) => ({ ...el, index: products[i].index }))
      .filter((_el, i) => products[i].isSelected)

    const avgChartHeight = avgChartBarHeight * filteredData.length

    chartsContainer
      .select('.axisX')
      .attr('transform', 'translate(0,' + avgChartHeight + ')')
      .call(
        d3
          .axisBottom(avgChartScaleX)
          .ticks(5)
          .tickSizeOuter(0)
      )
      .each(function () {
        d3.select(this)
          .selectAll('line')
          .attr('y2', -avgChartHeight)
        d3.select(this)
          .select('.domain')
          .remove()
      })

    const y = d3
      .scaleBand()
      .rangeRound([0, avgChartHeight])
      .paddingInner(0.5)
      .paddingOuter(0.25)
      .domain(products.filter(el => el.isSelected).map(el => el.index))

    chartsContainer
      .select('.axisY')
      .call(d3.axisLeft(y).tickSize(4))
      .each(function () {
        d3.select(this)
          .selectAll('line')
          .attr('transform', 'translate(0, ' + y.step() / 2 + ')')

        d3.select(this)
          .selectAll('text')
          .remove()
      })

    const bars = chartsContainer
      .select('.horizontal-barchart-g')
      .selectAll('.barCont')
      .data(filteredData, d => d.index)

    const barColor =
      chartSettings.singleColor && chartSettings.singleColor[0]
        ? chartSettings.singleColor[0]
        : defaultChartSettings.colorsArr[0]

    bars
      .enter()
      .append('g')
      .classed('barCont', true)
      .on('mouseover', d =>
        tooltipLayer.showTooltip({
          title: d.label,
          results: [
            {
              label: t('charts.table.mean'),
              value: parseFloat(d.value).toFixed(decimals)
            }
          ]
        })
      )
      .on('mousemove', tooltipLayer.updateTooltip)
      .on('mouseleave', tooltipLayer.clearTooltip)
      .each(function (d) {
        d3.select(this)
          .append('rect')
          .attr('width', avgChartScaleX(d.value))
          .attr('x', 0)
        d3.select(this)
          .append('text')
          .classed('resultsText', true)
          .style('text-anchor', 'left')
      })
      .merge(bars)
      .each(function (d) {
        d3.select(this)
          .select('rect')
          .attr('fill', barColor)
          .attr('y', y(d.index))
          .attr('height', y.bandwidth())
          .attr(
            'stroke',
            chartSettings.hasOutline
              ? chartSettings.hasOutline_color[0] ||
                  defaultChartSettings.outlineColor
              : '#fff'
          )
          .attr(
            'stroke-width',
            chartSettings.hasOutline ? chartSettings.hasOutline_width || 1 : 0.5
          )
        const valuesFontSize = chartSettings.fontSize_values
          ? chartSettings.fontSize_values
          : 1
        const valuesFontFamily = chartSettings.fontFamily_values
          ? chartSettings.fontFamily_values
          : 'Lato'
        d3.select(this)
          .select('text')
          .style('font-size', `${valuesFontSize}rem`)
          .style('font-family', valuesFontFamily)
          .text(d.value.toFixed && d.value.toFixed(decimals))
          .attr('y', y(d.index) + y.bandwidth() / 2 + 4 * valuesFontSize)
          .attr('x', avgChartScaleX(d.value) + 3)
          .style('opacity', chartSettings.isMeanProLabelsShown ? 1 : 0)
      })

    bars.exit().remove()

    chartsContainer
      .select('.horizontal-barchart .yLabels')
      .selectAll('.yLabel')
      .remove()

    const labelsSelection = chartsContainer
      .select('.horizontal-barchart .yLabels')
      .selectAll('.yLabel')
      .data(inputData.avg)

    labelsSelection
      .enter()
      .append('div')
      .attr('class', 'yLabel')
      .style('position', 'absolute')
      .style('width', avgChartMargin.left - 5 + 'px')
      .style('top', 0)
      .merge(labelsSelection)
      .style('font-size', `${chartSettings.fontSize_labels || 1}rem`)
      .style('font-family', chartSettings.fontFamily_labels || 'Lato')
      .each(function (d, i) {
        const txtSpan = d3.select(this).append('span')
        let txt = d.label
        const abbrText = '..'
        for (let i = 0; i <= 16; i++) {
          if (i === 1) {
            d3.select(this)
              .on('mouseover', () =>
                tooltipLayer.showTooltip({ title: d.name })
              )
              .on('mousemove', tooltipLayer.updateTooltip)
              .on('mouseleave', tooltipLayer.clearTooltip)
          }
          const txtBB = txtSpan
            .text(txt)
            .node()
            .getBoundingClientRect()
          if (
            txt.length > 1 + abbrText.length &&
            (txtBB.height > y.step() - 10 ||
              txtBB.width > avgChartMargin.left - 5)
          ) {
            txt = txt.substring(0, (txt.length * 3) / 4 - 1) + abbrText
          } else {
            break
          }
        }
        const h = txtSpan.node().getBoundingClientRect().height
        d3.select(this).style(
          'top',
          d => (y.step() - h) / 2 + y.step() * i + 'px'
        )
      })

    chartsContainer
      .selectAll('.axisX .tick text')
      .style('font-size', `${chartSettings.fontSize_axis || 1}rem`)
      .style('font-family', chartSettings.fontFamily_axis || 'Lato')
    chartsContainer
      .selectAll('.axisX .tick line')
      .style('display', chartSettings.isChartGridHidden ? 'none' : 'block')
  }

  return (
    <Container>
      <div id={chartName} />
    </Container>
  )
}

export default withPptDump(
  'stacked-column-chart',
  ({ inputData, chartSettings }) => ({
    ...inputData,
    howManyDecimals: (chartSettings && chartSettings.howManyDecimals) || 0
  })
)(StackedColumnChart)
