import React from 'react'
import { mount } from 'enzyme'
import { act } from 'react-dom/test-utils'
import StackedBarChart from './index'
import wait from '../../../utils/testUtils/waait'

export const inputData = {
  chart_id: '121',
  chart_type: 'stacked-bar',
  title: 'Willingness to purchase (% of Total)',
  trial: '',
  labels: ['Yes', 'No', 'Unknown'],
  chart_avg: true,
  avg: [
    {
      label: 'Activia Regular Yogurt (Strawberry)',
      value: 2.51
    },
    {
      label: 'Dannon Whole Milk Yogurt (Strawberry)',
      value: 2.45
    },
    {
      label: 'Yoplait Original Yogurt (Strawberry)',
      value: 2.96
    },
    {
      label: 'Great Value (Walmart) Original Yogurt (Strawberry)',
      value: 2.78
    },
    {
      label: 'Liberte Whole Milk Yogurt (Strawberry)',
      value: 2.16
    },
    {
      label: 'Oui French Style Yogurt (Strawberry)',
      value: 2.71
    },
    {
      label: 'Yoplait Fruitside Yogurt (Strawberry)',
      value: 4.45
    },
    {
      label: 'Trader Joes Regular Yogurt (Strawberry)',
      value: 2.04
    }
  ],
  avg_label: 'Mean',
  tableData: {
    columns: ['A', 'B'],
    rows: [
      ['x', '-'],
      ['x', '-'],
      ['x', '-'],
      ['-', 'x'],
      ['x', '-'],
      ['x', '-'],
      ['x', '-'],
      ['x', '-']
    ]
  },
  results: [
    {
      data: [
        {
          label: 'Yes',
          value: 62.38
        },
        {
          label: 'No',
          value: 37.62
        }
      ],
      name: 'Activia Regular Yogurt (Strawberry)'
    },
    {
      data: [
        {
          label: 'Yes',
          value: 70.45
        },
        {
          label: 'No',
          value: 29.55
        }
      ],
      name: 'Dannon Whole Milk Yogurt (Strawberry)'
    },
    {
      data: [
        {
          label: 'Yes',
          value: 74.53
        },
        {
          label: 'No',
          value: 25.47
        }
      ],
      name: 'Yoplait Original Yogurt (Strawberry)'
    },
    {
      data: [
        {
          label: 'Yes',
          value: 48.6
        },
        {
          label: 'No',
          value: 51.4
        }
      ],
      name: 'Great Value (Walmart) Original Yogurt (Strawberry)'
    },
    {
      data: [
        {
          label: 'Yes',
          value: 58.82
        },
        {
          label: 'No',
          value: 41.18
        }
      ],
      name: 'Liberte Whole Milk Yogurt (Strawberry)'
    },
    {
      data: [
        {
          label: 'Yes',
          value: 70.34
        },
        {
          label: 'No',
          value: 29.66
        }
      ],
      name: 'Oui French Style Yogurt (Strawberry)'
    },
    {
      data: [
        {
          label: 'Yes',
          value: 70.68
        },
        {
          label: 'No',
          value: 29.32
        }
      ],
      name: 'Yoplait Fruitside Yogurt (Strawberry)'
    },
    {
      data: [
        {
          label: 'Yes',
          value: 64.93
        },
        {
          label: 'No',
          value: 35.07
        }
      ],
      name: 'Trader Joes Regular Yogurt (Strawberry)'
    }
  ]
}

describe('StackedBarChart', () => {
  let testRender
  let chartIndex
  let container

  beforeEach(() => {
    container = global.document.createElement('div', { class: 'tooltip-title' })
    global.document.body.appendChild(container)

    chartIndex = 1
  })

  afterEach(() => {
    testRender.detach()
    testRender.unmount()
    global.document.body.removeChild(container)
    container = null
  })

  test('should render StackedBarChart', async () => {
    testRender = mount(
      <StackedBarChart
        chartSettings={{}}
        chartIndex={chartIndex}
        inputData={inputData}
      />,
      { attachTo: container }
    )
    expect(testRender).toMatchSnapshot()
    expect(testRender.find('#stackedchart_121').html()).toMatchSnapshot()
    await wait(500)
    expect(testRender.find('#stackedchart_121').html()).toMatchSnapshot()
  })

  test('legend events', async () => {
    testRender = mount(
      <StackedBarChart
        chartSettings={{}}
        chartIndex={chartIndex}
        inputData={inputData}
      />,
      { attachTo: container }
    )

    await wait(500)

    const YES = 0
    const NO = 1
    function clickLegendLabel (index) {
      act(() => {
        const allLegendLabels = global.document.querySelectorAll(
          '#stackedchart_121 .chart-legend .legend'
        )
        return allLegendLabels[index].click()
      })
    }

    function visibleLegends () {
      const labels = []
      for (let node of global.document.querySelectorAll(
        '#stackedchart_121 .legend:not(.faded-legend) span'
      )) {
        labels.push(node.innerHTML)
      }
      return labels
    }

    expect(visibleLegends()).toEqual(['Yes', 'No', 'Unknown'])

    // click on Yes => show Yes only
    clickLegendLabel(YES)
    expect(visibleLegends()).toEqual(['Yes'])

    // click on No => show Yes & No
    clickLegendLabel(NO)
    expect(visibleLegends()).toEqual(['Yes', 'No'])

    // click on Yes again => show No only
    clickLegendLabel(YES)
    expect(visibleLegends()).toEqual(['No'])

    // click on No again => show all
    clickLegendLabel(NO)
    expect(visibleLegends()).toEqual(['Yes', 'No', 'Unknown'])
  })

  // to do luca fix the test
  // test('table events', async () => {
  //   testRender = mount(
  //     <StackedBarChart
  //       chartSettings={{
  //         singleColor: ['#aaaaa'],
  //         colorsArr: ['#aaaaaa', `#666666`]
  //       }}
  //       chartIndex={chartIndex}
  //       inputData={inputData}
  //     />,
  //     { attachTo: container }
  //   )

  //   expect(testRender).toMatchSnapshot()
  //   await wait(500)

  //   function clickColumn (index) {
  //     act(() => {
  //       const allHeaders = global.document.querySelectorAll(
  //         '#stackedchart_121 table th'
  //       )
  //       return allHeaders[index].click()
  //     })
  //   }

  //   function visibleLabelsOnHorizontalChart () {
  //     const labels = []
  //     for (let node of global.document.querySelectorAll(
  //       '#stackedchart_121 .horizontal-barchart .yLabel'
  //     )) {
  //       if (parseFloat(node.style.opacity) !== 0) {
  //         labels.push(node.innerHTML)
  //       }
  //     }

  //     return labels
  //   }

  //   clickColumn(1)
  //   expect(visibleLabelsOnHorizontalChart()).toEqual([
  //     '<span>Activia Regular Yogurt (Strawberry)</span>'
  //   ])

  //   clickColumn(2)
  //   expect(visibleLabelsOnHorizontalChart()).toEqual([
  //     '<span>Activia Regular Yogurt (Strawberry)</span>',
  //     '<span>Dannon Whole Milk Yogurt (Strawberry)</span>'
  //   ])

  //   clickColumn(2)
  //   expect(visibleLabelsOnHorizontalChart()).toEqual([
  //     '<span>Activia Regular Yogurt (Strawberry)</span>'
  //   ])

  //   clickColumn(1)
  //   expect(visibleLabelsOnHorizontalChart()).toEqual([
  //     '<span>Activia Regular Yogurt (Strawberry)</span>',
  //     '<span>Dannon Whole Milk Yogurt (Strawberry)</span>',
  //     '<span>Yoplait Original Yogurt (Strawberry)</span>',
  //     '<span>Great Value (Walmart) Original Yogurt (Strawberry)</span>',
  //     '<span>Liberte Whole Milk Yogurt (Strawberry)</span>',
  //     '<span>Oui French Style Yogurt (Strawberry)</span>',
  //     '<span>Yoplait Fruitside Yogurt (Strawberry)</span>',
  //     '<span>Trader Joes Regular Yogurt (Strawberry)</span>'
  //   ])
  // })
})
