import styled from 'styled-components'
import { DefaultChart } from '../styles'
import { family } from '../../../utils/Fonts'
import colors from '../../../utils/Colors'

export const Container = styled(DefaultChart)`
  .stacked-barchart-means text {
    color: ${colors.SLATE_GREY};
    fill: ${colors.SLATE_GREY};
  }
  .stacked-barchart-container {
    position: relative;
  }

  .stacked-barchart {
    top: 0;
    left: 0;
    z-index: 2;
  }

  .stacked-barchart-g rect.faded-rect {
    opacity: 0;
  }

  .horizontal-barchart {
    position: absolute;
    right: 0px;
    top: 0;
    z-index: 1;
  }

  .horizontal-barchart rect.faded-rect {
    opacity: 0;
  }

  .barchart-table {
    text-align: center;
    font-size: 10px;
    color: #000;
    padding-top: 10px;
    margin-top: 26px;
  }

  .barchart-table .header-th {
    cursor: pointer;
    font-weight: 700;
    font-family: ${family.primaryBold};
    padding-bottom: 5px;
  }

  .barchart-table .faded-td {
    opacity: 0.5;
  }

  .barchart-table th:first-child,
  .barchart-table td:first-child {
    text-align: left;
  }

  .yLabel {
    padding: 0;
  }
`
