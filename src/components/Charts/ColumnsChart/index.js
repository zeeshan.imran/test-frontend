import React, { useState, useEffect, useRef, useMemo } from 'react'
import * as d3 from 'd3'
import { Container } from './styles'
import { getSelectedData } from '../../../containers/Charts/ColumnsChart/GetData'
import {
  getProducts,
  getMaxSelectedValue,
  getTotalSum
} from '../../../containers/Charts/GetData'
import {
  appendChartLegend,
  updateChartLegendColorScheme,
  tooltipLayer
} from '../../../utils/ChartsHelperFunctions'
import { defaultChartSettings } from '../../../defaults/chartSettings'
import { NOT_PRODUCT } from '../../../utils/chartUtils'
import { findIndex, propEq } from 'ramda'
import withPptDump from '../withPptDump'

const getInitialProducts = inputData =>
  getProducts(inputData)
    ? getProducts(inputData).map((el, i) => ({
        name: el,
        index: i,
        isSelected: true
      }))
    : [{ name: NOT_PRODUCT, index: 0, isSelected: true }]

const ColumnsChart = ({ cardWidth, inputData, chartSettings = {}, t }) => {
  const chartName = 'columnchart_' + inputData.chart_id
  const marginOuter = d3
    .scaleLinear()
    .range([26, 400])
    .domain([1, 0.2])(chartSettings.chartWidth ? chartSettings.chartWidth : 1)
  const chartMargin = {
    top: 16,
    right: marginOuter,
    bottom: 25,
    left: marginOuter
  }
  const svgWidth = parseInt(cardWidth, 10) * 0.95 + 'px'
  const svgHeight = parseInt(cardWidth, 10) * 0.28 + 'px'
  const chartWidth =
    parseInt(svgWidth, 10) - chartMargin.left - chartMargin.right
  const chartHeight =
    parseInt(svgHeight, 10) - chartMargin.top - chartMargin.bottom

  const labels = inputData.labels
  const [products, setProducts] = useState(getInitialProducts(inputData))

  const xScale = d3
    .scaleBand()
    .rangeRound([0, chartWidth])
    .paddingInner(0.2)
    .paddingOuter(0.1)
    .domain(labels)

  const colorsScale = d3
    .scaleOrdinal()
    .range(chartSettings.colorsArr || defaultChartSettings.colorsArr)
    .domain(
      products && products.length > 1 ? products.map(el => el.index) : labels
    )

  const mounted = useRef()

  useEffect(() => {
    if (!mounted.current) {
      initChart(inputData)
      mounted.current = true
    } else {
      updateChart()
    }
  }, [
    products,
    chartSettings.isCountProGroupShown,
    chartSettings.isCountProLabelShown,
    chartSettings.hasOutline,
    chartSettings.hasOutline_color,
    chartSettings.hasOutline_width,
    chartSettings.valueType,
    chartSettings.howManyDecimals,
    chartSettings.percentageGrouping,
    chartSettings.fontSize_labels,
    chartSettings.fontFamily_labels,
    chartSettings.fontSize_legend,
    chartSettings.fontFamily_legend,
    chartSettings.fontSize_axis,
    chartSettings.fontFamily_axis,
    chartSettings.fontSize_values,
    chartSettings.fontFamily_values,
    chartSettings.isChartGridHidden
  ])

  useEffect(() => {
    if (mounted.current) {
      // Products obj is invalid - reinit
      setProducts(getInitialProducts(inputData))
      mounted.current = false
    }
  }, [chartSettings.isProductLabelsSwapped, inputData])

  useEffect(() => {
    if (mounted.current) {
      updateChart()
      updateChartLegendColorScheme(
        d3.select('#' + chartName).select('.columns-barchart'),
        colorsScale.copy().domain(products.map(el => el.index))
      )
    }
  }, [chartSettings.colorsArr])

  const totalSum = useMemo(() => getTotalSum(inputData), [inputData])

  const initChart = async inputData => {
    d3.select('.' + chartName).remove()

    const chartsContainer = d3
      .select('#' + chartName)
      .append('div')
      .attr('class', chartName)
      .append('div')
      .attr('class', 'columns-barchart-container')

    const chartSvg = chartsContainer
      .append('div')
      .attr('class', 'columns-barchart')
      .style('width', svgWidth)
      .append('svg')

    const g = chartSvg
      .attr('width', chartWidth + chartMargin.left + chartMargin.right)
      .attr('height', chartHeight + chartMargin.top + chartMargin.bottom)
      .append('g')
      .attr('class', 'stacked')
      .attr(
        'transform',
        'translate(' + chartMargin.left + ',' + chartMargin.top + ')'
      )

    g.append('g').attr('class', 'axisY')

    g.append('g')
      .attr('class', 'axisX')
      .attr('transform', 'translate(0,' + chartHeight + ')')

    g.append('g').attr('class', 'columns-barchart-g-cont')

    chartsContainer
      .select('.columns-barchart')
      .append('div')
      .attr('class', 'xLabels')
      .style('position', 'relative')
      .style('left', chartMargin.left + 'px')
      .style('top', -chartMargin.bottom + 'px')
      .selectAll('.xLabel')
      .data(labels, d => d)
      .enter()
      .append('div')
      .attr('class', 'xLabel')
      .style('width', xScale.step() + 'px')
      .style(
        'left',
        (d, i) =>
          xScale(d) - (xScale.bandwidth() * xScale.paddingInner()) / 2 + 'px'
      )
      .style('position', 'absolute')
      .each(function (d) {
        const txtSpan = d3.select(this).append('span')
        let txt = d
        for (let i = 0; i <= 5; i++) {
          if (i === 1) {
            d3.select(this)
              .on('mouseover', () => tooltipLayer.showTooltip({ title: d }))
              .on('mousemove', tooltipLayer.updateTooltip)
              .on('mouseleave', tooltipLayer.clearTooltip)
          }
          const txtW = txtSpan
            .text(txt)
            .node()
            .getBoundingClientRect().width
          if (txtW > xScale.step() - 10) {
            txt = txt.substring(0, txt.length / 2) + '..'
          } else {
            break
          }
        }
      })

    // Add margin to xLabels
    let maxH = 0
    chartsContainer
      .select('.columns-barchart')
      .selectAll('.xLabel')
      .each(function (d) {
        const h = d3
          .select(this)
          .node()
          .getBoundingClientRect().height
        maxH = Math.max(h, maxH)
      })
    chartsContainer
      .select('.columns-barchart .xLabels')
      .style('height', maxH - chartMargin.bottom + 'px')

    const legendContainer = chartsContainer.select('.columns-barchart')

    if (!inputData.results.data && products.length > 1) {
      appendChartLegend(
        legendContainer,
        products,
        colorsScale.copy().domain(products.map(el => el.index)),
        setProducts,
        true
      )
    }

    updateChart()
  }

  const updateChart = () => {
    updateColumnsChart()
  }

  const updateColumnsChart = () => {
    if (getInitialProducts(inputData).length !== products.length) {
      // Hardfix for useEffect race conditions
      return
    }
    const decimals = !isNaN(chartSettings.howManyDecimals)
      ? parseInt(chartSettings.howManyDecimals, 10)
      : 2

    const maximalBarWidth = chartWidth / 6

    const chartsContainer = d3
      .select('#' + chartName)
      .select('.columns-barchart-container')

    const xInner = d3
      .scaleBand()
      .rangeRound([0, xScale.bandwidth()])
      .paddingInner(0.4)
      .paddingOuter(0.2)
      .domain(products.filter(el => el.isSelected).map(el => el.index))

    let yDomain
    if (chartSettings.valueType === 'percentage') {
      yDomain = [
        0,
        Math.min(
          Math.max(
            getMaxSelectedValue(inputData, products) / Math.max(totalSum, 1)
          ) * 1.2,
          1
        )
      ]
      if (yDomain[1] === 0) {
        yDomain[1] = 1
      }
    } else if (inputData.range) {
      yDomain = [inputData.range[0], inputData.range[1]]
    } else {
      yDomain = [0, Math.max(getMaxSelectedValue(inputData, products) * 1.2, 1)]
    }

    const y = d3
      .scaleLinear()
      .rangeRound([chartHeight, 0])
      .domain(yDomain)
      .nice()

    const yAxis = d3
      .axisLeft(y)
      .ticks(5)
      .tickSizeOuter(0)
      .tickSizeInner(-chartWidth)
      .tickFormat(
        chartSettings.valueType === 'percentage' ? d3.format('.0%') : null
      )

    const customYAxis = g => {
      const s = g.selection ? g.selection() : g
      g.call(yAxis)
      s.select('.domain').remove()
      s.selectAll('.tick line').attr('x2', chartWidth)
    }

    chartsContainer
      .select('.axisX')
      .call(d3.axisBottom(xScale))
      .each(function () {
        d3.select(this)
          .selectAll('line')
          .attr('transform', 'translate(' + xScale.step() / 2 + ', 0)')

        d3.select(this)
          .selectAll('text')
          .remove()
      })

    chartsContainer
      .select('.axisY')
      .transition()
      .duration(300)
      .call(customYAxis)
      .on('end', function () {
        d3.select(this)
          .selectAll('.tick line')
          .attr('x2', chartWidth)
      })

    const selectedData = getSelectedData(inputData, products)

    const labelsSelection = chartsContainer
      .select('.columns-barchart-g-cont')
      .selectAll('.outerContainer')
      .data(selectedData)

    const groupEnter = labelsSelection
      .enter()
      .append('g')
      .classed('outerContainer', true)

    const bars = groupEnter
      .merge(labelsSelection)
      .selectAll('.innerContainer')
      .data(
        function (d) {
          return d.data
        },
        d => d.name
      )

    const valuesFontSize = chartSettings.fontSize_values
      ? chartSettings.fontSize_values
      : 1
    const valuesFontFamily = chartSettings.fontFamily_values
      ? chartSettings.fontFamily_values
        : 'Lato'
    groupEnter
      .append('text')
      .classed('groupedCount', true)
      .style('text-anchor', 'middle')
      .classed('resultsText', true)

    if (chartSettings.isCountProGroupShown) {
      const selectedIndexes = products
        .filter(el => el.isSelected)
        .map(el => el.index)

      chartsContainer
        .select('.columns-barchart-g-cont')
        .selectAll('.groupedCount')
        .style('font-size', `${valuesFontSize}rem`)
        .style('font-family', valuesFontFamily)
        .attr('x', d => xScale(d.label) + xScale.bandwidth() / 2)
        .attr('y', -5 * valuesFontSize)
        .text(d => {
          let sum = 0
          selectedIndexes.forEach(index => (sum += d.data[index].value))
          return chartSettings.valueType === 'percentage'
            ? `${((100 * sum) / Math.max(totalSum, 1)).toFixed(decimals)}%`
            : sum
        })
        .style('opacity', 1)
    } else {
      chartsContainer
        .select('.columns-barchart-g-cont')
        .selectAll('.groupedCount')
        .style('opacity', 0)
    }

    const barsToUpdate = bars
      .enter()
      .append('g')
      .classed('innerContainer', true)
      .on('mousemove', tooltipLayer.updateTooltip)
      .on('mouseleave', tooltipLayer.clearTooltip)
      .each(function (d) {
        const xStart =
          xInner.bandwidth() > maximalBarWidth
            ? xScale(d.label) +
              xInner(findIndex(propEq('name', d.name), products)) +
              (xInner.bandwidth() - maximalBarWidth) / 2
            : xScale(d.label) +
              xInner(findIndex(propEq('name', d.name), products))
        const width = Math.min(xInner.bandwidth(), maximalBarWidth)
        d3.select(this)
          .append('rect')
          .attr('x', xStart)
          .attr('y', chartHeight)
          .attr('width', width)
          .attr('height', 0)
          .style('opacity', 0)
          .transition(300)
          .style('opacity', 1)
        d3.select(this)
          .append('text')
          .attr('text-anchor', 'middle')
          .classed('resultsText', true)
          .style('font-size', `${valuesFontSize}rem`)
          .style('font-family', chartSettings.fontFamily_values)
          .attr('x', xStart + width / 2 - 1)
          .attr('y', chartHeight - 4 * valuesFontSize)
          .style('opacity', chartSettings.isCountProLabelShown ? 1 : 0)
      })
      .merge(bars)

    barsToUpdate
      .on('mouseover', d => {
        const title = ['paired-questions', 'slider'].includes(
          inputData.question_type
        )
          ? t('charts.table.attribute')
          : t('charts.table.answer')

        let results = []

        if (['paired-questions', 'slider'].includes(inputData.question_type)) {
          results.push({
            label: t('charts.table.score'),
            value: d.value % 1 === 0 ? d.value : d.value.toFixed(decimals)
          })
        } else {
          results.push(
            {
              label: t('charts.tooltip.total'),
              value: d.value % 1 === 0 ? d.value : d.value.toFixed(decimals)
            },
            {
              label: t('charts.tooltip.percentageOfTotal'),
              value:
                ((100 * d.value) / Math.max(totalSum, 1)).toFixed(decimals) +
                '%'
            }
          )
        }

        if (
          inputData.question_with_products ||
          inputData.question_type === 'matrix'
        ) {
          results.unshift({
            label: t('charts.table.answer'),
            value: d.label
          })
          const prodSum = d3.sum(
            inputData.results[d.productIndex].data,
            el => el.value
          )
          results.push({
            label: chartSettings.isProductLabelsSwapped
              ? t('charts.tooltip.percentageOfTehGroup')
              : t('charts.tooltip.percentageOfProductGroup'),
            value: `${((100 * d.value) / prodSum).toFixed(decimals)}%`
          })
          const labelRes = inputData.results.map(prodRes =>
            prodRes.data.find(el => el.label === d.label)
          )
          results.push({
            label: chartSettings.isProductLabelsSwapped
              ? t('charts.tooltip.percentageOfProductGroup')
              : t('charts.tooltip.percentageOfTehGroup'),
            value: `${(
              (100 * d.value) /
              d3.sum(labelRes, el => el.value)
            ).toFixed(decimals)}%`
          })
        }

        return tooltipLayer.showTooltip({
          title:
            inputData.question_with_products ||
            inputData.question_type === 'matrix'
              ? d.name
              : `${title}: ${d.label}`,
          results
        })
      })
      .each(function (d) {
        const xStart =
          xInner.bandwidth() > maximalBarWidth
            ? xScale(d.label) +
              xInner(findIndex(propEq('name', d.name), products)) +
              (xInner.bandwidth() - maximalBarWidth) / 2
            : xScale(d.label) +
              xInner(findIndex(propEq('name', d.name), products))
        const yStart = y(
          chartSettings.valueType === 'percentage'
            ? d.value / Math.max(totalSum, 1)
            : d.value
        )
        const width = Math.min(xInner.bandwidth(), maximalBarWidth)
        const height = chartHeight - yStart

        d3.select(this)
          .select('rect')
          .attr('fill', d =>
            colorsScale(
              products && products.length > 1 ? d.productIndex : d.label
            )
          )
          .attr(
            'stroke',
            (chartSettings.hasOutline_color &&
              chartSettings.hasOutline_color[0]) ||
              defaultChartSettings.outlineColor
          )
          .attr(
            'stroke-width',
            chartSettings.hasOutline ? chartSettings.hasOutline_width || 1 : 0
          )
          .transition()
          .duration(mounted.current ? 400 : 0)
          .attr('x', xStart)
          .attr('y', yStart)
          .attr('width', width)
          .attr('height', height)

        if (chartSettings.isCountProLabelShown) {
          let text
          if (chartSettings.valueType === 'value' || !chartSettings.valueType) {
            // Check if has decimals
            text = d.value % 1 === 0 ? d.value : d.value.toFixed(decimals)
          } else if (
            inputData.question_with_products ||
            inputData.question_type === 'matrix'
          ) {
            if (
              (chartSettings.percentageGrouping === 'outer_group' &&
                !chartSettings.isProductLabelsSwapped) ||
              (chartSettings.percentageGrouping === 'inner_group' &&
                chartSettings.isProductLabelsSwapped)
            ) {
              const prodSum = d3.sum(
                inputData.results[d.productIndex].data,
                el => el.value
              )
              text = `${((100 * d.value) / Math.max(prodSum, 1)).toFixed(
                decimals
              )}%`
            } else {
              const labelRes = inputData.results.map(prodRes =>
                prodRes.data.find(el => el.label === d.label)
              )
              text = `${(
                (100 * d.value) /
                Math.max(d3.sum(labelRes, el => el.value), 1)
              ).toFixed(decimals)}%`
            }
          } else {
            text = `${((100 * d.value) / Math.max(totalSum, 1)).toFixed(
              decimals
            )}%`
          }
          d3.select(this)
            .select('text')
            .text(text)
            .style('opacity', 1)
            .transition()
            .duration(mounted.current ? 400 : 0)
            .attr('x', xStart + width / 2 - 1)
            .attr('y', yStart - 4)
        } else {
          d3.select(this)
            .select('text')
            .style('opacity', 0)
        }
      })

    bars.exit().remove()

    chartsContainer
      .selectAll('.xLabel')
      .style('font-size', `${chartSettings.fontSize_labels || 1}rem`)
      .style('font-family', chartSettings.fontFamily_labels || 'Lato')

    chartsContainer
      .selectAll('.chart-legend .legend')
      .style('font-size', `${chartSettings.fontSize_legend || 1.1}rem`)
      .style('font-family', chartSettings.fontFamily_legend || 'Lato')

    chartsContainer
      .selectAll('.axisY .tick text')
      .style('font-size', `${chartSettings.fontSize_axis || 1}rem`)
      .style('font-family', chartSettings.fontFamily_axis || 'Lato')

    chartsContainer
      .selectAll('.axisY .tick line')
      .style('display', chartSettings.isChartGridHidden ? 'none' : 'block')
  }

  return (
    <Container>
      <div id={chartName} />
    </Container>
  )
}

export default withPptDump('column-chart')(ColumnsChart)
