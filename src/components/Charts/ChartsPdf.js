import React from 'react'

import ChartSwitch from './ChartSwitch'
import { ChartContainer } from './styles'
const ChartsPdf = ({
  charts,
  hideFilter = true,
  idx,
  chartsSettings,
  t,
  fullWidth = '850px'
}) => {
  const fullCardWidth = fullWidth
  const halfCardWidth = '445px'
  const cardMargin = '15px auto'
  let syncedCharts = null
  if (charts) {
    syncedCharts = { ...charts, ...{ loading: false } }
  }
  return (
    <ChartContainer id={`chart-view-${idx}`}>
      <ChartSwitch
        el={syncedCharts}
        idx={idx}
        chartsSettings={chartsSettings}
        fullCardWidth={fullCardWidth}
        halfCardWidth={halfCardWidth}
        cardMargin={cardMargin}
        hideFilter={hideFilter}
        t={t}
      />
    </ChartContainer>
  )
}

export default ChartsPdf
