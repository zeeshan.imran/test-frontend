import React from 'react'
import { Table } from 'antd'
import { Container } from './styles'
import withPptDump from '../withPptDump'

const DataTable = ({ columns, rows, showAll = false }) => {
  const pageSize = showAll ? (rows && rows.length) || 0 : undefined
  return (
    <Container>
      <Table
        dataSource={rows}
        columns={columns}
        useFixedHeader={showAll}
        pagination={{
          hideOnSinglePage: true,
          pageSize
        }}
      />
    </Container>
  )
}

export default withPptDump('table', ({ columns, rows }) => ({ columns, dataSource:rows }))(
  DataTable
)
