import React from 'react'
import { mount } from 'enzyme'
import PaypalEmailForm from '.'

describe('PaypalEmailForm', () => {
  let testRender
  let validationSchema
  let prompt
  let handleFieldChange

  beforeEach(() => {
    validationSchema = ''
    prompt = ''
    handleFieldChange = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PaypalEmailForm', async () => {
    testRender = mount(
      <PaypalEmailForm
        validationSchema={validationSchema}
        prompt={prompt}
        handleFieldChange={handleFieldChange}
      />
    )
    expect(testRender.find(PaypalEmailForm)).toHaveLength(1)

    testRender
      .findWhere(c => c.name() === 'InputArea' && c.prop('name') === 'prompt')
      .first()
      .prop('onChange')({ target: { value: 'prompt' } })

    expect(handleFieldChange).toHaveBeenCalledWith({
      prompt: 'prompt'
    })
  })

  test('should render PaypalEmailForm of InputArea for secondaryPrompt', async () => {
    testRender = mount(
      <PaypalEmailForm
        validationSchema={validationSchema}
        prompt={prompt}
        handleFieldChange={handleFieldChange}
      />
    )
    testRender
      .findWhere(
        c => c.name() === 'InputArea' && c.prop('name') === 'secondaryPrompt'
      )
      .last()
      .prop('onChange')({
        target: {
          value: 'secondaryPrompt'
        }
      })

    expect(handleFieldChange).toHaveBeenCalledWith({
      secondaryPrompt: 'secondaryPrompt'
    })
  })
})
