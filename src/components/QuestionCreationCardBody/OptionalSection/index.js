import React from 'react'
import { Row } from 'antd'
import Checkbox from '../../Checkbox'
import { Container, CollapsibleSectionContainer } from './styles'

const OptionalSection = ({ label, onClickCheckbox, isExpanded, children }) => (
  <Container>
    <Row>
      <Checkbox checked={isExpanded} onChange={onClickCheckbox}>
        {label}
      </Checkbox>
    </Row>
    {isExpanded ? (
      <CollapsibleSectionContainer>{children}</CollapsibleSectionContainer>
    ) : null}
  </Container>
)

export default OptionalSection
