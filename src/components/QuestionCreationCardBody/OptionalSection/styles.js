import styled from 'styled-components'

export const Container = styled.div`
  margin-bottom: 1.5rem;
`

export const CollapsibleSectionContainer = styled.div`
  margin-top: 1.5rem;
`
