import React, { useState, useEffect } from 'react'
import { Row, Input as AntInput, Form, Col } from 'antd'
import * as Yup from 'yup'
import { useTranslation } from 'react-i18next'
import FieldLabel from '../../FieldLabel'
import UploadOptionPicture from '../../UploadOptionPicture'
import StaticUrlOrBase64 from '../../../utils/imagesStaticUrl'
import Input from '../../Input'
import {
  Tag,
  NewTagButton,
  uploadStyle,
  TagInputStyle,
} from './styles'

const TagInput = ({
  label,
  tags,
  handleAddTag,
  handleRemoveTag,
  onAddImageUrl,
  onRemoveImageUrl,
  isStrictMode,
  canAddMoreTags = tags => true,
  typeOfQuestion = '',
  onChangePairAttribute = () => {}
}) => {
  const { t } = useTranslation()
  const [inputFieldVisible, setInputFieldVisible] = useState(false)
  const [inputFieldValue, setInputFieldValue] = useState('')
  const [inputError, setInputError] = useState()
  const handleTagInput = () => {
    if (inputFieldValue) {
      handleAddTag(inputFieldValue)
    }
    setInputFieldVisible(false)
    setInputFieldValue('')
  }

  useEffect(() => {
    (async () => {
      try {
        await Yup.string()
          .test(
            'not-empty',
            t('validation.vertical.range.labels.required'),
            value => !/^[ ]+$/.test(value)
          )
          .test(
            'length-limitation',
            t('validation.paired.pairs.length'),
            value => (!value ? true : value.length < 75)
          )
          .required(t('validation.vertical.range.labels.required'))
          .validate(inputFieldValue)
        setInputError(false)
      } catch (error) {
        if (error.errors && error.errors.length) {
          setInputError(error.errors[0])
        }
      }
    })()
  })

  return (
    <FieldLabel label={label}>
      <Row>
        {tags.map((tag, index) => (
          <Row id={tag.pair} gutter={24} type='flex' align='middle'>
            <TagInputStyle flex='auto'>
              <Tag
                key={index}
                closable={!isStrictMode}
                onClose={() => {
                  handleRemoveTag(index)
                }}
              >
                {tag.pair}
              </Tag>
            </TagInputStyle>
            {typeOfQuestion && typeOfQuestion === 'paired-questions' && (
              <React.Fragment>
                <Col flex='auto'>
                  <Form.Item>
                    <Input
                      label={t('components.questionCreation.tagInput.internalName')}
                      name="internalName"
                      size="default"
                      placeholder="Internal name"
                      value={tag && tag.internalName}
                      // disabled={isStrictMode}
                      onChange={e =>
                        onChangePairAttribute(
                          'internalName',
                          e.target.value,
                          index
                        )
                      }
                    />
                  </Form.Item>
                </Col>

                <Col flex='auto'>
                  <Form.Item>
                    <Input
                      label={t('components.questionCreation.tagInput.tooltip')}
                      name="tooltip"
                      size="default"
                      maxLength={100}
                      placeholder="Tooltip"
                      value={tag && tag.tooltip}
                      onChange={e =>
                        onChangePairAttribute('tooltip', e.target.value, index)
                      }
                    />
                  </Form.Item>
                </Col>
              </React.Fragment>
            )}
            <Col flex='auto'>
              <UploadOptionPicture
                name={index}
                uploadStyle={uploadStyle}
                onAddImageUrl={onAddImageUrl}
                image={tag && tag.image800 && StaticUrlOrBase64(tag.image800)}
                onRemoveImageUrl={() => {
                  onRemoveImageUrl(tag.pair)
                }}
              />
            </Col>
          </Row>
        ))}

        {canAddMoreTags(tags) && !isStrictMode && (
          <React.Fragment>
            {inputFieldVisible ? (
              <Form.Item
                help={inputError}
                validateStatus={inputError ? 'error' : 'success'}
              >
                <AntInput
                  type='text'
                  size='default'
                  data-testid={'add-value-text'}
                  style={{ width: 78 }}
                  value={inputFieldValue}
                  onChange={e => setInputFieldValue(e.target.value)}
                  onBlur={handleTagInput}
                  onPressEnter={handleTagInput}
                />
              </Form.Item>
            ) : (
              <NewTagButton
                data-testid={'add-value-button'}
                onClick={() => {
                  setInputFieldVisible(true)
                }}
              >
                + {t('components.questionCreation.tagInput.add')}
              </NewTagButton>
            )}
          </React.Fragment>
        )}
      </Row>
    </FieldLabel>
  )
}

export default TagInput
