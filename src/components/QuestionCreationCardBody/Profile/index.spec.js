import React from 'react'
import { shallow } from 'enzyme'
import Profile from '.'

describe('Profile', () => {
  let testRender
  let prompt

  beforeEach(() => {
    prompt = ''
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Profile', async () => {
    testRender = shallow(<Profile prompt={prompt} />)
    expect(testRender).toMatchSnapshot()
  })
})
