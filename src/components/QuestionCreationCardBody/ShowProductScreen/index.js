import React from 'react'
import { Formik } from 'formik'
import { Form, Row, Col } from 'antd'
import { useQuery } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import useQuestionValidate from '../../../hooks/useQuestionValidate'
import InputArea from '../../InputArea'
import { useTranslation } from 'react-i18next'
import useValidateWithContext from '../../../hooks/useValidateWithContext'
import InputRichText from '../../InputRichText'
import QuestionInternalName from '../QuestionInternalName'
import DaysAndTimePicker from '../../DaysAndTimePicker'

const ShowProductScreen = ({
  validationSchema,
  prompt,
  secondaryPrompt,
  handleFieldChange,
  chooseProductOptions = {},
  productsSkip = [],
  internalPrompt = '',
  showInPreferedLanguage,
  addDelayToSelectNextProductAndNextQuestion = false,
  delayToNextQuestion = '',
  extraDelayToNextQuestion = '',
  ...rest
}) => {
  const { t } = useTranslation()
  const {
    data: {
      surveyCreation: { products }
    }
  } = useQuery(SURVEY_CREATION)
  productsSkip = productsSkip || []
  const formRef = useQuestionValidate()

  const validate = useValidateWithContext(
    validationSchema,
    () => ({ products }),
    [products]
  )

  return (
    <Formik
      ref={formRef}
      enableReinitialize
      validate={validate}
      initialValues={{
        prompt,
        secondaryPrompt,
        chooseProductOptions,
        internalPrompt,
        delayToNextQuestion,
        extraDelayToNextQuestion
      }}
      render={({ errors, setFieldValue, values }) => {
        return (
          <React.Fragment>
            <Row gutter={24}>
              <Col lg={16} md={24}>
                <Form.Item
                  help={errors.prompt}
                  validateStatus={errors.prompt ? 'error' : 'success'}
                >
                  <InputRichText
                    outerAlign='left'
                    name='prompt'
                    label={t('components.questionCreation.prompt')}
                    tooltip={t('tooltips.questionCreation.prompt')}
                    value={values.prompt}
                    onChange={value => {
                      setFieldValue('prompt', value)
                      handleFieldChange({ prompt: value })
                    }}
                    required
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col lg={16} md={24}>
                <Form.Item>
                  <InputArea
                    autoSize
                    name='secondaryPrompt'
                    value={values.secondaryPrompt}
                    onChange={event => {
                      setFieldValue('secondaryPrompt', event.target.value)
                      handleFieldChange({ secondaryPrompt: event.target.value })
                    }}
                    label={t('components.questionCreation.secondaryPrompt')}
                    tooltip={t('tooltips.questionCreation.secondaryPrompt')}
                    size='default'
                  />
                </Form.Item>
              </Col>
            </Row>
            {showInPreferedLanguage && (
              <QuestionInternalName
                setFieldValue={setFieldValue}
                handleFieldChange={handleFieldChange}
                internalPrompt={values.internalPrompt}
              />
            )}
            {/* Add delay for next question selection */}
            {addDelayToSelectNextProductAndNextQuestion && (
              <Row>
                <Form.Item>
                  <DaysAndTimePicker
                    delayToNext={values.delayToNextQuestion}
                    extraDelayToNext={values.extraDelayToNextQuestion}
                    onChangeDelayToNext={value => {
                      setFieldValue('delayToNextQuestion', value)
                      handleFieldChange({ delayToNextQuestion: value })
                    }}
                    onChangeExtraDelayToNext={value => {
                      setFieldValue('extraDelayToNextQuestion', value)
                      handleFieldChange({ extraDelayToNextQuestion: value })
                    }}
                    buttonText='Add delay to select next question'
                  />
                </Form.Item>
              </Row>
            )}
          </React.Fragment>
        )
      }}
    />
  )
}

const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      products
    }
  }
`

export default ShowProductScreen
