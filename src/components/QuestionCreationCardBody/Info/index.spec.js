import React from 'react'
import { mount } from 'enzyme'
import InfoForm from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k,
  getFixedT: () => () => ''
}))

describe('InfoForm', () => {
  let testRender
  let validationSchema
  let prompt
  let secondaryPrompt
  let handleFieldChange

  beforeEach(() => {
    validationSchema = ''
    prompt = ''
    secondaryPrompt = ''
    handleFieldChange = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render InfoForm', async () => {
    testRender = mount(
      <InfoForm
        validationSchema={validationSchema}
        prompt={prompt}
        secondaryPrompt={secondaryPrompt}
        handleFieldChange={handleFieldChange}
      />
    )
    expect(testRender.find(InfoForm)).toHaveLength(1)

    testRender
      .findWhere(c => c.name() === 'InputArea' && c.prop('name') === 'prompt')
      .first()
      .prop('onChange')({ target: { value: 'prompt' } })

    expect(handleFieldChange).toHaveBeenCalledWith({
      prompt: 'prompt',
      requiredQuestion: false
    })
  })

  test('should render InfoForm of InputRichText for secondaryPrompt', async () => {
    testRender = mount(
      <InfoForm
        validationSchema={validationSchema}
        secondaryPrompt={secondaryPrompt}
        handleFieldChange={handleFieldChange}
      />
    )
    testRender
      .findWhere(
        c =>
          c.name() === 'InputRichText' && c.prop('name') === 'secondaryPrompt'
      )
      .last()
      .prop('onChange')('secondaryPrompt')

    expect(handleFieldChange).toHaveBeenCalledWith({
      secondaryPrompt: 'secondaryPrompt',
      requiredQuestion: false
    })
  })
})
