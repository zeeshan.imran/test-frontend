import React, { useState } from 'react'
import { Row, Col, Radio, Collapse, Icon } from 'antd'
import { withTranslation, useTranslation } from 'react-i18next'
import FieldLabel from '../../FieldLabel'
import { StyledCollapse } from '../ChartSection/styles'
import { RadioButton } from './styles.js'

const ImageAndLabelSetting = ({
  isNewQuestion,
  onFieldChange,
  optionDisplayType,
  style,
  disabled
}) => {
  const { t } = useTranslation()
  const [displayOption, setDisplayOption] = useState(
    optionDisplayType ? optionDisplayType : 'labelOnly'
  )
  const [collapsed, setCollapsed] = useState(!isNewQuestion)

  const displayOptionValue = type => {
    setDisplayOption(type.target.value)
    onFieldChange({ optionDisplayType: type.target.value })
  }

  return (
    <StyledCollapse
      style={style}
      activeKey={collapsed ? [] : ['question-settings']}
      expandIconPosition='right'
      onChange={() => setCollapsed(!collapsed)}
    >
      <Collapse.Panel
        header={
          <React.Fragment>
            <Icon type='setting' />
            {t(
              'components.questionCreation.imageAndLabelSettings.questionSettings'
            )}
          </React.Fragment>
        }
        key='question-settings'
      >
        <Row gutter={24}>
          <Col>
            <FieldLabel
              label={t('components.questionCreation.imageAndLabelSettings.imageDisplayOptions')}
              tooltip={t('tooltips.imageDisplayOptionsHelper')}
              tooltipPlacement="bottomRight"
            >
              <Radio.Group
                size='large'
                buttonStyle='solid'
                onChange={displayOptionValue}
                value={displayOption}
                defaultChecked={displayOption}
              >
                <RadioButton value='labelOnly' disabled={disabled}>
                  {t(
                    'components.questionCreation.imageAndLabelSettings.labelOnly'
                  )}
                </RadioButton>
                <RadioButton value='imageOnly' disabled={disabled}>
                  {t(
                    'components.questionCreation.imageAndLabelSettings.imageOnly'
                  )}
                </RadioButton>
                <RadioButton value='imageAndLabel' disabled={disabled}>
                  {t(
                    'components.questionCreation.imageAndLabelSettings.imageAndLabel'
                  )}
                </RadioButton>
              </Radio.Group>
            </FieldLabel>
          </Col>
        </Row>
      </Collapse.Panel>
    </StyledCollapse>
  )
}

export default withTranslation()(ImageAndLabelSetting)
