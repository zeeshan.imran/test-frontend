import React, { useMemo } from 'react'
import { type, isNil, insert } from 'ramda'
import { Icon, Row, Col, Form } from 'antd'
import Input from '../../../Input'
import Button from '../../../Button'
import { AddOptionButtonContainer, LabelContainer, Label } from './styles'
import { useTranslation } from 'react-i18next'
import HelperIcon from '../../../HelperIcon'
import SliderInput from './SliderInput'

const OptionsSection = ({
  label,
  options = [],
  onChangeOptions,
  errors,
  minOptions = 2,
  maxOptions = null,
  addAnswerLabel
}) => {
  const { t } = useTranslation()

  const { canAdd, canRemove } = useMemo(() => {
    return {
      canAdd: maxOptions && options.length < maxOptions,
      canRemove: options.length > minOptions
    }
  }, [options, minOptions, maxOptions])
  const updatedOptions = (options, event, index, name) => {
    const mainOptions = [
      ...options.slice(0, index),
      {
        ...options[index],
        [name]: event.target.value
      },
      ...options.slice(index + 1)
    ]
    onChangeOptions(mainOptions, event.target.value)
  }
  return (
    <React.Fragment>
      <Row gutter={24}>
        <Col lg={16} md={24}>
          <LabelContainer
            help={type(errors) === 'String' && errors}
            validateStatus={
              type(errors) === 'String' && errors ? 'error' : 'success'
            }
          >
            <Label>{label}</Label>
          </LabelContainer>
        </Col>
      </Row>

      {options.map((option, index) => {
        if (option.isOpenAnswer) {
          return null
        }
        const isStrictMode = option.editMode === 'strict'
        return (
          <React.Fragment key={index}>
            <Row gutter={24}>
              <Col xs={5} xl={4}>
                <Form.Item
                  help={
                    type(errors) === 'Array' && !isNil(errors[index])
                      ? errors[index].label
                      : null
                  }
                  validateStatus={
                    (errors && errors[index] && errors[index].label) ||
                    (errors && type(errors) === 'String')
                      ? 'error'
                      : 'success'
                  }
                >
                  <Input
                    required
                    name={`${index}-label`}
                    size='default'
                    placeholder={t('placeholders.questionLabel')}
                    value={option.label}
                    onChange={(event) => {
                      const updatedOptions = [
                        ...options.slice(0, index),
                        { ...options[index], label: event.target.value },
                        ...options.slice(index + 1)
                      ]
                      onChangeOptions(updatedOptions, event.target.value)
                    }}
                    onBlur={(event) => {
                      if (!option.internalName || !option.tooltip) {
                        const updatedOptions = [
                          ...options.slice(0, index),
                          {
                            ...options[index],
                            internalName: option.internalName
                              ? option.internalName
                              : option.label,
                            tooltip: option.tooltip
                              ? option.tooltip
                              : option.label
                          },
                          ...options.slice(index + 1)
                        ]
                        onChangeOptions(updatedOptions, event.target.value)
                      }
                    }}
                  />
                </Form.Item>
              </Col>
              <SliderInput
                name={`${index}-label-internal-name`}
                size='default'
                placeholder='Label internal name'
                value={option.internalName}
                onChange={(event) => {
                  updatedOptions(options, event, index, 'internalName')
                }}
              />
              <SliderInput
                name={`${index}-label-tooltip`}
                size='default'
                placeholder='Label tooltip'
                value={option.tooltip}
                onChange={(event) => {
                  updatedOptions(options, event, index, 'tooltip')
                }}
              />
              <SliderInput
                name={`${index}-left-label`}
                size='default'
                placeholder='Left Label'
                value={option.leftLabel}
                maxLength={25}
                onChange={(event) => {
                  updatedOptions(options, event, index, 'leftLabel')
                }}
              />
              <SliderInput
                name={`${index}-label-right`}
                size='default'
                placeholder='Right Label'
                value={option.rightLabel}
                maxLength={25}
                onChange={(event) => {
                  updatedOptions(options, event, index, 'rightLabel')
                }}
              />

              {isStrictMode && (
                <Col xs={5} xl={4} flex='auto'>
                  <HelperIcon
                    placement='bottomRight'
                    helperText={t('tooltips.strictOptions')}
                  />
                </Col>
              )}

              {!isStrictMode && canRemove && (
                <Col xs={5} xl={4} flex='auto'>
                  <Button
                    size='default'
                    type='red'
                    onClick={() => {
                      onChangeOptions([
                        ...options.slice(0, index),
                        ...options.slice(index + 1)
                      ])
                    }}
                  >
                    <Icon type='close' />
                  </Button>
                </Col>
              )}
            </Row>
          </React.Fragment>
        )
      })}

      <AddOptionButtonContainer>
        <Row>
          <Col lg={8} md={8}>
            <Button
              disabled={!canAdd}
              onClick={() => {
                onChangeOptions(
                  insert(
                    options.length -
                      (options[options.length - 1] &&
                      options[options.length - 1].isOpenAnswer
                        ? 1
                        : 0),
                    { label: null },
                    options
                  )
                )
              }}
            >
              {addAnswerLabel ||
                t('components.questionCreation.optionsSection.addAnswer')}
            </Button>
          </Col>
        </Row>
      </AddOptionButtonContainer>
    </React.Fragment>
  )
}

export default OptionsSection
