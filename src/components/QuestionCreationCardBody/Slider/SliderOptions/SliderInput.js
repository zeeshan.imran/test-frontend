import React from 'react'
import { Col, Form } from 'antd'
import Input from '../../../Input'

const SliderInput = ({
  name,
  size = 'default',
  placeholder,
  value,
  onChange,
  ...props
}) => {
  return (
    <Col xs={5} xl={4}>
      <Form.Item>
        <Input
          required
          name={name}
          size={size}
          placeholder={placeholder}
          value={value}
          onChange={onChange}
          {...props}
        />
      </Form.Item>
    </Col>
  )
}

export default SliderInput
