import React from 'react'
import { type, isNil } from 'ramda'
import { Icon, Row, Col, Form } from 'antd'
import Input from '../../../Input'
import Button from '../../../Button'
import Checkbox from '../../../Checkbox'

import { AddMarksButtonContainer, LabelContainer, Label } from './styles'
import { useTranslation } from 'react-i18next'

const MarksSection = ({
  label,
  marks = [],
  onChangeMarks,
  errors,
  defaultValue,
  handleMarksPositionChange,
  isInOneRow
}) => {
  const { t } = useTranslation()
  return (
    <React.Fragment>
      <Row gutter={24}>
        <Col lg={16} md={24}>
          <LabelContainer
            help={type(errors) === 'String' && errors}
            validateStatus={
              type(errors) === 'String' && errors ? 'error' : 'success'
            }
          >
            <Label>{label}</Label>
          </LabelContainer>
        </Col>
      </Row>
      {marks.map((option, index) => (
        <React.Fragment key={index}>
          <Row type='flex' align='top' gutter={24}>
            <Col xs={6} lg={5}>
              <Form.Item
                help={
                  type(errors) === 'Array' && !isNil(errors[index])
                    ? errors[index].value
                    : null
                }
                validateStatus={
                  errors && errors[index] && errors[index].value
                    ? 'error'
                    : 'success'
                }
              >
                <Input
                  required
                  name={`${index}-value`}
                  size='default'
                  placeholder={t('placeholders.markPosition')}
                  value={option.value}
                  onChange={event => {
                    const updatedMarks = [
                      ...marks.slice(0, index),
                      { ...marks[index], value: event.target.value },
                      ...marks.slice(index + 1)
                    ]
                    onChangeMarks(updatedMarks, event.target.value)
                  }}
                />
              </Form.Item>
            </Col>
            <Col xs={6} lg={5}>
              <Form.Item
                help={
                  type(errors) === 'Array' && !isNil(errors[index])
                    ? errors[index].label
                    : null
                }
                validateStatus={
                  errors && errors[index] && errors[index].label
                    ? 'error'
                    : 'success'
                }
              >
                <Input
                  required
                  name={`${index}-label`}
                  size='default'
                  placeholder={t('placeholders.MarkLabel')}
                  value={option.label}
                  onChange={event => {
                    const updatedMarks = [
                      ...marks.slice(0, index),
                      { ...marks[index], label: event.target.value },
                      ...marks.slice(index + 1)
                    ]
                    onChangeMarks(updatedMarks)
                  }}
                />
              </Form.Item>
            </Col>
            <Col xs={8} lg={6}>
              <Form.Item
                help={
                  type(errors) === 'Array' && !isNil(errors[index])
                    ? errors[index].isMajorUnit
                    : null
                }
                validateStatus={
                  errors && errors[index] && errors[index].isMajorUnit
                    ? 'error'
                    : 'success'
                }
              >
                <Checkbox
                  name={`${index}-major`}
                  checked={!!option.isMajorUnit}
                  onChange={change => {
                    const updatedMarks = [
                      ...marks.slice(0, index),
                      { ...marks[index], isMajorUnit: !!change },
                      ...marks.slice(index + 1)
                    ]
                    onChangeMarks(updatedMarks)
                  }}
                >
                  {t('components.questionCreation.slider.marks.isMajor')}
                </Checkbox>
              </Form.Item>
            </Col>

            <Col xs={4} lg={4}>
              <Button
                size='default'
                type='red'
                onClick={() => {
                  onChangeMarks([
                    ...marks.slice(0, index),
                    ...marks.slice(index + 1)
                  ])
                }}
              >
                <Icon type='close' />
              </Button>
            </Col>
          </Row>
        </React.Fragment>
      ))}
      <AddMarksButtonContainer>
        <Row gutter={24}>
          <Col lg={5} md={5}>
            <Button
              onClick={() =>
                onChangeMarks([
                  ...marks,
                  { label: '', value: defaultValue || '', isMajorUnit: false }
                ])
              }
            >
              {t('components.questionCreation.slider.marks.add')}
            </Button>
          </Col>
          <Col lg={8} md={8}>
            <Form.Item>
              <Checkbox
                checked={isInOneRow}
                onChange={change => {
                  handleMarksPositionChange(change)
                }}
              >
                {t('components.questionCreation.slider.isInOneRow')}
              </Checkbox>
            </Form.Item>
          </Col>
        </Row>
      </AddMarksButtonContainer>
    </React.Fragment>
  )
}

export default MarksSection
