import styled from 'styled-components'
import { Row, Form } from 'antd'
import Text from '../../../Text'
import { family } from '../../../../utils/Fonts'

export const AddMarksButtonContainer = styled(Row)`
  margin-bottom: 1.5rem;
`

export const Label = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  color: rgba(0, 0, 0, 0.65);
`

export const LabelContainer = styled(Form.Item)`
  margin-bottom: 0;
`
