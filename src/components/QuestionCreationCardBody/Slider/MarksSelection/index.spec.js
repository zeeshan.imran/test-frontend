import React from 'react'
import { mount } from 'enzyme'
import MarksSection from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('MarksSection', () => {
  let testRender
  let label
  let marks
  let onChangeMarks
  let errors
  let defaultValue

  beforeEach(() => {
    label = ''
    marks = [{ label: 'label', value: 1 }]
    onChangeMarks = jest.fn()
    errors = {}
    defaultValue = ''
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render MarksSection', () => {
    testRender = mount(
      <MarksSection
        label={label}
        marks={marks}
        onChangeMarks={onChangeMarks}
        errors={errors}
        defaultValue={defaultValue}
      />
    )
    expect(testRender.find(MarksSection)).toHaveLength(1)

    testRender
      .findWhere(c => c.name() === 'Button')
      .first()
      .simulate('click')

    expect(onChangeMarks).toHaveBeenCalled()
  })
})
