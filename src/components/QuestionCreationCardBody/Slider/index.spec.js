import React from 'react'
import { create, act } from 'react-test-renderer'
import Slider from './index'
import { mount } from 'enzyme'
import Checkbox from '../../Checkbox'
import OptionalSection from '../OptionalSection'
import SliderOptions from './SliderOptions'
import MarksSection from './MarksSelection'
import AlertModal from '../../AlertModal'
import i18n from '../../../utils/internationalization/i18n'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'

// jest.mock('react-i18next', () => ({
//   useTranslation: () => ({ t: text => text })
// }))
// jest.mock('../../../utils/internationalization/i18n', () => ({
//   t: text => text
// }))

// jest.doMock('./index.js')
// jest.mock('rc-util/lib/Portal')
jest.doMock('./MarksSelection')
jest.mock('../../AlertModal')
const handleFieldChange = jest.fn()
const mockAlertModal = jest.fn()
const client = createApolloMockClient()

// describe(`Test QuestionCreationCardBody's Slider`, () => {
//   test('TODO: update this test', () => {})
describe(`add sliders and marks the QuestionCreationCardBody's Slider`, () => {
  let testRender

  AlertModal.mockImplementation(({ handleOk }) => {
    mockAlertModal()
    handleOk()
  })

  let mockHandleFieldChange = jest.fn()
  beforeEach(async () => {
    act(() => {
      testRender = create(
        <ApolloProvider client={client}>
          <Slider handleFieldChange={mockHandleFieldChange} />
        </ApolloProvider>
      )
    })
  })

  afterEach(() => {
    act(() => {
      testRender.unmount()
    })
  })

  test('add marks', () => {
    const addMarkButton = testRender.root.findByProps({
      children: i18n.t('components.questionCreation.slider.marks.add')
    })
    act(() => {
      addMarkButton.props.onClick()
    })
    expect(mockHandleFieldChange).toBeCalledWith({
      range: {
        marks: [
          {
            label: '',
            isMajorUnit: false,
            value: ''
          }
        ]
      }
    })
  })
  // test('add sliders', () => {
  //   const addSliderButton = testRender.root.findByProps({
  //     children: i18n.t(
  //       'components.questionCreation.optionsSection.addAnswer'
  //     )
  //   })
  //   act(() => {
  //     addSliderButton.props.onClick()
  //   })
  //   expect(mockHandleFieldChange).toBeCalledWith({
  //     sliderOptions: {
  //       sliders: [
  //         {
  //           label: null
  //         },
  //         {
  //           label: null
  //         }
  //       ]
  //     }
  //   })
  // })
})

describe('edit prompts, marks, and sliders', () => {
  let testRender
  let enzymeTestRender

  let data = {
    prompt: 'this is the prompt',
    secondaryPrompt: 'this is the secondary prompt',
    range: {
      min: 0.1,
      max: 50.6,
      step: 0.6,
      marks: [
        {
          value: 0,
          label: 'start',
          isMajorUnit: true
        },
        {
          value: 50,
          label: 'half way',
          isMajorUnit: false
        }
      ]
    },
    sliderOptions: {
      sliders: [
        {
          label: 'label 1'
        },
        {
          label: 'label 2'
        }
      ],
      isLastInLoop: ''
    }
  }

  let mockHandleFieldChange = jest.fn(toChange => {
    data = {
      ...data,
      ...toChange
    }
  })

  const update = () => {
    act(() => {
      testRender.update(
        <ApolloProvider client={client}>
          <Slider handleFieldChange={mockHandleFieldChange} {...data} />
        </ApolloProvider>
      )
    })
  }

  beforeEach(async () => {
    act(() => {
      testRender = create(
        <ApolloProvider client={client}>
          <Slider handleFieldChange={mockHandleFieldChange} {...data} />
        </ApolloProvider>
      )
    })
  })

  afterEach(() => {
    update()
    if (enzymeTestRender) enzymeTestRender.unmount()
  })

  test(`edit prompt and secondary prompt`, () => {
    const promptInput = testRender.root.findByProps({ name: 'prompt' })
    const secondaryPromptInput = testRender.root.findByProps({
      name: 'secondaryPrompt'
    })
    act(() => {
      promptInput.props.onChange({
        target: { value: 'this is the new prompt' }
      })
    })
    act(() => {
      secondaryPromptInput.props.onChange({
        target: { value: 'this is the new secondary prompt' }
      })
    })
    update()
    expect(promptInput.props.value).toBe('this is the new prompt')
    expect(secondaryPromptInput.props.value).toBe(
      'this is the new secondary prompt'
    )
  })

  test('set range', () => {
    const rangeMin = testRender.root.findByProps({ name: 'min' })
    const rangeStep = testRender.root.findByProps({ name: 'step' })
    const rangeMax = testRender.root.findByProps({ name: 'max' })

    act(() => {
      rangeMax.props.onChange({ target: { value: 100.5 } })
    })
    update()
    act(() => {
      rangeStep.props.onChange({ target: { value: 3 } })
    })
    update()
    act(() => {
      rangeMin.props.onChange({ target: { value: 0.4 } })
    })
    update()

    expect(rangeMax.props.value).toBe(100.5)
    expect(rangeStep.props.value).toBe(3)
    expect(rangeMin.props.value).toBe(0.4)
  })

  test('set marks', () => {
    const markPosition = testRender.root.findByProps({
      name: '1-value',
      placeholder: i18n.t('placeholders.markPosition')
    })
    const markName = testRender.root.findByProps({
      name: '1-label',
      placeholder: i18n.t('placeholders.MarkLabel')
    })
    const markCheck = testRender.root.findByProps({ name: '1-major' })

    act(() => {
      markPosition.props.onChange({ target: { value: 50 } })
    })
    update()
    act(() => {
      markName.props.onChange({ target: { value: 'new start' } })
    })
    update()
    act(() => {
      markCheck.props.onChange(false)
    })
    update()
    expect(markPosition.props.value).toBe(50)
    expect(markName.props.value).toBe('new start')
    expect(markCheck.props.checked).toBe(false)
  })

  test('set sliders', () => {
    const SliderName = testRender.root.findByProps({
      placeholder: i18n.t('placeholders.questionLabel'),
      name: `1-label`
    })
    act(() => {
      SliderName.props.onChange({ target: { value: 'new slider label 1' } })
    })
    update()
    expect(SliderName.props.value).toBe('new slider label 1')
  })

  test('should render Paired question OptionSelection', async () => {
    enzymeTestRender = mount(
      <ApolloProvider client={client}>
        <Slider
          label={'Show profile charts after this step'}
          onClickCheckbox={OptionalSection}
          isExpanded={false}
          sliderOptions={{}}
          chartType={'spider'}
          handleFieldChange={handleFieldChange}
          profilePrompt={''}
        />
      </ApolloProvider>
    )
    act(() => {
      enzymeTestRender
        .find(Checkbox)
        .last()
        .prop('onChange')({ isValid: true, value: 1 })
    })

    expect(handleFieldChange).toHaveBeenCalled()
  })

  test('should render ChartSection select onchange event', async () => {
    enzymeTestRender = mount(
      <ApolloProvider client={client}>
        <Slider
          validationSchema={''}
          secondaryPrompt={''}
          handleFieldChange={handleFieldChange}
          options={[]}
          skipToOptions={null}
        />
      </ApolloProvider>
    )

    enzymeTestRender
      .findWhere(c => c.name() === 'ChartSection')
      .first()
      .prop('onFieldChange')('chartTopic', 'spider')

    expect(handleFieldChange).toHaveBeenCalledWith({
      chartTopic: 'spider'
    })
  })

  test('should edit the profile prompt', () => {
    enzymeTestRender = mount(
      <ApolloProvider client={client}>
        <Slider
          validationSchema={''}
          secondaryPrompt={''}
          handleFieldChange={handleFieldChange}
          options={[]}
          skipToOptions={null}
          sliderOptions={{
            sliders: [],
            hasFollowUpProfile: true,
            profilePrompt: 'oldValue'
          }}
        />
      </ApolloProvider>
    )
    handleFieldChange.mockReset()
    enzymeTestRender.update()
    enzymeTestRender
      .find({ name: 'profilePrompt' })
      .first()
      .prop('onChange')({ target: { value: 'newValue' } })

    expect(handleFieldChange).toHaveBeenCalledWith({
      sliderOptions: {
        hasFollowUpProfile: true,
        profilePrompt: 'newValue',
        sliders: []
      }
    })
  })

  test('change slider', () => {
    enzymeTestRender = mount(
      <ApolloProvider client={client}>
        <Slider
          validationSchema={''}
          secondaryPrompt={''}
          handleFieldChange={handleFieldChange}
          options={[]}
          skipToOptions={null}
          sliderOptions={{
            sliders: [{ label: '1' }, { label: '2' }, { label: '3' }]
          }}
        />
      </ApolloProvider>
    )
    mockAlertModal.mockReset()
    handleFieldChange.mockReset()
    enzymeTestRender
      .find(SliderOptions)
      .first()
      .prop('onChangeOptions')([{ label: '1' }, { label: '2' }])
    enzymeTestRender.update()
    expect(mockAlertModal).toHaveBeenCalled()
    expect(handleFieldChange).toHaveBeenCalledWith({
      sliderOptions: {
        sliders: [{ label: '1' }, { label: '2' }]
      }
    })
  })

  test('change marks', () => {
    enzymeTestRender = mount(
      <ApolloProvider client={client}>
        <Slider
          validationSchema={''}
          secondaryPrompt={''}
          handleFieldChange={handleFieldChange}
          options={[]}
          skipToOptions={null}
          range={{
            marks: [{ label: '1', value: '1' }, { label: '2', value: '2' }]
          }}
        />
      </ApolloProvider>
    )
    mockAlertModal.mockReset()
    handleFieldChange.mockReset()
    enzymeTestRender
      .find(MarksSection)
      .first()
      .prop('onChangeMarks')([{ value: '1', label: '1' }])
    enzymeTestRender.update()
    expect(mockAlertModal).toHaveBeenCalled()
    expect(handleFieldChange).toHaveBeenCalledWith({
      range: {
        marks: [{ label: '1', value: '1' }]
      }
    })
  })
})
