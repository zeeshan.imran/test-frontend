import React, { useMemo } from 'react'
import { Formik } from 'formik'
import { Form, Row, Col } from 'antd'
import Input from '../../Input'
import SliderOptions from './SliderOptions'
import MarksSection from './MarksSelection'
import OptionalSection from '../OptionalSection'
import ChartSection from '../ChartSection'
import useQuestionValidate from '../../../hooks/useQuestionValidate'
import AlertModal from '../../AlertModal'
import InputArea from '../../InputArea'
import { useTranslation } from 'react-i18next'
import useValidateWithContext from '../../../hooks/useValidateWithContext'
import QuestionInternalName from '../QuestionInternalName'
import DaysAndTimePicker from '../../DaysAndTimePicker'
import UploadSurveyCoverPicture from '../../../containers/UploadSurveyCoverPicture'

const Slider = ({
  id,
  validationSchema,
  chartTypes = [],
  prompt,
  secondaryPrompt,
  range = {},
  sliderOptions = {},
  handleFieldChange,
  chartTitle = '',
  chartTopic = '',
  chartType,
  showOnShared = true,
  displayOn,
  internalPrompt = '',
  showInPreferedLanguage,
  addDelayToSelectNextProductAndNextQuestion = false,
  delayToNextQuestion = '',
  extraDelayToNextQuestion = '',
  image,
  hasImage
}) => {
  const { t } = useTranslation()
  const formRef = useQuestionValidate()
  const isStrictedEdit = window.location.pathname.includes('/stricted-edit/')

  const availableChartTypes = useMemo(() => {
    let items = [...chartTypes]

    if (range.min < 0 || range.max < 0) {
      items = items.filter(chartType => chartType !== 'column' && chartType !== 'bar')
    }

    if (sliderOptions.sliders && sliderOptions.sliders.length < 3) {
      items = items.filter(chartType => chartType !== 'spider')
    }

    return items
  }, [chartTypes, range, sliderOptions])

  const validate = useValidateWithContext(
    validationSchema,
    () => ({ availableChartTypes }),
    [availableChartTypes]
  )

  if (sliderOptions && !sliderOptions.sliders) {
    handleFieldChange({
      sliderOptions: {
        ...sliderOptions,
        sliders: [
          {
            label: null,
            internalName: null
          }
        ]
      }
    })
  }

  return (
    <Formik
      ref={formRef}
      enableReinitialize
      initialValues={{
        displayOn,
        prompt,
        secondaryPrompt,
        range,
        chartTitle,
        chartTopic,
        chartTypes,
        chartType,
        showOnShared,
        internalPrompt,
        delayToNextQuestion,
        extraDelayToNextQuestion,
        image,
        sliderOptions:
          sliderOptions && sliderOptions.sliders
            ? sliderOptions
            : { sliders: [{ label: null, internalName: null }] }
      }}
      validate={validate}
      render={({ errors, setFieldValue, values }) => {
        const { prompt: promptError } = errors

        const deleteAlert = (change, key, title) => {
          AlertModal({
            title: title,
            description: t(
              'components.questionCreation.slider.alert.description'
            ),
            okText: t('components.questionCreation.slider.alert.okText'),
            handleOk: () => {
              handleFieldChange(change)
              setFieldValue(key, change[key])
            },
            handleCancel: () => {}
          })
        }
        const handleSlidersChange = updatedSliders => {
          const change = {
            sliderOptions: {
              ...sliderOptions,
              sliders: updatedSliders
            }
          }
          if (
            sliderOptions.sliders &&
            updatedSliders &&
            updatedSliders.length < sliderOptions.sliders.length
          ) {
            deleteAlert(
              change,
              'sliderOptions',
              t('components.questionCreation.slider.alert.deleteSlider')
            )
          } else {
            setFieldValue('sliderOptions', change.sliderOptions)
            handleFieldChange(change)
          }
        }
        const handleMarksChange = updatedMarks => {
          const change = {
            range: { ...range, marks: updatedMarks }
          }
          if (
            updatedMarks &&
            range.marks &&
            updatedMarks.length < range.marks.length
          ) {
            deleteAlert(
              change,
              'range',
              t('components.questionCreation.slider.alert.deleteMark')
            )
          } else {
            setFieldValue('range', change.range)
            handleFieldChange(change)
          }
        }

        const handleMarksPositionChange = isInOneRow => {
          const change = {
            range: { ...range, isInOneRow: isInOneRow }
          }
          setFieldValue('range', change.range)
          handleFieldChange(change)
        }

        return (
          <React.Fragment>
            <Row gutter={24}>
              <Col lg={16} md={24}>
                <Row gutter={24}>
                  <Form.Item
                    help={promptError}
                    validateStatus={promptError ? 'error' : 'success'}
                  >
                    <InputArea
                      autoSize
                      name='prompt'
                      value={values.prompt}
                      onChange={event => {
                        setFieldValue('prompt', event.target.value)
                        handleFieldChange({ prompt: event.target.value })
                      }}
                      label={t('components.questionCreation.slider.prompt')}
                      tooltip={t('tooltips.questionCreation.prompt')}
                      size='default'
                      required
                    />
                  </Form.Item>
                </Row>
                <Row gutter={24}>
                  <Form.Item
                    help={errors.secondaryPrompt}
                    validateStatus={
                      errors.secondaryPrompt ? 'error' : 'success'
                    }
                  >
                    <InputArea
                      autoSize
                      name='secondaryPrompt'
                      value={values.secondaryPrompt}
                      onChange={event => {
                        setFieldValue('secondaryPrompt', event.target.value)
                        handleFieldChange({
                          secondaryPrompt: event.target.value
                        })
                      }}
                      label={t(
                        'components.questionCreation.slider.secondaryPrompt'
                      )}
                      tooltip={t('tooltips.questionCreation.secondaryPrompt')}
                      size='default'
                    />
                  </Form.Item>
                </Row>
              </Col>
              {hasImage && (
                <Col lg={8} md={8}>
                  <UploadSurveyCoverPicture
                    label={t('components.questionCreation.image')}
                    onChange={imageUrl => {
                      setFieldValue('image', imageUrl[0])
                      handleFieldChange({
                        image: imageUrl[0]
                      })
                    }}
                    value={values.image}
                  />
                </Col>
              )}
            </Row>
            {showInPreferedLanguage && (
              <QuestionInternalName
                setFieldValue={setFieldValue}
                handleFieldChange={handleFieldChange}
                internalPrompt={values.internalPrompt}
              />
            )}
            {/* Add delay for next question selection */}
            {addDelayToSelectNextProductAndNextQuestion && (
              <Row>
                <Form.Item>
                  <DaysAndTimePicker
                    delayToNext={values.delayToNextQuestion}
                    extraDelayToNext={values.extraDelayToNextQuestion}
                    onChangeDelayToNext={value => {
                      setFieldValue('delayToNextQuestion', value)
                      handleFieldChange({ delayToNextQuestion: value })
                    }}
                    onChangeExtraDelayToNext={value => {
                      setFieldValue('extraDelayToNextQuestion', value)
                      handleFieldChange({ extraDelayToNextQuestion: value })
                    }}
                    buttonText='Add delay to select next question'
                  />
                </Form.Item>
              </Row>
            )}
            <Row gutter={24}>
              <Col xs={8} lg={5} md={8}>
                <Form.Item
                  help={errors.range && errors.range.min}
                  validateStatus={
                    errors.range && errors.range.min ? 'error' : 'success'
                  }
                >
                  <Input
                    name='min'
                    value={values.range.min}
                    disabled={id && isStrictedEdit}
                    onChange={event => {
                      setFieldValue('range', {
                        ...range,
                        min: event.target.value
                      })
                      handleFieldChange({
                        range: { ...range, min: event.target.value }
                      })
                    }}
                    label={t('components.questionCreation.slider.min')}
                    size='default'
                    required
                  />
                </Form.Item>
              </Col>
              <Col xs={8} lg={5} md={8}>
                <Form.Item
                  help={errors.range && errors.range.step}
                  validateStatus={
                    errors.range && errors.range.step ? 'error' : 'success'
                  }
                >
                  <Input
                    name='step'
                    value={values.range.step}
                    disabled={id && isStrictedEdit}
                    onChange={event => {
                      const newStep = event.target.value
                      setFieldValue('range', {
                        ...range,
                        step: newStep
                      })
                      handleFieldChange({
                        range: { ...range, step: newStep }
                      })
                    }}
                    label={t('components.questionCreation.slider.step')}
                    size='default'
                    required
                  />
                </Form.Item>
              </Col>
              <Col xs={8} lg={6} md={8}>
                <Form.Item
                  help={errors.range && errors.range.max}
                  validateStatus={
                    errors.range && errors.range.max ? 'error' : 'success'
                  }
                >
                  <Input
                    name='max'
                    value={values.range.max}
                    disabled={id && isStrictedEdit}
                    onChange={event => {
                      setFieldValue('range', {
                        ...range,
                        max: event.target.value
                      })
                      handleFieldChange({
                        range: { ...range, max: event.target.value }
                      })
                    }}
                    label={t('components.questionCreation.slider.max')}
                    size='default'
                    required
                  />
                </Form.Item>
              </Col>
            </Row>
            <MarksSection
              label={t('components.questionCreation.slider.addMarks')}
              marks={values.range.marks || []}
              defaultValue={values.range.min}
              onChangeMarks={updatedMarks => {
                handleMarksChange(updatedMarks)
              }}
              errors={errors.range && errors.range.marks}
              handleMarksPositionChange={handleMarksPositionChange}
              isInOneRow={values.range.isInOneRow}
            />
            <SliderOptions
              minOptions={1}
              maxOptions={20}
              label={t('components.questionCreation.slider.addSliders')}
              options={values.sliderOptions && values.sliderOptions.sliders}
              onChangeOptions={updatedSliders => {
                handleSlidersChange(updatedSliders)
              }}
              errors={errors.sliderOptions && errors.sliderOptions.sliders}
              addAnswerLabel={t('components.questionCreation.slider.addSlider')}
            />
            <OptionalSection
              label={t('components.questionCreation.slider.showProfile')}
              onClickCheckbox={value => {
                setFieldValue('sliderOptions', {
                  ...sliderOptions,
                  hasFollowUpProfile: value,
                  profilePrompt:
                    value && sliderOptions ? sliderOptions.profilePrompt : null
                })
                handleFieldChange({
                  sliderOptions: {
                    ...sliderOptions,
                    hasFollowUpProfile: value,
                    profilePrompt:
                      value && sliderOptions
                        ? sliderOptions.profilePrompt
                        : null
                  }
                })
              }}
              isExpanded={
                values.sliderOptions && values.sliderOptions.hasFollowUpProfile
              }
            >
              <Row gutter={24}>
                <Col lg={16} md={24}>
                  <Form.Item
                    help={
                      errors.sliderOptions && errors.sliderOptions.profilePrompt
                    }
                    validateStatus={
                      errors.sliderOptions && errors.sliderOptions.profilePrompt
                        ? 'error'
                        : 'success'
                    }
                  >
                    <Input
                      name='profilePrompt'
                      value={
                        values.sliderOptions &&
                        values.sliderOptions.profilePrompt
                      }
                      onChange={event => {
                        const profilePrompt = event.target.value
                        setFieldValue('sliderOptions', {
                          ...sliderOptions,
                          profilePrompt: profilePrompt
                        })
                        handleFieldChange({
                          sliderOptions: {
                            ...sliderOptions,
                            profilePrompt: profilePrompt
                          }
                        })
                      }}
                      label={t(
                        'components.questionCreation.slider.profileStepTitle'
                      )}
                      size='default'
                      required
                    />
                  </Form.Item>
                </Col>
              </Row>
            </OptionalSection>
            <ChartSection
              questionType="slider"
              isNewQuestion={!id}
              chartTypes={availableChartTypes}
              values={values}
              errors={errors}
              onFieldChange={(name, value) => {
                setFieldValue('' + name, value)
                handleFieldChange({ [name]: value })
              }}
            />
          </React.Fragment>
        )
      }}
    />
  )
}

export default Slider
