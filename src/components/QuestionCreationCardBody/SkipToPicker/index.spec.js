import React from 'react'
import { mount } from 'enzyme'
import SkipToPicker from '.'
import Select from '../../Select'
import { RemoveSkipButton } from '../OptionsSection/styles'
import wait from '../../../utils/testUtils/waait'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('SkipToPicker', () => {
  let testRender
  let value
  let options
  let onSkipRemove
  let onSkipChange

  beforeEach(() => {
    value = 'one'
    options = [
      { label: 'one', value: 'one' },
      { label: 'two', value: 'two' }
    ]
    onSkipRemove = jest.fn()
    onSkipChange = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SkipToPicker', async () => {
    testRender = mount(
      <SkipToPicker
        value={value}
        options={options}
        onSkipRemove={onSkipRemove}
        onSkipChange={onSkipChange}
      />
    )

    testRender
      .find(Select)
      .first()
      .prop('onDropdownVisibleChange')({ target: { value: true } })

    await wait(0)
    testRender.update()

    testRender
      .find(RemoveSkipButton)
      .first()
      .simulate('click')

    testRender
      .find(RemoveSkipButton)
      .simulate('mouseDown', { preventDefault: () => true })
  })
})
