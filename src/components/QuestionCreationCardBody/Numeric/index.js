import React from 'react'
import { Formik } from 'formik'
import { Form, Row, Col } from 'antd'
import { withTranslation } from 'react-i18next'
import Input from '../../Input'
import useQuestionValidate from '../../../hooks/useQuestionValidate'
import Select from '../../Select'
import FieldLabel from '../../FieldLabel'
import ChartSection from '../ChartSection'
import { NumberSample } from './styles'
import useValidateWithContext from '../../../hooks/useValidateWithContext'
import QuestionInternalName from '../QuestionInternalName'
import DaysAndTimePicker from '../../DaysAndTimePicker'
import UploadSurveyCoverPicture from '../../../containers/UploadSurveyCoverPicture'

const isValidNumber = str => !isNaN(parseFloat(str))

const NumericForm = ({
  id,
  validationSchema,
  handleFieldChange,
  prompt,
  numericOptions,
  secondaryPrompt,
  chartTypes,
  chartTitle,
  chartTopic,
  chartType,
  showOnShared = true,
  t,
  hasDemographic,
  internalPrompt = '',
  showInPreferedLanguage,
  addDelayToSelectNextProductAndNextQuestion = false,
  delayToNextQuestion = '',
  extraDelayToNextQuestion = '',
  image,
  hasImage
}) => {
  const isDisabled = !!hasDemographic
  const formRef = useQuestionValidate()
  const validate = useValidateWithContext(
    validationSchema,
    () => ({ chartTypes }),
    [chartTypes]
  )

  return (
    <Formik
      ref={formRef}
      enableReinitialize
      validate={validate}
      initialValues={{
        prompt,
        secondaryPrompt,
        numericOptions,
        chartTitle,
        chartTopic,
        chartType,
        showOnShared,
        internalPrompt,
        delayToNextQuestion,
        extraDelayToNextQuestion,
        image
      }}
      render={({ errors, setFieldValue, values }) => {
        const { prompt: promptError } = errors
        const decimalNumbers = values.numericOptions
          ? values.numericOptions.decimalNumbers
          : 0
        const factor = Math.pow(10, decimalNumbers)

        return (
          <React.Fragment>
            <Row gutter={24}>
              <Col lg={16} md={24}>
                <Row gutter={24}>
                  <Form.Item
                    help={promptError}
                    validateStatus={promptError ? 'error' : 'success'}
                  >
                    <Input
                      name='prompt'
                      value={values.prompt}
                      onChange={event => {
                        setFieldValue('prompt', event.target.value)
                        handleFieldChange({ prompt: event.target.value })
                      }}
                      label={t('components.questionCreation.prompt')}
                      tooltip={t('tooltips.questionCreation.prompt')}
                      size='default'
                      required
                    />
                  </Form.Item>
                </Row>
                <Row gutter={24}>
                  <Form.Item
                    help={errors.secondaryPrompt}
                    validateStatus={
                      errors.secondaryPrompt ? 'error' : 'success'
                    }
                  >
                    <Input
                      name='secondaryPrompt'
                      value={values.secondaryPrompt}
                      onChange={event => {
                        setFieldValue('secondaryPrompt', event.target.value)
                        handleFieldChange({
                          secondaryPrompt: event.target.value
                        })
                      }}
                      label={t('components.questionCreation.secondaryPrompt')}
                      tooltip={t('tooltips.questionCreation.secondaryPrompt')}
                      size='default'
                    />
                  </Form.Item>
                </Row>
              </Col>
              {hasImage && (
                <Col lg={8} md={8}>
                  <UploadSurveyCoverPicture
                    label={t('components.questionCreation.image')}
                    onChange={imageUrl => {
                      setFieldValue('image', imageUrl[0])
                      handleFieldChange({
                        image: imageUrl[0]
                      })
                    }}
                    value={values.image}
                  />
                </Col>
              )}
            </Row>
            {showInPreferedLanguage && (
              <QuestionInternalName
                setFieldValue={setFieldValue}
                handleFieldChange={handleFieldChange}
                internalPrompt={values.internalPrompt}
              />
            )}
            {/* Add delay for next question selection */}
            {addDelayToSelectNextProductAndNextQuestion && (
              <Row>
                <Form.Item>
                  <DaysAndTimePicker
                    delayToNext={values.delayToNextQuestion}
                    extraDelayToNext={values.extraDelayToNextQuestion}
                    onChangeDelayToNext={value => {
                      setFieldValue('delayToNextQuestion', value)
                      handleFieldChange({ delayToNextQuestion: value })
                    }}
                    onChangeExtraDelayToNext={value => {
                      setFieldValue('extraDelayToNextQuestion', value)
                      handleFieldChange({ extraDelayToNextQuestion: value })
                    }}
                    buttonText='Add delay to select next question'
                  />
                </Form.Item>
              </Row>
            )}
            <Row gutter={24}>
              <Col lg={8} md={12}>
                <Form.Item
                  help={errors.numericOptions ? errors.numericOptions.min : ''}
                  validateStatus={
                    errors.numericOptions && errors.numericOptions.min
                      ? 'error'
                      : 'success'
                  }
                >
                  <Input
                    disabled={isDisabled}
                    name='numericOptions.min'
                    type='number'
                    value={
                      values.numericOptions &&
                      isValidNumber(values.numericOptions.min)
                        ? values.numericOptions.min.toFixed(decimalNumbers || 0)
                        : ''
                    }
                    onChange={event => {
                      setFieldValue(
                        'numericOptions.min',
                        parseFloat(event.target.value)
                      )
                      handleFieldChange({
                        numericOptions: {
                          ...numericOptions,
                          min: parseFloat(event.target.value)
                        }
                      })
                    }}
                    label={t(
                      'components.questionCreation.numeric.labels.minimum'
                    )}
                    size='default'
                  />
                </Form.Item>
              </Col>

              <Col lg={8} md={12}>
                <Form.Item
                  help={errors.numericOptions ? errors.numericOptions.max : ''}
                  validateStatus={
                    errors.numericOptions && errors.numericOptions.max
                      ? 'error'
                      : 'success'
                  }
                >
                  <Input
                    disabled={isDisabled}
                    name='numericOptions.max'
                    type='number'
                    value={
                      values.numericOptions &&
                      isValidNumber(values.numericOptions.max)
                        ? values.numericOptions.max.toFixed(decimalNumbers || 0)
                        : ''
                    }
                    onChange={event => {
                      setFieldValue(
                        'numericOptions.max',
                        parseFloat(event.target.value)
                      )
                      handleFieldChange({
                        numericOptions: {
                          ...numericOptions,
                          max: parseFloat(event.target.value)
                        }
                      })
                    }}
                    label={t(
                      'components.questionCreation.numeric.labels.maximum'
                    )}
                    size='default'
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col lg={8} md={12}>
                <Form.Item
                  help={
                    errors.numericOptions
                      ? errors.numericOptions.decimalNumbers
                      : ''
                  }
                  validateStatus={
                    errors.numericOptions &&
                    errors.numericOptions.decimalNumbers
                      ? 'error'
                      : 'success'
                  }
                >
                  <Select
                    disabled={isDisabled}
                    size='default'
                    label={t(
                      'components.questionCreation.numeric.labels.numberFormat'
                    )}
                    name='options.decimalNumbers'
                    getOptionValue={({ value }) => value}
                    getOptionName={({ name }) => name}
                    options={[
                      {
                        name: 'Integer',
                        value: 0
                      },
                      {
                        name: 'Float (0.#)',
                        value: 1
                      },
                      {
                        name: 'Float (0.##)',
                        value: 2
                      },
                      {
                        name: 'Float (0.###)',
                        value: 3
                      }
                    ]}
                    value={decimalNumbers}
                    onChange={value => {
                      setFieldValue('numericOptions.decimalNumbers', value)
                      handleFieldChange({
                        numericOptions: {
                          ...numericOptions,
                          decimalNumbers: value
                        }
                      })
                    }}
                  />
                </Form.Item>
              </Col>
              <Col lg={8} md={12}>
                <FieldLabel
                  label={t(
                    'components.questionCreation.numeric.labels.sampleOfNumber'
                  )}
                >
                  <NumberSample>
                    {Math.round(100.123 * factor) / factor}
                  </NumberSample>
                </FieldLabel>
              </Col>
            </Row>
            <ChartSection
              isNewQuestion={!id}
              chartTypes={chartTypes}
              values={values}
              errors={errors}
              onFieldChange={(name, value) => {
                setFieldValue(name, value)
                handleFieldChange({ [name]: value })
              }}
            />
          </React.Fragment>
        )
      }}
    />
  )
}

export default withTranslation()(NumericForm)
