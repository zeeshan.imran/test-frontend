import { path } from 'ramda'
import React, { useState } from 'react'
import { Formik } from 'formik'
import { Form, Row, Col } from 'antd'
import { useQuery } from 'react-apollo-hooks'
import { LabelContainer, Label, AddOptionButtonContainer } from './styles'
import gql from 'graphql-tag'
import Button from '../../Button'
import SkipToPicker from '../SkipToPicker'
import useQuestionValidate from '../../../hooks/useQuestionValidate'
import Input from '../../Input'
import InputArea from '../../InputArea'
import { useTranslation } from 'react-i18next'
import useValidateWithContext from '../../../hooks/useValidateWithContext'
import skipFlowUtils from '../../../utils/skipFlowUtils'
import useSkipToOptions from '../../../hooks/useSkipToOptions'
import InputRichText from '../../InputRichText'
import QuestionInternalName from '../QuestionInternalName'

const getMinimumProducts = path(['chooseProductOptions', 'minimumProducts'])
const getMaximumProducts = path(['chooseProductOptions', 'maximumProducts'])
const isRuleOfProduct = productId => rule => rule.productId === productId

const ChooseProduct = ({
  id,
  clientGeneratedId,
  validationSchema,
  prompt,
  secondaryPrompt,
  handleFieldChange,
  chooseProductOptions = {},
  skipFlow,
  productsSkip = [],
  internalPrompt = '',
  showInPreferedLanguage
}) => {
  const { t } = useTranslation()
  const {
    data: {
      surveyCreation: { products }
    }
  } = useQuery(SURVEY_CREATION)
  productsSkip = productsSkip || []
  const formRef = useQuestionValidate()
  const skipToOptions = useSkipToOptions({ id, clientGeneratedId })

  const [isSkipOptions, setIsSkipOptions] = useState(
    !skipFlowUtils.isAllSkipToEmpty(skipFlow)
  )
  const validate = useValidateWithContext(
    validationSchema,
    () => ({ products }),
    [products]
  )

  return (
    <Formik
      ref={formRef}
      enableReinitialize
      validate={validate}
      initialValues={{
        prompt,
        secondaryPrompt,
        chooseProductOptions,
        internalPrompt
      }}
      render={({ errors, setFieldValue, values }) => {
        return (
          <React.Fragment>
            <Row gutter={24}>
              <Col lg={16} md={24}>
                <Form.Item
                  help={errors.prompt}
                  validateStatus={errors.prompt ? 'error' : 'success'}
                >
                  <InputRichText
                    outerAlign='left'
                    name='prompt'
                    label={t('components.questionCreation.prompt')}
                    tooltip={t('tooltips.questionCreation.prompt')}
                    value={values.prompt}
                    onChange={value => {
                      setFieldValue('prompt', value)
                      handleFieldChange({ prompt: value })
                    }}
                    required
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col lg={16} md={24}>
                <Form.Item>
                  <InputArea
                    autoSize
                    name='secondaryPrompt'
                    value={values.secondaryPrompt}
                    onChange={event => {
                      setFieldValue('secondaryPrompt', event.target.value)
                      handleFieldChange({ secondaryPrompt: event.target.value })
                    }}
                    label={t('components.questionCreation.secondaryPrompt')}
                    tooltip={t('tooltips.questionCreation.secondaryPrompt')}
                    size='default'
                  />
                </Form.Item>
              </Col>
            </Row>
            {showInPreferedLanguage && (
              <QuestionInternalName
                setFieldValue={setFieldValue}
                handleFieldChange={handleFieldChange}
                internalPrompt={values.internalPrompt}
              />
            )}
            {products.length > 1 && (
              <Row gutter={24}>
                <Col lg={8} md={12}>
                  <Form.Item
                    help={getMinimumProducts(errors)}
                    validateStatus={
                      getMinimumProducts(errors) ? 'error' : 'success'
                    }
                  >
                    <Input
                      name='chooseProductOptions.minimumProducts'
                      value={getMinimumProducts(values)}
                      onChange={event => {
                        setFieldValue(
                          'chooseProductOptions.minimumProducts',
                          event.target.value
                        )
                        handleFieldChange({
                          chooseProductOptions: {
                            ...values.chooseProductOptions,
                            minimumProducts: event.target.value
                          }
                        })
                      }}
                      label={t(
                        'components.questionCreation.chooseProduct.minProductsToTaste'
                      )}
                      tooltip={t(
                        'tooltips.questionCreation.minProductsToTaste',
                        { min: getMinimumProducts(values) }
                      )}
                      size='default'
                    />
                  </Form.Item>
                </Col>
                <Col lg={8} md={12}>
                  <Form.Item
                    help={getMaximumProducts(errors)}
                    validateStatus={
                      getMaximumProducts(errors) ? 'error' : 'success'
                    }
                  >
                    <Input
                      name='chooseProductOptions.maximumProducts'
                      value={getMaximumProducts(values)}
                      onChange={event => {
                        setFieldValue(
                          'chooseProductOptions.maximumProducts',
                          event.target.value
                        )
                        handleFieldChange({
                          chooseProductOptions: {
                            ...values.chooseProductOptions,
                            maximumProducts: event.target.value
                          }
                        })
                      }}
                      label={t(
                        'components.questionCreation.chooseProduct.maxProductsToTaste'
                      )}
                      tooltip={t(
                        'tooltips.questionCreation.maxProductsToTaste',
                        { max: getMaximumProducts(values) }
                      )}
                      size='default'
                    />
                  </Form.Item>
                </Col>
              </Row>
            )}
            {isSkipOptions &&
              products.map(product => {
                const productId = product.id || product.clientGeneratedId
                const skipRule = skipFlowUtils
                  .fromSkipFlow(skipFlow)
                  .findRule(isRuleOfProduct(productId))
                return (
                  <Row gutter={24} key={productId}>
                    <Col xs={2} lg={8}>
                      <LabelContainer>
                        <Label>{product.name}</Label>
                      </LabelContainer>
                    </Col>
                    <Col xs={2} lg={8}>
                      <SkipToPicker
                        key={productId}
                        value={(skipRule && skipRule.skipTo) || null}
                        options={skipToOptions}
                        onSkipChange={skipTo => {
                          const nextSkipFlow = skipFlowUtils
                            .fromSkipFlow(skipFlow)
                            .removeRules(isRuleOfProduct(productId))
                            .addRule({ productId, skipTo })
                            .getNextSkipFlow()

                          setFieldValue('skipFlow', nextSkipFlow)
                          handleFieldChange({ skipFlow: nextSkipFlow })
                        }}
                        onSkipRemove={() => {
                          const nextSkipFlow = skipFlowUtils
                            .fromSkipFlow(skipFlow)
                            .removeRules(isRuleOfProduct(productId))
                            .getNextSkipFlow()

                          setFieldValue('skipFlow', nextSkipFlow)
                          handleFieldChange({ skipFlow: nextSkipFlow })
                        }}
                      />
                    </Col>
                  </Row>
                )
              })}
            <AddOptionButtonContainer>
              <Row>
                <Col lg={8} md={8}>
                  <Button
                    onClick={() => {
                      setIsSkipOptions(!isSkipOptions)
                    }}
                  >
                    {isSkipOptions
                      ? t('components.questionCreation.hideSkipOptions')
                      : t('components.questionCreation.showSkipOptions')}
                  </Button>
                </Col>
              </Row>
            </AddOptionButtonContainer>
          </React.Fragment>
        )
      }}
    />
  )
}

const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      products
    }
  }
`

export default ChooseProduct
