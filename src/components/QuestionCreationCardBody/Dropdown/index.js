import React from 'react'
import { Formik } from 'formik'
import { Row, Col, Form } from 'antd'
import OptionsSection from '../OptionsSection'
import ChartSection from '../ChartSection'
import { getDefaultSelectOptions } from '../../../utils/defaultQuestionValues'
import useQuestionValidate from '../../../hooks/useQuestionValidate'
import InputArea from '../../InputArea'
import { useTranslation } from 'react-i18next'
import { minOptions } from '../../../validates/questions/dropdown'
import useValidateWithContext from '../../../hooks/useValidateWithContext'
import QuestionInternalName from '../QuestionInternalName'
import DaysAndTimePicker from '../../DaysAndTimePicker'
import UploadSurveyCoverPicture from '../../../containers/UploadSurveyCoverPicture'

const Dropdown = ({
  id,
  clientGeneratedId,
  validationSchema,
  chartTypes,
  prompt,
  secondaryPrompt,
  handleFieldChange,
  options = [],
  skipFlow,
  chartTitle = '',
  chartTopic = '',
  chartType = '',
  showOnShared = true,
  compulsorySurvey,
  hasDemographic,
  internalPrompt = '',
  showInPreferedLanguage,
  addDelayToSelectNextProductAndNextQuestion = false,
  delayToNextQuestion = '',
  extraDelayToNextQuestion = '',
  image,
  hasImage
}) => {
  const isDisabled = !!hasDemographic
  const { t } = useTranslation()
  const formRef = useQuestionValidate()
  const validate = useValidateWithContext(
    validationSchema,
    () => ({ chartTypes, compulsorySurvey }),
    [chartTypes, compulsorySurvey]
  )

  if (!options || options.length < minOptions) {
    options = getDefaultSelectOptions(minOptions)
  }

  return (
    <Formik
      ref={formRef}
      enableReinitialize
      validate={validate}
      initialValues={{
        prompt,
        secondaryPrompt,
        options,
        chartTitle,
        chartTopic,
        chartType,
        showOnShared,
        internalPrompt,
        delayToNextQuestion,
        extraDelayToNextQuestion,
        image
      }}
      render={({ errors, setFieldValue, values }) => {
        return (
          <React.Fragment>
            <Row gutter={24}>
              <Col lg={16} md={24}>
                <Row gutter={24}>
                  <Form.Item
                    help={errors.prompt}
                    validateStatus={errors.prompt ? 'error' : 'success'}
                  >
                    <InputArea
                      autoSize
                      name='prompt'
                      value={values.prompt}
                      onChange={event => {
                        setFieldValue('prompt', event.target.value)
                        handleFieldChange({ prompt: event.target.value })
                      }}
                      label={t('components.questionCreation.dropdown.prompt')}
                      tooltip={t('tooltips.questionCreation.prompt')}
                      size='default'
                      required
                    />
                  </Form.Item>
                </Row>
                <Row gutter={24}>
                  <Form.Item>
                    <InputArea
                      autoSize
                      name='secondaryPrompt'
                      value={values.secondaryPrompt}
                      onChange={event => {
                        setFieldValue('secondaryPrompt', event.target.value)
                        handleFieldChange({
                          secondaryPrompt: event.target.value
                        })
                      }}
                      label={t(
                        'components.questionCreation.dropdown.secondaryPrompt'
                      )}
                      tooltip={t('tooltips.questionCreation.secondaryPrompt')}
                      size='default'
                    />
                  </Form.Item>
                </Row>
              </Col>
              {hasImage && (
                <Col lg={8} md={8}>
                  <UploadSurveyCoverPicture
                    label={t('components.questionCreation.image')}
                    onChange={imageUrl => {
                      setFieldValue('image', imageUrl[0])
                      handleFieldChange({
                        image: imageUrl[0]
                      })
                    }}
                    value={values.image}
                  />
                </Col>
              )}
            </Row>
            {showInPreferedLanguage && (
              <QuestionInternalName
                setFieldValue={setFieldValue}
                handleFieldChange={handleFieldChange}
                internalPrompt={values.internalPrompt}
              />
            )}
            {/* Add delay for next question selection */}
            {addDelayToSelectNextProductAndNextQuestion && (
              <Row>
                <Form.Item>
                  <DaysAndTimePicker
                    delayToNext={values.delayToNextQuestion}
                    extraDelayToNext={values.extraDelayToNextQuestion}
                    onChangeDelayToNext={value => {
                      setFieldValue('delayToNextQuestion', value)
                      handleFieldChange({ delayToNextQuestion: value })
                    }}
                    onChangeExtraDelayToNext={value => {
                      setFieldValue('extraDelayToNextQuestion', value)
                      handleFieldChange({ extraDelayToNextQuestion: value })
                    }}
                    buttonText='Add delay to select next question'
                  />
                </Form.Item>
              </Row>
            )}
            <OptionsSection
              disabled={isDisabled}
              id={id}
              clientGeneratedId={clientGeneratedId}
              label={t('components.questionCreation.dropdown.optionsSection')}
              options={values.options}
              onChangeOptions={updatedOptions => {
                setFieldValue('options', updatedOptions)
                handleFieldChange({
                  options: updatedOptions
                })
              }}
              errors={errors.options}
              skipFlow={skipFlow}
              onSkipFlowChange={nextSkipFlow => {
                setFieldValue('skipFlow', nextSkipFlow)
                handleFieldChange({
                  skipFlow: nextSkipFlow
                })
              }}
              compulsorySurvey={compulsorySurvey}
            />
            <ChartSection
              isNewQuestion={!id}
              chartTypes={chartTypes}
              values={values}
              errors={errors}
              onFieldChange={(name, value) => {
                setFieldValue(name, value)
                handleFieldChange({ [name]: value })
              }}
            />
          </React.Fragment>
        )
      }}
    />
  )
}

export default Dropdown
