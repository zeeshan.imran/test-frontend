import React from 'react'
import { mount } from 'enzyme'
import ChartSection from '.'
import { Select, Collapse, Input } from 'antd'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('ChartSection', () => {
  let testRender
  let values
  let errors
  let chartTypes
  let onFieldChange

  beforeEach(() => {
    values = {
      chartType: 'pie',
      chartTitle: 'title',
      chartTopic: 'topic'
    }
    errors = {}
    chartTypes = ['spider', 'pie']
    onFieldChange = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ChartSection', async () => {
    testRender = mount(
      <ChartSection
        isNewQuestion
        values={values}
        errors={errors}
        chartTypes={chartTypes}
        onFieldChange={onFieldChange}
      />
    )
    expect(testRender.find(ChartSection)).toHaveLength(1)

    testRender
      .findWhere(c => c.name() === 'Input' && c.prop('name') === 'chartTopic')
      .first()
      .prop('onChange')({ target: { value: 'chartTopic' } })

    expect(onFieldChange).toHaveBeenCalledWith('chartTopic', 'chartTopic')
  })

  test('should render ChartSection Input of chartTitle', async () => {
    testRender = mount(
      <ChartSection
        isNewQuestion
        values={values}
        errors={errors}
        chartTypes={chartTypes}
        onFieldChange={onFieldChange}
      />
    )
    expect(testRender.find(ChartSection)).toHaveLength(1)

    testRender
      .findWhere(c => c.name() === 'Input' && c.prop('name') === 'chartTitle')
      .first()
      .prop('onChange')({ target: { value: 'chartTitle' } })

    expect(onFieldChange).toHaveBeenCalledWith('chartTitle', 'chartTitle')
  })

  test('should render ChartSection select onchange event', async () => {
    testRender = mount(
      <ChartSection
        isNewQuestion
        values={values}
        errors={errors}
        chartTypes={chartTypes}
        onFieldChange={onFieldChange}
      />
    )
    expect(testRender.find(ChartSection)).toHaveLength(1)

    testRender
      .findWhere(c => c.name() === 'Select')
      .first()
      .prop('onChange')({ key: 'spider' })

    expect(onFieldChange).toHaveBeenCalledWith('chartType', 'spider')
  })

  test('should collapse when the question is not new', async () => {
    testRender = mount(
      <ChartSection
        values={values}
        errors={errors}
        chartTypes={chartTypes}
        onFieldChange={onFieldChange}
      />
    )

    // no inputs, selects since collapsed
    expect(testRender.find(Select)).toHaveLength(0)
    expect(testRender.find(Input)).toHaveLength(0)
    testRender.find(Collapse).prop('onChange')()

    testRender.update()
    expect(testRender.find(Select)).toHaveLength(1)
    expect(testRender.find(Input)).toHaveLength(2)
  })

  test('should expand when the question has errors', async () => {
    testRender = mount(
      <ChartSection
        values={values}
        errors={errors}
        chartTypes={chartTypes}
        onFieldChange={onFieldChange}
      />
    )

    // no inputs, selects since collapsed
    expect(testRender.find(Select)).toHaveLength(0)
    expect(testRender.find(Input)).toHaveLength(0)
    testRender.setProps({
      errors: { chartTitle: 'Error message' }
    })

    testRender.update()
    expect(testRender.find(Select)).toHaveLength(1)
    expect(testRender.find(Input)).toHaveLength(2)
  })
})
