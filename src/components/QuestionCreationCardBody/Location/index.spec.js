import React from 'react'
import { mount } from 'enzyme'
import Location from '.'
import { BrowserRouter as Router } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'

describe('Location', () => {
  let testRender
  let validationSchema
  let prompt
  let secondaryPrompt
  let handleFieldChange
  let chartTitle
  let chartTopic
  let chartType
  let region
  let client
  beforeEach(() => {
    validationSchema = ''
    prompt = ''
    secondaryPrompt = ''
    handleFieldChange = jest.fn()
    chartTitle = ''
    chartTopic = ''
    chartType = ''
    region = ''
    client = createApolloMockClient()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Location', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router>
          <Location
            validationSchema={validationSchema}
            prompt={prompt}
            secondaryPrompt={secondaryPrompt}
            handleFieldChange={handleFieldChange}
            chartTitle={chartTitle}
            chartTopic={chartTopic}
            chartType={chartType}
            region={region}
          />
        </Router>
      </ApolloProvider>
    )
    expect(testRender.find(Location)).toHaveLength(1)

    const regionSelect = testRender
      .findWhere(c => c.name() === 'Select' && c.prop('name') === 'region')
      .first()
    
    regionSelect
      .prop('onChange')({ key: 'EN' })

    expect(handleFieldChange).toHaveBeenCalledWith({
      region: 'EN'
    })

    expect(regionSelect.prop('disabled')).toBe(false)
  })

  test('should disable region selection in restricted mode', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router>
          <Location
            validationSchema={validationSchema}
            prompt={prompt}
            secondaryPrompt={secondaryPrompt}
            handleFieldChange={handleFieldChange}
            chartTitle={chartTitle}
            chartTopic={chartTopic}
            chartType={chartType}
            region={region}
            editMode={'strict'}
          />
        </Router>
      </ApolloProvider>
    )
    const regionSelect = testRender
      .findWhere(c => c.name() === 'Select' && c.prop('name') === 'region')
      .first()
    
    expect(regionSelect.prop('disabled')).toBe(true)
  })
})
