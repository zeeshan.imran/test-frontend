import React, { useCallback } from 'react'
import { Row, Col, Form } from 'antd'
import { Formik } from 'formik'
import { useTranslation } from 'react-i18next'
import ChartSection from '../ChartSection'
import Select from '../../Select'
import useQuestionValidate from '../../../hooks/useQuestionValidate'
import InputArea from '../../InputArea'
import useValidateWithContext from '../../../hooks/useValidateWithContext'
import { locationQuestionCountries } from '../../../utils/Constants'
import HelperIcon from '../../HelperIcon'
import QuestionInternalName from '../QuestionInternalName'
import DaysAndTimePicker from '../../DaysAndTimePicker'
import UploadSurveyCoverPicture from '../../../containers/UploadSurveyCoverPicture'

const LocationQuestion = ({
  id,
  validationSchema,
  chartTypes,
  prompt = '',
  secondaryPrompt = '',
  handleFieldChange,
  chartTitle = '',
  chartTopic = '',
  chartType = '',
  showOnShared = true,
  region = '',
  editMode,
  internalPrompt = '',
  showInPreferedLanguage,
  addDelayToSelectNextProductAndNextQuestion = false,
  delayToNextQuestion = '',
  extraDelayToNextQuestion = '',
  image,
  hasImage
}) => {
  const isStrictMode = editMode === 'strict'
  const { t } = useTranslation()
  const getNameOfRegion = useCallback(
    code =>
      code && t(`chartRegions.${code}`) !== `chartRegions.${code}`
        ? t(`chartRegions.${code}`)
        : '',
    [t]
  )

  const formRef = useQuestionValidate()
  const validate = useValidateWithContext(
    validationSchema,
    () => ({ chartTypes }),
    [chartTypes]
  )

  return (
    <Formik
      ref={formRef}
      enableReinitialize
      validate={validate}
      initialValues={{
        prompt,
        secondaryPrompt,
        chartTitle,
        chartTopic,
        chartType,
        showOnShared,
        region,
        internalPrompt,
        image
      }}
      render={({ errors, setFieldValue, values }) => {
        return (
          <React.Fragment>
            <Row gutter={24}>
              <Col lg={16} md={24}>
                <Row gutter={24}>
                  <Form.Item
                    help={errors.prompt}
                    validateStatus={errors.prompt ? 'error' : 'success'}
                  >
                    <InputArea
                      autoSize
                      name='prompt'
                      value={values.prompt}
                      onChange={event => {
                        setFieldValue('prompt', event.target.value)
                        handleFieldChange({ prompt: event.target.value })
                      }}
                      label={t('components.questionCreation.prompt')}
                      tooltip={t('tooltips.questionCreation.prompt')}
                      size='default'
                      required
                    />
                  </Form.Item>
                </Row>
                <Row gutter={24}>
                  <Form.Item>
                    <InputArea
                      autoSize
                      name='secondaryPrompt'
                      value={values.secondaryPrompt}
                      onChange={event => {
                        setFieldValue('secondaryPrompt', event.target.value)
                        handleFieldChange({
                          secondaryPrompt: event.target.value
                        })
                      }}
                      label={t('components.questionCreation.secondaryPrompt')}
                      tooltip={t('tooltips.questionCreation.secondaryPrompt')}
                      size='default'
                    />
                  </Form.Item>
                </Row>
              </Col>
              {hasImage && (
                <Col lg={8} md={8}>
                  <UploadSurveyCoverPicture
                    label={t('components.questionCreation.image')}
                    onChange={imageUrl => {
                      setFieldValue('image', imageUrl[0])
                      handleFieldChange({
                        image: imageUrl[0]
                      })
                    }}
                    value={values.image}
                  />
                </Col>
              )}
            </Row>
            {showInPreferedLanguage && (
              <QuestionInternalName
                setFieldValue={setFieldValue}
                handleFieldChange={handleFieldChange}
                internalPrompt={values.internalPrompt}
              />
            )}
            {/* Add delay for next question selection */}
            {addDelayToSelectNextProductAndNextQuestion && (
              <Row>
                <Form.Item>
                  <DaysAndTimePicker
                    delayToNext={values.delayToNextQuestion}
                    extraDelayToNext={values.extraDelayToNextQuestion}
                    onChangeDelayToNext={value => {
                      setFieldValue('delayToNextQuestion', value)
                      handleFieldChange({ delayToNextQuestion: value })
                    }}
                    onChangeExtraDelayToNext={value => {
                      setFieldValue('extraDelayToNextQuestion', value)
                      handleFieldChange({ extraDelayToNextQuestion: value })
                    }}
                    buttonText='Add delay to select next question'
                  />
                </Form.Item>
              </Row>
            )}
            <Row gutter={24}>
              <Col lg={8} md={12}>
                <Form.Item
                  help={errors.region}
                  validateStatus={errors.region ? 'error' : 'success'}
                >
                  <Select
                    name='region'
                    value={{
                      key: values.region,
                      label: getNameOfRegion(values.region)
                    }}
                    labelInValue
                    onChange={value => {
                      setFieldValue('region', value.key)
                      handleFieldChange({ region: value.key })
                    }}
                    options={locationQuestionCountries}
                    getOptionName={getNameOfRegion}
                    label={t('components.questionCreation.location.region')}
                    size='default'
                    disabled={isStrictMode}
                    required
                  />
                </Form.Item>
              </Col>
              {isStrictMode && (
                <Col>
                  <HelperIcon
                    placement='bottomRight'
                    style={{ position: 'relative', top: '25px' }}
                    helperText={
                      <span>
                        {t(
                          'components.questionCreation.location.regionRestricted'
                        )}
                      </span>
                    }
                  />
                </Col>
              )}
            </Row>
            <ChartSection
              isNewQuestion={!id}
              chartTypes={chartTypes}
              values={values}
              errors={errors}
              onFieldChange={(name, value) => {
                setFieldValue(name, value)
                handleFieldChange({ [name]: value })
              }}
            />
          </React.Fragment>
        )
      }}
    />
  )
}

export default LocationQuestion
