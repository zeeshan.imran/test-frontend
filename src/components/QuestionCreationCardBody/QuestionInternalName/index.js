import React from 'react'
import { Form, Row, Col } from 'antd'
import InputArea from '../../InputArea'

const QuestionInternalName = ({
  setFieldValue,
  handleFieldChange,
  internalPrompt
}) => (
  <Row gutter={24}>
    <Col lg={16} md={24}>
      <Form.Item>
        <InputArea
          autoSize
          name='internalPrompt'
          value={internalPrompt}
          onChange={event => {
            setFieldValue('internalPrompt', event.target.value)
            handleFieldChange({ internalPrompt: event.target.value })
          }}
          label='Internal text for question'
          tooltip='Internal text for question that are used for reporting purpose.'
          size='default'
        />
      </Form.Item>
    </Col>
  </Row>
)

export default QuestionInternalName
