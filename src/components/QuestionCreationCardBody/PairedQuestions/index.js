import React, { useState } from 'react'
import { useQuery } from 'react-apollo-hooks'
import { remove, findIndex, propEq, sortBy, prop } from 'ramda'
import { Formik } from 'formik'
import { Row, Col, Form } from 'antd'
import TagInput from '../TagInput'
import Select from '../../Select'
import Input from '../../Input'
import Checkbox from '../../Checkbox'
import FieldLabel from '../../FieldLabel'
import { combinations } from '../../../utils/combinations'
import { CheckboxContainer } from './styles'
import OptionalSection from '../OptionalSection'
import ChartSection from '../ChartSection'
import useQuestionValidate from '../../../hooks/useQuestionValidate'
import InputArea from '../../InputArea'
import { withTranslation } from 'react-i18next'
import useValidateWithContext from '../../../hooks/useValidateWithContext'
import useLikingQuestionOptions from '../../../hooks/useLikingQuestionOptions'
import gql from 'graphql-tag'
import {
  getOptionValue,
  getOptionName
} from '../../../utils/getters/OptionGetters'
import HelperIcon from '../../HelperIcon'
import { CHARTS_WITH_LIKING_QUESTION } from '../../../utils/Constants'
import { trimSpaces } from '../../../utils/trimSpaces'
import getBase64 from '../../../utils/getBase64'
import ImageResize from '../../../utils/imageResize'
import ImageAndLabelSetting from '../ImageAndLabelSetting'
import QuestionInternalName from '../QuestionInternalName'
import DaysAndTimePicker from '../../DaysAndTimePicker'
import UploadSurveyCoverPicture from '../../../containers/UploadSurveyCoverPicture'

const sortPairs = (pairs, elementsInPairs = []) => {
  if (elementsInPairs.length) {
    return sortBy(prop('optionId'), elementsInPairs)
  } else {
    return pairs
  }
}

const PairedQuestions = ({
  id,
  validationSchema,
  chartTypes,
  prompt,
  secondaryPrompt,
  handleFieldChange,
  pairsOptions = {},
  pairs = [],
  chartTitle = '',
  chartTopic = '',
  chartType,
  showOnShared = true,
  displayOn,
  optionDisplayType,
  likingQuestion,
  editMode,
  t,
  internalPrompt = '',
  showInPreferedLanguage,
  addDelayToSelectNextProductAndNextQuestion = false,
  delayToNextQuestion = '',
  extraDelayToNextQuestion = '',
  image,
  hasImage
}) => {
  // need to implement pairs and elementsInPairs
  const {
    data: {
      surveyCreation: { products }
    }
  } = useQuery(SURVEY_CREATION)
  const isStrictMode = editMode === 'strict'
  const {
    minPairs = '',
    maxPairs = '',
    customFlavorEnabled = false,
    oneFlavorNotPresentEnabled = false,
    neitherFlavorPresentEnabled = false,
    bothFlavorsPresentEnabled = false,
    hasFollowUpProfile = false,
    elementsInPairs
  } = pairsOptions
  const formRef = useQuestionValidate()
  const validate = useValidateWithContext(
    validationSchema,
    () => ({ chartTypes, products }),
    [chartTypes, products]
  )
  const [isMinTouched, setIsMinTouched] = useState(minPairs !== '')
  const [isMaxTouched, setIsMaxTouched] = useState(maxPairs !== '')
  const likingQuestionOptions = useLikingQuestionOptions()

  return (
    <Formik
      ref={formRef}
      validate={validate}
      enableReinitialize
      initialValues={{
        prompt,
        secondaryPrompt,
        maxPairs: combinations(pairs.length),
        pairs: sortPairs(pairs, elementsInPairs),
        pairsOptions: {
          minPairs,
          maxPairs
        },
        hasFollowUpProfile,
        profilePrompt:
          pairsOptions &&
          pairsOptions.profileQuestion &&
          pairsOptions.profileQuestion.prompt,
        chartTitle,
        chartTopic,
        chartType,
        showOnShared,
        likingQuestion,
        internalPrompt,
        delayToNextQuestion,
        extraDelayToNextQuestion,
        image
      }}
      render={({ errors, setFieldValue, values }) => {
        const handleAddTag = element => {
          if (!trimSpaces(element)) {
            return
          }
          const maxComb = combinations(values.pairs.length + 1)
          if (!isMinTouched) {
            setFieldValue('pairsOptions.minPairs', maxComb)

            handleFieldChange({
              pairsOptions: {
                ...pairsOptions,
                minPairs: maxComb
              }
            })
          }
          if (!isMaxTouched) {
            setFieldValue('pairsOptions.maxPairs', maxComb)

            handleFieldChange({
              pairsOptions: {
                ...pairsOptions,
                maxPairs: maxComb
              }
            })
          }
          const obj = {
            pair: element,
            internalName: element,
            tooltip: element,
            optionId: new Date().getTime()
          }
          handleFieldChange({
            pairsOptions: {
              ...pairsOptions,
              elementsInPairs: [...values.pairs, obj]
            }
          })
          setFieldValue('maxPairs', maxComb)
          setFieldValue('pairs', [...values.pairs, obj])
          handleFieldChange({
            pairs: [...values.pairs, obj]
          })
        }
        const onChangeLikingQuestion = value => {
          setFieldValue('likingQuestion', value)
          handleFieldChange({ likingQuestion: value })
        }
        if (
          values.likingQuestion &&
          findIndex(
            propEq('value', values.likingQuestion),
            likingQuestionOptions
          ) === -1
        ) {
          onChangeLikingQuestion(null)
        }
        const isLabelsValid = () => {
          try {
            validationSchema.validateSyncAt('pairs', values)
            return true
          } catch (e) {
            return false
          }
        }

        const addImageToOption = async (pairs, fileData) => {
          const index = Object.keys(fileData)[0]
          if (fileData[index]) {
            const base64Image = await getBase64(fileData[index])
            const ImageType = base64Image
              .match(/[^:]\w+\/[\w-+\d.]+(?=;|,)/)[0]
              .split('/')
            const ImageTypeQuality =
              ImageType[1].toLowerCase() === 'jpeg' ? 70 : 100
            const image800 = await ImageResize(
              fileData[index],
              800,
              800,
              ImageType[1].toLowerCase(),
              ImageTypeQuality
            )
            pairs[index].image800 = image800
            setFieldValue('pairs', [...pairs])
            handleFieldChange({
              pairs: [...pairs]
            })
          }
        }

        const onRemoveImageUrl = pairId => {
          // const index = Object.keys(fileData)[0]
          const newPairs = pairs.map((pair) => {
            if(pair.pair === pairId) {
              pair.image800 = undefined
            }

            return pair
          })

          const newElementsInPair = elementsInPairs.map((elementsInPair) => {
            if(elementsInPair.pair === pairId) {
              elementsInPair.image800 = undefined
            }

            return elementsInPair
          })

          const newSortPairs = sortPairs(newPairs, newElementsInPair)

          setFieldValue('pairs', [...newSortPairs])
          handleFieldChange({
            pairs: [...newSortPairs]
          })
        }

        const handlePairInternalName = async (pairs, name, value, index) => {
          pairs[index][name] = value
          setFieldValue('pairs', [...pairs])
          handleFieldChange({
            pairs: [...pairs]
          })
        }

        return (
          <React.Fragment>
            <Row gutter={24}>
              <Col lg={16} md={24}>
                <Row gutter={24}>
                  <Form.Item
                    help={errors.prompt}
                    validateStatus={errors.prompt ? 'error' : 'success'}
                  >
                    <InputArea
                      autoSize
                      name='prompt'
                      value={values.prompt}
                      onChange={event => {
                        setFieldValue('prompt', event.target.value)
                        handleFieldChange({ prompt: event.target.value })
                      }}
                      label={t('components.questionCreation.prompt')}
                      tooltip={t('tooltips.questionCreation.prompt')}
                      size='default'
                      required
                    />
                  </Form.Item>
                </Row>
                <Row gutter={24}>
                  <Form.Item
                    help={errors.secondaryPrompt}
                    validateStatus={
                      errors.secondaryPrompt ? 'error' : 'success'
                    }
                  >
                    <InputArea
                      autoSize
                      name='secondaryPrompt'
                      value={values.secondaryPrompt}
                      onChange={event => {
                        setFieldValue('secondaryPrompt', event.target.value)
                        handleFieldChange({
                          secondaryPrompt: event.target.value
                        })
                      }}
                      label={t('components.questionCreation.secondaryPrompt')}
                      tooltip={t('tooltips.questionCreation.secondaryPrompt')}
                      size='default'
                    />
                  </Form.Item>
                </Row>
              </Col>
              {hasImage && (
                <Col lg={8} md={8}>
                  <UploadSurveyCoverPicture
                    label={t('components.questionCreation.image')}
                    onChange={imageUrl => {
                      setFieldValue('image', imageUrl[0])
                      handleFieldChange({
                        image: imageUrl[0]
                      })
                    }}
                    value={values.image}
                  />
                </Col>
              )}
            </Row>
            {showInPreferedLanguage && (
              <QuestionInternalName
                setFieldValue={setFieldValue}
                handleFieldChange={handleFieldChange}
                internalPrompt={values.internalPrompt}
              />
            )}
            {/* Add delay for next question selection */}
            {addDelayToSelectNextProductAndNextQuestion && false && (
              <Row>
                <Form.Item>
                  <DaysAndTimePicker
                    delayToNext={values.delayToNextQuestion}
                    extraDelayToNext={values.extraDelayToNextQuestion}
                    onChangeDelayToNext={value => {
                      setFieldValue('delayToNextQuestion', value)
                      handleFieldChange({ delayToNextQuestion: value })
                    }}
                    onChangeExtraDelayToNext={value => {
                      setFieldValue('extraDelayToNextQuestion', value)
                      handleFieldChange({ extraDelayToNextQuestion: value })
                    }}
                    buttonText='Add delay to select next question'
                  />
                </Form.Item>
              </Row>
            )}
            <Row gutter={24}>
              <Col lg={8} md={12}>
                <Form.Item>
                  <FieldLabel label='Options'>
                    <CheckboxContainer>
                      <Checkbox
                        name='customFlavorEnabled'
                        checked={customFlavorEnabled}
                        onChange={value =>
                          handleFieldChange({
                            pairsOptions: {
                              ...pairsOptions,
                              customFlavorEnabled: value
                            }
                          })
                        }
                      >
                        {t(
                          'components.questionCreation.paired.pairsOptions.customFlavorEnabled'
                        )}
                      </Checkbox>
                    </CheckboxContainer>
                    <CheckboxContainer>
                      <Checkbox
                        name='oneFlavorNotPresentEnabled'
                        checked={oneFlavorNotPresentEnabled}
                        onChange={value =>
                          handleFieldChange({
                            pairsOptions: {
                              ...pairsOptions,
                              oneFlavorNotPresentEnabled: value
                            }
                          })
                        }
                      >
                        {t(
                          'components.questionCreation.paired.pairsOptions.oneFlavorNotPresentEnabled'
                        )}
                      </Checkbox>
                    </CheckboxContainer>
                    <CheckboxContainer>
                      <Checkbox
                        name='neitherFlavorPresentEnabled'
                        checked={neitherFlavorPresentEnabled}
                        onChange={value =>
                          handleFieldChange({
                            pairsOptions: {
                              ...pairsOptions,
                              neitherFlavorPresentEnabled: value
                            }
                          })
                        }
                      >
                        {t(
                          'components.questionCreation.paired.pairsOptions.neitherFlavorPresentEnabled'
                        )}
                      </Checkbox>
                    </CheckboxContainer>
                    <CheckboxContainer>
                      <Checkbox
                        name='bothFlavorsPresentEnabled'
                        checked={bothFlavorsPresentEnabled}
                        onChange={value =>
                          handleFieldChange({
                            pairsOptions: {
                              ...pairsOptions,
                              bothFlavorsPresentEnabled: value
                            }
                          })
                        }
                      >
                        {t(
                          'components.questionCreation.paired.pairsOptions.bothFlavorsPresentEnabled'
                        )}
                      </Checkbox>
                    </CheckboxContainer>
                  </FieldLabel>
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={24}>
              <Col>
                <Form.Item
                  help={
                    errors.pairs && errors.pairs.constructor === Array
                      ? errors.pairs[0]
                      : errors.pairs
                  }
                  validateStatus={errors.pairs ? 'error' : 'success'}
                >
                  <TagInput
                    label={t('components.questionCreation.paired.descriptors')}
                    tags={values.pairs}
                    handleAddTag={handleAddTag}
                    handleRemoveTag={index => {
                      const maxComb = combinations(values.pairs.length - 1)
                      if (!isMinTouched) {
                        setFieldValue('pairsOptions.minPairs', maxComb)
                        handleFieldChange({
                          pairsOptions: {
                            ...pairsOptions,
                            minPairs: maxComb
                          }
                        })
                      }
                      setFieldValue('maxPairs', maxComb)
                      setFieldValue('pairs', remove(index, 1, values.pairs))
                      handleFieldChange({
                        pairsOptions: {
                          ...pairsOptions,
                          elementsInPairs: remove(index, 1, values.pairs)
                        }
                      })
                      handleFieldChange({
                        pairs: remove(index, 1, values.pairs)
                      })
                    }}
                    onAddImageUrl={fileData =>
                      addImageToOption(values.pairs, fileData)
                    }
                    onRemoveImageUrl={onRemoveImageUrl}
                    typeOfQuestion='paired-questions'
                    onChangePairAttribute={(name, value, index) => {
                      handlePairInternalName(values.pairs, name, value, index)
                    }}
                    isStrictMode={isStrictMode}
                  />
                </Form.Item>
              </Col>
            </Row>
            {!errors.pairs && isLabelsValid() && (
              <React.Fragment>
                <Row>
                  <Col lg={8} md={12}>
                    <Form.Item
                      help={errors.pairsOptions && errors.pairsOptions.minPairs}
                      validateStatus={
                        errors.pairsOptions && errors.pairsOptions.minPairs
                          ? 'error'
                          : 'success'
                      }
                    >
                      <Input
                        name='pairsOptions.minPairs'
                        value={
                          values.pairsOptions && values.pairsOptions.minPairs
                        }
                        onChange={event => {
                          const minPairs = event.target.value
                          setFieldValue('pairsOptions.minPairs', minPairs)
                          setIsMinTouched(true)
                          handleFieldChange({
                            pairsOptions: {
                              ...pairsOptions,
                              minPairs
                            }
                          })
                        }}
                        data-testid={'paired-minimum-pairs'}
                        label={t(
                          'components.questionCreation.paired.minimumPairs',
                          { max: values.maxPairs }
                        )}
                        size='default'
                        required
                      />
                    </Form.Item>
                  </Col>
                </Row>
                <Row>
                  <Col lg={8} md={12}>
                    <Form.Item
                      help={errors.pairsOptions && errors.pairsOptions.maxPairs}
                      validateStatus={
                        errors.pairsOptions && errors.pairsOptions.maxPairs
                          ? 'error'
                          : 'success'
                      }
                    >
                      <Input
                        name='pairsOptions.maxPairs'
                        value={
                          values.pairsOptions && values.pairsOptions.maxPairs
                        }
                        onChange={event => {
                          const maxPairs = event.target.value
                          setFieldValue('pairsOptions.maxPairs', maxPairs)
                          setIsMaxTouched(true)
                          handleFieldChange({
                            pairsOptions: {
                              ...pairsOptions,
                              maxPairs
                            }
                          })
                        }}
                        data-testid={'paired-minimum-pairs'}
                        label={t(
                          'components.questionCreation.paired.maximumPairs',
                          {
                            max: values.maxPairs
                          }
                        )}
                        size='default'
                        required
                      />
                    </Form.Item>
                  </Col>
                </Row>
              </React.Fragment>
            )}
            {CHARTS_WITH_LIKING_QUESTION.includes(chartType) && (
              <Row gutter={24}>
                <Col lg={9} md={12}>
                  <Form.Item
                    help={errors.likingQuestion}
                    validateStatus={errors.likingQuestion ? 'error' : 'success'}
                  >
                    <Select
                      name='likingQuestion'
                      size='default'
                      onChange={onChangeLikingQuestion}
                      options={likingQuestionOptions}
                      value={values.likingQuestion}
                      getOptionValue={getOptionValue}
                      getOptionName={getOptionName}
                      disabled={isStrictMode}
                      label={t(
                        'components.questionCreation.paired.likingQuestion'
                      )}
                      required
                    />
                  </Form.Item>
                </Col>
                {isStrictMode && (
                  <Col>
                    <HelperIcon
                      placement='bottomRight'
                      style={{ position: 'relative', top: '32px' }}
                      helperText={t('tooltips.likingQuestionRestricted')}
                    />
                  </Col>
                )}
              </Row>
            )}

            <OptionalSection
              label={t('components.questionCreation.paired.hasFollowUpProfile')}
              onClickCheckbox={value => {
                setFieldValue('hasFollowUpProfile', value)
                handleFieldChange({
                  pairsOptions: {
                    ...pairsOptions,
                    hasFollowUpProfile: value
                  }
                })
              }}
              isExpanded={hasFollowUpProfile}
            >
              <Row gutter={24}>
                <Col lg={16} md={24}>
                  <Form.Item
                    help={errors.profilePrompt}
                    validateStatus={errors.profilePrompt ? 'error' : 'success'}
                  >
                    <Input
                      name='profilePrompt'
                      value={values.profilePrompt}
                      onChange={event => {
                        setFieldValue('profilePrompt', event.target.value)
                        handleFieldChange({
                          pairsOptions: {
                            ...pairsOptions,
                            profileQuestion: {
                              ...pairsOptions.profileQuestion,
                              prompt: event.target.value
                            }
                          }
                        })
                      }}
                      label={t(
                        'components.questionCreation.paired.profilePrompt'
                      )}
                      size='default'
                      required
                    />
                  </Form.Item>
                </Col>
              </Row>
            </OptionalSection>
            <ImageAndLabelSetting
              isNewQuestion={!id}
              optionDisplayType={optionDisplayType}
              onFieldChange={handleFieldChange}
            />
            <ChartSection
              isNewQuestion={!id}
              chartTypes={chartTypes}
              values={values}
              errors={errors}
              onFieldChange={(name, value) => {
                setFieldValue(name, value)
                handleFieldChange({ [name]: value })
              }}
            />
          </React.Fragment>
        )
      }}
    />
  )
}

const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      products
    }
  }
`

export default withTranslation()(PairedQuestions)
