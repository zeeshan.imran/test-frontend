import React from 'react'
import { mount } from 'enzyme'
import PairedQuestions from './index'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'
import wait from '../../../utils/testUtils/waait'
import ChartSection from '../ChartSection'
// import { act } from 'react-dom/test-utils'
// import Checkbox from '../../Checkbox'
// import Input from '../../Input'
import { SURVEY_CREATION } from '../../../queries/SurveyCreation'

jest.mock('../../../hooks/useLikingQuestionOptions')

jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k,
  getFixedT: () => () => ''
}))
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

const mockSurveyCreation = {
  products: [
    {
      id: 'product-1',
      name: 'product',
      brand: 'brand'
    }
  ]
}

describe('Test PairedQuestion CreationCard component', () => {
  let testRender
  let client
  const handleFieldChange = jest.fn()

  beforeEach(() => {
    client = createApolloMockClient()
    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: mockSurveyCreation
      }
    })
    handleFieldChange.mockReset()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('Should call handle function', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <PairedQuestions handleFieldChange={handleFieldChange} />
      </ApolloProvider>
    )

    await wait(0)
    handleFieldChange.mockReset()
    testRender
      .find(ChartSection)
      .first()
      .props()
      .onFieldChange('key', 'value')

    expect(handleFieldChange).toHaveBeenCalledWith({
      key: 'value'
    })

    handleFieldChange.mockReset()
    testRender
      .findWhere(n => n.name() === 'InputArea' && n.props().name === 'prompt')
      .first()
      .props()
      .onChange({ target: { value: 'promptValue' } })

    expect(handleFieldChange).toHaveBeenCalledWith({
      prompt: 'promptValue'
    })

    handleFieldChange.mockReset()
    testRender
      .findWhere(
        n => n.name() === 'InputArea' && n.props().name === 'secondaryPrompt'
      )
      .first()
      .props()
      .onChange({ target: { value: 'secondaryPromptValue' } })

    expect(handleFieldChange).toHaveBeenCalledWith({
      secondaryPrompt: 'secondaryPromptValue'
    })

    const checkboxes = [
      'customFlavorEnabled',
      'oneFlavorNotPresentEnabled',
      'neitherFlavorPresentEnabled',
      'bothFlavorsPresentEnabled'
    ]

    checkboxes.forEach(checkboxName => {
      handleFieldChange.mockReset()
      testRender
        .findWhere(
          n => n.name() === 'CustomCheckbox' && n.props().name === checkboxName
        )
        .first()
        .props()
        .onChange(true)

      expect(handleFieldChange).toHaveBeenCalledWith({
        pairsOptions: {
          [checkboxName]: true
        }
      })
      handleFieldChange.mockReset()
    })
  })

  /* test('Should update minPairs if it was not touched by user', async () => {
    const handleFieldChange = jest.fn()
    testRender = mount(
      <ApolloProvider client={client}>
        <PairedQuestions
          handleFieldChange={handleFieldChange}
          pairs={[{ pair: '1' }, { pair: '2' }, { pair: '3' }]}
        />
      </ApolloProvider>
    )
    await wait(0)

    testRender
      .findWhere(n => n.name() === 'TagInput')
      .first()
      .props()
      .handleAddTag('tag')

    expect(handleFieldChange).toHaveBeenCalledWith({
      pairs: [{ pair: '1' }, { pair: '2' }, { pair: '3' }, { pair: 'tag' }]
    })
    expect(handleFieldChange).toHaveBeenCalledWith({
      pairsOptions: { minPairs: 6 }
    })

    testRender
      .findWhere(n => n.name() === 'TagInput')
      .first()
      .props()
      .handleRemoveTag(1)

    expect(handleFieldChange).toHaveBeenCalledWith({
      pairs: [{ pair: '1' }, { pair: '3' }]
    })
    expect(handleFieldChange).toHaveBeenCalledWith({
      pairsOptions: { minPairs: 1 }
    })

    test('hide minPairs when having less than two tags', () => {
      expect(
        testRender.findWhere(
          n =>
            n.name() === 'Input' && n.props().name === 'pairsOptions.minPairs'
        ).length,
        0
      )
    })

    testRender.update()

    testRender
      .findWhere(n => n.name() === 'TagInput')
      .first()
      .props()
      .handleAddTag('tag')
    testRender.update()

    expect(handleFieldChange).toHaveBeenCalledWith({
      pairs: [{ pair: '1' }, { pair: '3' }, { pair: 'tag' }]
    })
    expect(handleFieldChange).toHaveBeenCalledWith({
      pairsOptions: { minPairs: 3 }
    })
  })
  test('should render Paired question OptionSelection', async () => {
    const OptionalSection = jest.fn()
    const handleFieldChange = jest.fn()
    testRender = mount(
      <ApolloProvider client={client}>
        <PairedQuestions
          label={'Show profile charts after this step'}
          onClickCheckbox={OptionalSection}
          isExpanded={false}
          pairsOptions={{}}
          handleFieldChange={handleFieldChange}
        />
      </ApolloProvider>
    )
    act(() => {
      testRender
        .find(Checkbox)
        .last()
        .prop('onChange')({ isValid: true, value: 1 })
    })

    expect(handleFieldChange).toHaveBeenCalledWith({
      pairsOptions: { hasFollowUpProfile: { isValid: true, value: 1 } }
    })
  })

  test('should render Paired question OptionSelection', async () => {
    const handleFieldChanges = jest.fn()
    testRender = mount(
      <ApolloProvider client={client}>
        <PairedQuestions
          pairs={['1', '2', '3']}
          name='profilePrompt'
          label={'Profile step title'}
          chartTypes={['spider']}
          pairsOptions={{ values: { profilePrompt: 'profile' } }}
          handleFieldChange={handleFieldChanges}
          validationSchema={''}
        />
      </ApolloProvider>
    )

    await wait(0)

    testRender
      .find(Input)
      .at(1)
      .prop('onChange')({ target: { value: 2 } })

    expect(handleFieldChanges).toHaveBeenCalled()
  })
*/
  test('should edit the profile prompt', () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <PairedQuestions
          validationSchema={''}
          secondaryPrompt={''}
          handleFieldChange={handleFieldChange}
          options={[]}
          skipToOptions={null}
          pairsOptions={{
            pairs: [],
            hasFollowUpProfile: true,
            profilePrompt: 'oldValue'
          }}
        />
      </ApolloProvider>
    )
    handleFieldChange.mockReset()
    testRender.update()
    testRender
      .find({ name: 'profilePrompt' })
      .first()
      .prop('onChange')({ target: { value: 'newValue' } })

    expect(handleFieldChange).toHaveBeenCalledWith({
      pairsOptions: {
        hasFollowUpProfile: true,
        pairs: [],
        profilePrompt: 'oldValue',
        profileQuestion: { prompt: 'newValue' }
      }
    })
  })
})
