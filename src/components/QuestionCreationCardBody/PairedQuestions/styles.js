import styled from 'styled-components'
import colors from '../../../utils/Colors'

export const CheckboxContainer = styled.div`
  display: flex;
  flex-direction: column;
`

export const InfoText = styled.div`
  margin-bottom: 24px;
  font-style: italic;
  color: ${colors.PALE_GREY};
`