import styled from 'styled-components'
import { Tag as AntTag, Form } from 'antd'
import Text from '../../Text'
import { family } from '../../../utils/Fonts'

export const Prompt = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  color: rgba(0, 0, 0, 0.65);
  display: flex;
  margin-bottom: 1.5rem;
`

export const Tag = styled(AntTag)`
  height: 3.2rem;
  display: inline-flex;
  align-items: center;
`

export const NewTag = styled(Tag)`
  background: '#fff';
  border-style: dashed;
`

export const ToggleSkipOptionsButton = styled.div`
  margin-bottom: 24px;
`

export const SkipOptions = styled.div`
  margin-bottom: 24px;
`

export const Label = styled(Text)`
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  color: rgba(0, 0, 0, 0.65);
`

export const LabelContainer = styled(Form.Item)`
  margin-bottom: 0;
`

export const NoSkippingRule = styled.div`
  padding: 2rem 2rem;
  margin: 0 0 1.5rem 0;
  border: 1px dashed #ddd;
  border-radius: 3px;
  color: #999;
  font-size: 1.2rem;
`
