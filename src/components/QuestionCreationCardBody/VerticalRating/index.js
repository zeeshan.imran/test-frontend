import React, { useState } from 'react'
import { Formik } from 'formik'
import { path, remove, lensPath, set } from 'ramda'
import { Icon, Row, Col, Form } from 'antd'
import TagInputVertical from '../TagInputVertical'
import Input from '../../Input'
import Button from '../../Button'
import ChartSection from '../ChartSection'
import {
  ToggleSkipOptionsButton,
  NoSkippingRule,
  SkipOptions,
  LabelContainer,
  Label
} from './styles'
import useQuestionValidate from '../../../hooks/useQuestionValidate'
import InputArea from '../../InputArea'
import { useTranslation } from 'react-i18next'
import useValidateWithContext from '../../../hooks/useValidateWithContext'
import useSkipToOptions from '../../../hooks/useSkipToOptions'
import SkipToPicker from '../SkipToPicker'
import skipFlowUtils from '../../../utils/skipFlowUtils'
import { trimSpaces } from '../../../utils/trimSpaces'
import QuestionInternalName from '../QuestionInternalName'
import DaysAndTimePicker from '../../DaysAndTimePicker'
import UploadSurveyCoverPicture from '../../../containers/UploadSurveyCoverPicture'

const VerticalRating = ({
  id,
  clientGeneratedId,
  handleFieldChange,
  validationSchema,
  chartTypes,
  prompt,
  secondaryPrompt,
  range = {},
  chartTitle = '',
  chartTopic = '',
  chartType = '',
  showOnShared = true,
  skipFlow = { rules: [] },
  internalPrompt = '',
  showInPreferedLanguage,
  addDelayToSelectNextProductAndNextQuestion = false,
  delayToNextQuestion = '',
  extraDelayToNextQuestion = '',
  image,
  hasImage
}) => {
  const { t } = useTranslation()
  const formRef = useQuestionValidate()
  const validate = useValidateWithContext(
    validationSchema,
    () => ({ chartTypes }),
    [chartTypes]
  )
  const skipToOptions = useSkipToOptions({ id, clientGeneratedId })
  const [isSkipOptionsShown, setIsSkipOptionsShown] = useState(
    !skipFlowUtils.isAllSkipToEmpty(skipFlow)
  )

  return (
    <Formik
      enableReinitialize
      ref={formRef}
      validate={validate}
      initialValues={{
        prompt,
        secondaryPrompt,
        range: {
          ...range
        },
        chartTitle,
        chartTopic,
        chartType,
        showOnShared,
        skipFlow,
        internalPrompt,
        delayToNextQuestion,
        extraDelayToNextQuestion,
        image
      }}
      render={({ errors, setFieldValue, values }) => {
        const showLabels =
          !errors.range || (!errors.range.max && !errors.range.min)
        const { labels, min, max } = values.range
        const handleSkipFlowChange = nextSkipFlow => {
          setFieldValue('skipFlow', nextSkipFlow)
          handleFieldChange({
            skipFlow: nextSkipFlow
          })
        }

        return (
          <React.Fragment>
            <Row gutter={24}>
              <Col lg={16} md={24}>
                <Row gutter={24}>
                  <Form.Item
                    help={errors.prompt}
                    validateStatus={errors.prompt ? 'error' : 'success'}
                  >
                    <InputArea
                      autoSize
                      name='prompt'
                      value={values.prompt}
                      onChange={event => {
                        setFieldValue('prompt', event.target.value)
                        handleFieldChange({ prompt: event.target.value })
                      }}
                      label={t('components.questionCreation.prompt')}
                      tooltip={t('tooltips.questionCreation.prompt')}
                      size='default'
                      required
                    />
                  </Form.Item>
                </Row>
                <Row gutter={24}>
                  <Form.Item>
                    <InputArea
                      autoSize
                      name='secondaryPrompt'
                      value={values.secondaryPrompt}
                      onChange={event => {
                        setFieldValue('secondaryPrompt', event.target.value)
                        handleFieldChange({
                          secondaryPrompt: event.target.value
                        })
                      }}
                      label={t('components.questionCreation.secondaryPrompt')}
                      tooltip={t('tooltips.questionCreation.secondaryPrompt')}
                      size='default'
                    />
                  </Form.Item>
                </Row>
              </Col>
              {hasImage && (
                <Col lg={8} md={8}>
                  <UploadSurveyCoverPicture
                    label={t('components.questionCreation.image')}
                    onChange={imageUrl => {
                      setFieldValue('image', imageUrl[0])
                      handleFieldChange({
                        image: imageUrl[0]
                      })
                    }}
                    value={values.image}
                  />
                </Col>
              )}
            </Row>
            {showInPreferedLanguage && (
              <QuestionInternalName
                setFieldValue={setFieldValue}
                handleFieldChange={handleFieldChange}
                internalPrompt={values.internalPrompt}
              />
            )}
            {/* Add delay for next question selection */}
            {addDelayToSelectNextProductAndNextQuestion && (
              <Row>
                <Form.Item>
                  <DaysAndTimePicker
                    delayToNext={values.delayToNextQuestion}
                    extraDelayToNext={values.extraDelayToNextQuestion}
                    onChangeDelayToNext={value => {
                      setFieldValue('delayToNextQuestion', value)
                      handleFieldChange({ delayToNextQuestion: value })
                    }}
                    onChangeExtraDelayToNext={value => {
                      setFieldValue('extraDelayToNextQuestion', value)
                      handleFieldChange({ extraDelayToNextQuestion: value })
                    }}
                    buttonText='Add delay to select next question'
                  />
                </Form.Item>
              </Row>
            )}
            <Row gutter={24}>
              <Col xs={12} lg={8}>
                <Form.Item
                  help={errors.range && errors.range.min}
                  validateStatus={
                    errors.range && errors.range.min ? 'error' : 'success'
                  }
                >
                  <Input
                    name='min'
                    type='number'
                    value={values.range.min}
                    onChange={event => {
                      const updatedRange = {
                        ...range,
                        min: event.target.value
                      }

                      setFieldValue('range', updatedRange)
                      handleFieldChange({
                        range: updatedRange
                      })
                    }}
                    size='default'
                    label={t(
                      'components.questionCreation.verticalRating.minimum'
                    )}
                    placeholder={t('placeholders.mininumScaleValue')}
                    required
                  />
                </Form.Item>
              </Col>
              <Col xs={12} lg={8}>
                <Form.Item
                  help={errors.range && errors.range.max}
                  validateStatus={
                    errors.range && errors.range.max ? 'error' : 'success'
                  }
                >
                  <Input
                    name='max'
                    type='number'
                    value={values.range.max}
                    onChange={event => {
                      const updatedRange = {
                        ...range,
                        max: event.target.value
                      }

                      setFieldValue('range', updatedRange)
                      handleFieldChange({
                        range: updatedRange
                      })
                    }}
                    size='default'
                    label={t(
                      'components.questionCreation.verticalRating.maximum'
                    )}
                    placeholder={t('placeholders.maximumScaleValue')}
                    required
                  />
                </Form.Item>
              </Col>
            </Row>
            {showLabels && (
              <Form.Item
                help={
                  errors.range &&
                  errors.range.labels &&
                  errors.range.labels.constructor === Array
                    ? errors.range.labels[0]
                    : errors.range && errors.range.labels
                }
                validateStatus={
                  errors.range && errors.range.labels ? 'error' : 'success'
                }
              >
                <TagInputVertical
                  label={t(
                    'components.questionCreation.verticalRating.scaleLabels',
                    {
                      min: values.range.min,
                      max: values.range.max
                    }
                  )}
                  tags={values.range.labels || []}
                  handleAddTag={label => {
                    if (!trimSpaces(label)) {
                      return
                    }
                    const updatedRange = {
                      ...range,
                      labels: [...(labels || []), label]
                    }
                    setFieldValue('range', updatedRange)
                    handleFieldChange({ range: updatedRange })
                  }}
                  handleRemoveTag={labelIndex => {
                    const updatedRange = {
                      ...range,
                      labels: remove(labelIndex, 1, labels)
                    }
                    setFieldValue('range', updatedRange)
                    handleFieldChange({ range: updatedRange })
                  }}
                  canAddMoreTags={(tags = []) =>
                    tags.length < parseInt(max, 10) - parseInt(min, 10) + 1
                  }
                />
              </Form.Item>
            )}
            {isSkipOptionsShown &&
              (!skipFlow || !skipFlow.rules || skipFlow.rules.length === 0) && (
                <Row gutter={24}>
                  <Col xs={6} lg={16}>
                    <NoSkippingRule>
                      {t(
                        'components.questionCreation.verticalRating.noSkipingRule'
                      )}
                    </NoSkippingRule>
                  </Col>
                </Row>
              )}
            {isSkipOptionsShown &&
              skipFlow &&
              skipFlow.rules.map((rule, index) => {
                const pathMinValue = path(
                  ['skipFlow', 'rules', index, 'minValue'],
                  errors
                )

                const pathMaxValue = path(
                  ['skipFlow', 'rules', index, 'maxValue'],
                  errors
                )
                return (
                  <SkipOptions key={index}>
                    <Row gutter={24}>
                      <Col xs={6} lg={8}>
                        <Form.Item
                          help={errors && pathMinValue}
                          validateStatus={
                            errors && pathMinValue ? 'error' : 'success'
                          }
                        >
                          <Input
                            htmlType='number'
                            name='skipFrom'
                            value={rule.minValue}
                            label={t(
                              'components.questionCreation.verticalRating.startingFrom'
                            )}
                            placeholder={t('placeholders.startingFrom')}
                            size='default'
                            onChange={event => {
                              const nextSkipFlow = set(
                                lensPath(['rules', index, 'minValue']),
                                event.target.value
                              )(skipFlow)

                              handleSkipFlowChange(nextSkipFlow)
                            }}
                          />
                        </Form.Item>
                      </Col>
                      <Col xs={6} lg={8}>
                        <Form.Item
                          help={pathMaxValue}
                          validateStatus={pathMaxValue ? 'error' : 'success'}
                        >
                          <Input
                            htmlType='number'
                            name='skipUpTo'
                            value={rule.maxValue}
                            label={t(
                              'components.questionCreation.verticalRating.upTo'
                            )}
                            placeholder={t('placeholders.upTo')}
                            size='default'
                            onChange={event => {
                              const nextSkipFlow = set(
                                lensPath(['rules', index, 'maxValue']),
                                event.target.value
                              )(skipFlow)

                              handleSkipFlowChange(nextSkipFlow)
                            }}
                          />
                        </Form.Item>
                      </Col>
                    </Row>
                    <Row gutter={24}>
                      <Col xs={6} lg={8}>
                        <LabelContainer>
                          <Label>
                            {t('components.questionCreation.jumpTo')}
                          </Label>
                        </LabelContainer>
                      </Col>
                      <Col xs={6} lg={8}>
                        <Form.Item>
                          <SkipToPicker
                            value={(rule && rule.skipTo) || null}
                            options={skipToOptions}
                            onSkipRemove={() => {
                              const nextSkipFlow = set(
                                lensPath(['rules', index, 'skipTo']),
                                null
                              )(skipFlow)

                              handleSkipFlowChange(nextSkipFlow)
                            }}
                            onSkipChange={skipTo => {
                              const nextSkipFlow = set(
                                lensPath(['rules', index, 'skipTo']),
                                skipTo
                              )(skipFlow)

                              handleSkipFlowChange(nextSkipFlow)
                            }}
                          />
                        </Form.Item>
                      </Col>
                      <Col xs={6} lg={8}>
                        <Button
                          size='default'
                          type='red'
                          onClick={() => {
                            handleSkipFlowChange({
                              ...skipFlow,
                              rules: remove(index, 1, skipFlow.rules)
                            })
                          }}
                        >
                          <Icon type='close' />
                        </Button>
                      </Col>
                    </Row>
                  </SkipOptions>
                )
              })}
            <Row gutter={24}>
              <Col xs={6} lg={8}>
                <ToggleSkipOptionsButton>
                  <Button
                    onClick={() => {
                      setIsSkipOptionsShown(!isSkipOptionsShown)
                    }}
                  >
                    {isSkipOptionsShown
                      ? t('components.questionCreation.hideSkip')
                      : t('components.questionCreation.showSkip')}
                  </Button>
                </ToggleSkipOptionsButton>
              </Col>
              {isSkipOptionsShown && (
                <Col xs={6} lg={8}>
                  <Button
                    onClick={() => {
                      handleSkipFlowChange({
                        ...skipFlow,
                        rules: [
                          ...skipFlow.rules,
                          {
                            minValue: null,
                            maxValue: null,
                            skipTo: null
                          }
                        ]
                      })
                    }}
                  >
                    {t(
                      'components.questionCreation.verticalRating.addSkipingRule'
                    )}
                  </Button>
                </Col>
              )}
            </Row>
            <ChartSection
              isNewQuestion={!id}
              chartTypes={chartTypes}
              values={values}
              errors={errors}
              onFieldChange={(name, value) => {
                setFieldValue(name, value)
                handleFieldChange({ [name]: value })
              }}
            />
          </React.Fragment>
        )
      }}
    />
  )
}

export default VerticalRating
