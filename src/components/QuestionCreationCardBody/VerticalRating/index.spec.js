import React from 'react'
import { mount } from 'enzyme'
import VerticalRating from './index'
import wait from '../../../utils/testUtils/waait'
import ChartSection from '../ChartSection'
import TagInputVertical from '../TagInputVertical'
import { ToggleSkipOptionsButton } from './styles'
import Button from '../../Button'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'
import SkipToPicker from '../SkipToPicker'

jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k,
  getFixedT: () => () => ''
}))

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('Test VerticalRating CreationCard component', () => {
  let handleFieldChange
  let client

  beforeEach(() => {
    //handleFieldChange.mockReset()
    handleFieldChange = jest.fn()

    client = createApolloMockClient()
  })

  test('Should call handle function', async () => {
    const testRender = mount(
      <ApolloProvider client={client}>
        <VerticalRating handleFieldChange={handleFieldChange} />
      </ApolloProvider>
    )

    await wait(0)

    handleFieldChange.mockReset()
    testRender
      .find(ChartSection)
      .first()
      .props()
      .onFieldChange('key', 'value')

    expect(handleFieldChange).toHaveBeenCalledWith({
      key: 'value'
    })

    handleFieldChange.mockReset()
    testRender
      .findWhere(n => n.name() === 'InputArea' && n.props().name === 'prompt')
      .first()
      .props()
      .onChange({ target: { value: 'promptValue' } })

    expect(handleFieldChange).toHaveBeenCalledWith({
      prompt: 'promptValue'
    })

    handleFieldChange.mockReset()
    testRender
      .findWhere(
        n => n.name() === 'InputArea' && n.props().name === 'secondaryPrompt'
      )
      .first()
      .props()
      .onChange({ target: { value: 'secondaryPromptValue' } })

    expect(handleFieldChange).toHaveBeenCalledWith({
      secondaryPrompt: 'secondaryPromptValue'
    })

    handleFieldChange.mockReset()
    testRender
      .findWhere(n => n.name() === 'Input' && n.props().name === 'min')
      .first()
      .props()
      .onChange({ target: { value: '100' } })

    expect(handleFieldChange).toHaveBeenCalledWith({
      range: {
        min: '100'
      }
    })

    testRender
      .findWhere(n => n.name() === 'Input' && n.props().name === 'max')
      .first()
      .props()
      .onChange({ target: { value: '1000' } })

    expect(handleFieldChange).toHaveBeenCalledWith({
      range: {
        max: '1000'
      }
    })

    testRender.setProps({
      range: { labels: ['1', '2'], min: 1, max: 9 },
      skipToOptions: []
    })

    handleFieldChange.mockReset()

    testRender.update()

    await wait(0)

    testRender
      .find(TagInputVertical)
      .first()
      .props()
      .handleAddTag('testTag')

    expect(handleFieldChange).toHaveBeenCalledWith({
      range: { labels: ['testTag'] }
    })

    handleFieldChange.mockReset()
    testRender.update()
    await wait(0)

    testRender
      .find(TagInputVertical)
      .first()
      .props()
      .handleRemoveTag(1)

    expect(handleFieldChange).toHaveBeenCalledWith({
      range: { labels: ['testTag'] }
    })

    handleFieldChange.mockReset()
    testRender.update()
    await wait(0)
  })
  test('Should call handle function with skipFlow', async () => {
    const skipFlow = {
      rules: [{ minValue: 1, maxValue: 20 }]
    }
    const testRender = mount(
      <ApolloProvider client={client}>
        <VerticalRating
          skipFlow={skipFlow}
          handleFieldChange={handleFieldChange}
        />
      </ApolloProvider>
    )

    await wait(0)

    testRender
      .find(ToggleSkipOptionsButton)
      .find(Button)
      .first()
      .props()
      .onClick()

    testRender.update()

    testRender
      .findWhere(n => n.name() === 'Input' && n.props().name === 'skipFrom')
      .first()
      .props()
      .onChange({ target: { value: '100' } })

    expect(handleFieldChange).toHaveBeenCalledWith({
      skipFlow: { rules: [{ maxValue: 20, minValue: '100' }] }
    })

    testRender
      .findWhere(n => n.name() === 'Input' && n.props().name === 'skipUpTo')
      .last()
      .props()
      .onChange({ target: { value: '200' } })

    expect(handleFieldChange).toHaveBeenCalledWith({
      skipFlow: { rules: [{ maxValue: 20, minValue: '100' }] }
    })

    testRender
      .find(SkipToPicker)
      .first()
      .props()
      .onSkipChange()

    testRender
      .find(SkipToPicker)
      .first()
      .props()
      .onSkipRemove()

    testRender
      .find(Button)
      .first()
      .props()
      .onClick()

    testRender
      .find(Button)
      .last()
      .props()
      .onClick()
    expect(handleFieldChange).toHaveBeenCalledWith({
      skipFlow: { rules: [{ maxValue: 20, minValue: '100' }] }
    })
  })
})
