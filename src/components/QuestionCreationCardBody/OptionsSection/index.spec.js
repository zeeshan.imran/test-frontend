describe('operator > OptionsSection', () => {
  test('should be fixed', () => {})
})
// import React from 'react'
// import { mount } from 'enzyme'
// import { append } from 'ramda'
// import Checkbox from '../../Checkbox'
// import Button from '../../Button'
// import { AddOptionButtonContainer } from './styles'
// import OptionsSection from './index'
// import { ApolloProvider } from 'react-apollo-hooks'
// import { createApolloMockClient } from '../../../utils/createApolloMockClient'
// import SkipToPicker from '../SkipToPicker'

// jest.mock('i18next', () => ({
//   use: () => ({ init: () => { } }),
//   init: () => { },
//   t: k => k
// }))

// describe('OptionsSection', () => {
//   let testRender
//   let label
//   let options
//   let handleChangeOptions
//   let errors
//   let skipToOptions
//   let noAnalytics
//   let minimOptions
//   let client

//   beforeEach(() => {
//     label = 'Please select option'
//     options = [
//       {
//         value: 'option1',
//         skipTo: true,
//         label: 'Label 1'
//       },
//       {
//         value: 'option2',
//         skipTo: false,
//         label: 'Label 2'
//       }
//     ]
//     client = createApolloMockClient()

//     handleChangeOptions = jest.fn()
//     errors = {}
//     skipToOptions = [{ value: 'option2', label: 'Label 2' }]

//     noAnalytics = false
//     minimOptions = 1
//   })

//   afterEach(() => {
//     testRender.unmount()
//   })

//   test('should render OptionsSection', async () => {
//     testRender = mount(
//       <ApolloProvider client={client}>
//         <OptionsSection
//           label={label}
//           options={options}
//           onChangeOptions={handleChangeOptions}
//           errors={errors}
//           skipToOptions={skipToOptions}
//           noAnalytics={noAnalytics}
//           minimOptions={minimOptions}
//         />
//       </ApolloProvider>
//     )
//     expect(testRender.find(OptionsSection)).toHaveLength(1)
//   })

//   test('should render OptionsSection', async () => {
//     testRender = mount(
//       <ApolloProvider client={client}>
//         <OptionsSection
//           label={label}
//           options={options}
//           onChangeOptions={handleChangeOptions}
//           errors={errors}
//           skipToOptions={skipToOptions}
//           noAnalytics={noAnalytics}
//           minimOptions={minimOptions}
//         />
//       </ApolloProvider>
//     )
//     testRender
//       .findWhere(c => c.name() === 'Input')
//       .at(0)
//       .prop('onChange')({ target: { value: '0' } })

//     expect(handleChangeOptions).toHaveBeenCalled()
//   })

//   test('should render OptionsSection minimOptions Button', async () => {
//     testRender = mount(
//       <ApolloProvider client={client}>
//         <OptionsSection
//           label={label}
//           options={options}
//           onChangeOptions={handleChangeOptions}
//           errors={errors}
//           skipToOptions={skipToOptions}
//           noAnalytics={noAnalytics}
//           minimOptions={1}
//         />
//       </ApolloProvider>
//     )
//     testRender
//       .findWhere(c => c.name() === 'Button')
//       .at(0)
//       .simulate('click')
//     expect(handleChangeOptions).toHaveBeenCalled()
//   })

//   test('should render OptionsSection noAnalytics Input', async () => {
//     testRender = mount(
//       <ApolloProvider client={client}>
//         <OptionsSection
//           label={'Analytics Value'}
//           options={['option1', 'option2']}
//           onChangeOptions={handleChangeOptions}
//           errors={errors}
//           noAnalytics={''}
//         />
//       </ApolloProvider>
//     )
//     testRender
//       .findWhere(c => c.name() === 'Input')
//       .at(1)
//       .prop('onChange')({ target: { event: '0' } })

//     expect(handleChangeOptions).toHaveBeenCalled()
//   })

//   test('should wender OptionsSection Analytics Value input', () => {
//     testRender = mount(
//       <ApolloProvider client={client}>
//         <OptionsSection
//           label={'Analytics Value'}
//           options={[{ label: 'option1' }, { label: 'option2' }]}
//           onChangeOptions={handleChangeOptions}
//           errors={errors}
//           noAnalytics={false}
//         />
//       </ApolloProvider>
//     )
//     testRender
//       .find({ placeholder: 'placeholders.analyticsValue' })
//       .first()
//       .prop('onChange')({ target: { value: 'val' } })

//     expect(handleChangeOptions).toHaveBeenCalledWith([
//       { label: 'option1', value: 'val' },
//       { label: 'option2' }
//     ])
//   })

//   test('should render OptionsSection Analytics Value input', () => {
//     testRender = mount(
//       <ApolloProvider client={client}>
//         <OptionsSection
//           label={'Analytics Value'}
//           options={[{ label: 'option1', value: 1 }]}
//           onChangeOptions={handleChangeOptions}
//           errors={errors}
//           noAnalytics={false}
//           minOptions={0}
//         />
//       </ApolloProvider>
//     )
//     testRender
//       .findWhere(c => c.name() === 'Button')
//       .at(0)
//       .simulate('click')

//     expect(handleChangeOptions).toHaveBeenCalled()
//   })

//   test('Should display "other" checkbox if allowed', async () => {
//     testRender = mount(
//       <ApolloProvider client={client}>
//         <OptionsSection onChangeOptions={handleChangeOptions} />
//       </ApolloProvider>
//     )
//     expect(testRender.find(Checkbox).length).toBe(0)
//     testRender.setProps({ isOpenAnswerPossible: true })
//     expect(testRender.find(Checkbox).length).toBe(0)
//   })

//   test('"Other" option should remains on last position', async () => {
//     const mockOptions = [{ label: '1-l', value: '1' }]
//     testRender = mount(
//       <ApolloProvider client={client}>
//         <OptionsSection
//           onChangeOptions={handleChangeOptions}
//           isOpenAnswerPossible
//           options={mockOptions}
//         />
//       </ApolloProvider>
//     )
//     testRender
//       .find(Checkbox)
//       .props()
//       .onChange()

//     testRender
//       .find(Checkbox)
//       .props()
//       .onChange(true)
//     expect(handleChangeOptions).toBeCalledWith([
//       { label: '1-l', value: '1' },
//       { isOpenAnswer: true, label: 'Other', skipTo: 'null', value: 'other' }
//     ])
//     handleChangeOptions.mockReset()
//     testRender.setProps({
//       options: append(
//         { isOpenAnswer: true, label: 'Other', skipTo: 'null', value: 'other' },
//         mockOptions
//       )
//     })
//     testRender
//       .find(AddOptionButtonContainer)
//       .find(Button)
//       .props()
//       .onClick()
//     expect(handleChangeOptions).toBeCalledWith([
//       { label: '1-l', value: '1' },
//       { label: null }
//     ])
//   })

//   test('should render OptionsSection canRemoveOption nextSkipFlow return 1', () => {
//     const skipFlow = {
//       rules: [{ index: 1, minValue: 1, maxValue: 20 }]
//     }
//     const onSkipFlowChange = jest.fn()
//     testRender = mount(
//       <ApolloProvider client={client}>
//         <OptionsSection
//           label={'Analytics Value'}
//           options={[
//             { label: 'option1', value: 1 },
//             { label: 'option2', value: 2 }
//           ]}
//           onChangeOptions={handleChangeOptions}
//           errors={errors}
//           minOptions={0}
//           skipFlow={{ ...skipFlow, type: 'MATCH_MULTIPLE_OPTION' }}
//           nextSkipFlow={skipFlow}
//           onSkipFlowChange={onSkipFlowChange}
//         />
//       </ApolloProvider>
//     )
//     testRender
//       .findWhere(c => c.name() === 'Button')
//       .at(0)
//       .simulate('click')

//     expect(handleChangeOptions).toHaveBeenCalled()
//     expect(onSkipFlowChange).toBeCalledWith({
//       rules: [{ index: 0, maxValue: 20, minValue: 1 }], type: 'MATCH_MULTIPLE_OPTION'

//     })
//   })

//   test('should render OptionsSection canRemoveOption nextSkipFlow return rule', () => {
//     const skipFlow = {
//       rules: [{ minValue: 1, maxValue: 20 }]
//     }
//     const onSkipFlowChange = jest.fn()
//     testRender = mount(
//       <ApolloProvider client={client}>
//         <OptionsSection
//           label={'Analytics Value'}
//           options={[
//             { label: 'option1', value: 1 },
//             { label: 'option2', value: 2 }
//           ]}
//           onChangeOptions={handleChangeOptions}
//           errors={errors}
//           minOptions={0}
//           skipFlow={{ ...skipFlow, type: 'MATCH_MULTIPLE_OPTION' }}
//           nextSkipFlow={skipFlow}
//           onSkipFlowChange={onSkipFlowChange}
//         />
//       </ApolloProvider>
//     )
//     testRender
//       .findWhere(c => c.name() === 'Button')
//       .at(0)
//       .simulate('click')

//     expect(handleChangeOptions).toHaveBeenCalled()
//     expect(onSkipFlowChange).toBeCalledWith({
//       rules: [{ maxValue: 20, minValue: 1 }],
//       type: 'MATCH_MULTIPLE_OPTION'
//     })
//   })

//   test('should render skip to options', () => {
//     const onSkipFlowChange = jest.fn()
//     testRender = mount(
//       <ApolloProvider client={client}>
//         <OptionsSection
//           label={label}
//           options={options}
//           onChangeOptions={handleChangeOptions}
//           errors={errors}
//           skipToOptions={skipToOptions}
//           noAnalytics={noAnalytics}
//           minimOptions={minimOptions}
//           onSkipFlowChange={onSkipFlowChange}
//           skipFlow={{ rules: [{ skipTo: 1 }, { skipTo: 2 }] }}
//         />
//       </ApolloProvider>
//     )
//     testRender
//       .find(SkipToPicker)
//       .at(0)
//       .prop('onSkipChange')(2)
//     expect(onSkipFlowChange).toHaveBeenCalled()
//     onSkipFlowChange.mockReset()
//     testRender
//       .find(SkipToPicker)
//       .at(0)
//       .prop('onSkipRemove')()
//     expect(onSkipFlowChange).toHaveBeenCalled()
//   })

//   test('should toggle skipTo options', async () => {
//     const onSkipFlowChange = jest.fn()
//     testRender = mount(
//       <ApolloProvider client={client}>
//         <OptionsSection
//           label={label}
//           options={options}
//           onChangeOptions={handleChangeOptions}
//           errors={errors}
//           skipToOptions={skipToOptions}
//           noAnalytics={noAnalytics}
//           minimOptions={minimOptions}
//           onSkipFlowChange={onSkipFlowChange}
//           skipFlow={{ rules: [{ skipTo: 1 }, { skipTo: 2 }] }}
//           noSkipOptions={false}
//           isOpenAnswerPossible={true}
//         />
//       </ApolloProvider>
//     )
//     // the above rows are copied from `should render skip to options`
//     expect(testRender.find(SkipToPicker)).toHaveLength(2)
//     testRender
//       .find(Button)
//       .last()
//       .simulate('click')
//     expect(testRender.find(SkipToPicker)).toHaveLength(0)
//   })
// })
