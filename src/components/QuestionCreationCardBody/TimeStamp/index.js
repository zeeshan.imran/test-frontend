import React from 'react'
import { Formik } from 'formik'
import { Row, Col } from 'antd'
import { useTranslation } from 'react-i18next'
import useQuestionValidate from '../../../hooks/useQuestionValidate'
import ChartSection from '../ChartSection'
import useValidateWithContext from '../../../hooks/useValidateWithContext'

const TimeStampQuestion = ({
  id,
  validationSchema,
  handleFieldChange,
  prompt,
  secondaryPrompt,
  chartTypes,
  chartTitle,
  chartTopic,
  chartType,
  showOnShared = true
}) => {
  const { t } = useTranslation()
  const formRef = useQuestionValidate()
  const validate = useValidateWithContext(
    validationSchema,
    () => ({ chartTypes }),
    [chartTypes]
  )

  return (
    <Formik
      ref={formRef}
      enableReinitialize
      validate={validate}
      initialValues={{
        prompt,
        secondaryPrompt,
        chartTitle,
        chartTopic,
        chartType,
        showOnShared
      }}
      render={({ errors, setFieldValue, values }) => {
        return (
          <React.Fragment>
            <Row gutter={24}>
              <Col lg={24} md={24}>
                {t('components.questionCreation.timestamp.info')}
              </Col>
            </Row>
            <ChartSection
              isNewQuestion={!id}
              style={{ margin: '2.5rem -3.5rem 0' }}
              chartTypes={chartTypes}
              values={values}
              errors={errors}
              onFieldChange={(name, value) => {
                setFieldValue('' + name, value)
                handleFieldChange({ [name]: value })
              }}
            />
          </React.Fragment>
        )
      }}
    />
  )
}

export default TimeStampQuestion
