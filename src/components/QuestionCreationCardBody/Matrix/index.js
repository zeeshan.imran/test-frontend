import React from 'react'
import { Row, Col, Form } from 'antd'
import { Formik } from 'formik'
import { withTranslation } from 'react-i18next'
import OptionsSection from '../OptionsSection'
import OptionalSection from '../OptionalSection'
import ChartSection from '../ChartSection'
import { getDefaultSelectOptions } from '../../../utils/defaultQuestionValues'
import { minOptions } from '../../../validates/questions/choose-one'
import InputArea from '../../InputArea'
import useValidateWithContext from '../../../hooks/useValidateWithContext'
import Input from '../../Input'
import useQuestionValidate from '../../../hooks/useQuestionValidate'
import QuestionInternalName from '../QuestionInternalName'
import DaysAndTimePicker from '../../DaysAndTimePicker'
import UploadSurveyCoverPicture from '../../../containers/UploadSurveyCoverPicture'

const Matrix = ({
  id,
  clientGeneratedId,
  validationSchema,
  chartTypes,
  prompt = '',
  handleFieldChange,
  options = [],
  settings,
  chartTitle = '',
  chartTopic = '',
  chartType = '',
  showOnShared = true,
  secondaryPrompt,
  matrixOptions = {},
  t,
  skipFlow,
  compulsorySurvey,
  internalPrompt = '',
  showInPreferedLanguage,
  addDelayToSelectNextProductAndNextQuestion = false,
  delayToNextQuestion = '',
  extraDelayToNextQuestion = '',
  image,
  hasImage
}) => {
  const formRef = useQuestionValidate()
  const validate = useValidateWithContext(
    validationSchema,
    () => ({ chartTypes, compulsorySurvey }),
    [chartTypes, compulsorySurvey]
  )
  if (options.length < minOptions) {
    options = getDefaultSelectOptions(minOptions)
  }
  if (matrixOptions && !matrixOptions.question) {
    handleFieldChange({
      matrixOptions: {
        ...matrixOptions,
        question: [
          {
            label: null
          },
          {
            label: null
          },
          {
            label: null
          }
        ]
      }
    })
  }
  return (
    <Formik
      ref={formRef}
      enableReinitialize
      validate={validate}
      initialValues={{
        prompt,
        options,
        settings,
        chartTitle,
        chartTopic,
        chartType,
        secondaryPrompt,
        matrixOptions,
        showOnShared,
        t,
        internalPrompt,
        delayToNextQuestion,
        extraDelayToNextQuestion,
        image
      }}
      render={({ errors, setFieldValue, values }) => {
        const handleMatrixChange = updatedMatrix => {
          const updateMatrixId = updatedMatrix.map((val, index) => {
            return { ...val, ...{ id: `row_${index}` } }
          })
          const change = {
            matrixOptions: {
              ...matrixOptions,
              question: updateMatrixId
            }
          }
          setFieldValue('matrixOptions', change.matrixOptions)
          handleFieldChange(change)
        }

        return (
          <React.Fragment>
            <Row gutter={24}>
              <Col lg={16} md={24}>
                <Row gutter={24}>
                  <Form.Item
                    help={errors.prompt}
                    validateStatus={errors.prompt ? 'error' : 'success'}
                  >
                    <InputArea
                      data-testid={'choose-multiple-question-text'}
                      autoSize
                      name='prompt'
                      value={values.prompt}
                      onChange={event => {
                        setFieldValue('prompt', event.target.value)
                        handleFieldChange({ prompt: event.target.value })
                      }}
                      label={t('components.questionCreation.prompt')}
                      tooltip={t('tooltips.questionCreation.prompt')}
                      size='default'
                      required
                    />
                  </Form.Item>
                </Row>
                <Row gutter={24}>
                  <Form.Item>
                    <InputArea
                      autoSize
                      name='secondaryPrompt'
                      value={values.secondaryPrompt}
                      onChange={event => {
                        setFieldValue('secondaryPrompt', event.target.value)
                        handleFieldChange({
                          secondaryPrompt: event.target.value
                        })
                      }}
                      label={t('components.questionCreation.secondaryPrompt')}
                      tooltip={t('tooltips.questionCreation.secondaryPrompt')}
                      size='default'
                    />
                  </Form.Item>
                </Row>
              </Col>
              {hasImage && (
                <Col lg={8} md={8}>
                  <UploadSurveyCoverPicture
                    label={t('components.questionCreation.image')}
                    onChange={imageUrl => {
                      setFieldValue('image', imageUrl[0])
                      handleFieldChange({
                        image: imageUrl[0]
                      })
                    }}
                    value={values.image}
                  />
                </Col>
              )}
            </Row>
            {showInPreferedLanguage && (
              <QuestionInternalName
                setFieldValue={setFieldValue}
                handleFieldChange={handleFieldChange}
                internalPrompt={values.internalPrompt}
              />
            )}
            {/* Add delay for next question selection */}
            {addDelayToSelectNextProductAndNextQuestion && (
              <Row>
                <Form.Item>
                  <DaysAndTimePicker
                    delayToNext={values.delayToNextQuestion}
                    extraDelayToNext={values.extraDelayToNextQuestion}
                    onChangeDelayToNext={value => {
                      setFieldValue('delayToNextQuestion', value)
                      handleFieldChange({ delayToNextQuestion: value })
                    }}
                    onChangeExtraDelayToNext={value => {
                      setFieldValue('extraDelayToNextQuestion', value)
                      handleFieldChange({ extraDelayToNextQuestion: value })
                    }}
                    buttonText='Add delay to select next question'
                  />
                </Form.Item>
              </Row>
            )}
            <OptionsSection
              id={id}
              clientGeneratedId={clientGeneratedId}
              label={'Columns'}
              options={values.options}
              onChangeOptions={updatedOptions => {
                setFieldValue('options', updatedOptions)
                handleFieldChange({
                  options: updatedOptions
                })
              }}
              errors={errors.options}
              addAnswerLabel='Add Column'
              compulsorySurvey={compulsorySurvey}
            />
            {/* Min and max start */}
            <OptionalSection
              label='Choose Multiple'
              onClickCheckbox={value => {
                setFieldValue('settings.chooseMultiple', value)
                handleFieldChange({
                  settings: {
                    ...settings,
                    chooseMultiple: value
                  }
                })
              }}
              isExpanded={values.settings && values.settings.chooseMultiple}
            >
              <Row gutter={24}>
                <Col lg={8} md={12}>
                  <Form.Item
                    help={errors.settings && errors.settings.minAnswerValues}
                    validateStatus={
                      errors.settings && errors.settings.minAnswerValues
                        ? 'error'
                        : 'success'
                    }
                  >
                    <Input
                      name='settings.minAnswerValues'
                      type='number'
                      min='1'
                      step='1'
                      value={values.settings && values.settings.minAnswerValues}
                      onChange={event => {
                        const value = event.target.value || null
                        setFieldValue('settings.minAnswerValues', value)
                        handleFieldChange({
                          settings: {
                            ...settings,
                            minAnswerValues: value
                          }
                        })
                      }}
                      label={t(
                        'components.questionCreation.chooseMultiple.minAnswerValues',
                        {}
                      )}
                      size='default'
                    />
                  </Form.Item>
                </Col>
                <Col lg={8} md={12}>
                  <Form.Item
                    help={errors.settings && errors.settings.maxAnswerValues}
                    validateStatus={
                      errors.settings && errors.settings.maxAnswerValues
                        ? 'error'
                        : 'success'
                    }
                  >
                    <Input
                      name='settings.maxAnswerValues'
                      type='number'
                      min='1'
                      step='1'
                      value={values.settings && values.settings.maxAnswerValues}
                      onChange={event => {
                        const value = event.target.value || null
                        setFieldValue('settings.maxAnswerValues', value)
                        handleFieldChange({
                          settings: {
                            ...settings,
                            maxAnswerValues: value
                          }
                        })
                      }}
                      label={t(
                        'components.questionCreation.chooseMultiple.maxAnswerValues',
                        {}
                      )}
                      size='default'
                    />
                  </Form.Item>
                </Col>
              </Row>
            </OptionalSection>
            {/* min and max end */}
            <OptionsSection
              id={id}
              clientGeneratedId={clientGeneratedId}
              minOptions={1}
              label={'Rows'}
              columnsData={values.options}
              options={values.matrixOptions && values.matrixOptions.question}
              onChangeOptions={updatedQuestion => {
                handleMatrixChange(updatedQuestion)
              }}
              errors={errors.options}
              noAnalytics
              addAnswerLabel='Add Row'
              skipFlow={skipFlow}
              onSkipFlowChange={nextSkipFlow => {
                setFieldValue('skipFlow', nextSkipFlow)
                handleFieldChange({
                  skipFlow: nextSkipFlow
                })
              }}
            />
            <ChartSection
              isNewQuestion={!id}
              chartTypes={chartTypes}
              values={values}
              errors={errors}
              onFieldChange={(name, value) => {
                setFieldValue(name, value)
                handleFieldChange({ [name]: value })
              }}
            />
          </React.Fragment>
        )
      }}
    />
  )
}

export default withTranslation()(Matrix)
