import React, { memo } from 'react'
import kebabToComponentCase from '../../utils/kebabToComponentCase'
import { Container } from './styles'
import { validateQuestionSchemas } from '../../validates'
import getChartTypes from '../../utils/getChartTypes'
import { clone } from 'ramda'

const QuestionCreationCardBody = ({
  handleFieldChange,
  type,
  displayOn,
  ...otherQuestionProps
}) => {
  if (!type) {
    return null
  }

  const componentName = kebabToComponentCase(type)
  const CreationFormComponent = require(`./${componentName}`).default
  const validationSchema = validateQuestionSchemas[type]
  const chartTypes = getChartTypes({
    type,
    displayOn
  })
  const hasImage = displayOn !== 'middle'

  return (
    <Container>
      <CreationFormComponent
        handleFieldChange={handleFieldChange}
        chartTypes={chartTypes}
        validationSchema={validationSchema}
        displayOn={displayOn}
        {...otherQuestionProps}
        skipFlow={clone(otherQuestionProps.skipFlow)}
        hasImage={hasImage}
      />
    </Container>
  )
}

export default memo(QuestionCreationCardBody)
