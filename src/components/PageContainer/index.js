import React from 'react'
import { Container } from './styles'

const PageContainer = ({ children }) => <Container>{children}</Container>

export default PageContainer
