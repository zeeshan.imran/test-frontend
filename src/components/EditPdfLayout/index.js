import React, { useCallback, useMemo } from 'react'
import { renderToStaticMarkup } from 'react-dom/server'
import ReactGridLayout from 'react-grid-layout'
import { Layout, Icon } from 'antd'
import { EditorContainer, Pages, Content, DeleteChartButton } from './styles'
import SideBar from './SideBar'
import LoadingState from './LoadingState'
import RenderingState from './RenderingState'
import { ChartContextProvider } from '../../contexts/ChartContext'
import { PAPER_SIZES, getColsPerPage } from '../../utils/paperSizes'
import { getPageOfItem } from '../../utils/pdfUtils'

const Paper = ({ layout: paperSize, colWidth, zoom = 1 }) => {
  const { cols, colsBetweenPages, height } = paperSize
  const outerWidth = (cols + colsBetweenPages) * colWidth

  return (
    <svg
      width={outerWidth}
      height={zoom * height}
      xmlns='http://www.w3.org/2000/svg'
      fill='#ff0000'
    >
      <rect
        x={0}
        y={0}
        width={cols * colWidth}
        height={zoom * height}
        fill='#ffffff'
      />
    </svg>
  )
}

const PageItem = React.memo(({ item, canDelete, onDeletePage, onAddPage }) => {
  return item.content({
    canDelete,
    onDeletePage,
    onAddPage
  })
})

const ElementItem = React.memo(({ item, onItemDelete }) => {
  return (
    <React.Fragment>
      {item.content}
      {!item.static && (
        <DeleteChartButton
          shape='circle'
          type='danger'
          size='small'
          onClick={() => onItemDelete(item.i)}
        >
          <Icon type='delete' />
        </DeleteChartButton>
      )}
    </React.Fragment>
  )
})

const EditPdfLayout = ({
  // state props
  loading,
  rendering,
  ready,
  //editor props
  templateName,
  colWidth,
  rowHeight,
  zoom,
  onZoom,
  showLayoutBorder,
  onShowLayoutBorderChange,
  onDownload,
  onTemplateSave,
  onClose,
  // grid props
  footerNote,
  layout,
  elementItems,
  onItemDelete,
  onDeletePage,
  onAddPage,
  onLayoutPropChange,
  onLayoutResizeStop,
  onLayoutChange,
  onLayoutReset
}) => {
  const paperSizeMeta = PAPER_SIZES[layout.paperSize]
  const {
    pageItems,
    deletedItems,
    logoPlacement,
    pageInfoPlacement,
    footerNoteAllPages,
    gridLayout
  } = layout
  const totalPages = layout.totalPages
  const colsPerPage = getColsPerPage(paperSizeMeta)
  const layoutsWithoutPages = gridLayout.filter(
    item => item.x % colsPerPage < paperSizeMeta.cols
  )

  const handleDownload = useCallback(async () => {
    await onDownload()
  }, [onDownload])

  const handleTemplateSave = useCallback(async () => {
    await onTemplateSave()
  }, [onTemplateSave])

  const handleClose = useCallback(async () => {
    await onClose()
  }, [onClose])

  const children = useMemo(() => {
    if (!pageItems || !elementItems) {
      return []
    }

    return [
      ...pageItems.map(item => {
        if (item.type === 'page-control') {
          return (
            <div key={item.i}>
              <PageItem
                item={item}
                canDelete={
                  !layoutsWithoutPages.some(
                    i => !i.static && getPageOfItem(layout.paperSize, i) === item.page
                  )
                }
                onAddPage={onAddPage}
                onDeletePage={onDeletePage}
              />
            </div>
          )
        }

        return <div key={item.i}>{item.content}</div>
      }),
      ...elementItems
        .filter(item => !deletedItems.includes(item.i))
        .map(item => (
          <div key={item.i} id={`pdf-${item.i}`}>
            <ElementItem item={item} onItemDelete={onItemDelete} />
          </div>
        ))
    ]
  }, [
    deletedItems,
    elementItems,
    layout.paperSize,
    layoutsWithoutPages,
    onAddPage,
    onDeletePage,
    onItemDelete,
    pageItems
  ])

  if (loading) {
    return <LoadingState />
  }

  return (
    <ChartContextProvider showAll printView>
      <Layout>
        {rendering && <RenderingState />}
        <Content>
          <EditorContainer>
            <Pages
              width={totalPages * colsPerPage * colWidth}
              height={zoom * paperSizeMeta.height}
              showLayoutBorder={showLayoutBorder}
              onShow
              style={{
                backgroundImage: `url('data:image/svg+xml;utf8,${encodeURIComponent(
                  renderToStaticMarkup(
                    <Paper
                      layout={paperSizeMeta}
                      colWidth={colWidth}
                      zoom={zoom}
                    />
                  )
                )}')`
              }}
            >
              {ready && (
                <ReactGridLayout
                  layout={gridLayout}
                  onLayoutChange={onLayoutChange}
                  onResizeStop={(_layout, _oldItem, newItem) => {
                    onLayoutResizeStop(newItem)
                  }}
                  cols={totalPages * colsPerPage}
                  width={totalPages * colsPerPage * colWidth}
                  rowHeight={rowHeight * 0.995}
                  margin={[0, 0]}
                  containerPadding={[0, 0]}
                  verticalCompact={false}
                  preventCollision
                  useCSSTransforms={!rendering}
                >
                  {children}
                </ReactGridLayout>
              )}
            </Pages>
          </EditorContainer>
        </Content>
        <SideBar
          footerNote={footerNote}
          templateName={templateName}
          paperSize={paperSizeMeta.name}
          zoom={zoom}
          deletedItems={deletedItems}
          logoPlacement={logoPlacement}
          pageInfoPlacement={pageInfoPlacement}
          footerNoteAllPages={footerNoteAllPages}
          onLayoutReset={onLayoutReset}
          onZoomChange={onZoom}
          onDownload={handleDownload}
          onTemplateSave={handleTemplateSave}
          showLayoutBorder={showLayoutBorder}
          onShowLayoutBorderChange={onShowLayoutBorderChange}
          onLayoutPropChange={onLayoutPropChange}
          onClose={handleClose}
        />
      </Layout>
    </ChartContextProvider>
  )
}

export default EditPdfLayout
