import React, { useState } from 'react'
import { Button, Icon, Checkbox, Select, Typography } from 'antd'
import ZoomButton from './ZoomButton'
import { Sider, ConfigItem, Actions, Label, TitleContainer } from './styles'
import { useTranslation } from 'react-i18next'

const Option = Select.Option

const SideBar = ({
  footerNote,
  templateName,
  zoom,
  onZoomChange,
  paperSize,
  deletedItems,
  logoPlacement,
  pageInfoPlacement,
  footerNoteAllPages,
  onLayoutReset,
  onLayoutPropChange,
  showLayoutBorder,
  onShowLayoutBorderChange,
  onTemplateSave,
  onClose,
  onDownload
}) => {
  const { t } = useTranslation()
  const [downloading, setDownloading] = useState(false)
  const [saving, setSaving] = useState(false)

  const handleDownload = async () => {
    setDownloading(true)
    await onDownload()
    setDownloading(false)
  }

  const handleTemplateSave = async () => {
    setSaving(true)
    await onTemplateSave()
    setSaving(false)
  }

  return (
    <Sider theme='light' width={300}>
      <TitleContainer>
        <Typography.Title level={4}>
          {t(`pdfLayoutEditor.title.${templateName}`)}
        </Typography.Title>
        <Button size='small' type='default' shape='circle' onClick={onClose}>
          <Icon type='close' />
        </Button>
      </TitleContainer>
      <ConfigItem>
        <Label>{t('pdfLayoutEditor.zoom')}</Label>
        <ZoomButton {...{ zoom, onZoomChange }} />
      </ConfigItem>
      <ConfigItem>
        <Label>{t('pdfLayoutEditor.paperSize')}</Label>
        <Select
          value={paperSize}
          style={{ width: 120 }}
          onChange={paperSize => onLayoutPropChange({ paperSize })}
        >
          <Option value='A4'>A4</Option>
          <Option value='Letter'>Letter</Option>
        </Select>
      </ConfigItem>
      <ConfigItem>
        <Label>{t('pdfLayoutEditor.logoPlacement')}</Label>
        <Select
          value={logoPlacement}
          style={{ width: 120 }}
          onChange={logoPlacement => onLayoutPropChange({ logoPlacement })}
        >
          <Option value='no'>No</Option>
          <Option value='topLeft'>Top left</Option>
          <Option value='topRight'>Top right</Option>
          <Option value='bottomLeft'>Bottom left</Option>
          <Option value='bottomRight'>Bottom right</Option>
        </Select>
      </ConfigItem>
      <ConfigItem>
        <Label>{t('pdfLayoutEditor.pageInfoPlacement')}</Label>
        <Select
          value={pageInfoPlacement}
          style={{ width: 120 }}
          onChange={pageInfoPlacement => {
            onLayoutPropChange({
              pageInfoPlacement
            })
          }}
        >
          <Option value='no'>No</Option>
          <Option value='topRight'>Top right</Option>
          <Option value='bottomRight'>Bottom right</Option>
        </Select>
      </ConfigItem>
      {!!footerNote && (
        <ConfigItem>
          <Checkbox
            checked={footerNoteAllPages}
            onChange={e =>
              onLayoutPropChange({ footerNoteAllPages: e.target.checked })
            }
          >
            {t('pdfLayoutEditor.footerNoteAllPages')}
          </Checkbox>
        </ConfigItem>
      )}
      <ConfigItem>
        <Checkbox
          checked={showLayoutBorder}
          onChange={e => onShowLayoutBorderChange(e.target.checked)}
        >
          {t('pdfLayoutEditor.showLayoutBorder')}
        </Checkbox>
      </ConfigItem>
      <Actions>
        <Button type='primary' loading={saving} onClick={handleTemplateSave}>
          {t('pdfLayoutEditor.saveAsTemplate')}
        </Button>
        <Button
          type='primary'
          icon='download'
          loading={downloading}
          onClick={handleDownload}
        />
      </Actions>
      <Actions>
        <Button type='primary' icon='undo' onClick={onLayoutReset}>
          {t('pdfLayoutEditor.resetToDefault')}
        </Button>
        {!!deletedItems.length && (
          <div>
            <Typography.Text type='secondary'>
              {t('pdfLayoutEditor.resetToDefaultNote', {
                count: deletedItems.length
              })}
            </Typography.Text>
          </div>
        )}
      </Actions>
    </Sider>
  )
}

export default SideBar
