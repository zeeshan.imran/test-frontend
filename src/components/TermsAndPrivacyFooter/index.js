import React from 'react'
import { Link } from 'react-router-dom'
import { Container, Message } from './styles'
import i18next from 'i18next'
import { withTranslation } from 'react-i18next'

const TermsAndPrivacyFooterComponent = ({ isSurvey, t, showMessage }) => {
  const tkey = `survey.refresh_if_stuck.${
    process.env.REACT_APP_THEME ? process.env.REACT_APP_THEME : `default`
  }`
  return (
    <Container isSurvey={!!isSurvey}>
      <Link to='/terms-of-use' target='_blank'>
        {i18next.t('survey.termsOfUse')}
      </Link>
      {showMessage && <Message>{i18next.t(tkey)}</Message>}
    </Container>
  )
}

export default withTranslation()(TermsAndPrivacyFooterComponent)
