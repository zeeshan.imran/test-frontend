import React from 'react'
import { shallow } from 'enzyme'
import SelectedItemsBanner from '.'

describe('SelectedItemsBanner', () => {
  let testRender
  let selectedItems

  beforeEach(() => {
    selectedItems = 1
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SelectedItemsBanner', async () => {
    testRender = shallow(<SelectedItemsBanner selectedItems={selectedItems} />)
    expect(testRender).toMatchSnapshot()
  })
})
