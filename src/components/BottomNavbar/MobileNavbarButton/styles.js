import styled from 'styled-components'
import NavbarButton from '../NavbarButton'

export const CustomNavbarButton = styled(NavbarButton)`
  width: 30%;
  margin-left: 1rem;
  margin-right: 1rem;
  @media (max-width: 600px) {
    span {
    white-space: pre-line;
  }
  }
 
`
