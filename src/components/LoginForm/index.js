import React, { useState } from 'react'
import { Form, Icon } from 'antd'
import { isEmpty } from 'ramda'
import Input from '../Input'
import Error from '../Error'
import OnboardingForm from '../OnboardingForm'
import SelectLanguage from '../SelectLanguage'
import {
  ButtonsContainer,
  FormErrorsContainer,
  SignUpTextContainer,
  StyledButton,
  ForgotPasswordLink,
  SelectLanguageLink
} from './styles'
import { useTranslation } from 'react-i18next'
const { REACT_APP_THEME } = process.env
const LoginForm = ({
  email,
  password,
  loginError,
  handleChange,
  handleSubmit,
  handleNavigateToRequestAccount,
  handleNavigateToCreateTasterAccount,
  handleNavigateToForgotPassword,
  loading,
  errors,
  resendEmailButton,
  resendVerification
}) => {
  const { email: emailError, password: passwordError } = errors
  const [showModal, setShowModal] = useState(false)
  const canLogin = !!email && !!password && isEmpty(errors)
  const { t } = useTranslation()

  return (
    <React.Fragment>
      <SelectLanguage showModal={showModal} setShowModal={setShowModal} />

      <OnboardingForm onSubmit={handleSubmit}>
        <Form.Item
          help={emailError}
          validateStatus={emailError ? 'error' : 'success'}
        >
          <Input
            name='email'
            value={email}
            onChange={handleChange}
            prefix={<Icon type='user' />}
            placeholder={t('placeholders.username')}
          />
        </Form.Item>
        <Form.Item
          help={passwordError}
          validateStatus={passwordError ? 'error' : 'success'}
        >
          <Input
            name='password'
            value={password}
            onChange={handleChange}
            type={'password'}
            prefix={<Icon type='lock' />}
            placeholder={t('placeholders.password')}
            onPressEnter={handleSubmit}
          />
        </Form.Item>
        {!!loginError && (
          <FormErrorsContainer>
            <Error>{loginError || ''}</Error>
          </FormErrorsContainer>
        )}
        <ForgotPasswordLink onClick={handleNavigateToForgotPassword}>
          {t('forgotPassword')}?
        </ForgotPasswordLink>
        <SelectLanguageLink
          onClick={() => {
            setShowModal(true)
          }}
        >
          {t(
            `components.createTasterAccount.forms.second.langugage.placeholder`
          )}
        </SelectLanguageLink>
        {resendEmailButton && (
          <ForgotPasswordLink onClick={resendVerification}>
            {t('components.login.resendVerificatioEmail')}
          </ForgotPasswordLink>
        )}
        <ButtonsContainer>
          <StyledButton
            loading={loading}
            type={canLogin ? 'primary' : 'disabled'}
            onClick={handleSubmit}
            data-testid='submit-button'
          >
            {t('signIn')}
          </StyledButton>
          <SignUpTextContainer>
            {/* <Text>{t('dontHaveAccount')} </Text>
          <Text>{t('requestAccount')} </Text>
          <SignUpText onClick={handleNavigateToRequestAccount}>
            {t('requestOperatorAccount')}
          </SignUpText> */}
            {REACT_APP_THEME === 'default' ? (
              <StyledButton
                type={'primary'}
                onClick={handleNavigateToCreateTasterAccount}
                data-testid='submit-button'
                changecolor="true"
              >
                {/* {"Sign up to be a Taster"} */}
                {t('requestAccountOr')}
              </StyledButton>
            ) : null}
          </SignUpTextContainer>
        </ButtonsContainer>
      </OnboardingForm>
    </React.Fragment>
  )
}

export default LoginForm
