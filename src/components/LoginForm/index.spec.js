import React from 'react'
import { mount } from 'enzyme'
import LoginForm from '.'
import Button from './Button'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('LoginForm', () => {
  let testRender
  let email
  let password
  let loginError
  let handleChange
  let handleSubmit
  let handleNavigateToRequestAccount
  let handleNavigateToForgotPassword
  let loading
  let errors

  beforeEach(() => {
    email = 'flavor-wiki@gmail.com'
    password = 'flavor-wiki@gmail.com'
    loginError = 'Invalid Credentials'
    handleChange = jest.fn()
    handleSubmit = jest.fn()
    handleNavigateToRequestAccount = ''
    handleNavigateToForgotPassword = ''
    loading = true
    errors = []
  })

  afterEach(() => {
    testRender.unmount()
  })

  // test('should render LoginForm input', async () => {
  //     testRender = mount(
  //         <LoginForm
  //             email={email}
  //             password={password}
  //             loginError={loginError}
  //             handleChange={handleChange}
  //             handleSubmit={handleSubmit}
  //             handleNavigateToRequestAccount={handleNavigateToRequestAccount}
  //             handleNavigateToForgotPassword={handleNavigateToForgotPassword}
  //             loading={loading}
  //             errors={errors}
  //         />
  //     )

  //     expect(testRender.find(LoginForm)).toHaveLength(1)

  //     testRender.find(Button).props('onClick').onClick()

  //     expect(handleSubmit).toHaveBeenCalled()
  // })

  test('should render LoginForm email input', async () => {
    testRender = mount(
      <LoginForm
        email={email}
        handleChange={handleChange}
        loading={loading}
        errors={errors}
        handleSubmit={handleSubmit}
      />
    )
    testRender
      .findWhere(c => c.name() === 'Input' && c.prop('name') === 'email')
      .first()
      .prop('onChange')({ target: { email: 'flavor-wiki@gmail.com' } })

    expect(handleChange).toHaveBeenCalledWith({
      target: { email: 'flavor-wiki@gmail.com' }
    })
  })

  test('should render LoginForm password input', async () => {
    testRender = mount(
      <LoginForm
        password={password}
        handleChange={handleChange}
        loading={loading}
        errors={errors}
        handleSubmit={handleSubmit}
      />
    )
    testRender
      .findWhere(c => c.name() === 'Input' && c.prop('name') === 'password')
      .first()
      .prop('onChange')({ target: { password: 'flavor-wiki@gmail.com' } })

    expect(handleChange).toHaveBeenCalledWith({
      target: { password: 'flavor-wiki@gmail.com' }
    })
  })
})
