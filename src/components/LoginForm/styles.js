import styled from 'styled-components'
import { family } from '../../utils/Fonts'
import colors from '../../utils/Colors'
import { DEFAULT_COMPONENTS_MARGIN } from '../../utils/Metrics'
import Button from './Button'

export const ButtonsContainer = styled.div`
  margin-top: ${DEFAULT_COMPONENTS_MARGIN}rem;
  text-align: center;
  display: flex;
  flex-direction: column;
`

export const SignUpTextContainer = styled.div`
  margin-top: ${DEFAULT_COMPONENTS_MARGIN}rem;
`

export const Text = styled.span`
  font-family: ${family.primaryLight};
  font-size: 1.4rem;
  line-height: 1.57;
  letter-spacing: 0.06rem;
  margin-top: ${DEFAULT_COMPONENTS_MARGIN}rem;
  margin-bottom: ${DEFAULT_COMPONENTS_MARGIN}rem;
  user-select: none;
`

export const FormErrorsContainer = styled.div`
  margin: ${DEFAULT_COMPONENTS_MARGIN}rem 0;
`

export const SignUpText = styled(Text)`
  color: ${colors.LIGHT_OLIVE_GREEN};
  cursor: pointer;
  font-family: ${family.primaryBold};
`

export const ForgotPasswordLink = styled.p`
  font-family: ${family.primaryLight};
  font-size: 1.4rem;
  line-height: 1.57;
  color: ${colors.LIGHT_BLUE};
  text-align: right;
  cursor: pointer;
`
export const SelectLanguageLink = styled.p`
  font-family: ${family.primaryLight};
  font-size: 1.4rem;
  line-height: 1.57;
  color: ${colors.LIGHT_BLUE};
  text-align: right;
  cursor: pointer;
`
export const StyledButton = styled(Button)`
  background-color:${props => props.changecolor ? "#1890ff" : "#8aba5c"};
  transition: all 0.2s;
  &:hover {
    background-color:${props => props.changecolor ? "#2e5496" : "#19902B"};
    transform: translateY(-3px);
    box-shadow: 0 10px 20px rgba(0, 0, 0, 0.2);
    &::after {
      transform: scaleX(1.4) scaleY(1.6);
      opacity: 0;
    }
  }

  &:active {
    transform: translateY(-1px);
    box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  }
  &::after {
    content: '';
    display: inline-block;
    height: 100%;
    width: 100%;
    border-radius: 100px;
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    transition: all 0.4s;
  }
`
