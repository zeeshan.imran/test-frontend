import styled from 'styled-components'
import BaseButton from '../Button'

export const Button = styled(BaseButton)`
  width: ${({ fullWidth }) => (fullWidth ? '100%' : '30rem')};
`

export const Container = styled.div`
  width: 100%;
  display: flex;
  justify-content: ${({ fullWidth }) => (fullWidth ? 'center' : 'flex-start')};
`
