import React from 'react'
import { mount } from 'enzyme'
import CategoriesTable from '.'

describe('CategoriesTable', () => {
  let testRender
    let selected
    let tableProps


  beforeEach(() => {
    selected = true
    tableProps = {}

  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render CategoriesTable', async () => {
    
    testRender = mount(
      <CategoriesTable
        selected={selected}
        tableProps={tableProps}

      />
    )
    expect(testRender.find(CategoriesTable)).toHaveLength(1)
  })
})
