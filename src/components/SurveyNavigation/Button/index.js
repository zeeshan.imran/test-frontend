import React from 'react'
import useResponsive from '../../../utils/useResponsive'

import { Container, CustomButton } from './styles'

const Button = ({ disabled, type = 'primary', ...otherProps }) => {
  const { mobile } = useResponsive()
  return (
    <Container fullWidth={mobile}>
      <CustomButton type={disabled ? 'disabled' : type} {...otherProps} />
    </Container>
  )
}

export default Button
