import React from 'react'
import { mount } from 'enzyme'
import SurveysGrid from './index';
import { Router } from 'react-router-dom'
import history from '../../history'
import SurveyCard from '../../containers/SurveyCard';

describe('SurveysGrid', () => {
    let surveys;
    let testRender;

    beforeEach(() => {

        surveys = [
            {
                id:1,
                title:'survey 1',
            },
            {
                id: 2,
                title:'survey 2',
            }
        ];
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render SurveysGrid', async () => {
        testRender = mount(
            <Router history={history}>
                <SurveysGrid surveys={surveys} />
            </Router>
        )
        expect(testRender.find(SurveyCard)).toHaveLength(2)
    })
});