import React, { useCallback } from 'react'
import { Menu } from 'antd'
import html2canvas from 'html2canvas'
import { useChartContext } from '../../contexts/ChartContext'
import * as jsPDF from 'fw-jspdf'

const inlineSVGStyles = targetElem => {
  const transformProperties = [
    'fill',
    'color',
    'stroke',
    'font',
    'font-family',
    'font-weight',
    'font-size',
    'stroke-linejoin',
    'stroke-linecap',
    'stroke-width',
    'paint-order'
  ]

  let svgElems = Array.from(targetElem.getElementsByTagName('svg'))

  for (let svgElement of svgElems) {
    recurseElementChildren(svgElement)
  }

  function recurseElementChildren (node) {
    if (!node.style) return

    let styles = getComputedStyle(node)

    for (let transformProperty of transformProperties) {
      node.style[transformProperty] = styles[transformProperty]
    }

    for (let child of Array.from(node.childNodes)) {
      recurseElementChildren(child)
    }
  }
}

const canvasToPDF = async (canvas, { margin = 40, filename }) => {
  const pdf = new jsPDF('p', 'px', 'a4')
  const BEST_WIDTH = Math.max(canvas.width, 795)
  const BEST_ELEMENT_WIDTH = BEST_WIDTH - 2 * margin

  const canvasWidth = canvas.width
  const canvasHeight = canvas.height
  const pageWidth = pdf.internal.pageSize.getWidth()
  const pageHeight = pdf.internal.pageSize.getHeight()

  const pageMargin = Number.parseInt((margin * pageWidth) / BEST_WIDTH, 10)
  const imgWidth = pageWidth - 2 * pageMargin
  const imgHeight = pageHeight - 2 * pageMargin

  let addImageWH = false
  if (canvasWidth <= BEST_ELEMENT_WIDTH) addImageWH = true

  let position = 0
  while (position <= canvasHeight) {
    const tempCanvas = document.createElement('canvas')
    const tempHeight =
      (Math.max(BEST_ELEMENT_WIDTH, canvasWidth) * imgHeight) / imgWidth
    const captureHeight = Math.min(tempHeight, canvasHeight - position)

    tempCanvas.width = canvasWidth
    tempCanvas.height = tempHeight
    const tempCtx = tempCanvas.getContext('2d')
    const bodyCtx = canvas.getContext('2d')
    const imgData = bodyCtx.getImageData(
      0,
      position,
      canvasWidth,
      captureHeight
    )
    tempCtx.putImageData(imgData, 0, 0, 0, 0, canvasWidth, tempHeight)
    // fill bottom space
    tempCtx.rect(0, captureHeight - 1, canvasWidth, tempHeight - captureHeight)
    tempCtx.fillStyle = '#FFFFFF'
    tempCtx.strokeStyle = '#FFFFFF'
    tempCtx.fill()

    const tempCanvasData = tempCanvas.toDataURL('image/png')
    if (addImageWH) {
      pdf.addImage(tempCanvasData, 'PNG', pageMargin, pageMargin)
    } else {
      pdf.addImage(
        tempCanvasData,
        'PNG',
        pageMargin,
        pageMargin,
        imgWidth,
        imgHeight
      )
    }

    position += tempHeight
    if (position <= canvasHeight) {
      pdf.addPage()
    }
  }
  await pdf.save(filename, { returnPromise: true })
}

const ChartDownloadMenu = (chartId, chartName, t, onLoadingChange) => {
  const ctx = useChartContext()
  const downloadImage = useCallback(
    (mimeType = 'image/png') => {
      ctx.setShowAll(true)
      ctx.setPrintView(true)
      onLoadingChange(true)
      setTimeout(async () => {
        try {
          const element = document.querySelector(`.${chartId} .ant-card-body`)
          inlineSVGStyles(element)
          element
            .querySelectorAll('.invisibleForExport')
            .forEach(el => (el.style.display = 'none'))
          const format = mimeType.replace('image/', '')
          const canvas = await html2canvas(element)
          element
            .querySelectorAll('.invisibleForExport')
            .forEach(el => (el.style.display = ''))
          let a = document.createElement('a')
          a.href = canvas.toDataURL(mimeType)
          a.download = `${chartName}.${format}`
          document.body.appendChild(a)
          a.click()
          document.body.removeChild(a)
        } catch (ex) {
          //
        }
        ctx.setShowAll(false)
        ctx.setPrintView(false)
        onLoadingChange(false)
      }, 1000)
    },
    [chartId, chartName, ctx, onLoadingChange]
  )

  const downloadPdf = useCallback(() => {
    ctx.setShowAll(true)
    ctx.setPrintView(true)
    onLoadingChange(true)
    setTimeout(async () => {
      try {
        const element = document.querySelector(`.${chartId} .ant-card-body`)
        inlineSVGStyles(element)
        const canvas = await html2canvas(element)
        await canvasToPDF(canvas, { filename: `${chartName}.pdf` })
      } catch (ex) {
        //
      }
      ctx.setShowAll(false)
      ctx.setPrintView(false)
      onLoadingChange(false)
    }, 1000)
  }, [chartId, chartName, ctx, onLoadingChange])

  return (
    <Menu>
      <Menu.Item key='1' onClick={() => downloadImage('image/png')}>
        {t('components.downloadMenu.downloadPng')}
      </Menu.Item>
      <Menu.Item key='2' onClick={() => downloadImage('image/jpeg')}>
        {t('components.downloadMenu.downloadJpeg')}
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key='4' onClick={downloadPdf}>
        {t('components.downloadMenu.downloadPdf')}
      </Menu.Item>
    </Menu>
  )
}
export default ChartDownloadMenu
