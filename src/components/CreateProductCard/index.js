import React, { useEffect, useRef } from 'react'
import { Form, Row, Col } from 'antd'
import { useTranslation } from 'react-i18next'
import { Formik } from 'formik'
import PubSub from 'pubsub-js'
import Input from '../Input'
import Button from '../Button'
import { Container, CheckboxSpacer, PrefixedInput } from './styles'
import UploadProductPicture from '../../containers/UploadProductPicture/index'
import { PUBSUB } from '../../utils/Constants'
import { validateProductSchema } from '../../validates'
import HelperIcon from '../HelperIcon'
import { StyledCheckbox } from '../StyledCheckBox'
import defaults from '../../defaults'
import DaysAndTimePicker from '../DaysAndTimePicker'

const isFlavorwiki = process.env.REACT_APP_THEME === 'default'

const CreateProductCard = ({
  onChange,
  handleRemove,
  name,
  brand,
  photo,
  reward,
  rejectedReward,
  delayToNextProduct,
  extraDelayToNextProduct,
  isAvailableError,
  isAvailable,
  prefix,
  suffix,
  editMode,
  disabledInputBrandName,
  products = [],
  disableProductAvailability,
  internalName,
  isProductRewardsRule,
  screenOutValue,
  dualCurrency,
  amountInDollar,
  isFlavorwikiOperator = false,
  addDelayToSelectNextProductAndNextQuestion = false
}) => {
  const { REACT_APP_THEME } = process.env
  const showColumn = REACT_APP_THEME === 'default'
  const isStrictMode = editMode === 'strict'
  const formRef = useRef()
  const { t } = useTranslation()
  useEffect(() => {
    const token = PubSub.subscribe(PUBSUB.VALIDATE_SURVEY_PRODUCTS, () => {
      if (formRef.current) {
        formRef.current.validateForm()
      }
    })
    return () => {
      PubSub.unsubscribe(token)
    }
  }, [])

  useEffect(() => {
    if (
      isProductRewardsRule &&
      (reward || reward === 0) &&
      (rejectedReward || rejectedReward === 0) &&
      (dualCurrency || dualCurrency === 0)
    ) {
      onChange({ reward: 0, rejectedReward: 0, dualCurrency: 0 })
    }
  }, [reward, isProductRewardsRule, rejectedReward, dualCurrency, onChange])

  return (
    <Formik
      ref={formRef}
      validationSchema={validateProductSchema(isProductRewardsRule)}
      initialValues={{
        name,
        brand,
        reward,
        isAvailable,
        rejectedReward,
        delayToNextProduct,
        extraDelayToNextProduct,
        amountInDollar,
        internalName
      }}
      render={({ values, errors, setFieldValue }) => {
        const {
          name: nameError,
          brand: brandError,
          reward: rewardError,
          rejectedReward: rejectedRewardError,
          amountInDollar: amountInDollarError
        } = errors
        return (
          <Container>
            <Row type='flex' justify='end'>
              <Col>
                {isStrictMode && (
                  <HelperIcon
                    placement='bottomRight'
                    helperText={t('tooltips.product.canNotDelete')}
                  />
                )}
                {!isStrictMode && (
                  <Button type='red' onClick={handleRemove}>
                    X
                  </Button>
                )}
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={{ span: 8 }}>
                <Row xs={{ span: 8 }}>
                  <Form.Item
                    help={nameError}
                    validateStatus={nameError ? 'error' : 'success'}
                  >
                    <Input
                      name='name'
                      value={values.name}
                      onChange={event => {
                        setFieldValue('name', event.target.value)
                        onChange({ name: event.target.value })
                      }}
                      onBlur={event => {
                        if (!values.internalName) {
                          setFieldValue('internalName', values.name)
                          onChange({ internalName: values.name })
                        }
                      }}
                      label={t('components.product.name')}
                      tooltip={t('tooltips.product.name')}
                      size='default'
                      required
                    />
                  </Form.Item>
                </Row>
                {showColumn && !isProductRewardsRule && isFlavorwikiOperator && isFlavorwiki ? (
                  <React.Fragment>
                    <Row xs={{ span: 8 }}>
                      <Form.Item
                        help={rewardError}
                        validateStatus={rewardError ? 'error' : 'success'}
                      >
                        <PrefixedInput
                          largePrefix={
                            (prefix && prefix.length > 2) ||
                            (suffix && suffix.length > 2)
                          }
                        >
                          <Input
                            min={0}
                            max={defaults.MAX_LIMIT_REFERRAL}
                            precision={2}
                            label={t('components.product.reward')}
                            tooltip={t('tooltips.product.reward')}
                            name='reward'
                            value={values.reward}
                            placeholder={t('components.product.reward')}
                            onChange={event => {
                              setFieldValue(
                                'reward',
                                parseFloat(event.target.value, 10)
                              )
                              onChange({
                                reward: parseFloat(event.target.value, 10)
                              })
                            }}
                            size='default'
                            prefix={prefix || suffix}
                            type={`number`}
                          />
                        </PrefixedInput>
                      </Form.Item>
                    </Row>
                    {dualCurrency && isFlavorwiki && (
                      <Row xs={{ span: 8 }}>
                        <Form.Item
                          help={amountInDollarError}
                          validateStatus={
                            amountInDollarError ? 'error' : 'success'
                          }
                        >
                          <PrefixedInput
                            largePrefix={
                              (prefix && prefix.length > 2) ||
                              (suffix && suffix.length > 2)
                            }
                          >
                            <Input
                              min={0}
                              max={defaults.MAX_LIMIT_REFERRAL}
                              precision={2}
                              label={t('Reward($)')}
                              tooltip={t('tooltips.product.reward')}
                              name='amountInDollar'
                              value={values.amountInDollar}
                              placeholder={t('components.product.reward')}
                              onChange={event => {
                                setFieldValue(
                                  'amountInDollar',
                                  parseFloat(event.target.value, 10)
                                )
                                onChange({
                                  amountInDollar: parseFloat(
                                    event.target.value,
                                    10
                                  )
                                })
                              }}
                              size='default'
                              prefix={'$'}
                              type={`number`}
                            />
                          </PrefixedInput>
                        </Form.Item>
                      </Row>
                    )}
                  </React.Fragment>
                ) : null}
                <Row xs={{ span: 8 }}>
                  <Form.Item>
                    <Input
                      name='internalName'
                      value={values.internalName}
                      onChange={event => {
                        setFieldValue('internalName', event.target.value)
                        onChange({ internalName: event.target.value })
                      }}
                      label='Internal name'
                      tooltip='Product internal name'
                      size='default'
                    />
                  </Form.Item>
                </Row>
              </Col>
              <Col xs={{ span: 8 }}>
                <Row xs={{ span: 8 }}>
                  <Form.Item
                    help={brandError}
                    validateStatus={brandError ? 'error' : 'success'}
                  >
                    <Input
                      required
                      name='brand'
                      value={values.brand}
                      onChange={event => {
                        setFieldValue('brand', event.target.value)
                        onChange({ brand: event.target.value })
                      }}
                      label={t('components.product.brand')}
                      tooltip={t('tooltips.product.brand')}
                      size='default'
                      disabledInput={disabledInputBrandName}
                    />
                  </Form.Item>
                </Row>
                {/* Reject Reward Start */}
                {showColumn && screenOutValue && !isProductRewardsRule && isFlavorwiki && (
                  <Row xs={{ span: 8 }}>
                    <Form.Item
                      help={rejectedRewardError}
                      validateStatus={rejectedRewardError ? 'error' : 'success'}
                    >
                      <PrefixedInput
                        largePrefix={
                          (prefix && prefix.length > 2) ||
                          (suffix && suffix.length > 2)
                        }
                      >
                        <Input
                          min={0}
                          max={defaults.MAX_LIMIT_REFERRAL}
                          precision={2}
                          label={'Rejected Reward'}
                          tooltip={t('tooltips.product.reward')}
                          name='rejectedReward'
                          value={values.rejectedReward}
                          placeholder={t('components.product.reward')}
                          onChange={event => {
                            setFieldValue(
                              'rejectedReward',
                              parseFloat(event.target.value, 10)
                            )
                            onChange({
                              rejectedReward: parseFloat(event.target.value, 10)
                            })
                          }}
                          size='default'
                          prefix={prefix || suffix}
                          type={`number`}
                        />
                      </PrefixedInput>
                    </Form.Item>
                  </Row>
                )}
                {/* Rejected reward end */}
                <Row xs={{ span: 8 }}>
                  <Form.Item
                    help={
                      isAvailableError && t('validation.products.isAvailable')
                    }
                    validateStatus={isAvailableError ? 'error' : 'success'}
                  >
                    <CheckboxSpacer />
                    <StyledCheckbox
                      name='isAvailable'
                      checked={values.isAvailable}
                      onChange={e => {
                        const { checked: value } = e.target
                        setFieldValue('isAvailable', value)
                        onChange({ isAvailable: value })
                      }}
                      disabled={disableProductAvailability}
                    >
                      {t('components.product.isAvailable')}
                      <HelperIcon
                        placement='bottomRight'
                        helperText={t('tooltips.product.isAvailable')}
                      />
                    </StyledCheckbox>
                  </Form.Item>
                </Row>
              </Col>
              <Col xs={{ span: 8 }}>
                <UploadProductPicture
                  tooltip={t('tooltips.product.picture')}
                  name='photo'
                  value={photo}
                  onChange={picture => onChange({ photo: picture[0] })}
                />
              </Col>
            </Row>
            {/* Add delay for next product selection */}
            {addDelayToSelectNextProductAndNextQuestion && (
              <Row>
                <Form.Item>
                  <DaysAndTimePicker
                    delayToNext={values.delayToNextProduct}
                    extraDelayToNext={values.extraDelayToNextProduct}
                    onChangeDelayToNext={value => {
                      setFieldValue('delayToNextProduct', value)
                      onChange({ delayToNextProduct: value })
                    }}
                    onChangeExtraDelayToNext={value => {
                      setFieldValue('extraDelayToNextProduct', value)
                      onChange({ extraDelayToNextProduct: value })
                    }}
                    buttonText='Add delay to select next product'
                  />
                </Form.Item>
              </Row>
            )}
          </Container>
        )
      }}
    />
  )
}

export default CreateProductCard
