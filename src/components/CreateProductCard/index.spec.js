 import React from 'react'
import { mount } from 'enzyme'
import CreateProductCard from './index'
import PubSub from 'pubsub-js'
import { PUBSUB } from '../../utils/Constants'
import wait from '../../utils/testUtils/waait'
import { Form } from 'antd'

// disable render UploadProductPicture
jest.mock('../../containers/UploadProductPicture', () => () => null)

describe('validate products', () => {
  let render

  afterEach(() => {
    render.unmount()
  })

  test('should show errors after receiving the validate products request', async () => {
    render = mount(<CreateProductCard />)

    render.update()
    await wait(0)

    PubSub.publish(PUBSUB.VALIDATE_SURVEY_PRODUCTS)
    await wait(0)

    render.update()
    await wait(0)

    expect(
      render
        .find(Form.Item)
        .first()
        .text()
    ).toBe('* NameName is required') 

    expect(
      render
        .find(Form.Item)
        .at(3)
        .text()
    ).toBe('Product is available')

  })
})
