import React from 'react'
import { ProductWithOutPictureContainer } from '../styles'

const ProductWithOutPicture = ({ product }) => (
  <ProductWithOutPictureContainer>
    <span>
      {product.name}
    </span>
  </ProductWithOutPictureContainer>
)

export default ProductWithOutPicture
