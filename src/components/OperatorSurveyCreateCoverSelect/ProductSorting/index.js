import React from 'react'
import {
  SortableContainer,
  SortableElement,
  SortableHandle
} from 'react-sortable-hoc'
import { ProductPicturesContainer, ProductContainer } from '../styles'
import ProductPicture from '../ProductPicture'
import ProductWithOutPicture from '../ProductWithOutPicture'

const ProductHandle = SortableHandle(props => {
  const {
    product,
    selectedProduct,
    getProductId,
    handleSelectedProductIdChange
  } = props
  return product.photo ? (
    <ProductContainer key={getProductId(product)}>
      <ProductPicture
        product={product}
        isSelected={product === selectedProduct}
        onClick={product =>
          handleSelectedProductIdChange(getProductId(product))
        }
      />
    </ProductContainer>
  ) : (
    <ProductContainer key={getProductId(product)}>
      <ProductWithOutPicture product={product} />
    </ProductContainer>
  )
})

const SortableItem = SortableElement(props => {
  const { shouldUseDragHandle, ...restProps } = props
  return shouldUseDragHandle && <ProductHandle {...restProps} />
})

const DragAndDropSortAbleItem = SortableContainer(props => {
  const { products, stricted, ...restProps } = props
  return (
    <ProductPicturesContainer>
      {products.map((product, index) => (
        <SortableItem
          key={`item-${product.id ||
            product.clientGeneratedId ||
            product.sortingOrderId ||
            index}`}
          index={index}
          product={product}
          {...restProps}
          disabled={stricted}
        />
      ))}
    </ProductPicturesContainer>
  )
})

export default DragAndDropSortAbleItem
