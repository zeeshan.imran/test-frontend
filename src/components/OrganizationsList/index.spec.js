import React from 'react'
import { mount } from 'enzyme'
import UsersListComponent from './index'

describe('UsersListComponent', () => {
  let testRender
  let isEditOrganization
  let toggleEditOrganizationForm
  let selected
  let handelAdd
  let newOrganizationData
  let setnewOrganizationData
  let handleEdit
  let handleSearch
  let searchBy
  let tableProps

  beforeEach(() => {
    isEditOrganization = false
    toggleEditOrganizationForm = false
    selected = false
    handelAdd = ''
    newOrganizationData = ''
    setnewOrganizationData = ''
    handleEdit = jest.fn()
    handleSearch = jest.fn()
    searchBy = ''
    tableProps = {}
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render UsersListComponent', async () => {
    testRender = mount(
      <UsersListComponent
        isEditOrganization={isEditOrganization}
        toggleEditOrganizationForm={toggleEditOrganizationForm}
        selected={selected}
        handelAdd={handelAdd}
        newOrganizationData={newOrganizationData}
        setnewOrganizationData={setnewOrganizationData}
        handleEdit={handleEdit}
        handleSearch={handleSearch}
        searchBy={searchBy}
        tableProps={tableProps}
      />
    )

    expect(testRender.find(UsersListComponent)).toHaveLength(1)
  })
})
