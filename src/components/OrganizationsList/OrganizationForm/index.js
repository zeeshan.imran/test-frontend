import kebabCase from 'lodash.kebabcase'
import React, { useState, useEffect, useRef } from 'react'
import * as Yup from 'yup'
import { Formik } from 'formik'
import { Modal, Form, Row } from 'antd'
import Input from '../../Input'
import { useTranslation } from 'react-i18next'
import i18n from '../../../utils/internationalization/i18n'
import InputTooltip from './InputTooltip'
import { Title } from './styles.js'
import colors from '../../../utils/Colors';

const validationSchema = Yup.object().shape({
  name: Yup.string()
    .test(
      'not-empty',
      i18n.t('validation.organizationForm.name.required'),
      value => !/^[ ]+$/.test(value)
    )
    .test(
      'max-length',
      i18n.t('validation.organizationForm.name.maxLength'),
      value => value && value.length <= 50
    )
    .test(
      'not-empty',
      i18n.t('validation.organizationForm.name.withoutSpaces'),
      value => /^(?! ).*/.test(value)
    )
    .required(i18n.t('validation.organizationForm.name.required')),
  uniqueName: Yup.string()
    .required(i18n.t('validation.organizationForm.uniqueName.required'))
    .test(
      'not-empty',
      i18n.t('validation.organizationForm.uniqueName.required'),
      value => !/^[ \t\r\n]+$/.test(value)
    )
    .matches(
      /^[0-9a-zA-Z-]+$/,
      i18n.t('validation.organizationForm.uniqueName.matches')
    )
})

const uniqueNameTooltips = {
  edit: (
    <InputTooltip
      titleKey='tooltips.forms.uniqueNameTooltip.edit'
      icon='exclamation-circle'
      color={colors.RED}
    />
  ),
  add: (
    <InputTooltip
      titleKey='tooltips.forms.uniqueNameTooltip.add'
      icon='warning'
      color='#f6b93b'
    />
  )
}

const OrganizationForm = ({
  editOrganizationId,
  onOrganizationFormClose,
  newOrganizationData,
  onSubmit,
  modalType
}) => {
  const { t } = useTranslation()
  const isAdding = modalType === 'add'

  const [isUniqueNameTouched, setIsUniqueNameTouched] = useState(false)
  const [isMounted, setIsMounted] = useState()

  const formRef = useRef()

  useEffect(() => {
    if (!isMounted && formRef.current && formRef.current.validateForm) {
      setIsMounted(true)
      formRef.current.validateForm()
    } else if (!isMounted) {
      setIsMounted(false)
    }
    setIsUniqueNameTouched(false)
  }, [editOrganizationId])

  if (!editOrganizationId) {
    return null
  }

  return (
    <Formik
      ref={formRef}
      enableReinitialize
      validationSchema={validationSchema}
      initialValues={newOrganizationData}
      render={({ errors, setFieldValue, values, isValid }) => {
        return (
          <Modal
            title={
              isAdding
                ? <Title>{t('components.organizationForm.addTitle')}</Title>
                : <Title>{t('components.organizationForm.editTitle')}</Title>
            }
            visible={editOrganizationId}
            okButtonProps={{ htmlType: 'submit', disabled: !isValid }}
            onOk={() => onSubmit(values)}
            onCancel={() => onOrganizationFormClose(false)}
          >
            <Row>
              <Form.Item
                help={errors.name}
                validateStatus={errors.name ? 'error' : 'success'}
              >
                <Input
                  name='name'
                  value={values.name}
                  onChange={event => {
                    const name = event.target.value
                    if (isAdding && !isUniqueNameTouched) {
                      const uniqueName = kebabCase(name)
                      setFieldValue('uniqueName', uniqueName)
                    }

                    setFieldValue('name', name)
                  }}
                  label={t('components.organizationForm.organizationName')}
                  size='default'
                  required
                />
              </Form.Item>
            </Row>
            <Row>
              <Form.Item
                help={errors.uniqueName}
                validateStatus={errors.uniqueName ? 'error' : 'success'}
              >
                <Input
                  name='uniqueName'
                  disabled={!isAdding}
                  value={values.uniqueName}
                  onChange={event => {
                    setIsUniqueNameTouched(true)
                    setFieldValue('uniqueName', event.target.value)
                  }}
                  label={t('components.organizationForm.uniqueName')}
                  size='default'
                  required
                  suffix={uniqueNameTooltips[isAdding ? 'add' : 'edit']}
                />
              </Form.Item>
            </Row>
          </Modal>
        )
      }}
    />
  )
}

export default OrganizationForm
