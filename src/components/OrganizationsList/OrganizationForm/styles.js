import styled from 'styled-components'
import { family } from '../../../utils/Fonts'

export const Title = styled.span`
  font-family: ${family.primaryBold};
  font-size: 2rem;
  color: rgba(0, 0, 0, 0.85);
`
