import React from 'react'
import Table from './Table'
import SearchBar from '../SearchBar'
import Button from '../Button'
import { Container, HeaderContainer, PageTitle, SearchBarContainer } from './styles'
import OrganizationForm from './OrganizationForm'
import { useTranslation } from 'react-i18next'
import { getAuthenticatedUser } from '../../utils/userAuthentication'

const OrganizationsListComponent = ({
  isArchivedList = false,
  editOrganizationId,
  onEditOrganizationIdChange,
  onUnarchivedOrganization,
  selected,
  newOrganizationData,
  setnewOrganizationData,
  setOrganizationData,
  setModalType,
  modalType,
  onOrganizationFormSubmit,
  handleSearch,
  searchBy,
  loading,
  ...tableProps
}) => {
  const { t } = useTranslation()
  const currentUser = getAuthenticatedUser()

  return (
    <Container>
      <HeaderContainer>
        {isArchivedList && (
          <PageTitle>{t('components.operatorSurveys.archivedOrganizations')}</PageTitle>
        )}

        {!isArchivedList && (
          <Button
            onClick={() => {
              setModalType('add')
              setOrganizationData({})
              onEditOrganizationIdChange(true)
            }}
          >
            {t('components.organizationsList.add')}
          </Button>
        )}

        <SearchBarContainer>
          <SearchBar
            placeholder={t('components.organizationsList.search')}
            withIcon
            handleChange={handleSearch}
            value={searchBy}
          />
        </SearchBarContainer>
      </HeaderContainer>
      <Table
        {...tableProps}
        loading={loading}
        isArchivedList={isArchivedList}
        currentUser={currentUser}
        setModalType={setModalType}
        onEditOrganization={org => onEditOrganizationIdChange(org.id)}
        onUnarchivedOrganization={onUnarchivedOrganization}
      />
      {editOrganizationId && (
        <OrganizationForm
          id='organization form'
          editOrganizationId={editOrganizationId}
          onOrganizationFormClose={() => onEditOrganizationIdChange(false)}
          newOrganizationData={newOrganizationData}
          onSubmit={onOrganizationFormSubmit}
          modalType={modalType}
        />
      )}
    </Container>
  )
}

export default OrganizationsListComponent
