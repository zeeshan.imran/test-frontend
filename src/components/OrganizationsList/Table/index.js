import React from 'react'
import { Table as AntTable } from 'antd'
import { DEFAULT_N_ELEMENTS_PER_PAGE } from '../../../utils/Constants'
import './index.css'
import i18n from '../../../utils/internationalization/i18n'

import { ColumnContentAligner, Action, Separator } from './styles'

export const nameColumn = {
  title: i18n.t('components.organizationsList.nameColumnTitle'),
  width: '30%',
  dataIndex: 'name',
  render: (_, rowData) => <span>{rowData.name}</span>,
  sorter: () => false
}

export const createdByColumn = {
  title: i18n.t('components.organizationsList.createdColumnTitle'),
  dataIndex: 'createdBy',
  width: '20%',
  render: (_, rowData) => (
    <span>
      {rowData.createdBy
        ? rowData.createdBy.fullName || rowData.createdBy.emailAddress
        : ''}
    </span>
  ),
  sorter: () => false,
  className: 'column'
}

export const modifiedByColumn = {
  title: i18n.t('components.organizationsList.modifiedColumnTitle'),
  width: '20%',
  dataIndex: 'modifiedBy',
  render: (_, rowData) => (
    <span>
      {rowData.modifiedBy
        ? rowData.modifiedBy.fullName || rowData.modifiedBy.emailAddress
        : ''}
    </span>
  ),
  sorter: () => false,
  className: 'column'
}

const userNumberColumn = {
  title: i18n.t('components.organizationsList.usersColumnTitle'),
  width: '15%',
  dataIndex: 'usersNumber',
  sorter: () => false,
  className: 'column'
}

const Table = ({
  isArchivedList,
  currentUser = {},
  organizations,
  handleRemove,
  onUnarchivedOrganization,
  onEditOrganization,
  page,
  organizationsCount,
  onTableChange,
  orderBy,
  orderDirection,
  setModalType,
  loading
}) => {
  const defaultColumns = [
    nameColumn,
    createdByColumn,
    modifiedByColumn,
    userNumberColumn,
    {
      title: i18n.t('components.organizationsList.actionsColumnTitle'),
      width: '15%',
      dataIndex: '',
      render: (_, rowData) => {
        return (
          <ColumnContentAligner>
            {isArchivedList && (
              <Action
                onClick={() => {
                  onUnarchivedOrganization(rowData.id)
                }}
              >
                {i18n.t('unarchivedOrganization.unarchived')}
              </Action>
            )}

            {!isArchivedList && (
              <React.Fragment>
                <Action
                  onClick={() => {
                    setModalType('edit')
                    onEditOrganization(rowData)
                  }}
                >
                  {i18n.t('components.organizationsList.edit')}
                </Action>

                {currentUser.organization &&
                  currentUser.organization.id !== rowData.id && (
                    <React.Fragment>
                      <Separator />
                      <Action onClick={() => handleRemove(rowData.id)}>
                        {i18n.t('components.organizationsList.delete')}
                      </Action>
                    </React.Fragment>
                  )}
              </React.Fragment>
            )}
          </ColumnContentAligner>
        )
      }
    }
  ]
  const columns = defaultColumns.map(col => ({
    ...col,
    sortOrder: col.dataIndex === orderBy ? orderDirection : undefined
  }))

  return (
    <AntTable
      loading={loading}
      key='organizationsTable'
      rowKey={record => record.id}
      columns={columns}
      pagination={
        organizationsCount > 10
          ? {
              pageSize: DEFAULT_N_ELEMENTS_PER_PAGE,
              total: organizationsCount,
              current: parseInt(page, 10) + 1
            }
          : false
      }
      onChange={(paginationConfig, filtersConfig, sortingConfig) => {
        onTableChange(
          paginationConfig.current,
          sortingConfig.field,
          sortingConfig.order
        )
      }}
      dataSource={organizations}
      tableLayout='fixed'
    />
  )
}

export default Table
