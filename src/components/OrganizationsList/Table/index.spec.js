import React from 'react'
import { mount } from 'enzyme'
import Table, { nameColumn, createdByColumn, modifiedByColumn } from '.'
import { Action } from './styles'

describe('Table', () => {
  let testRender
  let organizations
  let handleChangeSelection
  let handleRemove
  let handleEditOrganization
  let setModalType
  let page
  let organizationsCount
  let onTableChange
  let toggleEditOrganizationForm

  beforeEach(() => {
    organizations = [
      {
        id: '1',
        modifiedBy: { emailAddress: 'test@test.com' },
        createdBy: { emailAddress: 'test@test.com' },
        name: 'AA',
        usersNumber: 0
      },
      {
        id: '2',
        modifiedBy: { emailAddress: 'test2@test.com' },
        createdBy: { emailAddress: 'test2@test.com' },
        name: 'B',
        usersNumber: 0
      }
    ]
    handleChangeSelection = true
    handleRemove = jest.fn()
    handleEditOrganization = jest.fn()
    setModalType = jest.fn()
    page = 1
    organizationsCount = jest.fn()
    onTableChange = jest.fn()
    toggleEditOrganizationForm = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Table', () => {
    testRender = mount(
      <Table
        organizations={organizations}
        handleChangeSelection={handleChangeSelection}
        handleRemove={handleRemove}
        onEditOrganization={handleEditOrganization}
        page={page}
        organizationsCount={organizationsCount}
        onTableChange={onTableChange}
        setModalType={setModalType}
      />
    )

    expect(testRender.find(Table)).toHaveLength(1)
  })

  test('should call on click when edit', async () => {
    testRender = mount(
      <Table
        organizations={organizations}
        onEditOrganization={handleEditOrganization}
        setModalType={setModalType}
      />
    )
    testRender
      .find(Action)
      .first()
      .simulate('click')

    expect(handleEditOrganization).toHaveBeenCalled()
  })

  test('should call on click when remove', () => {
    testRender = mount(
      <Table
        currentUser={{ organization: { id: '3' } }}
        organizations={organizations}
        handleRemove={handleRemove}
        toggleEditOrganizationForm={toggleEditOrganizationForm}
        setModalType={setModalType}
      />
    )
    testRender
      .find(Action)
      .last()
      .simulate('click')

    expect(handleRemove).toHaveBeenCalled()
  })

  test('date sorter', () => {
    testRender = mount(
      <Table
        organizations={organizations}
        handleRemove={handleRemove}
        toggleEditOrganizationForm={toggleEditOrganizationForm}
        setModalType={setModalType}
      />
    )

    const result = nameColumn.sorter([])
    expect(result).toBe(false)

    const result1 = createdByColumn.sorter([])
    expect(result1).toBe(false)

    const result2 = modifiedByColumn.sorter([])
    expect(result2).toBe(false)
  })
})
