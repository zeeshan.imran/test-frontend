import React, { memo } from 'react'
import { pluck, uniq } from 'ramda'
import startCase from 'lodash.startcase'
import capitalize from 'lodash.capitalize'
import { useTranslation } from 'react-i18next'
import HelperIcon from '../HelperIcon'
import {
  Container,
  Title,
  WideButton,
  ButtonText,
  ActionContainer
} from './styles'
import { StyledCheckbox } from '../StyledCheckBox'
import { NEVER_REQUIRED_QUESTIONS_TYPES } from '../../utils/Constants'
import TooltipWrapper from '../../components/TooltipWrapper'
import templates from '../../utils/questionTemplates'

const QuestionCreationCardHeader = ({
  question,
  mandatory,
  onMandatoryChange,
  onDisplaySurveyNameChange,
  handleRemove,
  handleCopy,
  questionIndex,
  onProductChange,
  onScoreValueChange,
  isCompulsorySurvey,
  onDemographicChange,
  hasDemographic,
  showUserProfileDemographics
}) => {
  const isStrictMode = question.editMode === 'strict'
  const { t } = useTranslation()
  const demographicTemplates = uniq(pluck('questions', templates))
  const { type = '' } = question || {}
  const title = `${questionIndex + 1} - ${
    type
      ? `Type: ${capitalize(startCase(type))}`
      : t('components.questionCreation.newQuestion')
  }`

  const hint = t(`tooltips.questionHeader.${question.type}`, {
    defaultValue: null
  })

  const restrictedTypes = [
    'paypal-email',
    'choose-product',
    'choose-payment',
    'show-product-screen'
  ]
  const showButtons = !(restrictedTypes.indexOf(type) >= 0)

  return (
    <React.Fragment>
      <Container>
        <Title>
          {title}
          {hint && (
            <HelperIcon
              placement='rightTop'
              helperText={hint}
              overlayStyle={{ minWidth: '280px' }}
            />
          )}
        </Title>
        {showButtons && (
          <React.Fragment>
            {question && question.displayOn === 'middle' && (
              <StyledCheckbox
                checked={question && question.showProductImage}
                onChange={e => onProductChange(e.target.checked)}
              >
                {t('components.questionCreation.showProductImage')}
              </StyledCheckbox>
            )}
            {isCompulsorySurvey &&
              question &&
              question.displayOn === 'screening' && (
                <StyledCheckbox
                  checked={question && question.considerValueForScore}
                  onChange={e => onScoreValueChange(e.target.checked)}
                >
                  {t('components.questionCreation.scoreCalculation')}
                </StyledCheckbox>
              )}
            {showUserProfileDemographics && question.displayOn !== 'middle' &&
              demographicTemplates.includes(type) && (
                <StyledCheckbox
                  checked={hasDemographic}
                  onChange={e => onDemographicChange(e.target.checked)}
                >
                  {t('components.questionCreation.hasDemographic')}
                </StyledCheckbox>
              )}
            <StyledCheckbox
              checked={question.displaySurveyName}
              onChange={e => onDisplaySurveyNameChange(e.target.checked)}
            >
              {t('components.questionCreation.displaySurveyName')}
            </StyledCheckbox>
            <StyledCheckbox
              testId='mandatory'
              disabled={NEVER_REQUIRED_QUESTIONS_TYPES.has(question.type)}
              checked={mandatory}
              onChange={e => onMandatoryChange(e.target.checked)}
            >
              {t('components.questionCreation.mandatory')}
            </StyledCheckbox>
            <ActionContainer>
              <TooltipWrapper helperText={t('tooltips.duplicateQuestion')}>
                <WideButton withmargin={`true`} onClick={handleCopy}>
                  <ButtonText>
                    {t('components.questionCreation.duplicateButton')}
                  </ButtonText>
                </WideButton>
              </TooltipWrapper>
              {isStrictMode ? (
                <HelperIcon
                  placement='bottomRight'
                  helperText={t(
                    'components.questionCreation.removeButtonHelper'
                  )}
                />
              ) : (
                <TooltipWrapper helperText={t('tooltips.removeQuestion')}>
                  <WideButton type='red' onClick={handleRemove}>
                    <ButtonText>
                      {t('components.questionCreation.removeButton')}
                    </ButtonText>
                  </WideButton>
                </TooltipWrapper>
              )}
            </ActionContainer>
          </React.Fragment>
        )}
      </Container>
    </React.Fragment>
  )
}

export default memo(QuestionCreationCardHeader)
