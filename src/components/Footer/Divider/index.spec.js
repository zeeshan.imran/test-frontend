import React from 'react'
import { shallow } from 'enzyme'
import Divider from '.';

describe('Divider', () => {
    let testRender;
    
    beforeEach(() => {
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render Divider', async () => {
        testRender = shallow(
            <Divider
            />
        )
        expect(testRender).toMatchSnapshot()
        
    })
});