import React from 'react'
import { Col } from 'antd'
import { SocialMediaContainer, CustomIcon } from './styles'

const SocialMedia = ({ desktop }) => (
  <Col lg={{ span: 4 }}>
    <SocialMediaContainer desktop={desktop}>
      <a
        href='https://www.facebook.com/'
        target='_blank'
        rel='noopener noreferrer'
      >
        <CustomIcon type='facebook' theme='filled' />
      </a>
      <a
        href='https://www.twitter.com/'
        target='_blank'
        rel='noopener noreferrer'
      >
        <CustomIcon type='twitter' />
      </a>
      <a
        href='https://www.instagram.com/'
        target='_blank'
        rel='noopener noreferrer'
      >
        <CustomIcon styled={{ last: true }} type='instagram' />
      </a>
    </SocialMediaContainer>
  </Col>
)

export default SocialMedia
