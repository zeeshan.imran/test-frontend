import React from 'react'
import { shallow } from 'enzyme'
import Footer from '.'

describe('FooterContainer', () => {
  let testRender

  beforeEach(() => {
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render FooterContainer', async () => {
    window.matchMedia.setConfig({
      type: 'screen',
      width: 900,
      height: 800
    })

    testRender = shallow(
        <Footer />
      )
    expect(testRender).toMatchSnapshot()
    
  })
})
