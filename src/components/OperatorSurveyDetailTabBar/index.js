import React from 'react'
import { withRouter } from 'react-router-dom'
import gql from 'graphql-tag'
import { path } from 'ramda'
import { useQuery } from 'react-apollo-hooks'
import { useTranslation } from 'react-i18next'
import TabBar from '../TabBar'
import getQueryParams from '../../utils/getQueryParams'
import { getAuthenticatedOrganization } from '../../utils/userAuthentication'
const isScreenerOnly = path(['surveyCreation', 'basics', 'isScreenerOnly'])
const isCompulsorySurvey = path([
  'surveyCreation',
  'basics',
  'compulsorySurvey'
])

const OperatorSurveyDetailTabBar = ({ history, match, actions, location }) => {
  const {
    params: { step }
  } = match
  const { t } = useTranslation()
  const { data } = useQuery(SURVEY_CREATION)
  const userOrganizationId = getAuthenticatedOrganization()
  const { data: { organization: { name: userOrganizationName } = {} } = {} } = useQuery(
    GET_ORGANIZATION,
    {
      variables: { id: userOrganizationId }
    }
  )
  const screenerOnly = isScreenerOnly(data) || false
  const compulsorySurvey = isCompulsorySurvey(data) || false
  const { REACT_APP_THEME } = process.env
  const isFlavorwikiOperator = userOrganizationName === 'FlavorWiki'

  const tabPayments = () => ({
    title: t('components.operatorSurveyDetailBar.payments'),
    tooltip: t('tooltips.operatorSurveyDetailBar.payments'),
    key: 'financial?section=payments'
  })

  const tabTastingNotes = () => ({
    title: t('components.operatorSurveyDetailBar.tastingNotes'),
    tooltip: t('tooltips.operatorSurveyDetailBar.tastingNotes'),
    key: 'tasting-notes'
  })

  let tabs = [
    {
      title: t('components.operatorSurveyDetailBar.basics'),
      tooltip: t('tooltips.operatorSurveyDetailBar.basics'),
      key: 'basics'
    },
    {
      title: t('components.operatorSurveyDetailBar.settings'),
      tooltip: t('tooltips.operatorSurveyDetailBar.settings'),
      key: 'settings'
    },
    ...(['chrHansen', 'bunge'].includes(REACT_APP_THEME) ? [tabTastingNotes()] : []),
    ...(REACT_APP_THEME === 'default' && isFlavorwikiOperator
      ? [tabPayments()]
      : []),
    {
      title: t('components.operatorSurveyDetailBar.products'),
      tooltip: t('tooltips.operatorSurveyDetailBar.products'),
      key: 'products'
    },
    {
      title: t('components.operatorSurveyDetailBar.screening'),
      tooltip: t('tooltips.operatorSurveyDetailBar.screening'),
      key: 'questions?section=screening'
    },
    {
      title: t('components.operatorSurveyDetailBar.before'),
      tooltip: t('tooltips.operatorSurveyDetailBar.before'),
      key: 'questions?section=begin'
    },
    {
      title: t('components.operatorSurveyDetailBar.tasting'),
      tooltip: t('tooltips.operatorSurveyDetailBar.tasting'),
      key: 'questions?section=middle'
    },
    {
      title:
        REACT_APP_THEME === 'chrHansen'
          ? t('components.operatorSurveyDetailBarChrHansen.after')
          : t('components.operatorSurveyDetailBar.after'),
      tooltip: t('tooltips.operatorSurveyDetailBar.after'),
      key: 'questions?section=end'
    }
  ]

  if (screenerOnly) {
    tabs = [
      {
        title: t('components.operatorSurveyDetailBar.basics'),
        tooltip: t('tooltips.operatorSurveyDetailBar.basics'),
        key: 'basics'
      },
      {
        title: t('components.operatorSurveyDetailBar.settings'),
        tooltip: t('tooltips.operatorSurveyDetailBar.settings'),
        key: 'settings'
      },
      ...(REACT_APP_THEME === 'chrHansen' ? [tabTastingNotes()] : []),
      ...(REACT_APP_THEME !== 'chrHansen' ? [tabPayments()] : []),
      {
        title: t('components.operatorSurveyDetailBar.screening'),
        tooltip: t('tooltips.operatorSurveyDetailBar.screening'),
        key: 'questions?section=screening'
      },
      {
        title: t('components.operatorSurveyDetailBar.linkedSurveys'),
        tooltip: t('tooltips.operatorSurveyDetailBar.linkedSurveys'),
        key: 'linked-surveys'
      }
    ]
  }

  if (compulsorySurvey) {
    tabs = [
      {
        title: t('components.operatorSurveyDetailBar.basics'),
        tooltip: t('tooltips.operatorSurveyDetailBar.basics'),
        key: 'basics'
      },
      {
        title: t('components.operatorSurveyDetailBar.settings'),
        tooltip: t('tooltips.operatorSurveyDetailBar.settings'),
        key: 'settings'
      },
      {
        title: t('components.operatorSurveyDetailBar.screening'),
        tooltip: t('tooltips.operatorSurveyDetailBar.screening'),
        key: 'questions?section=screening'
      }
    ]
  }

  const { search } = location

  const activeTabUrl = [step, search].join('')
  const activeTabSliceIndex =
    activeTabUrl.indexOf('&&') !== -1
      ? activeTabUrl.indexOf('&&')
      : activeTabUrl.indexOf('?') !== -1
      ? activeTabUrl.indexOf('?')
      : activeTabUrl.length
  const activeTab = activeTabUrl.slice(0, activeTabSliceIndex)

  const { folderId } = getQueryParams(location)
  const folderQuerry =
    folderId && folderId !== 'null' && folderId !== 'undefined'
      ? `folderId=${folderId}`
      : ``

  return (
    <TabBar
      tabs={tabs}
      activeTab={activeTab}
      onTabSelection={tab => {
        const preSign = tab.includes('?') ? '&&' : '?'
        history.push(`${tab}${preSign}${folderQuerry}`)
      }}
      action={actions}
    />
  )
}

export default withRouter(OperatorSurveyDetailTabBar)

export const GET_ORGANIZATION = gql`
  query organization($id: ID!) {
    organization(id: $id) {
      id
      name
    }
  }
`

export const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      basics {
        isScreenerOnly
        authorizationType
        compulsorySurvey
      }
    }
  }
`
