import React from 'react'
import { mount } from 'enzyme'
import MyProfileDropdown from '.'
import { useSlaask } from '../../contexts/SlaaskContext'
import { Dropdown } from 'antd'
import { logout, getAuthenticatedUser } from '../../utils/userAuthentication'
import { Redirect } from 'react-router-dom'
import usePurgeCache from '../../hooks/usePurgeCache'

jest.mock('../../utils/userAuthentication', () => ({
  logout: jest.fn(),
  getAuthenticatedUser: jest.fn()
}))
jest.mock('../../hooks/usePurgeCache')
jest.mock('../../contexts/SlaaskContext')
jest.mock('react-router-dom')
jest.mock('../../utils/userAuthentication')

describe('MyProfileDropdown', () => {
  let testRender
  let picture
  let userId
  let displayName
  let reloadSlaask
  let purgeCache
  let hasOperatorAccount
  let MyMock

  beforeEach(() => {
    picture = ''
    userId = 1
    displayName = 'flavor-wiki'

    let storage = {}
    window.localStorage = {
      getItem: jest.fn(key => storage[key] || null),
      setItem: jest.fn((...args) => {
        if (args.length < 2) throw Error('2 argument required')
        const [key, value] = args
        storage[key] = value
      }),
      removeItem: jest.fn((...args) => {
        if (args.length < 1) throw Error('1 argument required')
        const [key] = args
        delete storage[key]
      }),
      clear: jest.fn(() => {
        storage = {}
      })
    }
    purgeCache = jest.fn()
    usePurgeCache.mockImplementation(() => purgeCache)
    reloadSlaask = jest.fn()
    useSlaask.mockImplementation(() => ({ reloadSlaask: reloadSlaask }))
    Redirect.mockReset()
    logout.mockImplementation(() => {})
    hasOperatorAccount = {}
    MyMock = jest.fn()
    MyMock.mockImplementation(function () {
      this.name = 'test'
      this.foo = jest.fn(() => {
        return 'bar'
      })
    })
  })

  afterEach(() => {
    testRender.unmount()
  })
  test('should render MyProfileDropdown', async () => {
    getAuthenticatedUser.mockImplementation(() => ({ isSuperAdmin: true }))

    testRender = mount(
      <MyProfileDropdown
        picture={picture}
        userId={userId}
        displayName={displayName}
      />
    )
    expect(testRender.find(MyProfileDropdown)).toHaveLength(1)

    testRender.find(Dropdown).simulate('click')
    testRender.find('[data-testid="logout-profile"]').simulate('click')

    expect(logout).toHaveBeenCalled()
    expect(reloadSlaask).toHaveBeenCalled()
  })

  // Commenting this because for now the view-profile option is hidden

  // test('should render MyProfileDropdown onClick View profile', async () => {
  //   let newMock = new MyMock()
  //   newMock.foo()

  //   getAuthenticatedUser.mockImplementation(() => ({ isSuperAdmin: true }))

  //   testRender = mount(
  //     <MyProfileDropdown
  //       picture={picture}
  //       userId={userId}
  //       displayName={displayName}
  //     />
  //   )
  //   testRender.find(Dropdown).simulate('click')
  //   testRender.find('[data-testid="view-profile"]').simulate('click') 
  //   expect(MyMock.mock.instances[0].foo).toHaveBeenCalled()
  // })

  test('should render MyProfileDropdown onClick Switch to Operator Account', async () => {
    let newMock = new MyMock()
    newMock.foo()

    getAuthenticatedUser.mockImplementation(() => ({ isSuperAdmin: true }))

    testRender = mount(
      <MyProfileDropdown
        picture={picture}
        userId={userId}
        displayName={displayName}
        hasOperatorAccount={hasOperatorAccount}
      />
    )
    testRender.find(Dropdown).simulate('click')
    testRender.find('[data-testid="switch-account"]').simulate('click')
    expect(MyMock.mock.instances[0].foo).toHaveBeenCalled()
  })
})
