/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from 'react'
import { Dropdown, Icon, Menu } from 'antd'
import { UserInfo, Picture, Container, HelpLink } from './styles'
import DefaultProfilePicture from '../../assets/svg/User.svg'
import { logout } from '../../utils/userAuthentication'
import { useSlaask } from '../../contexts/SlaaskContext'
import history from '../../history'
import WelcomeTasterModal from '../WelcomeTasterModal'

// const isProduct = window.location.host === 'app.flavorwiki.com'
// const openProfile = () => history.push('/my-profile')

const MyProfileDropdown = ({
  picture,
  userId,
  displayName,
  hasOperatorAccount,
  showhelp=false
}) => {
  const { reloadSlaask } = useSlaask()
  const [visible, setVisible] = useState(showhelp)

  return (
    <React.Fragment>
      <Container>
        {!hasOperatorAccount && (
          <HelpLink onClick={() => setVisible(true)}>Help</HelpLink>
        )}
        <Picture src={picture || DefaultProfilePicture} />
        <Dropdown
          placement='bottomCenter'
          overlay={
            <Menu>
              {/* TODO: disable STAGFW-342 */}

              {hasOperatorAccount && (
                <Menu.Item key='2'>
                  <a
                    data-testid='switch-account'
                    onClick={() => history.push('/operator')}
                  >
                    Switch to Operator Account
                  </a>
                </Menu.Item>
              )}
              <Menu.Item key='1'>
                <a
                  data-testid='logout-profile'
                  onClick={() => {
                    logout()
                    reloadSlaask()
                  }}
                >
                  Sign Out
                </a>
              </Menu.Item>
            </Menu>
          }
          trigger={['click']}
        >
          <UserInfo>
            {displayName}
            <Icon type='down' />
          </UserInfo>
        </Dropdown>
      </Container>
      {visible && (
        <WelcomeTasterModal visible={visible} setVisible={setVisible} showhelp={showhelp} />
      )}
    </React.Fragment>
  )
}

export default MyProfileDropdown
