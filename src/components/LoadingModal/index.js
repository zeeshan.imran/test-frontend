import React from 'react'
import { Modal, Icon } from 'antd'
import { Container, Text } from './styles'
import colors from '../../utils/Colors'

const LoadingModal = ({ visible, text }) => (
  <Modal visible={visible} centered closable={false} footer={null}>
    <Container>
      <Icon
        style={{ fontSize: '40px' }}
        type='setting'
        theme='twoTone'
        twoToneColor={colors.MODAL_ICON_COLOR}
        spin
      />
      <Text>{text}</Text>
    </Container>
  </Modal>
)

export default LoadingModal
