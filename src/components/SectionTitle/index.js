import React from 'react'
import { Desktop } from '../Responsive'
import { Container, TitleAligner, Title, Subtitle, Prompt } from './styles'

const SectionTitle = ({ title, subtitle, prompt }) => (
  <Desktop>
    {desktop => (
      <Container desktop={desktop}>
        <TitleAligner>
          <Title desktop={desktop}>{title}</Title>
          <Subtitle>{subtitle}</Subtitle>
        </TitleAligner>
        {prompt ? <Prompt>{prompt}</Prompt> : null}
      </Container>
    )}
  </Desktop>
)

export default SectionTitle
