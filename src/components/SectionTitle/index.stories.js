import React from 'react'
import { storiesOf } from '@storybook/react'
import SectionTitle from './'

storiesOf('Section Title', module)
  .add('Just title', () => <SectionTitle title='This is a title' />)
  .add('Title and subtitle', () => (
    <SectionTitle title='This is a title' subtitle='(with a subtitle)' />
  ))
  .add('Title and prompt', () => (
    <SectionTitle title='This is a title' prompt='Select something' />
  ))
  .add('Title, subtitle, and prompt', () => (
    <SectionTitle
      title='This is a title'
      subtitle='(with a subtitle)'
      prompt='Select something'
    />
  ))
