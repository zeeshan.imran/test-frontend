import React from 'react'
import { shallow } from 'enzyme'
import SectionTitle from '.'

describe('SectionTitle', () => {
  let testRender
  let title
  let subtitle
  let prompt

  beforeEach(() => {
    title = 'Title'
    subtitle = 'Sub title'
    prompt = false
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SectionTitle', async () => {
    testRender = shallow(
      <SectionTitle title={title} subtitle={subtitle} prompt={prompt} />
    )
    expect(testRender).toMatchSnapshot()
  })
})
