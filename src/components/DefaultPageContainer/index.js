import React from 'react'
import { Desktop } from '../Responsive'
import { Container } from './styles'

const DefaultPageContainer = ({ children }) => (
  <Desktop>
    {desktop => <Container desktop={desktop}>{children}</Container>}
  </Desktop>
)

export default DefaultPageContainer
