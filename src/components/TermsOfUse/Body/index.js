import React from 'react'
import {
  Container,
  Title,
  Subtitle,
  Footer,
  MainText,
  Bold,
  Link,
  Paragraph,
  List,
  Item,
  Underline as U
} from './styles.js'

const Body = () => {
  return (
    <Container>
      <MainText>
        <Title>FLAVORWIKI WEBSITE TERMS OF USE</Title>
        <br />
        <Bold>Welcome</Bold>
        <br />
        <Paragraph>
          We thank You for visiting our Internet website{' '}
          <Link
            rel='noopener'
            href='https://www.flavorwiki.com'
            target='_blank'
          >
            www.flavorwiki.com
          </Link>{' '}
          (the <Bold>"Website"</Bold>) powered by Smart Sensory Analytics LLC, a
          limited liability company incorporated under the laws of Delaware with
          its registered office at 140 Jonathans Drive West End, NC 27376,
          United States (hereinafter referred to as <Bold>"FlavorWiki"</Bold>,{' '}
          <Bold>"We"</Bold>, <Bold>"Us"</Bold>, <Bold>"Our"</Bold>).
        </Paragraph>
        <Paragraph>
          We make information, products, and services available on the Website,
          subject to the following terms and conditions for accessing, visiting
          and/or otherwise using any and all web pages and related content and
          materials (the <Bold>"Terms"</Bold>).
        </Paragraph>
        <Paragraph>
          <Bold>
            PLEASE READ THE TERMS CAREFULLY BEFORE USING THE WEBSITE. BY
            ACCESSING, VISITING AND/OR USING THE WEBSITE, YOU ARE AGREEING TO BE
            BOUND BY THE TERMS. IF YOU DO NOT AGREE TO THE TERMS; PLEASE DO NOT
            ACCESS AND/OR USE THE WEBSITE.
          </Bold>
        </Paragraph>
        <Paragraph>
          <Bold>"You"</Bold> or <Bold>"Your"</Bold> means You personally (i.e.
          the individual who reads and agrees to be bound by the Terms) and, if
          You access the Website on behalf of a corporation or other legal
          entity, collectively, You and such corporation or other legal entity
          on whose behalf You access the Website.
        </Paragraph>
        <Paragraph>
          Your use of the Website and Your access to the services provided by Us
          may be subject to additional terms and conditions provided by
          FlavorWiki and available on the Website. Such additional terms and
          conditions include, but are not limited to, license agreements,
          service-specific notices or statements and FlavorWiki's{' '}
          <Link rel='noopener' href='./privacy-policy' target='_blank'>
            privacy and personal data policy
          </Link>
          . Those additional terms are hereby incorporated by reference into the
          Terms. If there is any conflict between those additional terms and the
          Terms, the additional terms shall prevail with respect to the subject
          matter of such additional terms.
        </Paragraph>
        <Paragraph>
          FlavorWiki reserves the right to modify or revise the Terms at any
          time and without notice. Your continued use of the Website after such
          changes will indicate Your acceptance of such changes. Please remember
          to regularly consult this page in order to take note of any changes or
          amendments that may have been made to the Terms. The version published
          on the Website at the relevant time applies between You and
          FlavorWiki.
        </Paragraph>
        <Paragraph>
          FlavorWiki reserves the right to interrupt at all times the supply of
          the Website or to modify it, to any extent, without notice and without
          stating reasons. FlavorWiki further reserves the right to seek all
          remedies available by law and in equity, as the case may be, for any
          violation of the Terms. Any rights not expressly granted herein are
          reserved by FlavorWiki.
        </Paragraph>
        <Subtitle>Terms Applicable to Corporations</Subtitle>
        <Paragraph>
          The following terms and conditions apply specifically to any
          corporation or other legal entity that is subject to the Terms.
        </Paragraph>
        <Paragraph>
          If You are using this Website on behalf of a corporation or other
          legal entity, You represent that You are authorized to accept the
          Terms on behalf of such corporation or other legal entity.
        </Paragraph>
        <Paragraph>
          In addition, You agree to require each of Your employees to be bound
          by the terms and conditions of the Terms and You agree to remain
          responsible and liable for all acts and omissions of Your employees in
          connection with the Website, including any breaches of the Terms. All
          references to Your access and/or use of the Website herein include
          access and/or use of the Website by Your employees. You agree that
          each of Your employees is responsible for maintaining the
          confidentiality of any password that such employee may use to access
          the Website, and You agree not to let any employee transfer a password
          or user name, or lend or otherwise transfer use of or access to the
          Website, to other employees or any third party. If an employee leaves
          Your employ, or if You wish to disable an employee’s access to the
          Website, You are responsible for any such changes. You are fully
          responsible for all interaction with the Website that occurs in
          connection with passwords or user names associated with Your employees
          (including any former employees).
        </Paragraph>
        <Subtitle>Access to Website</Subtitle>
        <Paragraph>
          When accessing, visiting and/or otherwise using the Website, You must
          comply with all applicable laws, including federal, state, and local
          laws as well as the laws of Your jurisdiction. You agree not to use or
          exploit the content of the Website in any manner inconsistent with the
          Terms and, in particular, to always respect the rights granted or the
          restrictions set forth herein.
        </Paragraph>
        <Paragraph>
          Notwithstanding the above, and pursuant to the Terms, FlavorWiki
          grants You a personal, non-exclusive, non-transferable and revocable
          right to access the Website, to use its different functionalities,
          display its content and benefit from its services, under the following
          conditions and rules of conduct:
        </Paragraph>
        <List type='a'>
          <Item>
            (a) You may neither, under any circumstance, alter, disassemble or
            modify any part of the Website, nor attempt to, or encourage or
            assist any other person to, circumvent or modify any component of
            the Website;
          </Item>
          <Item>
            (b) You may not use or attempt to use any engine, software tool,
            agent, or other device or mechanism (including without limitation
            browsers, spiders, robots, offline readers, avatars, worms, or
            intelligent agents) or any other automated system which may have the
            purpose or effect to detrimentally affect the Website, to navigate
            or search the Website other than through generally available third
            party browsers;
          </Item>
          <Item>
            (c) You may not (i) violate copyright, trademark, or other
            intellectual property laws contained on the Website, (ii) attempt to
            collect, store or publish personally identifiable information
            without the owner’s knowledge and consent in any circumstance, and
            (iii) disable, disrupt, circumvent, interfere with, or otherwise
            violate the security of the Website;
          </Item>
          <Item>
            (d) Your use of the Website requires one or more compatible devices,
            Internet access and certain software and may require obtaining
            updates or upgrades from time to time. As the use of the Website
            involves hardware, software, and Internet access, Your ability to
            use the Website may be affected by the performance of such
            materials. You agree that such system requirements, as modified from
            time to time, are under Your own and sole responsibility;
          </Item>
          <Item>
            (e) You undertake to post and upload on the Website only valid and
            compliant contents with respect to applicable laws of the country
            You are a resident of;
          </Item>
          <Item>
            (f) You agree to use the Website at Your sole risk. In particular,
            You agree that FlavorWiki shall assume no liability for content that
            may be found to be offensive, indecent, or objectionable, either by
            Yourself or by other users. In case You find something on the
            Website to be offensive, indecent or objectionable, please contact
            FlavorWiki and report the problem;
          </Item>
          <Item>
            (g) You acknowledge that any use of any third-party websites – even
            if referred to by hyperlinks on the Website – shall be subject to
            their own terms of use and privacy policy, if any.
          </Item>
        </List>

        <Subtitle>Privacy and personal data policy</Subtitle>
        <Paragraph>
          In principle, You are free to visit parts of the Website without being
          obliged to provide any of Your personal data.
        </Paragraph>
        <Paragraph>
          However, when You visit certain areas of the Website and, in
          particular, when accessing and using Our services, You may submit
          content to Us that includes Your personal data. We know that by
          sharing with Us Your personal data, You are trusting Us to treat it
          appropriately.
        </Paragraph>
        <Paragraph>
          FlavorWiki's{' '}
          <Link rel='noopener' href='./privacy-policy' target='_blank'>
            privacy and personal data policy
          </Link>{' '}
          (the <Bold>"Policy"</Bold>) details how We collect and process Your
          personal data. You acknowledge that You have read and understood the
          Policy before providing Us with any of Your personal data.
        </Paragraph>
        <Paragraph>
          You in turn agree that FlavorWiki may use and share Your personal data
          in accordance with the Policy and the applicable data protection laws.
        </Paragraph>
        <Paragraph>
          In addition, You represent and warrant that any information that You
          provide in connection with Your use of the Website is and shall remain
          true, accurate and complete, and that You will maintain and update
          such information regularly. You agree that if any information that You
          provide is or become false, inaccurate, obsolete or incomplete,
          FlavorWiki may terminate Your use of the Website and Your access to
          the services.
        </Paragraph>
        <Subtitle>Account Registration</Subtitle>
        <Paragraph>
          In order to access certain areas of the Website, You may be required
          to have a valid account.
        </Paragraph>
        <Paragraph>
          To create an account, You must provide us with an electronic mail
          address, as well as other information (the <Bold>"Access Data"</Bold>
          ). You are responsible for maintaining the confidentiality of Your
          Access Data and for all activities that occur under Your account.
        </Paragraph>
        <Paragraph>
          You agree to immediately notify us of any unauthorized use of Your
          account or any other breach of security. You further commit to exit
          from Your account at the end of each session.
        </Paragraph>
        <Paragraph>
          You represent and warrant that Your Access Data are true, accurate,
          current and complete. We shall assume no liability in relation with
          Your Access Data. If You provide any information that is untrue,
          inaccurate, not current or incomplete, or we have reasonable grounds
          to suspect that it might be the case, we reserve the right to suspend
          or terminate Your account and refuse any and all current or future use
          of our Services or any portion thereof.
        </Paragraph>
        <Paragraph>
          By registering an account, You represent and warrant that You are at
          least 18 years of age. You are solely responsible for ensuring that
          the use of our Services is in compliance with all laws, rules and
          regulations applicable to You, it being specified that any use of our
          Services that conflicts with any applicable law, rule or regulation is
          strictly prohibited.
        </Paragraph>
        <Paragraph>
          We may, in our sole discretion, refuse to offer our Services to any
          specific person or entity and change our eligibility criteria at any
          time.
        </Paragraph>
        <Subtitle>Intellectual property rights</Subtitle>
        <Paragraph>
          The Website includes content owned, operated, licensed and/or
          controlled by FlavorWiki. The information and materials made available
          through the Website are and shall remain the property of FlavorWiki,
          and are protected by copyright, trademark, patent, and/or other
          proprietary rights of the like. FlavorWiki grants You a limited,
          revocable, non-sublicensable right to view the content of the Website
          solely for Your internal use of the Website.
        </Paragraph>
        <Paragraph>
          You may not use, download, upload, copy, print, display, perform,
          reproduce, publish, license, post, transmit, rent, lease, modify,
          loan, sell, distribute or create derivative works based (whether in
          whole or in part) on the Website or any information from the Website,
          in whole or in part, without the express prior written authorization
          of FlavorWiki. Elements of the Website are protected by copyright,
          trademark, unfair competition and/or other applicable laws and may not
          be copied or imitated in whole or in part. No logo, graphic, sound, or
          image from the Website may be copied or retransmitted unless expressly
          permitted in writing by FlavorWiki. Nothing contained on the Website
          should be construed as granting, by implication, estoppel or
          otherwise, any license or right to use any of FlavorWiki’s or its
          affiliates’ or suppliers’ trade names, trademarks or service marks
          without FlavorWiki’s express prior written consent.
        </Paragraph>
        <Subtitle>User content</Subtitle>
        <Paragraph>
          The Website might allow You to upload or otherwise provide Us text,
          files, images, photos, video, sounds, software, works of authorship,
          or other materials (collectively, the <Bold>"User Content"</Bold>) and
          to store, publish or share such content with Us.
        </Paragraph>
        <Paragraph>
          You retain ownership of Your User Content. FlavorWiki does not claim
          any ownership rights in that regard. However, by submitting Your User
          Content to Us through our Website, You grant Us a worldwide,
          royalty-free, sublicensable, perpetual, irrevocable license to use,
          modify, publicly perform, publicly display, reproduce, create
          derivative works, distribute, transfer and otherwise exploit such User
          Content, but only for providing You with access to the Website and the
          services as well as for fulfilling Our contractual obligations towards
          Our clients.
        </Paragraph>
        <Paragraph>
          In relation to Your User Content, You guarantee that (i) You have all
          the rights and authorization to grant the licenses to FlavorWiki as
          mentioned above and that (ii) the content You submit doesn’t violate
          any norm, contractual obligation or third party right, especially
          concerning intellectual property rights and copyrights.
        </Paragraph>
        <Paragraph>
          FlavorWiki will store and process Your User Content in a manner
          consistent with industry security standards. In this regard, We have
          implemented appropriate technical, organizational and administrative
          systems, policies and procedures designed to help ensure the security
          and integrity of Your User Content and to mitigate the risk of
          unauthorized access to – or use of – Your User Content.
        </Paragraph>
        <Subtitle>Surveys</Subtitle>
        <Paragraph>
          Through the Website, We provide access to surveys and questionnaires
          (the <Bold>"Surveys"</Bold>).
        </Paragraph>
        <Paragraph>
          Participation in the Survey may be remunerated according to the
          modalities indicated in each specific Survey terms and conditions
          which are accessible on the Survey page (the{' '}
          <Bold>"Survey Notice"</Bold>). Such remuneration is intended as a
          favor only. Neither Us nor our clients give any guarantee whatsoever
          in relation with the remuneration.
        </Paragraph>
        <Paragraph>
          The Surveys are only open to individuals who are (i) over the age of
          majority according to the legislation of their place of residence,
          (ii) free of food allergies unless specifically indicated and (iii)
          additional eligibility criteria as expressly indicated in the Survey
          Notice, which may be specific to each survey.
        </Paragraph>
        <Paragraph>
          To participate in the Survey, please declare Your intention by
          clicking on the corresponding link or button on the Website. In doing
          so, You guarantee that You will at all times comply with the Terms and
          the Survey Notice.
        </Paragraph>
        <Paragraph>
          You commit to accurately follow the instructions provided in the
          Survey Notice and to provide truthful and honest answers based on Your
          personal experience.
        </Paragraph>
        <Paragraph>
          <Bold>
            THE REMUNERATION (IF ANY) IS GRANTED ONLY FOR SURVEYS WHICH HAVE
            BEEN COMPLETED HONESTLY AND IN FULL. WHERE A SURVEY IS NOT COMPLETED
            (FOR WHATEVER REASON) OR IF THE PARTICIPANT FAILS TO ANSWER THE
            SURVEY TRUTHFULLY, OR ANSWERS ILLOGICALLY, OR WITHOUT FOLLOWING THE
            INSTRUCTIONS PROVIDED IN THE SURVEY NOTICE, THEN NO REMUNERATION
            WILL BE GRANTED.
          </Bold>
        </Paragraph>
        <Paragraph>
          <Bold>
            WE RESERVE THE RIGHT TO EXCLUDE PARTICIPANTS FROM THE SURVEYS, AT
            OUR ENTIRE DISCRETION, WITHOUT NOTICE AND WITHOUT HAVING TO STATE
            THE REASONS, PARTICULARLY AND WITHOUT LIMITATION IN THE EVENT OF
            FRAUDULENT OR UNFAIR CONDUCT AIMED AT MANIPULATING THE SURVEY.
          </Bold>
        </Paragraph>
        <Paragraph>
          <Bold>
            MORE GENERALLY, ANY DECISIONS TAKEN BY US IN THE CONTEXT OF THE
            SURVEYS ARE DEFINITIVE AND BINDING UPON PARTICIPANTS. THEY CANNOT BE
            THE SUBJECT OF ANY DISCUSSION OR APPEAL. NO CORRESPONDENCE SHALL BE
            EXCHANGED WITH REGARD TO SUCH DECISIONS. WE DO NOT NEED TO JUSTIFY
            OUR DECISIONS.
          </Bold>
        </Paragraph>
        <Subtitle>Links to other websites</Subtitle>
        <Paragraph>
          The Website might contain links which would take You outside of
          FlavorWiki's networks and systems, especially towards third-party
          websites. FlavorWiki accepts no responsibility concerning the content,
          the accuracy or the functioning of these third-party websites.
          FlavorWiki cannot be held liable for the content of the websites
          towards which a link is proposed. The inclusion of a link to other
          websites does not imply FlavorWiki's approval. We recommend You to
          carefully review the privacy and data protection policies as well as
          the general terms and conditions of each website You visit and of each
          online service You use.
        </Paragraph>
        <Subtitle>Emails security</Subtitle>
        <Paragraph>
          When You communicate with FlavorWiki by email, You should take into
          account that the protection of emails and messages transmitted through
          the Internet is not guaranteed. Consequently, while sending by emails
          important or confidential messages, not encoded, You accept the risks
          linked to this uncertainty and the possible lack of confidentiality
          over the Internet.
        </Paragraph>
        <Subtitle>No guarantees</Subtitle>
        <Paragraph>
          <U> In general</U>
        </Paragraph>
        <Paragraph>The use of the Website is at Your own risk.</Paragraph>
        <Paragraph>
          The Website is offered « as it is » and « as it is available ».
          Consequently, FlavorWiki does not offer any guarantee.
        </Paragraph>
        <Paragraph>
          We especially do not guarantee that (i) the Website and its related
          services will meet Your requirements; (ii) its content is exhaustive,
          accurate, precise, reliable, updated and does not transgress the right
          of third parties; (iii) Your access to the Website will be
          uninterrupted or error-free or virus-free, or that the Website will be
          available at all times; (iv) defects will be corrected; (v) the
          Website is secured; or that (vi) any advice or opinion received from
          us or through the Website is exact and reliable.
        </Paragraph>
        <Paragraph>
          Any information accessible via the Website is subject to modification
          or deletion without notice.
        </Paragraph>
        <Paragraph>
          The complete enjoyment of the services and/or features provided
          through the Website and FlavorWiki may need to use third-parties
          services (Google Maps™, YouTube™, Facebook™ or Twitter™ buttons,
          etc.). FlavorWiki does not guarantee that the access to these services
          is available in the country where You are located according to local
          applicable law and regulations. To that extent, FlavorWiki shall not
          bear any liability for damages due to the impossibility to connect to
          such services.
        </Paragraph>
        <Paragraph>
          <U>Accuracy, exhaustiveness and timeliness of information</U>
        </Paragraph>
        <Paragraph>
          We use reasonable efforts to ensure that information accessible via
          the Website is accurate and complete. Should this not be the case,
          FlavorWiki shall nevertheless accept no liability. You acknowledge
          that any reliance upon such information shall be at Your sole risk.
          You acknowledge that it is Your obligation and responsibility to keep
          Yourself informed of any change or amendment that may have been made
          to the content of the Website and its related services. Any liability
          on the part of FlavorWiki in this respect is excluded.
        </Paragraph>
        <Subtitle>Limitation of liability</Subtitle>
        <Paragraph>
          TO THE EXTENT PERMITTED BY LAW, ANY LIABILITY ON THE PART OF
          FLAVORWIKI, ITS REPRESENTATIVES, ITS PARTNERS OR ANY OF ITS
          AUXILIARIES THAT MIGHT RESULT FROM YOUR ACCESS TO – OR YOUR USE OF –
          THE WEBSITE, IS EXCLUDED.
        </Paragraph>
        <Paragraph>
          IN NO EVENT SHALL FLAVORWIKI AND/OR ITS RESPECTIVE SUBSIDIARIES,
          AFFILIATES, SUPPLIERS AND LICENSORS AND ITS AND THEIR DIRECTORS,
          OFFICERS, EMPLOYEES, AGENTS AND REPRESENTATIVES BE LIABLE FOR ANY
          SPECIAL, INDIRECT, EXEMPLARY, INCIDENTAL, CONSEQUENTIAL OR PUNITIVE
          DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA, OR
          PROFITS, LOSS OF OTHER INTANGIBLES, LOSS OF SECURITY OF INFORMATION
          YOU HAVE PROVIDED IN CONNECTION WITH YOUR USE OF THE WEBSITE, OR
          UNAUTHORIZED INTERCEPTION OF ANY SUCH INFORMATION BY THIRD PARTIES,
          WHETHER IN AN ACTION OF EQUITY, CONTRACT, NEGLIGENCE, OR OTHER
          TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
          PERFORMANCE OF THE WEBSITE, ANY PRODUCTS AND SERVICES AVAILABLE
          THROUGH THE WEBSITE, ANY SOFTWARE, INFORMATION, CONTENT, DOCUMENTS,
          RELATED GRAPHICS, PROVISION OF OR FAILURE TO PROVIDE SERVICES
          AVAILABLE FROM OR THROUGH THE WEBSITE, EVEN IF ADVISED IN ADVANCE OF
          SUCH DAMAGES OR LOSSES. IN PARTICULAR, AND WITHOUT LIMITATION,
          FLAVORWIKI AND/OR ITS RESPECTIVE SUBSIDIARIES, AFFILIATES, SUPPLIERS
          AND LICENSORS AND ITS AND THEIR DIRECTORS, OFFICERS, EMPLOYEES, AGENTS
          AND REPRESENTATIVES WILL NOT BE LIABLE FOR DAMAGES OF ANY KIND
          RESULTING FROM YOUR USE OF OR INABILITY TO USE THE WEBSITE OR FROM ANY
          SOFTWARE AND/OR OTHER CONTENT POSTED ON THE WEBSITE BY FLAVORWIKI OR
          ANY THIRD PARTY.
        </Paragraph>
        <Paragraph>
          YOUR SOLE AND EXCLUSIVE REMEDY FOR DISSATISFACTION WITH THE WEBSITE IS
          TO STOP USING THE WEBSITE. THE MAXIMUM LIABILITY OF FLAVORWIKI FOR ALL
          DAMAGES, LOSSES AND CAUSES OF ACTION, WHETHER IN CONTRACT, TORT
          (INCLUDING WITHOUT LIMITATION NEGLIGENCE) OR OTHERWISE, SHALL BE THE
          TOTAL AMOUNT, IF ANY, PAID BY YOU TO FLAVORWIKI TO ACCESS AND USE THE
          WEBSITE.
        </Paragraph>
        <Paragraph>
          THE WEBSITE, THE PRODUCTS AND SERVICES AVAILABLE THROUGH THE WEBSITE
          AND THE INFORMATION, CONTENT, SOFTWARE, DOCUMENTS, AND RELATED
          GRAPHICS PUBLISHED ON THE WEBSITE COULD INCLUDE TECHNICAL
          INACCURACIES, ERRORS, OR OMISSIONS. CHANGES MAY BE PERIODICALLY ADDED
          TO THE INFORMATION HEREIN. FlavorWiki AND/OR ITS RESPECTIVE
          SUBSIDIARIES, AFFILIATES, SUPPLIERS AND LICENSORS MAY, BUT ARE NOT
          OBLIGATED TO, MAKE IMPROVEMENTS AND/OR CHANGES IN THE WEBSITE, THE
          PRODUCTS AND SERVICES AVAILABLE THROUGH THE WEBSITE AND THE
          INFORMATION, SERVICE(S), PRODUCT(S), AND/OR THE PROGRAM(S) DESCRIBED
          HEREIN AT ANY TIME.
        </Paragraph>
        <Paragraph>
          AS A PREREQUISITE TO YOUR USE OF THE WEBSITE, YOU COMMIT YOURSELF TO
          INDEMNIFY FLAVORWIKI, ITS REPRESENTATIVES, ITS PARTNERS AND ITS
          AUXILIARIES, FROM ANY DAMAGE OR EXPENSE (INCLUDING LAWYERS’ FEES AND
          COURT COSTS) THAT MIGHT RESULT FROM YOUR ACCESS TO – OR YOUR USE OF –
          THE WEBSITE.
        </Paragraph>
        <Paragraph>
          NOTHING IN THIS SECTION SHALL LIMIT FLAVORWIKI's LIABILITY TO YOU TO
          THE EXTENT SUCH LIABILITY CANNOT BE EXCLUDED UNDER APPLICABLE LAWS,
          INCLUDING (ONLY TO THE EXTENT APPLICABLE AND WITHOUT LIMITATION)
          LIABILITY FOR DEATH OR PERSONAL INJURY RESULTING FROM FlavorWiki's
          PROVEN NEGLIGENCE, OR FRAUDULENT MISREPRESENTATION, OR CONCEALMENT.
        </Paragraph>
        <Subtitle>Miscellaneous</Subtitle>
        <Paragraph>
          The Terms constitute the entire agreement between You and us
          concerning the access to – and the use of – the Website.
        </Paragraph>
        <Paragraph>
          Unlike FlavorWiki, You are not allowed to transfer to third parties
          the rights and obligations which are Yours according to the Terms.
        </Paragraph>
        <Paragraph>
          If any provision of the Terms is held by a court or other tribunal of
          competent jurisdiction to be invalid or unenforceable for any reason,
          it shall be replaced with a provision having legal and economic
          effects as similar as possible to the invalid provision. In any event,
          all other provisions of the Terms shall remain valid and enforceable
          to the fullest extent possible.
        </Paragraph>
        <Paragraph>
          You may not use or export or re-export any content of the Website or
          any copy or adaptation of such content, or any product or service
          offered on the Website, in violation of any applicable export laws or
          regulations.
        </Paragraph>
        <Paragraph>
          If You or others violate the Terms and we take no immediate action,
          this in no way limits or waives our rights, such as our right to take
          action in the future or in similar situations.
        </Paragraph>
        <Paragraph>The Terms are available in English language only.</Paragraph>
        <Subtitle>Applicable law and jurisdiction</Subtitle>
        <Paragraph>
          You agree that all matters relating to Your access to or use of the
          Website, including all disputes based on the Terms, will be governed
          by the substantive laws of Delaware USA, without regard to its
          conflicts of laws provisions. The United Nations Convention on the
          International Sale of Goods is expressly excluded.
        </Paragraph>
        <Paragraph>
          You agree to the personal jurisdiction by and venue in the ordinary
          courts of venue, United States and waive any objection to such
          jurisdiction or venue.
        </Paragraph>
        <Paragraph>
          Any claim under the Terms must be brought within one (1) year after
          the cause of action arises, or such claim or cause of action is
          barred. No recovery may be sought or received for damages other than
          out-of-pocket expenses, except that the prevailing party will be
          entitled to costs and attorneys’ fees. In the event of any controversy
          or dispute between FlavorWiki and You arising out of or in connection
          with Your use of the Website and/or based on the Terms, the parties
          shall attempt, promptly and in good faith, to resolve any such
          dispute. If FlavorWiki and You are unable to resolve any such dispute
          within a reasonable time (not to exceed thirty (30) days), then either
          party may submit such controversy or dispute to mediation. If the
          dispute cannot be resolved through mediation, then the parties shall
          be free to pursue any right or remedy available to them under
          applicable law, as defined above.
        </Paragraph>
        <Paragraph>
          Notwithstanding the foregoing, if the mandatory laws or public policy
          of any country or territory in which the Terms are enforced or
          construed prohibit the application of the laws specified herein, then
          the laws of such country or territory shall instead apply to the
          extent required by such mandatory laws or public policy. Similarly, if
          You are an individual consumer, the preceding choice-of-law and
          choice-of-jurisdiction provisions shall not affect any mandatory right
          You may have to take action in Your country of residence under the
          laws of that country.
        </Paragraph>
      </MainText>

      <Footer>
        <Paragraph>
          The Terms were last updated on July 7, 2019 and are effective
          immediately.
        </Paragraph>
        <Paragraph>
          Should You have questions or comments regarding the Terms or
          suggestions regarding their improvement, please contact our support
          team at{' '}
          <Link
            rel='noopener'
            href='mailto:support@flavorwiki.com'
            target='_blank'
          >
            support@flavorwiki.com
          </Link>{' '}
          or by phone at +1 (910) 722 1560 or +41 79 137 2228.
        </Paragraph>
        <Paragraph>
          All rights on the Terms belong to their author. Any reproduction,
          without prior license, is strictly forbidden.
        </Paragraph>
        <Bold>Thank You for visiting FlavorWiki.</Bold>
      </Footer>
    </Container>
  )
}

export default Body
