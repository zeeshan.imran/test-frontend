import React from 'react'
import { Container } from './styles'
import Header from './Header'
import Body from './Body'
import Footer from './Footer'

const TermsOfUse = ({ goToPrivacyPolicy, isOperator }) => {
  return (
    <Container>
      {!isOperator && <Header />}
      <Body />
      {!isOperator && <Footer goToPrivacyPolicy={goToPrivacyPolicy} />}
    </Container>
  )
}

export default TermsOfUse
