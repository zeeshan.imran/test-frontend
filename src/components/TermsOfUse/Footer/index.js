import React from 'react'
import { Container } from './styles'
import { Button } from 'antd'

const Footer = ({goToPrivacyPolicy}) => {
  return <Container>
    <Button onClick={goToPrivacyPolicy}>
      Privacy Policy
    </Button>
  </Container>
}

export default Footer