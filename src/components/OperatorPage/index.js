import React from 'react'
import { Page } from './styles'

const OperatorPage = ({ children }) => <Page>{children}</Page>

export default OperatorPage
