import React, { useState, useEffect } from 'react'
import {
  AntModal,
  AntModalMain,
  Image,
  AntModalTitle,
  AntModalContent,
  AntButton
} from './styles'
import imageSrc from '../../../assets/pictures/onboarding/get-started.jpg'
import { getAuthenticatedUser, getImpersonateUserId } from '../../../utils/userAuthentication'
import { useTranslation } from 'react-i18next'
import { USER_TYPE } from '../../../utils/Constants'

const ModalGetStarted = ({
  loading,
  isCompulsorySurveyTaken,
  compulsorySurveys = [],
  onGetStarted
}) => {
  const { t } = useTranslation()
  const user = getAuthenticatedUser() || {}
  const impersonateUserId = getImpersonateUserId() || null

  const cacheKey = `flavorwiki-taster-${user.id}-onboarded`
  const [visible, setVisible] = useState(false)

  useEffect(() => {
    if (!impersonateUserId && !loading && !isCompulsorySurveyTaken && user.type === USER_TYPE.TASTER) {
      const isOnBoarded = window.localStorage.getItem(cacheKey)

      if (
        !visible &&
        (compulsorySurveys.length > 0 ||
          (compulsorySurveys.length === 0 && !isOnBoarded))
      ) {
        setVisible(true)
      }
    }
  }, [impersonateUserId, loading, user, isCompulsorySurveyTaken, compulsorySurveys])

  const onClick = e => {
    e.preventDefault()
    window.localStorage.setItem(cacheKey, true)

    if (compulsorySurveys.length > 0) {
      onGetStarted(compulsorySurveys[0])
    }

    setVisible(false)
  }

  return (
    <AntModal visible={visible} closable={false} footer={null}>
      <Image src={imageSrc} alt='Flavorwiki' />

      <AntModalMain>
        <AntModalTitle>
          {t('components.modalGetStarted.modalTitle')}
        </AntModalTitle>
        <AntModalContent>
          {t('components.modalGetStarted.modalContent')}
        </AntModalContent>
      </AntModalMain>

      <AntButton type='primary' size='large' onClick={onClick}>
        {t('components.modalGetStarted.callToAction')}
      </AntButton>
    </AntModal>
  )
}

export default ModalGetStarted
