import styled from 'styled-components'
import { Modal, Button } from 'antd'

export const AntModal = styled(Modal)`
  text-align: center;
  padding: 40px;
`

export const AntModalMain = styled.main`
  margin-top: 8px;
  margin-bottom: 40px;
`

export const Image = styled.img`
  max-width: 100%
`

export const AntModalTitle = styled.h5`
  font-size: 20px;
  font-weight: 500;
  text-transform: capitalize;
  line-height: 1.2;
  margin-bottom: 8px;
`

export const AntModalContent = styled.div`
  white-space: pre-line;
`

export const AntButton = styled(Button)`
  padding: 0 40px;
`
