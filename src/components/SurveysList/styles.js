import styled, { css } from 'styled-components'
import Text from '../Text'
import { family } from '../../utils/Fonts'
import { Table } from 'antd'
import colors from '../../utils/Colors'

export const FieldText = styled(Text)`
  font-size: 1.4rem;
  font-family: ${family.primaryRegular};
  line-height: 2.2rem;
  color: rgba(0, 0, 0, 0.45);
  user-select: none;
  white-space: pre-wrap;
`

export const SurveyName = styled(FieldText)`
  font-family: ${family.primaryBold};
  color: rgba(0, 0, 0, 0.65);
  width: 100%;
  display: flex;
  align-self: center;
  font-size: 1.3rem;
  @media (max-width: 600px) {
    font-size: 1.1rem;
  }
  @media screen and (max-width: 1023px) and (min-width: 768px) {
    font-size: 1.2rem;
  }
  ${({ checkScreener }) =>
    checkScreener &&
    css`
      padding-left: 30px;
    `}
`

export const SurveyCount = styled(FieldText)`
  font-family: ${family.primaryBold};
  color: rgba(0, 0, 0, 0.65);
  display: flex;
  align-self: center;
  justify-content: center;
  font-size: 1.2rem;
  @media (max-width: 600px) {
    font-size: 1.1rem;
  }
  @media screen and (max-width: 1023px) and (min-width: 768px) {
    font-size: 1.2rem;
  }
`

export const ImagePlaceHolder = styled.div`
  max-width: 44px;
  width: 44px;
  max-height: 44px;
  height: 44px;
  margin-right: 1rem;
  border-radius: 3px;
  background-position: center center;
  ${({ photo }) =>
    photo &&
    css`
      background-size: cover;
      background-image: url(${photo});
    `}
`

export const AntTable = styled(Table)`
  td {
    padding: 0.5rem;
    font-size: ${({ mobile }) => (mobile ? '1rem' : '1.2rem')};
  }
  .ant-table-row-expand-icon {
    display: none;
  }
  tr.disabled {
    ${SurveyName} {
      color: #999;
      font-family: ${family.primaryRegular};
    }
    ${ImagePlaceHolder} {
      opacity: 0.5;
    }
  }
  th {
    text-align: center;
  }
  th:first-child {
    text-align: left;
  }
  @media (max-width: 600px) {
    th {
      font-size: 1.1rem;
    }
    th:first-child {
      text-align: center;
    }
  }
  @media screen and (max-width: 1023px) and (min-width: 768px) {
    th {
      font-size: 1.2rem;
    }
    th:first-child {
      text-align: center;
    }
  }
  @media (max-width: 480px) {
    min-width: 480px;
  }
`
export const TableWrapper = styled.div`
  @media (max-width: 480px) {
    overflow-x: auto;
  }
`

export const ColumnContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
`

export const SurveyInitials = styled.span`
  flex: 1;
  align-self: center;
  font-size: ${({ mobile }) => (mobile ? '1.1rem' : '1.4rem')};
  @media (max-width: 600px) {
    font-size: 1.1rem;
  }
  @media screen and (max-width: 1023px) and (min-width: 768px) {
    font-size: 1.2rem;
  }
`

export const SurveyCompleted = styled(FieldText)`
  font-family: ${family.primaryLight};
  @media (max-width: 600px) {
    font-size: 1.1rem;
  }
  @media screen and (max-width: 1023px) and (min-width: 768px) {
    font-size: 1.2rem;
  }
`

export const NotAllowedStatus = styled.div`
  font-family: ${family.primaryRegular};
  font-size: 12px;
  color: #ff914d;
  line-height: 12px;
  @media (max-width: 600px) {
    font-size: 1.1rem;
  }
  @media screen and (max-width: 1023px) and (min-width: 768px) {
    font-size: 1.2rem;
  }
`
export const AllowedStatus = styled.div`
  font-family: ${family.primaryRegular};
  font-size: 11px;
  color: ${colors.ALLOWED_STATUS_COLOR};
  line-height: 14px;
  @media (max-width: 600px) {
    font-size: 1.1rem;
  }
  @media screen and (max-width: 1023px) and (min-width: 768px) {
    font-size: 1.2rem;
  }
`
export const Action = styled.span`
  cursor: pointer;
  color: rgb(24, 144, 255);
  font-family: ${family.primaryRegular};
  font-size: 1.4rem;
  line-height: 1;
`
