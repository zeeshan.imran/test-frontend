import React, { useState, useEffect } from 'react'
import {
  SurveyName,
  SurveyInitials,
  AntTable,
  ImagePlaceHolder,
  AllowedStatus,
  NotAllowedStatus,
  SurveyCount,
  TableWrapper,
  Action
} from './styles'
import { useTranslation } from 'react-i18next'
import { imagesCollection } from '../../assets/png/index.js'
import useResponsive from '../../utils/useResponsive'
import { CopyToClipboard as Copy } from 'react-copy-to-clipboard'
import { getShareLink } from '../../utils/shareUtils'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import {
  setCurrentSelectedSurvey,
  getAuthenticatedUser
} from '../../utils/userAuthentication'
import { displayInfoPopup } from '../../utils/displayInfoPopup'
import { DEFAULT_N_ELEMENTS_PER_PAGE } from '../../utils/Constants'

const NOT_COMPLETED = 'not-completed'
const COMPLETED = 'COMPLETED'

const SurveyStatus = ({ statusCheck, allowRetakes = false , screenOut = false ,screenOutSettings }) => {
  const { t } = useTranslation()
  const {
    enrollment: {
      totalRetake,
      maxTimeTaken,
      disableRow,
      retakeAfterString
    } = {},
    state,
    status
  } = statusCheck && statusCheck
  const reject = screenOutSettings && screenOutSettings.reject
  const completedString = t('components.surveyList.completed')
  const timeCondition = status === NOT_COMPLETED && state !== false
  if (allowRetakes && totalRetake) {
    if ((maxTimeTaken || disableRow) && timeCondition && totalRetake > 1) {
      return `${completedString} ${totalRetake - 1}x ,Click to resume survey`
    }

    if (timeCondition && totalRetake === 1) {
      return <AllowedStatus>{`Click to resume survey`}</AllowedStatus>
    }
    if (maxTimeTaken) {
      return `${completedString} ${totalRetake}x ,No more retakes`
    }
    if (disableRow) {
      return `${completedString} ${totalRetake}x , ${retakeAfterString}`
    } else if (timeCondition) {
      return <AllowedStatus>{`Click to resume survey`}</AllowedStatus>
    } else if (retakeAfterString && !disableRow) {
      return `${completedString} ${totalRetake}x , ${retakeAfterString}`
    } else {
      return (
        <AllowedStatus>
          {`${completedString} ${totalRetake}x,${t(
            'components.surveyList.ready'
          )}`}
        </AllowedStatus>
      )
    }
  }

  if (state === 'rejected' && screenOut && reject) {
    return ( <NotAllowedStatus>{t('components.surveyList.waitList')}</NotAllowedStatus>)
  }
  if (state === false && status === NOT_COMPLETED) {
    return <AllowedStatus>{t('components.surveyList.ready')}</AllowedStatus>
  } else if (state !== false && status === NOT_COMPLETED) {
    return <AllowedStatus>{t('components.surveyList.ready')}</AllowedStatus>
  }

  return (
    <NotAllowedStatus>{t(`components.surveyList.${state}`)}</NotAllowedStatus>
  )
}

const SurveysList = ({
  surveys,
  handleNavigateToSurvey,
  loading,
  findClosedEnrollments,
  findAllEnrollments,
  expandedRowKeys,
  getCurrency,
  allSurveyEnrollements,
  compulsorySurveys,
  isCompulsorySurveyTaken,
  isExpired
}) => {
  const { t } = useTranslation()
  const { mobile } = useResponsive()
  const [surveyList, setSurveyList] = useState([])
  const loggedInUser = getAuthenticatedUser()
  const mobileImage = process.env.REACT_APP_THEME
    ? imagesCollection[`${process.env.REACT_APP_THEME}Mobile`]
    : imagesCollection['defaultMobile']

  const getEnrollmentState = survey => {
    const enrollment = findAllEnrollments(survey)
    if (
      enrollment &&
      (enrollment.state === 'rejected' || enrollment.state === 'finished')
    ) {
      return { state: enrollment.state, status: COMPLETED, enrollment }
    } else if (
      enrollment &&
      (enrollment.state !== 'rejected' || enrollment.state !== 'finished')
    ) {
      return { state: enrollment.state, status: NOT_COMPLETED, enrollment }
    }
    return { state: false, status: NOT_COMPLETED, enrollment }
  }

  const expireCheck = survey => {
    const allEnrollments = findAllEnrollments(survey)
    const { maxTimeTaken, disableRow } = allEnrollments || {}
    if (maxTimeTaken && survey.allowRetakes) {
      return `survey-completed-ant-desgin-table disabled`
    } else if (!disableRow && survey.allowRetakes) {
      return `clickable`
    }
    if (findClosedEnrollments(survey) || isExpired(survey)) {
      return `survey-completed-ant-desgin-table disabled`
    } else {
      return `clickable`
    }
  }
  // Method to get share link
  const copyLink = survey => {
    const enrollment = findAllEnrollments(survey)
    if (enrollment) {
      let {
        uniqueName,
        screeners = [],
        compulsorySurvey,
        showSharingLink
      } = survey
      const { id } = enrollment
      let shareSurvey = uniqueName
      if (screeners && screeners.length > 0) {
        shareSurvey = survey.screeners[0].uniqueName
        showSharingLink = survey.screeners[0].showSharingLink
      }
      if (!compulsorySurvey && showSharingLink) {
        return (
          <Copy
            text={getShareLink(shareSurvey, id)}
            onCopy={() => {
              displaySuccessMessage(t(`containers.surveySharing.copiedMessage`))
            }}
          >
            <Action
              onClick={event => {
                event.stopPropagation()
              }}
            >
              {t(`containers.surveySharing.shareTextTaster`)}
            </Action>
          </Copy>
        )
      } else {
        return '-'
      }
    }
  }

  useEffect(() => {
    setSurveyList(surveys)
  }, [surveys])

  const maxReward = survey => {
    const {
      isScreenerOnly,
      linkedSurveys,
      products,
      productsRewardRule,
      compulsorySurvey,
      allowRetakes = false,
      maximumReward = 0
    } = survey
    const maximumRewardCurrency =
      maximumReward && getCurrency(survey.country, maximumReward)
    const allStatuses = getEnrollmentState(survey)
    let sum = 0
    let currencySum
    const {
      enrollment: { maxTimeTaken, savedRewards, totalEarned } = {},
      state
    } = allStatuses

    if (allowRetakes) {
      if (maxTimeTaken && savedRewards.length && state === 'finished') {
        currencySum = getCurrency(survey.country, totalEarned)
        return `${currencySum} Earned`
      }
      if (!maxTimeTaken && state === 'finished') {
        products.forEach(p => {
          if (p.isAvailable) {
            sum += p.reward
          }
        })
        currencySum = getCurrency(survey.country, sum)
        return !maximumReward
          ? `Earn up to ${currencySum}`
          : maximumReward > sum
          ? `Earn up to ${maximumRewardCurrency}`
          : `${currencySum} Earned`
      }
    } else {
      if (savedRewards && savedRewards.length && state === 'finished') {
        sum = getCurrency(survey.country, totalEarned)
        return `${sum} Earned`
      }
    }
    if (compulsorySurvey) {
      return ''
    }
    if (isScreenerOnly) {
      let canTakeSurvey = true
      let totalEarnedSum = 0
      linkedSurveys &&
        linkedSurveys.forEach(s => {
          let linkedResult = findAllEnrollments(s)
          totalEarnedSum += linkedResult ? linkedResult.totalEarned : 0
          if (
            linkedResult &&
            linkedResult.survey &&
            linkedResult.savedRewards &&
            linkedResult.savedRewards.length
          ) {
            let allowRetakes = linkedResult.survey.allowRetakes
              ? linkedResult.survey.maxRetake - linkedResult.totalRetake
              : 0
            canTakeSurvey =
              canTakeSurvey &&
              (linkedResult.state !== 'finished' || allowRetakes)
            linkedResult.savedRewards.forEach(p => {
              sum +=
                linkedResult.productsRewardRule &&
                linkedResult.productsRewardRule.active
                  ? linkedResult.productsRewardRule.max
                  : p.reward
            })
          } else {
            s.products.forEach(p => {
              if (p.isAvailable) {
                sum +=
                  productsRewardRule && productsRewardRule.active
                    ? productsRewardRule.max
                    : p.reward
              }
            })
          }
        })
      currencySum = getCurrency(survey.country, sum)
      const currencyTotalEarnedSum = getCurrency(survey.country, totalEarnedSum)
      return state !== 'finished'
        ? `Qualify to earn up to ${
            maximumRewardCurrency ? maximumRewardCurrency : currencySum
          }` //when the screener is not completed
        : !canTakeSurvey
        ? `${currencyTotalEarnedSum} earned` // none of the surveys can be taken (finished with no retakes allowed)
        : !maximumReward
        ? `Earn up to ${currencySum}` //there is no maximum reward => we use the sum
        : totalEarnedSum >= maximumReward
        ? `${currencyTotalEarnedSum} earned` // taster can not earn anymore
        : `Earn up to ${maximumRewardCurrency}` // taste can earn more
    } else {
      let result = findAllEnrollments(survey)
      if (result && result.savedRewards && result.savedRewards.length) {
        result.savedRewards.forEach(p => {
          if (result.state === 'finished') {
            sum += p.answered ? p.reward : 0
          } else {
            sum += p.reward
          }
        })
      } else {
        products.forEach(p => {
          if (p.isAvailable) {
            sum += p.reward
          }
        })
      }

      sum = getCurrency(survey.country, sum)
      return `Earn up to ${maximumRewardCurrency ? maximumRewardCurrency : sum}`
    }
  }

  const getNumberOfLinkedSurveys = survey => {
    const { isScreenerOnly, linkedSurveys, compulsorySurvey } = survey
    if (compulsorySurvey) {
      return ''
    }
    return survey && isScreenerOnly && linkedSurveys
      ? `${linkedSurveys.length} ${t('components.surveysList.surveys')}`
      : '-'
  }

  const getNumberOfProducts = survey => {
    const { isScreenerOnly, products, compulsorySurvey } = survey

    if (survey) {
      if (compulsorySurvey) {
        return ''
      }

      if (!isScreenerOnly && products) {
        const latestEnrollment = findAllEnrollments(survey)
        const { savedRewards = [], state } = latestEnrollment || {}
        let countOfProducts = 0
        if (savedRewards && savedRewards.length) {
          countOfProducts =
            state === 'finished'
              ? savedRewards.filter(sr => sr.answered).length
              : savedRewards.length
        } else {
          countOfProducts = products.filter(product => {
            return product.isAvailable
          }).length
        }

        return `${countOfProducts} ${t('components.surveysList.products')}`
      }

      return '-'
    }
  }
  const rowClick = (event, survey) => {
    const allEnrollments = findAllEnrollments(survey)
    const { maxTimeTaken, disableRow, state } = allEnrollments || {}
    if (
      compulsorySurveys &&
      compulsorySurveys.length &&
      !isCompulsorySurveyTaken &&
      loggedInUser &&
      loggedInUser.type === 'taster'
    ) {
      setCurrentSelectedSurvey(survey.id)
      handleNavigateToSurvey(compulsorySurveys[0])
    } else if (maxTimeTaken || disableRow) {
      event.stopPropagation()
      if (maxTimeTaken) {
        if (state === 'finished') {
          return displayInfoPopup(
            t('containers.surveySharing.maxRetakeMessage')
          )
        } else {
          handleNavigateToSurvey(survey)
        }
      }

      if (disableRow && state === 'finished') {
        return displayInfoPopup(
          t('containers.surveySharing.notAvailableMessage')
        )
      } else {
        handleNavigateToSurvey(survey)
      }
      handleNavigateToSurvey(survey)
    } else if (
      !isExpired(survey) &&
      (!getEnrollmentState(survey).status !== NOT_COMPLETED ||
        survey.allowRetakes)
    ) {
      handleNavigateToSurvey(survey)
    }
  }
  const columns = [
    {
      key: 'survey',
      title: t('components.surveysList.survey'),
      render: (_, survey) => {
        return (
          <React.Fragment>
            <SurveyName checkScreener={survey.hasScreener}>
              {survey.coverPhoto ? (
                <ImagePlaceHolder photo={survey.coverPhoto} />
              ) : (
                <ImagePlaceHolder photo={mobileImage} />
              )}

              <SurveyInitials mobile={mobile}>{survey.title}</SurveyInitials>
            </SurveyName>
          </React.Fragment>
        )
      }
    },
    {
      key: 'noOfSurveys',
      title: t('components.surveysList.noOfSurveys'),
      render: (_, survey) => {
        return (
          <React.Fragment>
            <SurveyCount>{getNumberOfLinkedSurveys(survey)}</SurveyCount>
          </React.Fragment>
        )
      }
    },
    {
      key: 'noOfProducts',
      title: t('components.surveysList.noOfProducts'),
      render: (_, survey) => {
        return (
          <React.Fragment>
            <SurveyCount>{getNumberOfProducts(survey)}</SurveyCount>
          </React.Fragment>
        )
      }
    },
    {
      key: 'status',
      title: t('components.surveysList.status'),
      render: (_, survey) => {
        return (
          <React.Fragment>
            <SurveyName>
              <SurveyStatus
                allowRetakes={survey.allowRetakes}
                statusCheck={getEnrollmentState(survey)}
                maxRetake={survey.maxRetake}
                retakeAfter={survey.retakeAfter}
                screenOut = {survey.screenOut}
                screenOutSettings = {survey.screenOutSettings}
              />
            </SurveyName>
          </React.Fragment>
        )
      }
    },
    {
      title: t('components.surveysList.reward'),
      render: (_, survey) => {
        if (
          isExpired(survey) &&
          getEnrollmentState(survey).status !== COMPLETED
        ) {
          return <SurveyName>{t('components.surveysList.expired')}</SurveyName>
        }
        return (
          <React.Fragment>
            <SurveyName>{maxReward(survey)}</SurveyName>
          </React.Fragment>
        )
      }
    },
    // Share link column and method
    {
      key: 'shareLink',
      title: t(`components.surveysList.share`),
      render: (_, survey) => {
        return (
          <React.Fragment>
            <SurveyCount>{copyLink(survey)}</SurveyCount>
          </React.Fragment>
        )
      }
    }
  ]

  return (
    <TableWrapper>
      <AntTable
        mobile={mobile}
        rowKey={record => record.key}
        expand
        expandedRowKeys={expandedRowKeys}
        rowClassName={survey => expireCheck(survey)}
        columns={columns}
        dataSource={surveyList}
        showHeader
        pagination={{
          pageSize: DEFAULT_N_ELEMENTS_PER_PAGE
        }}
        locale={{ emptyText: t('notAvailableinYourCountry') }}
        loading={loading}
        onRow={survey => ({
          onClick: e => rowClick(e, survey)
        })}
      />
    </TableWrapper>
  )
}

export default SurveysList
