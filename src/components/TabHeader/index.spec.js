import React from 'react'
import { shallow } from 'enzyme'
import TabHeader from '.';

describe('TabHeader', () => {

    let testRender;
    let icon;
    let title;


    beforeEach(() => {
        icon = 100;
        title = "Answered"
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render TabHeader', async () => {
        testRender = shallow(
            <TabHeader
                icon={icon}
                title={title}
            />
        )
        expect(testRender).toMatchSnapshot()
    })
});