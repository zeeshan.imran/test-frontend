import React from 'react'
import { mount } from 'enzyme'
import { Router } from 'react-router-dom'
import { act } from 'react-dom/test-utils'
import wait from '../../utils/testUtils/waait'
import AdditionalInformationForm from './index'
import history from '../../history'
import { MockedProvider } from 'react-apollo/test-utils'
import INCOME_RANGES_QUERY from '../../queries/incomeRanges'
import { CustomForm, CustomCol } from './styles'
import Select from '../Select'
import { Col } from 'antd'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = {
      ...Component.defaultProps,
      t: () => '',
      i18n: {
        t: () => ''
      }
    }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

const mocks = [
  {
    request: { query: INCOME_RANGES_QUERY, variables: { currency: null } },
    result: { data: { incomeRanges: {} } }
  }
]

describe('AdditionalInformationForm', () => {
  let testRender
  let desktop
  let submitForm
  let ethnicity
  let ethnicities
  let currency
  let incomeRange
  let hasChildren
  let childrenAges
  let smokerKinds
  let smokerKind
  let productCategories
  let preferredProductCategories
  let dietaryPreferences
  let dietaryPreference

  beforeEach(() => {
    desktop = 'center'
    submitForm = jest.fn()
    ethnicity = null
    ethnicities = [
      { label: 'START', value: 'foo' },
      { label: 'START', value: 'baz' },
      { label: 'END', value: 'foo' }
    ]
    currency = '$'
    incomeRange = 2
    hasChildren = 'Yes'
    childrenAges = [12]
    smokerKinds = []
    smokerKind = null
    productCategories = [
      { label: 'Cat 1', value: '1' },
      { label: 'Cat 2', value: '2' }
    ]
    preferredProductCategories = '1'
    dietaryPreferences = [
      { label: 'dietary 1', value: '1' },
      { label: 'dietary 2', value: '2' }
    ]
    dietaryPreference = null
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('CustomCol', () => {
    testRender = mount(<CustomCol />)

    expect(testRender.find(Col)).toHaveLength(1)
  })

  test('should render AdditionalInformationForm', async () => {
    testRender = mount(
      <MockedProvider mocks={mocks}>
        <Router history={history}>
          <AdditionalInformationForm
            desktop={desktop}
            submitForm={submitForm}
            ethnicity={ethnicity}
            ethnicities={ethnicities}
            currency={currency}
            incomeRange={incomeRange}
            hasChildren={hasChildren}
            childrenAges={childrenAges}
            smokerKinds={smokerKinds}
            smokerKind={smokerKind}
            productCategories={productCategories}
            preferredProductCategories={preferredProductCategories}
            dietaryPreferences={dietaryPreferences}
            dietaryPreference={dietaryPreference}
          />
        </Router>
      </MockedProvider>
    )

    expect(testRender.find(AdditionalInformationForm)).toHaveLength(1)

    await wait(0)
    testRender.update()

    expect(testRender.find(CustomForm)).toHaveLength(1)

    //
    act(() => {
      testRender
        .find(Select)
        .filter({ name: 'hasChildren' })
        .prop('onChange')({ target: { value: 'Yes' } })
    })

    await wait(0)
    testRender.update()

    expect(
      testRender.find(Select).filter({ name: 'children-age-0' })
    ).toHaveLength(1)

    act(() => {
      testRender.find('form').simulate('submit')
    })
    expect(submitForm).toHaveBeenCalled()
  })
})
