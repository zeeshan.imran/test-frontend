import React from 'react'
import PropTypes from 'prop-types'
import ChatIcon from '../../assets/svg/chat.svg'
import { Shape, Icon } from './styles'

const ChatButton = ({ onClick }) => (
  <Shape onClick={onClick}>
    <Icon src={ChatIcon} />
  </Shape>
)

ChatButton.propTypes = {
  onClick: PropTypes.func
}

ChatButton.defaultProps = {}

export default ChatButton
