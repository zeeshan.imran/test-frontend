import React, { Component } from 'react'
import styled from 'styled-components'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import ProductCard from './'

const Container = styled.div``
const product = {
  name: 'Unilever I Cant Believe Its Not Butter Vegetable Oil Spread, 15 oz',
  image:
    'https://i5.walmartimages.com/asr/be75486b-2623-4f57-9713-efa5bdbec9b1_1.05716c9f3ab241decfb7c7a81f5e6557.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF'
}

class StateWrapper extends Component {
  state = {
    selected: false,
    ranking: 0
  }

  render () {
    return this.props.children({
      toggleSelected: () => this.setState({ selected: !this.state.selected }),
      selected: this.state.selected,
      ranking: this.state.ranking,
      setRanking: ranking => this.setState({ ranking })
    })
  }
}

storiesOf('ProductCard', module)
  .add('Card Without Selection', () => (
    <Container>
      <ProductCard
        onClick={action('pressed')}
        name={product.name}
        image={product.image}
      />
    </Container>
  ))
  .add('Card With Selection', () => (
    <Container>
      <StateWrapper>
        {({ toggleSelected, selected }) => {
          return (
            <ProductCard
              selected={selected}
              onClick={toggleSelected}
              withSelection
              name={product.name}
              image={product.image}
            />
          )
        }}
      </StateWrapper>
    </Container>
  ))
  .add('Card Without Selection Locked', () => (
    <Container>
      <ProductCard name={product.name} image={product.image} locked />
    </Container>
  ))
  .add('Card With Ranking', () => (
    <Container>
      <StateWrapper>
        {({ ranking, setRanking }) => {
          return (
            <Container>
              <ProductCard
                ranking={ranking}
                maxRanking={6}
                name={product.name}
                image={product.image}
                onClick={setRanking}
              />
            </Container>
          )
        }}
      </StateWrapper>
    </Container>
  ))
