import React from 'react'
import { mount } from 'enzyme'
import ProductCard from '.'
import BaseCard from '../BaseCard'

window.matchMedia = jest.fn().mockImplementation(query => {
  return {
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(),
    removeListener: jest.fn()
  }
})

describe('ProductCard', () => {
  let testRender
  let name
  let image
  let withSelection
  let selected
  let locked
  let maxRanking
  let ranking
  let onClick
  let mobile

  beforeEach(() => {
    name = 'Ensure MAX protein French Vanilla'
    image =
      'https://i5.walmartimages.com/asr/be75486b-2623-4f57-9713-efa5bdbec9b1_1.05716c9f3ab241decfb7c7a81f5e6557.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF'
    withSelection = true
    selected = true
    locked = false
    maxRanking = 0
    ranking = 0
    onClick = jest.fn()
    mobile = '11.5rem'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ProductCard', async () => {
    testRender = mount(
      <ProductCard
        name={name}
        image={image}
        withSelection={withSelection}
        selected={selected}
        locked={locked}
        maxRanking={maxRanking}
        ranking={ranking}
        onClick={onClick}
        mobile={mobile}
      />
    )
    expect(testRender.find(ProductCard)).toHaveLength(1)

    testRender
      .find(BaseCard)
      .props()
      .onClick()

    expect(onClick).toHaveBeenCalled()
  })

  test('should render ProductCard handalClick onClick', async () => {
    testRender = mount(
      <ProductCard
        name={name}
        image={image}
        withSelection={withSelection}
        selected={selected}
        locked={locked}
        maxRanking={5}
        ranking={ranking}
        onClick={onClick}
      />
    )
    expect(testRender.find(ProductCard)).toHaveLength(1)

    testRender
      .find(BaseCard)
      .props()
      .onClick({ target: { value: '5' } })

    expect(onClick).toHaveBeenCalledWith(1)
  })

  test('should render ProductCard handalClick for locked', async () => {
    const lockedChange = true
    testRender = mount(<ProductCard locked={lockedChange} onClick={onClick} />)
    expect(testRender.find(ProductCard)).toHaveLength(1)

    testRender
      .find(BaseCard)
      .props()
      .onClick()

    expect(onClick).not.toHaveBeenCalled()
  })
})
