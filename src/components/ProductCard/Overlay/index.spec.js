import React from 'react'
import { shallow } from 'enzyme'
import Overlay from '.';

describe('Overlay', () => {
    let testRender
    let maxRanking
    let ranking
    let locked

    beforeEach(() => {
        maxRanking = 2
        ranking = 3
        locked = true
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render Overlay', async () => {
        testRender = shallow(

            <Overlay
                maxRanking={maxRanking}
                ranking={ranking}
                locked={locked}
            />
        )
        expect(testRender).toMatchSnapshot()
    })
});