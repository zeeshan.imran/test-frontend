import React from 'react'
import { Menu as AntMenu, Icon as AntIcon } from 'antd'
import { StyledMenu, Icon, Item } from './styles'

import survey_icon from '../../../../assets/png/survey_icon.png'
import terms_of_use from '../../../../assets/png/terms_of_use.png'
import privacy_policy from '../../../../assets/png/privacy_policy.png'

const customIcons = {
  survey_icon,
  terms_of_use,
  privacy_policy
}

const Menu = ({ entries, handleMenuClick, activeMenuEntry }) => (
  <StyledMenu
    theme='light'
    mode='inline'
    selectedKeys={[`${activeMenuEntry}`]}
    onClick={({ key }) => {
      // key == path
      handleMenuClick(key)
    }}
  >
    
    {entries.map((entry, index) => {
      const isActive = (entry.path === activeMenuEntry || entry.alias === activeMenuEntry).toString()

      const icon = (
        <React.Fragment>
          {customIcons[entry.icon] ? (
            <Icon
              src={customIcons[entry.icon]}
              isactive={isActive}
            />
          ) : (
            <AntIcon 
              type={entry.icon}
              isactive={isActive}
            />
          )}
        </React.Fragment>
      )

      return entry.submenu ? (
        <AntMenu.SubMenu
          key={index}
          title={
            <span>
              {icon}
              <span>{entry.title}</span>
            </span>
          }
        >
          {entry.submenu.map((submenu, i) => (
            <Item key={submenu.path} title={submenu.title}>
              <span>{submenu.title}</span>
            </Item>
          ))}
        </AntMenu.SubMenu>
      ) : (
        <Item key={entry.path} title={entry.title}>
          {icon}
          <span>{entry.title}</span>
        </Item>
      )
    })}
  </StyledMenu>
)

export default Menu
