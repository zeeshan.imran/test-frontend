import styled from 'styled-components'
import { Menu as AntMenu } from 'antd'
import colors from '../../../../utils/Colors'
import pngFilters from './pngFilters'

export const Icon = styled.div`
  background: url(${props => props.src}) no-repeat;
  display: inline-block;
  background-size: contain;
  width: 14px;
  height: 14px; 
  font-size: 14px;
  margin-right: 10px;
  vertical-align: -0.75px;
  filter: ${props => {
    return props.isActive
      ? pngFilters[process.env.REACT_APP_THEME]
      : ``
  }};
  -webkit-filter: ${props =>
    props.isActive
      ? 
      pngFilters[process.env.REACT_APP_THEME]
      : ``};
`

export const Item = styled(AntMenu.Item)`
  color : ${colors.SIDES_TEXT_COLOR};
  &:hover {
    ${Icon} {
      filter: ${pngFilters[process.env.REACT_APP_THEME]};
      -webkit-filter: ${pngFilters[process.env.REACT_APP_THEME]};
    }
    span{
      color : ${colors.SIDES_TEXT_HOVER_COLOR};
    }
  }

  &.ant-menu-item-selected {

    .anticon {
      color: ${colors.SIDES_TEXT_HOVER_COLOR};
    }
  }
`

export const StyledMenu = styled(AntMenu)`
  flex-basis: 0;
  flex-grow: 1;
  overflow-x: hidden;
  overflow-y: auto;

  &::-webkit-scrollbar {
    width: 0px;
    height: 0px;
    background: transparent;
  }

  &.ant-menu-inline,
  &.ant-menu-vertical,
  &.ant-menu-vertical-left {
    width: 100%;
  }
  &.ant-menu-inline .ant-menu-submenu {
    margin-bottom: 8px;
    margin-top: 4px;
  }
  &.ant-menu-inline-collapsed {
    & > .ant-menu-item {
      padding: 1rem 2.4rem !important;
      display: flex;
      flex-direction: column;
      align-items: center;
      height: 68px;

      span {
        height: auto;
        line-height: normal;
        opacity: 1;
        max-width: fit-content;
        font-size: 1.2rem;
      }
    }

    .anticon,
    ${Icon} {
      min-width: 2.5rem;
      height: 2.5rem;
      line-height: 2.5rem;
      margin-bottom: 1rem;
      margin-right: 0 !important;
    }
    svg {
      min-width: 2.5rem;
      height: 2.5rem;
    }
    ${Icon} {
      background-position: center top;
    }
  }
`
