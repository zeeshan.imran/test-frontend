import styled from 'styled-components'
import { Layout as AntLayout } from 'antd'
import colors from '../../../utils/Colors'

const { Content: AntContent } = AntLayout

export const StyledContent = styled(AntContent)`
  background-color: ${colors.ADMIN_CONTENT_BACKGROUND};
  position: relative;
  flex: 1;
  overflow-y: none;
  display: flex;
  flex-direction: column;
`
