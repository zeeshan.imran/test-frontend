import React from 'react'
import { shallow } from 'enzyme'
import OperatorLayout from '.'

describe('OperatorLayout', () => {
  window.localStorage = {
    getItem: () => '{}'
  }

  let testRender
  let children
  let menu
  let handleMenuEntryClick
  let activeMenuEntry
  let username

  beforeEach(() => {
    menu = [
      {
        title: 'Menu 1',
        visible: true
      },
      {
        title: 'Menu 2',
        visible: true
      }
    ]
    handleMenuEntryClick = jest.fn()
    activeMenuEntry = jest.fn()
    username = 'test'
    children = '<container>Hello</container>'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render OperatorLayout', async () => {
    testRender = shallow(
      <OperatorLayout
        menu={menu}
        handleMenuEntryClick={handleMenuEntryClick}
        activeMenuEntry={activeMenuEntry}
        username={username}
        children={children}
      />
    )
    expect(testRender).toMatchSnapshot()
  })
})
