import styled from 'styled-components'
import { Layout } from 'antd'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'

export const ContentContainer = styled(Layout)`
  height: 100vh;
  position: relative;
  display: flex;
  flex-direction: column;
  z-index: 0;
`

export const TooltipLayer = styled.div`
  top: 0;
  left: 0;
  position: absolute;
  z-index: 10;

  .tooltip-container {
    background: rgba(255, 255, 255, 0.85);
    border: 1px ${colors.LIGHT_OLIVE_GREEN} solid;
    transform: translateX(-50%);
    border-radius: 2px;
    padding: 3px 8px;
    font-size: 10px;
    white-space: normal;
    max-width: 300px;
    color: ${colors.BLACK};
    z-index: 10;
    word-wrap: break-word;
    
    .tooltip-row {
      font-family: ${family.primaryBold};
    }
    .tooltip-title {
      font-family: ${family.primaryBold};
      font-size: 12px;
      margin-bottom: 2px;
    }
  }
`
