import TabHorizontal from '.'

describe('TabHorizontal', () => {
  let tabPages
  let contents
  let f
  let tabId

  beforeEach(() => {
    tabPages = [{ name: 'tab one', path: 'tab-one' }]

    contents = 'First tab content here'
    f = jest.fn()
    tabId = 'tab-one'
  })

  test('should render TabHorizontal', () => {
    expect(TabHorizontal(tabPages, contents, f, tabId)).toBeTruthy()
  })
})
