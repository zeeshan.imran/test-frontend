import styled from 'styled-components'

export const Container = styled.div`
  font-size: 12px;
`
export const TabText = styled.div`
  display: flex;
  align-items: center;
  .ant-badge {
    margin-left: 0.5rem;
  }
`
