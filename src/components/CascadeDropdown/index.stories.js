import React, { Component } from 'react'
import { storiesOf } from '@storybook/react'
import CascadeDropdown from './'

const options = [
  {
    value: 'dairyAndCheese',
    label: 'Dairy & Cheese',
    isLeaf: false
  },
  {
    value: 'bakedGoods',
    label: 'Baked Goods',
    isLeaf: false
  },
  {
    value: 'softDrinks',
    label: 'Soft Drinks',
    isLeaf: false
  },
  {
    value: 'snacks',
    label: 'Snacks',
    isLeaf: false
  }
]

class StateWrapper extends Component {
  state = {
    options
  }

  render () {
    return this.props.children({
      options: this.state.options,
      handleChange: value =>
        console.log(`Selected Value: ${value[value.length - 1]}`),
      handleLoadData: selectedOptions => {
        const targetOption = selectedOptions[selectedOptions.length - 1]
        targetOption.loading = true

        // load options lazily
        setTimeout(() => {
          targetOption.loading = false
          targetOption.children = [
            {
              label: `${targetOption.label} Dynamic Field 1`,
              value: 'dynamic1'
            },
            {
              label: `${targetOption.label} Dynamic Field 2`,
              value: 'dynamic2'
            }
          ]
          this.setState({
            options: [...this.state.options]
          })
        }, 100)
      }
    })
  }
}

storiesOf('CascadeDropdown', module).add('Story', () => (
  <StateWrapper>
    {({ options, handleChange, handleLoadData }) => {
      return (
        <CascadeDropdown
          options={options}
          handleChange={handleChange}
          handleLoadData={handleLoadData}
        />
      )
    }}
  </StateWrapper>
))
