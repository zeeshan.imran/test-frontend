import styled from 'styled-components'
import { Cascader as AntCascader } from 'antd'

export const Cascader = styled(AntCascader)`
  width: 100%;
`
