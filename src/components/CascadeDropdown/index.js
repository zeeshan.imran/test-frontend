import React from 'react'
import { Cascader } from './styles'

const CascadeDropdown = ({ options, handleLoadData, handleChange }) => (
  <Cascader
    options={options}
    loadData={handleLoadData}
    onChange={handleChange}
    displayRender={label => label[label.length - 1]}
  />
)

export default CascadeDropdown
