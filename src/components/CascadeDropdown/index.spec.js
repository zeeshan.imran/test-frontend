import React from 'react'
import { mount } from 'enzyme'
import CascadeDropdown from '.';

describe('CascadeDropdown', () => {
    let testRender
    let options
    let handleLoadData
    let handleChange
    
    beforeEach(() => {
        options = [
            {
                value: 'dairyAndCheese',
                label: 'Dairy & Cheese',
                isLeaf: false
            },
            {
                value: 'bakedGoods',
                label: 'Baked Goods',
                isLeaf: false
            },
            {
                value: 'softDrinks',
                label: 'Soft Drinks',
                isLeaf: false
            },
            {
                value: 'snacks',
                label: 'Snacks',
                isLeaf: false
            }
        ]
        handleLoadData = jest.fn()
        handleChange=jest.fn()
        
    })

    afterEach(() => {
        testRender.unmount()
    })

    test('should render CascadeDropdown', async () => {
        testRender = mount(
            <CascadeDropdown
                options={options}
                handleLoadData={handleLoadData}
                handleChange={handleChange}
            />
        )
        expect(testRender).toMatchSnapshot()
    })
});