import styled from 'styled-components'
import Button from '../Button'

export const Container = styled.div`
  width: 100%;
`

export const ButtonAligner = styled.div`
  margin-top: 3.5rem;
  margin-bottom: ${({ desktop }) => (desktop ? 0 : `5rem`)};
`

export const TabsAligner = styled.div`
  display: flex;
  flex-direction: ${({ desktop }) => (desktop ? 'row' : 'column')};
  justify-content: center;
`

export const FormContainer = styled.div`
  padding-top: 3.5rem;
`

export const StyledButton = styled(Button)`
  width: 100%;
`
