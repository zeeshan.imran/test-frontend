import React from 'react'
import PayPal from '../../../components/SvgIcons/Paypal'
import PaymentCard from '../PaymentCard'
import { useTranslation } from 'react-i18next'


const PayPalCard = props => {
  const { t } = useTranslation();
  return (
    <PaymentCard
      {...props}
      logo={<PayPal />}
      title={t('components.paymentDetailsForm.payPalCard')}
      colors={['#009cde', '#003087']}
    />
  )
}

export default PayPalCard
