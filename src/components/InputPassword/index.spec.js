import React from 'react'
import { shallow } from 'enzyme'
import InputPassword from '.'

jest.mock('react-i18next', () => ({
  // this mock makes sure any components using the translate HoC receive the t function as a prop
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  }
}))

describe('InputPassword', () => {
  let testRender
  let handleChange
  let handleValidityChange
  let isRequired

  beforeEach(() => {
    handleChange = jest.fn()
    handleValidityChange = jest.fn()
    isRequired = true
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render InputPassword', async () => {
    testRender = shallow(
      <InputPassword
        handleChange={handleChange}
        handleValidityChange={handleValidityChange}
        isRequired={isRequired}
      />
    )
    expect(testRender).toMatchSnapshot()
  })
})
