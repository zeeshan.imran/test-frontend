import React, { useState } from 'react'
import { Form } from 'antd'
import * as Yup from 'yup'
import Input from '../Input'
import { withTranslation } from 'react-i18next'

const validateRequiredPassword = (password) =>
  Yup.string()
    .required()
    .min(8, '')
    .trim()
    .isValidSync(password)

const InputPassword = ({
  handleChange,
  handleValidityChange = () => {},
  isRequired,
  t
}) => {
  const [passwordErrors, setPasswordErrors] = useState('')

  const onChange = ({ target: { value = '' } }) => {
    const isEmptyValid = !isRequired && value.trim() === ''
    const isValid = validateRequiredPassword(value) ||
    isEmptyValid
    handleValidityChange(isValid)

    if (!isValid) {
      setPasswordErrors(t(`components.createTasterAccount.validations.first.password.min`))
    } else {
      setPasswordErrors('')
    }

    return handleChange(value)
  }
  
  return (
    <Form.Item
      help={passwordErrors}
      validateStatus={passwordErrors ? 'error' : 'success'}
    >
      <Input
        type={`password`}
        placeholder={t('placeholders.password')}
        onChange={onChange}
      />
    </Form.Item>
  )
}

export default withTranslation()(InputPassword)
