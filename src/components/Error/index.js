import React from 'react'
import { Container, Icon, Text } from './styles'

/**
 *
 * @param {boolean} invisible set as true if you want the error occupy a given
 * space but keep it hidden from the user
 */

const Error = ({
  invisible,
  className,
  children,
  big = false,
  dangerous = false
}) => (
  <Container className={className}>
    <Icon invisible={invisible} type='exclamation-circle' />
    {dangerous ? (
      <Text
        invisible={invisible}
        big={big}
        dangerouslySetInnerHTML={{
          __html: children
        }}
      />
    ) : (
      <Text invisible={invisible} big={big}>
        {children}
      </Text>
    )}
  </Container>
)

export default Error
