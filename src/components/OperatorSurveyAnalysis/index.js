import React, { useMemo } from 'react'
import OperatorPage from '../OperatorPage'
import OperatorPageContent from '../OperatorPageContent'
import { Steps } from 'antd'
import { StepsContainer, Step } from './styles'
import Settings from './Settings'
import { validationSchema as settingsValidation } from './Settings/validationSchema'
import Filter from './Filter'
import { withRouter, Route, Switch, Redirect } from 'react-router-dom'
import SurveyAnalysisReport from '../../containers/SurveyAnalysisReport'
import AlertModal from '../AlertModal'
import { useTranslation } from 'react-i18next'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import getErrorMessage from '../../utils/getErrorMessage'
import { validationSchema } from './Filter/validationSchema'

const pages = ['settings', 'filter', 'report']

const OperatorSurveyAnalysis = ({
  loading,
  products,
  questions,
  settings,
  onSettingsSave,
  filterProducts,
  filterQuestions,
  onFilterSave,
  onCreateJob,
  match,
  history
}) => {
  const { t } = useTranslation()
  const [SETTINGS_PATH, FILTER_PATH, REPORT_PATH] = useMemo(
    () => pages.map(page => `${match.path}/${page}`),
    [match]
  )

  const handleSaveFilter = async (filterProducts, filterQuestions) => {
    try {
      if (filterProducts.length === 0) {
        displayErrorPopup(
          t('validation.surveyAnalysis.filterProducts.minRequired')
        )
        return
      }

      try {
        await validationSchema.validate(
          { filterQuestions },
          { context: { questions } }
        )
      } catch (ex) {
        displayErrorPopup(t('validation.surveyAnalysis.filterQuestions.error'))
        return
      }

      onFilterSave(filterProducts, filterQuestions)
      const jobGroup = await onCreateJob(
        settings,
        filterProducts,
        filterQuestions
      )
      history.push(`./report/${jobGroup}`)
    } catch (ex) {
      displayErrorPopup(getErrorMessage(ex, 'ANALYSIS_ERROR'))
    }
  }

  const parentUrl = match.url

  return (
    <OperatorPage>
      <OperatorPageContent>
        <Route
          path={`${match.path}/:page`}
          render={({ match }) => {
            const { page } = match.params
            const pageIndex = pages.indexOf(page)

            const handleChangePage = nextUrl => {
              if (pageIndex === 2) {
                AlertModal({
                  title: t('surveyAnalysis.exitReport.title'),
                  description: t('surveyAnalysis.exitReport.desc'),
                  okText: t('surveyAnalysis.exitReport.ok'),
                  handleOk: () => history.push(nextUrl),
                  handleCancel: () => {}
                })
              } else {
                history.push(nextUrl)
              }
            }

            return (
              <StepsContainer>
                <Steps size='small' current={pageIndex}>
                  <Step
                    title={t('surveyAnalysis.tabs.settings')}
                    onClick={() => handleChangePage(`${parentUrl}/settings`)}
                  />
                  <Step
                    title={t('surveyAnalysis.tabs.filter')}
                    onClick={() => handleChangePage(`${parentUrl}/filter`)}
                  />
                  <Step
                    title={t('surveyAnalysis.tabs.report')}
                    disabled={pageIndex === 2}
                  />
                </Steps>
              </StepsContainer>
            )
          }}
        />

        <Switch>
          <Route
            path={SETTINGS_PATH}
            render={() => (
              <Settings
                loading={loading}
                questions={questions}
                initSettings={settings}
                onSettingsSave={onSettingsSave}
              />
            )}
          />
          <Route
            path={FILTER_PATH}
            render={() => {
              if (!settingsValidation.isValidSync(settings)) {
                history.push(`${parentUrl}/settings`)
                return null
              }

              const question = questions.find(q => q.id === settings.question)
              const referenceQuestion = questions.find(
                q => q.id === settings.referenceQuestion
              )

              if (!question) {
                history.push(`${parentUrl}/settings`)
                return null
              }

              const selectedQuestions = [
                question,
                ...(referenceQuestion ? [referenceQuestion] : [])
              ]

              return (
                <Filter
                  products={products}
                  questions={selectedQuestions}
                  filterProducts={filterProducts}
                  filterQuestions={filterQuestions}
                  onBack={() => history.push('./settings')}
                  onFilterSave={handleSaveFilter}
                />
              )
            }}
          />
          <Route
            path={`${REPORT_PATH}/:jobGroup`}
            render={({ match }) => (
              <SurveyAnalysisReport jobGroup={match.params.jobGroup} />
            )}
          />
          <Redirect to={SETTINGS_PATH} />
        </Switch>
      </OperatorPageContent>
    </OperatorPage>
  )
}

export default withRouter(OperatorSurveyAnalysis)
