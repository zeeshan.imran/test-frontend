import * as Yup from 'yup'
import i18n from '../../../utils/internationalization/i18n'

const ANALYSIS_WITH_REFERENCE = ['pls', 'penalty']

export const shouldRequireReference = analysisTypes => {
  return ANALYSIS_WITH_REFERENCE.some(type => analysisTypes.includes(type))
}

export const validationSchema = Yup.object().shape({
  question: Yup.string(i18n.t('validation.surveyAnalysis.question.required'))
    .nullable()
    .required(i18n.t('validation.surveyAnalysis.question.required')),

  analysisTypes: Yup.array()
    .of(Yup.string())
    .min(1, i18n.t('validation.surveyAnalysis.analysisTypes.minRequired'))
    .required(i18n.t('validation.surveyAnalysis.analysisTypes.minRequired')),

  referenceQuestion: Yup.string().when(['analysisTypes'], {
    is: analysisTypes => shouldRequireReference(analysisTypes),

    then: Yup.string(
      i18n.t('validation.surveyAnalysis.referenceQuestion.required')
    )
      .nullable(true)
      .required(i18n.t('validation.surveyAnalysis.referenceQuestion.required')),

    otherwise: Yup.string().nullable()
  })
})
