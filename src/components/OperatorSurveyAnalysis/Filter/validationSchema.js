import * as Yup from 'yup'
import i18n from '../../../utils/internationalization/i18n'
import { STATS_FILTER_ACCEPTED_TYPES } from '../../../utils/Constants'

export const validationSchema = Yup.lazy(({ filterQuestions }) => {
  const filterQuestionShape = Object.assign(
    ...Object.keys(filterQuestions).map(id => ({
      [id]: Yup.mixed().when('$questions', {
        is: questions => {
          const question = questions.find(q => q.id === id)
          const mustValidate =
            question && STATS_FILTER_ACCEPTED_TYPES.includes(question.type)
          return mustValidate
        },
        then: Yup.array()
          .min(
            1,
            i18n.t('validation.surveyAnalysis.filterQuestions.minRequired')
          )
          .required(
            i18n.t('validation.surveyAnalysis.filterQuestions.minRequired')
          )
      })
    }))
  )

  return Yup.object().shape({
    filterQuestions: Yup.object().shape(filterQuestionShape)
  })
})
