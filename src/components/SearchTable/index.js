import React, { useState, useRef, useEffect } from 'react'
import { Icon as AntIcon, Input } from 'antd'
import Highlighter from 'react-highlight-words'
import { SearchButton, SearchContainer } from './style'

export const SearchTable = dataIndexOfTable => {
  const [searchText, setSearchText] = useState('')
  const [searchedColumn, setSearchedColumn] = useState('')
  const searchInput = useRef(null)
  useEffect(() => {
    if (searchInput && searchInput.current) {
      searchInput.current.focus()
    }
  }, [])
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm()
    setSearchText(selectedKeys[0])
    setSearchedColumn(dataIndex)
  }
  const handleReset = clearFilters => {
    clearFilters()
    setSearchText('')
  }

  const getColumnSearchProps = dataIndex => {
    return {
      filterDropdown: ({
        setSelectedKeys,
        selectedKeys,
        confirm,
        clearFilters
      }) => (
        <SearchContainer>
          <Input
            ref={searchInput}
            placeholder={`Search ${dataIndex}`}
            value={selectedKeys[0]}
            onChange={e =>
              setSelectedKeys(e.target.value ? [e.target.value] : [])
            }
            onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
            style={{ width: 188, marginBottom: 8, display: 'block' }}
          />

          <SearchButton
            type='primary'
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={'search'}
            size='small'
          >
            Search
          </SearchButton>
          <SearchButton onClick={() => handleReset(clearFilters)} size='small'>
            Reset
          </SearchButton>
        </SearchContainer>
      ),
      filterIcon: filtered => (
        <AntIcon
          type='search'
          style={{ color: filtered ? '#1890ff' : undefined }}
        />
      ),
      onFilter: (value, record) =>
        record[dataIndex]
          .toString()
          .toLowerCase()
          .includes(value.toLowerCase()),
      onFilterDropdownVisibleChange: visible => {
        if (visible && searchInput.current) {
          try {
            setTimeout(() => searchInput.current.select())
          } catch (error) {
            console.log(error)
          }
        }
      },
      render: text =>
        searchedColumn === dataIndex ? (
          <Highlighter
            highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
            searchWords={[searchText]}
            autoEscape
            textToHighlight={text.toString()}
          />
        ) : (
          text
        )
    }
  }

  return getColumnSearchProps(dataIndexOfTable)
}
