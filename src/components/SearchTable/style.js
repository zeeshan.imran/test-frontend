import styled from 'styled-components'
import { Button as AntButton } from 'antd'

export const SearchButton = styled(AntButton)`
  width: 90px;
  margin-right: 5px;
`
export const SearchContainer = styled.div`
  padding: 8px;
`
