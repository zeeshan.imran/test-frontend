import React from 'react'
import { Switch } from 'antd'
import { Container, TextContainer, Icon, Title, Description } from './styles'

const ToggableSetting = ({
  title,
  description,
  icon,
  handleChange,
  ...rest
}) => (
  <Container>
    <Icon>{icon}</Icon>
    <TextContainer>
      <Title>{title}</Title>
      <Description>{description}</Description>
    </TextContainer>
    <Switch onChange={handleChange} {...rest} />
  </Container>
)

export default ToggableSetting
