import React, { useState, useEffect, useRef } from 'react'
import { Row } from 'antd'
import Select from '../Select/index'
import ProductCard from '../ProductCard'
import useResponsive from '../../utils/useResponsive'
import { Col, Container } from './styles'
import { withTranslation } from 'react-i18next'

const SurveyChooseProduct = ({
  autoAdvanceEnable,
  brands,
  products,
  takenProducts,
  selectedProduct,
  onChange,
  t,
  savedRewards = [],
  prefix,
  suffix,
  showIncentives,
  reduceRewardInTasting,
  isTimerLocked=false
}) => {
  const refOnChange = useRef(onChange)
  const takenProductsSet = new Set(takenProducts)
  const [selectedBrand, setSelectedBrand] = useState('')
  const { mobile } = useResponsive()
  const filteredProducts = products.filter(
    product => product.brand === selectedBrand
  )

  useEffect(() => {
    if (!autoAdvanceEnable) {
      // PRODFW-87: Select product list is not displayed when user start the survey.
      setSelectedBrand('')
      refOnChange.current(null)
    }
  }, [autoAdvanceEnable, takenProducts.length])

  const multipleBrands = brands.length !== 1

  return (
    <React.Fragment>
      {multipleBrands && (
        <Row>
          <Col xs={{ span: 12 }} md={{ span: 12 }}>
            <Select
              options={brands}
              getOptionValue={brand => brand.id}
              getOptionName={brand => brand.name}
              value={selectedBrand}
              placeholder={t('placeholders.selectBrand')}
              onChange={brandId => {
                onChange(null)
                setSelectedBrand(brandId)
              }}
              size={`large`}
            />
          </Col>
        </Row>
      )}
      <Row gutter={16}>
        <Container>
          {(multipleBrands ? filteredProducts : products).map(
            (product, index) => {
              const reward =
                savedRewards.find(reward => reward.id === product.id) || {}

              return (
                <Col
                  mobile={mobile}
                  key={index}
                  xs={{ span: 12 }}
                  md={{ span: 8 }}
                  lg={{ span: 6 }}
                >
                  <ProductCard
                    withSelection={!takenProductsSet.has(product.id)}
                    locked={takenProductsSet.has(product.id)}
                    selected={selectedProduct === product.id}
                    name={product.name}
                    reward={
                      showIncentives &&
                      `${prefix}${
                        reward.reward || reward.reward === 0
                          ? reward.reward
                          : product.reward
                      }${suffix}`
                    }
                    image={product.photo}
                    onClick={() => onChange(product.id)}
                    selectionDisable={
                      isTimerLocked || ((product && product.selectionDisable) || false)
                    }
                    reduceRewardInTasting={reduceRewardInTasting}
                  />
                </Col>
              )
            }
          )}
        </Container>
      </Row>
    </React.Fragment>
  )
}

export default withTranslation()(SurveyChooseProduct)
