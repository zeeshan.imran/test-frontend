import React from 'react'
import OptionsList from './OptionsList'

const ChooseMatrixSkipOptions = ({
  conditions,
  columnsData,
  canAddRules,
  setCanAddRules,
  conditionalOptions,
  addNewConditions,
  updateCurrentConditions,
  id,
  clientGeneratedId,
  disabled
}) => {
  
  if (conditions && conditions.length) {
    return conditions.map((condition, index) => {
      const columnAnswers = []
      const rowsData = condition.options.filter(option => {
        return option.label && option.label.length
      })
      columnsData.forEach(column => {
        let treeAnswer = {
          title: column.label,
          value: column.value,
          key: column.value,
          children: []
        }
        rowsData.forEach(row => {
          treeAnswer['children'].push({
            title: row.label + ' - ' + column.label,
            value: row.id + '-' + column.value,
            key: row.id + '-' + column.value,
          })
        })
        columnAnswers.push(treeAnswer)
      })
      
      return (
        <OptionsList
          key={index}
          answers={columnAnswers}
          rowData={rowsData}
          canAddRules={canAddRules}
          setCanAddRules={setCanAddRules}
          skipOptions={condition.skipOptions}
          conditionalOptions={conditionalOptions}
          updateCurrentConditions={updateCurrentConditions}
          selectedQuestion={condition.skipTo}
          selectedOptions={condition.answers}
          currentIndex={index}
          selectedType={condition.type}
          isLastIndex={conditions.length - 1 === index}
          id={id}
          clientGeneratedId={clientGeneratedId}
          disabled={disabled}
        />
      )
    })
  } else {
    return null
  }
}

export default ChooseMatrixSkipOptions
