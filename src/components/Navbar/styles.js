import styled from 'styled-components'
import colors from '../../utils/Colors'
import { components, zIndexes } from '../../utils/Metrics'

export const NavbarContainer = styled.div`
  width: 100%;
  background-color: ${colors.WHITE};
  height: ${components.NAVBAR_HEIGHT}rem;
  position: fixed;
  z-index: ${zIndexes.NAVBAR};
  box-shadow: ${({ withoutShadow }) =>
    withoutShadow ? 'none' : '0 4px 16px 0 rgba(0, 0, 0, 0.14)'};
`

export const ContentContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: ${({ desktop }) =>
    desktop
      ? `0rem ${components.NAVBAR_DESKTOP_H_PADDING}rem`
      : `${components.NAVBAR_MOBILE_V_PADDING}rem ${components.NAVBAR_IMAGE_PADDING}rem`};
`

export const ContentContainerForGuest = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: ${({ desktop }) => (desktop ? 'space-between' : 'center')};
  align-items: center;
  padding: ${({ desktop }) =>
    desktop
      ? `${components.NAVBAR_DESKTOP_V_PADDING}rem ${components.NAVBAR_DESKTOP_H_PADDING}rem`
      : `${components.NAVBAR_MOBILE_V_PADDING}rem ${components.NAVBAR_MOBILE_H_PADDING}rem`};
`

export const Logo = styled.div`
  background: url(${props => props.src}) no-repeat center;
  background-size: contain;
  width: 18.5rem;
  height: 3.4rem;
  cursor: ${({ onClick }) => (onClick ? 'pointer' : 'auto')};

  @media all and (max-width: 991px){
    width : 11rem;
  }
`

export const SurveyListing = styled.a`
  margin-left: 30px;
`

export const DashboardContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0rem;  
`