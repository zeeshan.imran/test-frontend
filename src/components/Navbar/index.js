import React from 'react'
import history from '../../history'
import { Desktop } from '../Responsive'
import MyProfileDropdown from '../MyProfileDropdown'
import HamburguerMenu from './HamburguerMenu'
import {
  NavbarContainer,
  ContentContainer,
  Logo,
  ContentContainerForGuest,
  SurveyListing,
  DashboardContainer
} from './styles'
import { withTranslation } from 'react-i18next'
import { getImages } from '../../utils/getImages'
import { imagesCollection } from '../../assets/png'
const { desktopImage } = getImages(imagesCollection)

const Navbar = ({
  entriesData,
  getEntryName,
  onClickEntry,
  mergeNavbarToContent,
  setShowHamburger,
  showHamburger,
  user,
  showhelp=false,
  t
}) => {
  if (!user) {
    return (
      <Desktop>
        {desktop => (
          <NavbarContainer withoutShadow={mergeNavbarToContent}>
            <ContentContainerForGuest desktop={desktop}>
              <Logo onClick={() => history.push('/')} src={desktopImage} />
            </ContentContainerForGuest>
          </NavbarContainer>
        )}
      </Desktop>
    )
  }

  const dashboardNotAvailable = [
    '/create-account/taster/additional-information',
    '/create-account/taster/personal-information'
  ]
  const location = window.location
  const disableElement = dashboardNotAvailable.includes(location.pathname)


  return (
    <Desktop>
      {desktop => (
        <NavbarContainer withoutShadow={mergeNavbarToContent}>
          <ContentContainer desktop={desktop}>
            <DashboardContainer desktop={desktop}>
              <Logo
                onClick={() => (!disableElement ? history.push('/') : null)}
                src={desktopImage}
              />
              {!disableElement && (
                <SurveyListing onClick={() => history.push('/')}>
                  {t(`containers.page.operator.menu.dashboard`)}
                </SurveyListing>
              )}
            </DashboardContainer>
            {desktop ? (
              <MyProfileDropdown
                displayName={user.fullName || user.emailAddress}
                userId={user.id}
                hasOperatorAccount={
                  user.type === 'operator' || user.type === 'power-user'
                }
                showhelp={showhelp}
              />
            ) : (
              <HamburguerMenu
                displayName={user.fullName || user.emailAddress}
                entriesData={entriesData}
                getEntryName={getEntryName}
                onClickEntry={onClickEntry}
                setShowHamburger={setShowHamburger}
                showHamburger={showHamburger}
                showhelp={showhelp}
                hasOperatorAccount={
                  user.type === 'operator' || user.type === 'power-user'
                }
              />
            )}
          </ContentContainer>
        </NavbarContainer>
      )}
    </Desktop>
  )
}

export default withTranslation()(Navbar)
