import React, { Component } from 'react'
import { storiesOf } from '@storybook/react'
import HamburguerMenu from './'

const MENU_ENTRIES = [
  {
    title: 'Entry #1',
    route: 'route-1'
  },
  {
    title: 'Entry #2',
    route: 'route-2'
  },
  {
    title: 'Entry #3',
    route: 'route-3'
  },
  {
    title: 'Entry #4',
    route: 'route-4'
  }
]

class StateWrapper extends Component {
  state = {
    visible: false
  }

  render () {
    return this.props.children({
      toggleMenu: visible => this.setState({ visible: !this.state.visible }),
      visible: this.state.visible
    })
  }
}

storiesOf('HamburguerMenu', module).add('Story', () => (
  <StateWrapper>
    {({ toggleMenu, visible }) => {
      return (
        <HamburguerMenu
          visible={visible}
          handleClick={toggleMenu}
          entriesData={MENU_ENTRIES}
          getEntryName={entry => entry.title}
          onClickEntry={entry => console.log('clicked entry to', entry.route)}
        />
      )
    }}
  </StateWrapper>
))
