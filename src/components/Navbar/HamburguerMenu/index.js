import React, { useState } from 'react'
import HamburgerIcon from 'react-hamburger-menu'
import Menu from './Menu'
import WelcomeTasterModal from '../../WelcomeTasterModal'
import colors from '../../../utils/Colors'
import { IconContainer } from './styles'

const HamburguerMenu = ({
  setShowHamburger,
  showHamburger,
  entriesData,
  onClickEntry,
  getEntryName,
  displayName,
  showhelp = false,
  hasOperatorAccount
}) => {
  const [visible, setVisible] = useState(showhelp)
  if(!hasOperatorAccount && !entriesData.find(ed => ed.title === `Help`)) {
    entriesData.unshift({
      title: `Help`,
      action: () => setVisible(true)
    })
      
  }
  
  return (
    <React.Fragment>
      <IconContainer visible={showHamburger}>
        <HamburgerIcon
          isOpen={showHamburger}
          menuClicked={setShowHamburger}
          width={20}
          height={15}
          strokeWidth={2}
          color={showHamburger ? colors.WHITE : colors.BLACK}
          animationDuration={0.5}
        />
      </IconContainer>
      <Menu
        title={displayName}
        visible={showHamburger}
        entriesData={entriesData}
        getEntryName={getEntryName}
        onClickEntry={onClickEntry}
      />
      {visible && (
        <WelcomeTasterModal
          visible={visible}
          setVisible={setVisible}
          showhelp={showhelp}
        />
      )}
    </React.Fragment>
  )
}
export default HamburguerMenu
