import React from 'react'
import { mount, shallow } from 'enzyme'
import HamburguerMenu from '.'
import { logout } from '../../../utils/userAuthentication'
import history from '../../../history'
import HamburgerIcon from 'react-hamburger-menu'

jest.mock('../../../history')

describe('HamburguerMenu', () => {
  let testRender

  let onClickEntry
  let displayName
  let entriesData
  let getEntryName
  let setShowHamburger
  let showHamburger

  beforeEach(() => {
    onClickEntry = jest.fn()
    setShowHamburger = jest.fn()
    displayName = 'Test Display'
    entriesData = [
      { title: 'Home', route: '/' },
      { title: 'Sign Out', action: logout }
    ]
    showHamburger = true
    getEntryName = jest.fn()

    history.push.mockImplementation(jest.fn())
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render HamburguerMenu', () => {
    testRender = shallow(
      <HamburguerMenu
        entriesData={entriesData}
        getEntryName={getEntryName}
        onClickEntry={onClickEntry}
        displayName={displayName}
        setShowHamburger={setShowHamburger}
        showHamburger={showHamburger}
      />
    )
    expect(testRender).toMatchSnapshot()
  })

  test('should render HamburgerIcon of HamburguerMenu ', () => {
    testRender = mount(
      <HamburguerMenu
        entriesData={entriesData}
        getEntryName={getEntryName}
        onClickEntry={onClickEntry}
        displayName={displayName}
        showHamburger
        setShowHamburger={setShowHamburger}
      />
    )
    testRender.find(HamburgerIcon).prop('menuClicked')()
    expect(getEntryName).toHaveBeenCalled()
  })
})
