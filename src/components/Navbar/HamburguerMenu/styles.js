import styled from 'styled-components'
import { zIndexes } from '../../../utils/Metrics'

export const IconContainer = styled.div`
  cursor: pointer;
  position: relative;
  float: right;
  z-index: ${zIndexes.HAMBURGER_MENU};
`
