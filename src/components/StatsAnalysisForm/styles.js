import styled from 'styled-components'
import { Collapse, Row } from 'antd'
import colors from '../../utils/Colors'

export const BottomRow = styled(Row)`
  margin-top: 24px;
`

export const StatisticRow = styled(Row)`
  margin-bottom: 6px;
`

export const StyledCollapse = styled(Collapse)`
  margin: 24px -24px -24px -24px;
  border: none;
  font-size: 1.4rem;

  & > .ant-collapse-item {
    background: #f5faeb;
    border-bottom: none !important;
  }
`

export const Errors = styled.span`
  margin-left: 16px;
  line-height: 30px;
  color: ${colors.ERROR};
`

export const ParameterRow = styled(Row)`
  margin-left: 24px;
  margin-top: 4px;
`

export const RequiredTitle = styled.span`
  line-height: 24px;
`

export const SectionTitle = styled.span`
  margin-left: 16px;
`
