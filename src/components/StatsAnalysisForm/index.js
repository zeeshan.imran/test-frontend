import React, { useState } from 'react'
import {
  StyledCollapse,
  BottomRow,
  Errors,
  RequiredTitle,
  SectionTitle,
  ParameterRow,
  StatisticRow
} from './styles'
import { Button, Col, Checkbox, Collapse, Icon } from 'antd'
import { useTranslation } from 'react-i18next'
import Select from '../Select'
import Input from '../Input'
import Loader from '../Loader'

const PENALTY_LEVELS = [
  {
    level: 3,
    label: '3'
  },
  {
    level: 5,
    label: '5'
  },
  {
    level: 7,
    label: '7'
  },
  {
    level: 9,
    label: '9'
  }
]

const StatsAnalysisFormComponent = ({
  availableOptions = [],
  jobs = [],
  selectAnalysis,
  chartId,
  settingsError,
  questions,
  selectRequiredParameter,
  getStatistics,
  loading
}) => {
  const { t } = useTranslation()
  const [collapsed, setCollapsed] = useState(false)

  const requieredForm = {
    referenceQuestion: job => (
      <Col span={12}>
        <Select
          size='small'
          placeholder={t('charts.analysis.placeholders.referenceQuestion')}
          options={questions}
          getOptionName={el => el.prompt}
          getOptionValue={el => el.id}
          onChange={selection =>
            selectRequiredParameter(
              job.analysisType,
              selection,
              'referenceQuestion'
            )
          }
          value={job.referenceQuestion}
        />
      </Col>
    ),
    penaltyLevel: job => (
      <Col span={12}>
        <Select
          size='small'
          placeholder={t('charts.analysis.placeholders.referenceQuestion')}
          options={PENALTY_LEVELS}
          getOptionName={el => el.label}
          getOptionValue={el => el.level}
          onChange={selection =>
            selectRequiredParameter(job.analysisType, selection, 'penaltyLevel')
          }
          value={job.penaltyLevel}
        />
      </Col>
    ),
    percentageLevel: job => (
      <Col span={12}>
        <Input
          placeholder={t('charts.analysis.placeholders.penaltyLevel')}
          size='small'
          value={job.penaltyLevel}
          onChange={input =>
            selectRequiredParameter(
              job.analysisType,
              input.target.value,
              'penaltyLevel'
            )
          }
        />
      </Col>
    ),
    alphaLevel: job => (
      <Col span={12}>
        <Input
          type='number'
          placeholder={t('charts.analysis.placeholders.alphaLevel')}
          min={0.001}
          max={0.999}
          step={0.001}
          size='small'
          value={job.alphaLevel}
          onChange={input => {
            const value = input.target.value
            if (value < 0 || value >= 1 || value.length > 5) {
              return null
            }
            selectRequiredParameter(job.analysisType, value, 'alphaLevel')
          }}
        />
      </Col>
    )
  }

  return (
    <StyledCollapse
      activeKey={
        collapsed && !loading ? [`stats-analysis-form-${chartId}`] : []
      }
      expandIconPosition='right'
      onChange={() => setCollapsed(!collapsed)}
    >
      <Collapse.Panel
        header={
          <React.Fragment>
            {loading ? <Loader /> : <Icon type='setting' />}
            <SectionTitle>
              {loading
                ? t('charts.analysis.loading')
                : t('charts.analysis.title')}
            </SectionTitle>
          </React.Fragment>
        }
        key={`stats-analysis-form-${chartId}`}
      >
        <Col>
          {availableOptions &&
            availableOptions.map(option => {
              const currentJobIndex = jobs.findIndex(
                job => job.analysisType === option.type
              )
              const currentJob = jobs[currentJobIndex]
              return (
                <StatisticRow key={option.type}>
                  <Checkbox
                    checked={currentJobIndex > -1}
                    onChange={e => {
                      selectAnalysis(option)
                    }}
                  >
                    {t(`charts.analysis.options.${option.type}`)}
                  </Checkbox>
                  {currentJob &&
                    Object.keys(currentJob)
                      .filter(
                        key =>
                          option.requireParameters.includes(key) &&
                          requieredForm[key]
                      )
                      .map(key => (
                        <ParameterRow align='middle' key>
                          <Col span={8}>
                            <RequiredTitle>
                              {t(`charts.analysis.requiredParameters.${key}`)}
                            </RequiredTitle>
                          </Col>
                          {requieredForm[key](currentJob)}
                        </ParameterRow>
                      ))}
                </StatisticRow>
              )
            })}
          <BottomRow align='middle'>
            <Button
              type='primary'
              disabled={settingsError && settingsError.length}
              size='default'
              onClick={() => {
                getStatistics()
                setCollapsed(false)
              }}
            >
              {loading ? <Loader /> : t('charts.analysis.run')}
            </Button>
            <Errors>{settingsError}</Errors>
          </BottomRow>
        </Col>
      </Collapse.Panel>
    </StyledCollapse>
  )
}

export default StatsAnalysisFormComponent
