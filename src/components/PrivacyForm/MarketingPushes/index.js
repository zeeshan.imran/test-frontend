import React from 'react'
import ToggableSetting from '../../../components/ToggableSetting'
import PushesIcon from '../../../components/SvgIcons/Marketing'
import { useTranslation } from 'react-i18next';

const MarketingPushes = ({ onChange, defaultValue}) => {
  const { t } = useTranslation();
  return (
    <ToggableSetting
      handleChange={onChange}
      title={t('components.privacyForm.marketingPushes.title')} 
      description={t('components.privacyForm.marketingPushes.description')}
      icon={<PushesIcon />}
      defaultChecked={defaultValue}
    />
  )
}

export default MarketingPushes
