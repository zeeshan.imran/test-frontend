import React from 'react'
import { Statistic } from 'antd'
const { Countdown } = Statistic

const TimerCountdown = ({
  titleCountdown,
  timeCountDown,
  onFinishCountDown
}) => (
  <Countdown
    title={titleCountdown}
    value={timeCountDown}
    onFinish={onFinishCountDown}
  />
)

export default TimerCountdown
