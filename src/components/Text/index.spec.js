import React from 'react'
import { shallow } from 'enzyme'
import Text from '.'
import { StyledText } from './styles'

describe('Text', () => {
  let testRender
  let className
  let children
  let otherProps

  beforeEach(() => {
    className = 'row'
    children = <StyledText>Enter text</StyledText>
    otherProps = {}
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Text', () => {
    testRender = shallow(
      <Text className={className} otherProps={otherProps}>
        {children}
      </Text>
    )
    expect(testRender).toMatchSnapshot()
  })
})