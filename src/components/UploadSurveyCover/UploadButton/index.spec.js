import React from 'react'
import { mount, shallow } from 'enzyme'
import UploadButton from '.'

describe('UploadButton', () => {
  let testRender
  let loading
  let error
  let imageUrl
  let handleRemove
  let handleBeforeUpload

  beforeEach(() => {
    loading = false
    error = {}
    imageUrl = 'http://localhost:3000'
    handleRemove = jest.fn()
    handleBeforeUpload = jest.fn()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render UploadButton', () => {
    testRender = mount(
      <UploadButton
        loading={loading}
        error={error}
        imageUrl={imageUrl}
        handleRemove={handleRemove}
        handleBeforeUpload={handleBeforeUpload}
      />
    )

    expect(testRender.find(UploadButton)).toHaveLength(1)

    // expect(testRender).toMatchSnapshot()
  })

  //   test('should render Upload before', () => {
  //     testRender = mount(<UploadButton handleBeforeUpload={handleRemove} />)

  //     testRender.find(UploadButton).simulate('change')
  //     expect(handleRemove).not.toHaveBeenCalled()
  //   })

  //   test('should remove picture', () => {
  //     testRender = mount(
  //       <UploadButton
  //         loading={loading}
  //         error={null}
  //         url={url}
  //         handleRemove={handleRemove}
  //       />
  //     )

  //     act(() => {
  //       testRender.find(Upload).prop('onRemove')()
  //     })
  //     expect(handleRemove).toHaveBeenCalled()
  //   })

  //   test('should handle before upload', () => {
  //     testRender = mount(
  //       <UploadButton
  //         loading={loading}
  //         error={null}
  //         url={url}
  //         handleRemove={handleRemove}
  //       />
  //     )

  //     act(() => {
  //       testRender.find(UploadButton).prop('handleBeforeUpload')()
  //     })
  //     expect(handleRemove).toHaveBeenCalled()
  //   })
})
