import React from 'react'
import { Avatar, Icon, Upload, IconAligner, RemoveIconAligner } from './styles'
import { Desktop } from '../../Responsive'
import TooltipWrapper from '../../../components/TooltipWrapper'
import { useTranslation } from 'react-i18next'

const UploadButton = ({
  imageUrl='',
  loading,
  handleRemove,
  handleBeforeUpload,
  removeButton = false,
  ...rest
}) => {
  const { t } = useTranslation()
 
  return (
    <Desktop>
      {desktop => (
        <Upload
          {...rest}
          data-testid='upload-button'
          accept='.jpg,.jpeg,.png'
          desktop={desktop}
          name='avatar'
          showUploadList={false}
          listType='picture-card'
          className='avatar-uploader'
          onRemove={handleRemove}
          beforeUpload={handleBeforeUpload}
          customRequest={() => {}} // This fixes the default 404 POST on localhost, and since we will not be using the request of the component, we can leave this as an empty function
        >
          {imageUrl && !loading ? (
            <Avatar src={imageUrl} alt={'avatar'} />
          ) : (
            <IconAligner>
              <Icon type={loading ? 'loading' : 'plus'} />
            </IconAligner>
          )}
          {imageUrl && removeButton && (
            <RemoveIconAligner>
              <TooltipWrapper
                helperText={t('tooltips.deletePicture')}
                placement='right'
              >
                <Icon type='delete' onClick={(e) => {
                  e.stopPropagation()
                  handleRemove()
                }}/>
              </TooltipWrapper>
            </RemoveIconAligner>
          )}
        </Upload>
      )}
    </Desktop>
  )
}

export default UploadButton
