import React, { useMemo } from 'react'
import { Breadcrumb, Icon, Dropdown, Menu } from 'antd'
import { useTranslation } from 'react-i18next'
import { BreadcrumbContainer, AntButton } from './styles'

const BreadcrumbFolder = ({ folderPath, onClickBreadcrumb }) => {
  const { t } = useTranslation()

  const { currentFolder, middleFolders } = useMemo(() => {
    const folders = folderPath.pathId.map((pathId, index) => {
      return {
        id: pathId,
        name: folderPath.pathName[index]
      }
    })

    const currentFolder = folders.pop()

    return {
      currentFolder,
      middleFolders: folders
    }
  }, [folderPath])

  const menu = useMemo(() => {
    const onClick = e => {
      onClickBreadcrumb(e.key)
    }

    return (
      <Menu onClick={onClick}>
        {middleFolders.map(folder => {

          return (
            <Menu.Item key={folder.id}>
              <Icon type="folder" theme="filled" />
              {folder.name}
            </Menu.Item>
          )
        })}
      </Menu>
    )
  }, [middleFolders])

  return (
    <BreadcrumbContainer>
      <Breadcrumb separator={<Icon type="right" />}>
        <Breadcrumb.Item
          onClick={() => {
            onClickBreadcrumb()
          }}
        >
          {t('components.operatorSurveys.home')}
        </Breadcrumb.Item>

        {middleFolders.length > 1 && (
          <Breadcrumb.Item>
            <Dropdown overlay={menu}>
              <AntButton>
                <Icon type='ellipsis' />
              </AntButton>
            </Dropdown>
          </Breadcrumb.Item>
        )}

        {middleFolders.length === 1 && (
          <Breadcrumb.Item
            onClick={() => {
              onClickBreadcrumb(middleFolders[0].id)
            }}
          >
            {middleFolders[0].name}
          </Breadcrumb.Item>
        )}

        {currentFolder && (
          <Breadcrumb.Item
            onClick={() => {
              onClickBreadcrumb(currentFolder.id)
            }}
          >
            {currentFolder.name}
          </Breadcrumb.Item>
        )}
      </Breadcrumb>
    </BreadcrumbContainer>
  )
}

export default BreadcrumbFolder
