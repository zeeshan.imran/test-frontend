import styled from 'styled-components'
import { Button } from 'antd'

export const BreadcrumbContainer = styled.div`
  .ant-breadcrumb {
    font-size: 2rem;
  }
  .ant-breadcrumb-link {
    cursor: pointer;
  }
`

export const AntButton = styled(Button)`
  border-radius: 50%;
  padding: 0 8px;
`
