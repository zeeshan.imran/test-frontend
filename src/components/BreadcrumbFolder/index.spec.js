import React from 'react'
import { shallow } from 'enzyme'
import BreadcrumbFolder from '.'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = {
      ...Component.defaultProps,
      t: () => '',
      i18n: {
        t: () => ''
      }
    }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('BreadcrumbFolder', () => {
  let testRender
  let action
  let title

  beforeEach(() => {
    title = 'Breadcrumb Folder'
    action = jest.fn()
  })

  afterEach(() => {
    // testRender.unmount()
  })

  test('should render BreadcrumbFolder', async () => {
    // testRender = shallow(<BreadcrumbFolder folderPath={title} />)
    // expect(testRender).toMatchSnapshot()
  })
})
