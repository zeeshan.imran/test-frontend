import styled from 'styled-components'
import { family } from '../../utils/Fonts'

export const ActionsContainer = styled.div`
  display: flex;
  justify-content: flex-end;
`

export const QrCode = styled.img`
  width: 40px;
  height: 40px;
  background: #ddd;
  border-radius: 3px;
`

export const TitleWrapper = styled.div``

export const QrCodeName = styled.div`
  font-family: ${family.primaryBold};
  color: rgba(0, 0, 0, 0.65);
`

export const QrCodeDesc = styled.div`
  font-size: 1.4rem;
  font-family: ${family.primaryRegular};
  line-height: 2.2rem;
  color: rgba(0, 0, 0, 0.45);
  user-select: none;
  white-space: pre-wrap;
`
