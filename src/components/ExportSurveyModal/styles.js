import { Row as AntdRow } from 'antd'
import styled from 'styled-components'
import RadioGroup from 'antd/lib/radio/group'
import colors from '../../utils/Colors'

export const Row = styled(AntdRow)`
  margin-top: 1rem;
  .ant-checkbox-wrapper {
    margin-right: 16px;
    padding: 7px 0;
  }
  .ant-calendar-picker-input.ant-input {
    line-height: 1.8;
  }
  .ant-calendar-picker {
    width: 320px;
  }
`

export const NotFound = styled.div`
  padding: 2rem 1rem;
`
export const AntdRadio = styled(RadioGroup)`
  .ant-radio-inner::after {
    background-color: ${colors.RADIO_BUTTON_BACKGROUND_COLOR};
  }
`

export const DropdownWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  width: 100%;
`

export const DropdownDescription = styled.div`
  padding: 16px;
`
