import React, { useMemo, useState } from 'react'
import { useTranslation } from 'react-i18next'
import {
  Modal,
  Select,
  Radio,
  Col,
  Form,
  Alert,
  Checkbox,
  DatePicker
} from 'antd'
import * as Yup from 'yup'
import FieldLabel from '../FieldLabel'
import { getAuthenticatedUser } from '../../utils/userAuthentication'
import { Formik } from 'formik'
import {
  Row,
  NotFound,
  AntdRadio,
  DropdownWrapper,
  DropdownDescription
} from './styles'
import { StyledCheckbox } from '../StyledCheckBox'
import { exportReportList } from '../../utils/exportReportList'
import isFlavorwikiUser from '../../utils/isFlavorwikiUser'

const { REACT_APP_THEME } = process.env
const validationSchema = Yup.object().shape({
  includeScreener: Yup.boolean(),
  sendToMe: Yup.boolean().required(),
  userId: Yup.string()
    .typeError('Please select the user to send')
    .required('Please select the user to send'),
  includeCompulsorySurvey: Yup.boolean(),
  reportList: Yup.array()
    .of(Yup.string())
    .required('Please select at least one report')
})

const ExportSurveyModal = ({
  tKey,
  surveyName,
  users,
  loading,
  saving,
  onSearch,
  onOk,
  onCancel,
  showScreenerOption = false,
  isImpersonating = false,
  compulsorySurvey = false,
  includeCompulsorySurveyDataInStats = false
}) => {
  const user = getAuthenticatedUser()
  const [isDateFilterEnabled, setIsDateFilterEnabled] = useState(false)
  const [focusedId, setFocusedId] = useState(null)
  const { t } = useTranslation()

  const focusedText = useMemo(() => {
    if (focusedId) {
      return t(`tooltips.exportReportType.${focusedId}`)
    }

    return ''
  }, [t, focusedId])

  return (
    <Formik
      enableReinitialize
      isInitialValid
      validationSchema={validationSchema}
      initialValues={{
        userId: user.id,
        sendToMe: true,
        includeScreener: false,
        dateFilter: [],
        includeCompulsorySurvey: false,
        reportList: ['generalQuestionsReport']
      }}
      onSubmit={values => onOk(values)}
    >
      {({
        values,
        setTouched,
        touched,
        errors,
        setFieldValue,
        handleSubmit
      }) => (
        <Modal
          title={t(`containers.${tKey}.alertModalTitle`, { name: surveyName })}
          visible
          okText={t(`containers.${tKey}.alertModalOkText`)}
          confirmLoading={saving}
          cancelButtonProps={{
            hidden: saving
          }}
          onOk={handleSubmit}
          onCancel={onCancel}
          closable={!saving}
        >
          {t(`containers.${tKey}.alertModalDescription`)}
          <Row gutter={24}>
            <Col md={24} lg={24}>
              <AntdRadio
                value={values.sendToMe}
                onChange={e => {
                  const nextSendToMe = e.target.value
                  setFieldValue('sendToMe', nextSendToMe)
                  setFieldValue('userId', nextSendToMe ? user.id : null)
                }}
              >
                <Radio value>
                  Send it to me
                  {isImpersonating ? '(' + user.fullName + ')' : ''}
                </Radio>
                <Radio value={false}>Send to other</Radio>
              </AntdRadio>
            </Col>
          </Row>
          <Row>
            <Col>
              {!values.sendToMe && user.isSuperAdmin && (
                <Alert
                  type='warning'
                  message='As administrator, you can send the report to any operator, power-user of any organization.'
                />
              )}

              {!values.sendToMe && !user.isSuperAdmin && (
                <Alert
                  type='warning'
                  message='As operator or power-user, you can send the report to operator and power-user of your organization.'
                />
              )}
            </Col>
          </Row>
          <Row gutter={24}>
            <Col md={24} lg={24}>
              {!values.sendToMe && (
                <Form.Item
                  help={touched.userId && errors.userId}
                  validateStatus={
                    touched.userId && errors.userId ? 'error' : 'success'
                  }
                >
                  <FieldLabel label={t(`containers.${tKey}.selectUser`)}>
                    <Select
                      placeholder={t('placeholders.user')}
                      style={{ width: '100%' }}
                      showSearch
                      loading={loading}
                      value={values.userId}
                      defaultActiveFirstOption={false}
                      filterOption={false}
                      notFoundContent={
                        <NotFound>{t(`containers.${tKey}.notFound`)}</NotFound>
                      }
                      onSearch={keyword => {
                        onSearch(keyword)
                        setTouched({ userId: true })
                      }}
                      onChange={value => setFieldValue('userId', value)}
                      optionLabelProp='label'
                    >
                      {users.map(s => (
                        <Select.Option key={s.id} label={s.emailAddress}>
                          <div>{s.fullName}</div>
                          <div>{s.emailAddress}</div>
                        </Select.Option>
                      ))}
                    </Select>
                  </FieldLabel>
                </Form.Item>
              )}
            </Col>
          </Row>
          {showScreenerOption && (
            <Row gutter={24}>
              <Col md={24} lg={24}>
                <Checkbox
                  checked={values.includeScreener}
                  onChange={el => {
                    setFieldValue('includeScreener', el.target.checked)
                  }}
                >
                  Include Screener Data
                </Checkbox>
              </Col>
            </Row>
          )}
          {REACT_APP_THEME === 'default' ? (
            <Row>
              <StyledCheckbox
                name='isDateFilterEnabled'
                checked={isDateFilterEnabled}
                onChange={e => {
                  const { checked: value } = e.target
                  setIsDateFilterEnabled(value)
                }}
              >
                {/* TODO: replace with i18n */}
                {'Date filter'}
              </StyledCheckbox>
              {isDateFilterEnabled && (
                <DatePicker.RangePicker
                  value={values.dateFilter}
                  onChange={dateRange => setFieldValue('dateFilter', dateRange)}
                />
              )}
            </Row>
          ) : null}
          {!compulsorySurvey && includeCompulsorySurveyDataInStats && (
            <Row gutter={24}>
              <Col md={24} lg={24}>
                <Checkbox
                  checked={values.includeCompulsorySurvey}
                  onChange={el => {
                    setFieldValue('includeCompulsorySurvey', el.target.checked)
                  }}
                >
                  Include compulsory survey data
                </Checkbox>
              </Col>
            </Row>
          )}
          <Row gutter={24}>
            <Select
              mode='multiple'
              style={{ width: '90%' }}
              placeholder='Please select reports'
              defaultValue={['generalQuestionsReport']}
              dropdownMatchSelectWidth
              dropdownRender={(menuNode, props) => {
                return (
                  <DropdownWrapper>
                    <span>{menuNode}</span>

                    <DropdownDescription>{focusedText}</DropdownDescription>
                  </DropdownWrapper>
                )
              }}
              onChange={value => setFieldValue('reportList', value)}
            >
              {exportReportList(isImpersonating || isFlavorwikiUser(user)).map(
                ({ id, name }) => (
                  <Select.Option
                    key={id}
                    label={name}
                    onMouseOver={() => {
                      setFocusedId(id)
                    }}
                  >
                    <div>{name}</div>
                  </Select.Option>
                )
              )}
            </Select>
          </Row>
        </Modal>
      )}
    </Formik>
  )
}

export default ExportSurveyModal
