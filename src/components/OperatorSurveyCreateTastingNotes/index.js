import React from 'react'
import { useTranslation } from 'react-i18next'
import { Formik } from 'formik'
import { Container } from './styles'
import { Row, Col, Form, Input as MainInput, DatePicker } from 'antd'
import Input from '../Input'
import FieldLabel from '../FieldLabel'
import settingsSchema from '../../validates/settings'
import moment from 'moment'

const TastingNotes = ({
  tastingNotes,
  onFieldUpdate
}) => {
  const { t } = useTranslation()
  
  return (
    <Formik
      enableReinitialize
      validationSchema={settingsSchema}
      initialValues={tastingNotes}
    >
      {({ errors, values, setFieldValue }) => (
        <Container>
          <Row gutter={24}>
            <Col>
              <Form.Item>
                <Input
                  label={t('surveySettings.tastingNotes.tastingId')}
                  size='default'
                  name={`tastingId`}
                  value={values.tastingId}
                  onChange={e => {
                    setFieldValue('tastingId', e.target.value)
                    onFieldUpdate('tastingId', e.target.value)
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col>
              <Form.Item>
                <Input
                  label={t('surveySettings.tastingNotes.tastingLeader')}
                  size='default'
                  name={`tastingLeader`}
                  value={values.tastingLeader}
                  onChange={e => {
                    setFieldValue('tastingLeader', e.target.value)
                    onFieldUpdate('tastingLeader', e.target.value)
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col>
              <Form.Item>
                <Input
                  label={t('surveySettings.tastingNotes.customer')}
                  size='default'
                  name={`customer`}
                  value={values.customer}
                  onChange={e => {
                    setFieldValue('customer', e.target.value)
                    onFieldUpdate('customer', e.target.value)
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col>
              <Form.Item>
                <Input
                  label={t('surveySettings.tastingNotes.country')}
                  size='default'
                  name={`country`}
                  value={values.country}
                  onChange={e => {
                    setFieldValue('country', e.target.value)
                    onFieldUpdate('country', e.target.value)
                  }}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col>
              <Form.Item>
                <FieldLabel label={t('surveySettings.tastingNotes.dateOfTasting')}>
                  <DatePicker
                    size='default'
                    name={`dateOfTasting`}
                    value={values.dateOfTasting ? moment(values.dateOfTasting) : ''}
                    onChange={(date, dateString) => {
                      setFieldValue('dateOfTasting', date)
                      onFieldUpdate('dateOfTasting', dateString)
                    }}
                  />
                </FieldLabel>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col>
              <Form.Item>
                <FieldLabel label={t('surveySettings.tastingNotes.otherInfo')}>
                  <MainInput.TextArea
                    name={`otherInfo`}
                    value={values.otherInfo}
                    autoSize={{ minRows: 5, maxRows: 10 }}
                    onChange={e => {
                      setFieldValue('otherInfo', e.target.value)
                      onFieldUpdate('otherInfo', e.target.value)
                    }}
                  />
                </FieldLabel>
              </Form.Item>
            </Col>
          </Row>
        </Container>
      )}
    </Formik>
  )
}

export default TastingNotes
