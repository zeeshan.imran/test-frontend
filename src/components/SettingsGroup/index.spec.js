import React from 'react'
import { mount } from 'enzyme'
import SettingsGroup from '.'
import { Title } from './styles'
describe('SettingsGroup', () => {
  let testRender
  let title
  let children
  let className

  beforeEach(() => {
    title = 'Settings'
    children = '<container>Settings</container>'
    className = 'center'
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SettingsGroup', async () => {
    testRender = mount(
      <SettingsGroup title={title} children={children} className={className} />
    )
    expect(testRender.find(SettingsGroup)).toHaveLength(1)

    expect(
      testRender
        .find(Title)
        .first()
        .text()
    ).toBe(title)
  })
})
