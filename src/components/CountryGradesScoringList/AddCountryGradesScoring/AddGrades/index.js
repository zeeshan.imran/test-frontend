import React, { useRef, useEffect } from 'react'
import { type, isNil, isEmpty } from 'ramda'
import { Formik } from 'formik'
import { Icon, Form, Row, Col } from 'antd'
import { COUNTRY_PHONE_CODES } from '../../../../utils/Constants'
import Input from '../../../Input'
import Button from '../../../Button'
import Select from '../../../Select'
import validationsSchema from './validation'

const MIN_GRADES = 3

const AddGrades = ({
  t,
  handleAddGrade,
  grades = [],
  onChangeGrades,
  selectedCountry,
  setCountry,
  handleDisableButton,
  disableCountrySelection
}) => {
  const form = useRef()

  useEffect(() => {
    if (form.current) {
      form.current.getFormikActions().validateForm()
    }
  })

  if (grades.length < MIN_GRADES) {
    grades = Array.from(new Array(MIN_GRADES)).map((d, i) => ({
      grade: '',
      scoreRange: {
        start: null,
        end: null
      }
    }))
  }

  const canRemoveOption = grades.length > MIN_GRADES

  const intialFormValues = {
    grades,
    country: selectedCountry
  }

  const errorsCheck = errors => {
    if (isEmpty(errors)) {
      handleDisableButton(false)
    } else {
      handleDisableButton(true)
    }
  }

  return (
    <React.Fragment>
      <Formik
        enableReinitialize
        ref={form}
        validationSchema={validationsSchema}
        initialValues={intialFormValues}
        render={({ errors, setFieldValue, values }) => (
          <React.Fragment>
            {errorsCheck(errors)}
            <Form.Item
              help={
                errors && errors.country && type(errors.country) === 'String'
                  ? errors.country
                  : null
              }
              validateStatus={
                errors && errors.country && type(errors.country) === 'String'
                  ? 'error'
                  : 'success'
              }
            >
              <Select
                disabled={disableCountrySelection}
                showSearch
                value={selectedCountry}
                options={COUNTRY_PHONE_CODES}
                optionFilterProp='children'
                getOptionName={option => option.name}
                getOptionValue={option => option.code}
                onChange={value => setCountry(value)}
                placeholder={t(
                  `components.createTasterAccount.forms.second.country.placeholder`
                )}
              />
            </Form.Item>
            {(values.grades || []).map((grade, index) => {
              return (
                <Row gutter={24} key={index}>
                  <Col xs={10} lg={6}>
                    <Form.Item
                      help={
                        errors &&
                        errors.grades &&
                        type(errors.grades) === 'Array' &&
                        !isNil(errors.grades[index])
                          ? errors.grades[index].grade
                          : errors &&
                            errors.grades &&
                            type(errors.grades) === 'String'
                          ? errors.grades
                          : null
                      }
                      validateStatus={
                        (errors &&
                          errors.grades &&
                          errors.grades[index] &&
                          errors.grades[index].grade) ||
                        (errors &&
                          errors.grades &&
                          type(errors.grades) === 'String')
                          ? 'error'
                          : 'success'
                      }
                    >
                      <Input
                        required
                        name={`${index}-grade`}
                        size='default'
                        placeholder={t('components.countryGradesScoring.gradesModal.placeholder.grade')}
                        value={grade.grade}
                        onChange={event => {
                          const updatedGrades = [
                            ...grades.slice(0, index),
                            { ...grades[index], grade: event.target.value },
                            ...grades.slice(index + 1)
                          ]
                          setFieldValue('grades', updatedGrades)
                          onChangeGrades(updatedGrades, event.target.value)
                        }}
                      />
                    </Form.Item>
                  </Col>
                  <Col xs={10} lg={6}>
                    <Form.Item
                      help={
                        errors &&
                        errors.grades &&
                        type(errors.grades) === 'Array' &&
                        !isNil(
                          errors.grades[index] &&
                            errors.grades[index].scoreRange
                        )
                          ? errors.grades[index].scoreRange.start
                          : null
                      }
                      validateStatus={
                        (errors &&
                          errors.grades &&
                          errors.grades[index] &&
                          errors.grades[index].scoreRange &&
                          errors.grades[index].scoreRange.start) ||
                        (errors &&
                          errors.grades &&
                          type(errors.grades) === 'String')
                          ? 'error'
                          : 'success'
                      }
                    >
                      <Input
                        required
                        type={`number`}
                        name={`${index}-start`}
                        size='default'
                        placeholder={t('components.countryGradesScoring.gradesModal.placeholder.start')}
                        value={grade.scoreRange.start}
                        onChange={event => {
                          const updatedGrades = [
                            ...grades.slice(0, index),
                            {
                              ...grades[index],
                              scoreRange: {
                                ...grade.scoreRange,
                                start: event.target.value
                              }
                            },
                            ...grades.slice(index + 1)
                          ]
                          setFieldValue('grades', updatedGrades)
                          onChangeGrades(updatedGrades, event.target.value)
                        }}
                      />
                    </Form.Item>
                  </Col>
                  <Col xs={10} lg={6}>
                    <Form.Item
                      help={
                        errors &&
                        errors.grades &&
                        type(errors.grades) === 'Array' &&
                        !isNil(
                          errors.grades[index] &&
                            errors.grades[index].scoreRange
                        )
                          ? errors.grades[index].scoreRange.end
                          : null
                      }
                      validateStatus={
                        (errors &&
                          errors.grades &&
                          errors.grades[index] &&
                          errors.grades[index].scoreRange &&
                          errors.grades[index].scoreRange.end) ||
                        (errors &&
                          errors.grades &&
                          type(errors.grades) === 'String')
                          ? 'error'
                          : 'success'
                      }
                    >
                      <Input
                        required
                        type={`number`}
                        name={`${index}-end`}
                        size='default'
                        placeholder={t('components.countryGradesScoring.gradesModal.placeholder.end')}
                        value={grade.scoreRange.end}
                        onChange={event => {
                          const updatedGrades = [
                            ...grades.slice(0, index),
                            {
                              ...grades[index],
                              scoreRange: {
                                ...grade.scoreRange,
                                end: event.target.value
                              }
                            },
                            ...grades.slice(index + 1)
                          ]
                          setFieldValue('grades', updatedGrades)
                          onChangeGrades(updatedGrades, event.target.value)
                        }}
                      />
                    </Form.Item>
                  </Col>
                  {canRemoveOption && (
                    <Col xs={5} lg={4} flex='auto'>
                      <Button
                        size='default'
                        type='red'
                        onClick={() => {
                          onChangeGrades([
                            ...grades.slice(0, index),
                            ...grades.slice(index + 1)
                          ])
                        }}
                      >
                        <Icon type='close' />
                      </Button>
                    </Col>
                  )}
                </Row>
              )
            })}
          </React.Fragment>
        )}
      />
      <Button
        onClick={() => handleAddGrade(grades)}
        type='primary'
        style={{
          marginTop: 16
        }}
      >
        {t('components.countryGradesScoring.gradesModal.addgradeButton')}
      </Button>
    </React.Fragment>
  )
}

export default AddGrades
