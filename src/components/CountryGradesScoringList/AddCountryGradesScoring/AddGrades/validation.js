import * as Yup from 'yup'
import { uniqBy, prop, includes, flatten } from 'ramda'
import i18n from '../../../../utils/internationalization/i18n'

const emptyStringToNull = (value, originalValue) => {
  if (typeof originalValue === 'string' && originalValue === '') {
    return null
  }
  return value
}

const gradingOverlapingCheck = grades =>
  grades &&
  grades.length &&
  grades.map((g1, indexG1) =>
    grades.map((g2, indexG2) => {
      if (indexG1 !== indexG2) {
        return (
          (Number(g2.scoreRange.start) >= Number(g1.scoreRange.start) &&
            Number(g2.scoreRange.start) <= Number(g1.scoreRange.end)) ||
          (Number(g2.scoreRange.end) >= Number(g1.scoreRange.start) &&
            Number(g2.scoreRange.end) <= Number(g1.scoreRange.end)) ||
          (Number(g1.scoreRange.start) >= Number(g2.scoreRange.start) &&
            Number(g1.scoreRange.start) <= Number(g2.scoreRange.end)) ||
          (Number(g1.scoreRange.end) >= Number(g2.scoreRange.start) &&
            Number(g1.scoreRange.end) <= Number(g2.scoreRange.end))
        )
      } else {
        return false
      }
    })
  )

export default Yup.object().shape({
  country: Yup.string().required(
    i18n.t('components.countryGradesScoring.gradesModal.validations.required')
  ),
  grades: Yup.array()
    .of(
      Yup.object().shape({
        grade: Yup.string()
          .test(
            'not-empty',
            i18n.t(
              'components.countryGradesScoring.gradesModal.validations.withoutSpace'
            ),
            value => /^(?! ).*/.test(value)
          )
          .test(
            'not-empty',
            i18n.t(
              'components.countryGradesScoring.gradesModal.validations.required'
            ),
            value => !/^[ ]+$/.test(value)
          )
          .required(
            i18n.t(
              'components.countryGradesScoring.gradesModal.validations.required'
            )
          ),
        scoreRange: Yup.object().shape({
          start: Yup.number()
            .test(
              'not-empty',
              i18n.t(
                'components.countryGradesScoring.gradesModal.validations.withoutSpace'
              ),
              value => /^(?! ).*/.test(value)
            )
            .test(
              'not-empty',
              i18n.t(
                'components.countryGradesScoring.gradesModal.validations.required'
              ),
              value => !/^[ ]+$/.test(value)
            )
            .test(
              'is-positive',
              i18n.t(
                'components.countryGradesScoring.gradesModal.validations.positiveValue'
              ),
              value => parseInt(value, 10) > 0
            )
            .required(
              i18n.t(
                'components.countryGradesScoring.gradesModal.validations.required'
              )
            )
            .transform(emptyStringToNull)
            .nullable(),
          end: Yup.number()
            .test(
              'not-empty',
              i18n.t(
                'components.countryGradesScoring.gradesModal.validations.withoutSpace'
              ),
              value => /^(?! ).*/.test(value)
            )
            .test(
              'not-empty',
              i18n.t(
                'components.countryGradesScoring.gradesModal.validations.required'
              ),
              value => !/^[ ]+$/.test(value)
            )
            .test(
              'is-positive',
              i18n.t(
                'components.countryGradesScoring.gradesModal.validations.positiveValue'
              ),
              value => parseInt(value, 10) > 0
            )
            .moreThan(
              Yup.ref('start'),
              i18n.t(
                'components.countryGradesScoring.gradesModal.validations.endValueGreater'
              )
            )
            .required(
              i18n.t(
                'components.countryGradesScoring.gradesModal.validations.required'
              )
            )
            .transform(emptyStringToNull)
            .nullable()
        })
      })
    )
    .test(
      'uniq-options',
      i18n.t(
        'components.countryGradesScoring.gradesModal.validations.uniqueGrades'
      ),
      value => {
        if (!value) return false
        if (uniqBy(prop('grade'), value).length !== value.length) {
          return false
        }
        return true
      }
    )
    .test(
      'uniq-options',
      i18n.t(
        'components.countryGradesScoring.gradesModal.validations.gradesOverlap'
      ),
      value => {
        if (!value) return false

        const overlap = gradingOverlapingCheck(value)
        if (includes(true, flatten(overlap))) return false

        return true
      }
    )
})
