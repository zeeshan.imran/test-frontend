import React, { useState, useEffect } from 'react'
import { Modal } from 'antd'
import AddGrades from './AddGrades'

const AddCountryGradesScoring = ({
  modalVisible,
  closeModal,
  addType,
  t,
  addGrading,
  editGrading,
  requestloading
}) => {
  const [selectedCountry, setSelectedCountry] = useState('')
  const [grades, setGrades] = useState([])
  const [updateGradingId, setUpdateGradingId] = useState(null)
  const [disableButton, setDisableButton] = useState(false)

  useEffect(() => {
    if (addType && addType.id && addType !== 'add') {
      const { id, country, grades } = addType
      setUpdateGradingId(id)
      setSelectedCountry(country)
      setGrades(grades)
    }
  }, [addType])

  const handleAddGrade = oldGrades => {
    const newGrade = {
      grade: '',
      scoreRange: {
        start: null,
        end: null
      }
    }
    setGrades([...oldGrades, newGrade])
  }

  const onChangeGrades = grades => setGrades(grades)

  const addGradesForCountry = () => {
    if (selectedCountry && grades.length) {
      addGrading(selectedCountry, grades)
    }
  }
  const editGradesForCountry = () => {
    if (updateGradingId) {
      editGrading(updateGradingId, selectedCountry, grades)
    }
  }

  const setCountry = value => setSelectedCountry(value)
  const handleDisableButton = value => setDisableButton(value)
  return (
    <Modal
      title={
        addType === 'add'
          ? t('components.countryGradesScoring.gradesModal.addGrades')
          : t('components.countryGradesScoring.gradesModal.updateGrades')
      }
      visible={modalVisible}
      okButtonProps={{ disabled: disableButton, loading: requestloading }}
      onOk={() => {
        if (addType === 'add') {
          addGradesForCountry()
        } else {
          editGradesForCountry()
        }
      }}
      onCancel={() => closeModal(false)}
    >
      <AddGrades
        t={t}
        handleAddGrade={handleAddGrade}
        grades={grades}
        onChangeGrades={onChangeGrades}
        selectedCountry={selectedCountry}
        setCountry={setCountry}
        disableCountrySelection={addType && addType.id ? true : false}
        handleDisableButton={handleDisableButton}
      />
    </Modal>
  )
}

export default AddCountryGradesScoring
