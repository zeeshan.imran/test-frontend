import React from 'react'
import { Table as AntTable } from 'antd'
import { Columns } from './columns'

const Table = ({ loading, countryList, editCountryGradesScoring, t }) => (
  <AntTable
    loading={loading}
    key='countryGradesScoringTable'
    rowKey={record => record.id}
    columns={Columns(t, editCountryGradesScoring)}
    dataSource={countryList}
    tableLayout='fixed'
  />
)

export default Table
