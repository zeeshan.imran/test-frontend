import React from 'react'
import { COUNTRY_PHONE_CODES } from '../../../utils/Constants'
import { ColumnContentAligner, Action } from './style'

export const Columns = (t, editCountryGradesScoring) => [
  {
    title: t(
      'components.countryGradesScoring.countryListTable.columns.country'
    ),
    dataIndex: 'country',
    key: 'country',
    align: 'left',
    render: country => {
      const { name } = COUNTRY_PHONE_CODES.find(obj => obj.code === country)
      return name
    }
  },
  {
    title: t(
      'components.countryGradesScoring.countryListTable.columns.createdBy'
    ),
    dataIndex: 'createdBy',
    key: 'createdBy',
    align: 'left',
    render: createdBy => createdBy.fullName
  },
  {
    title: t('components.countryGradesScoring.countryListTable.columns.action'),
    dataIndex: '',
    render: (_, rowData) => (
      <ColumnContentAligner>
        <Action onClick={() => editCountryGradesScoring(rowData)}>
          {t('components.countryGradesScoring.countryListTable.columns.edit')}
        </Action>
      </ColumnContentAligner>
    )
  }
]
