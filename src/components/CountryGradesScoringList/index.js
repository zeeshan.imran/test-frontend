import React from 'react'
import Button from '../Button'
import { Container, HeaderContainer } from './style'
import Table from './Table'
import AddCountryGradesScoring from './AddCountryGradesScoring'

const CountryGradesScoringComponent = ({
  t,
  loading,
  countryGradesScoringList,
  addCountryGradesScoring,
  editCountryGradesScoring,
  scoringModal,
  closeScoringModal,
  scoringEditionType,
  addGrading,
  editGrading,
  requestloading
}) => {
  return (
    <Container>
      <HeaderContainer>
        <Button onClick={() => addCountryGradesScoring()}>
          {t('components.countryGradesScoring.addButton')}
        </Button>
      </HeaderContainer>
      <Table
        loading={loading}
        countryList={countryGradesScoringList}
        editCountryGradesScoring={editCountryGradesScoring}
        t={t}
      />
      {scoringModal && (
        <AddCountryGradesScoring
          modalVisible={scoringModal}
          closeModal={closeScoringModal}
          addType={scoringEditionType}
          t={t}
          addGrading={addGrading}
          editGrading={editGrading}
          requestloading={requestloading}
        />
      )}
    </Container>
  )
}

export default CountryGradesScoringComponent
