import React from 'react'
import Loader from '../Loader'
import { Filler } from './styles'

const FillerLoader = ({ loading, fullScreen, children }) => (
  <Filler fullScreen={fullScreen}>
    <Loader loading={loading} children={children} />
  </Filler>
)

export default FillerLoader
