import React from 'react'
import { StyledForm } from './styles.js'

const OnboardingForm = ({ children, ...formProps }) => (
  <StyledForm {...formProps}>{children}</StyledForm>
)

export default OnboardingForm
