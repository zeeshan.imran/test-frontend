import { useQuery } from 'react-apollo-hooks'
import { path } from 'ramda'
import gql from 'graphql-tag'

const useAllOrganizations = () => {
  const organizationsListResult = useQuery(GET_ORGANIZATIONS_LIST, {
    variables: { input: { limit: 0 } },
    fetchPolicy: 'cache-and-network'
  })
  const organizationsList =
    path(['data', 'organizations'])(organizationsListResult) || []

  return {
    loading: organizationsList.loading,
    organizations: organizationsList
  }
}

const GET_ORGANIZATIONS_LIST = gql`
  query organizations($input: OrganizationsInput!) {
    organizations(input: $input) {
      id
      name
      members(types: ["operator", "power-user", "analytics"]) {
        id
        fullName
        emailAddress
      }
    }
  }
`

export default useAllOrganizations
