import { path, pipe, groupBy, values, map, reduce } from 'ramda'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import { useMemo } from 'react'

const getChartSettings = path(['getChartsSettings', 'chartsSettings'])
const getOperatorLayout = path(['getChartsSettings', 'operatorPdfLayout'])
const getTasterLayout = path(['getChartsSettings', 'tasterPdfLayout'])

const mergeChartResults = pipe(
  groupBy(path(['result', 'question'])),
  values,
  map(questionResults =>
    reduce(
      (acc, result) => ({
        ...acc,
        ...result,
        result: {
          ...acc.result,
          ...result.result,
          statistic: {
            ...acc.result.statistic,
            ...result.result.statistic
          }
        }
      }),
      { result: {} }
    )(questionResults)
  )
)

const useChartResults = ({ surveyId, jobGroupId }) => {
  const { data, loading, refetch: reloadChartResults } = useQuery(
    QUERY_STATS_RESULTS,
    {
      variables: {
        surveyId,
        jobGroupId
      }
    }
  )

  return {
    loading,
    reloadChartResults,
    survey: useMemo(() => data.survey, [data]),
    operatorLayout: useMemo(() => getOperatorLayout(data), [data]),
    tasterLayout: useMemo(() => getTasterLayout(data), [data]),
    settings: useMemo(() => getChartSettings(data) || {}, [data]),
    stats: useMemo(() => {
      return mergeChartResults(data.getStatsResults || [])
    }, [data])
  }
}

const QUERY_STATS_RESULTS = gql`
  query getStatsResults($surveyId: ID!, $jobGroupId: ID!) {
    survey(id: $surveyId) {
      id
      name
      pdfFooterSettings {
        active
        footerNote
      }
    }
    getChartsSettings(surveyId: $surveyId)
    getStatsResults(jobGroupId: $jobGroupId)
  }
`

export default useChartResults
