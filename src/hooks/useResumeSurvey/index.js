import { useMutation, useQuery } from 'react-apollo-hooks'
import path from 'path'
import gql from 'graphql-tag'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'

const useResumeSurvey = handleRedirect => {
  const advanceInSurveyMutation = useMutation(ADVANCE_IN_SURVEY)
  const {
    data: { currentSurveyParticipation = {} }
  } = useQuery(SURVEY_PARTICIPATION_QUERY)

  const { lastAnsweredQuestion } = currentSurveyParticipation

  const resumeSurvey = async ({ surveyId, lastAnsweredQuestionFromLogin }) => {
    if (!lastAnsweredQuestionFromLogin) {
      lastAnsweredQuestionFromLogin = lastAnsweredQuestion
    }
    const {
      data: { advanceInSurvey = {} }
    } = await advanceInSurveyMutation({
      variables: {
        fromStep:
          lastAnsweredQuestionFromLogin && lastAnsweredQuestionFromLogin.id,
          resumeSurvey: true
      }
    })

    const { currentSurveyStep, currentSurveySection } = advanceInSurvey

    handleRedirect(
      path.join(
        `/survey/${surveyId}`,
        currentSurveySection || '',
        currentSurveyStep || ''
      )
    )
  }

  return resumeSurvey
}

const ADVANCE_IN_SURVEY = gql`
  mutation advanceInSurvey($fromStep: ID!, $resumeSurvey: Boolean) {
    advanceInSurvey(fromStep: $fromStep, resumeSurvey: $resumeSurvey) @client
  }
`

export default useResumeSurvey
