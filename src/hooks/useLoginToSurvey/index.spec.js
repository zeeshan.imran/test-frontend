import React from 'react'
import { mount } from 'enzyme'
import { ApolloProvider } from 'react-apollo-hooks'
import { LOGIN_TO_SURVEY } from '../../mutations/loginToSurvey'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import useLoginToSurvey from '.'
import wait from '../../utils/testUtils/waait'
import { detect } from 'detect-browser'

jest.mock('../../utils/surveyAuthentication')
jest.mock('i18next', () => ({
  use: () => ({ init: () => { } }),
  init: () => { },
  t: k => k,
  getFixedT: () => () => ''
}))
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

const TestHook = ({ callback }) => {
  callback()
  return null
}

describe('option to have only 1 product without the choose product page', () => {
  let testRender
  let loginToSurveyMock
  beforeEach(() => {
    loginToSurveyMock = {
      id: 'surveyEnrollmentId',
      paypalEmail: 'flawor-wiki@test.com',
      state: 'completed',
      lastAnsweredQuestion: {
        id: 'question-1'
      },
      selectedProducts: [
        {
          id: 'product-1',
          name: 'product name'
        }
      ],
      savedRewards: [],
      answers: []
    }
  })
  afterEach(() => {
    testRender.unmount()
  })

  const createApolloClient = result =>
    createApolloMockClient({
      mocks: [
        {
          request: {
            query: LOGIN_TO_SURVEY,
            variables: {
              email: 'flawor-wiki@test.com',
              survey: 'survey-1',
              referral: 'refrall',
              isUserLoggedIn: true,
              password: '',
              country: '',
              browserInfo: detect()
            }
          },
          result
        }
      ]
    })

  test('should test loging survey with success', async () => {
    const returnData = jest.fn(() => ({
      data: {
        loginToSurvey: loginToSurveyMock
      }
    }))
    const client = createApolloClient(returnData)

    testRender = mount(
      <ApolloProvider client={client}>
        <TestHook
          callback={async () => {
            const loginToSurvey = useLoginToSurvey()
            await loginToSurvey({
              surveyId: 'survey-1',
              email: 'flawor-wiki@test.com',
              referral: 'refrall',
              isLoggedIn: true,
              password: '',
              country: '',
              browserInfo: detect()
            })
          }}
        />
      </ApolloProvider>
    )
    await wait(0)
    expect(returnData).toHaveBeenCalled()
  })

  test('should test loging survey with error', async () => {
    const client = createApolloClient({
      data: {},
      errors: [{ message: 'Error' }]
    })

    const handleError = jest.fn()

    testRender = mount(
      <ApolloProvider client={client}>
        <TestHook
          callback={async () => {
            const loginToSurvey = useLoginToSurvey()
            try {
              await loginToSurvey({
                surveyId: 'survey-1',
                email: 'flawor-wiki@test.com',
                referral: 'refrall',
                isLoggedIn: true,
                password: '',
                country: '',
                browserInfo: detect()
              })
            } catch (e) {
              handleError(e)
            }
          }}
        />
      </ApolloProvider>
    )

    await wait(100)

    expect(handleError).toHaveBeenCalled()
  })
})
