import { useCallback } from 'react'
import { useMutation } from 'react-apollo-hooks'
import { LOGIN_TO_SURVEY } from '../../mutations/loginToSurvey'
import gql from 'graphql-tag'
import { setAuthenticationToken } from '../../utils/surveyAuthentication'
import { detect } from 'detect-browser'

const useLoginToSurvey = () => {
  const handleLoginToSurvey = useMutation(LOGIN_TO_SURVEY)
  const saveCurrentSurveyParticipation = useMutation(SAVE_SURVEY_PARTICIPATION)

  return useCallback(
    async ({ surveyId, email, referral, extsource, extid, isLoggedIn, password, country }) => {
      try {
        const {
          data: {
            loginToSurvey: {
              id: surveyEnrollmentId,
              lastAnsweredQuestion,
              state: enrollmentState,
              selectedProducts,
              paypalEmail,
              savedRewards = [],
              savedQuestions = [],
              answers,
              lastSelectedProduct,
              productDisplayOrder,
              productDisplayType,
              productRewardsRule={},
            } = {}
          } = {}
        } = await handleLoginToSurvey({
          variables: {
            email,
            survey: surveyId,
            ...(referral && { referral }),
            ...(extsource && { extsource }),
            ...(extid && { extid }),
            password,
            country,
            isUserLoggedIn: isLoggedIn,
            browserInfo: detect()
          },
          returnPartialData: true
        })
      
        if (answers && answers.length) {
          answers.map(answer => ({
            ...answer,
            values: answer.value,
            __typename: 'LocalAnswer'
          }))
        }

        await saveCurrentSurveyParticipation({
          variables: {
            surveyId,
            lastAnsweredQuestion,
            state: enrollmentState,
            selectedProducts: selectedProducts ? selectedProducts.map(product => product.id) : [],
            surveyEnrollmentId,
            paypalEmail,
            savedRewards,
            savedQuestions,
            answers: answers || [],
            email: email || '',
            selectedProduct: lastSelectedProduct ? lastSelectedProduct : null,
            productDisplayOrder,
            productDisplayType,
            productRewardsRule
          }
        })

        setAuthenticationToken(surveyEnrollmentId)

        return {
          surveyEnrollmentId,
          lastSelectedProduct,
          lastAnsweredQuestion,
          state: enrollmentState,
          productDisplayOrder,
          productDisplayType
        }
      } catch (e) {
        throw e
      }
    },
    [saveCurrentSurveyParticipation, handleLoginToSurvey]
  )
}

export const SAVE_SURVEY_PARTICIPATION = gql`
  mutation saveCurrentSurveyParticipation(
    $surveyId: String
    $lastAnsweredQuestion: String
    $state: String
    $selectedProducts: [String]
    $selectedProduct: String
    $surveyEnrollmentId: String
    $paypalEmail: String
    $savedRewards: JSON
    $savedQuestions: JSON
    $answers: [LocalAnswer]
    $email: String
    $productDisplayOrder: [String]
    $productDisplayType: String
    $productRewardsRule: JSON
  ) {
    saveCurrentSurveyParticipation(
      surveyId: $surveyId
      lastAnsweredQuestion: $lastAnsweredQuestion
      state: $state
      selectedProducts: $selectedProducts
      selectedProduct: $selectedProduct
      surveyEnrollmentId: $surveyEnrollmentId
      paypalEmail: $paypalEmail
      answers: $answers
      savedRewards: $savedRewards
      savedQuestions: $savedQuestions
      email: $email
      productDisplayOrder: $productDisplayOrder
      productDisplayType: $productDisplayType
      productRewardsRule: $productRewardsRule
    ) @client
  }
`

export default useLoginToSurvey
