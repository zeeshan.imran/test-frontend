import gql from 'graphql-tag'
import { path } from 'ramda'
import { useQuery } from 'react-apollo-hooks'
import { useMemo } from 'react'

const useLikingQuestionOptions = () => {
  const { data } = useQuery(SURVEY_CREATION)

  return useMemo(() => {
    const questions = path(['surveyCreation', 'questions'], data) || []
    return [
      ...questions
        .map((question, index) => ({
          value: question.id || question.clientGeneratedId,
          label: `${index + 1}. ${question.prompt || ''}`,
          type: question.type,
          displayOn: question.displayOn
        }))
        .filter(q => q.type === 'vertical-rating' && q.displayOn === 'middle')
    ]
  }, [data])
}

export const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      questions
    }
  }
`

export default useLikingQuestionOptions
