import { map, prop } from 'ramda'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import { useMemo } from 'react'
const { REACT_APP_THEME } = process.env

const sortChartResults = (stats, data) => {
  if (
    data &&
    data.length &&
    stats &&
    stats.length &&
    REACT_APP_THEME === 'chrHansen'
  ) {
    const sortedIds = map(prop('id'))(data)
    let results = []
    sortedIds.forEach(item => {
      const object = stats.filter(items => {
        return items.result.question_id === item
      })

      results = [...results, ...object]
    })
    return results
  }
  return stats
}
const useSortChartResults = ({ surveyId, stats }) => {
  const {
    data: { getQuestionIdWithOrder: sortedData }
  } = useQuery(QUESTION_ORDER, {
    variables: {
      surveyId
    }
  })

  return {
    sortedStats: useMemo(() => {
      return sortChartResults(stats || [], sortedData)
    }, [stats, sortedData])
  }
}
const QUESTION_ORDER = gql`
  query getQuestionIdWithOrder($surveyId: ID!) {
    getQuestionIdWithOrder(surveyId: $surveyId) {
      id
    }
  }
`
export default useSortChartResults
