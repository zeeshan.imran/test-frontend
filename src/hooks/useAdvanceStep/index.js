import { useRef, useCallback, useMemo } from 'react'
import { uniq } from 'ramda'
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo-hooks'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import getAnswerLocalId from '../../utils/getAnswerLocalId'
import findSkipRule from '../../utils/findSkipRule'
import { questionInfo } from '../../fragments/survey'
const questionType = [
  'choose-product',
  'choose-one',
  'dropdown',
  'select-and-justify',
  'vertical-rating',
  'choose-multiple',
  'matrix'
]
const getSkipRuleAnswer = participationAnswer => {
  const answer =
    participationAnswer &&
    participationAnswer.values &&
    participationAnswer.values[0]
  return (answer && answer.values) || answer
}
const useAdvanceStep = inputStepId => {
  const {
    data: { currentSurveyParticipation }
  } = useQuery(SURVEY_PARTICIPATION_QUERY)
  const handleAdvance = useRef(useMutation(ADVANCE_IN_SURVEY))
  const setSelectedProducts = useMutation(SET_SELECTED_PRODUCTS)
  const handleSubmitAnswer = useRef(useMutation(SUBMIT_ANSWER))
  const saveCurrentSurveyParticipation = useRef(
    useMutation(SAVE_SURVEY_PARTICIPATION)
  )
  const saveRewards = useMutation(SAVE_REWARDS)
  let rejectedEnrollment = false
  let matchedSkipRule = null
  let answerValue = null
  const {
    data: {
      survey: { screenOut, products, productsQuestions }
    }
  } = useQuery(GET_SURVEY_INFO, {
    variables: { id: currentSurveyParticipation.surveyId }
  })

  const {
    answers,
    selectedProducts,
    surveyEnrollmentId,
    selectedProduct
  } = currentSurveyParticipation
  const stepId = inputStepId || currentSurveyParticipation.currentSurveyStep
  const chooseProductQuestion =
    productsQuestions && productsQuestions.length
      ? productsQuestions.find(q => q.type === 'choose-product')
      : {}
  let {
    data: { question }
  } = useQuery(GET_QUESTION_INFO, {
    variables: { id: inputStepId }
  })

  let rejectedRewards = []
  let currentAnswer =
    answers.find(a => a.id === getAnswerLocalId(stepId, selectedProducts)) || []
  if (currentAnswer && currentAnswer.values && !currentAnswer.values.length) {
    const newCurrentTestAnswer =
      answers.find(a => a.id === getAnswerLocalId(stepId)) || []
    if (
      newCurrentTestAnswer &&
      newCurrentTestAnswer.values &&
      newCurrentTestAnswer.values.length
    ) {
      currentAnswer = newCurrentTestAnswer
    }
  }

  // Condition implemented for rejection
  if (
    screenOut &&
    question &&
    currentAnswer &&
    currentAnswer.values &&
    currentAnswer.values.length &&
    questionType.includes(question.type)
  ) {
    answerValue = getSkipRuleAnswer(currentAnswer)
    matchedSkipRule = findSkipRule(question, answerValue)
    if (screenOut && matchedSkipRule && matchedSkipRule.skipTo === 'rejected') {
      rejectedEnrollment = true
      if (products && products.length) {
        const availableProducts = products.filter(
          product => product.isAvailable
        )
        rejectedRewards = availableProducts.map(product => ({
          id: product.id,
          reward: product.rejectedReward
        }))
      }
    } else {
      rejectedEnrollment = false
    }
  }
  const advanceStep = useCallback(
    async currentStep => {
      if (selectedProduct) {
        await saveCurrentSurveyParticipation.current({
          variables: {
            selectedProducts: uniq([...selectedProducts, selectedProduct])
          }
        })
        // Updating NEW PRODUCT in DB AS WELL, AS IT IS BEING USED LATER
        await setSelectedProducts({
          variables: {
            surveyEnrollmentId,
            selectedProducts: uniq([...selectedProducts, selectedProduct])
          }
        })
      }
      if (
        screenOut &&
        matchedSkipRule &&
        matchedSkipRule.skipTo === 'rejected' &&
        products
      ) {
        await saveCurrentSurveyParticipation.current({
          variables: {
            savedRewards: rejectedRewards
          }
        })
        saveRewards({
          variables: {
            rewards: rejectedRewards,
            surveyEnrollment: surveyEnrollmentId
          }
        })
      }
      // if (
      //   chooseProductQuestion &&
      //   stepId &&
      //   chooseProductQuestion.id === stepId &&
      //   selectedProduct
      // ) {
      //   const editedSavedRewards = savedRewards.map(sr => {
      //     if (sr.id === selectedProduct) {
      //       sr.startedAt = new Date().getTime()
      //     }
      //     return sr
      //   })
      //   await saveCurrentSurveyParticipation.current({
      //     variables: {
      //       savedRewards: editedSavedRewards
      //     }
      //   })
      //   await saveRewards({
      //     variables: {
      //       rewards: editedSavedRewards,
      //       surveyEnrollment: surveyEnrollmentId
      //     }
      //   })
      // }
      //main function to submit the answer
      await handleSubmitAnswer.current({
        variables: {
          input: {
            question: stepId,
            value: currentAnswer.values,
            context: currentAnswer.context,
            startedAt: currentAnswer.startedAt,
            surveyEnrollment: surveyEnrollmentId,
            selectedProduct,
            rejectedEnrollment
          }
        }
      })

      await handleAdvance.current({
        variables: { from: currentStep || stepId }
      })
    },
    [
      stepId,
      currentAnswer,
      selectedProducts,
      selectedProduct,
      surveyEnrollmentId,
      rejectedEnrollment,
      rejectedRewards,
      saveRewards,
      screenOut,
      matchedSkipRule,
      setSelectedProducts, //added this because it was missing from dependency
      products,
      chooseProductQuestion
    ]
  )

  return useMemo(
    () => ({
      currentSurveyParticipation,
      advanceStep
    }),
    [currentSurveyParticipation, advanceStep]
  )
}

const ADVANCE_IN_SURVEY = gql`
  mutation advanceInSurvey($from: ID, $skipLoop: Boolean) {
    advanceInSurvey(fromStep: $from, skipLoop: $skipLoop) @client
  }
`

const SUBMIT_ANSWER = gql`
  mutation submitAnswer($input: SubmitAnswerInput!) {
    submitAnswer(input: $input)
  }
`

export const SET_SELECTED_PRODUCTS = gql`
  mutation setSelectedProducts(
    $surveyEnrollmentId: ID!
    $selectedProducts: [ID!]
  ) {
    setSelectedProducts(
      surveyEnrollment: $surveyEnrollmentId
      selectedProducts: $selectedProducts
    ) {
      id
      selectedProducts {
        id
      }
    }
  }
`
export const GET_SURVEY_INFO = gql`
  query getSurveyInfo($id: ID) {
    survey(id: $id) @client {
      screenOut
      isScreenerOnly
      products {
        id
        name
        photo
        brand
        reward
        rejectedReward
        isAvailable
        isSurveyCover
        sortingOrderId
      }
      productsQuestions {
        id
        type
      }
    }
  }
`
const GET_QUESTION_INFO = gql`
  query getQuestion($id: ID) {
    question(id: $id) @client {
      ...questionInfo
    }
  }
  ${questionInfo}
`
const SAVE_REWARDS = gql`
  mutation saveRewards($rewards: JSON, $surveyEnrollment: ID) {
    saveRewards(rewards: $rewards, surveyEnrollment: $surveyEnrollment)
  }
`

const SAVE_SURVEY_PARTICIPATION = gql`
  mutation saveCurrentSurveyParticipation(
    $selectedProducts: [String]
    $savedRewards: JSON
  ) {
    saveCurrentSurveyParticipation(
      selectedProducts: $selectedProducts
      savedRewards: $savedRewards
    ) @client
  }
`

export default useAdvanceStep
