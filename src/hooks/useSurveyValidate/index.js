import { useCallback } from 'react'
import { useApolloClient } from 'react-apollo-hooks'
import { all, concat } from 'ramda'
import PubSub from 'pubsub-js'
import { message } from 'antd'
import { CHART_TYPE, PUBSUB } from '../../utils/Constants'
import {
  validateProductSchema,
  validateQuestionSchemas,
  validateSurveyBasics,
  validateProductRewardsRuleSchema,
  validateSurveyAdvanceSettings,
} from '../../validates'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import { useTranslation } from 'react-i18next'
import getChartTypes from '../../utils/getChartTypes'

const useSurveyValidate = () => {
  const { t } = useTranslation()
  const apollo = useApolloClient()

  const validateBasics = useCallback(async () => {
    const {
      data: {
        surveyCreation: { basics }
      }
    } = await apollo.query({
      query: SURVEY_CREATION,
      fetchPolicy: 'cache-only'
    })

    return validateSurveyBasics(true).isValidSync(basics)
  }, [apollo])

  const validateAdvanceSettings = useCallback(async () => {
    const {
      data: {
        surveyCreation: { basics }
      }
    } = await apollo.query({
      query: SURVEY_CREATION,
      fetchPolicy: 'cache-only'
    })

    if(!basics.promotionSettings) {
      return true
    }

    const promotionSettings = {
      promotionActive: basics.promotionSettings.active,
      promotionCode: basics.promotionSettings.code,
      promotionOptions: basics.promotionSettings.active ? basics.promotionOptions : null
    }

    return validateSurveyAdvanceSettings.isValidSync({...basics, ...promotionSettings})
  }, [apollo])

  const validateProducts = useCallback(async () => {
    const {
      data: {
        surveyCreation: {
          products,
          basics: { isScreenerOnly, productRewardsRule } = {}
        }
      }
    } = await apollo.query({
      query: SURVEY_CREATION,
      fetchPolicy: 'cache-only'
    })

    const isProductValid = validateProductSchema(
      productRewardsRule.active
    ).isValidSync.bind(validateProductSchema(productRewardsRule.active))

    const isProductsRuleValid = validateProductRewardsRuleSchema(
      productRewardsRule.active
    ).isValidSync(productRewardsRule)

    return (
      all(isProductValid, products, !isScreenerOnly) &&
      (!productRewardsRule.active || isProductsRuleValid)
    )
  }, [apollo])

  const validateQuestions = useCallback(
    async (fromPublish = false) => {
      const {
        data: {
          surveyCreation: {
            basics: { compulsorySurvey } = {},
            products,
            questions,
            mandatoryQuestions
          }
        }
      } = await apollo.query({ query: SURVEY_CREATION })

      const allQuestions = concat(questions, mandatoryQuestions)

      for (let question of allQuestions) {
        const validateQuestionSchema = validateQuestionSchemas[question.type]

        if (!question.type) {
          return { isValid: false, invalidQuestion: question }
        }

        if (validateQuestionSchema) {
          const chartTypes = getChartTypes(question)
          try {
            if(!question.chartType) {
              question.chartType = CHART_TYPE.NO_CHART
            }

            await validateQuestionSchema.validate(question, {
              context: { products, chartTypes }
            })
            if (compulsorySurvey) {
              await validateQuestionSchema.validate(question, {
                context: { compulsorySurvey }
              })
            }
          } catch (e) {
            return { isValid: false, invalidQuestion: question }
          }
        }
      }

      if (fromPublish && (!questions || !questions.length)) {
        return { isValid: false, noQuestionError: true }
      }

      return { isValid: true }
    },
    [apollo]
  )

  const validateChartTitles = useCallback(async () => {
    const {
      data: {
        surveyCreation: { questions, mandatoryQuestions }
      }
    } = await apollo.query({ query: SURVEY_CREATION })
    let hashTitle = {}
    let duplicateTitles = []
    const surveyQuestions = [...questions, ...mandatoryQuestions]

    surveyQuestions.forEach(question => {
      if (question.chartTitle) {
        if (!hashTitle[question.chartTitle]) {
          hashTitle[question.chartTitle] = 0
        }

        hashTitle[question.chartTitle]++
      }
    })

    Object.entries(hashTitle).forEach(([key, value]) => {
      if (value > 1) {
        duplicateTitles.push(key)
      }
    })

    return duplicateTitles
  }, [apollo])

  const showAdvanceSettingsErrors = () => {
    displayErrorPopup(t('containers.useSurveyValidate.showAdvanceSettingsErrors'))
    PubSub.publish(PUBSUB.VALIDATE_ADVANCE_SETTINGS)
  }

  const showProductsErrors = () => {
    displayErrorPopup(t('containers.useSurveyValidate.showProductsErrors'))
    PubSub.publish(PUBSUB.VALIDATE_SURVEY_PRODUCTS)
  }

  const showQuestionsErrors = () => {
    displayErrorPopup(t('containers.useSurveyValidate.showQuestionsErrors'))
    PubSub.publish(PUBSUB.VALIDATE_SURVEY_QUESTIONS)
  }

  const showOnlyMandatoryQuestionErrors = () => {
    displayErrorPopup(
      t('containers.useSurveyValidate.showOnlyMandatoryQuestionErrors')
    )
    PubSub.publish(PUBSUB.VALIDATE_SURVEY_QUESTIONS)
  }

  const showChartTitlesErrors = titles => {
    message.destroy()

    titles.forEach(title => {
      const error = t('containers.useSurveyValidate.showChartTitlesErrors', { title })
      message.error(error)
    })
    
    PubSub.publish(PUBSUB.VALIDATE_SURVEY_QUESTIONS)
  }

  return {
    validateBasics,
    validateAdvanceSettings,
    validateProducts,
    validateQuestions,
    validateChartTitles,
    showAdvanceSettingsErrors,
    showProductsErrors,
    showQuestionsErrors,
    showOnlyMandatoryQuestionErrors,
    showChartTitlesErrors
  }
}

export default useSurveyValidate
