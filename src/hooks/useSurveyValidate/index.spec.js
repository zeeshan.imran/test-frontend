import React from 'react'
import { mount } from 'enzyme'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import useSurveyValidate from './index'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import wait from '../../utils/testUtils/waait'
import { defaultSurvey } from '../../mocks'

const createSurveyCreationMock = secondProduct => ({
  ...defaultSurvey,
  products: [
    {
      name: 'the first product',
      brand: 'the brand',
      reward: 0,
      rejectedReward : 0,
    },
    secondProduct
  ]
})

const TestHook = ({ callback }) => {
  callback()
  return null
}

describe('validate products', () => {
  let client
  let testRender

  beforeEach(() => {
    client = createApolloMockClient()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should return isValid = false if the survey has a product with name is empty', async () => {
    let isValid
    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: createSurveyCreationMock({
          name: '',
          brand: 'the brand',
          reward: 2,
          rejectedReward : 0
        })
      }
    })

    testRender = mount(
      <ApolloProvider client={client}>
        <TestHook
          callback={async () => {
            try {
              const surveyValidate = useSurveyValidate()
              isValid = await surveyValidate.validateProducts()
            } catch (ex) {
              //
            }
          }}
        />
      </ApolloProvider>
    )

    await wait(0)

    expect(isValid).toBe(false)
  })

  test('should return isValid = false if the survey has a product with brand is empty', async () => {
    let isValid
    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: createSurveyCreationMock({
          name: 'the second product',
          brand: '',
          reward: 12,
          rejectedReward : 0,
        })
      }
    })

    testRender = mount(
      <ApolloProvider client={client}>
        <TestHook
          callback={async () => {
            const surveyValidate = useSurveyValidate()
            isValid = await surveyValidate.validateProducts()
          }}
        />
      </ApolloProvider>
    )

    await wait(0)

    expect(isValid).toBe(false)
  })

  test('should return isValid = true if the survey has all products filled with name and brand', async () => {
    let isValid
    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: createSurveyCreationMock({
          name: 'the second product',
          brand: 'the brand',
          reward: 2,
          rejectedReward : 0,
        })
      }
    })

    testRender = mount(
      <ApolloProvider client={client}>
        <TestHook
          callback={async () => {
            const surveyValidate = useSurveyValidate()
            isValid = await surveyValidate.validateProducts()
          }}
        />
      </ApolloProvider>
    )

    await wait(0)

    expect(isValid).toBe(true)
  })
})
