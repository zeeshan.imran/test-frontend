import { useQuery, useMutation } from 'react-apollo-hooks'
import { generate } from 'shortid'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import gql from 'graphql-tag'
import { map, flatten, pipe } from 'ramda'
import formatProductsForPublishing from '../../utils/formatProductsForPublishing'
import formatBasicsForPublishing from '../../utils/formatBasicsForPublishing'
import sortQuestionsForPublishing from '../../utils/sortQuestionsForPublishing'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import { getAuthenticatedOrganization } from '../../utils/userAuthentication'
import { trimSpaces } from '../../utils/trimSpaces'
import { questionInfo } from '../../fragments/survey'
import formatQuestionsForUpdating from '../../utils/formatQuestionsForUpdating'
import logger from '../../utils/logger'

export const trimFields = field => {
  if (!field) {
    return field
  }
  switch (field.constructor) {
    case Object: {
      let newField = {}
      for (let subfield of Object.keys(field)) {
        Object.assign(newField, {
          [subfield]: trimFields(field[subfield])
        })
      }
      return newField
    }
    case Array: {
      let newField = []
      for (let subfield of field) {
        newField = [...newField, trimFields(subfield)]
      }
      return newField
    }
    case Number:
      return field
    case String:
      return trimSpaces(field)
    default:
      return field
  }
}

const isPairedQuestionFollowedByProfile = question =>
  question.type === 'paired-questions' &&
  question.pairsOptions &&
  question.pairsOptions.hasFollowUpProfile

const isSliderQuestionFollowedByProfile = question =>
  question.type === 'slider' &&
  question.sliderOptions &&
  question.sliderOptions.hasFollowUpProfile

const usePublishSurvey = ({
  surveyInitialState,
  predecessorSurvey,
  successMessage,
  errorMessage,
  folderId
}) => {
  const {
    data: {
      surveyCreation: { products, basics, questions, mandatoryQuestions }
    }
  } = useQuery(SURVEY_CREATION)

  const createSurvey = useMutation(CREATE_SURVEY)
  const resetCreationForm = useMutation(RESET_CREATION_FORM)
  const reloadSurveyProductOnForm = useMutation(RELOAD_SURVEY_PRODUCT_ON_FORM)
  const reloadSurveyQuestionOnForm = useMutation(RELOAD_SURVEY_QUESTION_ON_FORM)
  const renewSurveyQuestionOnForm = useMutation(RENEW_SURVEY_QUESTION_ON_FORM)
  const moveToFolder = useMutation(MOVE_TO_FOLDER)

  return async () => {
    try {
      await renewSurveyQuestionOnForm()

      const formattedProducts = formatProductsForPublishing(products)

      const allQuestions = sortQuestionsForPublishing(
        questions,
        mandatoryQuestions
      )
      const formattedQuestions = formatQuestionsForUpdating({
        questions: allQuestions.map(question => {
          return {
            ...question,
            reorder: undefined
          }
        })
      })

      const publishQuestions = pipe(
        map(question => {
          const questionId = question.id || question.clientGeneratedId
          if (isPairedQuestionFollowedByProfile(question)) {
            // return two questions on paired, then flatten later
            return [
              question,
              {
                clientGeneratedId: generate(),
                ...question.pairsOptions.profileQuestion,
                relatedQuestions: [questionId],
                id: undefined
              }
            ]
          }
          if (isSliderQuestionFollowedByProfile(question)) {
            // return two questions on slider, then flatten later
            return [
              question,
              {
                clientGeneratedId: generate(),
                ...question.sliderOptions.profileQuestion,
                relatedQuestions: [questionId],
                id: undefined
              }
            ]
          }

          return [question]
        }),
        flatten
      )(formattedQuestions)

      // CREATE SURVEY
      const formattedBasics = formatBasicsForPublishing({ basics, products })
      const createSurveyInput = {
        basics: trimFields({
          ...formattedBasics,
          owner: getAuthenticatedOrganization(),
          state: surveyInitialState,
          predecessorSurvey: predecessorSurvey
        }),
        products: trimFields(formattedProducts),
        questions: trimFields(publishQuestions)
      }
      logger.setContext('createSurveyInput', createSurveyInput)

      const {
        data: {
          createSurvey: {
            survey: createdSurvey,
            replacedQuestions,
            replacedProducts
          }
        }
      } = await createSurvey({
        variables: {
          input: createSurveyInput
        }
      })

      for (const { clientGeneratedId, product } of replacedProducts) {
        await reloadSurveyProductOnForm({
          variables: { clientGeneratedId, product }
        })
      }

      // CREATE QUESTIONS
      for (const { clientGeneratedId, question } of replacedQuestions) {
        await reloadSurveyQuestionOnForm({
          variables: { clientGeneratedId, question }
        })
      }

      displaySuccessMessage(successMessage)
      resetCreationForm()

      logger.clearContext()
      if (folderId) {
        await moveToFolder({
          variables: { id: folderId, surveyId: createdSurvey.id }
        })
      }
      return {
        ...createdSurvey,
        folderId: folderId ? folderId : createdSurvey.folder
      }
    } catch (error) {
      logger.error(error)
      if (
        error.graphQLErrors &&
        error.graphQLErrors.length > 0 &&
        error.graphQLErrors[0].message === 'E_UNIQUE'
      ) {
        throw new Error('"Unique name" must be unique')
      } else {
        throw new Error(errorMessage)
      }
    }
  }
}

const RELOAD_SURVEY_PRODUCT_ON_FORM = gql`
  mutation($clientGeneratedId: ID, $product: Object) {
    reloadSurveyProductOnForm(
      clientGeneratedId: $clientGeneratedId
      product: $product
    ) @client
  }
`

const RELOAD_SURVEY_QUESTION_ON_FORM = gql`
  mutation($clientGeneratedId: ID, $question: Object) {
    reloadSurveyQuestionOnForm(
      clientGeneratedId: $clientGeneratedId
      question: $question
    ) @client
  }
`

const RENEW_SURVEY_QUESTION_ON_FORM = gql`
  mutation {
    renewSurveyQuestionOnForm @client
  }
`

export const CREATE_SURVEY = gql`
  mutation createSurvey($input: CreateSurveyInput!) {
    createSurvey(input: $input) {
      survey {
        id
        name
        folder
      }
      replacedProducts {
        clientGeneratedId
        product {
          id
          name
          brand
          photo
          isSurveyCover
          sortingOrderId
          internalName
        }
      }
      replacedQuestions {
        clientGeneratedId
        question {
          ...questionInfo
        }
      }
    }
  }

  ${questionInfo}
`

export const RESET_CREATION_FORM = gql`
  mutation resetCreationForm {
    resetCreationForm @client
  }
`

const MOVE_TO_FOLDER = gql`
  mutation moveSurveyToFolder($id: ID!, $folderId: ID, $surveyId: ID) {
    moveSurveyToFolder(id: $id, folderId: $folderId, surveyId: $surveyId) {
      id
      name
    }
  }
`

export default usePublishSurvey
