import React, { useCallback, useMemo, useRef } from 'react'
import { without } from 'ramda'
import gql from 'graphql-tag'
import { useApolloClient } from 'react-apollo-hooks'
import { message, notification } from 'antd'
import i18n from '../../utils/internationalization/i18n'

const showRawFile = statsResult => {
  const baseUri = {
    localhost: 'http://localhost:4100',
    'staging10.flavorwiki.com': 'http://78.47.104.144:4110'
  }[window.location.hostname]
  if (baseUri && statsResult.result && statsResult.result.statistic) {
    const questionId = statsResult.result && statsResult.result.question
    const statsName = Object.keys(statsResult.result.statistic)[0]
    const path = `${questionId}/${statsResult.jobId}`
    notification.open({
      message: `Download raw data (${statsName})`,
      description: <a href={`${baseUri}/raw/${path}`}>{path}</a>,
      duration: 10
    })
  }
}

const useSurveyStatsResult = () => {
  const client = useApolloClient()
  let timer = useRef(null)

  const startAutoRefresh = useCallback(
    (jobGroupId, jobIds, cacheQuery) =>
      new Promise(resolve => {
        {
          const refresh = async () => {
            try {
              const data = client.cache.readQuery(cacheQuery)

              const {
                data: { statsResults }
              } = await client.query({
                query: STATS_RESULTS,
                variables: { jobGroupId, jobIds },
                fetchPolicy: 'network-only'
              })

              for (let statsResult of statsResults) {
                if (['ERROR'].includes(statsResult.status)) {
                  message.error(
                    i18n.t(
                      `charts.analysis.errors.${statsResult.result.message}`
                    )
                  )
                }

                if (['ERROR', 'OK'].includes(statsResult.status)) {
                  showRawFile(statsResult)
                  jobIds = without(statsResult.jobId)(jobIds)
                  for (let tab of data.survey.stats.tabs) {
                    for (let chart of tab.charts) {
                      if (chart.question === statsResult.result.question) {
                        tab.completedCount = (tab.completedCount || 0) + 1
                        Object.assign(
                          chart,
                          statsResult.result,
                          {
                            statistic: {
                              ...chart.statistic,
                              ...statsResult.result.statistic
                            }
                          },
                          {
                            loading: false
                          }
                        )
                      }
                    }
                  }
                }
              }

              client.cache.writeQuery({
                ...cacheQuery,
                data
              })

              if (jobIds.length > 0) {
                timer.current = setTimeout(refresh, 500)
              } else {
                resolve()
              }
            } catch (error) {
              message.error(i18n.t(`charts.analysis.errors.apiError`))
            }
          }

          refresh()
        }
      }),
    [client]
  )

  const stopAutoRefresh = useCallback(() => {
    clearTimeout(timer.current)
  }, [])

  return useMemo(() => ({ startAutoRefresh, stopAutoRefresh }), [
    startAutoRefresh,
    stopAutoRefresh
  ])
}

const STATS_RESULTS = gql`
  query getStatsResults($jobGroupId: ID!, $jobIds: [ID!]) {
    statsResults: getStatsResults(jobGroupId: $jobGroupId, jobIds: $jobIds)
  }
`

export default useSurveyStatsResult
