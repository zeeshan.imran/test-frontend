import gql from 'graphql-tag'
import { path } from 'ramda'
import { useQuery } from 'react-apollo-hooks'
import { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { createBrowserHistory } from 'history'
import getQueryParams from '../../utils/getQueryParams'
import { getQuestionId } from '../../utils/getQuestionId'

const useSkipToOptions = (withoutQuestion, isMatrixQuestion = false) => {
  const { data } = useQuery(SURVEY_CREATION)
  const { t } = useTranslation()
  const history = createBrowserHistory()

  const { location } = history
  const queryData = getQueryParams(location)
  const { section } = queryData

  return useMemo(() => {
    let questions = path(['surveyCreation', 'questions'], data) || []

    if (
      questions.length &&
      data.surveyCreation &&
      data.surveyCreation.mandatoryQuestions.length
    ) {
      questions = [
        ...questions,
        ...data.surveyCreation.mandatoryQuestions.filter(q => {
          return q.type !== 'choose-payment' && q.type !== 'paypal-email'
        })
      ]
    }

    const sortedQuestion = questions.sort((a, b) => {
      return a.order - b.order
    })

    const indexId = sortedQuestion.findIndex(
      x => getQuestionId(x) === getQuestionId(withoutQuestion)
    )

    const skiptOptionsList = [
      ...sortedQuestion
        .map((question, index) => ({
          value: getQuestionId(question),
          label: `${index + 1}. ${question.prompt || question.type || ''}`,
          type: question.type,
          displayOn: question.displayOn,
          itemIndex: index
        }))
        .filter(
          ({ value: questionId, type, displayOn, itemIndex, ...other }) => {
            if (section === 'screening') {
              return (
                type &&
                (displayOn === section || displayOn === 'begin') &&
                itemIndex > indexId &&
                questionId !== getQuestionId(withoutQuestion)
              )
            } else if (section && displayOn) {
              return (
                type &&
                displayOn === section &&
                itemIndex > indexId &&
                questionId !== getQuestionId(withoutQuestion)
              )
            } else return type && questionId !== getQuestionId(withoutQuestion)
          }
        )
    ]
    if (isMatrixQuestion) {
      skiptOptionsList.push({
        value: 'continue',
        label: t('defaultValues.default.customButtons.continue')
      })
    }
    skiptOptionsList.push({
      value: 'finished',
      label: t('containers.questionsList.skipToOptions.finish')
    })
    skiptOptionsList.push({
      value: 'rejected',
      label: t('containers.questionsList.skipToOptions.reject')
    })

    return skiptOptionsList
  }, [data, t])
}

export const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      questions
      mandatoryQuestions
    }
  }
`

export default useSkipToOptions
