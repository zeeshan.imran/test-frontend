import React from 'react'
import { mount } from 'enzyme'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import useSkipToOptions, { SURVEY_CREATION } from '.'
import wait from '../../utils/testUtils/waait'

jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k,
  getFixedT: () => () => ''
}))
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

const TestHook = ({ callback }) => {
  callback()
  return null
}
const mockSurveyCreation = {
  __typename: 'SurveyCreation',
  surveyId: 'survey-1',
  surveyEnrollmentId: 'survey-enrollment-1',
  state: 'active',
  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?'
    },
    {
      type: 'info',
      prompt: 'What is your email?',
      clientGeneratedId: 'generated-2'
    },
    {
      id: 'question-3',
      type: 'info',
      prompt: 'What is your email?'
    }
  ],
  mandatoryQuestions: []
}

describe('useSkipToOptions hooks', () => {
  let testRender
  let client

  beforeEach(() => {
    client = createApolloMockClient()

    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: mockSurveyCreation
      }
    })
  })
  afterEach(() => {
    testRender.unmount()
  })

  test('should call SURVEY_CREATION on server side (hide local question)', async () => {
    let questions
    testRender = mount(
      <ApolloProvider client={client}>
        <TestHook
          callback={async () => {
            questions = await useSkipToOptions({
              clientGeneratedId:
                mockSurveyCreation.questions[1].clientGeneratedId
            })
          }}
        />
      </ApolloProvider>
    )

    await wait(0)
    expect(questions).toMatchObject([
      expect.objectContaining({ value: 'question-1' }),
      expect.objectContaining({ value: 'question-3' }),
      expect.objectContaining({ value: 'finished' }),
      expect.objectContaining({ value: 'rejected' })
    ])
  })

  test('should call SURVEY_CREATION on server side (hide: server question)', async () => {
    let questions
    testRender = mount(
      <ApolloProvider client={client}>
        <TestHook
          callback={async () => {
            questions = await useSkipToOptions({
              id: mockSurveyCreation.questions[0].id
            })
          }}
        />
      </ApolloProvider>
    )

    await wait(0)
    expect(questions).toMatchObject([
      expect.objectContaining({ value: 'generated-2' }),
      expect.objectContaining({ value: 'question-3' }),
      expect.objectContaining({ value: 'finished' }),
      expect.objectContaining({ value: 'rejected' })
    ])
  })
})
