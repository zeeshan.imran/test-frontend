import { useRef, useCallback, useEffect } from 'react'
import gql from 'graphql-tag'
import { useMutation, useApolloClient } from 'react-apollo-hooks'
import { map, prop } from 'ramda'
import useSurveyStatsResult from '../useSurveyStatsResult'

const getAnalysisType = prop('analysisType')

const useStatistic = ({
  cachedQuery,
  jobGroupId,
  productIds,
  questionFilters,
  dateFilter
}) => {
  const client = useApolloClient()
  const createJobs = useRef(useMutation(CREATE_JOBS))
  const statsResults = useSurveyStatsResult()
  const isStatisticsLoading = useRef(false)

  const resetStatistic = useCallback(
    (question, analysisTypes) => {
      const cachedData = client.cache.readQuery(cachedQuery)
      for (let tab of cachedData.survey.stats.tabs) {
        for (let chart of tab.charts) {
          if (chart.question === question) {
            Object.assign(chart, {
              statistic: analysisTypes.reduce(
                (acc, analysisType) => ({
                  ...acc,
                  [analysisType]: { loading: true }
                }),
                { loading: true }
              )
            })
          }
        }
      }
    },
    [client, cachedQuery]
  )

  const finishStatistic = useCallback(
    question => {
      const cachedData = client.cache.readQuery(cachedQuery)
      isStatisticsLoading.current = false
      for (let tab of cachedData.survey.stats.tabs) {
        for (let chart of tab.charts) {
          if (chart.question === question) {
            Object.assign(chart, {
              statistic: {
                ...chart.statistic,
                loading: false
              }
            })
          }
        }
      }
    },
    [client, cachedQuery]
  )

  const runStatistic = useCallback(
    async ({ question, analysisRequests }) => {
      resetStatistic(question, map(getAnalysisType)(analysisRequests))
      isStatisticsLoading.current = true

      const { data } = await createJobs.current({
        variables: {
          input: {
            //TODO: rename it
            jobGroup: jobGroupId,
            question,
            analysisRequests,
            productIds,
            questionFilters,
            dateFilter
          }
        }
      })

      const { jobs: jobIds } = data.createJobs
      await statsResults.startAutoRefresh(jobGroupId, jobIds, cachedQuery)

      finishStatistic(question)
    },
    [
      jobGroupId,
      productIds,
      questionFilters,
      cachedQuery,
      statsResults,
      resetStatistic,
      finishStatistic
    ]
  )

  useEffect(() => {
    return () => {
      statsResults.stopAutoRefresh()
    }
  }, [statsResults])

  return { runStatistic, isStatisticsLoading: isStatisticsLoading.current }
}

const CREATE_JOBS = gql`
  mutation createJobs($input: CreateJobsInput!) {
    createJobs(input: $input)
  }
`

export default useStatistic
