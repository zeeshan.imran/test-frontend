import { useMemo } from 'react'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'

const acceptedQuestions = {
  productsQuestions: ['vertical-rating']
}

const filterQuestions = (questions = {}, selectedQuestion) => {
  let filteredQuestions = []
  let index = 0
  Object.keys(acceptedQuestions).forEach(key => {
    if (questions[key]) {
      filteredQuestions.push(
        ...(acceptedQuestions[key] === 'all'
          ? questions[key]
          : questions[key].filter(question =>
              acceptedQuestions[key].includes(question.type)
            )
        )
          .filter(question => question.id !== selectedQuestion)
          .map(question => {
            index++
            return {
              ...question,
              prompt: `${index}. [${question.type}] ${question.prompt}`
            }
          })
      )
      index =
        acceptedQuestions[key] === 'all' ? index + questions[key].length : index
    }
  })
  return filteredQuestions
}

const useReferenceQuestions = (
  surveyId,
  selectedQuestion,
  fetchPolicy = 'network-only'
) => {
  const { data: { survey: questions } = {} } = useQuery(QUERY_SURVEY, {
    variables: {
      surveyId
    },
    fetchPolicy
  })
  return useMemo(() => {
    return filterQuestions(questions, selectedQuestion)
  }, [questions, selectedQuestion])
}

const QUERY_SURVEY = gql`
  query survey($surveyId: ID!) {
    survey(id: $surveyId) {
      id
      productsQuestions {
        id
        prompt
        type
      }
    }
  }
`

export default useReferenceQuestions
