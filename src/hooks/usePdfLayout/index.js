import { useCallback } from 'react'
import { getGridSize, PAPER_SIZES } from '../../utils/paperSizes'

export const usePdfLayout = () => {
  const fitGridItem = useCallback(
    (elementItem, layoutItem, { colWidth, rowHeight }) => {
      if (elementItem) {
        const width = Math.round(layoutItem.w * colWidth)
        const height = Math.round(layoutItem.h * rowHeight)
        const ref = elementItem.getRef && elementItem.getRef()
        if (ref) {
          //console.log(layoutItem.w, 'x', layoutItem.h, '->', width, 'x', height)
          ref.updateGridItemSize({
            width: width - 4,
            height: height - 4
          })
        }
      }
    },
    []
  )

  const fixLayout = useCallback(
    (elementItems, pageItems, gridLayout, paperSize, zoom) => {
      const paperSizeMeta = PAPER_SIZES[paperSize]
      const { colWidth, rowHeight } = getGridSize(paperSizeMeta, zoom)
      for (let layoutItem of gridLayout) {
        const id = layoutItem.i

        const elementItem =
          elementItems.find(item => item.i === id) ||
          pageItems.find(item => item.i === id)
        if (elementItem) {
          if (elementItem.align === 'fit') {
            const ref = elementItem.getRef && elementItem.getRef()
            if (ref) {
              fitGridItem(elementItem, layoutItem, { colWidth, rowHeight })
            }
          }

          if (elementItem.align === 'scale') {
            const ref = elementItem.getRef && elementItem.getRef()
            if (ref) {
              ref.setScale(zoom)
            }
          }
        }
      }
    },
    [fitGridItem]
  )

  return { fitGridItem, fixLayout }
}
