import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import { getAuthenticatedUser } from '../../utils/userAuthentication'
import { organizationFlavorwikiUniqueName } from '../../utils/Constants'

export const GET_ORGANIZATION = gql`
  query organization($id: ID!) {
    organization(id: $id) {
      id
      name
      uniqueName
    }
  }
`

const useFlavorwikiOrganization = () => {
  const user = getAuthenticatedUser()
  const organizationId = user && user.organization ? user.organization.id : null

  const { data: { organization = {} } = {} } = useQuery(GET_ORGANIZATION, {
    skip: !organizationId,
    variables: { id: organizationId }
  })

  return organizationId && organization.uniqueName === organizationFlavorwikiUniqueName
}

export default useFlavorwikiOrganization
