import { useCallback } from 'react'
import gql from 'graphql-tag'
import { useMutation } from 'react-apollo-hooks'

const useSelectSingleProduct = () => {
  const setSelectedProducts = useMutation(SET_SELECTED_PRODUCTS)

  return useCallback(
    async ({ surveyEnrollmentId, productId }) => {
      await setSelectedProducts({
        variables: { surveyEnrollmentId, selectedProduct: productId }
      })
    },
    [setSelectedProducts]
  )
}

export const SET_SELECTED_PRODUCTS = gql`
  mutation setSelectedProducts(
    $surveyEnrollmentId: ID!
    $selectedProduct: ID!
  ) {
    setSelectedProducts(
      surveyEnrollment: $surveyEnrollmentId
      selectedProducts: [$selectedProduct]
    ) {
      id
      selectedProducts {
        id
      }
    }

    saveCurrentSurveyParticipation(
      selectedProducts: [$selectedProduct]
      selectedProduct: $selectedProduct
    ) @client
  }
`

export default useSelectSingleProduct
