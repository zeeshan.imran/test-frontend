import { useMemo } from 'react'
import { useQuery } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { uniq } from 'ramda'

const getUniqueBrandIds = products => {
  products = products || []
  return uniq(products.map(product => product && product.brand)).filter(
    b => !!b
  )
}

const useProductsBrands = products => {
  const productsBrands = useMemo(() => getUniqueBrandIds(products), [products])
  const isNoProducts = !products || products.length === 0

  const { data, loading } = useQuery(BRANDS_QUERY, {
    variables: { ids: productsBrands },
    skip: isNoProducts,
    fetchPolicy: 'network-only'
  })

  if (isNoProducts) {
    return { data: { brands: [] }, loading: false }
  }

  return { data, loading }
}

export const BRANDS_QUERY = gql`
  query brands($ids: [ID]) {
    brands(ids: $ids) {
      id
      name
      logo
    }
  }
`

export default useProductsBrands
