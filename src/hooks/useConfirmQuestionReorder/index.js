import { useQuery } from 'react-apollo-hooks'
import { Modal } from 'antd'
import { useTranslation } from 'react-i18next'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'

const useConfirmQuestionReorder = () => {
  const { t } = useTranslation()
  const {
    data: {
      surveyCreation: { questions } = {}
    }
  } = useQuery(SURVEY_CREATION)

  return (onConfirm) => {
    const reorderedQuestion = questions.find(question => {
      return question.reorder
    })
  
    if (reorderedQuestion) {
      Modal.confirm({
        title: t('reorderQuestion.confirmModalTitle'),
        content: t('reorderQuestion.confirmModalContent'),
        onOk: () => {
          onConfirm()
        }
      })
    }
    else {
      onConfirm()
    }
  }
}


export default useConfirmQuestionReorder
