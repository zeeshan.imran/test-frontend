export const FLAVORWIKI_AUTH_TOKEN = 'flavorwiki-token'

export const FLAVORWIKI_AUTH_STATUS = 'flavorwiki-isAuthenticated'

export const FLAVORWIKI_SURVEY_REDIRECT = 'flavorwiki-redirect-to-survey'

export const setAuthenticationToken = token =>
  window.sessionStorage.setItem(FLAVORWIKI_AUTH_TOKEN, token)

export const setAuthenticationStatus = status =>
  window.sessionStorage.setItem(FLAVORWIKI_AUTH_STATUS, status)

export const getAuthenticationToken = () =>
  window.sessionStorage.getItem(FLAVORWIKI_AUTH_TOKEN)

export const isUserAuthenticated = () =>
  !!window.sessionStorage.getItem(FLAVORWIKI_AUTH_TOKEN)

const deleteAuthenticationToken = () =>
  window.sessionStorage.removeItem(FLAVORWIKI_AUTH_TOKEN)

const deleteAuthenticationStatus = () =>
  window.sessionStorage.removeItem(FLAVORWIKI_AUTH_STATUS)

export const logout = () => {
  deleteAuthenticationStatus()
  deleteAuthenticationToken()
}

export const setSurveyRedirect = surveyId =>
  window.localStorage.setItem(FLAVORWIKI_SURVEY_REDIRECT, surveyId)

export const getSurveyRedirect = () =>
  window.localStorage.getItem(FLAVORWIKI_SURVEY_REDIRECT)

export const removeSurveyRedirect = () =>
  window.localStorage.removeItem(FLAVORWIKI_SURVEY_REDIRECT)
