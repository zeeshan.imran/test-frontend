import { defaultChartSettings } from '../../defaults/chartSettings'

const availableFonts = [
  { name: 'sans-serif', value: 'sans-serif' },
  { name: 'Lato', value: 'Lato' },
  { name: 'Roboto', value: 'Roboto' },
  { name: 'Lato-Regular', value: 'Lato-Regular' },
  { name: 'Lato-Light', value: 'Lato-Light' },
  { name: 'Lato-Italic', value: 'Lato-Italic' },
  { name: 'Lato-Bold-Italic', value: 'Lato-Bold-Italic' },
  { name: 'Lato-Bold', value: 'Lato-Bold' },
  { name: 'Lato-Black', value: 'Lato-Black' },
  { name: 'NotoSans-kr-Regular', value: 'NotoSans-kr-Regular' },
  { name: 'NotoSans-kr-Light', value: 'NotoSans-kr-Light' },
  { name: 'NotoSans-kr-Bold', value: 'NotoSans-kr-Bold' },
  { name: 'NotoSans-kr-Black', value: 'NotoSans-kr-Black' },
  { name: 'NunitoSans-Regular', value: 'NunitoSans-Regular' },
  { name: 'NunitoSans-Light', value: 'NunitoSans-Light' },
  { name: 'NunitoSans-Bold', value: 'NunitoSans-Bold' },
  { name: 'NunitoSans-Black', value: 'NunitoSans-Black' },
  { name: 'Avenir-Light', value: 'Avenir-Light' },
  { name: 'Avenir-Roman', value: 'Avenir-Roman' },
  { name: 'Avenir-Medium', value: 'Avenir-Medium' },
  { name: 'Verdana-Bold', value: 'Verdana-Bold' },
  { name: 'Verdana', value: 'Verdana' },
  { name: 'Geeza-Pro-Regular', value: 'Geeza-Pro-Regular' },
  { name: 'Yehei', value: 'Yehei' },
  { name: 'Lato-Regular, Arial', value: 'Lato-Regular, Arial' }
]

const availableFontSizes = [
  { name: '80%', value: 0.8 },
  { name: '90%', value: 0.9 },
  { name: '100%', value: 1 },
  { name: '110%', value: 1.1 },
  { name: '120%', value: 1.2 },
  { name: '130%', value: 1.3 },
  { name: '140%', value: 1.4 },
  { name: '150%', value: 1.5 },
  { name: '160%', value: 1.6 },
  { name: '170%', value: 1.7 },
  { name: '180%', value: 1.8 },
  { name: '190%', value: 1.9 },
  { name: '200%', value: 2 }
]

export const settings = {
  divider: {
    type: 'divider'
  },
  fontFamily_axis: {
    type: 'dropdown',
    options: availableFonts,
    label: 'settings.charts.optionLabels.fontFamily_axis',
    name: 'fontFamily_axis',
    default: defaultChartSettings.fontFamily_axis
  },
  fontFamily_labels: {
    type: 'dropdown',
    options: availableFonts,
    label: 'settings.charts.optionLabels.fontFamily_labels',
    name: 'fontFamily_labels',
    default: defaultChartSettings.fontFamily_labels
  },
  fontFamily_legend: {
    type: 'dropdown',
    options: availableFonts,
    label: 'settings.charts.optionLabels.fontFamily_legend',
    name: 'fontFamily_legend',
    default: defaultChartSettings.fontFamily_legend
  },
  fontFamily_values: {
    type: 'dropdown',
    options: availableFonts,
    label: 'settings.charts.optionLabels.fontFamily_values',
    name: 'fontFamily_values',
    default: defaultChartSettings.fontFamily_values
  },
  isTotalRespondentsShown: {
    type: 'switch',
    name: 'isTotalRespondentsShown',
    label: 'settings.charts.optionLabels.isTotalRespondentsShown',
    default: defaultChartSettings.isTotalRespondentsShown
  },
  isTotalRespondentsProProductShown: {
    type: 'switch',
    name: 'isTotalRespondentsProProductShown',
    label: 'settings.charts.optionLabels.isTotalRespondentsProProductShown',
    default: defaultChartSettings.isTotalRespondentsProProductShown
  },
  chartWidth: {
    type: 'dropdown',
    options: [
      { name: '100%', value: 1 },
      { name: '90%', value: 0.9 },
      { name: '80%', value: 0.8 },
      { name: '70%', value: 0.7 },
      { name: '60%', value: 0.6 },
      { name: '50%', value: 0.5 },
      { name: '40%', value: 0.4 },
      { name: '30%', value: 0.3 },
      { name: '20%', value: 0.2 }
    ],
    name: 'chartWidth',
    label: 'settings.charts.optionLabels.chartWidth',
    default: defaultChartSettings.chartWidth
  },
  howManyDecimals: {
    type: 'dropdown',
    options: [
      { name: '0', value: 0 },
      { name: '1', value: 1 },
      { name: '2', value: 2 },
      { name: '3', value: 3 },
      { name: '4', value: 4 },
      { name: '5', value: 5 }
    ],
    name: 'howManyDecimals',
    label: 'settings.charts.optionLabels.howManyDecimals',
    placeholder: 'placeholders.chartsSettings.howManyDecimals',
    default: defaultChartSettings.howManyDecimals
  },
  isScaleShown: {
    type: 'switch',
    name: 'isScaleShown',
    label: 'settings.charts.optionLabels.isScaleShown',
    default: defaultChartSettings.isScaleShown
  },
  scaleRange: {
    type: 'dropdown',
    options: [
      { name: '0 - 10', value: '10' },
      { name: '0 - 100', value: '100' }
    ],
    name: 'scaleRange',
    label: 'settings.charts.optionLabels.scaleRange',
    default: defaultChartSettings.scaleRange
  },
  howManyDecimals_axis: {
    type: 'dropdown',
    options: [
      { name: '0', value: 0 },
      { name: '1', value: 1 },
      { name: '2', value: 2 },
      { name: '3', value: 3 },
      { name: '4', value: 4 },
      { name: '5', value: 5 }
    ],
    name: 'howManyDecimals_axis',
    label: 'settings.charts.optionLabels.howManyDecimals_axis',
    placeholder: 'placeholders.chartsSettings.howManyDecimals',
    default: defaultChartSettings.howManyDecimals_axis
  },
  fontSize_axis: {
    type: 'dropdown',
    options: availableFontSizes,
    name: 'fontSize_axis',
    label: 'settings.charts.optionLabels.fontSize_axis',
    default: defaultChartSettings.fontSize_axis
  },
  fontSize_labels: {
    type: 'dropdown',
    options: availableFontSizes,
    name: 'fontSize_labels',
    label: 'settings.charts.optionLabels.fontSize_labels',
    default: defaultChartSettings.fontSize_labels
  },
  fontSize_legend: {
    type: 'dropdown',
    options: availableFontSizes,
    name: 'fontSize_legend',
    label: 'settings.charts.optionLabels.fontSize_legend',
    default: defaultChartSettings.fontSize_legend
  },
  fontSize_values: {
    type: 'dropdown',
    options: availableFontSizes,
    name: 'fontSize_values',
    label: 'settings.charts.optionLabels.fontSize_values',
    default: defaultChartSettings.fontSize_values
  },
  valueType: {
    type: 'dropdown',
    options: [
      { name: 'Percentage', value: 'percentage' },
      { name: 'Absolute values', value: 'value' }
    ],
    name: 'valueType',
    label: 'settings.charts.optionLabels.valueType',
    placeholder: 'placeholders.chartsSettings.valueType',
    default: defaultChartSettings.valueType
  },
  isCountProLabelShown: {
    type: 'switch',
    name: 'isCountProLabelShown',
    label: 'settings.charts.optionLabels.isCountProLabelShown',
    default: defaultChartSettings.isCountProLabelShown
  },
  isChartGridHidden: {
    type: 'switch',
    name: 'isChartGridHidden',
    label: 'settings.charts.optionLabels.isChartGridHidden'
  },
  percentageGrouping: {
    type: 'dropdown',
    options: [
      {
        name: 'settings.charts.optionLabels.percentageGrouping.inner',
        value: 'inner_group'
      },
      {
        name: 'settings.charts.optionLabels.percentageGrouping.outer',
        value: 'outer_group'
      }
    ],
    name: 'percentageGrouping',
    label: 'settings.charts.optionLabels.percentageGroupingLabel',
    showIf: settings => settings.valueType === 'percentage',
    default: defaultChartSettings.percentageGrouping
  },
  isCountProGroupShown: {
    type: 'switch',
    name: 'isCountProGroupShown',
    label: 'settings.charts.optionLabels.isCountProGroupShown',
    default: defaultChartSettings.isCountProGroupShown
  },
  isProductLabelsSwapped: {
    type: 'switch',
    name: 'isProductLabelsSwapped',
    label: 'settings.charts.optionLabels.isProductLabelsSwapped',
    default: defaultChartSettings.isProductLabelsSwapped
  },
  isStackedLabelsReversed: {
    type: 'switch',
    name: 'isStackedLabelsReversed',
    label: 'settings.charts.optionLabels.isStackedLabelsReversed',
    default: defaultChartSettings.isStackedLabelsReversed
  },
  isMeanProLabelsShown: {
    type: 'switch',
    name: 'isMeanProLabelsShown',
    label: 'settings.charts.optionLabels.isMeanProLabelsShown',
    default: chart => chart.chart_type !== 'polar'
  },
  isMeanProProductShown: {
    type: 'switch',
    name: 'isMeanProProductShown',
    label: 'settings.charts.optionLabels.isMeanProProductShown',
    default: defaultChartSettings.isMeanProProductShown
  },
  isMeanProProductShown_howManyDecimals: {
    type: 'dropdown',
    options: [
      { name: '0', value: 0 },
      { name: '1', value: 1 },
      { name: '2', value: 2 },
      { name: '3', value: 3 },
      { name: '4', value: 4 },
      { name: '5', value: 5 }
    ],
    name: 'isMeanProProductShown_howManyDecimals',
    label: 'settings.charts.optionLabels.howManyDecimals',
    placeholder: 'placeholders.chartsSettings.howManyDecimals',
    showIf: settings => settings.isMeanProProductShown,
    default: defaultChartSettings.isMeanProProductShown_howManyDecimals
  },
  isLabelsShown: {
    type: 'switch',
    name: 'isLabelsShown',
    label: 'settings.charts.optionLabels.isLabelsShown',
    default: defaultChartSettings.isLabelsShown
  },
  isProductLabelsShown: {
    type: 'switch',
    name: 'isProductLabelsShown',
    label: 'settings.charts.optionLabels.isProductLabelsShown',
    default: false,
    showIf: settings => settings.isProductsShown
  },
  isAttributeLabelsShown: {
    type: 'switch',
    name: 'isAttributeLabelsShown',
    label: 'settings.charts.optionLabels.isAttributeLabelsShown',
    default: false,
    showIf: settings => settings.isVectorsShown
  },
  isProductsShown: {
    type: 'switch',
    name: 'isProductsShown',
    label: 'settings.charts.optionLabels.isProductsShown',
    default: true
  },
  isVectorsShown: {
    type: 'switch',
    name: 'isVectorsShown',
    label: 'settings.charts.optionLabels.isVectorsShown',
    default: true
  },
  isVectorsShown_size: {
    type: 'dropdown',
    options: [
      { name: '50%', value: 0.5 },
      { name: '100%', value: 1 },
      { name: '150%', value: 1.5 },
      { name: '200%', value: 2 },
      { name: '250%', value: 2.5 },
      { name: '300%', value: 3 }
    ],
    name: 'isVectorsShown_size',
    label: 'settings.charts.optionLabels.isVectorsShown_size',
    placeholder: 'placeholders.chartsSettings.isVectorsShown_size',
    default: 1,
    showIf: settings => settings.isVectorsShown
  },
  sortOrder: {
    type: 'dropdown',
    options: [
      {
        name: 'settings.charts.optionLabels.sortOrder.valueAsc',
        value: 'value.asc'
      },
      {
        name: 'settings.charts.optionLabels.sortOrder.valueDesc',
        value: 'value.desc'
      },
      {
        name: 'settings.charts.optionLabels.sortOrder.nameAsc',
        value: 'label.asc'
      },
      {
        name: 'settings.charts.optionLabels.sortOrder.nameDesc',
        value: 'label.desc'
      }
    ],
    name: 'sortOrder',
    label: 'settings.charts.optionLabels.barsSortOrder',
    placeholder: 'placeholders.chartsSettings.barsSortOrder',
    default: defaultChartSettings.sortOrder
  },
  isDataTableShown: {
    type: 'switch',
    name: 'isDataTableShown',
    label: 'settings.charts.optionLabels.isDataTableShown',
    default: defaultChartSettings.isDataTableShown
  },
  isDataTableShown_format: {
    type: 'dropdown',
    options: [
      { name: 'settings.charts.optionLabels.dataformat.abs', value: 'abs' },
      { name: 'settings.charts.optionLabels.dataformat.perc', value: 'perc' },
      {
        name: 'settings.charts.optionLabels.dataformat.perc_abs',
        value: 'perc_abs'
      }
    ],
    name: 'isDataTableShown_format',
    label: 'settings.charts.optionLabels.isDataTableShown_format',
    placeholder: 'placeholders.chartsSettings.isDataTableShown_format',
    showIf: settings => settings.isDataTableShown,
    default: defaultChartSettings.isDataTableShown_format
  },
  isDataTableShown_isPaginationEnabled: {
    type: 'switch',
    name: 'isDataTableShown_isPaginationEnabled',
    label: 'settings.charts.optionLabels.isDataTableShown_isPaginationEnabled',
    tooltip:
      'settings.charts.optionTooltips.isDataTableShown_isPaginationEnabled',
    showIf: settings => settings.isDataTableShown,
    default: defaultChartSettings.isDataTableShown_isPaginationEnabled
  },
  hasOutline: {
    type: 'switch',
    name: 'hasOutline',
    label: 'settings.charts.optionLabels.hasOutline',
    default: defaultChartSettings.hasOutline
  },
  hasOutline_width: {
    type: 'dropdown',
    options: [
      { name: '1', value: 1 },
      { name: '2', value: 2 },
      { name: '3', value: 3 },
      { name: '4', value: 4 }
    ],
    name: 'hasOutline_width',
    label: 'settings.charts.optionLabels.outlineWidth',
    placeholder: 'placeholders.chartsSettings.outlineWidth',
    showIf: settings => settings.hasOutline,
    default: defaultChartSettings.hasOutline_width
  },
  hasOutline_color: {
    type: 'color-picker',
    name: 'hasOutline_color',
    label: 'settings.charts.optionLabels.outlineColor',
    default: [defaultChartSettings.outlineColor],
    showIf: settings => settings.hasOutline,
    colorLimit: 1
  },
  groupLabels: {
    type: 'group-labels',
    name: 'groupLabels',
    label: 'settings.charts.optionLabels.groupLabels',
    default: chart => chart.labels.map(label => [label])
  },
  colorsArr: {
    type: 'color-picker',
    name: 'colorsArr',
    label: 'settings.charts.optionLabels.generalColorPicker',
    default: [...defaultChartSettings.colorsArr]
  },
  colorsArr_map: {
    type: 'color-picker',
    name: 'colorsArr_map',
    label: 'settings.charts.optionLabels.mapColorPicker',
    default: [...defaultChartSettings.mapChartColorsArr]
  },
  singleColor: {
    type: 'color-picker',
    name: 'singleColor',
    label: 'settings.charts.optionLabels.timestampColorPicker',
    default: [defaultChartSettings.outlineColor],
    colorLimit: 1
  }
}
