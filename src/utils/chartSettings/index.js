import { settings } from './settings'
import { clone } from 'ramda'

export const getAvailableChartSettings = ({
  chartType,
  questionType,
  hasProducts
}) => {
  const chartSettings = []

  // Settings for all charts
  chartSettings.push(settings.isTotalRespondentsShown, settings.howManyDecimals)

  // Append middle section of settings. Very variative depending on chart / question / survey section
  switch (chartType) {
    case 'polar':
      chartSettings.push(
        settings.divider,
        settings.isScaleShown,
        settings.howManyDecimals_axis
      )

      if (questionType === 'paired-questions') {
        chartSettings.push(settings.scaleRange)
      }
      break
    case 'pie':
      chartSettings.push(settings.divider, settings.valueType)
      break
    case 'map':
      chartSettings.push(settings.isLabelsShown)
      break
    case 'line':
      chartSettings.push(settings.divider, settings.valueType, {
        ...settings.isCountProLabelShown,
        default: true
      })
      break
    case 'columns-mean':
    case 'horizontal-bars-mean':
      chartSettings.push(
        settings.divider,
        settings.isCountProLabelShown,
        settings.isChartGridHidden
      )
      if (hasProducts) {
        chartSettings.push(settings.sortOrder)
      }
      break
    case 'bar':
    case 'column':
      if (!['paired-questions', 'slider'].includes(questionType)) {
        chartSettings.push(settings.divider, settings.valueType)
        if (hasProducts || questionType === 'matrix')
          chartSettings.push(settings.percentageGrouping)
      }
      chartSettings.push(
        settings.divider,
        settings.isCountProLabelShown,
        settings.isChartGridHidden
      )
      if (
        chartType !== 'bar' &&
        (hasProducts || questionType === 'matrix') &&
        !['paired-questions', 'slider'].includes(questionType)
      ) {
        chartSettings.push(settings.isCountProGroupShown)
      }
      if (hasProducts || questionType === 'matrix') {
        chartSettings.push(settings.isProductLabelsSwapped)
      }
      break
    case 'stacked-bar':
    case 'stacked-column':
      chartSettings.push(
        settings.divider,
        settings.isCountProLabelShown,
        settings.isStackedLabelsReversed,
        settings.isChartGridHidden,
        settings.isTotalRespondentsProProductShown
      )
      break
    case 'stacked-column-horizontal-bars':
      chartSettings.push(
        settings.divider,
        settings.isCountProLabelShown,
        settings.isStackedLabelsReversed,
        settings.isMeanProLabelsShown,
        settings.sortOrder,
        settings.isChartGridHidden,
        settings.isTotalRespondentsProProductShown
      )
      break
    case 'all':
      chartSettings.push(
        settings.divider,
        settings.isCountProLabelShown,
        settings.isCountProGroupShown,
        settings.isMeanProLabelsShown,
        settings.isProductLabelsSwapped,
        settings.isStackedLabelsReversed,
        settings.sortOrder,
        settings.isChartGridHidden
      )
      break
    default:
      break
  }
  if (
    [
      'stacked-column-horizontal-bars',
      'stacked-column',
      'column',
      'columns-mean'
    ].includes(chartType)
  ) {
    chartSettings.push(settings.chartWidth)
  }
  if (
    ['stacked-column-horizontal-bars', 'stacked-column'].includes(chartType)
  ) {
    chartSettings.push(
      settings.isMeanProProductShown,
      settings.isMeanProProductShown_howManyDecimals
    )
  }

  if (
    !['table', 'spearmann-table', 'pearson-table', 'tags-list'].includes(
      chartType
    )
  ) {
    // Append font style section
    chartSettings.push(
      settings.divider,
      settings.fontSize_labels,
      settings.fontFamily_labels
    )
    switch (chartType) {
      case 'polar':
        chartSettings.push(settings.fontSize_axis, settings.fontFamily_axis)
        if (hasProducts) {
          chartSettings.push(
            settings.fontSize_legend,
            settings.fontFamily_legend
          )
        }
        break
      case 'bar':
      case 'column':
        chartSettings.push(
          settings.fontSize_axis,
          settings.fontFamily_axis,
          settings.fontSize_values,
          settings.fontFamily_values
        )
        if (hasProducts || questionType === 'matrix') {
          chartSettings.push(
            settings.fontSize_legend,
            settings.fontFamily_legend
          )
        }
        break
      case 'stacked-column':
      case 'stacked-bar':
      case 'stacked-column-horizontal-bars':
        chartSettings.push(
          settings.fontSize_axis,
          settings.fontFamily_axis,
          settings.fontSize_legend,
          settings.fontFamily_legend,
          settings.fontSize_values,
          settings.fontFamily_values
        )
        break
      case 'columns-mean':
      case 'line':
      case 'horizontal-bars-mean':
        chartSettings.push(
          settings.fontSize_axis,
          settings.fontFamily_axis,
          settings.fontSize_values,
          settings.fontFamily_values
        )
        break
      case 'pie':
        chartSettings.push(settings.fontSize_legend, settings.fontFamily_legend)
        break
      case 'all':
        chartSettings.push(
          settings.fontSize_axis,
          settings.fontFamily_axis,
          settings.fontSize_legend,
          settings.fontFamily_legend,
          settings.fontSize_values,
          settings.fontFamily_values
        )
        break
      default:
        break
    }

    // Append data table settings
    // In proposal: extract data table in separate type of chart
    chartSettings.push(settings.divider, settings.isDataTableShown)
    chartType !== 'map' &&
      chartSettings.push(settings.isDataTableShown_isPaginationEnabled)
  }
  switch (chartType) {
    case 'column':
    case 'bar':
    case 'stacked-column':
    case 'stacked-bar':
    case 'stacked-column-horizontal-bars':
      if (
        !['paired-questions', 'slider'].includes(questionType) ||
        (questionType === 'slider' && chartType === 'stacked-bar')
      ) {
        const dataFormatOptions = clone(
          settings.isDataTableShown_format.options
        )
        if (hasProducts || questionType === 'matrix') {
          dataFormatOptions.push(
            {
              name: 'settings.charts.optionLabels.dataformat.perc_abs_teh',
              value: 'perc_abs_teh'
            },
            {
              name: 'settings.charts.optionLabels.dataformat.perc_prod',
              value: 'perc_prod'
            },
            {
              name: 'settings.charts.optionLabels.dataformat.perc_abs_prod',
              value: 'perc_abs_prod'
            }
          )
        }
        chartSettings.push({
          ...settings.isDataTableShown_format,
          options: dataFormatOptions
        })
      }
      break
    case 'map':
    case 'line':
    case 'pie':
    case 'all':
      chartSettings.push(settings.isDataTableShown_format)
      break
    default:
      break
  }

  // Outline settings
  if (
    [
      'stacked-column',
      'stacked-column-horizontal-bars',
      'stacked-bar',
      'horizontal-bars-mean',
      'column',
      'columns-mean',
      'bar',
      'all'
    ].includes(chartType)
  ) {
    chartSettings.push(
      settings.divider,
      settings.hasOutline,
      settings.hasOutline_width,
      settings.hasOutline_color
    )
  }

  // Grouping settings
  if (
    ([
      'stacked-column-horizontal-bars',
      'column',
      'stacked-column',
      'stacked-bar',
      'pie'
    ].includes(chartType) &&
      !['paired-questions', 'slider'].includes(questionType)) ||
    (chartType === 'stacked-bar' && questionType === 'slider')
  ) {
    chartSettings.push(settings.divider, settings.groupLabels)
  }

  // Color pickers
  switch (chartType) {
    case 'pie':
    case 'polar':
    case 'stacked-column':
    case 'stacked-bar':
    case 'horizontal-bars-mean':
    case 'column':
    case 'columns-mean':
    case 'bar':
      chartSettings.push(settings.divider, settings.colorsArr)
      break
    case 'map':
      chartSettings.push(settings.divider, settings.colorsArr_map)
      break
    case 'line':
      chartSettings.push(settings.divider, settings.singleColor)
      break
    case 'stacked-column-horizontal-bars':
      chartSettings.push(
        settings.divider,
        settings.colorsArr,
        settings.singleColor
      )
      break
    case 'all':
      chartSettings.push(
        settings.divider,
        settings.colorsArr,
        settings.colorsArr_map,
        settings.singleColor
      )
      break
    default:
      break
  }

  return chartSettings
}

export const getAvailableStatisticSettings = statisticName => {
  const statisticSettings = []
  switch (statisticName) {
    case 'pca':
      statisticSettings.push(
        settings.isProductsShown,
        settings.isProductLabelsShown,
        settings.divider,
        settings.isVectorsShown,
        settings.isAttributeLabelsShown,
        settings.isVectorsShown_size
      )
      break
    case 'penalty':
      statisticSettings.push(
        settings.fontFamily_legend,
        settings.fontSize_legend
      )
      break
    default:
      // no settings
      break
  }
  return statisticSettings
}

export const getDefaultGlobalChartSettings = () => {
  const globalSettings = {}

  Object.keys(settings).forEach(settingName => {
    const settingObj = settings[settingName]
    if (settingObj.type === 'divider') {
      return
    }
    if (['groupLabels'].includes(settingObj.name)) {
      return
    }
    globalSettings[settingName] =
      typeof settingObj.default === 'function'
        ? settingObj.default({})
        : settingObj.default || false
  })

  return globalSettings
}

// Returns settings for all charts
export const getDefaultChartSettings = stats => {
  const defaultSettings = {}

  stats.tabs.forEach(tab => {
    tab.charts.forEach(chart => {
      const chartDefaultSettings = getDefaultChartSettingsByChart(chart)

      defaultSettings[
        `${chart.question}-${chart.chart_type}`
      ] = chartDefaultSettings

      if (chart.statistic) {
        Object.keys(chart.statistic).forEach(statistic => {
          if (['pca', 'penalty'].includes(statistic)) {
            defaultSettings[
              `${chart.question}-${statistic}`
            ] = getDefaultChartSettingsByStatistic(statistic)
          }
        })
      }
    })
  })

  defaultSettings['global'] = getDefaultGlobalChartSettings()

  return defaultSettings
}

export const getDefaultChartSettingsByChart = chart => {
  const availableSettings = getAvailableChartSettings({
    chartType: chart.chart_type,
    questionType: chart.question_type,
    hasProducts: chart.question_with_products
  })

  const chartDefaultSettings = {}

  availableSettings.forEach(setting => {
    chartDefaultSettings[setting.name] =
      typeof setting.default === 'function'
        ? setting.default(chart)
        : setting.default || false
  })

  return chartDefaultSettings
}

export const getDefaultChartSettingsByStatistic = statistic => {
  const availableSettings = getAvailableStatisticSettings(statistic)

  const chartDefaultSettings = {}

  availableSettings.forEach(setting => {
    chartDefaultSettings[setting.name] = setting.default || false
  })

  return chartDefaultSettings
}
