const { REACT_APP_THEME } = process.env
const rowSize = () => {
  const sizeValue =
    REACT_APP_THEME !== 'default'
      ? { spanValue: 16, fieldValue: 12 }
      : { spanValue: 24, fieldValue: 8 }
  return sizeValue
}

export default rowSize;