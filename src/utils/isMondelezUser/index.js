const mondelezRegex = /(@mdlz\.com)$/g

const isMondelezUser = user =>
  user && user.emailAddress && user.emailAddress.search(mondelezRegex) > 0

export default isMondelezUser
