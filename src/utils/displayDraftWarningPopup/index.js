import { message as AntMessage } from 'antd'

const message = `This is only a preview of the survey. 
Please do not distribute the link.
All responses will be deleted when the survey will be published.`

// AntMessage.config({
//   maxCount: 1
// })

export const displayDraftWarningPopup = () => {
  AntMessage.destroy()
  AntMessage.warning(message, 0)
}

export const destroyDraftWarningPopup = () => {
  AntMessage.destroy()
}
