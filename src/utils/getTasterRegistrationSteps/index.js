import gql from 'graphql-tag'

export const getTasterRegistrationSteps = (loggedInUser = {}, t) => {
  return [
    {
      title: t('components.createTasterAccount.steps.first.title'),
      description: t('components.createTasterAccount.steps.first.description'),
      path: 'welcome',
      isValid: !!loggedInUser,
      formData: {
        emailAddress: loggedInUser ? loggedInUser.emailAddress : '',
        country: loggedInUser ? loggedInUser.country : '',
        state: loggedInUser ? loggedInUser.state : '',
        language: loggedInUser ? loggedInUser.language : '',
      },
      okText: t('components.createTasterAccount.steps.first.okText'),
      redirectToLogin: true,
      mutation: gql`
        mutation createTasterAccount($input: CreateTasterAccountInput) {
          createTasterAccount(input: $input) {
            token
            user {
              id
              emailAddress
              country
              state
              language
              type
              isDummyTaster
            }
          }
        }
      `
    },
    {
      title: 'components.createTasterAccount.steps.second.title',
      description: 'components.createTasterAccount.steps.second.description',
      path: 'personal-information',
      isValid:
        loggedInUser &&
        loggedInUser.fullName &&
        loggedInUser.gender &&
        loggedInUser.dateofbirth,
      formData: {
        firstName:
          loggedInUser &&
          loggedInUser.fullName &&
          loggedInUser.emailAddress !== loggedInUser.fullName
            ? loggedInUser.fullName.split(' ')[0]
            : '',
        lastName:
          loggedInUser && loggedInUser.fullName
            ? loggedInUser.fullName.split(' ')[1]
            : '',
        gender: loggedInUser ? loggedInUser.gender : '',
        dateofbirth: loggedInUser ? loggedInUser.dateofbirth : '',
        paypalEmailAddress: loggedInUser ? loggedInUser.paypalEmailAddress : '',
        isProfileCompleted: loggedInUser
          ? loggedInUser.isProfileCompleted
          : false
      },
      okText: 'components.createTasterAccount.steps.second.okText',
      mutation: gql`
        mutation updateTasterAccount($input: UpdateTasterAccountInput) {
          updateTasterAccount(input: $input) {
            fullName
            gender
            dateofbirth
            country
            state
            language
            paypalEmailAddress
          }
        }
      `
    },
    {
      title: 'components.createTasterAccount.steps.third.title',
      description: 'components.createTasterAccount.steps.third.description',
      path: 'additional-information',
      isValid:
        loggedInUser &&
        // loggedInUser.ethnicity &&
        // loggedInUser.incomeRange &&
        loggedInUser.hasChildren &&
        loggedInUser.smokerKind &&
        loggedInUser.foodAllergies &&
        loggedInUser.marketResearchParticipation,
      // loggedInUser.paypalEmailAddress,
      formData: {
        ethnicity: loggedInUser ? loggedInUser.ethnicity : '',
        incomeRange: loggedInUser ? loggedInUser.incomeRange : '',
        hasChildren: loggedInUser ? loggedInUser.hasChildren : '',
        numberOfChildren: loggedInUser && loggedInUser.numberOfChildren ? loggedInUser.numberOfChildren : '0',
        childrenAges: loggedInUser ? loggedInUser.childrenAges : {},
        smokerKind: loggedInUser ? loggedInUser.smokerKind : '',
        foodAllergies: loggedInUser ? loggedInUser.foodAllergies : '',
        marketResearchParticipation: loggedInUser
          ? loggedInUser.marketResearchParticipation
          : '',
        isProfileCompleted: loggedInUser
          ? loggedInUser.isProfileCompleted
          : false
      },
      okText: 'components.createTasterAccount.steps.third.okText',
      mutation: gql`
        mutation updateTasterAccount($input: UpdateTasterAccountInput) {
          updateTasterAccount(input: $input) {
            dateofbirth
            country
            ethnicity
            incomeRange
            hasChildren
            numberOfChildren
            childrenAges
            smokerKind
            foodAllergies
            marketResearchParticipation
            paypalEmailAddress
          }
        }
      `
    }
  ]
}

export const getStartingStep = steps => {
  for (let stepCount = 0; stepCount < steps.length; stepCount++) {
    let currentStep = steps[stepCount]
    if (!currentStep.isValid) {
      return currentStep.path
    }
  }
  return false
}
