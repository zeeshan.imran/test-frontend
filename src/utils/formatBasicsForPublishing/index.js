import { pickBy, isNil, omit } from 'ramda'
import camelCase from 'lodash.camelcase'

const withoutTypename = omit(['__typename'])

const formatBasicsForPublishing = ({ basics, products }) => {
  const correctedMinumumProducts = Math.min(
    products.length,
    basics.minimumProducts || 1
  )
  const formattedCustomButtons = pickBy(
    (val, key) => key !== '__typename' && !isNil(val),
    basics.customButtons || {}
  )
  const formattedExclusiveTasters =
    basics.authorizationType === 'selected' ? basics.exclusiveTasters : []

  const formattedUniqueName =
    basics.uniqueName && basics.uniqueName.replace(/\s/g, '')

  const formatedProductRewardsRule = pickBy(
    (val, key) => key !== '__typename' && !isNil(val),
    basics.productRewardsRule || {}
  )

  const formattedBasics = pickBy(
    (val, key) => key !== '__typename' && !isNil(val),
    {
      ...basics,
      minimumProducts: correctedMinumumProducts,
      maximumProducts: parseInt(basics.maximumProducts, 10) || products.length,
      exclusiveTasters: formattedExclusiveTasters,
      uniqueName: formattedUniqueName,
      productRewardsRule: formatedProductRewardsRule
    }
  )

  const { enabledEmailTypes, emails } = basics

  return {
    ...formattedBasics,
    customButtons: formattedCustomButtons,
    enabledEmailTypes,
    emails: enabledEmailTypes
      .map(camelCase)
      .reduce(
        (acc, type) => ({ ...acc, [type]: withoutTypename(emails[type]) }),
        {}
      )
  }
}

export default formatBasicsForPublishing
