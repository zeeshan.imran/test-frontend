import * as Yup from 'yup'
import PolarChart from '../components/Charts/PolarChart'
import PieChart from '../components/Charts/PieChart'
import LineChart from '../components/Charts/LineChart'
import StackedColumnsChart from '../components/Charts/StackedColumnsChart'
import StackedBarChart from '../components/Charts/StackedBarChart'
import BarChart from '../components/Charts/BarChart'
import MultiChart from '../components/Charts/MultiChart'
import ColumnsChart from '../components/Charts/ColumnsChart'
import ColumnsChartMean from '../components/Charts/ColumnsChartMean'
import MapChart from '../components/Charts/MapChart'
import TableChart from '../components/Charts/TableChart'
import TagsList from '../components/Charts/TagsList'
import {
  lineChartInputSchema,
  defaultChartSchema,
  defaultChartSchemaGoupedByProducts,
  defaultWithAvgChartSchema,
  defaultWithCountryGeoData,
  polarChartInputSchema,
  tableSchema
} from '../validates/chartsResults'

export const CHART_SIZES = {
  DEFAULT: 24,
  '': 12,
  pie: 12
}

export const CARD_WIDTH = {
  DEFAULT: 'full',
  '': 'half',
  pie: 'half'
}

const CHART_COMPONENTS = {
  DEFAULT: PieChart,
  pie: PieChart,
  map: MapChart,
  'stacked-column': StackedColumnsChart,
  'stacked-column-horizontal-bars': StackedColumnsChart,
  column: ColumnsChart,
  'columns-mean': ColumnsChartMean,
  polar: PolarChart,
  line: LineChart,
  table: TableChart,
  'spearmann-table': TableChart,
  'pearson-table': TableChart,
  'tags-list': TagsList,
  bar: BarChart,
  'horizontal-bars-mean': StackedBarChart,
  'stacked-bar': StackedBarChart
}

const CHART_SCHEMAS = {
  DEFAULT: defaultChartSchema,
  map: defaultWithCountryGeoData,
  'stacked-column-horizontal-bars': defaultWithAvgChartSchema,
  'stacked-column': defaultWithAvgChartSchema,
  'horizontal-bars-mean': defaultWithAvgChartSchema,
  polar: polarChartInputSchema,
  line: lineChartInputSchema,
  table: tableSchema,
  'spearmann-table': tableSchema,
  'pearson-table': tableSchema,
  'tags-list': Yup.mixed()
}

export const getChartSchema = el => {
  if (isMultiChart(el) && el.chart_type !== 'bar') {
    return defaultChartSchemaGoupedByProducts
  }
  return CHART_SCHEMAS[el.chart_type] || CHART_SCHEMAS.DEFAULT
}

// Maintain in-sync with statsapi chartUtils function
export const isMultiChart = el =>
  el.question_with_products &&
  ((el.question_type === 'slider' && ['stacked-bar'].includes(el.chart_type)) ||
    (el.question_type === 'matrix' &&
      ['column', 'stacked-bar', 'stacked-column'].includes(el.chart_type)))

export const getChartComponent = (el, isSingleChartComponentForced) => {
  if (!isSingleChartComponentForced && isMultiChart(el)) {
    return MultiChart
  }
  return CHART_COMPONENTS[el.chart_type] || CHART_COMPONENTS.DEFAULT
}
