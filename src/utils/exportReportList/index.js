export const exportReportList = (isFlavorwiki = false) => {
  const exportArray = [
    {
      id: 'stackedColumn',
      name: 'All Answers Stacked Only'
    },
    {
      id: 'admLike',
      name: 'Liking + Pair Calc'
    },
    {
      id: 'generalQuestionsReport',
      name: 'All Answers Rows'
    },
    {
      id: 'generalQuestionsReportLite',
      name: 'All Answers Rows (Lite)'
    },
    {
      id: 'generalQuestionsReportOpen',
      name: 'All Answers Rows (Open Answers)'
    },
    {
      id: 'generalQuestionsReportPivot',
      name: 'All Answers Rows (Pivots)'
    },
    {
      id: 'questionLegends',
      name: 'Question Legends'
    },
    {
      id: 'answerAndProfileReport',
      name: 'Answers + Individual Pair Calc'
    },
    {
      id: 'permutationReport',
      name: 'Permutation Report'
    }
  ]
  if (isFlavorwiki) {
    exportArray.push({
      id: 'generalQuestionsReportTimings',
      name: 'All Answers Rows (Timings)'
    })
    exportArray.push({
      id: 'photoValidationReport',
      name: 'Photo Validation Report'
    })
  }
  return exportArray
}
