import skipFlowUtils from '../skipFlowUtils'

export default (question, answerValue) => {
  if (question.skipFlow) {
    switch (question.skipFlow.type) {
      case 'MATCH_RANGE': {
        const numericValue = parseFloat(answerValue)
        return skipFlowUtils
          .fromSkipFlow(question.skipFlow)
          .findRule(
            rule =>
              rule.minValue <= numericValue && numericValue <= rule.maxValue
          )
      }
      case 'MATCH_OPTION': {
        const optionIndex = question.options.findIndex(
          op => op.value === answerValue
        )
        return skipFlowUtils
          .fromSkipFlow(question.skipFlow)
          .findRule(rule => rule.index === optionIndex)
      }
      case 'MATCH_PRODUCT': {
        const productId = answerValue
        return skipFlowUtils
          .fromSkipFlow(question.skipFlow)
          .findRule(rule => rule.productId === productId)
      }
      case 'MATCH_MULTIPLE_OPTION': {
        return skipFlowUtils
          .fromSkipFlow(question.skipFlow)
          .findMultipleRule(answerValue)
      }
      case 'MATCH_MATRIX_OPTION': {
        const matrixRule = skipFlowUtils
          .fromSkipFlow(question.skipFlow)
          .findMatrixRule(answerValue, question)
        if (
          matrixRule &&
          matrixRule.skipTo &&
          matrixRule.skipTo === 'continue'
        ) {
          return false
        }
        return matrixRule
      }
      default:
        break
    }
  }
}
