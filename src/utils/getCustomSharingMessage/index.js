import ReactDOMServer from 'react-dom/server'

export const getMessage = (tkey, t) => {
  return t(tkey, {
    amount: ReactDOMServer.renderToString('{{amount}}')
  })
}
