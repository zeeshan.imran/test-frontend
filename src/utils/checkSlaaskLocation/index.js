const { REACT_APP_SLAASK } = process.env
const disabled = REACT_APP_SLAASK === 'disabled'

const checkSlaaskLocation = location => {
  const { pathname } = location
  let substrings = [
    'taster-pdf',
    'stats-pdf',
    'preview-operator-pdf',
    'preview-taster-pdf',
    'operator'
  ]
  let flaskLocationFlag = false
  if (substrings.some(v => pathname.includes(v))) {
    flaskLocationFlag = true
  } else if (disabled) {
    flaskLocationFlag = true
  }
  return flaskLocationFlag
}

export default checkSlaaskLocation
