import * as d3 from 'd3'
import { clone, sortBy, prop } from 'ramda'
import { TOOLTIP_LAYER_ID } from '../chartUtils'

export const appendChartLegend = (
  container,
  labels,
  colorsScale,
  clickCallback,
  padded
) => {
  let labelsState = labels
  const legendContainer = container.append('div').attr('class', 'chart-legend')
  legendContainer
    .classed('padded', padded)
    .selectAll('.legend')
    .data(labels)
    .enter()
    .append('div')
    .attr('class', 'legend')
    .each(function (d) {
      d3.select(this)
        .append('div')
        .attr('class', 'legend-marker')
        .style('background', colorsScale(d.index))
      d3.select(this)
        .append('span')
        .text(d.name)
    })
    .on('click', (d, i) => {
      if (labelsState.filter(el => el.isSelected).length === labels.length) {
        labelsState.forEach(el => (el.isSelected = false))
        labelsState[i].isSelected = true
      } else {
        labelsState[i].isSelected = !labelsState[i].isSelected
      }
      if (labelsState.filter(el => el.isSelected).length === 0) {
        labelsState.forEach(el => (el.isSelected = true))
      }
      legendContainer.selectAll('.legend').each(function (d, i) {
        d3.select(this).classed('faded-legend', !labelsState[i].isSelected)
      })
      clickCallback(clone(labelsState))
    })
}

export const updateChartLegendColorScheme = (container, colorsScale) => {
  const legendContainer = container.select('.chart-legend')
  legendContainer.selectAll('.legend').each(function (d) {
    d3.select(this)
      .select('.legend-marker')
      .style('background', colorsScale(d.index))
  })
}

const createTooltipLayer = () => {
  let currentTooltip

  const showTooltip = data => {
    const tooltipLayer = d3.select('#' + TOOLTIP_LAYER_ID)
    const event = d3.event
    tooltipLayer.selectAll('.tooltip-container').remove()
    currentTooltip = tooltipLayer
      .append('div')
      .attr('class', 'tooltip-container')
      .style('position', 'fixed')
      .style('left', event.clientX + 'px')
      .style('top', event.clientY + 14 + 'px')

    data.title &&
      currentTooltip
        .append('div')
        .attr('class', 'tooltip-title')
        .text(data.title)
        .style('color', data.titleColor)

    if (data.results) {
      currentTooltip
        .selectAll('.tooltip-result')
        .data(data.results)
        .enter()
        .append('div')
        .attr('class', 'tooltip-row tooltip-result')
        .each(function (d) {
          d3.select(this)
            .append('span')
            .text(d.label + ': ')
            .style('color', d.color || '')
          d3.select(this)
            .append('span')
            .text(d.value)
        })
    }
  }

  const updateTooltip = () => {
    currentTooltip &&
      currentTooltip
        .style('left', d3.event.clientX + 'px')
        .style('top', d3.event.clientY + 14 + 'px')
  }

  const clearTooltip = () => {
    d3.select('#' + TOOLTIP_LAYER_ID)
      .select('.tooltip-container')
      .remove()
  }

  return { showTooltip, updateTooltip, clearTooltip }
}

export const getStackedTooltip = ({
  inputData,
  d,
  colorsStackedChart,
  decimals,
  t
}) => {
  return {
    title:
      inputData.question_with_products || inputData.question_type === 'matrix'
        ? d.data._metadata.name
        : t('charts.axis.stackedAnswerName'),
    results:
      inputData.question_with_products || inputData.question_type === 'matrix'
        ? inputData.results[d.data._metadata.productIndex].data.map(el => ({
            ...el,
            color: colorsStackedChart(el.label),
            value: `${el.value} (${(
              (100 * el.value) /
              d.data._metadata.summ
            ).toFixed(decimals)}%)`
          }))
        : inputData.results.data.map(el => ({
            ...el,
            color: colorsStackedChart(el.label),
            value: `${el.value} (${(
              (100 * el.value) /
              d.data._metadata.summ
            ).toFixed(decimals)}%)`
          }))
  }
}

export const tooltipLayer = createTooltipLayer()

export const hasCollision = (bbox1, bbox2) => {
  return (
    bbox2.top <= bbox1.top + bbox1.height &&
    bbox2.top + bbox2.height >= bbox1.top &&
    bbox2.left <= bbox1.left + bbox1.width &&
    bbox2.left + bbox2.width >= bbox1.left
  )
}

export const sortResultByAnalyticalValue = (inputData) => {
  if(inputData && inputData.results && inputData.results.data) {
    inputData.results.data = sortBy(prop('analytics_value'), inputData.results.data)
  }
  return inputData
}
