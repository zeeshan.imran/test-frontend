import formatChooseProductOptions from '.'

describe('formatChooseProductOptions', () => {
  test('formatChooseProductOptions to be undefined', () => {
    expect(formatChooseProductOptions()).toBe(undefined)
  })

  test('formatChooseProductOptions tobe false', () => {
    expect(formatChooseProductOptions(false)).toBe(null)
  })

  test('formatChooseProductOptions tobe false', () => {
    const options = {
      minimumProducts: 1,
      maximumProducts: 2
    }
    expect(formatChooseProductOptions(options)).toMatchObject({
      maximumProducts: 2,
      minimumProducts: 1
    })
  })
})
