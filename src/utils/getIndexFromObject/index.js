import { findIndex, propEq } from 'ramda'

export const getIndexFromObject = (key, data, index) => {
  return findIndex(propEq(index, key))(data)
}
