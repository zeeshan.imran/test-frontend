import { getRoutesArray } from '.'

describe('getRoutesArray', () => {
  test('should render getRoutesArray', () => {
    expect(
      getRoutesArray({
        pathname: '/survey/complete'
      })
    ).toMatchObject([
      { breadcrumbName: 'Survey', path: 'survey' },
      { breadcrumbName: 'Complete', path: 'complete' }
    ])
  })
})
