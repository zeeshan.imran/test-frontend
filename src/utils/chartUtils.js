import colors from './Colors'
import strToPath from './strToPath'
export const NOT_PRODUCT = 'not_product_constant'

export const DISABLED_COLOR = colors.LIGHT_GREY

export const TOOLTIP_LAYER_ID = 'tooltip-layer-charts'

const tabIdGenerators = () => {
  const nameToId = {}
  const usedIds = []
  function tryToAddId (name, id) {
    if (!usedIds.includes(id)) {
      nameToId[name] = id
      usedIds.push(id)
      return id
    }

    return null
  }

  return name => {
    if (nameToId[name]) {
      return nameToId[name]
    }

    const baseId = strToPath(name) || encodeURIComponent(name)
    if (tryToAddId(name, baseId) !== null) {
      return nameToId[name]
    }

    for (let i = 1; i < 100; i++) {
      if (tryToAddId(name, `${baseId}-${i}`) !== null) {
        return nameToId[name]
      }
    }
  }
}

export const getTabPages = stats => {
  const generate = tabIdGenerators()
  const tabPages = []
  for (let i = 0; i < stats.tabs.length; i++) {
    tabPages.push({
      name: stats.tabs[i].title,
      count: stats.tabs[i].completedCount,
      path: generate(stats.tabs[i].title)
    })
  }
  return tabPages
}
export const getChartTabsContent = stats => {
  const tabsContent = []
  for (let i = 0; i < stats.tabs.length; i++) {
    tabsContent.push(stats.tabs[i].charts)
  }
  return tabsContent
}

export const getChartView = stats => {
  const { tabs } = stats
  let mergedChartData = []
  tabs.forEach(element => {
    if (element.charts.length > 0) {
      element.charts.forEach(chart => {
        mergedChartData.push(chart)
      })
    }
  })

  return mergedChartData
}
