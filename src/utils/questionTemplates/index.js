const fullName = {
  name: 'fullName',
  questions: 'taster-name',
  getData: ({ t }) => ({
    prompt: t('components.questionCreation.templates.name.prompt'),
    secondaryPrompt: t(
      'components.questionCreation.templates.name.secondaryPrompt'
    ),
    settings: {
      answerFormat: 'any',
      minLength: 4,
      maxLength: 100,
      allowSpaces: true
    }
  })
}

const gender = {
  name: 'gender',
  questions: 'choose-one',
  getData: ({ t }) => ({
    prompt: t('components.questionCreation.templates.gender.prompt'),
    secondaryPrompt: t(
      'components.questionCreation.templates.gender.secondaryPrompt'
    ),
    options: [
      {
        label: t('components.questionCreation.templates.gender.female'),
        value: 1
      },
      {
        label: t('components.questionCreation.templates.gender.male'),
        value: 2
      },
      {
        label: t('components.questionCreation.templates.gender.other'),
        value: 3,
        isOpenAnswer: false
      }
    ]
  })
}

const dateofbirth = {
  name: 'dateofbirth',
  questions: 'choose-date',
  getData: ({ t }) => ({
    prompt: t('components.questionCreation.templates.age.prompt'),
    secondaryPrompt: t(
      'components.questionCreation.templates.age.secondaryPrompt'
    )
  })
}

const stateQuestion = {
  name: 'state',
  questions: 'location',
  getData: ({ t, country }) => ({
    prompt: t('components.questionCreation.templates.region.prompt'),
    secondaryPrompt: t(
      'components.questionCreation.templates.region.secondaryPrompt'
    ),
    region: country ? country.toLowerCase() : ''
  })
}


const ethnicity = {
  name: 'ethnicity',
  questions: 'choose-one',
  country: 'FR',
  getData: ({ t }) => ({
    prompt: t('components.questionCreation.templates.etnicity.prompt'),
    secondaryPrompt: t(
      'components.questionCreation.templates.etnicity.secondaryPrompt'
    ),
    options: [
      {
        label: t('components.questionCreation.templates.etnicity.african'),
        value: 1
      },
      {
        label: t('components.questionCreation.templates.etnicity.eastAsian'),
        value: 2
      },
      {
        label: t('components.questionCreation.templates.etnicity.asian'),
        value: 3
      },
      {
        label: t('components.questionCreation.templates.etnicity.hispanic'),
        value: 4
      },
      {
        label: t('components.questionCreation.templates.etnicity.caucasian'),
        value: 5
      },
      {
        label: t('components.questionCreation.templates.etnicity.other'),
        value: 6,
        isOpenAnswer: false
      }
    ]
  })
}

const incomeRange = {
  name: 'incomeRange',
  questions: 'choose-one',
  getData: ({ t, prefix, suffix }) => ({
    prompt: t('components.questionCreation.templates.income.prompt'),
    secondaryPrompt: t(
      'components.questionCreation.templates.income.secondaryPrompt'
    ),
    options: [
      {
        label: t('components.questionCreation.templates.income.<25', {
          prefix,
          suffix
        }),
        value: 1
      },
      {
        label: t('components.questionCreation.templates.income.25-50', {
          prefix,
          suffix
        }),
        value: 2
      },
      {
        label: t('components.questionCreation.templates.income.50-75', {
          prefix,
          suffix
        }),
        value: 3
      },
      {
        label: t('components.questionCreation.templates.income.75-100', {
          prefix,
          suffix
        }),
        value: 4
      },
      {
        label: t('components.questionCreation.templates.income.>100', {
          prefix,
          suffix
        }),
        value: 5,
        isOpenAnswer: false
      }
    ]
  })
}

const maritalStatus = {
  name: 'maritalStatus',
  questions: 'choose-one',
  getData: ({ t }) => ({
    prompt: t('components.questionCreation.templates.mariageStatus.prompt'),
    secondaryPrompt: t(
      'components.questionCreation.templates.mariageStatus.secondaryPrompt'
    ),
    options: [
      {
        label: t('components.questionCreation.templates.mariageStatus.alone'),
        value: 1
      },
      {
        label: t('components.questionCreation.templates.mariageStatus.partner'),
        value: 2
      },
      {
        label: t(
          'components.questionCreation.templates.mariageStatus.partnerAndChild'
        ),
        value: 3
      },
      {
        label: t(
          'components.questionCreation.templates.mariageStatus.partnerAndChildrens'
        ),
        value: 4
      },
      {
        label: t('components.questionCreation.templates.mariageStatus.child'),
        value: 5
      },
      {
        label: t(
          'components.questionCreation.templates.mariageStatus.relatives'
        ),
        value: 6
      },
      {
        label: t('components.questionCreation.templates.mariageStatus.friends'),
        value: 7,
        isOpenAnswer: false
      }
    ]
  })
}

const hasChildren = {
  name: 'hasChildren',
  questions: 'choose-one',
  getData: ({ t }) => ({
    prompt: t('components.questionCreation.templates.childrenStatus.prompt'),
    secondaryPrompt: t(
      'components.questionCreation.templates.childrenStatus.secondaryPrompt'
    ),
    options: [
      {
        label: t('components.questionCreation.templates.childrenStatus.yes'),
        value: 1
      },
      {
        label: t('components.questionCreation.templates.childrenStatus.no'),
        value: 2,
        isOpenAnswer: false
      }
    ]
  })
}

const numberOfChildren = {
  name: 'numberOfChildren',
  questions: 'numeric',
  getData: ({ t }) => ({
    prompt: t('components.questionCreation.templates.childrenAge.prompt'),
    secondaryPrompt: t(
      'components.questionCreation.templates.childrenAge.secondaryPrompt'
    ),
    options: { decimalNumbers: 0 },
    numericOptions: {
      max: 20,
      min: 1
    }
  })
}

const smokerKind = {
  name: 'smokerKind',
  questions: 'choose-one',
  getData: ({ t }) => ({
    prompt: t('components.questionCreation.templates.smoker.prompt'),
    secondaryPrompt: t(
      'components.questionCreation.templates.smoker.secondaryPrompt'
    ),
    options: [
      {
        label: t('components.questionCreation.templates.smoker.yes'),
        value: 1
      },
      {
        label: t('components.questionCreation.templates.smoker.no'),
        value: 2
      }
    ]
  })
}

const foodAllergies = {
  name: 'foodAllergies',
  questions: 'choose-one',
  getData: ({ t }) => ({
    prompt: t('components.questionCreation.templates.hasAllergies.prompt'),
    secondaryPrompt: t(
      'components.questionCreation.templates.hasAllergies.secondaryPrompt'
    ),
    options: [
      {
        label: t('components.questionCreation.templates.hasAllergies.yes'),
        value: 1
      },
      {
        label: t('components.questionCreation.templates.hasAllergies.no'),
        value: 2
      }
    ]
  })
}

// const whatAllergiesAndIntollerances = {
//   name: 'allergies-list',
//   questions: 'choose-multiple',
//   getData: ({ t }) => ({
//     prompt: t(
//       'components.questionCreation.templates.allergiesList.prompt'
//     ),
//     secondaryPrompt: t(
//       'components.questionCreation.templates.allergiesList.secondaryPrompt'
//     ),
//     options: [
//       {
//         label: t(
//           'components.questionCreation.templates.allergiesList.milk'
//         ),
//         value: 1
//       },
//       {
//         label: t(
//           'components.questionCreation.templates.allergiesList.eggs'
//         ),
//         value: 2
//       },
//       {
//         label: t(
//           'components.questionCreation.templates.allergiesList.peanuts'
//         ),
//         value: 3
//       },
//       {
//         label: t(
//           'components.questionCreation.templates.allergiesList.nuts'
//         ),
//         value: 4
//       },
//       {
//         label: t(
//           'components.questionCreation.templates.allergiesList.soy'
//         ),
//         value: 5
//       },
//       {
//         label: t(
//           'components.questionCreation.templates.allergiesList.wheat'
//         ),
//         value: 6
//       },
//       {
//         label: t(
//           'components.questionCreation.templates.allergiesList.fish'
//         ),
//         value: 7
//       },
//       {
//         label: t(
//           'components.questionCreation.templates.allergiesList.shellfish'
//         ),
//         value: 8
//       },
//       {
//         label: t(
//           'components.questionCreation.templates.allergiesList.other'
//         ),
//         value: 9
//       }
//     ]
//   })
// }

// const dietaryPreferences = {
//   name: 'diet',
//   questions: 'choose-multiple',
//   getData: ({ t }) => ({
//     prompt: t('components.questionCreation.templates.diet.prompt'),
//     secondaryPrompt: t(
//       'components.questionCreation.templates.diet.secondaryPrompt'
//     ),
//     options: [
//       {
//         label: t('components.questionCreation.templates.diet.gluten'),
//         value: 1
//       },
//       {
//         label: t('components.questionCreation.templates.diet.kosher'),
//         value: 2
//       },
//       {
//         label: t('components.questionCreation.templates.diet.halal'),
//         value: 3
//       },
//       {
//         label: t('components.questionCreation.templates.diet.vegetarian'),
//         value: 4
//       },
//       {
//         label: t('components.questionCreation.templates.diet.vegan'),
//         value: 5
//       },
//       {
//         label: t('components.questionCreation.templates.diet.pescetarian'),
//         value: 6
//       },
//       {
//         label: t(
//           'components.questionCreation.templates.diet.lactoVegetarian'
//         ),
//         value: 7
//       }
//     ]
//   })
// }

const marketResearchParticipation = {
  name: 'marketResearchParticipation',
  questions: 'choose-one',
  getData: ({ t }) => ({
    prompt: t(
      'components.questionCreation.templates.marketReserchParticipation.prompt'
    ),
    secondaryPrompt: t(
      'components.questionCreation.templates.marketReserchParticipation.secondaryPrompt'
    ),
    options: [
      {
        label: t(
          'components.questionCreation.templates.marketReserchParticipation.week'
        ),
        value: 1
      },
      {
        label: t(
          'components.questionCreation.templates.marketReserchParticipation.month'
        ),
        value: 2
      },
      {
        label: t(
          'components.questionCreation.templates.marketReserchParticipation.twoMonths'
        ),
        value: 3
      },
      {
        label: t(
          'components.questionCreation.templates.marketReserchParticipation.sixMonths'
        ),
        value: 4
      },
      {
        label: t(
          'components.questionCreation.templates.marketReserchParticipation.year'
        ),
        value: 5
      },
      {
        label: t(
          'components.questionCreation.templates.marketReserchParticipation.years'
        ),
        value: 6
      },
      {
        label: t(
          'components.questionCreation.templates.marketReserchParticipation.firstTime'
        ),
        value: 7
      }
    ]
  })
}

export default [
  fullName,
  gender,
  dateofbirth,
  stateQuestion,
  ethnicity,
  incomeRange,
  maritalStatus,
  hasChildren,
  numberOfChildren,
  smokerKind,
  foodAllergies,
  // whatAllergiesAndIntollerances,
  // dietaryPreferences,
  marketResearchParticipation
]

// questions
// 'open-answer',
// 'choose-product',
// 'email',
// 'upload-picture',
// 'info',
// 'choose-one',
// 'choose-multiple',
// 'numeric',
// 'select-and-justify',
// 'matrix',
// 'paired-questions',
// 'dropdown',
// 'vertical-rating',
// 'choose-date',
// 'slider',
// 'location',
// 'paypal-email',
// 'taster-name',
// 'time-stamp',
// 'choose-payment',
// 'show-product-screen'
