const chrisHansenRegex = /(@chr-hansen\.com)$/g

const isChrisHansenUser = user =>
  user && user.emailAddress && user.emailAddress.search(chrisHansenRegex) > 0

export default isChrisHansenUser
