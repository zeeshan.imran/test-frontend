import en from './en.json'
import de from './de.json'
import kr from './ko.json'
import fr from './fr.json'
import pt from './pt.json'
import es from './es.json'
import it from './it_IT.json'
import ja from './ja.json'
import pl from './pl.json'
import ru from './ru_RU.json'
import tr from './tr.json'
import cn from './zh_CN.json'
import fil from './fil.json'
import pt_BR from './pt_BR.json'

const resources = {
  en: {
    translation: en
  },
  de: {
    translation: de
  },
  kr: {
    translation: kr
  },
  fr: {
    translation: fr
  },
  pt: {
    translation: pt
  },
  pt_BR: {
    translation: pt_BR
  },
  es: {
    translation: es
  },
  it: {
    translation: it
  },
  ja: {
    translation: ja
  },
  pl: {
    translation: pl
  },
  ru: {
    translation: ru
  },
  tr: {
    translation: tr
  },
  cn: {
    translation: cn
  },
  fil: {
    translation: fil
  }
}

const defaultOptions = [
  {
    value: 'de',
    label: 'languages.deutsch'
  },
  {
    value: 'en',
    label: 'languages.english'
  },
  {
    value: 'fr',
    label: 'languages.french'
  },
  {
    value: 'kr',
    label: 'languages.korean'
  },
  {
    value: 'pt',
    label: 'languages.portuguese'
  },
  {
    value: 'es',
    label: 'languages.spanish'
  },
  {
    value: 'it',
    label: 'languages.italian'
  },
  {
    value: 'ja',
    label: 'languages.japanese'
  },
  {
    value: 'tr',
    label: 'languages.turkish'
  },
  {
    value: 'ru',
    label: 'languages.russian'
  },
  {
    value: 'pl',
    label: 'languages.polish'
  },
  {
    value: 'cn',
    label: 'languages.chinese'
  },
  {
    value: 'fil',
    label: 'languages.filipino'
  },
  {
    value: 'pt_BR',
    label: 'languages.br_portuguese'
  }
]

const bungeOptions = [
  {
    value: 'de',
    label: 'languages.deutsch'
  },
  {
    value: 'en',
    label: 'languages.english'
  },
  {
    value: 'fr',
    label: 'languages.french'
  },
  {
    value: 'kr',
    label: 'languages.korean'
  },
  {
    value: 'pt',
    label: 'languages.portuguese'
  },
  {
    value: 'es',
    label: 'languages.spanish'
  },
  {
    value: 'it',
    label: 'languages.italian'
  },
  {
    value: 'ja',
    label: 'languages.japanese'
  },
  {
    value: 'tr',
    label: 'languages.turkish'
  },
  {
    value: 'ru',
    label: 'languages.russian'
  },
  {
    value: 'pl',
    label: 'languages.polish'
  },
  {
    value: 'cn',
    label: 'languages.chinese'
  },
  {
    value: 'fil',
    label: 'languages.filipino'
  }
]

const chrHansenOptions = [
  {
    value: 'en',
    label: 'languages.english'
  },
  {
    value: 'de',
    label: 'languages.deutsch'
  },
  {
    value: 'es',
    label: 'languages.spanish'
  },
  {
    value: 'tr',
    label: 'languages.turkish'
  },
  {
    value: 'ru',
    label: 'languages.russian'
  },
  {
    value: 'fr',
    label: 'languages.french'
  },
]

const allOptions = {
  default: defaultOptions,
  bunge: bungeOptions,
  chrHansen: chrHansenOptions
}

const options = allOptions[process.env.REACT_APP_THEME]
  ? allOptions[process.env.REACT_APP_THEME]
  : defaultOptions


export { resources, options }
