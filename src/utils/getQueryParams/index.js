import queryString from 'qs'

const getQueryParams = location =>
  location.search ? queryString.parse(location.search.split('?').pop()) : {}

export default getQueryParams
