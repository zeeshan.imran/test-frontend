const colors = require('../Colors')
const Fonts = require('../Fonts')

module.exports = () => {
  return {
    '@link-color': '#7ecde9', // link color
    '@primary-color': '#7ecde9', // primary color
    '@success-color': '#becc00', // success state color
    '@warning-color': '#f77f00', // warning state color
    '@error-color': '#d63a66', // error state color
    '@font-family': Fonts.family.primaryRegular,
    '@font-size-base': '14px', // major text font size
    '@heading-color': 'rgba(0, 0, 0, .85)', // heading text color
  }
}
