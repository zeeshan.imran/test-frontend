import defaults from '../../defaults'

const getCurrency = (country = 'US', amount = 0) => {
  const availableCountry = defaults.countries.find(c => c.code === country)
  const rewardCurrency = availableCountry || {}
  const currencyPrefix =
    defaults.currencyPrefixes[rewardCurrency.currency] || ''
  const currencySuffix =
    defaults.currencySuffixes[rewardCurrency.currency] || ''

  return `${currencyPrefix}${amount}${currencySuffix}`
}

export default getCurrency
