const linkedInShare = (e, link) => {
  e.preventDefault()

  const w = 520
  const h = 570
  const dualScreenLeft =
    window.screenLeft !== undefined ? window.screenLeft : window.screenX
  const dualScreenTop =
    window.screenTop !== undefined ? window.screenTop : window.screenY

  const width = window.innerWidth
    ? window.innerWidth
    : document.documentElement.clientWidth
    ? document.documentElement.clientWidth
    : window.screen.width

  const height = window.innerHeight
    ? window.innerHeight
    : document.documentElement.clientHeight
    ? document.documentElement.clientHeight
    : window.screen.height

  const systemZoom = width / window.screen.availWidth
  const left = (width - w) / 2 / systemZoom + dualScreenLeft
  const top = (height - h) / 2 / systemZoom + dualScreenTop
  const strWindowFeatures = `location=yes,crollbars=yes,status=yes,width=${w /
    systemZoom},height=${h / systemZoom},top=${top},left=${left}`

  const URL = 'https://www.linkedin.com/shareArticle/?mini=true&url=' + link
  window.open(URL, '_blank', strWindowFeatures)

  return true
}

export default linkedInShare
