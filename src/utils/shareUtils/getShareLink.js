const getShareLink = (uniqueName, surveyEnrollmentId) => {
  const shareLink =
    process.env.NODE_ENV === 'development'
      ? `http://localhost:1337/share/${uniqueName}`
      : `${window.location.origin}/share/${uniqueName}`
  const query = []
  if (surveyEnrollmentId) {
    query.push(`referral=${surveyEnrollmentId}`)
  }
  return shareLink + (query.length > 0 ? `?${query.join('&')}` : '')
}

export default getShareLink
