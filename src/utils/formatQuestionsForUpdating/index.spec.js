import formatQuestionsForUpdating from './index'

describe('formatQuestionsForUpdating', () => {
  test('should set updating requiredQuestion for slider', () => {
    const question = [
      {
        id: 'question-2',
        required: true,
        type: 'slider',
        prompt: 'select 2 pairs',
        sliderOptions: {
          sliders: [
            {
              label: 'label 1'
            },
            {
              label: 'label 2'
            }
          ],
          hasFollowUpProfile: true
        },
        clientGeneratedId: 'question-1'
      }
    ]
    const surveyId = 'survey-1'
    expect(
      formatQuestionsForUpdating({ questions: question, surveyId: surveyId })
    ).toMatchObject([
      {
        chooseProductOptions: undefined,
        clientGeneratedId: 'question-1',
        order: 0,
        prompt: 'select 2 pairs',
        requiredQuestion: true,
        sliderOptions: {
          hasFollowUpProfile: true,
          profileQuestion: {
            chooseProductOptions: undefined,
            displayOn: undefined,
            order: 1,
            requiredQuestion: undefined,
            survey: 'survey-1',
            type: 'profile'
          },
          sliders: [{ label: 'label 1' }, { label: 'label 2' }]
        },
        survey: 'survey-1',
        type: 'slider'
      }
    ])
  })

  test('should set updating requiredQuestion for paired-questions', () => {
    const question = [
      {
        id: 'question-1',
        required: true,
        name: 'test survey',
        type: 'paired-questions',
        prompt: 'select 2 pairs',
        displayOn: 'section',
        pairsOptions: {
          minPairs: 2,
          hasFollowUpProfile: true,
          profileQuestion: {
            required: true,
            id: 'question-2',
            type: 'name',
            prompt: 'enter name here',
            displayOn: 'middel'
          }
        },

        clientGeneratedId: 'question-1'
      }
    ]
    const surveyId = 'survey-1'
    expect(
      formatQuestionsForUpdating({ questions: question, surveyId: surveyId })
    ).toMatchObject([
      {
        chooseProductOptions: undefined,
        clientGeneratedId: 'question-1',
        displayOn: 'section',
        name: 'test survey',
        order: 0,
        pairsOptions: {
          hasFollowUpProfile: true,
          minPairs: 2,
          profileQuestion: {
            chooseProductOptions: undefined,
            displayOn: 'section',
            order: 1,
            prompt: 'enter name here',
            requiredQuestion: true,
            survey: 'survey-1',
            type: 'profile'
          }
        },
        prompt: 'select 2 pairs',
        requiredQuestion: true,
        survey: 'survey-1',
        type: 'paired-questions'
      }
    ])
  })

  test('should set updating.requiredQuestion', () => {
    const question = [
      {
        id: 'question-2',
        type: 'name',
        prompt: 'enter name here',
        displayOn: 'middel',
        required: true
      }
    ]
    const surveyId = 'survey-1'
    expect(
      formatQuestionsForUpdating({ questions: question, surveyId: surveyId })
    ).toMatchObject([
      {
        chooseProductOptions: undefined,
        displayOn: 'middel',
        order: 0,
        prompt: 'enter name here',
        requiredQuestion: true,
        survey: 'survey-1',
        type: 'name'
      }
    ])
  })
})
