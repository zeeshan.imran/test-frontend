import { getPPI } from './getPPI'

export const A4 = {
  name: 'A4',
  printZoom: 3,
  width: (21 / 2.54) * getPPI(),
  height: (29.7 / 2.54) * getPPI(),
  cols: 20,
  rows: 28,
  colsBetweenPages: 3
}
