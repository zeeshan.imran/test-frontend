export const getPPI = () => {
  var div = document.createElement('div')
  div.style.width = '100in'
  var body = document.getElementsByTagName('body')[0]
  body.appendChild(div)
  var ppi = document.defaultView
    .getComputedStyle(div, null)
    .getPropertyValue('width')
  body.removeChild(div)
  return parseFloat(ppi) / 100
}
