import moment from 'moment'

export const formatDemographicAnswer = (userAuth, question) => {
  const { demographicField } = question
  const dateFormat = 'YYYY-MM-DD'
  const value = userAuth[demographicField]
  let resultValue = ''

  switch (demographicField) {
    case 'fullName':
    case 'numberOfChildren':
      return userAuth[demographicField] ? [userAuth[demographicField]] : ''

    case 'gender':
    case 'state':
    case 'ethnicity':
    case 'incomeRange':
    case 'maritalStatus':
    case 'hasChildren':
    case 'smokerKind':
    case 'foodAllergies':
    case 'marketResearchParticipation':
      question.options.find(option => {
        if (
          option.label.toLowerCase() ===
          userAuth[demographicField].toLowerCase()
        ) {
          resultValue = option.value
          return true
        }
        return false
      })
      return resultValue ? [resultValue] : ''

    case 'dateofbirth':
      return value ? [moment(value).format(dateFormat)] : ''

    default:
      return userAuth[demographicField] ? [userAuth[demographicField]] : ''
  }
}
