export const getImages = imagesCollection => {
  const { REACT_APP_THEME } = process.env
  const desktopImage = REACT_APP_THEME
    ? imagesCollection[`${REACT_APP_THEME}Desktop`]
    : imagesCollection['defaultDesktop']

  const mobileImage = REACT_APP_THEME
    ? imagesCollection[`${REACT_APP_THEME}Mobile`]
    : imagesCollection['defaultMobile']

  return { desktopImage, mobileImage }
}
