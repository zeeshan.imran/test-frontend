import { NEVER_REQUIRED_QUESTIONS_TYPES } from '../Constants'

// all added question are separated by section
const separateQuestionsBySection = questions => {
  let questionsBySection = []
  if (questions) {
    questions.forEach(singleQuestion => {
      if (!questionsBySection[singleQuestion.displayOn]) {
        questionsBySection[singleQuestion.displayOn] = []
        questionsBySection[singleQuestion.displayOn]['showProductImage'] = true
        questionsBySection[singleQuestion.displayOn]['considerValueForScore'] = true
        questionsBySection[singleQuestion.displayOn]['allRequired'] = true
        questionsBySection[singleQuestion.displayOn]['displaySurveyNameOnAll'] = true
        questionsBySection[singleQuestion.displayOn]['questions'] = []
      }

      if (!singleQuestion.showProductImage) {
        questionsBySection[singleQuestion.displayOn]['showProductImage'] = false
      }

      if (!singleQuestion.considerValueForScore) {
        questionsBySection[singleQuestion.displayOn]['considerValueForScore'] = false
      }

      if (
        !singleQuestion.required &&
        !NEVER_REQUIRED_QUESTIONS_TYPES.has(singleQuestion.type)
      ) {
        questionsBySection[singleQuestion.displayOn]['allRequired'] = false
      }
      if (!singleQuestion.displaySurveyName) {
        questionsBySection[singleQuestion.displayOn]['displaySurveyNameOnAll'] = false
      }
      questionsBySection[singleQuestion.displayOn]['questions'].push(
        singleQuestion
      )
    })
  }

  return questionsBySection
}

export default separateQuestionsBySection
