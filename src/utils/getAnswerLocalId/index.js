export default (questionId, selectedProducts) => {
  if (!selectedProducts || selectedProducts.length === 0) {
    return questionId
  } else {
    return `${questionId}-${selectedProducts.length}`
  }
}
