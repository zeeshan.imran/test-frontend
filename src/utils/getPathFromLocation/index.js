export const getPathFromLocation = (location, part = 2) =>
  location.pathname && location.pathname.split('/')[part]
