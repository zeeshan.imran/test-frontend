export const getSingleChartSettings = (chartsSettings, { question, chart_type }) => {
  return chartsSettings && chartsSettings[`${question}-${chart_type}`]
    ? chartsSettings[`${question}-${chart_type}`]
    : chartsSettings
}
