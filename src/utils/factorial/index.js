import { range } from 'ramda'

export const factorial = number => {
  if (number === 0) return 1
  return range(1, number + 1).reduce((prev, acc) => prev * acc)
}
