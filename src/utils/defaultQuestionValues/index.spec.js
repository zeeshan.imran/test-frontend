import { getDefaultSelectOptions } from './index'

describe('getDefaultSelectOptions', () => {
  test('should render getDefaultSelectOptions with null amount', () => {
    expect(getDefaultSelectOptions({})).toMatchObject([
      { label: '', value: '' }
    ])
  })
  test('should render getDefaultSelectOptions', () => {
    expect(getDefaultSelectOptions(2)).toMatchObject([
      { label: '', value: '' },
      { label: '', value: '' }
    ])
  })
})
