export const trimSpaces = string => string.replace(/\s\s+/g, ' ').trim()
