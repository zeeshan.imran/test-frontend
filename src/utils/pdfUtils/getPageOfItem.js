import { PAPER_SIZES } from '../paperSizes'

const getPageOfItem = (paperSize, item) => {
  const paperSizeMeta = PAPER_SIZES[paperSize]

  return Math.floor(
    item.x / (paperSizeMeta.cols + paperSizeMeta.colsBetweenPages)
  )
}

export default getPageOfItem
