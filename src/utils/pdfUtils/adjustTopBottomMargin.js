import getPageOfItem from './getPageOfItem'
import { PAPER_SIZES } from '../paperSizes'

const adjustTopBottomMargin = (
  paperSize,
  elementsLayout,
  page,
  top = 2,
  bottom = 2
) => {
  const itemsInPage = elementsLayout.filter(
    item => getPageOfItem(paperSize, item) === page
  )

  let adjustTop = 0

  for (let pageItem of itemsInPage) {
    if (!pageItem.static) {
      adjustTop = Math.max(adjustTop, top - pageItem.y)
    }
  }

  const paperSizeMeta = PAPER_SIZES[paperSize]
  for (const pageItem of itemsInPage) {
    if (!pageItem.static) {
      pageItem.y += adjustTop
      if (pageItem.y + pageItem.h >= paperSizeMeta.rows - bottom) {
        pageItem.h = paperSizeMeta.rows - bottom - pageItem.y
      }
    }
  }
}

export default adjustTopBottomMargin
