import { getBoundRect, getBottomOfItems, PAPER_SIZES } from '../paperSizes'
import { getChartId } from './getChartId'
import { getSingleChartSettings } from '../ChartSettingsUtils'
import OVERRIDER_SETTINGS from './OVERRIDER_SETTINGS'

const getDefaultSize = item => {
  if (item.isMultiChart) {
    return [18, 18]
  }

  switch (item.result.chart_type) {
    case 'pie':
      return [9, 9]
    case 'map':
      return [18, 11]
    case 'polar':
      return [18, 12]
    case 'stacked-bar':
      return [18, 12]
    case 'table':
      return [18, 24]
    default:
      return [18, 9]
  }
}

const autoExpand = ({ bottom }, items) => {
  for (let item of items) {
    if (items.every(otherItem => item.y >= otherItem.y)) {
      item.h = bottom - item.y
    }
  }
}

const calculateElementsLayout = (paperSizeName, settings, stats, templateName) => {
  let page = 0
  const pages = [[]]
  const paperSize = PAPER_SIZES[paperSizeName]
  let rect = getBoundRect(paperSize)
  let { left, right, bottom } = rect

  pages[0].push({
    i: 'banner',
    x: 1,
    y: 0,
    w: 18,
    h: 3,
    static: true
  })

  let x = 1
  let y = 3
  stats.forEach(chart => {
    const chartSettings = {
      ...getSingleChartSettings(settings, chart.result),
      ...OVERRIDER_SETTINGS[templateName]
    }

    const hasTables =
      chartSettings.isStatisticsTableShown || chartSettings.isDataTableShown

    let [w, h] = getDefaultSize(chart)
    let allH = hasTables ? 22 : h

    // over horizontal => move to bottom of items
    if (x + w > right) {
      x = left
      y = getBottomOfItems(pages[page])

      // over vertical => next page
      if (y + allH > bottom) {
        autoExpand({ bottom }, pages[page])
        page = page + 1
        pages[page] = pages[page] || []
        x = rect.left
        y = rect.top
      }
    }
    pages[page].push({
      i: getChartId(chart),
      x,
      y,
      w,
      h,
      static: false
    })
    if (hasTables) {
      pages[page].push({
        i: `${getChartId(chart)}-table`,
        x,
        y: y + h,
        w,
        h: allH - h,
        static: false
      })
    }
    x = x + w
  })

  autoExpand({ bottom }, pages[page])
  return pages
}
export default calculateElementsLayout
