import styled from 'styled-components'
import { getImages } from '../getImages'
import { imagesCollection } from '../../assets/png'

const { desktopImage } = getImages(imagesCollection)

const Logo = styled.div`
  transform-origin: bottom left;
  background: url(${desktopImage}) no-repeat center;
  background-size: contain;
  width: 10rem;
  height: 2rem;
  margin-bottom: 4px;
  background-position-x: ${({ align }) => ({ right: 'right' }[align] || 'left')};
`

export default Logo
