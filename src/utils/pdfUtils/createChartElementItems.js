import React from 'react'
import { unnest } from 'ramda'
import { getSingleChartSettings } from '../../utils/ChartSettingsUtils'
import { isMultiChart } from '../../utils/chartConfigs'
import { getChartId } from './getChartId'
import GridItemContent from './GridItemContent'
import OVERRIDER_SETTINGS from './OVERRIDER_SETTINGS'

const createPrintPanel = (itemId, chart, chartSettings, panelProps) => {
  let ref = null

  return {
    i: itemId,
    type: `chart-${chart.result.chart_type}`,
    chartType: chart.result.chart_type,
    isMultiChart: isMultiChart(chart.result),
    align: 'fit',
    settings: chartSettings,
    getRef: () => ref,
    content: (
      <GridItemContent
        chartId={itemId}
        ref={_ref => (ref = _ref)}
        chart={chart}
        {...panelProps}
      />
    )
  }
}

const createChartElementItems = (stats, settings, templateName) =>
  unnest(
    stats.map(chart => {
      const chartSettings = {
        ...getSingleChartSettings(settings, chart.result),
        ...OVERRIDER_SETTINGS[templateName]
      }

      const tablelesCharts =
        chart && chart.result
          ? ['table', 'spearmann-table', 'pearson-table', 'tags-list'].includes(
              chart.result.chart_type
            )
          : true

      const hasTables =
        chartSettings.isStatisticsTableShown || chartSettings.isDataTableShown

      return [
        createPrintPanel(getChartId(chart), chart, chartSettings, {
          settings: chartSettings,
          printFormat: 'chart'
        }),
        hasTables &&
          !tablelesCharts &&
          createPrintPanel(getChartId(chart) + '-table', chart, chartSettings, {
            settings: { ...chartSettings, isTotalRespondentsShown: false },
            printFormat: 'tables'
          })
      ].filter(panel => !!panel)
    })
  )

export default createChartElementItems
