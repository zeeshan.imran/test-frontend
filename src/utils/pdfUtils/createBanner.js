import React from 'react'
import Banner from './Banner'

const createBanner = (surveyName) => {
  let ref = null

  return {
    i: 'banner',
    type: 'banner',
    align: 'scale',
    getRef: () => ref,
    content: <Banner title={surveyName} ref={_ref => (ref = _ref)} />,
    static: true
  }
}

export default createBanner