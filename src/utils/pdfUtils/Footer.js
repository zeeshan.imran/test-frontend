import React, { useState, useImperativeHandle, forwardRef } from 'react'
import styled from 'styled-components'
import PageInfo from './PageInfo'
import SurveyNote from './SurveyNote'
import Logo from './Logo'

const FooterWrapper = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  height: 90%;
  border-top: 1px solid #ddd;
  transform: ${({ scale }) => `scale(${scale || 1})`};
  transform-origin: left top;
  width: ${({ scale }) => `${100 / (scale || 1)}%`};
  padding: 0 40px 4px;
  margin-top: 4px;

  ${Logo} {
    margin-top: 8px;
  }
`
const Left = styled.div``

const Right = styled.div`
  ${PageInfo} {
    margin: 4px;
  }
`

const Footer = forwardRef(
  ({ page, logoPlacement, pageInfoPlacement, footerNote }, ref) => {
    const [scale, setScale] = useState()

    useImperativeHandle(
      ref,
      () => ({
        setScale
      }),
      []
    )

    return (
      <FooterWrapper scale={scale}>
        <Left>
          {footerNote && (
            <SurveyNote dangerouslySetInnerHTML={{ __html: footerNote }} />
          )}
          {logoPlacement === 'bottomLeft' && <Logo align='left' />}
        </Left>
        <Right>
          {logoPlacement === 'bottomRight' && <Logo align='right' />}
          {pageInfoPlacement === 'bottomRight' && (
            <PageInfo align='right'>{page + 1}</PageInfo>
          )}
        </Right>
      </FooterWrapper>
    )
  }
)

export default Footer
