import { range, pick } from 'ramda'
import { PAPER_SIZES } from '../paperSizes'
import { isHeaderVisible, isFooterVisible } from './headerFooterUtils'
import { PDF_LAYOUT_PROPS } from '../Constants'

const calculateHeaderFooterLayout = (footerNote, layout) => {
  const { totalPages, paperSize } = layout
  const layoutProps = pick(PDF_LAYOUT_PROPS)(layout)
  const paperSizeMeta = PAPER_SIZES[paperSize]

  return [
    ...range(0, totalPages)
      .filter(isHeaderVisible(layoutProps))
      .map(page => ({
        i: `header-of-page-${page}-${totalPages}`,
        x: page * (paperSizeMeta.cols + paperSizeMeta.colsBetweenPages),
        y: 0,
        w: paperSizeMeta.cols,
        h: 2,
        static: true
      })),
    ...range(0, totalPages)
      .filter(isFooterVisible(footerNote, layout))
      .map(page => ({
        i: `footer-of-page-${page}-${totalPages}`,
        x: page * (paperSizeMeta.cols + paperSizeMeta.colsBetweenPages),
        y: paperSizeMeta.rows - 2,
        w: paperSizeMeta.cols,
        h: 2,
        static: true
      }))
  ]
}

export default calculateHeaderFooterLayout
