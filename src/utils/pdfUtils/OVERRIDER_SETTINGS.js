const OVERRIDER_SETTINGS = {
  operator: {},
  taster: {
    isDataTableShown_format: 'abs',
    isDataTableShown: true
  }
}

export default OVERRIDER_SETTINGS