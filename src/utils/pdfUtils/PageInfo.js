import styled from 'styled-components'

const PageInfo = styled.div`
  flex: 120px 0;
  order: ${({ align }) => ({ right: 2 }[align] || 0)};
  text-align: ${({ align }) => ({ right: 'right' }[align] || 'left')};
  font-size: 1.4rem;
`

export default PageInfo
