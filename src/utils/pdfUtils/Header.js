import React, { useState, useImperativeHandle, forwardRef } from 'react'
import styled from 'styled-components'
import PageInfo from './PageInfo'
import Logo from './Logo'

const HeaderWrapper = styled.div`
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
  height: 90%;
  border-bottom: 1px solid #ddd;
  transform: ${({ scale }) => `scale(${scale || 1})`};
  transform-origin: left bottom;
  width: ${({ scale }) => `${100 / (scale || 1)}%`};
  padding: 0 40px 4px;
  font-size: 1.6rem;
`

const Left = styled.div``

const Right = styled.div``

const Header = forwardRef(({ logoPlacement, pageInfoPlacement, page }, ref) => {
  const [scale, setScale] = useState()

  useImperativeHandle(
    ref,
    () => ({
      setScale
    }),
    []
  )

  return (
    <HeaderWrapper scale={scale}>
      <Left>
        {logoPlacement === 'topLeft' && <Logo align='left' />}
      </Left>
      <Right>
        {logoPlacement === 'topRight' && <Logo align='right' />}
        {pageInfoPlacement === 'topRight' && (
          <PageInfo align='right'>{page + 1}</PageInfo>
        )}
      </Right>
    </HeaderWrapper>
  )
})

export default Header
