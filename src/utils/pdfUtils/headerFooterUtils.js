export const isHeaderVisible = ({ logoPlacement, pageInfoPlacement }) => page =>
  (logoPlacement.includes('top') || pageInfoPlacement.includes('top')) &&
  page > 0

export const isFooterVisible = (
  footerNote,
  { totalPages, logoPlacement, pageInfoPlacement, footerNoteAllPages }
) => page =>
  logoPlacement.includes('bottom') ||
  pageInfoPlacement.includes('bottom') ||
  ((footerNoteAllPages || page + 1 === totalPages) && footerNote)
