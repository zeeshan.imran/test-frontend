import React from 'react'
import { range, pick } from 'ramda'
import Header from './Header'
import Footer from './Footer'
import { isHeaderVisible, isFooterVisible } from './headerFooterUtils'
import { PDF_LAYOUT_PROPS } from '../Constants'

export const createHeader = ({
  page,
  totalPages,
  logoPlacement,
  pageInfoPlacement
}) => {
  let ref = null
  return {
    i: `header-of-page-${page}-${totalPages}`,
    type: 'header',
    align: 'scale',
    getRef: () => ref,
    content: (
      <Header
        page={page}
        totalPages={totalPages}
        logoPlacement={logoPlacement}
        pageInfoPlacement={pageInfoPlacement}
        ref={_ref => (ref = _ref)}
      />
    ),
    static: true
  }
}

export const createFooter = (
  footerNote,
  { page, totalPages, logoPlacement, pageInfoPlacement, footerNoteAllPages }
) => {
  let ref = null
  return {
    i: `footer-of-page-${page}-${totalPages}`,
    type: 'footer',
    align: 'scale',
    getRef: () => ref,
    content: (
      <Footer
        page={page}
        totalPages={totalPages}
        logoPlacement={logoPlacement}
        pageInfoPlacement={pageInfoPlacement}
        footerNote={
          (footerNoteAllPages || page + 1 === totalPages) && footerNote
        }
        ref={_ref => (ref = _ref)}
      />
    ),
    static: true
  }
}

const createHeaderFooterItems = (footerNote, layout) => {
  const { totalPages } = layout
  const layoutProps = pick(PDF_LAYOUT_PROPS)(layout)

  return [
    ...range(0, totalPages)
      .filter(isHeaderVisible(layoutProps))
      .map(page =>
        createHeader({
          page,
          totalPages,
          ...layoutProps
        })
      ),
    ...range(0, totalPages)
      .filter(isFooterVisible(footerNote, layout))
      .map(page => {
        return createFooter(footerNote, {
          page,
          totalPages,
          ...layoutProps
        })
      })
  ]
}

export default createHeaderFooterItems
