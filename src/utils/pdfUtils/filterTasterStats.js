import { filter } from 'ramda'

export default filter(
  chart => chart && chart.result && !['map'].includes(chart.result.chart_type)
)
