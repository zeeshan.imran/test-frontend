import React, {
  useMemo,
  useState,
  useImperativeHandle,
  forwardRef,
  useEffect
} from 'react'
import { useTranslation } from 'react-i18next'
import * as Yup from 'yup'
import { getChartComponent } from '../../utils/chartConfigs'
import DataGuard from '../../components/Charts/DataGuard'
import { ChartLayoutItem } from '../../components/EditPdfLayout/styles'
import { getChartId } from './getChartId'

const mixed = Yup.mixed()

const CARD_WIDTHS = {
  DEFAULT: 900,
  '': 450,
  pie: 450
}

export const GridItem = ({
  printFormat,
  settings,
  chart,
  scale,
  cardWidth,
  cardHeight,
  chartWidth
}) => {
  const { el, chartId } = useMemo(() => {
    const el = chart.result
    return {
      el,
      chartId: getChartId(chart)
    }
  }, [chart])

  const { t } = useTranslation()
  const ChartComponent = getChartComponent(el)
  const chartElement = useMemo(
    () => (
      <DataGuard
        printFormat={printFormat}
        cardWidth={`${chartWidth}px`}
        cardMargin='0 0'
        validationSchema={mixed}
        chartsSettings={settings}
        chartType={el.chart_type}
        chartId={chartId}
        inputData={{ ...el, chart_id: chartId }}
        t={t}
        component={ChartComponent}
      />
    ),
    [t, chartWidth, ChartComponent, el, chartId, printFormat, settings]
  )

  return (
    <ChartLayoutItem
      scale={scale}
      cardWidth={cardWidth}
      cardHeight={cardHeight}
      chartWidth={chartWidth}
    >
      {chartElement}
    </ChartLayoutItem>
  )
}

const GridItemContent = forwardRef(
  ({ chartId, chart, settings, printFormat }, ref) => {
    const chartWidth = useMemo(() => {
      const el = chart.result
      return CARD_WIDTHS[el.chart_type] || CARD_WIDTHS.DEFAULT
    }, [chart])

    const [gridItem, setGridItem] = useState(null)
    const [chartSize, setChartSize] = useState({
      width: chartWidth,
      height: null
    })
    const [scale, setScale] = useState(0)

    const cardWidth = useMemo(() => {
      if (!gridItem) {
        return 0
      }

      return Math.max(chartWidth, Math.round(gridItem.width / scale) || 0)
    }, [chartWidth, scale, gridItem])

    useImperativeHandle(
      ref,
      () => ({
        updateGridItemSize: ({ width, height }) => {
          if (
            !gridItem ||
            gridItem.width !== width ||
            gridItem.height !== height
          ) {
            //console.log('trigger', { width, height })
            setGridItem({
              height,
              width
            })
          }
        },
        getChartSize: () => ({
          scale,
          cardWidth
        })
      }),
      [gridItem, cardWidth, scale]
    )

    useEffect(() => {
      setTimeout(() => {
        // measure the height of card after rendering
        const dom = document.querySelector(`#pdf-${chartId} > div > div`)
        if (dom) {
          const height = dom.offsetHeight
          if (height !== chartSize.height) {
            //console.log(chartId, 'new height', height)
            setChartSize({ ...chartSize, height })
          }
        }
      }, 0)
    })

    useEffect(() => {
      if (gridItem && chartSize.height) {
        const newScale =
          Math.round(
            Math.min(
              gridItem.width / chartSize.width,
              gridItem.height / chartSize.height
            ) * 1e5
          ) / 1e5

        // prettier-ignore
        //console.log(chartId, 'inner:', chartSize, 'outer:', gridItem, '->', newScale)

        if (!isNaN(newScale)) {
        setScale(newScale)
      }
      }
    }, [chartId, chartSize, gridItem])

    if (!gridItem) {
      return null
    }

    return (
      <GridItem
        chart={chart}
        printFormat={printFormat}
        settings={settings}
        scale={scale}
        cardWidth={cardWidth}
        cardHeight={chartSize.height / scale}
        chartWidth={chartWidth}
      />
    )
  }
)

export default GridItemContent