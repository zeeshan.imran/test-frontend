import React, { useState, useImperativeHandle, forwardRef } from 'react'
import styled from 'styled-components'
import moment from 'moment'
import { getImages } from '../../utils/getImages'
import { imagesCollection } from '../../assets/png'

const { desktopImage } = getImages(imagesCollection)

const Logo = styled.div`
  transform-origin: bottom left;
  background: url(${props => props.src}) no-repeat center;
  background-size: contain;
  width: 18.5rem;
  height: 3.4rem;
  margin-bottom: 4px;
`

const Title = styled.div`
  flex: 1;
  transform-origin: bottom right;
  text-align: right;
  h3 {
    font-size: 1.6rem;
    margin-bottom: 0;
  }
  span {
    font-size: 1.2rem;
    color: #333;
  }
`

const BannerWrapper = styled.div`
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
  height: 90%;
  border-bottom: 1px solid #999;
  transform: ${({ scale }) => `scale(${scale || 1})`};
  transform-origin: left bottom;
  width: ${({ scale }) => `${100 / (scale || 1)}%`};
`

const Banner = forwardRef(({ title }, ref) => {
  const [scale, setScale] = useState()

  useImperativeHandle(
    ref,
    () => ({
      setScale
    }),
    []
  )

  return (
    <BannerWrapper scale={scale}>
      <Logo src={desktopImage} />
      <Title>
        <h3>{title}</h3>
        <span>Exported at: {moment().format('MMM D, YYYY')}</span>
      </Title>
    </BannerWrapper>
  )
})

export default Banner
