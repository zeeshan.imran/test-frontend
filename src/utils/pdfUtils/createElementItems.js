import createChartElementItems from './createChartElementItems'
import createBanner from './createBanner'

const createElementItems = (surveyName, stats, settings, templateName) => {
  return [
    createBanner(surveyName),
    ...createChartElementItems(stats, settings, templateName)
  ]
}

export default createElementItems
