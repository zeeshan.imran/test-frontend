const { default: styled } = require("styled-components");

const SurveyNote = styled.div`
  flex: 200px 1;
  font-size: 1.2rem;
`

export default SurveyNote