const { default: getPageOfItem } = require('./getPageOfItem')

const moveElementsDown = (layout, page) => {
  const itemInPage = layout.gridLayout.filter(
    item => getPageOfItem(layout.paperSize, item) === page
  )

  itemInPage.forEach(item => (item.y = item.y - 1))
}

export default moveElementsDown
