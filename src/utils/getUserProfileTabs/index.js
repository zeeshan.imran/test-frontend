export const getUserProfileTabs = (t) => {
    return [
        {
          title: t('containers.userProfile.tabs.personal'),
          key: "personal",
          iconType: "user"
        },
        {
          title: t('containers.userProfile.tabs.additional'),
          key: "additional",
          iconType: "profile"
        },
        {
          title: t('containers.userProfile.tabs.payment'),
          key: "payment",
          iconType: "history"
        },
        {
          title: t('containers.userProfile.tabs.password'),
          key: "password",
          iconType: "key"
        },
        {
          title: t('containers.userProfile.tabs.privacy'),
          key: "privacy",
          iconType: "lock"
        }
      ]
  }