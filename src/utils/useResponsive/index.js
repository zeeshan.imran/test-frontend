import { useState, useEffect } from 'react'
const BREAKPOINTS = {
  XL: 1200,
  LG: 992,
  MD: 768,
  SM: 576
}
export const ONE_PIXEL_IN_REM = 0.0625

const QUERIES = [
  `(max-width: ${BREAKPOINTS.MD - ONE_PIXEL_IN_REM}px)`, // 0: Mobile
  `(min-width: ${BREAKPOINTS.MD}px) and (max-width: ${BREAKPOINTS.LG -
    ONE_PIXEL_IN_REM}px)`, // 1: Tablet
  `(min-width: ${BREAKPOINTS.LG}px)` // 2: Desktop
]

const matcher = () => {
  if (window.matchMedia(QUERIES[0]).matches) {
    return 'mobile'
  }
  if (window.matchMedia(QUERIES[1]).matches) {
    return 'tablet'
  }
  if (window.matchMedia(QUERIES[2]).matches) {
    return 'desktop'
  }
  return 'unrecognized'
}

const useResponsive = () => {
  const [matched, setMatched] = useState(matcher)
  const updateMatched = () => setMatched(matcher)
  useEffect(() => {
    updateMatched()
    window.addEventListener('resize', updateMatched)
    window.addEventListener('orientationchange', updateMatched)
    return () => {
      window.removeEventListener('resize', updateMatched)
      window.removeEventListener('orientationchange', updateMatched)
    }
  }, [])

  return {
    mobile: matched === 'mobile',
    tablet: matched === 'tablet',
    desktop: matched === 'desktop',
    matched
  }
}

export default useResponsive
