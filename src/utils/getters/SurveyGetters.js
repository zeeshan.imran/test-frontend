export const getSurveyTitle = (survey = {}) => survey.title

export const getSurveyId = (survey = {}) => survey.id
