export const getStepTitle = (step = {}) => step.title

export const getStepDescription = (step = {}) => step.description

export const getStepPath = (step = {}) => step.path
