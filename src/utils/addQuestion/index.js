import { groupBy, prop } from 'ramda'

const addQuestion = (questions, newQuestion) => {
  const questionsByType = groupBy(prop('displayOn'))(questions)
  questionsByType[newQuestion.displayOn] = [
    ...(questionsByType[newQuestion.displayOn] || []),
    newQuestion
  ]
  return [
    ...(questionsByType.screening || []),
    ...(questionsByType.begin || []),
    ...(questionsByType.middle || []),
    ...(questionsByType.end || [])
  ]
}

export default addQuestion
