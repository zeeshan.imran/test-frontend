import addQuestion from "./index";

describe('add question', () => {
  test('should add to the end of section', () => {
    expect(
      addQuestion(
        [
          { id: 1, displayOn: 'screening' },
          { id: 2, displayOn: 'begin' },
          { id: 3, displayOn: 'middle' },
          { id: 4, displayOn: 'middle' },
          { id: 5, displayOn: 'end' }
        ],
        { id: 'new', displayOn: 'middle' }
      )
    ).toMatchObject([
      { id: 1, displayOn: 'screening' },
      { id: 2, displayOn: 'begin' },
      { id: 3, displayOn: 'middle' },
      { id: 4, displayOn: 'middle' },
      { id: 'new', displayOn: 'middle' },
      { id: 5, displayOn: 'end' }
    ])
  })

  test('should add screening question on top', () => {
    expect(
      addQuestion(
        [
          { id: 3, displayOn: 'middle' },
          { id: 4, displayOn: 'middle' },
          { id: 5, displayOn: 'end' }
        ],
        { id: 'new', displayOn: 'screening' }
      )
    ).toMatchObject([
      { id: 'new', displayOn: 'screening' },
      { id: 3, displayOn: 'middle' },
      { id: 4, displayOn: 'middle' },
      { id: 5, displayOn: 'end' }
    ])
  })

  test('should add middle in middle', () => {
    expect(
      addQuestion(
        [
          { id: 1, displayOn: 'screening' },
          { id: 2, displayOn: 'begin' },
          { id: 5, displayOn: 'end' }
        ],
        { id: 'new', displayOn: 'middle' }
      )
    ).toMatchObject([
      { id: 1, displayOn: 'screening' },
      { id: 2, displayOn: 'begin' },
      { id: 'new', displayOn: 'middle' },
      { id: 5, displayOn: 'end' }
    ])
  })

  test('should add end question on bottom', () => {
    expect(
      addQuestion(
        [
          { id: 3, displayOn: 'begin' },
          { id: 4, displayOn: 'middle' },
          { id: 5, displayOn: 'end' }
        ],
        { id: 'new', displayOn: 'end' }
      )
    ).toMatchObject([
      { id: 3, displayOn: 'begin' },
      { id: 4, displayOn: 'middle' },
      { id: 5, displayOn: 'end' },
      { id: 'new', displayOn: 'end' }
    ])
  })
})
