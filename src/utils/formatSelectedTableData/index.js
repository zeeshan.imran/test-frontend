const getValue = el => {
  switch (typeof el) {
    case 'boolean':
      return el ? 'true' : 'false'
    case 'number':
      return el.toFixed(5)
    default:
      return el
  }
}

const formatSelectedTableData = (table, t) => {
  const columns = table.labels.map(el => ({
    title:
      t(`charts.analysis.tableHeaders.${el}`) !==
      `charts.analysis.tableHeaders.${el}`
        ? t(`charts.analysis.tableHeaders.${el}`)
        : el,
    dataIndex: el,
    key: el.toLowerCase()
  }))
  const rows = table.result.map((row, i) => {
    const rowData = row.reduce(
      (acc, el, index) => ({
        ...acc,
        [columns[index].dataIndex]: getValue(el)
      }),
      {}
    )
    rowData.key = i
    return rowData
  })
  return {
    columns,
    rows
  }
}

export default formatSelectedTableData
