import { useMemo } from 'react'
import { useTranslation } from 'react-i18next'

const formatAnalysisTableData = (data, chartSettings) => {
  const { t } = useTranslation()

  let columns = useMemo(
    () =>
      data &&
      data.column_labels &&
      data.column_labels.map(el => ({
        title:
          t(`charts.analysis.tableHeaders.${el}`) !==
          `charts.analysis.tableHeaders.${el}`
            ? t(`charts.analysis.tableHeaders.${el}`)
            : el,
        dataIndex: el,
        key: el.toLowerCase(),
        sorter: (a, b) => (a[el] > b[el] ? 1 : a[el] < b[el] ? -1 : 0)
      })),
    [data]
  )

  const firstColumn = data.column_labels[0]
  const rows = useMemo(
    () =>
      data &&
      data.result &&
      data.row_labels &&
      data.column_labels &&
      data.result.map((result, index) => {
        const rowTranslationsKey = `charts.analysis.tableHeaders.${
          data.row_labels[index + 1]
        }`
        const translatedRowLabel = t(
          `charts.analysis.tableHeaders.${data.row_labels[index + 1]}`
        )
        let row = {
          [firstColumn]:
            rowTranslationsKey !== translatedRowLabel
              ? translatedRowLabel
              : data.row_labels[index + 1],
          key: index
        }
        data.column_labels.slice(1).forEach((column, columnIndex) => {
          row[column] = parseFloat(result[columnIndex], 10)
            ? parseFloat(result[columnIndex], 10).toFixed(
                chartSettings.howManyDecimals
              )
            : result[columnIndex]
        })
        return row
      }),
    [firstColumn, data]
  )

  return { columns: columns, rows: rows }
}

export default formatAnalysisTableData
