export const imagesCollection = {
  chrHansenMobile: require('./hansen-mobile.png'),
  chrHansenDesktop: require('./hansen-desktop.png'),
  defaultDesktop: require('./FlavorWiki_Full_Logo.png'),
  defaultMobile: require('./FlavorWiki_Leaf_Logo.png'),
  bungeDesktop: require('./bunge-desktop.png'),
  bungeMobile: require('./bunge-mobile.png'),
  poweredBy : require('./FlavorWiki_Powered_Logo.png'),
  bungeLogo: require(`./bungeLogo.png`),
  chrHansenLogo: require(`./chrHansenLogo.png`),
  defaultLogo: require(`./defaultLogo.png`),
}
