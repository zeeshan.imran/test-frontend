import gql from 'graphql-tag'
import { incomeRangeInfo } from '../../fragments/incomeRange'

const INCOME_RANGES_QUERY = gql`
  query incomeRanges($currency: String) {
    incomeRanges(currency: $currency) {
      ...incomeRangeInfo
    }
  }
  ${incomeRangeInfo}
`

export default INCOME_RANGES_QUERY
