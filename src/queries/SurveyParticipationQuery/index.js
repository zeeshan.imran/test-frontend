import gql from 'graphql-tag'

const SURVEY_PARTICIPATION_QUERY = gql`
  query {
    currentSurveyParticipation @client {
      surveyId
      surveyEnrollmentId
      state
      selectedProduct
      selectedProducts
      paypalEmail
      currentSurveyStep
      currentSurveySection
      isCurrentAnswerValid
      canSkipCurrentQuestion
      answers
      lastAnsweredQuestion
      savedRewards
      savedQuestions
      email
      productDisplayOrder
      rejectedQuestion
      productDisplayType
      productRewardsRule {
        min
        max
        percentage
        active
      }
    }
  }
`

export default SURVEY_PARTICIPATION_QUERY
