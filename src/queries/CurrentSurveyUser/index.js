import gql from 'graphql-tag'

const CURRENT_SURVEY_USER_QUERY = gql`
  query {
    currentSurveyUser @client {
      lastAnsweredQuestion
      state
      selectedProducts
      surveyEnrollmentId
      paypalEmail
    }
  }
`

export default CURRENT_SURVEY_USER_QUERY
