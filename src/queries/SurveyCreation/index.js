import gql from 'graphql-tag'

export const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      products
      basics {
        name
        title
        coverPhoto
        instructionsText
        instructionSteps
        thankYouText
        rejectionText
        screeningText
        uniqueName
        authorizationType
        exclusiveTasters
        allowRetakes
        forcedAccount
        forcedAccountLocation
        tastingNotes {
          tastingId
          tastingLeader
          customer
          country
          dateOfTasting
          otherInfo
        }
        enabledEmailTypes
        referralAmount
        savedRewards
        recaptcha
        minimumProducts
        maximumProducts
        surveyLanguage
        country
        customizeSharingMessage
        loginText
        pauseText
        isScreenerOnly
        showSurveyProductScreen
        showIncentives
        productDisplayType
        showGeneratePdf
        linkedSurveys
        sharingButtons
        showSharingLink
        validatedData
        compulsorySurvey
        showSurveyScore
        showInternalNameInReports
        disableAllEmails
        reduceRewardInTasting
        includeCompulsorySurveyDataInStats
        showInPreferedLanguage
        addDelayToSelectNextProductAndNextQuestion
        showOnTasterDashboard
        pdfFooterSettings {
          active
          footerNote
        }
        autoAdvanceSettings {
          active
          debounce
          hideNextButton
        }
        promotionSettings {
          active
          showAfterTaster
          code
        }
        promotionOptions {
          promoProducts {
            label
            url
          }
        }
        productRewardsRule {
          active
          min
          max
          percentage
        }
        emails {
          surveyWaiting {
            subject
            html
            text
          }
          surveyCompleted {
            subject
            html
            text
          }
          surveyRejected {
            subject
            html
            text
          }
        }
        promotionOptions {
          promoProducts {
            label
            url
          }
        }
        maxProductStatCount
        allowedDaysToFillTheTasting
        isPaypalSelected
        isGiftCardSelected
        customButtons {
          continue
          start
          next
          skip
        }
        screenOut
        screenOutSettings {
          reject
          rejectAfterStep
        }
        showUserProfileDemographics
        dualCurrency
        referralAmountInDollar
        retakeAfter
        maxRetake
        maximumReward
      }
      questions
      mandatoryQuestions
      uniqueQuestionsToCreate
      editMode
    }
  }
`
