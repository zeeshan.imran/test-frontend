import gql from 'graphql-tag'

import { categoryCardInfo } from '../../fragments/category'

export const CATEGORIES_QUERY = gql`
  query productCategories($key: String) {
    productCategories(rootOnly: false, key: $key) {
      ...categoryCardInfo
    }
  }
  ${categoryCardInfo}
`
