import React, { Component } from 'react'
import { Redirect, Router, Switch, Route } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo'
import { ApolloProvider as ApolloHooksProvider } from 'react-apollo-hooks'
import { message } from 'antd'
import history from './history'
import bootstrapApollo from './config/ApolloClient'
import PageNotFound from './pages/PageNotFound'
import Onboarding from './pages/Onboarding'
import Survey from './pages/Survey'
import Taster from './pages/Taster'
import Operator from './pages/Operator'
import CompleteTasterProfile from './pages/CompleteTasterProfile'
import PrivateRoute from './components/PrivateRoute'
import TermsOfUsePage from './pages/TermsOfUse'
import PrivacyPolicyPage from './pages/PrivacyPolicy'
import UserProfile from './pages/UserProfile'
import i18n from './utils/internationalization/i18n'
import Impersonate from './containers/Impersonate'
import SlaaskProvider from './contexts/SlaaskContext'
import CreateTasterAccount from '../src/pages/CreateTasterAccount'
// import { hotjar } from 'react-hotjar';

import TasterPdf from './containers/TasterPdf'
import PreviewOperatorPpt from './pages/PreviewOperatorPpt'
import PreviewOperatorPdf from './pages/PreviewOperatorPdf'
import PreviewTasterPdf from './pages/PreviewTasterPdf'
import EditOperatorPdfLayout from './pages/EditOperatorPdfLayout'
import EditTasterPdfLayout from './pages/EditTasterPdfLayout'
import { logout as surveyLogout } from './utils/surveyAuthentication'
import { AUTHENTICATION_REDIRECT } from './utils/Constants'

const { BroadcastChannel } = require('broadcast-channel')

const isProduct = window.location.host === 'app.flavorwiki.com'
class App extends Component {
  state = {
    client: null,
    isLoading: true
  }

  addLogOutEventListener () {
    const channel = new BroadcastChannel('logout')
    channel.onmessage = () => {
      const pathname = history.location.pathname
      if (
        pathname.includes('onboarding') ||
        pathname.includes('onboarding') ||
        pathname.includes('create-account') ||
        pathname.includes('terms-of-use') ||
        pathname.includes('privacy-policy') ||
        pathname.includes('/share/') ||
        pathname.includes('/login') ||
        pathname.includes('create-demo-account')
      ) {
        return
      }

      surveyLogout()
      history.push(`${AUTHENTICATION_REDIRECT}/login`)
    }
  }

  async componentDidMount () {
    const currentUrl = window.location.href.split('/')
    let usePersistentStorage = true
    if (currentUrl.includes('survey') && !currentUrl.includes('operator')) {
      usePersistentStorage = false

      // ENABLING HOTJAR
      // hotjar.initialize('1767607', '6');
    }

    const client = await bootstrapApollo(usePersistentStorage)
    this.setState({ client, isLoading: false })

    window.addEventListener('online', () => {
      message.destroy()
      message.success(i18n.t('internet.online'))
    })

    window.addEventListener('offline', () => {
      message.destroy()
      message.error(i18n.t('internet.offline'), 0)
    })
    this.addLogOutEventListener()
  }

  render () {
    const { isLoading, client } = this.state
    if (isLoading) return null

    return (
      <ApolloProvider client={client}>
        <ApolloHooksProvider client={client}>
          <Router history={history}>
            <SlaaskProvider>
              <Switch>
                <Redirect exact path='/' to='/onboarding' />
                <PrivateRoute
                  exact
                  authenticateTaster
                  path={`/taster/complete-profile`}
                  component={CompleteTasterProfile}
                />
                <PrivateRoute
                  authenticateTaster
                  path='/taster'
                  component={Taster}
                />
                {/* TODO: disable STAGFW-342 */}
                {!isProduct && (
                  <Route path='/my-profile' component={UserProfile} />
                )}
                <Route path='/onboarding' component={Onboarding} />
                <Route path='/survey/:surveyId' component={Survey} />
                <Redirect exact path='/operator' to='/operator/dashboard' />
                <PrivateRoute
                  authenticateOperator
                  authenticateAnalytics
                  authenticatePowerUser
                  path='/operator'
                  component={Operator}
                />
                <Route
                  path={`/edit-operator-pdf/:surveyId/:jobGroupId`}
                  component={EditOperatorPdfLayout}
                />
                <Route
                  path={`/edit-taster-pdf/:surveyId/:jobGroupId`}
                  component={EditTasterPdfLayout}
                />
                <Route
                  path={`/preview-operator-ppt/:surveyId/:jobGroupId`}
                  component={PreviewOperatorPpt}
                />
                <Route
                  path={`/preview-operator-pdf/:surveyId/:jobGroupId`}
                  component={PreviewOperatorPdf}
                />
                <Route
                  path={`/preview-taster-pdf/:surveyId/:jobGroupId`}
                  component={PreviewTasterPdf}
                />
                <Route path='/taster-pdf/:surveyId' component={TasterPdf} />
                <Route path='/terms-of-use' component={TermsOfUsePage} />
                <Route path='/privacy-policy' component={PrivacyPolicyPage} />
                <Route path='/impersonate/exit' component={Impersonate} />
                <Route
                  path='/impersonate/:impersonateUserId'
                  component={Impersonate}
                />
                {/* Route For Taster Account Signup */}
                <Route
                  path='/create-account/taster'
                  component={CreateTasterAccount}
                />

                <Route
                  path='/create-demo-account/taster'
                  component={CreateTasterAccount}
                />

                <Route component={PageNotFound} />
              </Switch>
            </SlaaskProvider>
          </Router>
        </ApolloHooksProvider>
      </ApolloProvider>
    )
  }
}
export default App
