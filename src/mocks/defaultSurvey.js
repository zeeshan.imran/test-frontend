import i18n from '../utils/internationalization/i18n'
import { getMessage } from '../utils/getCustomSharingMessage'
import { getAuthenticatedOrganization } from '../utils/userAuthentication'
import { DEFAULT_TASTING_NOTES } from '../utils/Constants'

const { REACT_APP_THEME } = process.env
const hansenTheme = REACT_APP_THEME === 'chrHansen'

const getTranslation = (key, fn) => {
  const t = i18n.getFixedT('en')
  const tKey = `defaultValues.${process.env.REACT_APP_THEME}.${key}`
  const defaultKey = `defaultValues.default.${key}`

  if (fn) {
    return fn(tKey, t) === tKey ? fn(defaultKey, t) : fn(tKey, t)
  }

  return t(tKey) === tKey ? t(defaultKey) : t(tKey)
}

export default {
  editMode: 'default',
  products: [],
  basics: {
    name: null,
    title: null,
    coverPhoto: null,
    instructionsText: getTranslation('instructionsText'),
    instructionSteps: [
      getTranslation('instructionSteps.0'),
      getTranslation('instructionSteps.1'),
      getTranslation('instructionSteps.2'),
      getTranslation('instructionSteps.3')
    ],
    thankYouText: getTranslation('thankYouText', getMessage),
    rejectionText: getTranslation('rejectionText', getMessage),
    screeningText: getTranslation('screeningText', getMessage),
    uniqueName: null,
    authorizationType: 'public',
    exclusiveTasters: [],
    allowRetakes: false,
    isScreenerOnly: false,
    showSurveyProductScreen: false,
    showIncentives: false,
    showGeneratePdf: hansenTheme,
    screenOut: false,
    screenOutSettings: {
      reject: false,
      rejectAfterStep: 1
    },
    optionDisplayType: 'labelOnly',
    productDisplayType: 'none',
    linkedSurveys: [],
    forcedAccount: false,
    forcedAccountLocation: 'start',
    tastingNotes: DEFAULT_TASTING_NOTES,
    referralAmount: 1,
    recaptcha: false,
    savedRewards: [],
    minimumProducts: 1,
    maximumProducts: null,
    surveyLanguage: 'en',
    country: 'US',
    maxProductStatCount: 6,
    customizeSharingMessage: getTranslation(
      'customizeSharingMessage',
      getMessage
    ),
    loginText: getTranslation('loginText'),
    pauseText: getTranslation('pauseText'),
    owner: getAuthenticatedOrganization(),
    allowedDaysToFillTheTasting: 5,
    isPaypalSelected: false,
    isGiftCardSelected: false,
    customButtons: {
      continue: 'Continue',
      start: 'Start',
      next: 'Next',
      skip: 'Skip'
    },
    autoAdvanceSettings: {
      active: false,
      debounce: 0,
      hideNextButton: false
    },
    pdfFooterSettings: {
      active: hansenTheme,
      footerNote: ''
    },
    promotionSettings: {
      active: false,
      showAfterTaster: false,
      code: ''
    },
    promotionOptions: {
      promoProducts: [{ label: null, url: null }]
    },
    sharingButtons: true,
    showSharingLink: true,
    validatedData: false,
    compulsorySurvey: false,
    showSurveyScore: false,
    showInternalNameInReports: hansenTheme,
    disableAllEmails: false,
    reduceRewardInTasting: false,
    includeCompulsorySurveyDataInStats: false,
    showInPreferedLanguage: false,
    addDelayToSelectNextProductAndNextQuestion: false,
    showOnTasterDashboard: false,
    enabledEmailTypes: [],
    emails: {
      surveyWaiting: {
        subject: i18n.t('email.surveyWaiting.subject'),
        html: i18n.t('email.surveyWaiting.html'),
        text: i18n.t('email.surveyWaiting.text')
      },
      surveyCompleted: {
        subject: i18n.t('email.surveyCompleted.subject'),
        html: i18n.t('email.surveyCompleted.html'),
        text: i18n.t('email.surveyCompleted.text')
      },
      surveyRejected: {
        subject: i18n.t('email.surveyRejected.subject'),
        html: i18n.t('email.surveyRejected.html'),
        text: i18n.t('email.surveyRejected.text')
      }
    },
    productRewardsRule: {
      active: false,
      min: 0,
      max: 0,
      percentage: 0
    },
    showUserProfileDemographics: false,
    dualCurrency: false,
    referralAmountInDollar: 1,
    retakeAfter: 1,
    maxRetake: 1,
    maximumReward: 0
  },
  questions: [],
  mandatoryQuestions: [],
  uniqueQuestionsToCreate: []
}
