const { REACT_APP_THEME } = process.env
const themeName = REACT_APP_THEME || 'default'

const defaultTheme = {
  fontFamily_axis: 'Lato',
  fontFamily_labels: 'Lato',
  fontFamily_legend: 'Lato-Bold',
  fontFamily_values: 'Lato',
  isTotalRespondentsShown: true,
  isTotalRespondentsProProductShown: false,
  chartWidth: 1,
  howManyDecimals: 1,
  isScaleShown: true,
  scaleRange: '10',
  howManyDecimals_axis: 1,
  fontSize_axis: 1,
  fontSize_labels: 1,

  fontSize_legend: 1.1,
  fontSize_values: 1,
  valueType: 'value',
  percentageGrouping: 'inner_group',
  isCountProGroupShown: true,
  isMeanProProductShown_howManyDecimals: 1,
  isLabelsShown: true,
  sortOrder: 'label.asc',
  isDataTableShown: true,
  isDataTableShown_format: 'abs',
  hasOutline_width: 1,
  colorsArr: [
    '#3497db',
    '#e74c3c',
    '#2fcc71',
    '#f1c40f',
    '#8e44ad',
    '#95a5a7',
    '#ff7f0e',
    '#1abc9c',
    '#8c564b',
    '#e377c2',
    '#c3cfe1',
    '#7a0f2d'
  ],
  mapChartColorsArr: ['#3497db', '#b4d69a'],
  outlineColor: '#424242'
}

const chrHansenSettings = {
  isTotalRespondentsShown: true,
  howManyDecimals: 1,
  howManyDecimals_axis: 1,
  valueType: 'value',
  isCountProLabelShown: true,
  percentageGrouping: 'inner_group',
  isCountProGroupShown: true,
  isProductLabelsSwapped: true,
  isStackedLabelsReversed: true,
  isMeanProLabelsShown: true,
  sortOrder: 'label.asc',
  isDataTableShown: true,
  isDataTableShown_format: 'abs',
  isDataTableShown_isPaginationEnabled: true,
  isStatisticsTableShown: false,
  statisticsTable_howManyDecimals: 1,
  isStatisticsTableMeanShown: true,
  isStatisticsTableStdevShown: false,
  hasOutline: true,
  hasOutline_width: 1,

  fontFamily_axis: 'Lato',
  fontFamily_labels: 'Lato',
  fontFamily_legend: 'Lato-Bold',
  fontFamily_values: 'Lato',
  fontSize_axis: 1,
  fontSize_labels: 1,
  fontSize_legend: 1.1,
  fontSize_values: 1,

  colorsArr: [
    // colors at 100%:
    '#003a72',
    '#fcb813',
    '#007770',
    '#c30c3e',
    '#c3cfe1',
    '#f37321',
    '#009864',
    '#9b9b9b',
    '#7a0f2d',
    '#4c6c9c',
    '#4a4a4a',
    '#004b88'
  ],
  mapChartColorsArr: ['#c3cfe1', '#9b9b9b'],
  outlineColor: '#c3cfe1'
}

const settings = {
  default: defaultTheme,
  chrHansen: chrHansenSettings
}

export const defaultChartSettings = settings[themeName]
  ? settings[themeName]
  : defaultTheme
