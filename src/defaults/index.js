import currentSurveyParticipationDefaults from './currentSurveyParticipation'
import surveyCreationDefaults from './surveyCreation'
import currency from './currency'
import countries from './countries'
import constants from "./constants";

export default {
  ...currentSurveyParticipationDefaults,
  ...surveyCreationDefaults,
  ...currency,
  ...countries,
  ...constants
}
 