const defaultQuestions = {
  'paired-questions': {
    hasDemographic: false,
    showProductImage: true
  },
  'choose-one': {
    hasDemographic: false,
    showProductImage: true,
    considerValueForScore: true,
    skipFlow: {
      type: 'MATCH_OPTION',
      rules: []
    }
  },
  'choose-product': {
    hasDemographic: false,
    skipFlow: {
      type: 'MATCH_PRODUCT',
      rules: []
    },
    type: 'choose-product',
    displayOn: 'middle',
    chooseProductOptions: {
      minimumProducts: 1,
      maximumProducts: 1
    },
    required: true
  },
  'paypal-email': {
    hasDemographic: false,
    type: 'paypal-email',
    displayOn: 'payments',
    required: true
  },
  dropdown: {
    hasDemographic: false,
    showProductImage: true,
    considerValueForScore: true,
    skipFlow: {
      type: 'MATCH_OPTION',
      rules: []
    }
  },
  'select-and-justify': {
    hasDemographic: false,
    showProductImage: true,
    skipFlow: {
      type: 'MATCH_OPTION',
      rules: []
    }
  },
  'vertical-rating': {
    hasDemographic: false,
    showProductImage: true,
    considerValueForScore: true,
    range: {
      labels: [],
      min: 1,
      max: 9
    },
    skipFlow: {
      type: 'MATCH_RANGE',
      rules: []
    }
  },
  numeric: {
    hasDemographic: false,
    showProductImage: true,
    numericOptions: {
      decimalNumbers: 0
    }
  },
  'choose-multiple': {
    hasDemographic: false,
    showProductImage: true,
    considerValueForScore: true,
    settings: {
      chooseMultiple: null,
      maxAnswerValues: null,
      minAnswerValues: null
    },
    skipFlow: {
      type: 'MATCH_MULTIPLE_OPTION',
      rules: []
    }
  },
  'choose-payment': {
    hasDemographic: false,
    type: 'choose-payment',
    displayOn: 'payments',
    hasPaymentOptions: true,
    required: true,
    options: [
      {
        label: 'Paypal',
        value: '10',
        editMode: 'strict'
      },
      {
        label: 'Gift Card',
        value: '11',
        editMode: 'strict'
      }
    ]
  },
  matrix: {
    hasDemographic: false,
    considerValueForScore: true,
    skipFlow: {
      type: 'MATCH_MATRIX_OPTION',
      rules: []
    }
  },
  'show-product-screen': {
    hasDemographic: false,
    type: 'show-product-screen',
    displayOn: 'begin',
    required: true
  },
  'open-answer': {
    hasDemographic: false,
    chartTitle: '',
    chartTopic: '',
    chartType: ''
  },
  email: {
    hasDemographic: false,
    chartTitle: '',
    chartTopic: '',
    chartType: ''
  },
  info: {
    hasDemographic: false,
    chartTitle: '',
    chartTopic: '',
    chartType: ''
  },
  'upload-picture': {
    hasDemographic: false,
    chartTitle: '',
    chartTopic: '',
    chartType: ''
  }
}

export default defaultQuestions
