export default {
  currentSurveyParticipation: {
    __typename: 'CurrentSurveyParticipation',
    surveyId: null,
    surveyEnrollmentId: null,
    state: null,
    selectedProducts: [],
    selectedProduct: null,
    paypalEmail: null,
    currentSurveyStep: null,
    currentSurveySection: null,
    isCurrentAnswerValid: false,
    canSkipCurrentQuestion: false,
    answers: [],
    lastAnsweredQuestion: null,
    country: '',
    savedRewards: [],
    savedQuestions: [],
    email: null,
    productDisplayOrder: [],
    rejectedQuestion : [],
    productDisplayType: '',
    productRewardsRule: {
      __typename: 'ProductRewardsRule',
      active: false,
      min: 0,
      max: 0,
      percentage:0
    }
  }
}
