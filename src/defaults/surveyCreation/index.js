import i18n from '../../utils/internationalization/i18n'
import { getMessage } from '../../utils/getCustomSharingMessage'
import { DEFAULT_TASTING_NOTES } from '../../utils/Constants'

const { REACT_APP_THEME } = process.env
const defaultTheme = REACT_APP_THEME === 'default'
const hansenTheme = REACT_APP_THEME === 'chrHansen'
const getTranslation = (key, fn) => {
  const tKey = `defaultValues.${process.env.REACT_APP_THEME}.${key}`
  const defaultKey = `defaultValues.default.${key}`

  if (fn) {
    return fn(tKey, (key, parameter) => i18n.t(key, parameter)) === tKey
      ? fn(defaultKey, (key, parameter) => i18n.t(key, parameter))
      : fn(tKey, (key, parameter) => i18n.t(key, parameter))
  }

  return i18n.t(tKey) === tKey ? i18n.t(defaultKey) : i18n.t(tKey)
}

const defaultSurveyCreation = {
  surveyCreation: {
    __typename: 'SurveyCreation',
    editMode: 'default',
    products: [],
    basics: {
      __typename: 'BasicInfo',
      name: null,
      title: '',
      instructionsText: getTranslation('instructionsText'),
      instructionSteps: [
        getTranslation('instructionSteps.0'),
        getTranslation('instructionSteps.1'),
        getTranslation('instructionSteps.2'),
        getTranslation('instructionSteps.3')
      ],
      thankYouText: getTranslation('thankYouText'),
      rejectionText: getTranslation('rejectionText'),
      screeningText: getTranslation('screeningText'),
      uniqueName: null,
      authorizationType: 'public',
      coverPhoto: null,
      exclusiveTasters: [],
      allowRetakes: false,
      forcedAccount: false,
      forcedAccountLocation: 'start',
      tastingNotes: {
        __typename: 'SurveyCreationTastingNotes',
        ...DEFAULT_TASTING_NOTES
      },
      referralAmount: 0,
      recaptcha: false,
      savedRewards: [],
      minimumProducts: 1,
      maximumProducts: null,
      surveyLanguage: 'en',
      country: 'US',
      maxProductStatCount: 6,
      allowedDaysToFillTheTasting: 5,
      customizeSharingMessage: getTranslation(
        'customizeSharingMessage',
        getMessage
      ),
      loginText: getTranslation('loginText'),
      pauseText: getTranslation('pauseText'),
      isPaypalSelected: false,
      isGiftCardSelected: false,
      customButtons: {
        __typename: 'CustomButtons',
        continue: 'Continue',
        start: 'Start',
        next: 'Next',
        skip: 'Skip'
      },
      isScreenerOnly: false,
      screenOut: false,
      screenOutSettings: {
        __typename: 'ScreenOutSettings',
        reject: false,
        rejectAfterStep: 1
      },
      showSurveyProductScreen: false,
      showIncentives: false,
      productDisplayType: 'none',
      showGeneratePdf: hansenTheme,
      linkedSurveys: [],
      sharingButtons: true,
      showSharingLink: defaultTheme,
      validatedData: false,
      compulsorySurvey: false,
      showSurveyScore: false,
      showInternalNameInReports: hansenTheme,
      disableAllEmails: false,
      reduceRewardInTasting: false,
      includeCompulsorySurveyDataInStats: false,
      showInPreferedLanguage: false,
      addDelayToSelectNextProductAndNextQuestion: false,
      showOnTasterDashboard: false,
      autoAdvanceSettings: {
        __typename: 'AutoAdvanceSettings',
        active: false,
        debounce: 0,
        hideNextButton: false
      },
      pdfFooterSettings: {
        __typename: 'PdfFooterSettings',
        active: hansenTheme,
        footerNote: ''
      },
      promotionSettings: {
        __typename: 'PromotionSettings',
        active: false,
        showAfterTaster: false,
        code: ''
      },
      promotionOptions: {
        __typename: 'PromotionOptions',
        promoProducts: [
          {
            __typename: 'PromotionProduct',
            label: '',
            url: ''
          }
        ]
      },
      enabledEmailTypes: [],
      emails: {
        __typename: 'SurveyCreationEmails',
        surveyWaiting: {
          __typename: 'SurveyCreationEmail',
          subject: i18n.t('email.surveyWaiting.subject'),
          html: i18n.t('email.surveyWaiting.html'),
          text: i18n.t('email.surveyWaiting.text')
        },
        surveyCompleted: {
          __typename: 'SurveyCreationEmail',
          subject: i18n.t('email.surveyCompleted.subject'),
          html: i18n.t('email.surveyCompleted.html'),
          text: i18n.t('email.surveyCompleted.text')
        },
        surveyRejected: {
          __typename: 'SurveyCreationEmail',
          subject: i18n.t('email.surveyRejected.subject'),
          html: i18n.t('email.surveyRejected.html'),
          text: i18n.t('email.surveyRejected.text')
        }
      },
      productRewardsRule: {
        __typename: 'ProductRewardsRule',
        active: false,
        min: 0,
        max: null,
        percentage: null
      },
      showUserProfileDemographics: false,
      dualCurrency: false,
      referralAmountInDollar: 1,
      retakeAfter: 1,
      maxRetake: 1,
      maximumReward: 0
    },
    questions: [],
    mandatoryQuestions: [],
    uniqueQuestionsToCreate: []
  }
}

export default defaultSurveyCreation
