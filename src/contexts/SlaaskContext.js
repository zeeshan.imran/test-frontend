import React, { useContext, useMemo } from 'react'
import { getAuthenticatedUser } from '../utils/userAuthentication'
import checkSlaaskLocation from '../utils/checkSlaaskLocation'
import history from '../history'
import { withRouter } from 'react-router-dom'
import { generate } from 'shortid'

const keys = {
  default: 'spk-06e763ca-ff93-44d8-b15d-5ed5f0e8a273'
  // key for special users base on the user type
  // operator: 'spk-a7111c16-e1a8-4723-a113-8a8755d69d4d'
}

const SlaaskContext = React.createContext()

const SlaaskProvider = ({ children }) => {
  const { location } = history

  const value = useMemo(() => {
    const { _slaaskLoader } = window

    const reloadSlaask = async () => {
      if (!checkSlaaskLocation(location)) {
        const user = getAuthenticatedUser()
        const key = keys.default
        window._slaaskSettings = {
          identify: () => {
            if (!user)
              return {
                id: generate(),
                name: `Guest`
              }
            return {
              id: user.id,
              name: user.fullName || user.emailAddress,
              email: user.emailAddress,
              type: user.type
            }
          },
          key
        }
        if (_slaaskLoader && !_slaaskLoader._loaded) {
          await _slaaskLoader._load()
          await window._slaaskSettings.identify()
        } else if (window._slaask && checkSlaaskLocation(location)) {
          window._slaask.identifyContact()
        }
      } else if (window._slaask && checkSlaaskLocation(location)) {
        _slaaskLoader._loaded = false
        window._slaask.destroy()
      }
    }
    reloadSlaask()

    return {
      reloadSlaask
    }
  }, [location])

  return (
    <SlaaskContext.Provider value={value}>{children}</SlaaskContext.Provider>
  )
}

export default withRouter(SlaaskProvider)

export const useSlaask = () => useContext(SlaaskContext)
