import React, { useMemo, useContext, useState } from 'react'

const ChartContext = React.createContext({
  showAll: false,
  printView: false
})

export const ChartContextProvider = ({
  showAll: initShowAll = false,
  printView: initPrintView = false,
  children
}) => {
  const [showAll, setShowAll] = useState(initShowAll)
  const [printView, setPrintView] = useState(initPrintView)
  const value = useMemo(
    () => ({ showAll, printView, setShowAll, setPrintView }),
    [showAll, printView]
  )
  return <ChartContext.Provider value={value}>{children}</ChartContext.Provider>
}

export const useChartContext = () => {
  return useContext(ChartContext)
}
