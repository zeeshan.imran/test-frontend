import gql from 'graphql-tag'

export const RESET_SIGN_UP_FORM = gql`
  mutation {
    resetSignUpForm @client
  }
`
