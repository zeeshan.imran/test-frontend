import gql from 'graphql-tag'

export const SUBMIT_SURVEY_ANSWERS = gql`
  mutation submitSurveyAnswers(
    $surveyId: ID!
    $participationEmail: String!
    $answers: [Answer]
  ) {
    submitSurveyAnswers(
      surveyId: $surveyId
      participationEmail: $participationEmail
      answers: $answers
    ) {
      ok
      error
    }
  }
`
