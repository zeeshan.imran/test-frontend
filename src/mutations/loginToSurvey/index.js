import gql from 'graphql-tag'

export const LOGIN_TO_SURVEY = gql`
  mutation loginToSurvey(
    $email: String
    $survey: String!
    $referral: ID
    $extsource: String
    $extid: String
    $isUserLoggedIn: Boolean,
    $password: String
    $country: String
    $browserInfo: JSON
  ) {
    loginToSurvey(
      email: $email
      survey: $survey
      referral: $referral
      extsource: $extsource
      extid: $extid
      isUserLoggedIn: $isUserLoggedIn
      password: $password
      country: $country
      browserInfo: $browserInfo
    ) {
      id
      lastAnsweredQuestion {
        id
      }
      state
      selectedProducts {
        id
        name
      }
      paypalEmail
      savedRewards
      savedQuestions
      answers
      lastSelectedProduct
      productDisplayOrder
      productDisplayType
      productRewardsRule {
        min
        max
        percentage
        active
      }
    }
  }
`
