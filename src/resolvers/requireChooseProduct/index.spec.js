import requireChooseProduct from './index'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'

const mockSurveyCreation = {
  surveyId: 'survey-1',
  products: [
    {
      id: 'product-1',
      name: 'product',
      brand: 'brand'
    }
  ],
  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?'
    },
    {
      id: 'question-2',
      type: 'choose-product',
      prompt: 'Product namem'
    },
    {
      id: 'question-3',
      type: 'name',
      prompt: 'What is your name?'
    }
  ],
  mandatoryQuestions: ['question-2', 'question-3'],
  uniqueQuestionsToCreate: []
}

const mockSurveyCreationNotChooseProduct = {
  surveyId: 'survey-1',
  products: [
    {
      id: 'product-1',
      name: 'product',
      brand: 'brand'
    }
  ],
  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?'
    },
    {
      id: 'question-3',
      type: 'name',
      prompt: 'What is your name?'
    }
  ],
  mandatoryQuestions: ['question-1'],
  uniqueQuestionsToCreate: []
}

describe('requireChooseProduct', () => {
  const mockCache = {
    readQuery: jest.fn(),
    writeQuery: jest.fn()
  }
  beforeEach(() => jest.resetAllMocks())

  test('should call SURVEY_CREATION query for requireChooseProduct', () => {
    mockCache.readQuery.mockReturnValueOnce({
      surveyCreation: mockSurveyCreation
    })

    requireChooseProduct({}, {}, { cache: mockCache })
    expect(mockCache.readQuery).toHaveBeenCalledTimes(1)
    expect(mockCache.readQuery).toHaveBeenCalledWith({
      query: SURVEY_CREATION
    })
  })

  test('should call SURVEY_CREATION query for requireChooseProduct choose-product question null', () => {
    mockCache.readQuery.mockReturnValueOnce({
      surveyCreation: mockSurveyCreationNotChooseProduct
    })

    requireChooseProduct({}, {}, { cache: mockCache })
    expect(mockCache.readQuery).toHaveBeenCalledTimes(1)
    expect(mockCache.readQuery).toHaveBeenCalledWith({
      query: SURVEY_CREATION
    })
  })
})
