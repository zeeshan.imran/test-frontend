import { SURVEY_CREATION } from '../../queries/SurveyCreation'

export default (
  _,
  { questionIndex, questionField, mandatoryQuestion, question },
  { cache }
) => {
  const { surveyCreation } = cache.readQuery({ query: SURVEY_CREATION })

  let targetQuestions = surveyCreation.questions
  let targetProperty = 'questions'

  if (mandatoryQuestion) {
    targetQuestions = surveyCreation.mandatoryQuestions
    targetProperty = 'mandatoryQuestions'
    if(question) {
      let qType = question.type
      questionIndex = targetQuestions.findIndex((item) => {
        return item.type === qType
      });
    }
  }
  
  const updatedQuestions = [
    ...targetQuestions.slice(0, questionIndex),
    { ...targetQuestions[questionIndex], ...questionField },
    ...targetQuestions.slice(questionIndex + 1)
  ]
  
  cache.writeQuery({
    query: SURVEY_CREATION,
    data: {
      surveyCreation: {
        __typename: 'SurveyCreation',
        ...surveyCreation,
        [targetProperty]: updatedQuestions
      }
    }
  })

  return null
}
