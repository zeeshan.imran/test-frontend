import updateSurveyCreationQuestion from './index'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'

describe('updateSurveyCreationQuestion', () => {
  const mockCache = {
    readQuery: jest.fn(),
    writeQuery: jest.fn()
  }
  beforeEach(() => jest.resetAllMocks())

  test('should call SURVEY_CREATION query for mandatoryQuestions', () => {
    mockCache.readQuery.mockReturnValueOnce({
      surveyCreation: {
        mandatoryQuestions: [
          {
            id: 'question-1',
            type: 'email',
            prompt: 'What is your email?'
          }
        ],
        questions: [
          {
            id: 'question-1',
            type: 'email',
            prompt: 'What is your email?'
          }
        ]
      }
    })

    updateSurveyCreationQuestion(
      {},
      {
        questionIndex: 1,
        questionField: 'email',
        mandatoryQuestion: 'question-1'
      },
      { cache: mockCache }
    )
    expect(mockCache.readQuery).toHaveBeenCalledTimes(1)
    expect(mockCache.readQuery).toHaveBeenCalledWith({
      query: SURVEY_CREATION
    })
  })
})
