import { SURVEY_CREATION } from '../../queries/SurveyCreation'

export default (_, __, { cache }) => {
  const {
    surveyCreation
  } = cache.readQuery({ query: SURVEY_CREATION })

  const { mandatoryQuestions } = surveyCreation

  const requiredQuestionIndex = mandatoryQuestions.findIndex(
    q => q.type === 'paypal-email'
  )
  
  if (requiredQuestionIndex === -1) {
    return
  }

  const updatedMandatoryQuestions = [
    ...mandatoryQuestions.slice(0, requiredQuestionIndex),
    ...mandatoryQuestions.slice(requiredQuestionIndex + 1)
  ]
  
  cache.writeQuery({
    query: SURVEY_CREATION,
    data: {
      surveyCreation: {
        ...surveyCreation,
        basics: {
          ...surveyCreation.basics
        },
        __typename: 'SurveyCreation',
        mandatoryQuestions: updatedMandatoryQuestions,
      }
    }
  })

  return null
}
