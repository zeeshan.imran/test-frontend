import { filter, propEq, omit, uniq, without } from 'ramda'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import defaultQuestions from '../../defaults/questions'

const SECTION_ORDER = {
  screening: 1,
  begin: 2,
  middle: 3,
  end: 4
}

const getLinkedProfileQuestion = (allQuestions, question, questionIndex) => {
  const nextQuestions = allQuestions.slice(questionIndex)

  return nextQuestions.find(
    q =>
      q.type === 'profile' &&
      q.relatedQuestions &&
      q.relatedQuestions[0] === question.id // for now we only support one related question
  )
}

const associateProfileQuestion = (pairedQuestion, profileQuestion) => {
  const combinedQuestion = { ...pairedQuestion }
  const updatedPairsOptions = {
    ...pairedQuestion.pairsOptions,
    hasFollowUpProfile: true,
    profileQuestion
  }

  return { ...combinedQuestion, pairsOptions: updatedPairsOptions }
}

export default (_, { questions = [] }, { cache }) => {
  let incomingQuestionsToDelete = []

  let formattedQuestions = questions
    .map(q => ({
      ...q,
      skipFlow:
        q.skipFlow ||
        (defaultQuestions[q.type] && defaultQuestions[q.type].skipFlow)
    }))
    .map((q, index) => {
      if (q.type === 'paired-questions' && q.pairs && q.pairs.length > 0) {
        const profileQuestion = getLinkedProfileQuestion(questions, q, index)
        const pairsLabels = uniq(
          q.pairs.reduce(
            (acc, pair) => [...acc, pair.leftAttribute, pair.rightAttribute],
            []
          )
        )

        let formattedPairQuestion = { ...q, pairs: pairsLabels }

        if (profileQuestion) {
          // if there's an associated profile question
          // combine it with the paired question
          formattedPairQuestion = associateProfileQuestion(
            formattedPairQuestion,
            profileQuestion
          )
          // and mark to delete the "independent" profile question
          incomingQuestionsToDelete = [
            ...incomingQuestionsToDelete,
            profileQuestion
          ]
        }

        return formattedPairQuestion
      }
      if (q.type === 'slider') {
        const profileQuestion = getLinkedProfileQuestion(questions, q, index)
        let formattedPairQuestion = { ...q }
        if (profileQuestion) {
          // if there's an associated profile question
          // combine it with the slider question
          formattedPairQuestion = associateProfileQuestion(
            formattedPairQuestion,
            profileQuestion
          )
          // and mark to delete the "independent" profile question
          incomingQuestionsToDelete = [
            ...incomingQuestionsToDelete,
            profileQuestion
          ]
        }

        return formattedPairQuestion
      }
      return q
    }, [])
    .sort((qA, qB) => {
      const sortedBySection =
        SECTION_ORDER[qA.displayOn] - SECTION_ORDER[qB.displayOn]
      if (sortedBySection === 0) {
        // in same section
        return qA.order - qB.order
      } else {
        return sortedBySection
      }
    })
    .filter(q => q.type !== 'profile') // we won't be rendering profile questions

  const { surveyCreation: cachedData = { products: [] } } = cache.readQuery({
    query: SURVEY_CREATION
  })

  const restOfCachedData = omit(['questions'], cachedData)
  const hasManyProducts = cachedData.products.length > 0
  let mandatoryQuestions = []

  if (hasManyProducts) {
    mandatoryQuestions = filter(propEq('type', 'choose-product'))(
      formattedQuestions
    )
  }

  cache.writeQuery({
    query: SURVEY_CREATION,
    data: {
      surveyCreation: {
        __typename: 'SurveyCreation',
        ...restOfCachedData,
        mandatoryQuestions,
        questions: without(mandatoryQuestions, formattedQuestions)
      }
    }
  })
}
