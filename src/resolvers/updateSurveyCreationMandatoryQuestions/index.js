import { SURVEY_CREATION } from '../../queries/SurveyCreation'

export default (_, { mandatoryQuestions }, { cache }) => {
  const { surveyCreation } = cache.readQuery({ query: SURVEY_CREATION })

  cache.writeQuery({
    query: SURVEY_CREATION,
    data: {
      surveyCreation: {
        ...surveyCreation,
        __typename: 'SurveyCreation',
        mandatoryQuestions
      }
    }
  })

  return null
}
