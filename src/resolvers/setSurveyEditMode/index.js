/** @typedef {import("apollo-cache").ApolloCache} ApolloCache */
import { SURVEY_CREATION } from '../../queries/SurveyCreation'

const buildSetProductEditMode = editMode => product => ({
  ...product,
  editMode
})

const buildSetQuestionEditMode = editMode => ({ options, ...rest }) => ({
  ...rest,
  options: Array.isArray(options)
    ? options.map(op => ({ ...op, editMode }))
    : options,
  editMode
})

/**
 * @param {*} _
 * @param {*} param1
 * @param {{ cache: ApolloCache }} param2
 */
const setSurveyEditMode = (_, { editMode = 'strict' }, { cache }) => {
  const { surveyCreation } = cache.readQuery({ query: SURVEY_CREATION })
  const setProductEditMode = buildSetProductEditMode(editMode)
  const setQuestionEditMode = buildSetQuestionEditMode(editMode)

  cache.writeQuery({
    query: SURVEY_CREATION,
    data: {
      surveyCreation: {
        ...surveyCreation,
        products: surveyCreation.products.map(setProductEditMode),
        questions: surveyCreation.questions.map(setQuestionEditMode),
        mandatoryQuestions: surveyCreation.mandatoryQuestions.map(setQuestionEditMode)
      }
    }
  })
}

export default setSurveyEditMode
