import { remove } from 'ramda'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import { UNIQUE_QUESTION_TYPES } from '../../utils/Constants'
import skipFlowUtils from '../../utils/skipFlowUtils'

const removeLinkedSkipRule = (questions, idToDelete) =>
  questions.map(question => {
    if (!skipFlowUtils.hasSkipFlow(question)) {
      return question
    }
    return {
      ...question,
      skipFlow: skipFlowUtils
        .fromSkipFlow(question.skipFlow)
        .removeRules(rule => rule.skipTo === idToDelete)
        .getNextSkipFlow()
    }
  })

export default (_, { questionIndex }, { cache }) => {
  const {
    surveyCreation: {
      questions,
      mandatoryQuestions,
      ...restOfExistingData
    }
  } = cache.readQuery({ query: SURVEY_CREATION })

  const updatedQuestions = remove(questionIndex, 1, questions)

  const uniqueTypesPresent = updatedQuestions
    .filter(({ type }) => UNIQUE_QUESTION_TYPES[type])
    .map(({ type }) => type)

  const questionToDelete = questions[questionIndex]
  const idToDelete = questionToDelete.id || questionToDelete.clientGeneratedId

  cache.writeQuery({
    query: SURVEY_CREATION,
    data: {
      surveyCreation: {
        __typename: 'SurveyCreation',
        ...restOfExistingData,
        questions: removeLinkedSkipRule(updatedQuestions, idToDelete),
        mandatoryQuestions: removeLinkedSkipRule(
          mandatoryQuestions,
          idToDelete
        ),
        uniqueQuestionsToCreate: uniqueTypesPresent
      }
    }
  })

  return null
}
