import { pick, omit, pluck, map } from 'ramda'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import surveyCreationDefaults from '../../defaults/surveyCreation'

const pickBasics = [
  'name',
  'title',
  'coverPhoto',
  'instructionsText',
  'instructionSteps',
  'thankYouText',
  'rejectionText',
  'screeningText',
  'uniqueName',
  'authorizationType',
  'recaptcha',
  'minimumProducts',
  'maximumProducts',
  'surveyLanguage',
  'customButtons',
  'exclusiveTasters',
  'allowRetakes',
  'isScreenerOnly',
  'showSurveyProductScreen',
  'showIncentives',
  'showGeneratePdf',
  'optionDisplayType',
  'productDisplayType',
  'linkedSurveys',
  'forcedAccount',
  'forcedAccountLocation',
  'tastingNotes',
  'autoAdvanceSettings',
  'pdfFooterSettings',
  'promotionSettings',
  'promotionOptions',
  'sharingButtons',
  'showSharingLink',
  'validatedData',
  'compulsorySurvey',
  'showSurveyScore',
  'showInternalNameInReports',
  'disableAllEmails',
  'reduceRewardInTasting',
  'includeCompulsorySurveyDataInStats',
  'showInPreferedLanguage',
  'addDelayToSelectNextProductAndNextQuestion',
  'showOnTasterDashboard',
  'enabledEmailTypes',
  'emails',
  'referralAmount',
  'savedRewards',
  'country',
  'maxProductStatCount',
  'customizeSharingMessage',
  'loginText',
  'pauseText',
  'allowedDaysToFillTheTasting',
  'isPaypalSelected',
  'isGiftCardSelected',
  'screenOut',
  'screenOutSettings',
  'productRewardsRule',
  'showUserProfileDemographics',
  'dualCurrency',
  'referralAmountInDollar',
  'retakeAfter',
  'maxRetake',
  'maximumReward'
]

const addSurveyCreationEmailTypename = map((email) => ({
  ...email,
  __typename: 'SurveyCreationEmail'
}))

export default (_, { survey }, { cache }) => {
  const {
    surveyCreation: { basics: basicsDefaults }
  } = surveyCreationDefaults

  const basics = pick(pickBasics, survey)
  const { settings: { recaptcha } = {}, linkedSurveys } = survey
  const { surveyCreation = { basics: {} } } = cache.readQuery({
    query: SURVEY_CREATION
  })
  const remainingData = omit(['basics'], surveyCreation)

  const availableLinkedSurveys = linkedSurveys.filter(
    (survey) => survey.state !== 'deprecated'
  )

  cache.writeQuery({
    query: SURVEY_CREATION,
    data: {
      surveyCreation: {
        __typename: 'SurveyCreation',
        basics: {
          __typename: 'BasicInfo',
          ...basicsDefaults,
          recaptcha,
          ...basics,
          emails: {
            ...addSurveyCreationEmailTypename(basics.emails || {}),
            __typename: 'SurveyCreationEmails'
          },
          tastingNotes: {
            ...basics.tastingNotes,
            __typename: 'SurveyCreationTastingNotes'
          },
          maximumProducts:
            basics.maximumProducts ||
            (survey.products && survey.products.length) ||
            0,
          exclusiveTasters: (basics.exclusiveTasters || []).map(
            ({ emailAddress }) => emailAddress
          ),
          customButtons: {
            __typename: 'CustomButtons',
            ...basics.customButtons
          },
          productRewardsRule: {
            __typename: 'ProductRewardsRule',
            ...basics.productRewardsRule
          },
          showUserProfileDemographics: basics.showUserProfileDemographics,
          linkedSurveys: availableLinkedSurveys
            ? pluck('id', availableLinkedSurveys)
            : []
        },
        ...remainingData
      }
    }
  })
}
