import { map, assocPath, any } from 'ramda'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import skipFlowUtils from '../../utils/skipFlowUtils'

const setMaximumProducts = assocPath([
  'chooseProductOptions',
  'maximumProducts'
])

const updateChooseProductQuestion = (products, lastProducts) => question => {
  if (question.type !== 'choose-product') {
    return question
  }

  const { chooseProductOptions } = question

  if (chooseProductOptions.maximumProducts === lastProducts.length) {
    return setMaximumProducts(products.length, question)
  }

  if (chooseProductOptions.maximumProducts > products.length) {
    return setMaximumProducts(products.length, question)
  }

  if (skipFlowUtils.hasSkipFlow(question)) {
    return {
      ...question,
      skipFlow: skipFlowUtils
        .fromSkipFlow(question.skipFlow)
        .removeRules(
          rule => !any(product => product.id === rule.value, products)
        )
        .getNextSkipFlow()
    }
  }

  return question
}
export default (_, { products }, { cache }) => {
  const { surveyCreation } = cache.readQuery({ query: SURVEY_CREATION })

  const mapChooseProductQuestion = map(
    updateChooseProductQuestion(products, surveyCreation.products)
  )

  cache.writeQuery({
    query: SURVEY_CREATION,
    data: {
      surveyCreation: {
        ...surveyCreation,
        questions: mapChooseProductQuestion(surveyCreation.questions),
        mandatoryQuestions: mapChooseProductQuestion(
          surveyCreation.mandatoryQuestions
        ),
        basics: {
          ...surveyCreation.basics
        },
        __typename: 'SurveyCreation',
        products
      }
    }
  })

  return null
}
