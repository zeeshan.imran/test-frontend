import { uniq, pick, clone } from 'ramda'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import {
  UNIQUE_QUESTION_TYPES,
  NEVER_REQUIRED_QUESTIONS_TYPES
} from '../../utils/Constants'
import defaultQuestions from '../../defaults/questions'

const UNCHANGED_PROPS = [
  'id',
  'clientGeneratedId',
  'prompt',
  'secondaryPrompt',
  'type',
  'displayOn',
  'image'
]

const cleanQuestion = (question, nextType) => {
  if (nextType === question.type) {
    return
  }

  let cleanQuestion = pick(UNCHANGED_PROPS, question)
  return cleanQuestion
}

export const getNextQuestion = (question, nextType) => {
  return {
    ...clone(defaultQuestions[nextType]),
    ...cleanQuestion(question, nextType),
    required: !NEVER_REQUIRED_QUESTIONS_TYPES.has(nextType),
    type: nextType
  }
}

export default (_, { questionIndex, questionType }, { cache }) => {
  const { surveyCreation } = cache.readQuery({ query: SURVEY_CREATION })

  const { questions, mandatoryQuestions } = surveyCreation

  const updatedQuestions = [
    ...questions.slice(0, questionIndex),
    getNextQuestion(questions[questionIndex], questionType),
    ...questions.slice(questionIndex + 1)
  ]

  const uniqueTypesPresent = [...updatedQuestions, ...mandatoryQuestions]
    .filter(({ type }) => UNIQUE_QUESTION_TYPES[type])
    .map(({ type }) => type)

  cache.writeQuery({
    query: SURVEY_CREATION,
    data: {
      surveyCreation: {
        ...surveyCreation,
        __typename: 'SurveyCreation',
        questions: updatedQuestions,
        uniqueQuestionsToCreate: uniq(uniqueTypesPresent)
      }
    }
  })

  return null
}
