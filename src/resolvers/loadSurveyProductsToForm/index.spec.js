import loadSurveyProductsToForm from './index'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'

describe('loadSurveyProductsToForm', () => {
  const mockCache = {
    readQuery: jest.fn(),
    writeQuery: jest.fn()
  }
  beforeEach(() => jest.resetAllMocks())

  test('should call SURVEY_CREATION query', () => {
    mockCache.readQuery.mockReturnValueOnce({
      surveyCreation: {
        products: {}
      }
    })

    const products = [
      {
        id: 'product-1',
        name: 'product',
        brand: 'brand'
      }
    ]

    const brands = [
      { id: 'brand-1', name: 'Brand 1' },
      { id: 'brand-2', name: 'Brand 2' }
    ]

    const argument = { products, brands }
    loadSurveyProductsToForm({}, argument, { cache: mockCache })
    expect(mockCache.readQuery).toHaveBeenCalledTimes(1)
    expect(mockCache.readQuery).toHaveBeenCalledWith({
      query: SURVEY_CREATION
    })
  })
})
