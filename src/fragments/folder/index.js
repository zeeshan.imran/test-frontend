import gql from 'graphql-tag'

export const folderFragment = gql`
  fragment folderFragment on Folder {
      id
      name
      owner
      parent
      children {
        id
        name
        owner
        parent
        status
        createdAt
        updatedAt
      }
      surveys
      status
      createdAt
      updatedAt
}
`
