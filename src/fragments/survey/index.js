import gql from 'graphql-tag'

const rangeInfo = gql`
  fragment rangeInfo on Range {
    min
    max
    step
    labels
    isInOneRow
    marks {
      label
      value
      isMajorUnit
    }
  }
`

const pairInfo = gql`
  fragment pairInfo on Pair {
    id
    leftAttribute
    rightAttribute
  }
`

const pairsOptionsInfo = gql`
  fragment pairsOptionsInfo on PairsOptions {
    elementsInPairs
    minPairs
    maxPairs
    customFlavorEnabled
    oneFlavorNotPresentEnabled
    neitherFlavorPresentEnabled
    bothFlavorsPresentEnabled
  }
`

const optionsInfo = gql`
  fragment optionsInfo on Options {
    label
    value
    scoreValue
    isOpenAnswer
    image200
    image800
    internalName
    hasReduceReward
    reduceRewardValue
  }
`

const sliderOptionsInfo = gql`
  fragment sliderOptionsInfo on SliderOptions {
    sliders {
      label
      internalName
      tooltip
      leftLabel
      rightLabel
    }
    hasFollowUpProfile
    profilePrompt
  }
`

const matrixOptionsInfo = gql`
  fragment matrixOptionsInfo on MatrixOptions {
    question {
      label
      id
    }
  }
`

const numericOptionsInfo = gql`
  fragment numericOptionsInfo on NumericOptions {
    min
    max
    decimalNumbers
  }
`

export const basicQuestionInfo = gql`
  fragment basicQuestionInfo on Question {
    id
    type
    required
    displaySurveyName
    prompt
    secondaryPrompt
    internalPrompt
    order
    chartTitle
    chartTopic
    chartType
    showOnShared
    nextQuestion
    displayOn
    optionDisplayType
    showProductImage
    considerValueForScore
    hasDemographic
    demographicField
    delayToNextQuestion
    extraDelayToNextQuestion
    image
  }
`

export const questionNavigationInfo = gql`
  fragment questionNavigationInfo on Question {
    id
    order
    type
    nextQuestion
    displayOn
    hasDemographic
    demographicField
    options {
      ...optionsInfo
    }
    sliderOptions {
      ...sliderOptionsInfo
    }
    matrixOptions {
      ...matrixOptionsInfo
    }
    numericOptions {
      ...numericOptionsInfo
    }
  }
  ${optionsInfo}
  ${sliderOptionsInfo}
  ${matrixOptionsInfo}
  ${numericOptionsInfo}
`

export const questionInfo = gql`
  fragment questionInfo on Question {
    ...basicQuestionInfo
    currentTime
    range {
      ...rangeInfo
    }
    options {
      ...optionsInfo
    }
    pairsOptions {
      ...pairsOptionsInfo
    }
    sliderOptions {
      ...sliderOptionsInfo
    }
    matrixOptions {
      ...matrixOptionsInfo
    }
    numericOptions {
      ...numericOptionsInfo
    }
    pairs {
      ...pairInfo
    }
    chooseProductOptions {
      minimumProducts
      maximumProducts
    }
    settings {
      ... on MultipleQuestionSettings {
        minAnswerValues
        maxAnswerValues
        chooseMultiple
      }
      ... on TasterNameSettings {
        minLength
        maxLength
        answerFormat
        allowSpaces
      }
    }
    addCustomOption
    relatedQuestions
    likingQuestion
    region
    skipFlow {
      ... on SkipOptionFlow {
        type
        rules {
          index
          skipTo
        }
      }
      ... on SkipProductFlow {
        type
        rules {
          productId
          skipTo
        }
      }
      ... on SkipRangeFlow {
        type
        rules {
          minValue
          maxValue
          skipTo
        }
      }
      ... on SkipMultipleFlow {
        type
        rules {
          options {
            value
            label
          }
          skipOptions {
            value
            label
          }
          answers
          skipTo
          type
        }
      }
      ... on SkipMatrixFlow {
        type
        rules {
          options {
            id
            label
          }
          skipOptions {
            value
            label
          }
          answers
          skipTo
          type
        }
      }
    }
  }
  ${basicQuestionInfo}
  ${rangeInfo}
  ${pairsOptionsInfo}
  ${pairInfo}
  ${sliderOptionsInfo}
  ${matrixOptionsInfo}
  ${numericOptionsInfo}
  ${optionsInfo}
`

export const surveyBasicInfo = gql`
  fragment surveyBasicInfo on Survey {
    coverPhoto
    id
    name
    title
    uniqueName
    owner
    state
    minimumProducts
    maximumProducts
    surveyLanguage
    instructionSteps
    products {
      id
      name
      photo
      brand
      reward
      rejectedReward
      delayToNextProduct
      extraDelayToNextProduct
      amountInDollar
      isAvailable
      isSurveyCover
      sortingOrderId
      internalName
    }
    authorizationType
    exclusiveTasters {
      emailAddress
    }
    sharedStatsUsers {
      id
      organization {
        id
      }
    }
    isScreenerShownInShare
    isCompulsorySurveyShownInShare
    allowRetakes
    isScreenerOnly
    showSurveyProductScreen
    showIncentives
    productDisplayType
    showGeneratePdf
    screenOut
    screenOutSettings {
      reject
      rejectAfterStep
    }
    linkedSurveys {
      id
      name
      uniqueName
      state
    }
    forcedAccount
    forcedAccountLocation
    tastingNotes {
      tastingId
      tastingLeader
      customer
      country
      dateOfTasting
      otherInfo
    }
    autoAdvanceSettings {
      active
      debounce
      hideNextButton
    }
    pdfFooterSettings {
      active
      footerNote
    }
    promotionSettings {
      active
      showAfterTaster
      code
    }
    promotionOptions {
      promoProducts {
        label
        url
      }
    }
    enabledEmailTypes
    emails {
      surveyWaiting {
        subject
        html
        text
      }
      surveyRejected {
        subject
        html
        text
      }
      surveyCompleted {
        subject
        html
        text
      }
    }
    sharingButtons
    showSharingLink
    validatedData
    compulsorySurvey
    showSurveyScore
    showInternalNameInReports
    disableAllEmails
    reduceRewardInTasting
    includeCompulsorySurveyDataInStats
    showInPreferedLanguage
    addDelayToSelectNextProductAndNextQuestion
    showOnTasterDashboard
    referralAmount
    savedRewards
    settings
    country
    maxProductStatCount
    customizeSharingMessage
    loginText
    pauseText
    allowedDaysToFillTheTasting
    isPaypalSelected
    isGiftCardSelected
    customButtons {
      continue
      start
      next
      skip
    }
    createdAt
    updatedAt
    openedAt
    productRewardsRule {
      min
      max
      percentage
      active
    }
    showUserProfileDemographics
    dualCurrency
    referralAmountInDollar
    retakeAfter
    maxRetake
    maximumReward
  }
`

export const surveyQuestionsBasicInfo = gql`
  fragment surveyQuestionsBasicInfo on Survey {
    id
    name
    screeningQuestions {
      ...questionNavigationInfo
    }
    setupQuestions {
      ...questionNavigationInfo
    }
    productsQuestions {
      ...questionNavigationInfo
    }
    finishingQuestions {
      ...questionNavigationInfo
    }
    paymentQuestions {
      ...questionNavigationInfo
    }
    authorizationType
    settings
  }

  ${questionNavigationInfo}
`

export const surveyInfo = gql`
  fragment surveyInfo on Survey {
    ...surveyBasicInfo
    instructionsText
    instructionSteps
    ...surveyQuestionsBasicInfo
    thankYouText
    rejectionText
    screeningText
    customizeSharingMessage
    loginText
    pauseText
  }
  ${surveyBasicInfo}
  ${surveyQuestionsBasicInfo}
`
