import gql from 'graphql-tag'
import { categoryInfo } from '../category'

export const preferredProductCategoryInfo = gql`
  fragment preferredProductCategoryInfo on PreferredProductCategoryType {
    categoryId
    category {
      ...categoryInfo
    }
  }
  ${categoryInfo}
`
