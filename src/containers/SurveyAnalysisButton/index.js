import React from 'react'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import Text from '../../components/Text'

const SurveyAnalysisButton = ({ surveyId, history, onMenuItemClick }) => {
  const { t } = useTranslation()
  return (
    <Text
      onClick={() => {
        onMenuItemClick()
        history.push(`/operator/analysis/${surveyId}`)
      }}
    >
      {t('components.dasboard.survey.actions.analyzeSurvey')}
    </Text>
  )
}
export default withRouter(SurveyAnalysisButton)
