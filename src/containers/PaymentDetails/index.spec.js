import React from 'react'
import { mount } from 'enzyme'
import { PaymentDetailsWrapped } from './index'
import wait from '../../utils/testUtils/waait'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('PaymentDetails', () => {
  let testRender
  let user
  let handleUpdateTesterInfo
  let t
  let desktop

  function fillForm () {
    testRender
      .find({ id: 'paypalEmail' })
      .first()
      .prop('onChange')({ target: { value: 'test@flavorwiki.com' } })
  }

  beforeEach(() => {
    t = jest.fn(key => key)
    handleUpdateTesterInfo = {
      ok: jest.fn(() => ({
        data: { updateTesterInfo: { ok: true } }
      })),
      error: jest.fn(() => ({
        data: { updateTesterInfo: { error: 'ERROR on updateTesterInfo' } }
      })),
      throw: jest.fn(() => {
        throw 'Error'
      })
    }

    user = {
      id: 'user-1',
      testerInfo: {
        paypalEmail: 'flovorwiki@gmail.com'
      }
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PaymentDetailsWrapped', () => {
    testRender = mount(
      <PaymentDetailsWrapped t={t} desktop={desktop} user={user} />
    )

    expect(testRender).toMatchSnapshot()
  })

  test('skip process when nothing is touched', () => {
    testRender = mount(
      <PaymentDetailsWrapped t={t} desktop={desktop} user={user} />
    )
    testRender.find('form').simulate('submit')
  })

  test('should call handleUpdateTesterInfo when the user touched fields', async () => {
    testRender = mount(
      <PaymentDetailsWrapped
        t={t}
        desktop={null}
        user={user}
        updateTesterInfo={handleUpdateTesterInfo.ok}
      />
    )

    fillForm()
    testRender.update()
    testRender.find('button').simulate('click')
    await wait(0)
    expect(handleUpdateTesterInfo.ok).toHaveBeenCalledWith({
      variables: {
        paypalEmail: 'test@flavorwiki.com',
        id: 'user-1'
      }
    })
  })
  test('should display error when handleUpdateTesterInfo returns error', async () => {
    testRender = mount(
      <PaymentDetailsWrapped
        t={t}
        desktop={''}
        user={user}
        updateTesterInfo={handleUpdateTesterInfo.error}
      />
    )
    fillForm()
    testRender.update()
    testRender.find('button').simulate('click')

    await wait(0)

    expect(handleUpdateTesterInfo.error).toHaveBeenCalledWith({
      variables: {
        paypalEmail: 'test@flavorwiki.com',
        id: 'user-1'
      }
    })
    expect(t).toHaveBeenCalledWith('error.update', {
      error: 'ERROR on updateTesterInfo'
    })
  })

  test('should display error when handleUpdateTesterInfo throws network error', async () => {
    testRender = mount(
      <PaymentDetailsWrapped
        t={t}
        desktop={''}
        user={user}
        updateTesterInfo={handleUpdateTesterInfo.throw}
      />
    )
    fillForm()
    testRender.update()
    testRender.find('button').simulate('click')

    await wait(0)

    expect(handleUpdateTesterInfo.throw).toHaveBeenCalledWith({
      variables: {
        paypalEmail: 'test@flavorwiki.com',
        id: 'user-1'
      }
    })
    expect(t).toHaveBeenCalledWith('containers.paymentDetails.error')
  })
})
