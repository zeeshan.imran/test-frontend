import React from 'react'
import { mount } from 'enzyme'
import PrivacyPolicyContainer from '.'
import PrivacyPolicy from '../../components/PrivacyPolicy'
import { act } from 'react-dom/test-utils'
import wait from '../../utils/testUtils/waait'

describe('PrivacyPolicyContainer', () => {
  let testRender
  let location
  let handleChange
  let mockScrollTo = jest.fn()
  let goToTermsOfUse

  beforeAll(() => {
    Object.defineProperty(window, 'scrollTo', { value: mockScrollTo })
  })

  beforeEach(() => {
    location = {
      pathname: '/survey/operator/terms-of-use'
    }
    handleChange = jest.fn()
    goToTermsOfUse = () => {
      history.push('/terms-of-use')
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render PrivacyPolicyContainer', () => {
    const historyData = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      }
    }

    testRender = mount(
      <PrivacyPolicyContainer
        history={historyData}
        location={location}
        handleChange={handleChange}
        goToTermsOfUse={goToTermsOfUse}
      />
    )
    expect(PrivacyPolicyContainer).toHaveLength(1)
  })

  test('should change route and scroll to top', async () => {
    const historyData = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      }
    }

    testRender = mount(
      <PrivacyPolicyContainer history={historyData} location={location} />
    )
    act(() => {
      testRender.find(PrivacyPolicy).prop('goToTermsOfUse')()
    })
    testRender.update()
    await wait(0)
    expect(historyData.push).toHaveBeenCalledWith('/terms-of-use')
  })
})
