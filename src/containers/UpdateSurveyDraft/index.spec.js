import React from 'react'
import { mount } from 'enzyme'
import { ApolloProvider } from 'react-apollo-hooks'
import wait from '../../utils/testUtils/waait'
import Button from '../../components/Button'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import useSurveyValidate from '../../hooks/useSurveyValidate'
import useUpdateSurvey from '../../hooks/useUpdateSurvey'
import UpdateSurveyDraft from './index'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import { defaultSurvey } from '../../mocks'

const createSurveyCreationMock = secondProduct => ({
  ...defaultSurvey,
  products: [
    {
      name: 'the first product',
      brand: 'the brand'
    },
    secondProduct
  ]
})

jest.mock('../../hooks/useUpdateSurvey')
jest.mock('../../hooks/useSurveyValidate')
jest.mock('../../utils/displayErrorPopup')

describe('update survey draft', () => {
  let updateSurvey
  let history
  let client
  let render
  let showProductsErrors
  const match = { params: { surveyId: 'survey-1' } }
  let showQuestionsErrors

  beforeEach(() => {
    client = createApolloMockClient()
    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: createSurveyCreationMock({
          name: '',
          brand: 'the brand'
        })
      }
    })
    updateSurvey = jest.fn()
    showProductsErrors = jest.fn()
    showQuestionsErrors = jest.fn()
    useUpdateSurvey.mockImplementation(() => updateSurvey)

    history = {
      push: jest.fn()
    }
  })

  afterEach(() => {
    render.unmount()
  })

  test('should switch to products tab and show errors when the survey has at least one invalid product', async () => {
    useSurveyValidate.mockImplementation(() => ({
      validateAdvanceSettings: () => true,
      validateProducts: () => false,
      validateQuestions: () => true,
      validateChartTitles: () => [],
      showProductsErrors
    }))

    render = mount(
      <ApolloProvider client={client}>
        <UpdateSurveyDraft.WrappedComponent history={history} match={match} />
      </ApolloProvider>
    )

    await wait(0)

    render
      .find(Button)
      .first()
      .prop('onClick')()

    await wait(0)

    expect(history.push).toHaveBeenCalledWith('./products')
    expect(showProductsErrors).toHaveBeenCalled()
    expect(updateSurvey).not.toHaveBeenCalled()
  })

  test('should call updateSurvey when the survey has all products is valid', async () => {
    useSurveyValidate.mockImplementation(() => ({
      validateAdvanceSettings: () => true,
      validateProducts: () => true,
      validateQuestions: () => ({ isValid: true }),
      validateChartTitles: () => [],
      showProductsErrors
    }))

    render = mount(
      <ApolloProvider client={client}>
        <UpdateSurveyDraft.WrappedComponent history={history} match={match} />
      </ApolloProvider>
    )

    await wait(0)

    await render
      .find(Button)
      .first()
      .prop('onClick')()

    await wait(0)

    expect(showProductsErrors).not.toHaveBeenCalled()
    expect(updateSurvey).toHaveBeenCalled()
  })

  test('should call publishSurvey then redirect to /surveys when the survey has all products is invalid valid', async () => {
    const match = { params: { surveyId: 'survey-1' } }

    useSurveyValidate.mockImplementation(() => ({
      validateAdvanceSettings: () => true,
      validateProducts: () => true,
      validateQuestions: () => ({ invalidQuestion: false }),
      validateChartTitles: () => [],
      showQuestionsErrors
    }))
    render = mount(
      <ApolloProvider client={client}>
        <UpdateSurveyDraft.WrappedComponent history={history} match={match} />
      </ApolloProvider>
    )

    await wait(0)

    await render
      .find(Button)
      .first()
      .prop('onClick')()
  })

  test('should show error message while saving error', async () => {
    useUpdateSurvey.mockImplementation(() => () => {
      throw new Error('Save the survey draft fail')
    })
    useSurveyValidate.mockImplementation(() => ({
      validateAdvanceSettings: () => true,
      validateProducts: () => true,
      validateQuestions: () => ({ isValid: true }),
      validateChartTitles: () => [],
      showProductsErrors
    }))

    render = mount(
      <ApolloProvider client={client}>
        <UpdateSurveyDraft.WrappedComponent history={history} match={match} />
      </ApolloProvider>
    )

    await wait(0)

    render
      .find(Button)
      .first()
      .prop('onClick')()

    await wait(0)
    expect(displayErrorPopup).toHaveBeenCalledWith('Save the survey draft fail')
  })
})
