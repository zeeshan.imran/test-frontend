import React, { useEffect, useState } from 'react'
import { path } from 'ramda'
import gql from 'graphql-tag'
import * as Yup from 'yup'
import * as chartUtils from '../../utils/chartUtils'
import ChartsPdf from '../../components/Charts/ChartsPdf'
import LoadingModal from '../../components/LoadingModal'
import { useQuery } from 'react-apollo-hooks'
import OperatorPage from '../../components/OperatorPage'
import OperatorPageContent from '../../components/OperatorPageContent'
import { Alert } from 'antd'
import { useTranslation } from 'react-i18next'
import ErrorBoundary from '../SurveyStats/ErrorBoundary'
import EmptyState from '../SurveyStats/EmptyState'
import { ERRORS } from '../../utils/Constants'
import { getImages } from '../../utils/getImages'
import {
  BaseContainer,
  Container,
  ChartContainer,
  HeaderContainer,
  Logo
} from './styles'
import { imagesCollection } from '../../assets/png'
import { getDefaultChartSettings } from '../../utils/chartSettings'
import logger from '../../utils/logger'

const { desktopImage } = getImages(imagesCollection)

const getStats = path(['survey', 'stats'])

const SurveyStatsPdf = ({
  match: {
    params: { surveyId }
  },
  history
}) => {
  const { t } = useTranslation()

  let productIds = JSON.parse(
    window.localStorage.getItem('flavorwiki-products-details')
  )
  if (productIds == null) {
    productIds = []
  }
  const {
    data: { getChartsSettings = {} } = {},
    loading: chartsLoading
  } = useQuery(CHARTS_SETTINGS_QUERY, {
    variables: {
      surveyId
    }
  })
  const [chartsSettings, setChartsSettings] = useState(
    getChartsSettings.chartsSettings || {}
  )
  const validationSchema = Yup.object().shape({
    tabs: Yup.array()
      .typeError(t('containers.surveyStats.validation.typeError'))
      .of(
        Yup.object().shape({
          title: Yup.string().required(),
          charts: Yup.array()
        })
      )
  })

  const { loading, data, error } = useQuery(QUERY_SURVEY_STATS, {
    variables: {
      surveyId,
      checkOrg: true,
      sync: true,
      productIds: productIds,
      questionFilters: []
    },
    fetchPolicy: 'network-only'
  })
  if (error && error.message.includes(ERRORS.NOT_AUTH_SURVEY)) {
    history.push('/operator/not-authorized')
  }

  let contents = []
  let stats = []
  stats = getStats(data)
  let chartsData = []
  stats &&
    validationSchema.isValidSync(stats) &&
    chartUtils.getChartView(stats).forEach((el, ind) => {
      Object.assign(el, { chart_id: ind, key: ind })
      chartsData.push(el)
    })
  if (chartsData.length > 0) {
    chartsData.forEach((element, index) => {
      contents.push(
        <ChartsPdf
          charts={element}
          chartsSettings={
            chartsSettings &&
            chartsSettings[`${element.question}-${element.chart_type}`]
              ? chartsSettings[`${element.question}-${element.chart_type}`]
              : {}
          }
          hideFilter
          key={`new-chart +${index}`}
          idx={index}
          t={t}
        />
      )
    })
  }

  const setDefaultChartSettings = () => {
    stats = getStats(data)
    if (stats) {
      const defaultSettings = getDefaultChartSettings(stats)

      setChartsSettings(defaultSettings)
    }
  }

  useEffect(() => {
    if (!chartsLoading && !loading) {
      // Chart settings from graphQL
      if (getChartsSettings.chartsSettings) {
        setChartsSettings(getChartsSettings.chartsSettings)
      } else {
        setDefaultChartSettings()
      }
    }
  }, [chartsLoading, loading])

  if (error) {
    logger.error(error)
    return (
      <OperatorPage>
        <OperatorPageContent>
          <Alert
            type='error'
            message={t('containers.surveyStats.fetchError.message')}
            description={t('containers.surveyStats.fetchError.description')}
          />
        </OperatorPageContent>
      </OperatorPage>
    )
  }

  if (loading) {
    return <LoadingModal visible />
  }

  if (contents.length === 0) {
    return <EmptyState />
  }

  return (
    <BaseContainer
      id={`content-${contents.length}`}
      className='content-container'
    >
      <Container id='main-container'>
        <OperatorPage>
          <OperatorPageContent>
            <ChartContainer id='chart-container'>
              <HeaderContainer>
                <Logo onClick={() => history.push('/')} src={desktopImage} />
              </HeaderContainer>
              {contents}
            </ChartContainer>
          </OperatorPageContent>
        </OperatorPage>
      </Container>
    </BaseContainer>
  )
}

const QUERY_SURVEY_STATS = gql`
  query surveyStatsData(
    $surveyId: ID!
    $productIds: [String!]
    $questionFilters: [Object!]
    $checkOrg: Boolean
    $sync: Boolean
  ) {
    survey(id: $surveyId, checkOrg: $checkOrg) {
      id
      stats(
        productIds: $productIds
        sync: $sync
        questionFilters: $questionFilters
      )
    }
  }
`
const CHARTS_SETTINGS_QUERY = gql`
  query getChartsSettings($surveyId: ID!) {
    getChartsSettings(surveyId: $surveyId)
  }
`

export default props => (
  <ErrorBoundary>
    <SurveyStatsPdf {...props} />
  </ErrorBoundary>
)
