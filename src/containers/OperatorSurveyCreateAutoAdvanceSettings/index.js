import React, { useCallback, useEffect, useRef } from 'react'
import { path } from 'ramda'
import { useQuery, useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import AutoAdvanceSettings from '../../components/OperatorSurveyCreateAutoAdvanceSettings'
import { getAuthenticatedOrganization } from '../../utils/userAuthentication'

const getAutoAdvanceSettings = path([
  'surveyCreation',
  'basics',
  'autoAdvanceSettings'
])

const getProductRewardsRuleSettings = path([
  'surveyCreation',
  'basics',
  'productRewardsRule'
])

const getPdfFooterSettings = path([
  'surveyCreation',
  'basics',
  'pdfFooterSettings'
])
const getQuestions = path(['surveyCreation', 'questions'])
const getSharingButtons = path(['surveyCreation', 'basics', 'sharingButtons'])
const getSharingLink = path(['surveyCreation', 'basics', 'showSharingLink'])
const isScreenerOnly = path(['surveyCreation', 'basics', 'isScreenerOnly'])
const getValidatedData = path(['surveyCreation', 'basics', 'validatedData'])
const getGeneratePdf = path(['surveyCreation', 'basics', 'showGeneratePdf'])
const getSurveyProductScreen = path([
  'surveyCreation',
  'basics',
  'showSurveyProductScreen'
])
// Screen Out related variable
const getScreenOut = path(['surveyCreation', 'basics', 'screenOut'])
const getScreenOutSettings = path([
  'surveyCreation',
  'basics',
  'screenOutSettings'
])
const getPromotionSettings = path([
  'surveyCreation',
  'basics',
  'promotionSettings'
])

const getPromotionOptions = path([
  'surveyCreation',
  'basics',
  'promotionOptions'
])
// End
const getCompulsorySurvey = path([
  'surveyCreation',
  'basics',
  'compulsorySurvey'
])
const getShowSurveyScore = path(['surveyCreation', 'basics', 'showSurveyScore'])

const getAuthorizationType = path([
  'surveyCreation',
  'basics',
  'authorizationType'
])
const getShowOnTasterDashboard = path([
  'surveyCreation',
  'basics',
  'showOnTasterDashboard'
])
const getShowInternalNameInReports = path([
  'surveyCreation',
  'basics',
  'showInternalNameInReports'
])
const getDisableAllEmails = path([
  'surveyCreation',
  'basics',
  'disableAllEmails'
])
const getIncludeCompulsorySurveyDataInStats = path([
  'surveyCreation',
  'basics',
  'includeCompulsorySurveyDataInStats'
])
const getShowInPreferedLanguage = path([
  'surveyCreation',
  'basics',
  'showInPreferedLanguage'
])
const getAddDelayToSelectNextProductAndNextQuestion = path([
  'surveyCreation',
  'basics',
  'addDelayToSelectNextProductAndNextQuestion'
])
const getShowUserProfileDemographics = path([
  'surveyCreation',
  'basics',
  'showUserProfileDemographics'
])
const getDualCurrency = path(['surveyCreation', 'basics', 'dualCurrency'])

const getShowIncentives = path(['surveyCreation', 'basics', 'showIncentives'])
const getReduceRewardInTasting = path([
  'surveyCreation',
  'basics',
  'reduceRewardInTasting'
])

const OperatorSurveyCreateSettings = ({ editing = false, stricted }) => {
  const { data, refetch: refetchCurrent } = useQuery(SURVEY_CREATION)
  const updateSurveyBasics = useMutation(UPDATE_SURVEY_CREATION_BASICS)
  const updateQuestions = useMutation(UPDATE_SURVEY_CREATION_QUESTIONS)
  const updateProducts = useMutation(UPDATE_SURVEY_CREATION_PRODUCTS)
  const unrequireChooseProductStep = useMutation(UNREQUIRE_CHOOSE_PRODUCT)
  const requireShowProductScreen = useMutation(REQUIRE_SHOW_PRODUCT_SCREEN)
  const unrequireShowProductScreen = useMutation(UNREQUIRE_SHOW_PRODUCT_SCREEN)
  const refetch = useRef(refetchCurrent)

  useEffect(() => {
    refetch.current()
  }, [data.surveyCreation.basics.authorizationType])

  const productRewardsRule = getProductRewardsRuleSettings(data) || {
    active: false,
    min: 0,
    max: 0,
    percentage: 0
  }

  const autoAdvanceSettings = getAutoAdvanceSettings(data) || {
    active: false,
    min: 0
  }

  const pdfFooterSettings = getPdfFooterSettings(data) || {
    active: false,
    footerNote: ''
  }

  const screenerOnly = isScreenerOnly(data) || false
  const questions = getQuestions(data) || []
  const sharingButtons = getSharingButtons(data) || false
  const showSharingLink = getSharingLink(data) || false
  const validatedData = getValidatedData(data) || false
  const showGeneratePdf = getGeneratePdf(data) || false
  const showSurveyProductScreen = getSurveyProductScreen(data) || false
  const showIncentives = getShowIncentives(data) || false
  const authorizationType = getAuthorizationType(data)
  const showOnTasterDashboard = getShowOnTasterDashboard(data) || false
  const screenOut = getScreenOut(data) || false
  const screenOutSettings = getScreenOutSettings(data) || {
    reject: false,
    rejectAfterStep: 1
  }
  const promotionSettings = getPromotionSettings(data) || {
    active: false,
    showAfterTaster: false,
    code: ''
  }
  const promotionOptions = getPromotionOptions(data) || {
    promoProducts: [{ label: null, url: null }]
  }
  const compulsorySurvey = getCompulsorySurvey(data) || false
  const showSurveyScore = getShowSurveyScore(data) || false
  const showInternalNameInReports = getShowInternalNameInReports(data) || false
  const disableAllEmails = getDisableAllEmails(data) || false
  const includeCompulsorySurveyDataInStats =
    getIncludeCompulsorySurveyDataInStats(data) || false
  const showInPreferedLanguage = getShowInPreferedLanguage(data) || false
  const addDelayToSelectNextProductAndNextQuestion = getAddDelayToSelectNextProductAndNextQuestion(data) || false
  const showUserProfileDemographics =
    getShowUserProfileDemographics(data) || false
  const isFlavorwiki = process.env.REACT_APP_THEME === 'default'
  const dualCurrency = getDualCurrency(data) || false
  const reduceRewardInTasting = getReduceRewardInTasting(data) || false

  const userOrganizationId = getAuthenticatedOrganization()
  const {
    data: { organization: { name: userOrganizationName } = {} } = {}
  } = useQuery(GET_ORGANIZATION, {
    variables: { id: userOrganizationId }
  })
  const isFlavorwikiOperator = userOrganizationName === 'FlavorWiki'

  const handleSharingButtonsChange = useCallback(
    async (sharingButtons) => {
      await updateSurveyBasics({
        variables: {
          basics: {
            sharingButtons
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  const handleSharingLinkChange = useCallback(
    async (value) => {
      await updateSurveyBasics({
        variables: {
          basics: {
            showSharingLink: value
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  useEffect(() => {
    if (userOrganizationName && !isFlavorwikiOperator && showSharingLink) {
      handleSharingLinkChange(false)
    }
  }, [isFlavorwikiOperator, showSharingLink, userOrganizationName])

  const handleValidatedData = useCallback(
    async (validatedData) => {
      await updateSurveyBasics({
        variables: {
          basics: {
            validatedData
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  const handleShowOnTasterDashboard = useCallback(
    async (showOnTasterDashboard) => {
      await updateSurveyBasics({
        variables: {
          basics: {
            showOnTasterDashboard
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  const handlePdfSetting = useCallback(
    async (pdfFooterSettings) => {
      await updateSurveyBasics({
        variables: {
          basics: {
            pdfFooterSettings
          }
        }
      })
    },
    [updateSurveyBasics]
  )
  const handleGeneratePdfButton = useCallback(
    async (showGeneratePdf) => {
      await updateSurveyBasics({
        variables: {
          basics: {
            showGeneratePdf
          }
        }
      })
    },
    [updateSurveyBasics]
  )
  // Function to change screen out

  const handleScreenOut = useCallback(
    async (screenOut) => {
      await updateSurveyBasics({
        variables: {
          basics: {
            screenOut
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  const handleScreenOutSettings = useCallback(
    async (screenOutSettings) => {
      await updateSurveyBasics({
        variables: {
          basics: {
            screenOutSettings
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  const onChangePromotionSettings = useCallback(
    async (promotionSettings) => {
      await updateSurveyBasics({
        variables: {
          basics: {
            promotionSettings
          }
        }
      })
    },
    [updateSurveyBasics]
  )
  const onChangePromotionOptions = useCallback(
    async (promotionOptions) => {
      await updateSurveyBasics({
        variables: {
          basics: {
            promotionOptions
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  const handleProductRewardsRule = useCallback(
    async (active) => {
      await updateSurveyBasics({
        variables: {
          basics: {
            productRewardsRule: {
              __typename: 'ProductRewardsRule',
              min: 0,
              max: 0,
              percentage: 0,
              active
            }
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  const handleShowUserProfileDemographics = useCallback(
    async (newValue) => {
      await updateSurveyBasics({
        variables: {
          basics: {
            showUserProfileDemographics: newValue
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  const handleShowProductScreen = useCallback(
    async (showSurveyProductScreen) => {
      if (showSurveyProductScreen) {
        requireShowProductScreen()
      } else {
        unrequireShowProductScreen()
      }
      await updateSurveyBasics({
        variables: {
          basics: {
            showSurveyProductScreen
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  const handleShowIncentives = useCallback(
    async (showIncentives) => {
      await updateSurveyBasics({
        variables: {
          basics: {
            showIncentives
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  const handleSettingsChange = useCallback(
    async (autoAdvanceSettings, screenerOnly) => {
      let data = {
        autoAdvanceSettings: autoAdvanceSettings,
        isScreenerOnly: screenerOnly
      }
      if (screenerOnly) {
        data = {
          autoAdvanceSettings: autoAdvanceSettings,
          isScreenerOnly: screenerOnly,
          forcedAccount: true,
          showGeneratePdf: false,
          showSurveyProductScreen: false,
          compulsorySurvey: false,
          reduceRewardInTasting: false,
          validatedData: false
        }
      }
      await updateSurveyBasics({
        variables: {
          basics: data
        }
      })

      if (screenerOnly) {
        if (questions.length) {
          const newQuestions = questions.filter((singleQuestion) => {
            return (
              singleQuestion.displayOn === 'screening' ||
              singleQuestion.displayOn === 'payments'
            )
          })
          await updateQuestions({
            variables: {
              questions: newQuestions
            }
          })
          unrequireChooseProductStep()
        }
        await updateProducts({
          variables: {
            products: []
          }
        })
      }
    },
    [questions, updateProducts, updateQuestions, updateSurveyBasics]
  )

  const handleShowSurveyScore = useCallback(
    async (showSurveyScore) => {
      await updateSurveyBasics({
        variables: {
          basics: {
            showSurveyScore
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  const handleShowInternalNameInReports = useCallback(
    async (showInternalNameInReports) => {
      await updateSurveyBasics({
        variables: {
          basics: {
            showInternalNameInReports
          }
        }
      })
    },
    [updateSurveyBasics]
  )
  
  const handleDisableAllEmails = useCallback(
    async (disableAllEmails) => {
      await updateSurveyBasics({
        variables: {
          basics: {
            disableAllEmails
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  const handleIncludeCompulsorySurveyDataInStats = useCallback(
    async (includeCompulsorySurveyDataInStats) => {
      await updateSurveyBasics({
        variables: {
          basics: {
            includeCompulsorySurveyDataInStats,
            compulsorySurvey: false,
            showSurveyScore: false
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  const handleShowInPreferedLanguage = useCallback(
    async (showInPreferedLanguage) => {
      await updateSurveyBasics({
        variables: {
          basics: {
            showInPreferedLanguage
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  const handleAddDelayToSelectNextProductAndNextQuestion = useCallback(
    async addDelayToSelectNextProductAndNextQuestion => {
      await updateSurveyBasics({
        variables: {
          basics: {
            addDelayToSelectNextProductAndNextQuestion
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  const handleCompulsorySurvey = useCallback(
    async (compulsorySurvey) => {
      if (compulsorySurvey) {
        if (questions.length) {
          const newQuestions = questions.filter((singleQuestion) => {
            return singleQuestion.displayOn === 'screening'
          })
          await updateQuestions({
            variables: {
              questions: newQuestions
            }
          })
          unrequireChooseProductStep()
        }
        await updateProducts({
          variables: {
            products: []
          }
        })
      }

      if (!compulsorySurvey) {
        handleShowSurveyScore(false)
      }

      await updateSurveyBasics({
        variables: {
          basics: {
            compulsorySurvey,
            includeCompulsorySurveyDataInStats: false,
            isScreenerOnly: false
          }
        }
      })
    },
    [
      questions,
      updateSurveyBasics,
      unrequireChooseProductStep,
      updateProducts,
      updateQuestions,
      handleShowSurveyScore
    ]
  )

  const handleCurrencySurvey = useCallback(
    async (dualCurrency) => {
      await updateSurveyBasics({
        variables: {
          basics: {
            dualCurrency
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  const handleReduceRewardInTasting = useCallback(
    async (reduceRewardInTasting) => {
      await updateSurveyBasics({
        variables: {
          basics: {
            reduceRewardInTasting
          }
        }
      })
    },
    [updateSurveyBasics]
  )

  return (
    <AutoAdvanceSettings
      autoAdvanceSettings={autoAdvanceSettings}
      pdfFooterSettings={pdfFooterSettings}
      onSettingsChange={handleSettingsChange}
      screenerOnly={screenerOnly}
      sharingButtons={sharingButtons}
      showSharingLink={showSharingLink}
      showGeneratePdf={showGeneratePdf}
      handleGeneratePdfButton={handleGeneratePdfButton}
      onSharingButtonsChange={handleSharingButtonsChange}
      onSharingLinkChange={handleSharingLinkChange}
      editing={editing}
      handleValidatedDataChange={handleValidatedData}
      validatedData={validatedData}
      handlePdfSetting={handlePdfSetting}
      surveyAuthorizationType={authorizationType}
      handleShowOnTasterDashboard={handleShowOnTasterDashboard}
      showOnTasterDashboard={showOnTasterDashboard}
      showSurveyProductScreen={showSurveyProductScreen}
      handleShowProductScreen={handleShowProductScreen}
      handleProductRewardsRule={handleProductRewardsRule}
      productRewardsRule={productRewardsRule}
      compulsorySurvey={compulsorySurvey}
      handleCompulsorySurvey={handleCompulsorySurvey}
      showSurveyScore={showSurveyScore}
      handleShowSurveyScore={handleShowSurveyScore}
      stricted={stricted}
      handleScreenOut={handleScreenOut}
      screenOut={screenOut}
      screenOutSettings={screenOutSettings}
      handleScreenOutSettings={handleScreenOutSettings}
      promotionSettings={promotionSettings}
      onChangePromotionSettings={onChangePromotionSettings}
      onChangePromotionOptions={onChangePromotionOptions}
      promotionOptions={promotionOptions}
      handleShowInternalNameInReports={handleShowInternalNameInReports}
      handleDisableAllEmails={handleDisableAllEmails}
      showInternalNameInReports={showInternalNameInReports}
      disableAllEmails={disableAllEmails}
      handleIncludeCompulsorySurveyDataInStats={
        handleIncludeCompulsorySurveyDataInStats
      }
      includeCompulsorySurveyDataInStats={includeCompulsorySurveyDataInStats}
      handleShowInPreferedLanguage={handleShowInPreferedLanguage}
      showInPreferedLanguage={showInPreferedLanguage}
      handleAddDelayToSelectNextProductAndNextQuestion={handleAddDelayToSelectNextProductAndNextQuestion}
      addDelayToSelectNextProductAndNextQuestion={addDelayToSelectNextProductAndNextQuestion}
      showIncentives={showIncentives}
      handleShowIncentives={handleShowIncentives}
      showUserProfileDemographics={showUserProfileDemographics}
      isFlavorwiki={isFlavorwiki}
      handleShowUserProfileDemographics={handleShowUserProfileDemographics}
      handleCurrencySurvey={handleCurrencySurvey}
      dualCurrency={dualCurrency}
      handleReduceRewardInTasting={handleReduceRewardInTasting}
      reduceRewardInTasting={reduceRewardInTasting}
      isFlavorwikiOperator={isFlavorwikiOperator}
    />
  )
}

export const GET_ORGANIZATION = gql`
  query organization($id: ID!) {
    organization(id: $id) {
      id
      name
    }
  }
`

export const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      questions
      basics {
        isScreenerOnly
        sharingButtons
        validatedData
        compulsorySurvey
        showSurveyScore
        showGeneratePdf
        showSurveyProductScreen
        showIncentives
        authorizationType
        showOnTasterDashboard
        screenOut
        screenOutSettings {
          reject
          rejectAfterStep
        }
        showSharingLink
        showInternalNameInReports
        disableAllEmails
        reduceRewardInTasting
        includeCompulsorySurveyDataInStats
        showInPreferedLanguage
        addDelayToSelectNextProductAndNextQuestion
        showUserProfileDemographics
        autoAdvanceSettings {
          active
          debounce
          hideNextButton
        }
        pdfFooterSettings {
          active
          footerNote
        }
        promotionSettings {
          active
          showAfterTaster
          code
        }
        promotionOptions {
          promoProducts {
            label
            url
          }
        }
        productRewardsRule {
          active
        }
        dualCurrency
      }
    }
  }
`
export const UPDATE_SURVEY_CREATION_QUESTIONS = gql`
  mutation updateSurveyCreationQuestions($questions: questions) {
    updateSurveyCreationQuestions(questions: $questions) @client
  }
`

const UPDATE_SURVEY_CREATION_PRODUCTS = gql`
  mutation updateSurveyCreationProducts($products: products) {
    updateSurveyCreationProducts(products: $products) @client
  }
`

export const UPDATE_SURVEY_CREATION_BASICS = gql`
  mutation updateSurveyCreationBasics($basics: basics) {
    updateSurveyCreationBasics(basics: $basics) @client
  }
`
const UNREQUIRE_CHOOSE_PRODUCT = gql`
  mutation unrequireChooseProduct {
    unrequireChooseProduct(id: "") @client
  }
`

export const REQUIRE_SHOW_PRODUCT_SCREEN = gql`
  mutation requireShowProductScreen {
    requireShowProductScreen(id: "") @client
  }
`

export const UNREQUIRE_SHOW_PRODUCT_SCREEN = gql`
  mutation unrequireShowProductScreen {
    unrequireShowProductScreen(id: "") @client
  }
`

export default OperatorSurveyCreateSettings
