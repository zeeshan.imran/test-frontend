import { path, filter } from 'ramda'
import React, { useState, useCallback, useRef, useMemo } from 'react'
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo-hooks'
import OperatorSurveyAnalysisComponent from '../../components/OperatorSurveyAnalysis'
import { withRouter } from 'react-router-dom'

const selectQuestions = path(['survey', 'productsQuestions'])
const selectProducts = path(['survey', 'products'])
const NON_TRACK_QUESTIONS = ['choose-product', 'paypal-email', 'choose-payment']
const withoutNonTrackQuestions = filter(
  question => !NON_TRACK_QUESTIONS.includes(question.type)
)

const OperatorSurveyAnalysis = ({ history, match }) => {
  const { surveyId } = match.params
  const [settings, setSettings] = useState({
    question: null,
    analysisTypes: [],
    referenceQuestion: null
  })
  const [filterProducts, setFilterProducts] = useState()
  const [filterQuestions, setFilterQuestions] = useState({})
  const { data, loading } = useQuery(SURVEY_QUERY, {
    variables: { id: surveyId },
    fetchPolicy: 'cache-and-network'
  })

  const createJobs = useRef(useMutation(CREATE_JOBS))

  const questions = useMemo(
    () => withoutNonTrackQuestions(selectQuestions(data) || []),
    [data]
  )
  const products = useMemo(() => selectProducts(data) || [], [data])

  const handleSettingsSave = useCallback(
    nextSettings => {
      setSettings(nextSettings)
      setFilterProducts(products.map(p => p.id))
      const initFilterQuestions = questions
        // show filter for question & referenceQuestion only
        .filter(
          q =>
            q.id === nextSettings.question ||
            q.id === nextSettings.referenceQuestion
        )
        .reduce(
          (acc, question) => ({
            ...acc,
            [question.id]: (question.options || []).map(o => o.value)
          }),
          {}
        )
      setFilterQuestions(initFilterQuestions)
      history.push({
        pathname: `${match.url}/filter`
      })
    },
    [history, match, products, questions]
  )

  const handleFilterSave = useCallback((filterProducts, filterQuestions) => {
    setFilterProducts(filterProducts)
    setFilterQuestions(filterQuestions)
  }, [])

  const handleCreateJob = useCallback(
    async (settings, filterProducts, filterQuestions) => {
      const productNames = Object.assign(
        ...products
          .filter(p => filterProducts.includes(p.id))
          .map(p => ({
            [p.id]: p.name
          }))
      )

      const {
        data: { jobGroup }
      } = await createJobs.current({
        variables: {
          input: {
            ...settings,
            productNames,
            questionCriteria: filterQuestions
            //  TODO: cehck if dateFilter needed
          }
        }
      })

      return jobGroup
    },
    [products]
  )

  return (
    <OperatorSurveyAnalysisComponent
      loading={loading}
      settings={settings}
      filterProducts={filterProducts}
      filterQuestions={filterQuestions}
      onSettingsSave={handleSettingsSave}
      onFilterSave={handleFilterSave}
      onCreateJob={handleCreateJob}
      products={products}
      questions={questions}
    />
  )
}

const SURVEY_QUERY = gql`
  query survey($id: ID!) {
    survey(id: $id) {
      id
      products {
        id
        name
        photo
      }
      productsQuestions {
        id
        prompt
        type
        chartTitle
        options {
          label
          value
        }
      }
    }
  }
`

const CREATE_JOBS = gql`
  mutation createJobs($input: CreateJobsInput!) {
    jobGroup: createJobs(input: $input)
  }
`

export default withRouter(OperatorSurveyAnalysis)
