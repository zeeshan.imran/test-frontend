import { path } from 'ramda'
import React, { useState } from 'react'
import StatsAnalysisFormComponent from '../../components/StatsAnalysisForm'
import { useTranslation } from 'react-i18next'
import useReferenceQuestions from '../../hooks/useReferenceQuestions'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import { getAuthenticatedUser } from '../../utils/userAuthentication'
import isFlavorwikiUser from '../../utils/isFlavorwikiUser'
import isMondelezUser from '../../utils/isMondelezUser'
import isChrisHansenUser from '../../utils/isChrisHansenUser'

const getDefaultValue = (name, question) => {
  switch (name) {
    case 'penaltyLevel': {
      const level = path(['range', 'labels', 'length'], question)
      return [3, 5, 7, 9].includes(level) ? level : 9
    }
    case 'alphaLevel': {
      return 0.05
    }
    default:
      return null
  }
}

const correctValue = (name, value) => {
  switch (name) {
    case 'penaltyLevel':
      return parseFloat(value)
    case 'alphaLevel':
      return value === '' ? value : parseFloat(value)
    default:
      return value
  }
}

const allowedUsers = [
  'laurence.minisini@givaudan.com',
  'jeremy.roque@givaudan.com',
  'sophie.davodeau@givaudan.com'
]

const StatsAnalysisForm = ({
  isOwner,
  availableStatistics,
  surveyId,
  question: questionId,
  loading,
  statsApi
}) => {
  const { t } = useTranslation()
  const referenceQuestions = useReferenceQuestions(surveyId, questionId)
  const { loading: loadingForm, data } = useQuery(QUERY_QUESTION, {
    variables: { id: questionId }
  })
  const [jobs, setJobs] = useState([])

  if (loadingForm) {
    return null
  }

  const enabledFeature =
    isFlavorwikiUser(getAuthenticatedUser()) ||
    allowedUsers.includes(getAuthenticatedUser().emailAddress) ||
    isMondelezUser(getAuthenticatedUser()) ||
    isChrisHansenUser(getAuthenticatedUser())

  if (!enabledFeature) {
    return null
  }

  // NOTE: Who can use this features?
  //  - Anyone with his/her surveys
  //  - Givaudan with their surveys & shared surveys
  //  - Mondelez with their surveys

  const isAllowed =
    isOwner ||
    allowedUsers.includes(getAuthenticatedUser().emailAddress) ||
    isMondelezUser(getAuthenticatedUser()) ||
    isChrisHansenUser(getAuthenticatedUser())

  if (!isAllowed) {
    return null
  }

  const { question } = data

  let settingsError = false
  if (jobs.length === 0) {
    settingsError = t('charts.analysis.errors.selectionRequired')
  }

  const selectRequiredParameter = (jobType, selection, parameter) => {
    const jobIndex = jobs.findIndex(el => el.analysisType === jobType)
    const newJobs = [
      ...jobs.slice(0, jobIndex),
      { ...jobs[jobIndex], [parameter]: correctValue(parameter, selection) },
      ...jobs.slice(jobIndex + 1)
    ]
    setJobs(newJobs)
  }

  const selectAnalysis = ({ type }) => {
    const jobIndex = jobs.findIndex(el => el.analysisType === type)
    let newJobs = []
    if (jobIndex !== -1) {
      newJobs = [...jobs.slice(0, jobIndex), ...jobs.slice(jobIndex + 1)]
    } else {
      let job = {
        analysisType: type
      }
      const newJobIndex = availableStatistics.findIndex(el => el.type === type)
      availableStatistics[newJobIndex].requireParameters.forEach(
        parameter => (job[parameter] = getDefaultValue(parameter, question))
      )
      newJobs = [...jobs, job]
    }
    setJobs(newJobs)
  }

  if (!availableStatistics || availableStatistics.length === 0) {
    return null
  }

  const getStatistics = () => {
    statsApi.runStatistic({
      question: questionId,
      analysisRequests: jobs
    })
  }

  return (
    <StatsAnalysisFormComponent
      jobs={jobs}
      availableOptions={availableStatistics}
      selectAnalysis={selectAnalysis}
      settingsError={settingsError}
      questions={referenceQuestions}
      selectRequiredParameter={selectRequiredParameter}
      getStatistics={getStatistics}
      loading={loading}
    />
  )
}

const QUERY_QUESTION = gql`
  query getQuestion($id: ID!) {
    question(id: $id) {
      id
      range {
        labels
      }
    }
  }
`

export default StatsAnalysisForm
