import React, { useCallback, memo } from 'react'
import QuestionCreationCardTypeSelectComponent from '../../components/QuestionCreationCardTypeSelect'
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo-hooks'
import {
  SCREENING_QUESTION_TYPES,
  BEFORE_QUESTION_TYPES,
  LOOP_QUESTION_TYPES,
  AFTER_QUESTION_TYPES,
  PAYMENT_QUESTION_TYPES
} from '../../utils/Constants'
import templates from '../../utils/questionTemplates'
import defaults from '../../defaults'
import i18n from '../../utils/internationalization/i18n'

const QuestionCreationCardTypeSelect = ({
  questionIndex,
  question,
  mandatoryQuestion,
  hasDemographic,
  demographicField
}) => {
  const {
    data: { surveyCreation = {} }
  } = useQuery(GET_QUESTION_TYPES)

  const {
    uniqueQuestionsToCreate = [],
    basics: { country, surveyLanguage } = {}
  } = surveyCreation

  const availableCountry = defaults.countries.find(c => c.code === country)
  const rewardCurrency = availableCountry || {}
  const currencyPrefix =
    defaults.currencyPrefixes[rewardCurrency.currency] || ''
  const currency = defaults.currencySuffixes[rewardCurrency.currency] || ''

  const updateQuestionTypeMutation = useMutation(
    UPDATE_SURVEY_CREATION_QUESTION_TYPE
  )
  const updateQuestionType = useCallback(
    type => {
      updateQuestionTypeMutation({
        variables: {
          questionType: type,
          questionIndex,
          id: question.id
        }
      })
    },
    [questionIndex, updateQuestionTypeMutation]
  )

  const filteredTemplates = templates
    .filter(
      template =>
        template.questions === question.type && template.country !== country
    )
    .map(template => ({
      ...template,
      getData: undefined,
      data:
        template &&
        template.getData({
          t: i18n.getFixedT(surveyLanguage),
          suffix: currency,
          prefix: currencyPrefix,
          country: country
        })
    }))

  const updateQuestionMutation = useMutation(UPDATE_SURVEY_CREATION_QUESTION)
  const onSelectTemplate = name => {
    const templateIndex = filteredTemplates.findIndex(
      template => template.name === name
    )
    const template = filteredTemplates[templateIndex]

    const newValues = template && template.data ? template.data : {}
    updateQuestionMutation({
      variables: {
        questionField: { demographicField: name, ...newValues },
        questionIndex,
        mandatoryQuestion
      }
    })
  }

  const updateQuestion = useCallback(
    field => {
      updateQuestionMutation({
        variables: {
          questionField: field,
          questionIndex,
          mandatoryQuestion
        }
      })
    },
    [questionIndex, updateQuestionMutation, mandatoryQuestion]
  )

  let types
  switch (question.displayOn) {
    case 'payments':
      types = PAYMENT_QUESTION_TYPES
      break
    case 'screening':
      types = SCREENING_QUESTION_TYPES
      break
    case 'begin':
      types = BEFORE_QUESTION_TYPES
      break
    case 'middle':
      types = LOOP_QUESTION_TYPES
      break
    case 'end':
      types = AFTER_QUESTION_TYPES
      break
    default:
      types = LOOP_QUESTION_TYPES
  }

  if (
    [
      'choose-product',
      'paypal-email',
      'choose-payment',
      'show-product-screen'
    ].includes(question.type)
  ) {
    return null
  }

  const isStrictMode = question.editMode === 'strict'
  return (
    <QuestionCreationCardTypeSelectComponent
      disabled={mandatoryQuestion || isStrictMode}
      types={types}
      type={question.type}
      existingUniqueTypes={uniqueQuestionsToCreate}
      onSelect={data => {
        if (question.type === 'info') {
          updateQuestion({ secondaryPrompt: '' })
        }
        updateQuestionType(data)
      }}
      hasDemographic={hasDemographic}
      demographicField={demographicField}
      templates={filteredTemplates}
      onSelectTemplate={onSelectTemplate}
    />
  )
}

const GET_QUESTION_TYPES = gql`
  query {
    surveyCreation @client {
      products
      uniqueQuestionsToCreate
      basics {
        country
        surveyLanguage
      }
    }
  }
`

const UPDATE_SURVEY_CREATION_QUESTION_TYPE = gql`
  mutation updateSurveyCreationQuestionType(
    $questionType: String
    $questionIndex: Int
  ) {
    updateSurveyCreationQuestionType(
      questionType: $questionType
      questionIndex: $questionIndex
    ) @client
  }
`
const UPDATE_SURVEY_CREATION_QUESTION = gql`
  mutation updateSurveyCreationQuestion(
    $questionField: Question
    $questionIndex: Int
    $mandatoryQuestion: Boolean
  ) {
    updateSurveyCreationQuestion(
      questionField: $questionField
      questionIndex: $questionIndex
      mandatoryQuestion: $mandatoryQuestion
    ) @client
  }
`

export default memo(QuestionCreationCardTypeSelect)
