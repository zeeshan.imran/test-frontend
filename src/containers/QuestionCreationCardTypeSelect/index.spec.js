import React from 'react'
import { mount } from 'enzyme'
import QuestionCreationCardTypeSelect from '.'
import wait from '../../utils/testUtils/waait'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import gql from 'graphql-tag'
import { createBrowserHistory } from 'history'
import { Router } from 'react-router-dom'
import QuestionCreationCardTypeSelectComponent from '../../components/QuestionCreationCardTypeSelect'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import { defaultSurvey } from '../../mocks'

jest.mock('../../resolvers/updateSurveyCreationQuestionType/index.js')
jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k,
  exists: val => !!val,
  getFixedT: () => () => ''
}))

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

const mockSurveyCreation = {
  ...defaultSurvey,
  surveyId: 'survey-1',
  basics: {
    ...defaultSurvey.basics,
    __typename: undefined,
    name: 'Survey 1',
    uniqueName: 'survey-1',
    customButtons: {
      continue: 'Continue',
      start: 'Start',
      next: 'Next',
      skip: 'Skip'
    },
    autoAdvanceSettings: {
      active: false,
      debounce: 0,
      hideNextButton: false
    },
    pdfFooterSettings:{
      active:false,
      footerNote:""
    }
  },
  products: [
    {
      id: 'product-1',
      name: 'product',
      brand: 'brand'
    }
  ],
  questions: [
    {
      id: 'question-1',
      type: 'info',
      prompt: 'What is your email?'
    }
  ]
}

const GET_QUESTION_TYPES = gql`
  query {
    surveyCreation @client {
      products
      uniqueQuestionsToCreate
    }
  }
`

describe('QuestionCreationCardTypeSelect', () => {
  let testRender
  let client

  beforeEach(() => {
    client = createApolloMockClient()

    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: mockSurveyCreation
      }
    })

    // client.cache.writeQuery({
    //   query: GET_QUESTION_TYPES,
    //   data: {
    //     surveyCreation: {
    //       products: [
    //         {
    //           id: 'product-1',
    //           name: 'product',
    //           brand: 'brand'
    //         }
    //       ]
    //     }
    //   }
    // })
  })

  test('should render QuestionCreationCardTypeSelect', async () => {
    const history = createBrowserHistory()

    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <QuestionCreationCardTypeSelect
            question={{
              id: 'question-1',
              type: 'info',
              prompt: 'What is your email?',
              secondaryPrompt: 'secondaryPrompt'
            }}
          />
        </Router>
      </ApolloProvider>
    )

    await wait(0)

    testRender
      .find(QuestionCreationCardTypeSelectComponent)
      .props()
      .onSelect({
        id: 'question-1',
        type: 'info',
        prompt: 'What is your email?',
        secondaryPrompt: 'secondaryPrompt'
      })

    testRender.unmount()
  })
  test('should render QuestionCreationCardTypeSelect type of choose - product', async () => {
    const history = createBrowserHistory()

    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <QuestionCreationCardTypeSelect
            question={{
              id: 'question-1',
              type: 'choose-product',
              prompt: 'Product here'
            }}
          />
        </Router>
      </ApolloProvider>
    )

    await wait(0)

    expect(testRender).toMatchSnapshot()

    testRender.unmount()
  })
})
