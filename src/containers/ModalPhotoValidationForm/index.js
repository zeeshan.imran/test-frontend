import React, { useMemo } from 'react'
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo-hooks'
import { Modal } from 'antd'
import PhotoValidationForm from '../../components/PhotoHub/PhotoValidationForm'

export const GET_PHOTO_VALIDATION_LOGS = gql`
  query photoValidationLogs($surveyEnrollmentId: ID!) {
    photoValidationLogs(surveyEnrollmentId: $surveyEnrollmentId) {
      id
      createdAt
      action
      type
      collections
      values
      by {
        id
        fullName
        emailAddress
      }
      impersonatedBy {
        id
        fullName
        emailAddress
      }
    }
  }
`

export const GET_USER_VALIDATION_COUNT = gql`
  query userValidations($userId: ID!) {
    userValidations(userId: $userId) {
      validCount
      validAnswers {
        id
        value
      }
      invalidCount
      invalidAnswers {
        id
        value
      }
    }
  }
`

export const GET_PAYPAL_USERS = gql`
  query paypalUsers($paypalEmailAddress: String!) {
    paypalUsers(paypalEmailAddress: $paypalEmailAddress) {
      id
      fullName
      emailAddress
      paypalEmailAddress
      country
      state
      blackListed
      surveyEnrollments {
        id
      }
    }
  }
`

export const ADD_TO_BLACK_LIST = gql`
  mutation blackListUser($id: ID!) {
    blackListUser(id: $id)
  }
`

const ModalPhotoValidationForm = ({
  isVisible,
  isUpdating,
  canModify,
  selectedAnswer = {},
  setSelectedAnswer,
  onOk,
  onCancel,
  survey,
  refetchEnrollments
}) => {
  const addToBlackList = useMutation(ADD_TO_BLACK_LIST)

  const paypalEmailAddress = useMemo(() => {
    if (selectedAnswer.paypalEmail) {
      return selectedAnswer.paypalEmail
    }

    if (selectedAnswer.user && selectedAnswer.user.paypalEmailAddress) {
      return selectedAnswer.user.paypalEmailAddress
    }

    return null
  }, [selectedAnswer])

  const {
    loading,
    data: { photoValidationLogs = [] } = {}
  } = useQuery(GET_PHOTO_VALIDATION_LOGS, {
    fetchPolicy: 'cache-and-network',
    skip: !selectedAnswer.id,
    variables: {
      surveyEnrollmentId: selectedAnswer.id
    }
  })

  const { data: { userValidations } = {} } = useQuery(
    GET_USER_VALIDATION_COUNT,
    {
      fetchPolicy: 'cache-and-network',
      skip: !selectedAnswer.user || !selectedAnswer.user.id,
      variables: {
        userId: selectedAnswer.user && selectedAnswer.user.id
      }
    }
  )

  const { refetch: refetchUsers, data: { paypalUsers = [] } = {} } = useQuery(GET_PAYPAL_USERS, {
    fetchPolicy: 'cache-and-network',
    skip: !paypalEmailAddress,
    variables: {
      paypalEmailAddress: paypalEmailAddress
    }
  })

  const availableUsers = useMemo(() => {
    if (selectedAnswer.user) {
      return paypalUsers.filter((user) => {
        return user.id !== selectedAnswer.user.id
      })
    }

    return paypalUsers
  }, [selectedAnswer, paypalUsers])

  const onAddToBlackList = async userId => {
    await addToBlackList({
      variables: {
        id: userId
      }
    })

    refetchEnrollments()
    refetchUsers()
  }

  return (
    <Modal
      visible={isVisible}
      keyboard={false}
      closable={false}
      okButtonProps={{
        disabled: !canModify || isUpdating
      }}
      onOk={onOk}
      onCancel={onCancel}
    >
      <PhotoValidationForm
        loading={loading}
        survey={survey}
        paypalEmailAddress={paypalEmailAddress}
        paypalUsers={availableUsers}
        photoValidationLogs={photoValidationLogs}
        userValidations={userValidations}
        enrollment={selectedAnswer}
        setEnrollment={setSelectedAnswer}
        canModify={canModify}
        onAddToBlackList={onAddToBlackList}
      />
    </Modal>
  )
}

export default ModalPhotoValidationForm
