import React from 'react'
import { mount } from 'enzyme'
import SurveyScreeningCompleted from './index'
import gql from 'graphql-tag'
import { ApolloProvider } from 'react-apollo-hooks'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import { SURVEY_QUERY } from '../../queries/Survey'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import defaults from '../../defaults'
import { act } from 'react-test-renderer'
import wait from '../../utils/testUtils/waait'
import history from '../../history'
import { Router } from 'react-router-dom'
import i18n from '../../utils/internationalization/i18n'
import sinon from 'sinon'
import SurveyInfoText from '../../components/SurveyInfoText'

const SAVE_SURVEY_PARTICIPATION = gql`
  mutation saveCurrentSurveyParticipation($selectedProduct: String) {
    saveCurrentSurveyParticipation(selectedProduct: $selectedProduct) @client
  }
`
const FINISH_SURVEY_SCREENING = gql`
  mutation finishSurveyScreening($input: EnrollmentStateInput) {
    finishSurveyScreening(input: $input)
  }
`

describe('SurveyScreeningCompleted', () => {
  let testRender
  let mockClient
  let spyChange

  beforeEach(() => {
    mockClient = createApolloMockClient()
    mockClient.cache.writeQuery({
      query: SURVEY_PARTICIPATION_QUERY,
      data: {
        currentSurveyParticipation: {
          ...defaults.currentSurveyParticipation,
          surveyId: 'survey-1',
          products: ['product-1', 'product-2'],
          selectedProducts: ['product-1'],
          answers: [],
          surveyEnrollmentId: 'surveyEnrollment',
          productRewardsRule: {
            active: false,
            min: 0,
            max: 0,
            percentage: 0
          }
        }
      }
    })

    mockClient.cache.writeQuery({
      query: SURVEY_QUERY,
      variables: {
        id: 'survey-1'
      },

      data: {
        survey: {
          products: [],
          customButtons: {
            continue: 'Continue',
            start: 'Start',
            next: 'Next',
            skip: 'Skip'
          },
          surveyLanguage: 'en',
          screeningText: null,
          customizeSharingMessage: '',
          loginText: '',
          pauseText: ''
        }
      }
    })

    mockClient.cache.writeQuery({
      query: SAVE_SURVEY_PARTICIPATION,
      variables: {
        selectedProduct: 'product-1'
      },
      data: {
        ok: '',
        error: ''
      }
    })

    mockClient.cache.writeQuery({
      query: FINISH_SURVEY_SCREENING,
      variables: {
        input: {
          surveyEnrollment: 'surveyEnrollment'
        }
      },
      data: {
        surveyEnrollmentId: 'surveyEnrollment'
      }
    })

    const stub = sinon.stub(mockClient, 'mutate')
    spyChange = jest.fn()
    stub
      .withArgs(sinon.match({ mutation: FINISH_SURVEY_SCREENING }))
      .callsFake(spyChange)
    stub.callThrough()
  })
  afterEach(() => {})

  test('should render SurveyScreeningCompleted', async () => {
    const mockChangeLang = jest.fn()
    i18n.changeLanguage = mockChangeLang
    i18n.language = 'de'

    act(() => {
      testRender = mount(
        <ApolloProvider client={mockClient}>
          <Router history={history}>
            <SurveyScreeningCompleted surveyId={'survey-1'} />
          </Router>
        </ApolloProvider>
      )
    })
    await wait(0)
    expect(testRender).toMatchSnapshot()

    testRender.unmount()
  })

  test('should render SurveyScreeningCompleted', async () => {
    const mockChangeLang = jest.fn()
    i18n.changeLanguage = mockChangeLang
    i18n.language = 'en'

    act(() => {
      testRender = mount(
        <ApolloProvider client={mockClient}>
          <Router history={history}>
            <SurveyScreeningCompleted surveyId={'survey-1'} />
          </Router>
        </ApolloProvider>
      )
    })
    await wait(500)
    spyChange.mockReset()
    testRender.unmount()
  })
})
