import { path } from 'ramda'
import React, { useCallback, useEffect } from 'react'
import SurveyScreeningInfoText from '../../components/SurveyScreeningInfoText'
import gql from 'graphql-tag'
import { useMutation, useQuery } from 'react-apollo-hooks'
import { withTranslation } from 'react-i18next'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import { SURVEY_QUERY } from '../../queries/Survey'

const SurveyScreeningCompleted = ({ t, surveyId, i18n }) => {
  const {
    data: {
      currentSurveyParticipation: { surveyEnrollmentId }
    }
  } = useQuery(SURVEY_PARTICIPATION_QUERY)

  const finishSurveyScreening = useMutation(FINISH_SURVEY_SCREENING)
  const handleAdvance = useMutation(ADVANCE_IN_SURVEY)

  const { data } = useQuery(SURVEY_QUERY, {
    variables: { id: surveyId }
  })
  let buttonLabel
  if (data.survey) {
    buttonLabel = t('defaultValues.default.customButtons.next')
    if (data.survey.customButtons && data.survey.customButtons.next) {
      buttonLabel = data.survey.customButtons.next
    }

    let surveyLanguage = data.survey.surveyLanguage || 'en'
    if (i18n.language !== surveyLanguage) {
      i18n.changeLanguage(surveyLanguage)
    }
  }

  const onClick = useCallback(async () => {
    await handleAdvance({ variables: { fromStep: 'screening-completed' } })
  }, [handleAdvance])

  useEffect(() => {
    if (surveyEnrollmentId) {
      finishSurveyScreening({
        variables: { input: { surveyEnrollment: surveyEnrollmentId } }
      })
    }
  }, [surveyEnrollmentId])

  const screeningText = path(['survey', 'screeningText'], data)

  return (
    <SurveyScreeningInfoText
      description={screeningText || t('defaultValues.screeningText')}
      onClick={onClick}
      buttonLabel={buttonLabel}
    />
  )
}

const ADVANCE_IN_SURVEY = gql`
  mutation advanceInSurvey($fromStep: ID!) {
    advanceInSurvey(fromStep: $fromStep) @client
  }
`

const FINISH_SURVEY_SCREENING = gql`
  mutation finishSurveyScreening($input: EnrollmentStateInput) {
    finishSurveyScreening(input: $input)
  }
`

export default withTranslation()(SurveyScreeningCompleted)
