import React from 'react'
import { Query } from 'react-apollo'
import Loader from '../../components/Loader'
import SurveysGridComponent from '../../components/SurveysGrid'
import { POINTS_SURVEY_QUERY } from '../../queries/PointsSurveysGrid'

const PointsSurveysGrid = () => (
  <Query query={POINTS_SURVEY_QUERY}>
    {({ data: { surveys }, loading }) => {
      if (loading) return <Loader />
      return <SurveysGridComponent surveys={surveys} />
    }}
  </Query>
)

export default PointsSurveysGrid
