import React, { useState } from 'react'
import Text from '../../components/Text'
import IncentiveModal from './UploadIncentiveModal'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

const ImportProductIncentives = ({ state, survey, onMenuItemClick }) => {
  const { t } = useTranslation()
  const [isVisible, setIsVisible] = useState(false)

  const toggleModal = () => {
    setIsVisible(!isVisible)
  }
  return (
    <React.Fragment>
      <Text
        onClick={() => {
          toggleModal()
          onMenuItemClick()
        }}
      >
        {t('tooltips.importProductIncentives')}
      </Text>
      <IncentiveModal
        visible={isVisible}
        state={state}
        survey={survey}
        t={t}
        toggleModal={toggleModal}
      />
    </React.Fragment>
  )
}

export default withRouter(ImportProductIncentives)
