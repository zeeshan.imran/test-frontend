import React from 'react'
import { mount } from 'enzyme'
import { ApolloProvider } from 'react-apollo-hooks'
import copy from 'copy-to-clipboard'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import OperatorQrCodesComponent from '../../components/OperatorQrCodes'
import OperatorQrCodes, {
  QUERY_QRCODES,
  DELETE_QR_CODE,
  QUERY_COUNT_QRCODES
} from './index'
import wait from '../../utils/testUtils/waait'
import { Modal } from 'antd'

jest.mock('copy-to-clipboard')
jest.mock('../../utils/getErrorMessage')
jest.mock('../../components/AlertModal', () => ({ handleOk }) => {
  handleOk()
})

jest.mock('../../utils/internationalization/i18n', () => ({
  t: text => text,
  getFixedT: () => () => ''
}))

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: text => text }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

const mockCountQrCodesResult = { data: { count: 2 } }

const mockQrCodesResult = {
  data: {
    qrCodes: [
      {
        id: 'qr-code-1',
        name: 'Qr Code 1',
        uniqueName: 'qr-code-1',
        redirectLink: '/qr-code/org-1/qr-code-1',
        targetType: 'link',
        survey: null,
        targetLink: '',
        qrCodePhoto: 'https://fake-s3/bucket/qr-code/org-1/qr-code-1'
      },
      {
        id: 'qr-code-2',
        name: 'Qr Code 2',
        uniqueName: 'qr-code-2',
        redirectLink: '/qr-code/org-1/qr-code-2',
        targetType: 'survey',
        survey: {
          id: 'survey-1',
          name: 'Survey 1',
          uniqueName: 'survey-1',
          savedRewards: [],
          country: 'United States of America'
        },
        targetLink: '',
        qrCodePhoto: 'https://fake-s3/bucket/qr-code/org-1/qr-code-2'
      }
    ]
  }
}

describe('Qr Codes', () => {
  global.jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000
  let client
  let history
  let deleteQrCode
  let render

  beforeEach(() => {
    deleteQrCode = jest.fn()
    client = createApolloMockClient({
      mocks: [
        {
          request: {
            query: QUERY_QRCODES,
            variables: {
              input: {
                keyword: '',
                orderBy: undefined,
                orderDirection: undefined,
                skip: 0
              }
            }
          },
          result: mockQrCodesResult
        },
        {
          request: {
            query: QUERY_COUNT_QRCODES,
            variables: { input: { keyword: '' } }
          },
          result: mockCountQrCodesResult
        },
        {
          request: {
            query: QUERY_QRCODES,
            variables: {
              input: {
                keyword: '',
                orderBy: undefined,
                orderDirection: undefined,
                skip: 0
              }
            }
          },
          result: mockQrCodesResult
        },
        {
          request: {
            query: QUERY_COUNT_QRCODES,
            variables: { input: { keyword: '' } }
          },
          result: mockCountQrCodesResult
        },
        {
          request: { query: DELETE_QR_CODE, variables: { id: 'qr-code-1' } },
          result: () => {
            deleteQrCode()
            return {
              data: {
                deleteQrCode: 'qr-code-1'
              }
            }
          }
        }
      ]
    })

    history = {
      push: jest.fn()
    }
  })

  afterEach(() => {
    render.unmount()
  })

  test('should show loading', async () => {
    render = mount(
      <ApolloProvider client={client}>
        <OperatorQrCodes.WrappedComponent location={{ search: '' }} />
      </ApolloProvider>
    )

    expect(
      render
        .find(OperatorQrCodesComponent)
        .first()
        .prop('loading')
    ).toBe(true)
  })

  test('should show Qr Code List after loading', async () => {
    render = mount(
      <ApolloProvider client={client}>
        <OperatorQrCodes.WrappedComponent location={{ search: '' }} />
      </ApolloProvider>
    )

    await wait(0)
    render.update()

    expect(
      render
        .find(OperatorQrCodesComponent)
        .first()
        .prop('qrCodes')
    ).toHaveLength(2)
  })

  test('should redirect to ./qr-codes/create when clicking on Create Qr', async () => {
    render = mount(
      <ApolloProvider client={client}>
        <OperatorQrCodes.WrappedComponent
          history={history}
          location={{ search: '' }}
        />
      </ApolloProvider>
    )

    await wait(0)
    render.update()

    render.find('button[data-testid="create-qr"]').simulate('click')

    await wait(0)

    expect(history.push).toHaveBeenCalledWith('./qr-codes/create')
  })

  test('should redirect to ./qr-codes/qr-code-1 when clicking on Edit button on the first row', async () => {
    render = mount(
      <ApolloProvider client={client}>
        <OperatorQrCodes.WrappedComponent
          history={history}
          location={{ search: '' }}
        />
      </ApolloProvider>
    )

    await wait(0)
    render.update()

    render
      .find('[data-testid="edit-qr"]')
      .first()
      .simulate('click')

    await wait(0)

    expect(history.push).toHaveBeenCalledWith('./qr-codes/qr-code-1')
  })

  test('should copy to clipboard the link when clicking on Copy button on the first row', async () => {
    render = mount(
      <ApolloProvider client={client}>
        <OperatorQrCodes.WrappedComponent
          history={history}
          location={{ search: '' }}
        />
      </ApolloProvider>
    )

    await wait(0)
    render.update()

    render
      .find('[data-testid="copy-qr"]')
      .first()
      .simulate('click')

    await wait(0)

    expect(copy).toHaveBeenCalledWith(
      'http://localhost/qr-code/org-1/qr-code-1'
    )
  })

  test('should open the download modal when clicking on the download button', async () => {
    render = mount(
      <ApolloProvider client={client}>
        <OperatorQrCodes.WrappedComponent
          history={history}
          location={{ search: '' }}
        />
      </ApolloProvider>
    )

    await wait(0)
    render.update()

    render
      .find('[data-testid="download-qr"]')
      .first()
      .simulate('click')

    await wait(0)

    expect(render.find(Modal)).toMatchSnapshot()
  })

  test('should call mutation to delete Qr code when clicking on Delete button on the first row', async () => {
    render = mount(
      <ApolloProvider client={client}>
        <OperatorQrCodes.WrappedComponent
          history={history}
          location={{ search: '' }}
        />
      </ApolloProvider>
    )

    await wait(0)
    render.update()

    await render
      .find('[data-testid="delete-qr"]')
      .first()
      .simulate('click')

    await wait(0)
    render.update()

    const deleteButton = global.document.querySelector(
      '.ant-modal-body .ant-btn-primary'
    )
    // expect(deleteButton.textContent).toBe('Proceed')
    // deleteButton.click()

    await wait(0)

    expect(deleteQrCode).toHaveBeenCalledWith()
  })
})
