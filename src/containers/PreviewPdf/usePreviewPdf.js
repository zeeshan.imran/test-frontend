import { useCallback, useState } from 'react'
import { pick } from 'ramda'
import * as pdfUtils from '../../utils/pdfUtils'
import {
  PDF_LAYOUT_PROPS,
  PDF_LAYOUT_DEFAULT_PROPS
} from '../../utils/Constants'
import {
  isHeaderVisible,
  isFooterVisible
} from '../../utils/pdfUtils/headerFooterUtils'
import { PAPER_SIZES } from '../../utils/paperSizes'

const usePreviewPdf = (survey, settings, operatorOrTaster) => {
  const [pages, setPages] = useState([])
  const [layout, setLayout] = useState(null)

  const applyLayout = useCallback(
    (stats, userLayout, footerNote) => {
      let layout = userLayout
      if (!userLayout) {
        const paperSize = 'A4'
        layout = {
          ...PDF_LAYOUT_DEFAULT_PROPS,
          pages: pdfUtils.calculateElementsLayout('A4', settings, stats, operatorOrTaster),
          paperSize
        }
      }
      setLayout(layout)

      const layoutProps = pick(PDF_LAYOUT_PROPS)(layout)
      const elementItems = pdfUtils.createElementItems(
        survey.name,
        stats,
        settings,
        operatorOrTaster
      )

      const pages = layout
        ? layout.pages
        : pdfUtils.calculateElementsLayout(layout.paperSize, settings, stats, operatorOrTaster)

      const paperSizeMeta = PAPER_SIZES[layout.paperSize]

      const pagesWithContent = pages.map((itemsInPage, page) => [
        ...itemsInPage.map(gridItem => ({
          ...gridItem,
          ...elementItems.find(e => e.i === gridItem.i)
        })),
        ...(isHeaderVisible(layoutProps)(page)
          ? [
              {
                ...pdfUtils.createHeader({
                  page,
                  totalPages: pages.length,
                  ...layoutProps
                }),
                x: 0,
                y: 0,
                w: paperSizeMeta.cols,
                h: 2
              }
            ]
          : []),
        ...(isFooterVisible(footerNote, layoutProps)(page)
          ? [
              {
                ...pdfUtils.createFooter(footerNote, {
                  page,
                  totalPages: pages.length,
                  ...layoutProps
                }),
                x: 0,
                y: paperSizeMeta.rows - 2,
                w: paperSizeMeta.cols,
                h: 2
              }
            ]
          : [])
      ])

      const totalPages = pagesWithContent.length
      pagesWithContent.forEach((itemsInPage, page) =>
        pdfUtils.adjustTopBottomMargin(
          layout.paperSize,
          itemsInPage,
          0,
          isHeaderVisible(layoutProps)(page) ? 2 : 1,
          isFooterVisible(footerNote, {
            totalPages,
            ...layoutProps
          })(page)
            ? 2
            : 1
        )
      )

      setPages(pagesWithContent)
    },
    [survey, settings, operatorOrTaster]
  )

  return { pages, layout, applyLayout }
}

export default usePreviewPdf
