import React from 'react'
import { mount } from 'enzyme'
import DeleteSurveyButton from '.'
import gql from 'graphql-tag'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import AlertModal from '../../components/AlertModal'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import Text from '../../components/Text'
import sinon from 'sinon'

jest.mock('../../components/AlertModal')

describe('DeleteSurveyButton', () => {
  let testRender
  let client
  let spyChange
  let history 

  beforeEach(() => {
    client = createApolloMockClient()
    const DEPRECATE_SURVEY = gql`
      mutation deprecateSurvey($id: ID!) {
        deprecateSurvey(id: $id)
      }
    `

    client.cache.writeQuery({
      query: DEPRECATE_SURVEY,
      variables: { id: 'survey-1' },
      data: {
        survey: {
          __typename: 'SurveyCreation',
          surveyId: 'survey-1',
          surveyEnrollmentId: null,
          state: 'draft'
        }
      }
    })

    const stub = sinon.stub(client, 'mutate')
    spyChange = jest.fn()
    stub
      .withArgs(sinon.match({ mutation: DEPRECATE_SURVEY }))
      .callsFake(spyChange)
    stub.callThrough()

    history = createBrowserHistory()
  })

  afterEach(() => {
    testRender.unmount()
  })
  test('should render DeleteSurveyButton', () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <DeleteSurveyButton surveyId={'survey-1'} state={'draft'} onMenuItemClick={jest.fn()} />
        </Router>
      </ApolloProvider>
    )
    expect(testRender).toMatchSnapshot()

    testRender
      .find(Text)
      .first()
      .prop('onClick')()

    const onClickFn = testRender.find(Text).prop('onClick')

    spyChange.mockReset()
    AlertModal.mockImplementationOnce(({ handleOk }) => handleOk())
    onClickFn()
    expect(spyChange).toHaveBeenCalled()
  })
})
