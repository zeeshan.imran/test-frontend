import React from 'react'
import { withRouter } from 'react-router-dom'
import { useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import Text from '../../components/Text'
import AlertModal from '../../components/AlertModal'
import { useTranslation } from 'react-i18next'
import { message } from 'antd'
import PubSub from 'pubsub-js'
import { PUBSUB } from '../../utils/Constants'

const DeleteSurveyButton = ({
  state,
  surveyId,
  onMenuItemClick,
  surveyName
}) => {
  const { t } = useTranslation()
  const deleteSurvey = useMutation(DEPRECATE_SURVEY, {
    variables: { id: surveyId },
    refetchQueries: ['surveys', 'surveysTotal']
  })

  const onClick = () => {
    onMenuItemClick()
    AlertModal({
      title:
        state === 'draft'
          ? t('containers.deleteSurveyButton.alertModalTitleYes', { name: surveyName })
          : t('containers.deleteSurveyButton.alertModalTitleNo', { name: surveyName }),
      description:
        state === 'draft'
          ? t('containers.deleteSurveyButton.alertModalDescriptionYes')
          : t('containers.deleteSurveyButton.alertModalDescriptionNo'),
      okText: t('containers.deleteSurveyButton.alertModalOkText'),
      handleOk: async () => {
        const response = await deleteSurvey()
        if (response && response.data && response.data.deprecateSurvey) {
          message.success(t(`containers.deleteSurveyButton.success`))
          PubSub.publish(PUBSUB.REFETCH_FOLDERS)
        } else {
          message.error(t(`containers.deleteSurveyButton.error`))
        }
      },
      handleCancel: () => {}
    })
  }

  return <Text onClick={onClick}>{t('tooltips.deleteSurveyButton')}</Text>
}

const DEPRECATE_SURVEY = gql`
  mutation deprecateSurvey($id: ID!) {
    deprecateSurvey(id: $id)
  }
`

export default withRouter(DeleteSurveyButton)
