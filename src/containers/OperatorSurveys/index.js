import React, { useState, useEffect } from 'react'
import gql from 'graphql-tag'
import { withRouter } from 'react-router-dom'
import { useMutation, useQuery } from 'react-apollo-hooks'
import OperatorSurveysComponent from '../../components/OperatorSurveys'
import useAllOrganizations from '../../hooks/useAllOrganizations'
import getQueryParams from '../../utils/getQueryParams'
import OperatorPage from '../../components/OperatorPage'
import OperatorPageContent from '../../components/OperatorPageContent'
import { folderFragment } from '../../fragments/folder'
import PubSub from 'pubsub-js'
import { PUBSUB } from '../../utils/Constants'

const OperatorSurveys = ({ location, history, match }) => {
  const { page = 0, folderId } = getQueryParams(location)
  const [searchTerm, setSearchTerm] = useState('')
  const [surveyState, setSurveyState] = useState('')
  const [currentFolder, setCurrentFolder] = useState({
    pathName: [],
    pathId: []
  })
  const [foldersList, setFoldersList] = useState([])
  const [isLoading, setIsLoading] = useState(false)

  const {
    data: { showFolder: list } = {},
    refetch: refetchCurrentFolder,
    loading: foldersLoading
  } = useQuery(SHOW_FOLDER, {
    variables: {
      keyword: searchTerm
      // page: page ? parseInt(page, 10) - 1 : 0
    },
    fetchPolicy: 'network-only'
  })

  const {
    data: { quickAccessSurveys } = {},
    loading: quickAccessLoading
  } = useQuery(GET_QUICK_ACCESS, {
    fetchPolicy: 'network-only'
  })

  const refetch = async (resetPage = false) => {
    setIsLoading(true)

    history.push(
      `surveys?${page ? `page=${resetPage ? 1 : page}` : ''}${
        folderId ? `${page ? '&&' : ''}folderId=${folderId}` : ''
      }`
    )

    const { data: { showFolder: list } = {} } = await refetchCurrentFolder({
      parentId: folderId ? folderId : null,
      keyword: searchTerm,
      state: surveyState.toLocaleLowerCase()
      // page: resetPage ? 0 : page ? parseInt(page, 10) - 1 : 0
    })

    setFoldersList(list)
    setIsLoading(false)
  }

  useEffect(() => {
    if(searchTerm) {
      history.push('surveys')
    }
  }, [searchTerm])

  useEffect(() => {
    refetch()
  }, [folderId, surveyState])

  useEffect(() => {
    if (!foldersLoading) {
      setFoldersList(list)
    }
  }, [foldersLoading, list])
  
  useEffect(() => {
    const token = PubSub.subscribe(PUBSUB.REFETCH_FOLDERS, refetch)

    return () => {
      PubSub.unsubscribe(token)
    }
  })

  useEffect(() => {
    const token = PubSub.subscribe(PUBSUB.RESET_CURRENT_FOLDER, () => {
      setCurrentFolder({
        pathName: [],
        pathId: []
      })
    })

    return () => {
      PubSub.unsubscribe(token)
    }
  })

  const { loading: loadingOrg, organizations } = useAllOrganizations()
  const resetCreationForm = useMutation(RESET_CREATION_FORM)
  const createFolder = useMutation(CREATE_FOLDER)
  const updateFolder = useMutation(UPDATE_FOLDER)
  const deleteFolder = useMutation(DELETE_FOLDER)
  const moveToFolder = useMutation(MOVE_TO_FOLDER)

  const handleCreateNew = () => {
    resetCreationForm()
    const folderQuerry =
      folderId && folderId !== 'null' && folderId !== 'undefined'
        ? `?folderId=${folderId}`
        : ''
    history.push(`/operator/survey/create${folderQuerry}`)
  }

  const handleCreateFolder = async name => {
    setIsLoading(true)
    await createFolder({ variables: { name: name, parent: folderId } })
    refetch()
  }

  const handleUpdateFolder = async (name, id) => {
    setIsLoading(true)
    await updateFolder({
      variables: { name: name, id: id, parent: folderId }
    })
    refetch()
  }

  const handleDeleteFolder = async id => {
    if (!id) {
      return
    }
    setIsLoading(true)
    await deleteFolder({ variables: { id: id } })
    refetch()
  }

  const handleChangeFolders = nextPath => {
    setIsLoading(true)
    setCurrentFolder({ ...nextPath })

    const nextFolderId = nextPath.pathId[nextPath.pathId.length - 1]

    if(nextFolderId) {
      setSearchTerm('')
      history.push(`surveys?folderId=${nextFolderId}`)
    }
  }

  const handleMoveToFolder = async (id, folderId, surveyId) => {
    setIsLoading(true)
    await moveToFolder({
      variables: { id: id, folderId: folderId, surveyId: surveyId }
    })
    refetch()
    setIsLoading(false)
  }

  // const changePage = pageNumber => {
  //   history.push(
  //     `${match.url}?page=${pageNumber + 1 || 1}${
  //       folderId ? '&&folderId=' + folderId : ''
  //     }`
  //   )
  // }

  return (
    <OperatorPage>
      <OperatorPageContent>
        <OperatorSurveysComponent
          loading={
            foldersLoading || isLoading || quickAccessLoading || loadingOrg
          }
          organizations={organizations}
          searchTerm={searchTerm}
          onSearch={setSearchTerm}
          surveyState={surveyState}
          setSurveyState={setSurveyState}
          // page={Number(page) - 1}
          // onChangePage={changePage}
          createNew={handleCreateNew}
          createFolder={handleCreateFolder}
          updateFolder={handleUpdateFolder}
          deleteFolder={handleDeleteFolder}
          changeFolder={handleChangeFolders}
          surveysList={foldersList}
          folderId={folderId}
          currentFolder={currentFolder}
          moveSurveyOrFolderToFolder={handleMoveToFolder}
          quickAccessSurveys={quickAccessSurveys}
          // total={total}
        />
      </OperatorPageContent>
    </OperatorPage>
  )
}

const RESET_CREATION_FORM = gql`
  mutation resetCreationForm {
    resetCreationForm @client
  }
`

const CREATE_FOLDER = gql`
  mutation createFolder($name: String!, $parent: ID) {
    createFolder(name: $name, parent: $parent) {
      ...folderFragment
    }
  }

  ${folderFragment}
`

const UPDATE_FOLDER = gql`
  mutation updateFolder($name: String!, $parent: ID, $id: ID!) {
    updateFolder(name: $name, parent: $parent, id: $id) {
      ...folderFragment
    }
  }

  ${folderFragment}
`

const DELETE_FOLDER = gql`
  mutation destroyFolder($id: ID!) {
    destroyFolder(id: $id) {
      id
    }
  }
`

const SHOW_FOLDER = gql`
  query showFolder($parentId: ID, $state: String, $keyword: String) {
    showFolder(parentId: $parentId, state: $state, keyword: $keyword)
  }
`

const GET_QUICK_ACCESS = gql`
  query quickAccessSurveys {
    quickAccessSurveys {
      name
      id
      uniqueName
      state
      title
      coverPhoto
      isScreenerOnly
      compulsorySurvey
      products {
        id
      }
    }
  }
`

const MOVE_TO_FOLDER = gql`
  mutation moveSurveyToFolder($id: ID!, $folderId: ID, $surveyId: ID) {
    moveSurveyToFolder(id: $id, folderId: $folderId, surveyId: $surveyId) {
      id
      name
    }
  }
`

export default withRouter(OperatorSurveys)
