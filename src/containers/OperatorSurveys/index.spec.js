import React from 'react'
import { mount } from 'enzyme'
import OperatorSurveys from '.'
import { Router } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import OperatorSurveysComponent from '../../components/OperatorSurveys'
import sinon from 'sinon'
import gql from 'graphql-tag'
import { defaultSurvey } from '../../mocks'
import { GraphQLNonNull } from 'graphql'

const RESET_CREATION_FORM = gql`
  mutation resetCreationForm {
    resetCreationForm @client
  }
`

const SHOW_FOLDER = gql`
  query showFolder($parentId: ID, $state: String, $keyword: String) {
    showFolder(parentId: $parentId, state: $state, keyword: $keyword)
  }
`

const createSurveyCreationMock = secondProduct => ({
  ...defaultSurvey,
  products: [
    {
      name: 'the first product',
      brand: 'the brand'
    },
    secondProduct
  ]
})

describe('OperatorSurveys', () => {
  let testRender
  let history
  let client
  let spyChange

  beforeEach(() => {
    history = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }

    const mockShowFolder = {
      request: {
        query: SHOW_FOLDER,
        variables: { parentId: null, state: '', keyword: '' }
      },
      result: () => {
        return [{ id: 'id', name: 'name' }]
      }
    }

    client = createApolloMockClient({
      mocks: [mockShowFolder]
    })

    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: createSurveyCreationMock({
          name: '',
          brand: 'the brand'
        })
      }
    })

    const stub = sinon.stub(client, 'mutate')
    spyChange = jest.fn()
    stub
      .withArgs(sinon.match({ mutation: RESET_CREATION_FORM }))
      .callsFake(spyChange)
    stub.callThrough()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render OperatorSurveys', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <OperatorSurveys
            history={history}
            match={{ params: { surveyId: 'survey-1' } }}
            location={{ search: '' }}
          />
        </Router>
      </ApolloProvider>
    )
    expect(testRender).toMatchSnapshot()
  })

  test('should render OperatorSurveys onpage change event', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <OperatorSurveys
            history={history}
            match={{ params: { surveyId: 'survey-1' } }}
            location={{ search: '' }}
          />
        </Router>
      </ApolloProvider>
    )

    spyChange.mockReset()
    testRender.find(OperatorSurveysComponent).prop('createNew')()
    expect(spyChange).toHaveBeenCalled()

    // spyChange.mockReset()
    // testRender.find(OperatorSurveysComponent).prop('onChangePage')()
  })
})
