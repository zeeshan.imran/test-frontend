import React, { useState } from 'react'
import AuthenticatedLayout from '../../components/AuthenticatedLayout'
import history from '../../history'
import { logout, getAuthenticatedUser } from '../../utils/userAuthentication'
import SelectLanguage from '../../components/SelectLanguage'
import i18n from '../../utils/internationalization/i18n'

const getEntryName = ({ title }) => title
const handleNavigate = entry =>
  entry.action ? entry.action() : history.push(entry.route)

const AuthenticatedLayoutContainer = props => {
  const user = getAuthenticatedUser()
  const [showHamburger, setHamburger] = useState(false)
  const [showModal, setShowModal] = useState(false)
  const setShowHamburger = () => {
    setHamburger(!showHamburger)
  }
  
  const NAVIGATION_ENTRIES = [
    {
      title: i18n.t('containers.authenticatedLayut.home'),
      route: '/'
    },
    {
      title: i18n.t('containers.authenticatedLayut.signOut'),
      action: logout
    }
  ]
  return (
    <React.Fragment>
      {showModal && (
        <SelectLanguage showModal={showModal} setShowModal={setShowModal} />
      )}
      <AuthenticatedLayout
        user={user}
        getEntryName={getEntryName}
        handleNavigate={handleNavigate}
        navigationEntries={NAVIGATION_ENTRIES}
        showHamburger={showHamburger}
        setShowHamburger={setShowHamburger}
        {...props}
      />
    </React.Fragment>
  )
}

export default AuthenticatedLayoutContainer
