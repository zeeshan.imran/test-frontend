import React, { useEffect, useRef } from 'react'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import SurveyAnalysisReportComponent from '../../components/SurveyAnalysisReport'

const SurveyAnalysisReport = ({ jobGroup }) => {
  const { data, refetch: refetchCurrent } = useQuery(
    GET_SURVEY_ANALYSIS_RESULT,
    {
      variables: { jobGroup },
      fetchPolicy: 'cache-and-network'
    }
  )
  const refetch = useRef(refetchCurrent)

  useEffect(() => {
    let timer = null
    const refreshRunningJobs = data => {
      const jobResults = (data && data.jobResults) || []
      const hasRunningJob = jobResults.find(job => job.status === 'RUNNING')
      if (hasRunningJob) {
        timer = setTimeout(async () => {
          const { data } = await refetch.current()
          refreshRunningJobs(data)
        }, 1000)
      }
    }

    refreshRunningJobs(data)
    return () => {
      clearTimeout(timer)
    }
  }, [data])

  return (
    <SurveyAnalysisReportComponent
      jobResults={(data && data.jobResults) || []}
    />
  )
}

const GET_SURVEY_ANALYSIS_RESULT = gql`
  query findSurveyAnalysisResults($jobGroup: ID!) {
    jobResults: findSurveyAnalysisResults(jobGroup: $jobGroup) {
      id
      name
      status
      message
      description
      files {
        type
        name
      }
    }
  }
`

export default SurveyAnalysisReport
