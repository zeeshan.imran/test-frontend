import React from 'react'
import { mount } from 'enzyme'
import CloneSurveyButton from '.'
import gql from 'graphql-tag'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import AlertModal from '../../components/AlertModal'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import Text from '../../components/Text'
import sinon from 'sinon'

jest.mock('../../components/AlertModal')

describe('CloneSurveyButton', () => {
  let testRender
  let client
  let spyChange
  let history

  beforeEach(() => {
    client = createApolloMockClient()
    const CLONE_SURVEY = gql`
      mutation cloneSurvey($id: ID!) {
        cloneSurvey(id: $id)
      }
    `

    client.cache.writeQuery({
      query: CLONE_SURVEY,
      variables: { id: 'survey-1' },
      data: {
        survey: {
          __typename: 'SurveyCreation',
          surveyId: 'survey-1',
          surveyEnrollmentId: null,
          state: 'draft'
        }
      }
    })

    const stub = sinon.stub(client, 'mutate')
    spyChange = jest.fn()
    stub.withArgs(sinon.match({ mutation: CLONE_SURVEY })).callsFake(spyChange)
    stub.callThrough()

    history = createBrowserHistory()
  })

  afterEach(() => {
    testRender.unmount()
  })
  test('should render CloneSurveyButton', () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <CloneSurveyButton surveyId={'survey-1'} state={'draft'} onMenuItemClick={jest.fn()}/>
        </Router>
      </ApolloProvider>
    )
    expect(testRender).toMatchSnapshot()
  })

  test('should render CloneSurveyButton', () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <CloneSurveyButton surveyId={'survey-1'} state={'draft'} onMenuItemClick={jest.fn()}/>
        </Router>
      </ApolloProvider>
    )

    testRender
      .find(Text)
      .first()
      .prop('onClick')()

    const onClickFn = testRender.find(Text).prop('onClick')

    spyChange.mockReset()
    AlertModal.mockImplementationOnce(({ handleOk }) => handleOk())
    onClickFn()
    expect(spyChange).toHaveBeenCalled()

    const handleCancel = testRender.find(Text).prop('onClick')

    spyChange.mockReset()
    AlertModal.mockImplementationOnce(({ handleCancel }) => handleCancel())
    handleCancel() 
  })
})
