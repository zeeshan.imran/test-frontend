import React from 'react'
import { withRouter } from 'react-router-dom'
import { useQuery } from 'react-apollo-hooks'
import Button from '../../components/Button'
import LoadingModal from '../../components/LoadingModal'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import useToggle from '../../hooks/useToggle/index'
import useSurveyValidate from '../../hooks/useSurveyValidate'
import usePublishSurvey from '../../hooks/usePublishSurvey'
import useConfirmQuestionReorder from '../../hooks/useConfirmQuestionReorder'
import { useTranslation } from 'react-i18next'
import settingsSchema from '../../validates/settings'
import { emailsValidationSchema } from '../../components/OperatorSurveyCreateEmailSettings'
import TooltipWrapper from '../../components/TooltipWrapper'
import getQueryParams from '../../utils/getQueryParams'
 
const PublishSurvey = ({ history, location }) => {
  const { t } = useTranslation()
  const confirmQuestionReorder = useConfirmQuestionReorder()
  const {
    data: {
      surveyCreation: { products, basics, questions, mandatoryQuestions }
    }
  } = useQuery(SURVEY_CREATION)
  const [publishing, togglePublishing] = useToggle(false)

  const { folderId } = getQueryParams(location)
  const folderQuerry =
    folderId && folderId !== 'null' && folderId !== 'undefined'
      ? `folderId=${folderId}`
      : ``

  const publishSurvey = usePublishSurvey({
    surveyInitialState: 'active',
    successMessage: t('containers.publishSurvey.successMessage'),
    errorMessage: t('containers.publishSurvey.errorMessage'),
    folderId:
      folderId && folderId !== 'null' && folderId !== 'undefined'
        ? folderId
        : undefined
  })
  const surveyValidate = useSurveyValidate()

  let submitDisabled = true
  if (basics.isScreenerOnly) {
    submitDisabled =
      !basics.uniqueName ||
      questions.length + mandatoryQuestions.length === 0 ||
      basics.linkedSurveys.length === 0
  } else if (basics.compulsorySurvey) {
    submitDisabled = !basics.uniqueName || questions.length === 0
  } else {
    submitDisabled =
      !basics.uniqueName ||
      products.length === 0 ||
      questions.length + mandatoryQuestions.length === 0 ||
      !settingsSchema.isValidSync(basics.autoAdvanceSettings) ||
      products.reduce((result, product) => result && !product.isAvailable, true)
  }
  
  const onPublishSurvey = async () => {
    try {
      emailsValidationSchema.validateSync(basics)
    } catch (ex) {
      history.push(`./emails?${folderQuerry}`)
      return
    }

    const isAdvanceSettingsValid = await surveyValidate.validateAdvanceSettings()
    if (!isAdvanceSettingsValid) {
      history.push(`./settings`)
      await surveyValidate.showAdvanceSettingsErrors()
      return false
    }

    const isProductsValid = await surveyValidate.validateProducts()
    if (!isProductsValid) {
      history.push(`./products?${folderQuerry}`)
      await surveyValidate.showProductsErrors()
      return
    }

    const {
      isValid: isQuestionsValid,
      invalidQuestion,
      noQuestionError
    } = await surveyValidate.validateQuestions(true)

    if (noQuestionError) {
      await surveyValidate.showOnlyMandatoryQuestionErrors()
      return
    }

    if (!isQuestionsValid && invalidQuestion.displayOn === 'payments') {
      history.push(
        `./financial?section=${invalidQuestion.displayOn}&&${folderQuerry}`
      )
      await surveyValidate.showQuestionsErrors()
      return
    }

    if (!isQuestionsValid) {
      history.push(
        `./questions?section=${invalidQuestion.displayOn}&&${folderQuerry}`
      )
      await surveyValidate.showQuestionsErrors()
      return
    }

    const duplicateTitles = await surveyValidate.validateChartTitles()
    
    if(duplicateTitles.length > 0) {
      await surveyValidate.showChartTitlesErrors(duplicateTitles)
      return
    }

    const onConfirm = async () => {
      togglePublishing()

      try {
        await publishSurvey()
        togglePublishing()
        history.push(`/operator/surveys?${folderQuerry}`)
      } catch (error) {
        togglePublishing()
        displayErrorPopup(error && error.message)
      }
    }
    
    confirmQuestionReorder(onConfirm)
  }

  return (
    <TooltipWrapper
      helperText={t('tooltips.publishSurvey')}
      placement='leftTop'
    >
      <Button
        data-testid='publish-survey-button'
        type={submitDisabled ? 'disabled' : 'primary'}
        size='default'
        onClick={onPublishSurvey}
      >
        {t('containers.publishSurvey.publishButton')}
      </Button>
      <LoadingModal
        visible={publishing}
        text={t('containers.publishSurvey.loadingText')}
      />
    </TooltipWrapper>
  )
}

export default withRouter(PublishSurvey)
