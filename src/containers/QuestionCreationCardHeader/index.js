import React, { memo, useCallback } from 'react'
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo-hooks'
import { clone } from 'ramda'
import { generate } from 'shortid'
import QuestionCreationCardHeaderComponent from '../../components/QuestionCreationCardHeader'
import AlertModal from '../../components/AlertModal'
import addQuestion from '../../utils/addQuestion'
import { useTranslation } from 'react-i18next'

import PubSub from 'pubsub-js'
import { PUBSUB } from '../../utils/Constants'
import { message } from 'antd'

const QuestionCreationCardHeader = ({
  questionIndex,
  questionIndexInSection,
  question,
  mandatory,
  allToMandatory,
  unsetMandatory,
  displaySurveyNameOnAll,
  unsetDisplaySurveyNameOnAll,
  unsetPicture,
  allToPicture,
  hasDemographic,
  ...otherProps
}) => {
  const { t } = useTranslation()
  const removeQuestionMutation = useMutation(REMOVE_SURVEY_CREATION_QUESTION)
  const copyQuestionMutation = useMutation(COPY_SURVEY_CREATION_QUESTIONS)
  const updateQuestionRequire = useMutation(UPDATE_QUESTION_REQUIRE)

  const removeQuestion = useCallback(async () => {
    await removeQuestionMutation({ variables: { questionIndex } })
    PubSub.publish(PUBSUB.VALIDATE_SURVEY_QUESTIONS)
  }, [questionIndex, removeQuestionMutation])

  const onRemoveQuestion = () => {
    AlertModal({
      title: t('containers.questionCreationCardHeader.title'),
      okText: t('containers.questionCreationCardHeader.okText'),
      description: t('containers.questionCreationCardHeader.description'),
      handleOk: removeQuestion,
      handleCancel: () => {}
    })
  }

  const {
    data: {
      surveyCreation: {
        basics: { compulsorySurvey, showUserProfileDemographics },
        questions
      }
    }
  } = useQuery(SURVEY_CREATION)

  const copyQuestion = () => {
    try {
      const newQuestion = {
        ...clone(question),
        clientGeneratedId: generate(),
        id: null,
        order: null,
        editMode: null
      }

      copyQuestionMutation({
        variables: {
          questions: addQuestion(questions, newQuestion)
        }
      })
      message.success(t('components.questionCreation.questionDuplicateSuccess'))
    } catch (error) {
      message.error(t('components.questionCreation.questionDuplicateError'))
    }
  }

  const isNotPaypall = !question.type || question.type !== 'paypal-email'

  return (
    <QuestionCreationCardHeaderComponent
      {...otherProps}
      isNotPaypall={isNotPaypall}
      questionIndexInSection={questionIndexInSection}
      question={question}
      questionIndex={questionIndex}
      mandatory={mandatory}
      hasDemographic={hasDemographic}
      showUserProfileDemographics={
        showUserProfileDemographics ? showUserProfileDemographics : false
      }
      onDemographicChange={value => {
        updateQuestionRequire({
          variables: {
            questionIndex,
            questionField: { hasDemographic: value }
          }
        })
      }}
      onMandatoryChange={() => {
        updateQuestionRequire({
          variables: {
            questionIndex,
            questionField: { required: !question.required }
          }
        })
        if (allToMandatory) {
          unsetMandatory(false)
        }
      }}
      onDisplaySurveyNameChange={() => {
        updateQuestionRequire({
          variables: {
            questionIndex,
            questionField: { displaySurveyName: !question.displaySurveyName }
          }
        })
        if (displaySurveyNameOnAll) {
          unsetDisplaySurveyNameOnAll(false)
        }
      }}
      onProductChange={value => {
        updateQuestionRequire({
          variables: {
            questionIndex,
            questionField: { showProductImage: value }
          }
        })
        if (allToPicture) {
          unsetPicture(false)
        }
      }}
      onScoreValueChange={value => {
        updateQuestionRequire({
          variables: {
            questionIndex,
            questionField: { considerValueForScore: value }
          }
        })
      }}
      handleCopy={copyQuestion}
      handleRemove={onRemoveQuestion}
      isCompulsorySurvey={compulsorySurvey}
    />
  )
}

const REMOVE_SURVEY_CREATION_QUESTION = gql`
  mutation removeSurveyCreationQuestion($questionIndex: Number) {
    removeSurveyCreationQuestion(questionIndex: $questionIndex) @client
  }
`

const COPY_SURVEY_CREATION_QUESTIONS = gql`
  mutation updateSurveyCreationQuestions($questions: questions) {
    updateSurveyCreationQuestions(questions: $questions) @client
  }
`

const UPDATE_QUESTION_REQUIRE = gql`
  mutation($questionIndex: Number, $questionField: values) {
    updateSurveyCreationQuestion(
      questionIndex: $questionIndex
      questionField: $questionField
    ) @client
  }
`

const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      basics {
        compulsorySurvey
        showUserProfileDemographics
      }
      questions
    }
  }
`

export default memo(QuestionCreationCardHeader)
