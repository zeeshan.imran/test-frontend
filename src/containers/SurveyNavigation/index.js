import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import SurveyNavigationComponent from '../../components/SurveyNavigation'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import { surveyInfo } from '../../fragments/survey'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import {
  destroyDraftWarningPopup,
  displayDraftWarningPopup
} from '../../utils/displayDraftWarningPopup'
import { withTranslation, Trans } from 'react-i18next'
import useAdvanceStep from '../../hooks/useAdvanceStep'
import useSkipStep from '../../hooks/useSkipStep'

const SurveyNavigation = ({
  match,
  withSkipButton,
  onSubmit,
  onSubmitSuccess = () => {},
  onSubmitError = () => {},
  showSubmit,
  type,
  t
}) => {
  const { stepId } = match.params
  const { advanceStep } = useAdvanceStep(stepId) //extracting the hook
  const skipLoop = useSkipStep()
  const {
    data: {
      currentSurveyParticipation: {
        surveyId,
        currentSurveyStep,
        isCurrentAnswerValid,
        canSkipCurrentQuestion,
        selectedProducts
      } = {}
    }
  } = useQuery(SURVEY_PARTICIPATION_QUERY) // Query to get current participation from Cache
  const {
    data: {
      survey: { products = [], minimumProducts, customButtons, state } = {}
    } = {}
  } = useQuery(SURVEY_QUERY, { variables: { id: surveyId } })

  useEffect(() => {
    if (state === 'draft') {
      displayDraftWarningPopup()
    }

    return destroyDraftWarningPopup()
  }, [state])

  let nextButtonLabel = t('defaultValues.default.customButtons.next')
  if (customButtons && customButtons.next) {
    nextButtonLabel = customButtons.next
  }
  let skipButtonLabel = t('defaultValues.default.customButtons.skip')
  if (customButtons && customButtons.skip) {
    skipButtonLabel = customButtons.skip
  }
  const [submitting, setSubmitting] = useState(false)

  let isButtonDisabled =
    !isCurrentAnswerValid &&
    !canSkipCurrentQuestion &&
    !submitting &&
    type !== 'show-product-screen'

  const handleAdvanceStep = async currentStep => {
    setSubmitting(true)
    try {
      isButtonDisabled = false
      onSubmit()
      await advanceStep(currentStep)
      onSubmitSuccess()
    } catch (e) {
      setSubmitting(false)
      displayErrorPopup(t('errors.submitAnswerFailed'))
      onSubmitError()
    }
  }

  return (
    <SurveyNavigationComponent
      okText={nextButtonLabel}
      onOk={() => handleAdvanceStep(currentSurveyStep)}
      okDisabled={isButtonDisabled}
      showSubmit={showSubmit}
      showSkip={selectedProducts.length >= minimumProducts}
      skipProgressText={
        <Trans
          i18nKey='components.surveyNavigation.skipProgressText'
          values={{
            products: selectedProducts.length,
            minimumProducts
          }}
        />
      }
      onSkip={products.length > 1 && withSkipButton ? skipLoop : null}
      skipText={skipButtonLabel}
      showProductProgressText={
        <Trans
          i18nKey='components.surveyNavigation.showProductProgressText'
          values={{
            products: products.length,
            minimumProducts
          }}
        />
      }
      showProgressText={type === 'show-product-screen'}
    />
  )
}

const SURVEY_QUERY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      ...surveyInfo
    }
  }

  ${surveyInfo}
`

export default withRouter(withTranslation()(SurveyNavigation))
