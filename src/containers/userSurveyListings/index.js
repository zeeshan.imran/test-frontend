import React, { useState } from 'react'
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo-hooks'
import { useTranslation } from 'react-i18next'
import UserSurveyListingsComponent from '../../components/UserSurveyListings'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'

const UserSurveyListings = ({
  userId,
  surveyListingsModal,
  closeUserSurveysModal
}) => {
  const { t } = useTranslation()
  const [numberOfDays, setNumberOfDays] = useState(null)
  const [expiryLoader, setExpiryLoader] = useState(false)

  const updateSurveyEnrollmentExpiry = useMutation(
    UPDATE_USER_SURVEY_ENROLLMENTS_EXPIRY
  )

  const {
    data: { userSurveyEnrollments },
    refetch: refetchUpdateEnrollments,
    loading: userSurveyLoadging
  } = useQuery(USER_SURVEY_ENROLLMENTS, {
    variables: {
      userId: userId
    },
    fetchPolicy: 'network-only'
  })

  const updateNumberOfDays = days => {
    setNumberOfDays(days)
  }

  const handleUpdateExpiry = async (enrollmentId, days) => {
    try {
      setExpiryLoader(true)
      await updateSurveyEnrollmentExpiry({
        variables: {
          input: { enrollmentId, days }
        }
      })
      displaySuccessMessage(
        t('components.userSurveyListings.updateSuccessfully')
      )
      await refetchUpdateEnrollments()
      setNumberOfDays(null)
      setExpiryLoader(false)
    } catch (error) {
      setExpiryLoader(false)
    }
  }

  return (
    <UserSurveyListingsComponent
      t={t}
      modalVisibilty={surveyListingsModal}
      handleCancel={closeUserSurveysModal}
      loading={userSurveyLoadging}
      userSurveyList={userSurveyEnrollments}
      numberOfDays={numberOfDays}
      updateNumberOfDays={updateNumberOfDays}
      updateEnrollmentExpiry={handleUpdateExpiry}
      expiryRequestLoader={expiryLoader}
    />
  )
}

const USER_SURVEY_ENROLLMENTS = gql`
  query userSurveyEnrollments($userId: ID!) {
    userSurveyEnrollments(userId: $userId) {
      id
      state
      survey {
        name
      }
      allowedDaysToFillTheTasting
      createdAt
      expiredAt
    }
  }
`

const UPDATE_USER_SURVEY_ENROLLMENTS_EXPIRY = gql`
  mutation updateUserEnrollementExpiry($input: UpdateEnrollmentExpiry) {
    updateUserEnrollementExpiry(input: $input) {
      id
    }
  }
`

export default UserSurveyListings
