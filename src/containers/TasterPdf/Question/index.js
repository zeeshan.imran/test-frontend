import React from 'react'
import DefaultQuestion from './DefaultQuestion'
import PairedQuestion from './PairedQuestion'
import ChooseMultiple from './ChooseMultiple'
import SliderQuestion from './SliderQuestion'

const Question = ({
  mainCharts,
  answer,
  question,
  product,
  typeOf,
  value,
  singleChart,
  chartsSettings,
  t
}) => {
  const updatedSettings = {
    ...chartsSettings,
    ...{ isCountProLabelShown: true },
    ...{ isDataTableShown: false },
    ...{isStackedValuesShown: true},
    ...{isMeanProLabelsShown: true}
  }
  return (
    <React.Fragment>
      {(() => {
        switch (typeOf) {
          case 'paired-questions':
            return (
              <PairedQuestion
                question={question}
                product={product}
                value={value}
                mainCharts={mainCharts}
                singleChart={singleChart}
                chartsSettings={updatedSettings}
                t={t}
              />
            )
          case 'choose-multiple':
            return (
              <ChooseMultiple
                question={question}
                product={product}
                value={value}
                mainCharts={mainCharts}
                singleChart={singleChart}
                chartsSettings={updatedSettings}
                answer={answer}
                t={t}
              />
            )
          case 'slider':
            return (
              <SliderQuestion
                question={question}
                product={product}
                value={value}
                mainCharts={mainCharts}
                singleChart={singleChart}
                chartsSettings={updatedSettings}
                t={t}
              />
            )
          default:
            return (
              <DefaultQuestion
                product={product}
                answer={answer}
                question={question}
                mainCharts={mainCharts}
                chartsSettings={updatedSettings}
                t={t}
              />
            )
        }
      })()}
    </React.Fragment>
  )
}

export default Question
