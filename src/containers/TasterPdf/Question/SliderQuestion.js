import React from 'react'
import {
  TextColumn,
  GraphColumn,
  MainRow,
  AnswerStyle,
  QuestionStyle,
  HeadingStyle
} from './styles.js'
import ChartsPdf from '../../../components/Charts/ChartsPdf'

const SliderQuestion = ({
  mainCharts,
  index,
  question,
  product,
  value,
  chartsSettings,
  t
}) => {
  const sliderList = items => {
    const listItem = items.map((item, index) => {
      return <li key={index}>{`${item.label} : ${item.value}`}</li>
    })
    return <ul>{listItem}</ul>
  }
  return (
    <MainRow className='main_chart-view'>
      <TextColumn span={product ? 8 : 12}>
        <HeadingStyle>Question:</HeadingStyle>
        <QuestionStyle>{question}</QuestionStyle>
      </TextColumn>
      {
        (product) && (
          <TextColumn span={8}>
            <HeadingStyle>Product:</HeadingStyle>
            <QuestionStyle>{product}</QuestionStyle>
          </TextColumn>
        ) 
      }
      <TextColumn span={product ? 8 : 12}>
        <HeadingStyle>Answer:</HeadingStyle>
        <AnswerStyle>{value.length && sliderList(value)}</AnswerStyle>
      </TextColumn>
      <GraphColumn span={24}>
        {mainCharts && (
          <ChartsPdf
            charts={mainCharts}
            hideFilter
            idx={index}
            chartsSettings={chartsSettings}
            operator={false}
            t={t}
          />
        )}
      </GraphColumn>
    </MainRow>
  )
}
export default SliderQuestion
