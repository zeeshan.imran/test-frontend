import React, { useState, useEffect, useCallback } from 'react'
import { useQuery, useMutation, useApolloClient } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { withRouter } from 'react-router-dom'
import { Desktop } from '../../components/Responsive'
import FillerLoader from '../../components/FillerLoader'
import SurveyCompletedComponent from '../../components/SurveyCompleted'
import SurveyLinkedSurveyComponent from '../../components/SurveyLinkedSurveyComponent'
import AuthenticatedLayout from '../../containers/AuthenticatedLayout'
import PaypalEmailModal from '../../components/PaypalEmailModal'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import { getAuthenticationToken } from '../../utils/surveyAuthentication'
import useToggle from '../../hooks/useToggle'
import { withTranslation } from 'react-i18next'
import { Container } from './styles'
import TermsAndPrivacyFooter from '../TermsAndPrivacyFooter'

import { getAuthenticatedUser } from '../../utils/userAuthentication'
import { surveyInfo } from '../../fragments/survey'
import { getShareLink } from '../../utils/shareUtils'

const SurveyCompleted = ({ surveyId, i18n, history, t }) => {
  const {
    data: {
      currentSurveyParticipation: {
        paypalEmail,
        selectedProducts,
        answers,
        surveyEnrollmentId,
        savedRewards,
        currentSurveyStep
      }
    }
  } = useQuery(SURVEY_PARTICIPATION_QUERY)

  const getDownloadLink = useMutation(GET_DOWNLOAD_LINK)

  const handleSubmitAnswer = useMutation(SUBMIT_ANSWER)
  const saveRewards = useMutation(SAVE_REWARDS)

  const hasReward =
    selectedProducts.reduce((acc, curr) => acc + curr.reward, 0) > 0

  //update rewards with answered: true
  const newRewards = savedRewards.map(reward => ({
    ...reward,
    answered: selectedProducts.includes(reward.id)
  }))

  saveRewards({
    variables: {
      rewards: newRewards,
      surveyEnrollment: surveyEnrollmentId
    }
  })

  const shouldShowPaypaylModal = !paypalEmail && hasReward

  const { data, loading, error } = useQuery(SURVEY_QUERY, {
    fetchPolicy: 'network-only',
    variables: { id: surveyId }
  })

  if (data.survey) {
    let surveyLanguage = data.survey.surveyLanguage || 'en'
    if (i18n.language !== surveyLanguage) {
      i18n.changeLanguage(surveyLanguage)
    }
  }

  const submitPaypalEmail = useMutation(SET_PAYPAL_EMAIL)
  const saveCurrentSurveyParticipation = useMutation(UPDATE_CURRENT_USER_PAYPAL)
  const finishSurvey = useMutation(FINISH_SURVEY)

  const [paypalModalVisible, toggleModal] = useToggle(shouldShowPaypaylModal)
  const [submissionError, setSubmissionError] = useState('')
  const [pdfLoader, setPdfLoader] = useState(false)
  const [socioEconomicRating, setSocioEconomicRating] = useState({})

  useEffect(() => {
    async function fetchData () {
      const result = await finishSurvey({
        variables: { input: { surveyEnrollment: surveyEnrollmentId } }
      })
      if (
        result &&
        result.data &&
        result.data.finishSurvey &&
        result.data.finishSurvey.socioEconomicRating &&
        result.data.finishSurvey.socioEconomicRating.grade &&
        result.data.finishSurvey.socioEconomicRating.score
      ) {
        setSocioEconomicRating(result.data.finishSurvey.socioEconomicRating)
      }
    }
    if (
      surveyEnrollmentId &&
      currentSurveyStep &&
      currentSurveyStep === 'finished'
    ) {
      fetchData()
    }
    answers.forEach(async answer => {
      try {
        if (answer && answer.context && answer.context.type === 'slider') {
          const questionId = answer.id && answer.id.split('-')[0]
          await handleSubmitAnswer({
            variables: {
              input: {
                question: questionId,
                value: answer.values,
                context: { ...answer.context, save: true },
                startedAt: answer.startedAt,
                surveyEnrollment: surveyEnrollmentId,
                selectedProduct:
                  answer.context && answer.context.selectedProduct
              }
            }
          })
        }
      } catch (error) {
        //
      }
    })
  }, [surveyEnrollmentId, answers])
  const apollo = useApolloClient()
  const getPdf = useCallback(async () => {
    try {
      const enrollmentId = getAuthenticationToken()
      setPdfLoader(true)
      const { data } = await apollo.query({
        query: QUERY_STATS,
        variables: {
          surveyId,
          surveyEnrollment: enrollmentId,
          additionalText: t('surveySettings.pdfFooterSettings.countRespondents')
        },
        fetchPolicy: 'network-only'
      })

      const { jobGroupId } = data.survey.stats
      const {
        data: { downloadFile }
      } = await getDownloadLink({
        variables: {
          surveyId,
          jobGroupId
        }
      })

      const url = `${process.env.REACT_APP_BACKEND_API_URL}/pdf/${downloadFile}`
      window.open(url, '_self')

      setPdfLoader(false)
    } catch (error) {
      setPdfLoader(false)
    }
  }, [apollo, surveyId, getDownloadLink])
  const handleSubmit = async paypalEmail => {
    const {
      data: {
        setPaypalEmail: { paypalEmail: updatedPaypalEmail }
      }
    } = await submitPaypalEmail({
      variables: { surveyEnrollment: getAuthenticationToken(), paypalEmail }
    })

    if (paypalEmail !== updatedPaypalEmail) {
      setSubmissionError(t('errors.updatePaypallFailed'))
    }

    saveCurrentSurveyParticipation({ variables: { paypalEmail } })
    toggleModal()
  }

  if (loading) return <FillerLoader fullScreen loading />

  if (error) return `Error! ${error.message}`

  const { survey } = data

  const loggedInUser = getAuthenticatedUser()

  let shareSurvey = survey
  if (survey.screeners && survey.screeners.length > 0) {
    shareSurvey = survey.screeners[0]
  }

  const { uniqueName } = shareSurvey

  const {
    thankYouText,
    isScreenerOnly,
    linkedSurveys,
    showSurveyScore,
    compulsorySurvey
  } = survey
  const { REACT_APP_THEME } = process.env
  const sharingButtonStatus = REACT_APP_THEME !== 'chrHansen' ? true : false



  return (
    <Desktop>
      {desktop => (
        <AuthenticatedLayout mergeNavbarToContent>
          <Container desktop={desktop}>
            <PaypalEmailModal
              errors={submissionError}
              onClick={handleSubmit}
              visible={paypalModalVisible}
            />
            {compulsorySurvey ||
            (isScreenerOnly &&
            linkedSurveys &&
            linkedSurveys.length > 0 &&
            loggedInUser &&
            loggedInUser.id) ? (
              <SurveyLinkedSurveyComponent
                sharingButtons={survey.sharingButtons}
                text={thankYouText}
                shareLink={getShareLink(uniqueName)}
                linkedSurveys={linkedSurveys}
                compulsorySurvey={compulsorySurvey}
                history={history}
              />
            ) : (
              <SurveyCompletedComponent
                sharingButtons={survey.sharingButtons}
                text={thankYouText}
                shareLink={getShareLink(uniqueName)}
                sharingButtonStatus={sharingButtonStatus}
                surveyEnrollment={surveyEnrollmentId}
                getPdf={getPdf}
                showGeneratePdf={survey.showGeneratePdf}
                loading={pdfLoader}
                socioEconomicRating={socioEconomicRating}
                showSurveyScore={showSurveyScore}
              />
            )}

            <TermsAndPrivacyFooter showMessage={true} />
          </Container>
        </AuthenticatedLayout>
      )}
    </Desktop>
  )
}

const SURVEY_QUERY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      ...surveyInfo
      linkedSurveys {
        id
        name
        title
        uniqueName
        authorizationType
      }
      screeners {
        id
        uniqueName
      }
    }
  }
  ${surveyInfo}
`

const SET_PAYPAL_EMAIL = gql`
  mutation setPaypalEmail($surveyEnrollment: ID!, $paypalEmail: String!) {
    setPaypalEmail(
      surveyEnrollment: $surveyEnrollment
      paypalEmail: $paypalEmail
    ) {
      paypalEmail
    }
  }
`

const UPDATE_CURRENT_USER_PAYPAL = gql`
  mutation saveCurrentSurveyParticipation($paypalEmail: String) {
    saveCurrentSurveyParticipation(paypalEmail: $paypalEmail) @client
  }
`

const FINISH_SURVEY = gql`
  mutation finishSurvey($input: EnrollmentStateInput) {
    finishSurvey(input: $input) {
      socioEconomicRating
    }
  }
`

const SUBMIT_ANSWER = gql`
  mutation submitAnswer($input: SubmitAnswerInput!) {
    submitAnswer(input: $input)
  }
`
const QUERY_STATS = gql`
  query survey($surveyId: ID!, $surveyEnrollment: String!) {
    survey(id: $surveyId) {
      id
      stats(enrollmentId: [$surveyEnrollment], questionFilters: [], sync: true)
    }
  }
`

const GET_DOWNLOAD_LINK = gql`
  mutation getDownloadLink($surveyId: ID!, $jobGroupId: ID!) {
    downloadFile: getDownloadLink(
      surveyId: $surveyId
      jobGroupId: $jobGroupId
      templateName: taster
    )
  }
`

export const SAVE_REWARDS = gql`
  mutation saveRewards($rewards: JSON, $surveyEnrollment: ID) {
    saveRewards(rewards: $rewards, surveyEnrollment: $surveyEnrollment)
  }
`

export default withRouter(withTranslation()(SurveyCompleted))
