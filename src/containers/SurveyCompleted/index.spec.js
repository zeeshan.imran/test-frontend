import React from 'react'
import { mount } from 'enzyme'
import SurveyCompleted, { SAVE_REWARDS } from '.'
import { getAuthenticatedUser } from '../../utils/userAuthentication'
import wait from '../../utils/testUtils/waait'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import gql from 'graphql-tag'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import defaults from '../../defaults'
import i18n from '../../utils/internationalization/i18n'
import { SURVEY_QUERY } from '../../queries/Survey'
import { questionInfo } from '../../fragments/survey'
import sinon from 'sinon'

jest.mock('../../utils/userAuthentication')

const QUESTION_ID = 'question-1'
const SURVEY_ID = 'survey-1'

const QUESTION_QUERY = gql`
  query question($id: ID) {
    question(id: $id) {
      ...questionInfo
    }
  }
  ${questionInfo}
`

const mockQuestionQuery = isRequired => ({
  request: {
    query: QUESTION_QUERY,
    variables: { id: QUESTION_ID }
  },
  result: {
    data: {
      question: {
        id: QUESTION_ID,
        required: isRequired
      }
    }
  }
})

const history = createBrowserHistory()

const createMockSurveyQuery = state => ({
  request: {
    query: SURVEY_QUERY,
    variables: {
      surveyId: 'survey-1'
    }
  },
  result: {
    data: {
      survey: {
        id: 'survey-1',
        state: 'completed',
        thankYouText: 'thankYouText',
        surveyLanguage: 'en'
      }
    }
  }
})

describe('SurveyCompleted', () => {
  let testRender
  let client
  let user
  let mockedQuery
  let mockClient

  beforeEach(() => {
    user = getAuthenticatedUser()

    mockClient = createApolloMockClient({
      mocks: [createMockSurveyQuery('completed')]
    })

    mockClient.cache.writeQuery({
      query: SURVEY_PARTICIPATION_QUERY,
      data: {
        currentSurveyParticipation: {
          ...defaults.currentSurveyParticipation,
          surveyId: SURVEY_ID,
          products: ['product-1', 'product-2'],
          selectedProducts: ['product-1'],
          answers: [],
          paypalEmail: 'test@flowor-wiki.com',
          surveyEnrollmentId: 'surveyEnrollmentId',
          productRewardsRule: {
            active: false,
            min: 0,
            max: 0,
            percentage: 0
          }
        }
      }
    })

    const stub = sinon.stub(mockClient, 'mutate')
    stub.withArgs(sinon.match({ mutation: SAVE_REWARDS })).returns({ data: {} })
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('Should set Values for SurveyQuestion component ', async () => {
    getAuthenticatedUser.mockImplementation(() => ({
      isSuperAdmin: true
    }))

    const mockChangeLang = jest.fn()
    i18n.changeLanguage = mockChangeLang
    i18n.language = 'en'

    testRender = mount(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <SurveyCompleted
            desktop={''}
            user={user}
            match={{ params: { surveyId: 'survey-1' } }}
            surveyId={'survey-1'}
          />
        </Router>
      </ApolloProvider>
    )
    await wait(0)
    testRender.update()
    await wait(0)

    expect(testRender).toMatchSnapshot()
  })
})
