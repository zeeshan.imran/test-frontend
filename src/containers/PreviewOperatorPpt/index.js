import React from 'react'
import { useTranslation } from 'react-i18next'
import * as Yup from 'yup'
import useChartResults from '../../hooks/useChartResults'
import { OperatorPdfContainer } from './styles'
import { ChartContextProvider } from '../../contexts/ChartContext'
import { getChartId } from '../../utils/pdfUtils/getChartId'
import { getChartComponent } from '../../utils/chartConfigs'
import DataGuard from '../../components/Charts/DataGuard'
import { getSingleChartSettings } from '../../utils/ChartSettingsUtils'
import OVERRIDER_SETTINGS from '../../utils/pdfUtils/OVERRIDER_SETTINGS'

const mixed = Yup.mixed()

const CARD_WIDTHS = {
  DEFAULT: 900,
  '': 450,
  pie: 450
}

const PreviewOperatorPpt = ({ jobGroupId, surveyId }) => {
  const { loading, stats, settings } = useChartResults({
    jobGroupId,
    surveyId
  })
  const { t } = useTranslation()

  if (loading) {
    return null
  }

  return (
    <ChartContextProvider showAll printView>
      <OperatorPdfContainer>
        {stats.map(stat => {
          const chartId = getChartId(stat)
          const { result: chart } = stat
          const chartSettings = {
            ...getSingleChartSettings(settings, chart),
            ...OVERRIDER_SETTINGS.operator
          }

          return (
            <div id={`chart-view-${chartId}`}>
              <DataGuard
                cardWidth={`${CARD_WIDTHS[chart.chart_type] ||
                  CARD_WIDTHS.DEFAULT}px`}
                cardMargin='0 0'
                validationSchema={mixed}
                chartsSettings={chartSettings}
                chartType={chart.chart_type}
                chartId={chartId}
                inputData={{ ...chart, chart_id: chartId }}
                t={t}
                component={getChartComponent(chart)}
              />
            </div>
          )
        })}
      </OperatorPdfContainer>
    </ChartContextProvider>
  )
}

export default PreviewOperatorPpt
