import React, { useEffect } from 'react'
import { usePdfLayout } from '../../hooks/usePdfLayout'

const Page = ({ layout }) => {
  const { fixLayout } = usePdfLayout()

  useEffect(() => {
    fixLayout(layout, layout, layout, "A4", 2)
  }, [layout, fixLayout])
  return layout.map(item => (
    <div key={item.i} id={`pdf-${item.i}`}>
      {item.content}
    </div>
  ))
}

export default Page
