import React from 'react'
import { mount } from 'enzyme'
import UserSurveyCard from '.'
import UserSurveyCardComponent from '../../components/UserSurveyCard'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import { Router } from 'react-router-dom'
import moment from 'moment'

jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k,
  getFixedT: () => () => ''
}))
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

const mockSurveyCreation = {
  surveyId: 'survey-1',
  products: [
    {
      id: 'product-1',
      name: 'product',
      brand: 'brand'
    }
  ],
  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?'
    }
  ],
  mandatoryQuestions: [],
  uniqueQuestionsToCreate: []
}

describe('UserSurveyCard', () => {
  let testRender
  let survey
  let otherProps
  let mockClient
  let history

  beforeEach(() => {
    survey = {
      inProgress: false,
      numberOfProducts: 2,
      expirationDate: moment('2019-11-02T05:19:14.683Z')
    }

    otherProps = {}
    mockClient = createApolloMockClient()
    mockClient.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: mockSurveyCreation
      }
    })
    history = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render UserSurveyCard', async () => {
    testRender = mount(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <UserSurveyCard
            data={{ survey }}
            otherProps={otherProps}
            history={history}
          />
        </Router>
      </ApolloProvider>
    )
    expect(testRender).toMatchSnapshot()
    //expect(testRender.find(UserSurveyCard)).toHaveLength(1)
  })

  test('should render UserSurveyCard onclick history', async () => {
    testRender = mount(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <UserSurveyCard
            data={{ survey }}
            otherProps={otherProps}
            history={history}
          />
        </Router>
      </ApolloProvider>
    )
    testRender.find(UserSurveyCardComponent).prop('onClick')()
  })
})
