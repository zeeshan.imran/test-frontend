import React, { useState } from 'react'
import { filter } from 'ramda'
import { useQuery } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { withRouter } from 'react-router-dom'
import ImpersonateFormComponent from '../../components/ImpersonateForm'
import {
  getAuthenticatedUser,
  getImpersonateUserId
} from '../../utils/userAuthentication'

const KEEP_ROUTERS = [
  '/operator/dashboard',
  '/operator/sharedWithMe',
  '/operator/surveys',
  '/operator/terms-of-use',
  '/operator/privacy-policy'
]

const getImpersonatePath = (impersonateUserId, pathname = '/') => {
  const shouldKeepUrl = KEEP_ROUTERS.includes(pathname)
  return `/impersonate/${impersonateUserId}?redirect=${
    shouldKeepUrl ? pathname : '/'
  }`
}

const ImpersonateForm = ({ location, history }) => {
  const authUser = getAuthenticatedUser()
  const impersonateUserId = getImpersonateUserId()
  const [keyword, setKeyword] = useState('')
  const { data, loading } = useQuery(QUERY_USERS, {
    fetchPolicy: 'cache-and-network',
    variables: {
      keyword: keyword || ''
    }
  })

  const { users = [] } = data

  if (impersonateUserId) {
    return null
  }

  if (authUser && authUser.isSuperAdmin) {
    const filteredUsers = filter(u => u.id !== authUser.id, users)
    return (
      <ImpersonateFormComponent
        loading={loading}
        impersonateUserId={impersonateUserId}
        users={filteredUsers}
        onSearch={setKeyword}
        onImpersonUserIdChange={async userId => {
          history.replace(getImpersonatePath(userId, location.pathname))
        }}
      />
    )
  }

  return null
}

export const QUERY_USERS = gql`
  query users($keyword: String) {
    users(input: {
      keyword: $keyword,
      type: ["operator", "power-user"]
    }) {
      id
      fullName
      emailAddress
      type
    }
  }
`

export default withRouter(ImpersonateForm)
