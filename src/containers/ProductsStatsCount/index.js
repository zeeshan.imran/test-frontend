import React, { useRef, useEffect } from 'react'
import { Row, Col, Form } from 'antd'
import { useTranslation } from 'react-i18next'
import Input from '../../components/Input'
import { Formik } from 'formik'
import { productStatMaxCount } from '../../validates'
import {
  PRODUCT_STATS_COUNT_MIN,
  PRODUCT_STATS_COUNT_MAX
} from '../../utils/Constants'
import { Container } from './styles'

const ProductsStatsCount = ({
  productCount,
  maxProductStatCount,
  totalProductCount
}) => {
  const { t } = useTranslation()
  const formRef = useRef()
  
  useEffect(() => {
    if (formRef.current)
      formRef.current.setFieldValue('totalProductCount', totalProductCount)
  }, [totalProductCount])

  return (
    <Formik
      ref={formRef}
      validationSchema={productStatMaxCount}
      initialValues={{
        maxProductStatCount,
        totalProductCount
      }}
      render={({ values, errors, setFieldValue }) => {
        const { maxProductStatCount: maxProductStatCountError } = errors
        return (
          <Container>
            <Row type='flex'>
              <Col>
                <Form.Item
                  help={maxProductStatCountError}
                  validateStatus={
                    maxProductStatCountError ? 'error' : 'success'
                  }
                >
                  <Input
                    label={t('components.maxProductStatCount.label')}
                    size='default'
                    name='maxProductStatCount'
                    value={maxProductStatCount}
                    type={`number`}
                    min={PRODUCT_STATS_COUNT_MIN}
                    max={PRODUCT_STATS_COUNT_MAX}
                    onChange={event => {
                      productCount(event.target.value)
                      setFieldValue('maxProductStatCount', event.target.value)
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Container>
        )
      }}
    />
  )
}

export default ProductsStatsCount
