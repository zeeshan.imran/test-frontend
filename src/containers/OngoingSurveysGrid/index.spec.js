import React from 'react'
import { mount } from 'enzyme'
import OngoingSurveysGrid from '.'
import Loader from '../../components/Loader'
import { getAuthenticatedUser } from '../../utils/userAuthentication'
import { MockedProvider } from 'react-apollo/test-utils'
import wait from '../../utils/testUtils/waait'
import OngoingSurveysGridComponent from '../../components/OngoingSurveysGrid'
import { ONGOING_SURVEYS_QUERY } from '../../queries/OngoingSurveysGrid'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

const mocks = [
  {
    request: {
      query: ONGOING_SURVEYS_QUERY
    },
    result: {
      data: {
        currentUser: { testerInfo: { surveys: [{ title: 'test', id: 1 }] } }
      }
    }
  }
]

describe('OngoingSurveysGrid', () => {
  let testRender

  let user

  beforeEach(() => {
    user = getAuthenticatedUser()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render loader', () => {
    testRender = mount(
      <MockedProvider>
        <OngoingSurveysGrid desktop={''} user={user} />
      </MockedProvider>
    )

    expect(Loader).toHaveLength(1)
  })

  test('should render OngoingSurveysGridComponent', async () => {
    testRender = mount(
      <MockedProvider mocks={mocks}>
        <OngoingSurveysGrid desktop={''} user={user} />
      </MockedProvider>
    )
    await wait(0)
    expect(OngoingSurveysGridComponent).toHaveLength(1)
  })
})
