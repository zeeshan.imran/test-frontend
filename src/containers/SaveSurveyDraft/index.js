import React from 'react'
import { withRouter } from 'react-router-dom'
import { useQuery } from 'react-apollo-hooks'
import LoadingModal from '../../components/LoadingModal'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import useToggle from '../../hooks/useToggle'
import useSurveyValidate from '../../hooks/useSurveyValidate'
import usePublishSurvey from '../../hooks/usePublishSurvey'
import useConfirmQuestionReorder from '../../hooks/useConfirmQuestionReorder'
import TooltipWrapper from '../../components/TooltipWrapper'

import { ButtonWithMargin } from './styles'
// import { validateSurveyBasics, validateProductSchema } from '../../validates'
import { useTranslation } from 'react-i18next'
import settingsSchema from '../../validates/settings'
import { emailsValidationSchema } from '../../components/OperatorSurveyCreateEmailSettings'
import getQueryParams from '../../utils/getQueryParams'

const SaveDraftSurvey = ({ history, location }) => {
  const { t } = useTranslation()
  const confirmQuestionReorder = useConfirmQuestionReorder()
  const { data: { surveyCreation: { products, basics } = {} } = {} } = useQuery(
    SURVEY_CREATION
  )
  const [publishing, togglePublishing] = useToggle(false)

  const { folderId } = getQueryParams(location)
  const folderQuerry =
    folderId && folderId !== 'null' && folderId !== 'undefined'
      ? `folderId=${folderId}`
      : ``

  const saveDraftSurvey = usePublishSurvey({
    surveyInitialState: t('containers.saveDraftSurvey.surveyInitialState'),
    successMessage: t('containers.saveDraftSurvey.successMessage'),
    errorMessage: t('containers.saveDraftSurvey.errorMessage'),
    folderId:
      folderId && folderId !== 'null' && folderId !== 'undefined'
        ? folderId
        : undefined
  })
  const surveyValidate = useSurveyValidate()

  let submitDisabled = true
  if (basics.isScreenerOnly) {
    submitDisabled = !basics.uniqueName || basics.linkedSurveys.length === 0
  } else if (basics.compulsorySurvey) {
    submitDisabled = !basics.uniqueName
  } else {
    submitDisabled =
      !basics.uniqueName ||
      products.length === 0 ||
      !settingsSchema.isValidSync(basics.autoAdvanceSettings) ||
      products.reduce((result, product) => result && !product.isAvailable, true)
  }

  const onSaveDraft = async () => {
    try {
      emailsValidationSchema.validateSync(basics)
    } catch (ex) {
      history.push(`./emails?${folderQuerry}`)
      return
    }

    const isAdvanceSettingsValid = await surveyValidate.validateAdvanceSettings()
    if (!isAdvanceSettingsValid) {
      history.push(`./settings`)
      await surveyValidate.showAdvanceSettingsErrors()
      return false
    }

    const isProductsValid = await surveyValidate.validateProducts()
    if (!isProductsValid) {
      history.push(`./products?${folderQuerry}`)
      await surveyValidate.showProductsErrors()
      return
    }

    const {
      isValid: isQuestionsValid,
      invalidQuestion
    } = await surveyValidate.validateQuestions()

    if (!isQuestionsValid && invalidQuestion.displayOn === 'payments') {
      history.push(
        `./financial?section=${invalidQuestion.displayOn}&&${folderQuerry}`
      )
      await surveyValidate.showQuestionsErrors()
      return
    }

    if (!isQuestionsValid) {
      history.push(
        `./questions?section=${invalidQuestion.displayOn}&&${folderQuerry}`
      )
      await surveyValidate.showQuestionsErrors()
      return
    }

    const duplicateTitles = await surveyValidate.validateChartTitles()

    if (duplicateTitles.length > 0) {
      await surveyValidate.showChartTitlesErrors(duplicateTitles)
      return
    }

    const onConfirm = async () => {
      togglePublishing()

      try {
        const savedSurvey = await saveDraftSurvey()
        togglePublishing()
        history.push(`/operator/draft/edit/${savedSurvey.id}?${folderQuerry}`)
      } catch (error) {
        togglePublishing()
        displayErrorPopup(error && error.message)
      }
    }

    confirmQuestionReorder(onConfirm)
  }

  return (
    <TooltipWrapper
      helperText={t('tooltips.saveDraftSurvey')}
      placement="leftTop"
    >
      <ButtonWithMargin
        data-testid="save-draft-survey-button"
        type={'secondary'}
        disabled={submitDisabled}
        size="default"
        onClick={onSaveDraft}
      >
        {t('containers.saveDraftSurvey.publishButton')}
      </ButtonWithMargin>
      <LoadingModal
        visible={publishing}
        text={t('containers.saveDraftSurvey.loadingText')}
      />
    </TooltipWrapper>
  )
}

export default withRouter(SaveDraftSurvey)
