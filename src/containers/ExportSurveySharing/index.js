import React, { useState, useRef } from 'react'
import moment from 'moment'
import gql from 'graphql-tag'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { useMutation } from 'react-apollo-hooks'
import Text from '../../components/Text'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import { CSVLink } from 'react-csv'

const ExportSurveySharing = ({ survey, onMenuItemClick }) => {
  const { t } = useTranslation()
  const csvLinkRef = useRef()
  const getSurveySharesMutation = useMutation(GET_SURVEY_SHARES)
  const [downloadData, setDownloadData] = useState([])
  const [filename, setFilename] = useState(
    `${survey.uniqueName}-sharing-payment-${moment(new Date()).format(
      'YYYYMMDD-HHmmss'
    )}.csv`
  )

  const filterData = async () => {
    displaySuccessMessage(
      t('containers.exportSharingContainer.popups.downloading')
    )
    const {
      data: { getSurveyShares }
    } = await getSurveySharesMutation({
      variables: {
        surveyId: survey.id
      }
    })
    if (getSurveyShares.length) {
      await downloadCSVFile(getSurveyShares, false) //Download sheet For Paypal
      await downloadCSVFile(getSurveyShares, true) //Download sheet For internal use
    } else {
      displayErrorPopup(t('containers.exportSharingContainer.popups.error'))
    }
    setDownloadData([])
  }

  const downloadCSVFile = async (getSurveyShares, internal = false) => {
    const finalData = []
    getSurveyShares.forEach(surveyShare => {
      if (internal) {
        if (surveyShare.refferedIds && surveyShare.refferedIds.length) {
          surveyShare.refferedIds.forEach(id => {
            finalData.push({ id: id })
          })
        }
        setFilename(`internal-${filename}`)
      } else {
        const externalDocument = {
          email: surveyShare.email,
          amount: surveyShare.amount,
          currency: surveyShare.currency,
          fullName: surveyShare.fullName
        }
        finalData.push({
          ...externalDocument,
          ...(Number(surveyShare.amountInDollar) > 0 && {
            amountInDollar: surveyShare.amountInDollar
          })
        })
      }
    })
    setDownloadData(finalData)
    displaySuccessMessage(
      t('containers.exportSharingContainer.popups.downloaded')
    )
    csvLinkRef.current.link.click() // Trigger CSVLink automatically
    setFilename(
      `${survey.uniqueName}-sharing-payment-${moment(new Date()).format(
        'YYYYMMDD-HHmmss'
      )}.csv`
    )
  }

  return (
    <React.Fragment>
      <Text
        onClick={() => {
          filterData()
          onMenuItemClick()
        }}
      >
        {t('tooltips.exportSharingButton')}
      </Text>
      {downloadData.length ? (
        <CSVLink
          filename={filename}
          target='_self'
          ref={csvLinkRef}
          data={downloadData}
        />
      ) : null}
    </React.Fragment>
  )
}

export default withRouter(ExportSurveySharing)

const GET_SURVEY_SHARES = gql`
  mutation getSurveyShares($surveyId: ID!) {
    getSurveyShares(surveyId: $surveyId) {
      email
      amount
      currency
      refferedIds
      fullName
      amountInDollar
    }
  }
`
