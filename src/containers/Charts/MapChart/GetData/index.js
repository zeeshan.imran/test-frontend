import { find } from 'ramda'

export const mergeResultsWithGeojson = (results, geoJson) => {
  // Map values into GeoJson object
  results.forEach(el => {
    for (let i = 0; i < geoJson.features.length; i++) {
      const result = (geoJson.features[i].properties.value = find(
        el => el.label === geoJson.features[i].properties.name,
        results
      ))
      geoJson.features[i].properties.value = result ? result.value : 0
    }
  })
  return geoJson.features
}
