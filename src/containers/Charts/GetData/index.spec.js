import { getTableData } from '.'

describe('Charts GetData container', () => {
  let t9n = str => str

  test('should build table with percentage grouping data for general chart', () => {
    const chartData = {
      chart_type: 'stacked-column',
      question_type: 'choose-multiple',
      question_with_products: 1,
      labels: ['a', 'b', 'Other'],
      results: [
        {
          name: 'Prod 1',
          data: [
            {
              label: 'a',
              analytics_value: '1',
              value: 1
            },
            {
              label: 'b',
              analytics_value: '2',
              value: 2
            },
            {
              label: 'Other',
              analytics_value: 'other',
              value: 3
            }
          ]
        },
        {
          name: 'Prod 2',
          data: [
            {
              label: 'a',
              analytics_value: '1',
              value: 4
            },
            {
              label: 'b',
              analytics_value: '2',
              value: 5
            },
            {
              label: 'Other',
              analytics_value: 'other',
              value: 6
            }
          ]
        },
        {
          name: 'Prod 3',
          data: [
            {
              label: 'a',
              analytics_value: '1',
              value: 7
            },
            {
              label: 'b',
              analytics_value: '2',
              value: 8
            },
            {
              label: 'Other',
              analytics_value: 'other',
              value: 9
            }
          ]
        }
      ]
    }
    const chartSettings = {
      howManyDecimals: 2,
      isDataTableShown_format: 'perc_abs_teh'
    }
    const expectedColumns = [
      { dataIndex: 'label', title: 'charts.table.answer' },
      { dataIndex: 'Prod 1', title: 'Prod 1' },
      { dataIndex: 'Prod 2', title: 'Prod 2' },
      { dataIndex: 'Prod 3', title: 'Prod 3' }
    ]
    let expectedDataSource = [
      {
        'Prod 1': '8.33% (1)',
        'Prod 2': '33.33% (4)',
        'Prod 3': '58.33% (7)',
        label: 'a',
        name: 'a'
      },
      {
        'Prod 1': '13.33% (2)',
        'Prod 2': '33.33% (5)',
        'Prod 3': '53.33% (8)',
        label: 'b',
        name: 'b'
      },
      {
        'Prod 1': '16.67% (3)',
        'Prod 2': '33.33% (6)',
        'Prod 3': '50.00% (9)',
        label: 'Other',
        name: 'Other'
      },
      {
        'Prod 1': 6,
        'Prod 2': 15,
        'Prod 3': 24,
        label: 'charts.table.total',
        name: 'charts.table.total'
      }
    ]
    let { columns, rows } = getTableData(chartData, chartSettings, t9n)
    expect(columns).toMatchObject(expectedColumns)
    expect(rows).toMatchObject(expectedDataSource)

    chartSettings.isDataTableShown_format = 'perc_abs_prod'
    expectedDataSource = [
      {
        'Prod 1': '16.67% (1)',
        'Prod 2': '26.67% (4)',
        'Prod 3': '29.17% (7)',
        label: 'a',
        name: 'a'
      },
      {
        'Prod 1': '33.33% (2)',
        'Prod 2': '33.33% (5)',
        'Prod 3': '33.33% (8)',
        label: 'b',
        name: 'b'
      },
      {
        'Prod 1': '50.00% (3)',
        'Prod 2': '40.00% (6)',
        'Prod 3': '37.50% (9)',
        label: 'Other',
        name: 'Other'
      },
      {
        'Prod 1': 6,
        'Prod 2': 15,
        'Prod 3': 24,
        label: 'charts.table.total',
        name: 'charts.table.total'
      }
    ]
    ;({ columns, rows } = getTableData(chartData, chartSettings, t9n))
    expect(columns).toMatchObject(expectedColumns)
    expect(rows).toMatchObject(expectedDataSource)

    chartSettings.isDataTableShown_format = 'perc_abs'
    expectedDataSource = [
      {
        'Prod 1': '2.22% (1)',
        'Prod 2': '8.89% (4)',
        'Prod 3': '15.56% (7)',
        label: 'a',
        name: 'a'
      },
      {
        'Prod 1': '4.44% (2)',
        'Prod 2': '11.11% (5)',
        'Prod 3': '17.78% (8)',
        label: 'b',
        name: 'b'
      },
      {
        'Prod 1': '6.67% (3)',
        'Prod 2': '13.33% (6)',
        'Prod 3': '20.00% (9)',
        label: 'Other',
        name: 'Other'
      },
      {
        'Prod 1': 6,
        'Prod 2': 15,
        'Prod 3': 24,
        label: 'charts.table.total',
        name: 'charts.table.total'
      }
    ]
    ;({ columns, rows } = getTableData(chartData, chartSettings, t9n))
    expect(columns).toMatchObject(expectedColumns)
    expect(rows).toMatchObject(expectedDataSource)
  })

  test('should build table with abs data for "Columns Mean" chart', () => {
    const chartData = {
      chart_type: 'columns-mean',
      question_type: 'choose-one',
      question_with_products: 1,
      labels: ['a', 'b', 'c'],
      chart_avg: true,
      avg: [
        {
          label: 'Prod 1',
          value: 1
        },
        {
          label: 'Prod 2',
          value: 1
        },
        {
          label: 'Prod 3',
          value: 9
        }
      ],
      results: [
        {
          name: 'Prod 1',
          data: [
            {
              label: 'a',
              analytics_value: '1',
              value: 1
            },
            {
              label: 'b',
              analytics_value: '5',
              value: 0
            },
            {
              label: 'c',
              analytics_value: '9',
              value: 0
            }
          ]
        },
        {
          name: 'Prod 2',
          data: [
            {
              label: 'a',
              analytics_value: '1',
              value: 1
            },
            {
              label: 'b',
              analytics_value: '5',
              value: 0
            },
            {
              label: 'c',
              analytics_value: '9',
              value: 0
            }
          ]
        },
        {
          name: 'Prod 3',
          data: [
            {
              label: 'a',
              analytics_value: '1',
              value: 0
            },
            {
              label: 'b',
              analytics_value: '5',
              value: 0
            },
            {
              label: 'c',
              analytics_value: '9',
              value: 1
            }
          ]
        }
      ]
    }
    const chartSettings = {
      howManyDecimals: 1,
      isDataTableShown_format: 'abs'
    }
    const expectedColumns = [
      { dataIndex: 'label', title: 'charts.table.answer' },
      { dataIndex: 'Prod 1', title: 'Prod 1' },
      { dataIndex: 'Prod 2', title: 'Prod 2' },
      { dataIndex: 'Prod 3', title: 'Prod 3' }
    ]
    const expectedDataSource = [
      {
        'Prod 1': 1,
        'Prod 2': 1,
        'Prod 3': 0,
        label: 'a',
        name: 'a'
      },
      {
        'Prod 1': 0,
        'Prod 2': 0,
        'Prod 3': 0,
        label: 'b',
        name: 'b'
      },
      {
        'Prod 1': 0,
        'Prod 2': 0,
        'Prod 3': 1,
        label: 'c',
        name: 'c'
      },
      {
        'Prod 1': 1,
        'Prod 2': 1,
        'Prod 3': 1,
        label: 'charts.table.total',
        name: 'charts.table.total'
      }
    ]
    let { columns, rows } = getTableData(chartData, chartSettings, t9n)
    expect(columns).toMatchObject(expectedColumns)
    expect(rows).toMatchObject(expectedDataSource)
  })

  test('should build table with abs data for "Spider(polar)" chart', () => {
    const chartData = {
      chart_type: 'polar',
      question_type: 'paired-questions',
      question_with_products: 1,
      labels: ['Second', 'First', 'Third'],
      range: [0, 10],
      series: [
        {
          name: 'Prod 1',
          data: [7.5, 2.5, 5]
        },
        {
          name: 'Prod 2',
          data: [5, 5, 5]
        },
        {
          name: 'Prod 3',
          data: [5, 5, 5]
        }
      ]
    }
    const chartSettings = {
      howManyDecimals: 1,
      isDataTableShown_format: 'abs'
    }
    const expectedColumns = [
      {
        dataIndex: 'label',
        title: 'charts.table.attribute',
        width: '25%'
      },
      { dataIndex: 'Prod 1', title: 'Prod 1', width: '25%' },
      { dataIndex: 'Prod 2', title: 'Prod 2', width: '25%' },
      { dataIndex: 'Prod 3', title: 'Prod 3', width: '25%' }
    ]
    const expectedDataSource = [
      {
        'Prod 1': '7.5',
        'Prod 2': '5.0',
        'Prod 3': '5.0',
        label: 'Second',
        name: 'Second'
      },
      {
        'Prod 1': '2.5',
        'Prod 2': '5.0',
        'Prod 3': '5.0',
        label: 'First',
        name: 'First'
      },
      {
        'Prod 1': '5.0',
        'Prod 2': '5.0',
        'Prod 3': '5.0',
        label: 'Third',
        name: 'Third'
      }
    ]
    let { columns, rows } = getTableData(chartData, chartSettings, t9n)
    expect(columns).toMatchObject(expectedColumns)
    expect(rows).toMatchObject(expectedDataSource)
  })

  test('should build table with percentage data for "Matrix" chart', () => {
    const chartData = {
      chart_type: 'column',
      question_type: 'matrix',
      question_with_products: 1,
      labels: ['No', 'doubt', 'Probably', 'Yes'],
      attributes: [
        'Do you like it?',
        'Do you like it?a',
        'Pls rate these survey'
      ],
      results: [
        {
          data: [
            {
              label: 'No',
              analytics_value: '1',
              value: 1
            },
            {
              label: 'doubt',
              analytics_value: '2',
              value: 0
            },
            {
              label: 'Probably',
              analytics_value: '3',
              value: 0
            },
            {
              label: 'Yes',
              analytics_value: '4',
              value: 0
            }
          ],
          name: 'Do you like it?'
        },
        {
          data: [
            {
              label: 'No',
              analytics_value: '1',
              value: 0
            },
            {
              label: 'doubt',
              analytics_value: '2',
              value: 1
            },
            {
              label: 'Probably',
              analytics_value: '3',
              value: 0
            },
            {
              label: 'Yes',
              analytics_value: '4',
              value: 0
            }
          ],
          name: 'Do you like it?a'
        },
        {
          data: [
            {
              label: 'No',
              analytics_value: '1',
              value: 0
            },
            {
              label: 'doubt',
              analytics_value: '2',
              value: 0
            },
            {
              label: 'Probably',
              analytics_value: '3',
              value: 1
            },
            {
              label: 'Yes',
              analytics_value: '4',
              value: 0
            }
          ],
          name: 'Pls rate these survey'
        }
      ]
    }
    const chartSettings = {
      howManyDecimals: 1,
      isDataTableShown_format: 'perc'
    }
    const expectedColumns = [
      {
        dataIndex: 'label',
        title: 'charts.table.answer'
      },
      {
        dataIndex: 'Do you like it?',
        title: 'Do you like it?'
      },
      {
        dataIndex: 'Do you like it?a',
        title: 'Do you like it?a'
      },
      {
        dataIndex: 'Pls rate these survey',
        title: 'Pls rate these survey'
      }
    ]
    const expectedDataSource = [
      {
        'Do you like it?': '33.3%',
        'Do you like it?a': '0.0%',
        'Pls rate these survey': '0.0%',
        label: 'No',
        name: 'No'
      },
      {
        'Do you like it?': '0.0%',
        'Do you like it?a': '33.3%',
        'Pls rate these survey': '0.0%',
        label: 'doubt',
        name: 'doubt'
      },
      {
        'Do you like it?': '0.0%',
        'Do you like it?a': '0.0%',
        'Pls rate these survey': '33.3%',
        label: 'Probably',
        name: 'Probably'
      },
      {
        'Do you like it?': '0.0%',
        'Do you like it?a': '0.0%',
        'Pls rate these survey': '0.0%',
        label: 'Yes',
        name: 'Yes'
      },
      {
        'Do you like it?': 1,
        'Do you like it?a': 1,
        'Pls rate these survey': 1,
        label: 'charts.table.total',
        name: 'charts.table.total'
      }
    ]
    let { columns, rows } = getTableData(chartData, chartSettings, t9n)
    expect(columns).toMatchObject(expectedColumns)
    expect(rows).toMatchObject(expectedDataSource)
  })
})
