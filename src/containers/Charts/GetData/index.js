import { findIndex, clone, sortBy, prop, find, propEq } from 'ramda'
import * as d3 from 'd3'

const ALL_PROD = 'all_prod'

export const getProducts = inputData => {
  const data = inputData.results ? inputData.results : inputData.series
  return Array.isArray(data) ? data.map(el => el.name) : null
}

export const getAnalyticValuesExtent = inputData => {
  if (inputData.valuesExtent) {
    return inputData.valuesExtent
  }
  let min = Number.MAX_VALUE
  let max = Number.MIN_VALUE
  const res = Array.isArray(inputData.results)
    ? inputData.results[0]
    : inputData.results
  res.data &&
    res.data.forEach(el => {
      const val = parseInt(el.analytics_value, 10)
      if (!isNaN(val)) {
        min = Math.min(val, min)
        max = Math.max(val, max)
      }
    })
  return [min, max]
}

export const getSelectedStackedData = (inputData, products, labels) => {
  if (Array.isArray(inputData.results)) {
    return inputData.results
      .map((result, i) => {
        if (!products[i].isSelected) return null
        let data = {
          _metadata: {
            name: products[i].name,
            productIndex: products[i].index,
            summ: 0
          }
        }
        for (let i = 0; i < result.data.length; i++) {
          if (
            findIndex(
              el => el.name === result.data[i].label && el.isSelected,
              labels
            ) !== -1
          ) {
            data[result.data[i].label] = result.data[i].value
            data._metadata.summ += result.data[i].value
          } else {
            data[result.data[i].label] = 0
          }
        }
        return data
      })
      .filter(el => el)
  } else {
    let data = {
      _metadata: {
        name: products[0].name,
        productIndex: products[0].index,
        summ: 0
      }
    }
    for (let i = 0; i < inputData.results.data.length; i++) {
      if (
        findIndex(
          el => el.name === inputData.results.data[i].label && el.isSelected,
          labels
        ) !== -1
      ) {
        data[inputData.results.data[i].label] = inputData.results.data[i].value
        data._metadata.summ += inputData.results.data[i].value
      } else {
        data[inputData.results.data[i].label] = 0
      }
    }
    return [data]
  }
}

export const getMaxSelectedValue = (inputData, products) => {
  if (inputData.results.data) {
    // No-products chart
    return inputData.results.data.reduce(
      (max, cur) => Math.max(max, cur.value),
      0
    )
  } else {
    return inputData.results
      .filter((_el, i) => (products ? products[i].isSelected : true))
      .reduce(
        (max, cur) =>
          Math.max(
            cur.data.reduce((max, cur) => Math.max(max, cur.value), 0),
            max
          ),
        0
      )
  }
}

export const getSumSelectedValue = (inputData, products) => {
  if (inputData.results.data) {
    // No-products chart
    return inputData.results.data.reduce((acc, cur) => acc + cur.value, 0)
  } else {
    return inputData.results
      .filter((_el, i) => (products ? products[i].isSelected : true))
      .reduce((max, cur) => {
        return Math.max(
          max,
          cur.data.reduce((acc, cur) => acc + cur.value, 0)
        )
      }, 0)
  }
}

// Sums ALL answers throu all products.
// !!Not equals Amount of respondents
export const getTotalSum = inputData => {
  if (
    inputData.question_with_products ||
    inputData.question_type === 'matrix'
  ) {
    if (!Array.isArray(inputData.results)) {
      return 1
    }
    return inputData.results.reduce(
      (acc, el) => acc + d3.sum(el.data, d => d.value),
      0
    )
  } else {
    return inputData.results ? d3.sum(inputData.results.data, d => d.value) : 1
  }
}

const getTableColumnWidth = inputData => {
  if (inputData.results) {
    if (Array.isArray(inputData.results)) {
      return 100 / (inputData.results.length + 1) + '%'
    } else {
      return '50%'
    }
  } else if (inputData.series) {
    if (Array.isArray(inputData.series)) {
      return 100 / (inputData.series.length + 1) + '%'
    } else {
      return '50%'
    }
  }
}

// Transforms STATS response into table representation
export const getTableData = (inputData, chartSettings, t) => {
  const firstColName = ['paired-questions', 'slider'].includes(
    inputData.question_type
  )
    ? t('charts.table.attribute')
    : t('charts.table.answer')
  const scoreName = ['paired-questions', 'slider'].includes(
    inputData.question_type
  )
    ? t('charts.table.score')
    : t('charts.table.value')
  let columnWidth = getTableColumnWidth(inputData)
  const columns = [
    {
      title: firstColName,
      dataIndex: 'label',
      key: -1,
      width: columnWidth
    }
  ]
  const rows = []
  const labelRows = new Map(inputData.labels.map(l => [l, { name: l }]))
  const totalSum = Math.max(getTotalSum(inputData), 1)
  const decimals = !isNaN(chartSettings.howManyDecimals)
    ? chartSettings.howManyDecimals
    : 2

  const parseProdResultInRows = (res, i = ALL_PROD) => {
    columns.push({
      title: res.name || scoreName,
      dataIndex: `${i}`,
      key: i,
      width: columnWidth
    })
    const prodSum = d3.sum(res.data, el => el.value)
    res.data.forEach(labelRes => {
      let formattedVal, tehSum
      switch (chartSettings.isDataTableShown_format) {
        case 'perc':
          formattedVal =
            ((100 * labelRes.value) / totalSum).toFixed(decimals) + '%'
          break

        case 'perc_abs':
          formattedVal =
            ((100 * labelRes.value) / totalSum).toFixed(decimals) + '%'
          formattedVal += ` (${labelRes.value})`
          break
        case 'perc_abs_teh':
          tehSum = inputData.results.map(res =>
            res.data.find(el => el.label === labelRes.label)
          )
          formattedVal =
            (
              (100 * labelRes.value) /
              Math.max(
                d3.sum(tehSum, el => el.value),
                1
              )
            ).toFixed(decimals) + '%'
          formattedVal += ` (${labelRes.value})`
          break
        case 'perc_prod':
          formattedVal =
            ((100 * labelRes.value) / Math.max(prodSum, 1)).toFixed(decimals) +
            '%'
          break
        case 'perc_abs_prod':
          formattedVal =
            ((100 * labelRes.value) / Math.max(prodSum, 1)).toFixed(decimals) +
            '%'
          formattedVal += ` (${labelRes.value})`
          break
        default:
          formattedVal =
            labelRes.value % 1 === 0
              ? labelRes.value
              : labelRes.value.toFixed(decimals)
      }
      labelRows.get(labelRes.label)[i] = formattedVal
    })
    if (shouldHaveTotalRow(inputData)) {
      labelRows.get('__total__')[i] = prodSum
    }
  }

  // For Polar chart
  const parseProdSeriesInRows = (res, i = ALL_PROD) => {
    columns.push({
      title: res.name || t('charts.table.value'),
      dataIndex: `${i}`,
      key: i,
      width: columnWidth
    })
    res.data.forEach((result, resIndex) => {
      labelRows.get(inputData.labels[resIndex])[i] = result.toFixed(decimals)
    })
  }

  const shouldHaveTotalRow = inputData =>
    inputData.results &&
    inputData.question_type !== 'paired-questions' &&
    !(
      inputData.question_type === 'slider' &&
      inputData.chart_type !== 'stacked-bar'
    )

  if (inputData.results) {
    if (shouldHaveTotalRow(inputData)) {
      labelRows.set('__total__', { name: t('charts.table.total') })
    }
    if (Array.isArray(inputData.results)) {
      if (inputData.chart_type === 'columns-mean' && inputData.avg) {
        // averages are already sorted via chart settings
        // Use the same order for data table columns
        inputData.avg.forEach(el => {
          const res = find(propEq('name', el.label), inputData.results)
          parseProdResultInRows(
            res,
            inputData.question_with_products ? res.name : ALL_PROD
          )
        })
      } else if (inputData.question_type === 'matrix') {
        inputData.results.forEach(r => parseProdResultInRows(r, r.name))
      } else {
        inputData.results.forEach(r =>
          parseProdResultInRows(
            r,
            inputData.question_with_products ? r.name : ALL_PROD
          )
        )
      }
    } else {
      parseProdResultInRows(inputData.results)
    }
  } else if (inputData.series) {
    if (Array.isArray(inputData.series)) {
      inputData.series.forEach(r =>
        parseProdSeriesInRows(
          r,
          inputData.question_with_products ? r.name : ALL_PROD
        )
      )
    } else {
      parseProdSeriesInRows(inputData.series)
    }
  }

  let i = 0
  labelRows.forEach((row, key) => {
    rows.push(Object.assign(row, { label: row.name, key: `label-${i++}` }))
  })
  return {
    columns,
    rows
  }
}

export const swapProductsWithLabels = inputData => {
  let swappedData = clone(inputData)
  swappedData.labels = inputData.results.map(el => el.name)
  swappedData.results = []
  let accumulatedResults = {}
  inputData.results.forEach(prodResult => {
    if (!Array.isArray(prodResult.data)) {
      return inputData
    }
    prodResult.data.forEach(labelResult => {
      if (!accumulatedResults[labelResult.label]) {
        accumulatedResults[labelResult.label] = []
      }
      accumulatedResults[labelResult.label].push({
        label: prodResult.name,
        value: labelResult.value
      })
    })
  })
  inputData.labels.forEach(label => {
    swappedData.results.push({
      name: label,
      data: accumulatedResults[label] || []
    })
  })
  return swappedData
}

const sortMeanValues = (inputData, sortOrder) => {
  if (!sortOrder) {
    return inputData
  }
  const [sortProp, sortDirection] = sortOrder.split('.')
  if (inputData.avg) {
    inputData.avg = sortBy(prop(sortProp), inputData.avg)
    if (sortDirection === 'desc') {
      inputData.avg = inputData.avg.reverse()
    }
    if (inputData.chart_type === 'stacked-column-horizontal-bars') {
      inputData.results = inputData.avg.map(avgResult =>
        find(propEq('name', avgResult.label), inputData.results)
      )
    }
  }
  if (inputData.chart_type === 'bar') {
    if (inputData.results.data) {
      inputData.results.data = sortBy(prop(sortProp), inputData.results.data)
      if (sortDirection === 'desc') {
        inputData.results.data = inputData.results.data.reverse()
      }
    } else {
      inputData.results = inputData.results.map(res => {
        let data = sortBy(prop(sortProp), res.data)
        if (sortDirection === 'desc') {
          data = data.reverse()
        }
        return {
          ...res,
          data
        }
      })
    }
  }
  return inputData
}

const mergeLabelsInResult = (results, labelGroup, labelName) => {
  const mergedResult = []
  let summ = 0
  let analytics_value = 0

  results.forEach(res => {
    if (labelGroup.includes(res.label)) {
      summ += res.value
      analytics_value += parseInt(res.analytics_value, 10)
    } else {
      mergedResult.push(res)
    }
  })
  mergedResult.push({
    label: labelName,
    value: summ,
    analytics_value: analytics_value / labelGroup.length
  })
  return mergedResult
}

export const groupResultsByLabels = (inputData, labelGroups, sortOrder) => {
  if (!labelGroups || !labelGroups.length) {
    return sortOrder ? sortMeanValues(inputData, sortOrder) : inputData
  }

  let results = clone(inputData.results),
    labels = []

  labelGroups.forEach(labelGroup => {
    if (labelGroup.length === 1) {
      labels.push(labelGroup[0])
    } else {
      const labelName = labelGroup.join(', ')
      const strLabelGroup = labelGroup.map(el => el + '')
      if (
        !inputData.question_with_products &&
        inputData.question_type !== 'matrix'
      ) {
        results.data = mergeLabelsInResult(
          results.data,
          strLabelGroup,
          labelName
        )
      } else {
        results.forEach(res => {
          res.data = mergeLabelsInResult(res.data, strLabelGroup, labelName)
        })
      }
    }
  })

  // not-default grouping: need sort
  const comparator = (a, b) => {
    const aFloat = parseFloat(a.analytics_value)
    const bFloat = parseFloat(b.analytics_value)
    if (isNaN(aFloat) && isNaN(bFloat)) {
      return a.analytics_value > b.analytics_value ? 1 : -1
    }
    if (isNaN(aFloat)) {
      return 1
    }
    if (isNaN(bFloat)) {
      return -1
    }
    return a.analytics_value - b.analytics_value
  }
  if (
    !inputData.question_with_products &&
    inputData.question_type !== 'matrix'
  ) {
    results.data.sort(comparator)
  } else {
    results = results.map(res => ({
      ...res,
      data: res.data.sort(comparator)
    }))
  }

  labels =
    inputData.question_with_products || inputData.question_type === 'matrix'
      ? results[0].data.map(el => el.label)
      : results.data.map(el => el.label)

  const resultData = {
    ...inputData,
    labels,
    results,
    valuesExtent: getAnalyticValuesExtent(inputData)
  }

  return sortOrder ? sortMeanValues(resultData, sortOrder) : resultData
}
