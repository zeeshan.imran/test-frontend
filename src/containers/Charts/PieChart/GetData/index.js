import { find, propEq } from 'ramda'

export const parseData = (inputData, labelsSelection) => {
  let data = []
  const dataArr = inputData.results.data
  for (let i = 0; i < labelsSelection.length; i++) {
    if (labelsSelection[i].isSelected) {
      data.push({
        value: find(propEq('label', labelsSelection[i].name), dataArr).value,
        index: labelsSelection[i].index
      })
    }
  }
  return data
}
