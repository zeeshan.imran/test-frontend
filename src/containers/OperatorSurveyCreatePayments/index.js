import React, { useEffect, useRef } from 'react'
import { useQuery, useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import OperatorSurveyPayments from '../../components/OperatorSurveyCreatePayments'

const OperatorSurveyCreatePaymentSettings = ({ stricted = false }) => {
  const {
    data: {
      surveyCreation: { basics }
    },
    refetch: refetchCurrent
  } = useQuery(SURVEY_CREATION)
  const refetch = useRef(refetchCurrent)
  const updateSurveyBasics = useMutation(UPDATE_SURVEY_CREATION_BASICS)
  const requirePaypalEmailStep = useMutation(REQUIRE_PAYPAL_EMAIL)
  const unrequirePaypalEmailStep = useMutation(UNREQUIRE_PAYPAL_EMAIL)
  const requirePaymentQuestion = useMutation(REQUIRE_PAYMENT_QUESTION)
  const unrequirePaymentQuestion = useMutation(UNREQUIRE_PAYMENT_QUESTION)
  
  const requireShowProductScreen = useMutation(REQUIRE_SHOW_PRODUCT_SCREEN)
  const unrequireShowProductScreen = useMutation(UNREQUIRE_SHOW_PRODUCT_SCREEN)

  useEffect(() => {
    if (refetch && refetch.current && typeof refetch.current === 'function') {
      refetch.current()
    }
  }, [basics.country])

  const handleChange = value => {
    const basicsUpdated = { ...basics, ...value }
    if (basicsUpdated.isPaypalSelected && basicsUpdated.isGiftCardSelected) {
      requirePaymentQuestion()
    } else {
      unrequirePaymentQuestion()
    }

    if (basicsUpdated.referralAmount && basicsUpdated.isPaypalSelected) {
      requirePaypalEmailStep()
    } else {
      unrequirePaypalEmailStep()
    }

    if (basicsUpdated.showSurveyProductScreen) {
      requireShowProductScreen()
    } else {
      unrequireShowProductScreen()
    }

    updateSurveyBasics({
      variables: {
        basics: basicsUpdated
      }
    })
  }

  return (
    <OperatorSurveyPayments
      onChange={handleChange}
      data={basics}
      stricted={stricted}
    />
  )
}

export const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      basics {
        isPaypalSelected
        isGiftCardSelected
        referralAmount
        country
        dualCurrency
        referralAmountInDollar
      }
    }
  }
`
export const UPDATE_SURVEY_CREATION_BASICS = gql`
  mutation updateSurveyCreationBasics($basics: basics) {
    updateSurveyCreationBasics(basics: $basics) @client
  }
`
export const REQUIRE_PAYPAL_EMAIL = gql`
  mutation requirePaymentQuestion {
    requirePaypalEmail(id: "") @client
  }
`

export const UNREQUIRE_PAYPAL_EMAIL = gql`
  mutation unrequirePaypalEmail {
    unrequirePaypalEmail(id: "") @client
  }
`

export const REQUIRE_PAYMENT_QUESTION = gql`
  mutation requirePaymentQuestion {
    requirePaymentQuestion(id: "") @client
  }
`

export const UNREQUIRE_PAYMENT_QUESTION = gql`
  mutation unrequirePaymentQuestion {
    unrequirePaymentQuestion(id: "") @client
  }
`

export const REQUIRE_SHOW_PRODUCT_SCREEN = gql`
  mutation requireShowProductScreen {
    requireShowProductScreen(id: "") @client
  }
`

export const UNREQUIRE_SHOW_PRODUCT_SCREEN = gql`
  mutation unrequireShowProductScreen {
    unrequireShowProductScreen(id: "") @client
  }
`


export default OperatorSurveyCreatePaymentSettings
