import React, { useEffect, useRef } from 'react'
import ProductsIncentiveRulesComponent from '../../components/ProductsIncentiveRules'
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo-hooks'
import defaults from '../../defaults'

const ProductsIncentiveRules = ({ stricted }) => {
  const {
    data: {
      surveyCreation: { basics }
    },
    refetch: refetchCurrent
  } = useQuery(SURVEY_CREATION)

  const productRewardsRule = basics.productRewardsRule || {}
  const refetch = useRef(refetchCurrent)

  useEffect(() => {
    if (refetch && refetch.current && typeof refetch.current === 'function') {
      refetch.current()
    }
  }, [productRewardsRule])

  const updateBasics = useMutation(UPDATE_SURVEY_CREATION_BASICS)

  if (!productRewardsRule.active) {
    return null
  }

  const updateIncentives = (change) => {
    const basicsUpdated = {
      ...basics,
      productRewardsRule: { ...basics.productRewardsRule, ...change }
    }
    updateBasics({
      variables: {
        basics: basicsUpdated
      }
    })
  }

  const currentCountry = basics ? basics.country : ''
  const availableCountry = defaults.countries.find(
    country => country.code === currentCountry
  )
  const rewardCurrency = availableCountry || {}
  const prefix = defaults.currencyPrefixes[rewardCurrency.currency] || ''
  const suffix = defaults.currencySuffixes[rewardCurrency.currency] || ''

  return (
    <ProductsIncentiveRulesComponent
      onChange={updateIncentives}
      {...productRewardsRule}
      stricted={stricted}
      prefix={prefix}
      suffix={suffix}
    />
  )
}

const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      products
      basics {
        country
        productRewardsRule {
          min
          max
          percentage
          active
        }
      }
    }
  }
`

export const UPDATE_SURVEY_CREATION_BASICS = gql`
  mutation updateSurveyCreationBasics($basics: basics) {
    updateSurveyCreationBasics(basics: $basics) @client
  }
`

export default ProductsIncentiveRules
