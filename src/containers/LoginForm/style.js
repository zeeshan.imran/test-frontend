import styled from 'styled-components'

export const NotificationText = styled.div`
  font-size: 2rem;
  padding-bottom: 2rem;
  text-align: center;
`
