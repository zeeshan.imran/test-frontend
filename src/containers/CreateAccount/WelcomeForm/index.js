import React from 'react'
import { Formik } from 'formik'
import * as Yup from 'yup'
import WelcomeFormComponent from '../../../pages/CreateAccount/WelcomeForm'
import SignUpFormContainer from '..'
import { useTranslation } from 'react-i18next';

const WelcomeForm = () => (
  <SignUpFormContainer>
    {({
      fieldChangeHandler,
      formData: { email, password, firstStepValid }
    }) => {
      const { t } = useTranslation();
      return (
        <Formik
          validationSchema={Yup.object().shape({
            email: Yup.string()
              .email(t('containers.createAccount.welcomeForm.emailValid'))
              .required(t('containers.createAccount.welcomeForm.emailRequire')),
            password: Yup.string()
              .min(8, t('containers.createAccount.welcomeForm.passwordMin'))
              .required(t('containers.createAccount.welcomeForm.passwordRequire')),
            confirm: Yup.string()
              .oneOf([Yup.ref('password')], t('containers.createAccount.welcomeForm.confirmMatch'))
              .required(t('containers.createAccount.welcomeForm.confirmRequire'))
          })}
          initialValues={{ email, password }}
          render={({ values, handleChange, ...rest }) => {
            if (rest.isValid !== firstStepValid) {
              fieldChangeHandler('firstStepValid', rest.isValid)
            }

            const getChangeHandler = field => event => {
              if (field !== 'confirm') {
                fieldChangeHandler(field, event.target.value)
              }
              return handleChange(event)
            }

            return (
              <WelcomeFormComponent
                {...rest}
                {...values}
                getChangeHandler={getChangeHandler}
              />
            )
          }}
        />
      )
    }}
  </SignUpFormContainer>
)

export default WelcomeForm
