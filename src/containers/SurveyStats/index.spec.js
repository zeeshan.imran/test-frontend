import React from 'react'
import { mount } from 'enzyme'
import SurveyStats from './index'
import gql from 'graphql-tag'
import wait from '../../utils/testUtils/waait'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'

jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k,
  getFixedT: () => () => ''
}))

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

const SAVE_CHARTS_SETTINGS = gql`
  mutation saveChartsSettings($surveyId: ID!, $chartsSettings: JSON) {
    saveChartsSettings(surveyId: $surveyId, chartsSettings: $chartsSettings)
  }
`

const QUERY_SURVEY_STATS = gql`
  query surveyStats(
    $surveyId: ID!
    $productIds: [String!]
    $questionFilters: [Object!]
    $checkOrg: Boolean
  ) {
    survey(id: $surveyId, checkOrg: $checkOrg) {
      id
      owner
      showGeneratePdf
      stats(productIds: $productIds, questionFilters: $questionFilters)
    }
  }
`

const QUERY_SURVEY = gql`
  query surveyStats($surveyId: ID!, $checkOrg: Boolean) {
    survey(id: $surveyId, checkOrg: $checkOrg) {
      id
      name
      maxProductStatCount
      products {
        id
        name
        photo
      }
      screeningQuestions {
        id
        prompt
        chartTopic
        chartTitle
        type
        options {
          label
          value
        }
      }
    }
  }
`

const CHARTS_SETTINGS_QUERY = gql`
  query getChartsSettings($surveyId: ID!) {
    getChartsSettings(surveyId: $surveyId)
  }
`

describe('SurveyStats', () => {
  let testRender
  let historyMock

  const mockApolloClient = ({ statsResult }) => {
    return createApolloMockClient({
      mocks: [
        {
          request: {
            query: QUERY_SURVEY,
            variables: {
              surveyId: 'survey-1',
              checkOrg: true,
              productIds: [],
              questionFilters: []
            }
          },
          result: statsResult
        },
        {
          request: {
            query: QUERY_SURVEY_STATS,
            variables: {
              surveyId: 'survey-1',
              checkOrg: true,
              productIds: [],
              questionFilters: []
            }
          },
          result: statsResult
        },
        {
          request: {
            query: CHARTS_SETTINGS_QUERY,
            variables: {
              surveyId: 'survey-1'
            }
          },
          result: {
            data: {
              getChartsSettings: {
                chartsSettings: []
              }
            }
          }
        },
        {
          request: {
            query: SAVE_CHARTS_SETTINGS,
            variables: {
              surveyId: 'survey-1',
              chartsSettings: {
                'undefined-pie': {
                  howManyDecimals: 2,
                  colorsArr: [
                    '#3497db',
                    '#e74c3c',
                    '#2fcc71',
                    '#f1c40f',
                    '#8e44ad',
                    '#95a5a7',
                    '#ff7f0e',
                    '#1abc9c',
                    '#8c564b',
                    '#e377c2'
                  ]
                },
                'undefined-map': {
                  howManyDecimals: 2,
                  mapColorsArr: ['#3497db', '#b4d69a']
                },
                global: {
                  singleColor: ['#3497db'],
                  mapColorsArr: ['#3497db', '#b4d69a'],
                  colorsArr: [
                    '#3497db',
                    '#e74c3c',
                    '#2fcc71',
                    '#f1c40f',
                    '#8e44ad',
                    '#95a5a7',
                    '#ff7f0e',
                    '#1abc9c',
                    '#8c564b',
                    '#e377c2'
                  ],
                  groupLabels: true,
                  hasOutline_color: ['#424242'],
                  isStatisticsTableMeanShown: true,
                  statisticsTable_howManyDecimals: '2',
                  howManyDecimals: 2
                }
              }
            }
          },
          result: {
            data: {
              saveChartsSettings: {}
            }
          }
        }
      ]
    })
  }

  beforeEach(() => {
    historyMock = {
      push: jest.fn(),
      goBack: () => {
        return jest.fn()
      },
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  it('should display SurveyStats', async () => {
    const client = mockApolloClient({
      statsResult: {
        data: {
          survey: {
            id: 'survey-1',
            name: 'survey',
            showGeneratePdf: false,
            stats: {
              tabs: [
                {
                  title: 'The chart title',
                  charts: [{}]
                }
              ]
            }
          }
        }
      }
    })

    testRender = mount(
      <ApolloProvider client={client}>
        <SurveyStats
          history={historyMock}
          match={{ params: { surveyId: 'survey-1' } }}
        />
      </ApolloProvider>
    )

    await wait(0)
    testRender.update()

    expect(testRender.find(SurveyStats)).toHaveLength(1)
  })

  it('should prevent from unauthorization', async () => {
    const client = mockApolloClient({
      statsResult: {
        errors: [
          {
            message:
              'StatusCodeError: 400 - "Could not find survey with the name or id survey-id"'
          }
        ]
      }
    })

    testRender = mount(
      <ApolloProvider client={client}>
        <SurveyStats
          history={historyMock}
          match={{ params: { surveyId: 'survey-1' } }}
        />
      </ApolloProvider>
    )

    await wait(0)

    expect(testRender.find(SurveyStats)).toHaveLength(1)
  })

  it('should show blank when no tabpages are available', async () => {
    const clientTabPages = mockApolloClient({
      statsResult: {
        data: {
          survey: {
            id: 'survey-1',
            name: 'survey',
            maxProductStatCount: 6,
            products: {
              id: 'product-1',
              name: 'Product 1',
              photo: ''
            },
            showGeneratePdf: false,
            stats: {
              tabs: []
            }
          }
        }
      }
    })

    testRender = mount(
      <ApolloProvider client={clientTabPages}>
        <SurveyStats
          history={historyMock}
          match={{ params: { surveyId: 'survey-1' } }}
        />
      </ApolloProvider>
    )
    await wait(0)
    expect(testRender.find(SurveyStats)).toHaveLength(1)
  })
})
