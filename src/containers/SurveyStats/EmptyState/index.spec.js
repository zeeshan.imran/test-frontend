import React from 'react'
import { mount, shallow } from 'enzyme'
import EmptyState from '.'
import { Router } from 'react-router-dom'
import { Button } from 'antd'

describe('EmptyState', () => {
  let testRender
  let historyMock

  beforeEach(() => {
    historyMock = {
      push: jest.fn(),
      goBack: () => {
        return jest.fn()
      },
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' } 
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  it('should display EmptyState', () => {
    testRender = shallow(
      <Router history={historyMock}>
        <EmptyState history={historyMock} />
      </Router>
    )
    expect(testRender).toMatchSnapshot()
  })

  test('should render EmptyState history go back', async () => {
    const mockGoBack = jest.fn()
    const initialProps = {
      history: {
        goBack: mockGoBack,
        location: {
          pathname: ''
        }
      }
    }
    testRender = mount(
      <Router history={historyMock}>
        <EmptyState {...initialProps} />
      </Router>
    )

    testRender.find(Button).simulate('click') 
  })
})
