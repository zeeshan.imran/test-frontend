import React, { useEffect, useState, useRef, useMemo } from 'react'
import { path, append, remove, clone } from 'ramda'
import gql from 'graphql-tag'
import * as Yup from 'yup'
import TabHorizontal from '../../components/TabHorizontal'
import * as chartUtils from '../../utils/chartUtils'
import { displayInfoPopup } from '../../utils/displayInfoPopup'
import Charts from '../../components/Charts'
import StatsProductSection from '../../components/StatsProductSection'
import LoadingModal from '../../components/LoadingModal'
import { useQuery, useMutation } from 'react-apollo-hooks'
import OperatorPage from '../../components/OperatorPage'
import OperatorPageContent from '../../components/OperatorPageContent'
import { Alert, Button, Icon, Card, message, notification } from 'antd'
import { useTranslation } from 'react-i18next'
import ErrorBoundary from './ErrorBoundary'
import EmptyState from './EmptyState'
import useSurveyStatsResult from '../../hooks/useSurveyStatsResult'
import { ERRORS, STATS_FILTER_ACCEPTED_TYPES } from '../../utils/Constants'
import StatsFilterModal from '../../components/StatsFilterModal'
import StatsOptionsForm from '../../components/StatsOptionsForm'
import { ChartsRow } from './styles'
import GeneratePdf from '../../components/GeneratePdf/index'
import GeneratePpt from '../../components/GeneratePpt/index'
import {
  getAuthenticatedOrganization,
  getImpersonateUserId,
  isUserAuthenticatedAsAnalytics
} from '../../utils/userAuthentication'
import TooltipWrapper from '../../components/TooltipWrapper'
import AlertModal from '../../components/AlertModal'
import {
  getAvailableChartSettings,
  getDefaultChartSettings,
  getDefaultChartSettingsByChart
} from '../../utils/chartSettings'
import logger from '../../utils/logger'
import useStatistic from '../../hooks/useStatistic'

const getStats = path(['survey', 'stats'])
const getStatsJobGroupId = path(['survey', 'stats', 'jobGroupId'])
const getStatsJobIds = path(['survey', 'stats', 'jobIds'])
const getOwner = path(['survey', 'owner'])

const SurveyStats = ({ match: { params }, history }) => {
  const isAnalytics = isUserAuthenticatedAsAnalytics()
  const tabId = useRef()
  if (params.tabId && tabId.current !== params.tabId) {
    tabId.current = params.tabId
  }
  const surveyId = params.surveyId
  const isImpersonating = !!getImpersonateUserId()
  const { t } = useTranslation()
  const [productsSelected, setProductsSelected] = useState([])
  const [finalQuestionsSelected, setFinalQuestionsSelected] = useState([])
  const [finalDateFilter, setFinalDateFilter] = useState([])
  const [isLoaded, setIsLoaded] = useState(true)

  const [shouldGenerateCharts, setShouldGenerateCharts] = useState(false)
  const [hasProductSelection, setHasProductSelection] = useState(false)
  const [questionList, setQuestionList] = useState(false)

  const validationSchema = Yup.object().shape({
    tabs: Yup.array()
      .typeError(t('containers.surveyStats.validation.typeError'))
      .of(
        Yup.object().shape({
          title: Yup.string().required(),
          charts: Yup.array()
        })
      )
  })

  const handleTabChange = tabId => {
    history.push(`/operator/stats/${surveyId}/${tabId}`)
  }

  const submitProductIds = useMemo(
    () => (shouldGenerateCharts ? productsSelected : []),
    [shouldGenerateCharts, productsSelected]
  )
  const { loading, data, error } = useQuery(
    shouldGenerateCharts ? QUERY_SURVEY_STATS : QUERY_SURVEY,
    {
      variables: {
        surveyId,
        checkOrg: true,
        productIds: submitProductIds,
        questionFilters: finalQuestionsSelected,
        dateFilter: finalDateFilter
      },
      fetchPolicy: 'network-only'
    }
  )
  const statsResults = useSurveyStatsResult()
  const jobGroupId = getStatsJobGroupId(data)
  const jobIds = getStatsJobIds(data)
  const isOwner = useMemo(
    () => getOwner(data) === getAuthenticatedOrganization(),
    [data]
  )

  const cachedQuery = useMemo(
    () => ({
      query: QUERY_SURVEY_STATS,
      variables: {
        surveyId,
        checkOrg: true,
        productIds: productsSelected,
        questionFilters: finalQuestionsSelected,
        dateFilter: finalDateFilter
      }
    }),
    [surveyId, productsSelected, finalQuestionsSelected, finalDateFilter]
  )

  const openValidationOptionNotification = () => {
    const args = {
      message: t('charts.noValidResultsTitle'),
      description: (
        <div
          dangerouslySetInnerHTML={{
            __html: t('charts.noValidResults', { surveyId })
          }}
        />
      ),
      duration: 0
    }
    notification.open(args)
  }

  const areStatsEmpty = () => {
    const stats = getStats(data)
    let isEmpty = true
    let hasStats = false

    stats.tabs.forEach(tab =>
      tab.charts.forEach(chart => {
        if (!chart.series && !chart.results) {
          return
        }

        setIsLoaded(false)
        hasStats = true

        const chartData = chart.series
          ? chart.series
          : chart.results
          ? chart.results.data
            ? chart.results.data
            : chart.results
          : []

        if (!chartData.length) {
          return
        }

        chartData.forEach(result => {
          if (result.value) {
            isEmpty = false
          }
        })
      })
    )

    return isEmpty && hasStats
  }

  useEffect(() => {
    if (
      isLoaded &&
      data.survey &&
      data.survey.stats &&
      data.survey.stats.tabs &&
      areStatsEmpty() &&
      data.survey.validatedData
    ) {
      openValidationOptionNotification()
    }
  })

  useEffect(() => {
    if (jobGroupId) {
      statsResults.startAutoRefresh(jobGroupId, jobIds, cachedQuery)
    }

    return () => {
      statsResults.stopAutoRefresh()
    }
  }, [statsResults, jobGroupId, jobIds, cachedQuery])

  const statsApi = useStatistic({
    cachedQuery,
    jobGroupId,
    productIds: productsSelected,
    questionFilters: finalQuestionsSelected,
    dateFilter: finalDateFilter
  })

  if (error && error.message.includes(ERRORS.NOT_AUTH_SURVEY)) {
    history.push('/operator/not-authorized')
  }

  const applyFilters = async () => {
    if (hasProductSelection && !shouldGenerateCharts) {
      return displayInfoPopup(
        t('containers.surveyStats.otherFilters.selectProductsFirst', {
          buttonText: t('containers.surveyStats.productsTab.buttonText')
        })
      )
    }
    setShouldGenerateCharts(true)
  }

  const saveChartsSettings = useMutation(SAVE_CHARTS_SETTINGS)
  const {
    data: { getChartsSettings = {} } = {},
    loading: chartsLoading
  } = useQuery(CHARTS_SETTINGS_QUERY, {
    fetchPolicy: 'network-only',
    variables: {
      surveyId
    }
  })

  const [chartsSettings, setChartsSettings] = useState(false)

  const setDefaultChartSettings = () => {
    stats = getStats(data)
    if (stats) {
      const defaultSettings = getDefaultChartSettings(stats)

      setChartsSettings(defaultSettings)

      saveChartsSettings({
        variables: {
          surveyId: surveyId,
          chartsSettings: defaultSettings
        }
      })
    }
  }

  const createNewSettings = (chartId, newSettings, tag) => {
    let newChartsSettings = { ...chartsSettings }

    if (chartId && chartId !== 'global' && !tag) {
      newChartsSettings = {
        ...newChartsSettings,
        [chartId]: newSettings
      }
      return newChartsSettings
    }

    stats = getStats(data)
    stats.tabs.forEach(tab => {
      tab.charts.forEach(chart => {
        const availableSettings = getAvailableChartSettings({
          chartType: chart.chart_type,
          questionType: chart.question_type,
          hasProducts: chart.question_with_products
        })
        availableSettings.forEach(setting => {
          const newValue =
            tag === 'all'
              ? newChartsSettings['global'][setting.name]
              : newSettings[setting.name]
          if (setting.type === 'group-labels') {
            if (chart.question_id === chartId.split('-')[0]) {
              newChartsSettings[chartId][setting.name] = newValue
            }
            return
          }
          if (
            setting.type === 'dropdown' &&
            !setting.options.map(el => el.value).includes(newValue)
          ) {
            // New value is not allowed for this setting in this chart
            return
          }
          if (tag === 'all') {
            if (setting.name in newChartsSettings['global']) {
              newChartsSettings[`${chart.question}-${chart.chart_type}`][
                setting.name
              ] = newValue
            }
          } else if (chart.chart_type === tag) {
            if (setting.name in newSettings) {
              newChartsSettings[`${chart.question}-${chart.chart_type}`][
                setting.name
              ] = newValue
            }
          }
        })
      })
    })

    return newChartsSettings
  }

  const resetSimilarSettings = tag => {
    if (!tag || tag === 'global') return
    const newChartsSettings = clone(chartsSettings)
    stats = getStats(data)
    stats.tabs.forEach(tab => {
      tab.charts.forEach(chart => {
        if (chart.chart_type === tag) {
          newChartsSettings[
            `${chart.question}-${chart.chart_type}`
          ] = getDefaultChartSettingsByChart(chart)
        }
      })
    })
    setChartsSettings(newChartsSettings)
    saveChartsSettings({
      variables: {
        surveyId: surveyId,
        chartsSettings: newChartsSettings
      }
    })
  }

  const saveSettings = (chartId, newSettings, tag) => {
    try {
      let newChartsSettings = createNewSettings(chartId, newSettings, tag)

      setChartsSettings(clone(newChartsSettings))
      saveChartsSettings({
        variables: {
          surveyId: surveyId,
          chartsSettings: newChartsSettings
        }
      })
    } catch (error) {
      message.error(t('settings.charts.error'))
    }
  }

  let tabPages = []
  let contents = []
  let stats = []

  if (data.survey) {
    if (
      ((data.survey.screeningQuestions &&
        data.survey.screeningQuestions.length) ||
        data.survey.screeners) &&
      !questionList
    ) {
      let filteredQuestions = data.survey.screeningQuestions
      if (data.survey.screeners && data.survey.screeners.length) {
        data.survey.screeners.forEach(screener => {
          if (screener.screeningQuestions) {
            filteredQuestions = filteredQuestions.concat(
              screener.screeningQuestions
            )
          }
        })
      }
      if (data.survey.compulsory && data.survey.compulsory.length) {
        data.survey.compulsory.forEach(singleCompulsory => {
          if (singleCompulsory.screeningQuestions) {
            filteredQuestions = filteredQuestions.concat(
              singleCompulsory.screeningQuestions
            )
          }
        })
      }
      setQuestionList(
        filteredQuestions.filter(question => {
          return STATS_FILTER_ACCEPTED_TYPES.indexOf(question.type) >= 0
        })
      )
    }
    if (
      data.survey.products &&
      data.survey.products.length >= data.survey.maxProductStatCount
    ) {
      !hasProductSelection && setHasProductSelection(true)
      tabPages = [
        {
          name: t('containers.surveyStats.productsTab.title'),
          path: 'select-products'
        }
      ]
      contents = [
        <StatsProductSection
          products={data.survey.products}
          selectedProducts={productsSelected}
          toggleProductSeletion={productId => {
            productsSelected.indexOf(productId) >= 0
              ? setProductsSelected(
                  remove(
                    productsSelected.indexOf(productId),
                    1,
                    productsSelected
                  )
                )
              : setProductsSelected(append(productId, productsSelected))
          }}
        />
      ]
    } else {
      if (!shouldGenerateCharts) {
        setShouldGenerateCharts(true)
      }
    }
  }

  useEffect(() => {
    if (loading || error) {
      return
    }

    const activeTabPage = tabPages.find(
      tabPage => tabPage.path === tabId.current
    )
    if (!activeTabPage && tabPages.length > 0 && tabId.current !== 'settings') {
      history.push(`/operator/stats/${surveyId}/${tabPages[0].path}`)
    }
  }, [
    loading,
    error,
    tabPages,
    surveyId,
    tabId.current,
    history,
    stats,
    validationSchema
  ])

  useEffect(() => {
    if (!chartsLoading && !loading) {
      // Chart settings from graphQL
      if (getChartsSettings.chartsSettings) {
        const settings = getChartsSettings.chartsSettings
        const stats = getStats(data)
        if (stats) {
          const defaultSettings = getDefaultChartSettings(stats)
          // Add missing setting or remove obsolete
          Object.keys(defaultSettings).forEach(settingKey => {
            if (settingKey === 'global') {
              return
            }
            if (settings[settingKey]) {
              Object.keys(defaultSettings[settingKey]).forEach(setting => {
                if (settings[settingKey][setting] === undefined) {
                  // Add default if setting missing
                  settings[settingKey][setting] =
                    defaultSettings[settingKey][setting]
                }
              })
            } else {
              // Chart settings missing
              settings[settingKey] = defaultSettings[settingKey]
            }
          })
        }
        setChartsSettings(settings)
      } else {
        setDefaultChartSettings()
      }
    }
  }, [chartsLoading, loading, statsApi.isStatisticsLoading])

  const handleEditOperatorLayout = () => {
    history.push(`/edit-operator-pdf/${surveyId}/${jobGroupId}`)
  }
  const handleEditTasterLayout = () => {
    history.push(`/edit-taster-pdf/${surveyId}/${jobGroupId}`)
  }

  if (error) {
    logger.error(error)
    return (
      <OperatorPage>
        <OperatorPageContent>
          <Alert
            type='error'
            message={t('containers.surveyStats.fetchError.message')}
            description={t('containers.surveyStats.fetchError.description')}
          />
        </OperatorPageContent>
      </OperatorPage>
    )
  }

  if (loading || (!chartsSettings && shouldGenerateCharts)) {
    return <LoadingModal visible />
  }

  if (shouldGenerateCharts) {
    stats = getStats(data)

    tabPages =
      stats &&
      validationSchema.isValidSync(stats) &&
      chartUtils.getTabPages(stats)
    contents =
      stats &&
      validationSchema.isValidSync(stats) &&
      chartUtils.getChartTabsContent(stats).map((el, ind) => {
        el.map((chart, chart_i) =>
          Object.assign(chart, { chart_id: ind + '_' + chart_i })
        )
        return (
          <Charts
            charts={el}
            chartsSettings={chartsSettings || {}}
            saveChartsSettings={saveSettings}
            resetSimilarSettings={resetSimilarSettings}
            isOwner={isOwner}
            statsApi={statsApi}
            t={t}
            surveyId={surveyId}
          />
        )
      })
  }

  if (tabPages.length === 0) {
    return <EmptyState />
  }

  const isBusy =
    stats &&
    (stats.tabs || []).some(tab =>
      (tab.charts || []).some(chart => chart.loading)
    )

  const showConfirmationModal = chartsSettings => {
    AlertModal({
      title: t('settings.charts.alertModalTitle'),
      description: t('settings.charts.alertModalDescription'),
      okText: t('settings.charts.setGlobal'),
      handleOk: () => {
        saveSettings('global', chartsSettings, 'all')
        history.push(`/operator/stats/${surveyId}/${tabPages[0].path}`)
      },
      handleCancel: () => {}
    })
  }

  const showResetConfirmationModal = chartsSettings => {
    AlertModal({
      title: t('settings.charts.alertModalResetTitle'),
      description: t('settings.charts.alertModalResetDescription'),
      okText: t('settings.charts.resetDefaults'),
      handleOk: () => {
        setDefaultChartSettings()
        history.push(`/operator/stats/${surveyId}/${tabPages[0].path}`)
      },
      handleCancel: () => {}
    })
  }

  return (
    <OperatorPage>
      <OperatorPageContent>
        {jobGroupId && (
          <React.Fragment>
            <GeneratePdf
              surveyId={surveyId}
              jobGroupId={jobGroupId}
              disabled={isBusy}
            />{' '}
            {!isAnalytics && isOwner && (
              <Button
                type='primary'
                disabled={isBusy}
                onClick={handleEditOperatorLayout}
              >
                {t('containers.surveyStats.editOperatorLayout')}
              </Button>
            )}{' '}
            {!isAnalytics &&  isOwner && data && data.survey && data.survey.showGeneratePdf && (
              <Button
                type='primary'
                disabled={isBusy}
                onClick={handleEditTasterLayout}
              >
                {t('containers.surveyStats.editTasterLayout')}
              </Button>
            )}
            <GeneratePpt
              disabled={isBusy}
              surveyId={surveyId}
              jobGroupId={jobGroupId}
              isImpersonating={isImpersonating}
            />
          </React.Fragment>
        )}

        {TabHorizontal(
          [...tabPages],
          contents,
          handleTabChange,
          tabId.current,
          !isAnalytics && isOwner && shouldGenerateCharts && (
            <Button
              type='secondary'
              size='default'
              onClick={() => {
                handleTabChange('settings')
              }}
            >
              <Icon type='setting' />
            </Button>
          )
        )}
        {tabId.current === 'settings' && (
          <ChartsRow>
            <Card
              title='Settings'
              style={{ minWidth: '930px', margin: '15px auto', padding: 0 }}
              bodyStyle={{ padding: 0 }}
              actions={[
                <TooltipWrapper helperText={t('tooltips.stats.reset')}>
                  <Button
                    key='1'
                    onClick={() => {
                      showResetConfirmationModal()
                    }}
                  >
                    {t('settings.charts.resetDefaults')}
                  </Button>
                </TooltipWrapper>,
                <TooltipWrapper helperText={t('tooltips.stats.setGlobal')}>
                  <Button
                    key='2'
                    type='primary'
                    onClick={() => {
                      showConfirmationModal()
                    }}
                  >
                    {t('settings.charts.setGlobal')}
                  </Button>
                </TooltipWrapper>
              ]}
            >
              <StatsOptionsForm
                settings={(chartsSettings && chartsSettings.global) || {}}
                setSettings={newSettings => {
                  setChartsSettings({
                    ...chartsSettings,
                    global: { ...chartsSettings.global, ...newSettings }
                  })
                }}
                chartType='all'
              />
            </Card>
          </ChartsRow>
        )}
      </OperatorPageContent>

      {(!isAnalytics || hasProductSelection) && (
        <StatsFilterModal
          modalHeaderTitle={t('containers.surveyStats.otherFilters.title')}
          questionList={questionList}
          finalQuestionsSelected={finalQuestionsSelected}
          finalDateFilter={finalDateFilter}
          resetText={t('containers.surveyStats.otherFilters.resetText')}
          onReset={() => {
            setFinalQuestionsSelected([])
            setFinalDateFilter([])
            applyFilters()
          }}
          submitText={t('containers.surveyStats.otherFilters.filterText')}
          onSubmit={(questionsSelected, dateFilter) => {
            setFinalQuestionsSelected(questionsSelected)
            setFinalDateFilter(dateFilter)
            applyFilters()
          }}
          hasProductSelection={hasProductSelection}
          shouldGenerateCharts={shouldGenerateCharts}
          setShouldGenerateCharts={setShouldGenerateCharts}
          productsSelected={productsSelected}
          data={data}
        />
      )}
    </OperatorPage>
  )
}

const QUERY_SURVEY = gql`
  query survey($surveyId: ID!, $checkOrg: Boolean) {
    survey(id: $surveyId, checkOrg: $checkOrg) {
      id
      name
      maxProductStatCount
      validatedData
      owner
      products {
        id
        name
        photo
      }
      screeningQuestions {
        id
        prompt
        chartTopic
        chartTitle
        type
        options {
          label
          value
        }
      }
      screeners {
        id
        screeningQuestions {
          id
          prompt
          chartTopic
          chartTitle
          type
          options {
            label
            value
          }
        }
      }
      compulsory {
        id
        screeningQuestions {
          id
          prompt
          chartTopic
          chartTitle
          type
          options {
            label
            value
          }
        }
      }
    }
  }
`

const QUERY_SURVEY_STATS = gql`
  query surveyStats(
    $surveyId: ID!
    $productIds: [String!]
    $questionFilters: [Object!]
    $dateFilter: [Date!]
    $checkOrg: Boolean
  ) {
    survey(id: $surveyId, checkOrg: $checkOrg) {
      id
      owner
      validatedData
      showGeneratePdf
      stats(
        productIds: $productIds
        questionFilters: $questionFilters
        dateFilter: $dateFilter
      )
    }
  }
`

const CHARTS_SETTINGS_QUERY = gql`
  query getChartsSettings($surveyId: ID!) {
    getChartsSettings(surveyId: $surveyId)
  }
`

const SAVE_CHARTS_SETTINGS = gql`
  mutation saveChartsSettings($surveyId: ID!, $chartsSettings: JSON) {
    saveChartsSettings(surveyId: $surveyId, chartsSettings: $chartsSettings)
  }
`

export default props => (
  <ErrorBoundary>
    <SurveyStats {...props} />
  </ErrorBoundary>
)
