import React from 'react'
import TermsAndPrivacyFooterComponent from '../../components/TermsAndPrivacyFooter'

const TermsAndPrivacyFooter = ({ history, isSurvey, showMessage = false }) => {
  return <TermsAndPrivacyFooterComponent isSurvey={isSurvey} showMessage={showMessage} />
}

export default TermsAndPrivacyFooter
