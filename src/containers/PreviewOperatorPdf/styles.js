import styled from 'styled-components'
import { fixChartPrint } from '../PreviewPdf/styles'

export const OperatorPdfContainer = styled.div`
  position: relative;
  ${fixChartPrint}
`

export const FooterNote = styled.div`
  position: absolute;
  bottom: 20px;
`
