import React, { useEffect, useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import Page from './Page'
import useChartResults from '../../hooks/useChartResults'
import useSortChartResults from '../../hooks/useSortChartResults'
import { getColWidth, getRowHeight, PAPER_SIZES } from '../../utils/paperSizes'
import useRenderingState from '../../hooks/useRenderingState'
import { OperatorPdfContainer } from './styles'
import { ChartContextProvider } from '../../contexts/ChartContext'
import usePreviewPdf from '../PreviewPdf/usePreviewPdf'

const BACKEND_LAYOUT =
  localStorage.getItem('pdf-layout') &&
  JSON.parse(localStorage.getItem('pdf-layout'))

const BACKEND_TOTAL_RESPONDENTS = parseInt(
  localStorage.getItem('total-respondents'),
  10
)

const PreviewOperatorPdf = ({
  savedLayout = BACKEND_LAYOUT,
  jobGroupId,
  surveyId
}) => {
  let {
    loading,
    survey,
    stats,
    settings,
    operatorLayout: defaultLayout
  } = useChartResults({
    jobGroupId,
    surveyId
  })
  const { sortedStats } = useSortChartResults({
    surveyId,
    stats
  })
  if (sortedStats && sortedStats.length && savedLayout === defaultLayout) {
    stats = sortedStats
  }
  const { rendering, showRendering } = useRenderingState()
  const { t } = useTranslation()
  const { layout, pages, applyLayout } = usePreviewPdf(
    survey,
    settings,
    'operator'
  )
  const paperSize = (layout && layout.paperSize) || 'A4'
  const paperSizeMeta = PAPER_SIZES[paperSize]
  const zoom = paperSizeMeta.printZoom
  const colWidth = getColWidth(paperSizeMeta) * zoom
  const rowHeight = getRowHeight(paperSizeMeta) * zoom
  const footerNote = useMemo(() => {
    if (survey && survey.pdfFooterSettings && survey.pdfFooterSettings.active) {
      const totalRespondents = !isNaN(BACKEND_TOTAL_RESPONDENTS)
        ? BACKEND_TOTAL_RESPONDENTS
        : stats &&
          stats[0] &&
          stats[0].result &&
          stats[0].result.totalRespondents

      return t('pdfLayoutEditor.footerNoteTemplate', {
        note: survey.pdfFooterSettings.footerNote,
        count: totalRespondents
      })
    }

    return null
  }, [stats, survey, t])

  useEffect(() => {
    if (!loading) {
      const layout = savedLayout || defaultLayout

      applyLayout(stats, layout, footerNote)
      showRendering(1000)
    }
  }, [
    applyLayout,
    defaultLayout,
    footerNote,
    loading,
    savedLayout,
    showRendering,
    stats
  ])

  window.__PAGE_HEIGHT = Math.ceil(rowHeight * paperSizeMeta.rows)
  return (
    <ChartContextProvider showAll printView>
      <OperatorPdfContainer>
        {rendering && <div id='loading'>loading</div>}
        {pages.map((page, index) => (
          <Page
            key={`page-${index}`}
            paperSize={paperSize}
            colWidth={colWidth}
            rowHeight={rowHeight * 0.995}
            zoom={zoom}
            layout={page}
          />
        ))}
      </OperatorPdfContainer>
    </ChartContextProvider>
  )
}

export default PreviewOperatorPdf
