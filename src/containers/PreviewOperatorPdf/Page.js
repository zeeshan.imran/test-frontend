import React, { useEffect } from 'react'
import ReactGridLayout from 'react-grid-layout'
import { usePdfLayout } from '../../hooks/usePdfLayout'
import { PAPER_SIZES } from '../../utils/paperSizes'

const Page = ({ paperSize, colWidth, rowHeight, zoom, layout }) => {
  const { fixLayout } = usePdfLayout()

  useEffect(() => {
    fixLayout(layout, layout, layout, paperSize, zoom)
  }, [layout, paperSize, zoom, fixLayout])

  const paperSizeMeta = PAPER_SIZES[paperSize]

  return (
    <div
      style={{
        minWidth: paperSizeMeta.cols * colWidth,
        maxWidth: paperSizeMeta.cols * colWidth,
        maxHeight: rowHeight * paperSizeMeta.rows * 0.9975,
        minHeight: rowHeight * paperSizeMeta.rows * 0.9975,
        pageBreakAfter: 'always'
      }}
    >
      <ReactGridLayout
        layout={layout}
        cols={paperSizeMeta.cols}
        width={paperSizeMeta.cols * colWidth}
        rowHeight={rowHeight * 0.995}
        margin={[0, 0]}
        containerPadding={[0, 0]}
        verticalCompact={false}
        preventCollision
        isDraggable={false}
        isResizable={false}
      >
        {layout.map(item => (
          <div key={item.i} id={`pdf-${item.i}`}>
            {item.content}
          </div>
        ))}
      </ReactGridLayout>
    </div>
  )
}

export default Page
