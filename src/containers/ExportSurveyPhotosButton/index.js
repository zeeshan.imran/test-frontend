import React, { useState } from 'react'
import { useMutation } from 'react-apollo-hooks'
import { withRouter } from 'react-router-dom'
import gql from 'graphql-tag'
import ExportSurveyButtonComponent from '../../components/ExportSurveyButton'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import { useTranslation } from 'react-i18next'
import ExportSurveyModal from '../ExportSurveyModal'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import getErrorMessage from '../../utils/getErrorMessage'

const ExportSurveyPhotosButton = ({ surveyId, surveyName, onMenuItemClick }) => {
  const { t } = useTranslation()
  const sendReport = useMutation(MUTATION_SEND_REPORT)
  const [modalVisible, setModalVisible] = useState(false)

  const handleSubmit = async userId => {
    try {
      setModalVisible(false)
      await sendReport({
        variables: {
          surveyId,
          userId
        }
      })
      displaySuccessMessage(t(`containers.exportSurveyButton.emailSend`))
    } catch (ex) {
      displayErrorPopup(getErrorMessage(ex, 'E_EXPORT_DEFAULT'))
    }
  }

  return (
    <React.Fragment>
      <ExportSurveyButtonComponent
        onClick={() => {
          onMenuItemClick()
          setModalVisible(true)
        }}
        icon='picture'
        tKey='exportSurveyPhotosButton'
      />
      {modalVisible && (
        <ExportSurveyModal
          tKey='exportSurveyPhotosButton'
          surveyName={surveyName}
          onCancel={() => setModalVisible(false)}
          onSubmit={handleSubmit}
        />
      )}
    </React.Fragment>
  )
}

const MUTATION_SEND_REPORT = gql`
  mutation sendSurveyPhotos($surveyId: ID!, $userId: ID!) {
    sendSurveyPhotos(surveyId: $surveyId, userId: $userId)
  }
`

export default withRouter(ExportSurveyPhotosButton)
