import React from 'react'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import Text from '../../components/Text'

import { PHOTO_VALIDATION_PAGE_PARAMS } from '../../utils/Constants'

const PhotosSurveyButton = ({ surveyId, history, onMenuItemClick }) => {
  const { t } = useTranslation()
  return (
    <Text
      onClick={() => {
        onMenuItemClick()
        history.push(`/operator/survey/photos/${surveyId}?${PHOTO_VALIDATION_PAGE_PARAMS}`)
      }}
    >
      {t('components.dasboard.survey.actions.photoValidation')}
    </Text>
  )
}
export default withRouter(PhotosSurveyButton)
