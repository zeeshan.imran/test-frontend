import styled from 'styled-components'
import Text from '../../components/Text'
import colors from '../../utils/Colors'
import { family } from '../../utils/Fonts'

export const Container = styled.div`
  background-color: ${colors.WHITE};
  padding: 2.4rem 3.2rem 3.2rem;
`

export const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 6.4rem;
`
export const BodyContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 6.4rem;
`

export const Title = styled(Text)`
  font-family: ${family.primaryBold};
  font-size: 2rem;
  color: rgba(0, 0, 0, 0.85);
  display: flex;
`

export const ContentText = styled(Text)`
  font-family: ${family.primaryLight};
  font-size: 2rem;
`