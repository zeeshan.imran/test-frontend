import React from 'react'
import { mount } from 'enzyme'
import BreadcrumbFolder from '.'
import gql from 'graphql-tag'
import { Router } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'

import wait from '../../utils/testUtils/waait'

const GET_FOLDER_PATH = gql`
  query getFolderPath($folderId: ID) {
    getFolderPath(folderId: $folderId) {
      pathId
      pathName
    }
  }
`

describe('BreadcrumbFolder', () => {
  let testRender
  let client
  let history

  beforeEach(() => {
    client = createApolloMockClient()
    client.cache.writeQuery({
      query: GET_FOLDER_PATH,
      variables: {
        folderId: 'breadcrumb01'
      },
      data: {
        getFolderPath: {
          pathName: ['Breadcrumb 01'],
          pathId: ['breadcrumb01']
        }
      }
    })
    history = {
      push: jest.fn(),
      goBack: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render BreadcrumbFolder', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <BreadcrumbFolder />
        </Router>
      </ApolloProvider>
    )
    expect(testRender).toMatchSnapshot()
  })
})
