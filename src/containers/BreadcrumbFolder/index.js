import React from 'react'
import { withRouter } from 'react-router-dom'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import Presentation from '../../components/BreadcrumbFolder'

const GET_FOLDER_PATH = gql`
  query getFolderPath($folderId: ID) {
    getFolderPath(folderId: $folderId) {
      pathId
      pathName
    }
  }
`
const defaultFolderPath = {
  pathId: [],
  pathName: [],
}

const BreadcrumbFolder = ({ history, folderId }) => {
  const {
    data: { getFolderPath: folderPath = defaultFolderPath } = {}
  } = useQuery(GET_FOLDER_PATH, { variables: { folderId: folderId } })

  const onClickBreadcrumb = (nextFolderId) => {
    const url = nextFolderId ? `surveys?folderId=${nextFolderId}` : `surveys`
    history.push(url)
  }

  return (
    <Presentation folderPath={folderPath} onClickBreadcrumb={onClickBreadcrumb} />
  )
}

export default withRouter(BreadcrumbFolder)
