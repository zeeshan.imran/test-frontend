import { useState, useCallback } from 'react'

const useLayoutState = () => {
  const [layout, setLayout] = useState({
    // data: will store in papes
    pageItems: [],
    gridLayout: [],
    totalPages: 0,
    // flags
    paperSize: 'A4',
    logoPlacement: 'no',
    pageInfoPlacement: 'no',
    footerNoteAllPages: false,
    deletedItems: []
  })

  return {
    layout,
    updateLayout: useCallback(
      newProps => {
        setLayout({ ...layout, ...newProps })
      },
      [layout]
    )
  }
}

export default useLayoutState
