import { range } from 'ramda'
import { PAPER_SIZES } from '../../utils/paperSizes'
import { calculateHeaderFooterLayout } from '../../utils/pdfUtils'

const calculatePagesLayout = (footerNote, layout) => {
  const { totalPages, paperSize } = layout
  const paperSizeMeta = PAPER_SIZES[paperSize]

  return [
    ...calculateHeaderFooterLayout(footerNote, layout),
    ...range(0, totalPages).map(page => ({
      i: `end-of-page-${page}-${totalPages}`,
      x:
        paperSizeMeta.cols +
        page * (paperSizeMeta.cols + paperSizeMeta.colsBetweenPages),
      y: 0,
      w: paperSizeMeta.colsBetweenPages,
      h: paperSizeMeta.rows,
      static: true
    }))
  ]
}

export default calculatePagesLayout
