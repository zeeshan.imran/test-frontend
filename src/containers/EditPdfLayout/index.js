import React, { useCallback, useState, useEffect, useMemo } from 'react'
import { flatten, pipe, merge, pickBy, pick, range, equals, uniq } from 'ramda'
import { withRouter } from 'react-router-dom'
import { useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { useTranslation } from 'react-i18next'
import { message } from 'antd'
import 'react-grid-layout/css/styles.css'
import 'react-resizable/css/styles.css'
import EditPdfLayoutComponent from '../../components/EditPdfLayout'
import * as pdfUtils from '../../utils/pdfUtils'
import useChartResults from '../../hooks/useChartResults'
import useSortChartResults from '../../hooks/useSortChartResults'
import useRenderingState from '../../hooks/useRenderingState'
import createPageItems from './createPageItems'
import calculatePagesLayout from './calculatePagesLayout'
import { usePdfLayout } from '../../hooks/usePdfLayout'
import AlertModal from '../../components/AlertModal'
import useLayoutState from './useLayoutState'
import useBestZoom from './useBestZoom'
import { getColsPerPage, PAPER_SIZES } from '../../utils/paperSizes'
import serializeLayout from './serializeLayout'
import {
  PDF_LAYOUT_PROPS,
  PDF_LAYOUT_DEFAULT_PROPS
} from '../../utils/Constants'
import {
  isHeaderVisible,
  isFooterVisible
} from '../../utils/pdfUtils/headerFooterUtils'

const flatternPages = (pages, colsPerPage) =>
  pipe(
    pages =>
      pages.map((elementInPageLayouts, page) =>
        elementInPageLayouts.map(e => ({
          ...e,
          x: e.x + page * colsPerPage
        }))
      ),
    flatten
  )(pages)

const compareItems = (a, b) =>
  a.x > b.x || (a.x === b.x && a.y > b.y) ? 1 : -1

const isSimilar = (layout1, layout2) => {
  const isSameProps = PDF_LAYOUT_PROPS.every(name =>
    equals(layout1[name], layout2[name])
  )

  if (!isSameProps) {
    return false
  }

  if (layout1.pages.length !== layout2.pages.length) {
    return false
  }

  return layout1.pages.every((items_1, index) => {
    const items_2 = [...layout2.pages[index]].sort(compareItems)
    if ((items_1 || []).length !== (items_2 || []).length) {
      return false
    }

    return [...items_1].sort(compareItems).every((item_1, index) => {
      const item_2 = items_2[index]

      return ['i', 'x', 'y', 'w', 'h'].every(
        name => item_1[name] === item_2[name]
      )
    })
  })
}

/*
Noted:
  pageItems: element for each page
  elementItems: chart elements
 */
const EditPdfLayout = ({ history, templateName, jobGroupId, surveyId }) => {
  let {
    survey,
    stats: allStats,
    settings,
    operatorLayout,
    tasterLayout,
    loading,
    reloadChartResults
  } = useChartResults({
    surveyId,
    jobGroupId
  })
  const { sortedStats } = useSortChartResults({
    surveyId,
    stats: allStats
  })
  if (sortedStats && sortedStats.length) {
    allStats = sortedStats
  }

  const stats = useMemo(
    () =>
      templateName === 'operator'
        ? allStats
        : pdfUtils.filterTasterStats(allStats),
    [allStats, templateName]
  )
  const { t } = useTranslation()
  const loadedLayout = useMemo(
    () => (templateName === 'operator' ? operatorLayout : tasterLayout),
    [templateName, operatorLayout, tasterLayout]
  )
  const getDownloadLink = useMutation(GET_DOWNLOAD_LINK)
  const saveSettings = useMutation(SAVE_SETTINGS)
  const { rendering, showRendering } = useRenderingState()
  const { layout, updateLayout } = useLayoutState()
  const [ready, setReady] = useState(false)
  const [showLayoutBorder, setShowLayoutBorder] = useState(true)
  const [userZoom, setUserZoom] = useState(0.5)

  const { zoom, colWidth, rowHeight } = useBestZoom(layout.paperSize, userZoom)
  const [elementItems, setElementItems] = useState([])
  const colsPerPage = getColsPerPage(PAPER_SIZES[layout.paperSize] || 'A4')

  const footerNote = useMemo(() => {
    if (survey && survey.pdfFooterSettings && survey.pdfFooterSettings.active) {
      if (templateName !== 'operator') {
        return null
      }

      const totalRespondents =
        stats && stats[0] && stats[0].result && stats[0].result.totalRespondents

      return t('pdfLayoutEditor.footerNoteTemplate', {
        note: survey.pdfFooterSettings.footerNote,
        count: totalRespondents
      })
    }

    return null
  }, [stats, survey, t, templateName])

  const { fixLayout, fitGridItem } = usePdfLayout()

  const getDefaultLayoutState = useCallback(() => {
    const paperSize = 'A4'

    return {
      ...PDF_LAYOUT_DEFAULT_PROPS,
      paperSize,
      pages: pdfUtils.calculateElementsLayout(
        paperSize,
        settings,
        stats,
        templateName
      )
    }
  }, [settings, stats, templateName])

  const getLayoutState = useCallback(
    loadedLayout => {
      if (!loadedLayout) {
        return getDefaultLayoutState()
      }

      const layoutState = merge(
        getDefaultLayoutState(),
        pickBy(value => value !== null && value !== undefined)({
          pages: loadedLayout.pages,
          ...pick(PDF_LAYOUT_PROPS)(loadedLayout)
        })
      )

      return layoutState
    },
    [getDefaultLayoutState]
  )

  const applyLayoutState = useCallback(
    (elementItems, { pages, paperSize, deletedItems, ...layoutProps }) => {
      const totalPages = pages.length
      const pageItems = createPageItems(t, footerNote, {
        totalPages,
        ...layoutProps
      })
      const pagesLayout = calculatePagesLayout(footerNote, {
        paperSize,
        totalPages,
        ...layoutProps
      })
      const elementsLayout = flatternPages(pages, colsPerPage)
      const gridLayout = [...pagesLayout, ...elementsLayout]

      range(0, totalPages).forEach(page =>
        pdfUtils.adjustTopBottomMargin(
          paperSize,
          elementsLayout,
          page,
          isHeaderVisible(layoutProps)(page) ? 2 : 1,
          isFooterVisible(footerNote, {
            totalPages,
            ...layoutProps
          })(page)
            ? 2
            : 1
        )
      )

      updateLayout({
        totalPages,
        paperSize,
        pageItems,
        gridLayout,
        deletedItems,
        ...layoutProps
      })

      fixLayout(elementItems, pageItems, gridLayout, paperSize, zoom)
    },
    [colsPerPage, fixLayout, footerNote, t, updateLayout, zoom]
  )

  useEffect(() => {
    if (!ready && !loading) {
      const elementItems = pdfUtils.createElementItems(
        survey.name,
        stats,
        settings,
        templateName
      )
      setElementItems(elementItems)

      const layout =
        loadedLayout && loadedLayout.pages
          ? getLayoutState(loadedLayout)
          : getDefaultLayoutState()

      const pageContains = item => itemsInPage =>
        itemsInPage.some(i => i.i === item.i)

      const notVisibleItems = elementItems
        .filter(item => !layout.pages.some(pageContains(item)))
        .map(item => item.i)

      layout.deletedItems = uniq([...layout.deletedItems, ...notVisibleItems])

      applyLayoutState(elementItems, layout)

      setReady(true)
    }
  }, [
    applyLayoutState,
    getDefaultLayoutState,
    getLayoutState,
    loadedLayout,
    loading,
    ready,
    settings,
    stats,
    templateName,
    survey
  ])

  useEffect(
    () => {
      fixLayout(
        elementItems,
        layout.pageItems,
        layout.gridLayout,
        layout.paperSize,
        zoom
      )
      showRendering(1000)
    },
    // show loading layout when zoom, elementItems change
    // layout is excluded because we don't show loading after users drag elements
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [zoom, showRendering, fixLayout, elementItems]
  )

  useEffect(() => {
    if (!loading && !ready) {
      showRendering(1000)
    }
  }, [loading, ready, showRendering])

  const handleLayoutPropChange = useCallback(
    newProps => {
      const newLayout = {
        ...layout,
        ...newProps
      }
      updateLayout(newProps)

      range(0, layout.totalPages).forEach(page => {
        const isHeaderVisibleOld = isHeaderVisible(layout)(page)
        if (isHeaderVisibleOld) {
          const isHeaderVisibleNew = isHeaderVisible(newLayout)(page)
          const isHeaderFromShowToHide =
            isHeaderVisibleOld && !isHeaderVisibleNew
          if (isHeaderFromShowToHide) {
            pdfUtils.moveElementsDown(newLayout, page, 1)
          }
        }
      })

      setTimeout(() => {
        showRendering(50)
        setTimeout(() => {
          applyLayoutState(
            elementItems,
            serializeLayout(newLayout, elementItems)
          )
        }, 0)
      }, 0)
    },
    [applyLayoutState, elementItems, layout, showRendering, updateLayout]
  )

  const handleDownload = useCallback(async () => {
    try {
      const {
        data: { downloadFile }
      } = await getDownloadLink({
        variables: {
          surveyId,
          jobGroupId,
          templateName,
          layout: serializeLayout(layout, elementItems)
        }
      })

      const url = `${process.env.REACT_APP_BACKEND_API_URL}/pdf/${downloadFile}`
      window.open(url, '_self')
    } catch (ex) {
      message.error(t('pdfLayoutEditor.messages.downloadError'))
    }
  }, [
    elementItems,
    getDownloadLink,
    jobGroupId,
    layout,
    surveyId,
    t,
    templateName
  ])

  const handleTemplateSave = useCallback(async () => {
    try {
      const targetProp =
        templateName === 'operator' ? 'operatorPdfLayout' : 'tasterPdfLayout'
      await saveSettings({
        variables: {
          surveyId,
          [targetProp]: serializeLayout(layout, elementItems)
        }
      })
      await reloadChartResults()
      setReady(false)
      message.success(t('pdfLayoutEditor.messages.saveSuccess'))
    } catch (ex) {
      message.error(t('pdfLayoutEditor.messages.saveError'))
    }
  }, [
    elementItems,
    layout,
    reloadChartResults,
    saveSettings,
    surveyId,
    t,
    templateName
  ])

  const handleClose = useCallback(async () => {
    const newData = serializeLayout(layout, elementItems)
    let initData = []
    if (!loadedLayout) {
      initData = getDefaultLayoutState()
      const layoutProps = pick(PDF_LAYOUT_PROPS)(initData)
      const totalPages = initData.pages.length
      range(0, initData.pages.length).forEach(page => {
        pdfUtils.adjustTopBottomMargin(
          initData.paperSize,
          initData.pages[page],
          0,
          isHeaderVisible(layoutProps)(page) ? 2 : 1,
          isFooterVisible(footerNote, {
            totalPages,
            ...layoutProps
          })(page)
            ? 2
            : 1
        )
      })
    } else {
      initData = loadedLayout
    }

    if (!isSimilar(initData, newData)) {
      AlertModal({
        title: t('pdfLayoutEditor.discard.title'),
        description: t('pdfLayoutEditor.discard.description'),
        okText: t('pdfLayoutEditor.discard.ok'),
        handleOk: async () => {
          history.goBack()
        },
        handleCancel: () => {}
      })
    } else {
      history.goBack()
    }
  }, [elementItems, footerNote, getDefaultLayoutState, history, layout, loadedLayout, t])

  const handleDeleteItem = useCallback(
    itemToDelete => {
      updateLayout({
        deletedItems: [...layout.deletedItems, itemToDelete]
      })
    },
    [updateLayout, layout.deletedItems]
  )

  const handleAddPage = useCallback(
    page => {
      const newGridLayout = layout.gridLayout.map(gridItem => {
        if (gridItem.x > (page + 1) * colsPerPage) {
          return { ...gridItem, x: gridItem.x + colsPerPage }
        }
        return gridItem
      })

      showRendering(50)
      const newTotalPages = layout.totalPages + 1
      const newLayout = {
        ...layout,
        totalPages: newTotalPages
      }
      const pageItems = createPageItems(t, footerNote, newLayout)
      const gridLayout = [
        ...calculatePagesLayout(footerNote, newLayout),
        ...newGridLayout
      ]
      setTimeout(() => {
        updateLayout({
          totalPages: newTotalPages,
          pageItems,
          gridLayout
        })
        fixLayout(elementItems, pageItems, gridLayout, layout.paperSize, zoom)
      }, 0)
    },
    [
      layout,
      showRendering,
      t,
      footerNote,
      colsPerPage,
      updateLayout,
      fixLayout,
      elementItems,
      zoom
    ]
  )

  const handleDeletePage = useCallback(
    page => {
      const newGridLayout = layout.gridLayout.map(gridItem => {
        if (gridItem.x > page * colsPerPage) {
          return { ...gridItem, x: gridItem.x - colsPerPage }
        }
        return gridItem
      })

      showRendering(50)
      const newTotalPages = layout.totalPages - 1
      const newLayout = {
        ...layout,
        totalPages: newTotalPages
      }
      const pageItems = createPageItems(t, footerNote, newLayout)
      const gridLayout = [
        ...calculatePagesLayout(footerNote, newLayout),
        ...newGridLayout
      ]
      setTimeout(() => {
        updateLayout({
          totalPages: newTotalPages,
          pageItems,
          gridLayout
        })
        fixLayout(elementItems, pageItems, gridLayout, layout.paperSize, zoom)
      }, 0)
    },
    [
      layout,
      showRendering,
      t,
      footerNote,
      colsPerPage,
      updateLayout,
      fixLayout,
      elementItems,
      zoom
    ]
  )

  const handleLayoutChange = useCallback(
    layout => {
      updateLayout({
        gridLayout: layout
      })
    },
    [updateLayout]
  )

  const handleLayoutResizeStop = useCallback(
    gridItem => {
      const elementItem = elementItems.find(item => item.i === gridItem.i)
      if (elementItem) {
        fitGridItem(elementItem, gridItem, { colWidth, rowHeight })
      }
    },
    [colWidth, elementItems, fitGridItem, rowHeight]
  )

  const handleLayoutReset = useCallback(() => {
    AlertModal({
      title: t('pdfLayoutEditor.reset.title'),
      description: t('pdfLayoutEditor.reset.description'),
      okText: t('pdfLayoutEditor.reset.ok'),
      handleOk: async () => {
        setTimeout(() => {
          applyLayoutState(elementItems, getDefaultLayoutState())
        }, 0)
      },
      handleCancel: () => {}
    })
  }, [applyLayoutState, elementItems, getDefaultLayoutState, t])

  return (
    <EditPdfLayoutComponent
      loading={loading}
      rendering={rendering}
      ready={ready}
      //
      templateName={templateName}
      colWidth={colWidth}
      rowHeight={rowHeight * 0.995}
      zoom={userZoom}
      onZoom={setUserZoom}
      showLayoutBorder={showLayoutBorder}
      onShowLayoutBorderChange={setShowLayoutBorder}
      onDownload={handleDownload}
      onTemplateSave={handleTemplateSave}
      onClose={handleClose}
      //
      footerNote={footerNote}
      layout={layout}
      elementItems={elementItems}
      onItemDelete={handleDeleteItem}
      onDeletePage={handleDeletePage}
      onAddPage={handleAddPage}
      onLayoutResizeStop={handleLayoutResizeStop}
      onLayoutChange={handleLayoutChange}
      onLayoutReset={handleLayoutReset}
      onLayoutPropChange={handleLayoutPropChange}
    />
  )
}

const GET_DOWNLOAD_LINK = gql`
  mutation getDownloadLink(
    $surveyId: ID!
    $jobGroupId: ID!
    $templateName: TemplateName!
    $layout: JSON
  ) {
    downloadFile: getDownloadLink(
      surveyId: $surveyId
      jobGroupId: $jobGroupId
      templateName: $templateName
      layout: $layout
    )
  }
`

const SAVE_SETTINGS = gql`
  mutation saveChartsSettings(
    $surveyId: ID
    $operatorPdfLayout: JSON
    $tasterPdfLayout: JSON
  ) {
    saveChartsSettings(
      surveyId: $surveyId
      operatorPdfLayout: $operatorPdfLayout
      tasterPdfLayout: $tasterPdfLayout
    )
  }
`

export default withRouter(EditPdfLayout)
