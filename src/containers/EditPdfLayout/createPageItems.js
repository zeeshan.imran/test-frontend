import React from 'react'
import { range } from 'ramda'
import { Typography, Button, Icon, Tooltip } from 'antd'
import { PageInfo } from '../../components/EditPdfLayout/styles'
import { createHeaderFooterItems } from '../../utils/pdfUtils'

const createPageController = (t, page, pageCount) => ({
  i: `end-of-page-${page}-${pageCount}`,
  type: 'page-control',
  page,
  content: ({ canDelete, onAddPage, onDeletePage }) => (
    <PageInfo>
      <Typography.Text type='secondary'>
        {t(`pdfLayoutEditor.pageText`, { page: page + 1, total: pageCount })}
      </Typography.Text>
      <Tooltip title={t('pdfLayoutEditor.addPage.tooltip')}>
        <Button
          shape='circle'
          type='primary'
          size='small'
          onClick={() => onAddPage(page)}
        >
          <Icon type='file' />
        </Button>
      </Tooltip>
      <Tooltip
        title={
          canDelete
            ? t('pdfLayoutEditor.removePage.tooltip')
            : t('pdfLayoutEditor.removePage.disabledTooltip')
        }
      >
        <Button
          shape='circle'
          type='danger'
          size='small'
          disabled={!canDelete}
          onClick={() => onDeletePage(page)}
        >
          <Icon type='delete' />
        </Button>
      </Tooltip>
    </PageInfo>
  )
})
const createPageItems = (t, footerNote, layout) => {
  const { totalPages } = layout
  return [
    ...createHeaderFooterItems(footerNote, layout),
    ...range(0, totalPages).map(page =>
      createPageController(t, page, totalPages)
    )
  ]
}

export default createPageItems
