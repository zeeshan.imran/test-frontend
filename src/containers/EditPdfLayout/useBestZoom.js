import { useMemo } from 'react'
import { getColWidth, getRowHeight, PAPER_SIZES } from '../../utils/paperSizes'

const useBestZoom = (paperSize, userZoom) => {
  const paperSizeInfo = PAPER_SIZES[paperSize || 'A4']
  const zoom = useMemo(() => {
    const colWidth = getColWidth(paperSizeInfo) * userZoom
    return (userZoom * Math.round(colWidth)) / colWidth
  }, [paperSizeInfo, userZoom])
  const colWidth = Math.round(getColWidth(paperSizeInfo) * zoom)
  const rowHeight = getRowHeight(paperSizeInfo) * zoom

  return { zoom, colWidth, rowHeight }
}

export default useBestZoom