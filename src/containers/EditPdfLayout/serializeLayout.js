import { range, pick } from 'ramda'
import { PAPER_SIZES, getColsPerPage } from '../../utils/paperSizes'
import { getPageOfItem } from '../../utils/pdfUtils'
import { PDF_LAYOUT_PROPS } from '../../utils/Constants'

const serializeLayout = (layout, elementItems) => {
  const { gridLayout, paperSize, totalPages } = layout
  const paperSizeMeta = PAPER_SIZES[paperSize]
  const colsPerPage = getColsPerPage(paperSizeMeta)

  const layoutsWithoutPages = gridLayout.filter(item =>
    elementItems.some(e => e.i === item.i)
  )

  const layoutByPages = range(0, totalPages).map(page => {
    return layoutsWithoutPages
      .filter(item => getPageOfItem(layout.paperSize, item) === page)
      .map(item => {
        const id = item.i
        const gridItem = elementItems.find(it => it.i === id)
        const ref = gridItem && gridItem.getRef && gridItem.getRef()

        return {
          ...item,
          x: item.x % colsPerPage,
          align: gridItem.align,
          ...(ref && ref.getChartSize ? ref.getChartSize() : {})
        }
      })
  })

  return {
    pages: layoutByPages,
    ...pick(PDF_LAYOUT_PROPS)(layout)
  }
}

export default serializeLayout
