import React, { useState } from 'react'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import FormComponent from '../../components/CompleteTasterProfileForm'

import {
  getAuthenticatedUser,
  setAuthenticatedUser
} from '../../utils/userAuthentication'
import { useSlaask } from '../../contexts/SlaaskContext'
import { useTranslation } from 'react-i18next';

const CompleteTasterProfileForm = ({ onCompleteProfile }) => {
  const { t } = useTranslation();
  const loggedInUser = getAuthenticatedUser()
  const { reloadSlaask } = useSlaask()
  const [submissionError, setSubmissionError] = useState('')
  const completeProfile = useMutation(COMPLETE_PROFILE, {
    update: async (_, result) => {
      const {
        data: { completeProfile: user }
      } = result
      await setAuthenticatedUser(user)
      await reloadSlaask()
      onCompleteProfile()
    }
  })
  const handleCompleteProfile = async values => {
    try {
      setSubmissionError('')
      if (loggedInUser && loggedInUser.id) {
        const yearAsNumber = Number(values.birthYear)
        await completeProfile({
          variables: {
            input: {
              ...values,
              fullName: values.fullName.trim(),
              birthYear: yearAsNumber,
              id: loggedInUser.id
            }
          }
        })
        return
      }
      setSubmissionError(t('containers.CompleteTasterProfileForm.submissionError'))
    } catch (error) {
      setSubmissionError(t('containers.CompleteTasterProfileForm.error'))
    }
  }

  return (
    <Formik
      validationSchema={Yup.object().shape({
        fullName: Yup.string().required(t('containers.CompleteTasterProfileForm.validation.fullnameRequire')),
        birthYear: Yup.number()
          .min(1900, t('containers.CompleteTasterProfileForm.validation.birthYearMin'))
          .max(new Date().getFullYear() - 17, t('containers.CompleteTasterProfileForm.validation.birthYearMax'))
          .required(t('containers.CompleteTasterProfileForm.validation.birthYearRequire')),
        gender: Yup.string().required(t('containers.CompleteTasterProfileForm.validation.genderRequire')),
        nationality: Yup.string().required(
          t('containers.CompleteTasterProfileForm.validation.nationalityRequire')
        )
      })}
      validateOnChange={false}
      validateOnBlur
      initialValues={{
        fullName: '',
        birthYear: '1980',
        gender: '',
        nationality: ''
      }}
      onSubmit={handleCompleteProfile}
      render={({
        values,
        errors,
        handleChange,
        setFieldValue,
        setFieldTouched,
        isSubmitting,
        ...formikProps
      }) => (
        <FormComponent
          {...formikProps}
          {...values}
          loading={isSubmitting}
          errors={{ ...errors, submission: submissionError }}
          fieldChangeHandler={field => e => {
            setFieldValue(
              field,
              e.target.value ? e.target.value.replace(/^\s+/g, '') : ''
            )
            setFieldTouched(field, true)
          }}
          dropdownChangeHandler={field => v => {
            setFieldValue(field, v)
            setFieldTouched(field, true)
          }}
        />
      )}
    />
  )
}

const COMPLETE_PROFILE = gql`
  mutation completeProfile($input: CompleteProfileInput) {
    completeProfile(input: $input) {
      id
      initialized
      emailAddress
      isTaster
      type
      fullName
      isVerified
      dateofbirth
      country
      state
      language
      gender
      paypalEmailAddress
      smokerKind
      foodAllergies
      marketResearchParticipation
      isProfileCompleted
      organization {
        name
        id
      }
    }
  }
`

export default CompleteTasterProfileForm
