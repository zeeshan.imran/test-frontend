import React from 'react'
import { mount } from 'enzyme'
import NewPasswordForm from '.'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { Router } from 'react-router-dom'
import sinon from 'sinon'
import gql from 'graphql-tag'
import { Formik } from 'formik'
import { getAuthenticatedUser } from '../../utils/userAuthentication'

jest.mock('../../utils/userAuthentication')
jest.mock('../../utils/displayErrorPopup')

const RESET_PASSWORD = gql`
  mutation resetPassword($input: ResetPasswordInput) {
    resetPassword(input: $input)
  }
`

describe('NewPasswordForm', () => { 
  let testRender
  let client
  let historyMock
  let spyChange

  let token
  let email
  let onSetNewPassword

  beforeEach(() => {
    token = 'token01token'
    email = 'test@test.com'
    onSetNewPassword = jest.fn()

    historyMock = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }

    client = createApolloMockClient()

    client.cache.writeQuery({
      query: RESET_PASSWORD,
      variables: {
        input: {
          email: email,
          token: token,
          password: 'test'
        }
      },

      data: {}
    })

    const stub = sinon.stub(client, 'mutate')
    spyChange = jest.fn()
    stub
      .withArgs(sinon.match({ mutation: RESET_PASSWORD }))
      .callsFake(spyChange)
    stub.callThrough()
  })

  afterEach(() => {
    testRender.unmount()
  })
  test('should render NewPasswordForm', () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={historyMock}>
          <NewPasswordForm
            token={token}
            email={email}
            onSetNewPassword={onSetNewPassword}
          />
        </Router>
      </ApolloProvider>
    )
    expect(testRender).toMatchSnapshot()

    const submitValue = {
      password: 'password'
    }

    const onSubmit = testRender
      .find(Formik)
      .first()
      .props('onSubmit')
    onSubmit.onSubmit(submitValue)
  })

  test('should fail new password', () => {
    getAuthenticatedUser.mockImplementation(() => ({ isSuperAdmin: false }))
    let requestAccountMock = {
      request: {
        query: RESET_PASSWORD,
        variables: { input: { surveyEnrollment: 'survey-enrollment-1' } }
      },
      result: () => {
        throw new Error('testing the error')
      }
    }
    client = createApolloMockClient({
      mocks: [requestAccountMock]
    })
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={historyMock}>
          <NewPasswordForm
            token={token}
            email={email}
            onSetNewPassword={onSetNewPassword}
          />
        </Router>
      </ApolloProvider>
    )

    const submitValue = {
      password: 'password'
    }

    const onSubmit = testRender
      .find(Formik)
      .first()
      .props('onSubmit')
    onSubmit.onSubmit(submitValue)
  })
})
