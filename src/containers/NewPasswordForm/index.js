import React, { useEffect, useState } from 'react'
import { Formik } from 'formik'
import * as yup from 'yup'
import NewPasswordFormComponent from '../../components/NewPasswordForm'
import { useMutation, useQuery } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { useTranslation } from 'react-i18next'

const loginLinks = {
  'chrHansen': 'https://chr-hansen.flavorwiki.com/',
  'bunge': 'https://blc.flavorwiki.com/',
  'default': 'https://app.flavorwiki.com/'
}
const link = loginLinks[process.env.REACT_APP_THEME] || loginLinks.default
const hours = process.env.REACT_APP_THEME === 'chrHansen' ? 48 : 24

const NewPasswordForm = ({ token, email, onSetNewPassword }) => {
  const [error, setError] = useState(false)
  const [message, setMessage] = useState('')
  const [submitted, setSubmitted] = useState(false)
  const [loading, setLoading] = useState(false)
  const resetPassword = useMutation(RESET_PASSWORD)
  const {
    data: { isResetPasswordTokenValid: isResetPasswordTokenValid = true } = {}
  } = useQuery(IS_RESET_PASSWORD_VALID, {
    variables: { input: { email, token } }
  })
  const { t } = useTranslation()

  useEffect(() => {
    if (
      !isResetPasswordTokenValid &&
      message !== t('containers.newPasswordF.expired', { hours, link })
    ) {
      setError(true)
      setMessage(t('containers.newPasswordF.expired', { hours, link }))
    }
  }, [isResetPasswordTokenValid])

  return (
    <Formik
      onSubmit={async ({ password }) => {
        setLoading(true)
        try {
          await resetPassword({
            variables: { input: { email, token, password } }
          })
          setLoading(false)
          setError(false)
          setMessage(t('containers.newPasswordF.success'))
          setSubmitted(true)
          onSetNewPassword()
        } catch (error) {
          setLoading(false)
          setError(true)
          setMessage(
            t(
              error.message.includes('expired')
                ? 'containers.newPasswordF.expired'
                : 'containers.newPasswordF.error',
              { hours, link }
            )
          )
        }
      }}
      validationSchema={yup.object().shape({
        password: yup
          .string()
          .min(8, t('containers.newPasswordF.validation.passwordMin'))
          .required(t('containers.newPasswordF.validation.passwordRequire')),
        confirmPassword: yup
          .string()
          .oneOf(
            [yup.ref('password')],
            t('containers.newPasswordF.validation.confirmMatch')
          )
          .required(t('containers.newPasswordF.validation.confirmRequire'))
      })}
      render={({ values, ...formikProps }) => (
        <NewPasswordFormComponent
          {...values}
          {...formikProps}
          loading={loading}
          submitted={submitted}
          infoMessage={message}
          errorSubmitting={error}
          disabled={!isResetPasswordTokenValid}
          link={link}
          hours={hours}
        />
      )}
    />
  )
}

const RESET_PASSWORD = gql`
  mutation resetPassword($input: ResetPasswordInput) {
    resetPassword(input: $input)
  }
`

const IS_RESET_PASSWORD_VALID = gql`
  query isResetPasswordTokenValid($input: isResetPasswordTokenValidInput) {
    isResetPasswordTokenValid(input: $input)
  }
`

export default NewPasswordForm
