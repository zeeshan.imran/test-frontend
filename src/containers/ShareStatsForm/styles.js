import styled from 'styled-components'
import Text from '../../components/Text'
import { Transfer } from 'antd'
import { family } from '../../utils/Fonts'
import colors from '../../utils/Colors'

export const StyledText = styled(Text)`
  font-size: 1.5rem;
  font-family: ${family.primaryBold};
  line-height: 3.2rem;
`

export const StyledTransfer = styled(Transfer)`
  .ant-checkbox-wrapper + span {
    max-width: 100%;
  }

  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner {
    border-color: ${colors.CHECKBOX_HOVER_COLOR};
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${colors.CHECKBOX_BACKGROUND_COLOR};
    border-color: ${colors.CHECKBOX_HOVER_COLOR};
  }
  .ant-checkbox-disabled.ant-checkbox-checked .ant-checkbox-inner::after {
    border-color: ${colors.CHECKBOX_HOVER_COLOR} !important;
  }
  .ant-checkbox-checked:after {
    border: 1px solid ${colors.CHECKBOX_DISABLE_COLOR};
  }
`
