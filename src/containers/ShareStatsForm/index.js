import React, { useState, useEffect } from 'react'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import { Formik } from 'formik'
import { Form, Row } from 'antd'
import { useTranslation } from 'react-i18next'
import { StyledText, StyledTransfer } from './styles'
import { pluck, equals } from 'ramda'
import { StyledCheckbox } from '../../components/StyledCheckBox'

const GET_SURVEY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      creator {
        id
      }
    }
  }
`

const ShareStatsForm = ({
  survey,
  surveyId,
  sharedStatsUsers,
  organizations,
  setNewShares,
  setIsOkDisabled,
  isOkDisabled,
  setIsScreenerShownInShare,
  isScreenerShownInShare,
  showScreenerOption,
  isCompulsorySurveyShownInShare,
  setIsCompulsorySurveyShownInShare,
  compulsorySurvey,
  includeCompulsorySurveyDataInStats
}) => {
  const { t } = useTranslation()
  const [userTransferProps, setUserTransferProps] = useState({})
  const membersByOrg = []

  const { data: { survey: { creator = {} } = {} } = {} } = useQuery(
    GET_SURVEY,
    { variables: { id: surveyId } }
  )

  const organizationDataMock = organizations.map(org => {
    let memberLength = 0
    org.members.map(member => {
      if (member.isSuperAdmin || member.type !== 'taster') {
        memberLength++

        membersByOrg.push({
          key: member.id,
          title: member.fullName || member.emailAddress,
          org: org.id
        })
      }
      return member
    })
    return {
      key: org.id,
      title: org.name.trim(),
      memberLength
    }
  })

  sharedStatsUsers =
    (sharedStatsUsers &&
      sharedStatsUsers.length &&
      sharedStatsUsers.map(ls => (ls.user ? ls.user : ls))) ||
    []

  const initialMemberDataSelected =
    sharedStatsUsers && sharedStatsUsers.length
      ? pluck('id', sharedStatsUsers)
      : []

  useEffect(() => {
    setNewShares(initialMemberDataSelected)
  }, [])

  const [organizationDataSelected, setOrganizationDataSelected] = useState(
    sharedStatsUsers && sharedStatsUsers.length
      ? pluck('id', pluck('organization', sharedStatsUsers))
      : []
  )

  const initialMemberDataMock =
    organizationDataSelected && organizationDataSelected.length
      ? membersByOrg.filter(member => {
          return (
            organizationDataSelected.indexOf(member.org) >= 0 &&
            (!creator || member.key !== creator.id)
          )
        })
      : []
  const [memberDataMock, setMemberDataMock] = useState(initialMemberDataMock)

  const [memberDataSelected, setMemberDataSelected] = useState(
    sharedStatsUsers && sharedStatsUsers.length
      ? pluck('id', sharedStatsUsers)
      : []
  )

  useEffect(() => {
    if (Object.keys(userTransferProps).length) {
      setUserTransferProps({})
    }
    if (!isOkDisabled) {
      if (
        equals(initialMemberDataSelected, memberDataSelected) &&
        equals(initialMemberDataMock, memberDataMock) &&
        isScreenerShownInShare === survey.isScreenerShownInShare &&
        isCompulsorySurveyShownInShare === survey.isCompulsorySurveyShownInShare
      ) {
        setIsOkDisabled(true)
      }
    } else if (
      !equals(initialMemberDataSelected, memberDataSelected) ||
      !equals(initialMemberDataMock, memberDataMock) ||
      isScreenerShownInShare !== survey.isScreenerShownInShare ||
      isCompulsorySurveyShownInShare !== survey.isCompulsorySurveyShownInShare
    ) {
      setIsOkDisabled(false)
    }
  })

  return (
    <Formik
      render={() => {
        return (
          <React.Fragment>
            <Row>
              <Form.Item>
                <StyledText>
                  {t(`components.shareStatsForm.organizations.label`)}
                </StyledText>
                <StyledTransfer
                  titles={[
                    t(`components.shareStatsForm.organizations.tabs.all`),
                    t(`components.shareStatsForm.organizations.tabs.selected`)
                  ]}
                  dataSource={organizationDataMock.filter(
                    org => org.memberLength > 0
                  )}
                  targetKeys={organizationDataSelected}
                  onChange={targetKeys => {
                    setOrganizationDataSelected(targetKeys)
                    const newSelectedMembers = []
                    const newMembersList = membersByOrg.filter(member => {
                      const inIndex =
                        targetKeys.includes(member.org) &&
                        (!creator || member.key !== creator.id)

                      if (inIndex && memberDataSelected.includes(member.key)) {
                        newSelectedMembers.push(member.key)
                      }

                      return inIndex
                    })

                    setMemberDataMock(newMembersList)
                    setMemberDataSelected(newSelectedMembers)
                    setNewShares(newSelectedMembers)
                    setUserTransferProps({
                      selectedKeys: []
                    })
                  }}
                  render={item => item.title}
                />
              </Form.Item>
            </Row>
            <Row>
              <Form.Item>
                <StyledText>
                  {t(`components.shareStatsForm.users.label`)}
                </StyledText>
                <StyledTransfer
                  titles={[
                    t(`components.shareStatsForm.users.tabs.all`),
                    t(`components.shareStatsForm.users.tabs.selected`)
                  ]}
                  dataSource={memberDataMock}
                  targetKeys={memberDataSelected}
                  onChange={targetKeys => {
                    setMemberDataSelected(targetKeys)
                    setNewShares(targetKeys)
                    setUserTransferProps({
                      selectedKeys: []
                    })
                  }}
                  render={item => item.title}
                  {...userTransferProps}
                />
              </Form.Item>
            </Row>
            {showScreenerOption && (
              <Row>
                <StyledCheckbox
                  checked={!isScreenerShownInShare}
                  onChange={e => {
                    setIsScreenerShownInShare(!e.target.checked)
                  }}
                >
                  {t('components.shareStatsForm.isScreenerHiddenInShare')}
                </StyledCheckbox>
              </Row>
            )}
            {!compulsorySurvey && includeCompulsorySurveyDataInStats && (
              <Row>
                <StyledCheckbox
                  checked={!isCompulsorySurveyShownInShare}
                  onChange={e => {
                    setIsCompulsorySurveyShownInShare(!e.target.checked)
                  }}
                >
                  Hide compulsory survey stats
                </StyledCheckbox>
              </Row>
            )}
          </React.Fragment>
        )
      }}
    />
  )
}

export default ShareStatsForm
