// import React from 'react'
// import { mount } from 'enzyme'
// import { Router } from 'react-router-dom'
// import { ApolloProvider } from 'react-apollo-hooks'
// import gql from 'graphql-tag'
// import ChooseProduct from './index'
// import ChooseProductComponent from '../../components/SurveyChooseProduct'
// import { createApolloMockClient } from '../../utils/createApolloMockClient'
// import { getAuthenticatedUser } from '../../utils/userAuthentication'
// import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
// import { SURVEY_QUERY } from '../../queries/Survey'
// import defaults from '../../defaults'
// import wait from '../../utils/testUtils/waait'
// import saveCurrentSurveyParticipation from '../../resolvers/saveCurrentSurveyParticipation'

// jest.mock('../../utils/userAuthentication')
// jest.mock('../../resolvers/saveCurrentSurveyParticipation')

// const SURVEY_ID = 'survey-1'
// const QUESTION_ID = 'question-1'

// const QUESTION_QUERY = gql`
//   query question($id: ID) {
//     question(id: $id) {
//       type
//     }
//   }
// `
// const BRANDS_QUERY = gql`
//   query brands($ids: [ID]) {
//     brands(ids: $ids) {
//       id
//       name
//       logo
//     }
//   }
// `

// const mockSurveyQuery = {
//   request: { query: SURVEY_QUERY, variables: { id: SURVEY_ID } },
//   result: {
//     data: {
//       survey: {
//         id: SURVEY_ID,
//         name: 'survey',
//         uniqueName: SURVEY_ID,
//         products: [
//           {
//             id: 'product-1',
//             name: 'product',
//             photo: 'photo',
//             brand: 'brand'
//           },
//           {
//             id: 'product-2',
//             name: 'product',
//             photo: 'photo',
//             brand: 'brand'
//           }
//         ],
//         minimumProducts: 1,
//         maximumProducts: 1,
//         surveyLanguage: 'en',
//         instructionsText: '',
//         instructionSteps: [],
//         exclusiveTasters: [],
//         screeningQuestions: [],
//         setupQuestions: [],
//         productsQuestions: [],
//         finishingQuestions: [],
//         thankYouText: '',
//         rejectionText: '',
//         screeningText: '',
//         customButtons: {
//           continue: 'continue',
//           start: 'start',
//           next: 'next',
//           skip: 'skip'
//         },
//         settings: {},
//         allowRetakes: false,
//         forcedAccount: false,
//         forcedAccountLocation: `start`,
//         authorizationType: 'public',
//         state: 'draft',
//         __typename: 'Survey'
//       }
//     }
//   }
// }

// const mockBrandsQuery = {
//   request: { query: BRANDS_QUERY, variables: { ids: ['brand'] } },
//   result: {
//     data: {
//       brands: [{ id: 'brand', name: 'brand', logo: 'logo' }]
//     }
//   }
// }

// const mockQuestionQuery = isRequired => ({
//   request: {
//     query: QUESTION_QUERY,
//     variables: { id: QUESTION_ID }
//   },
//   result: {
//     data: {
//       question: {
//         id: QUESTION_ID,
//         required: isRequired,
//         products: []
//       }
//     }
//   }
// })

describe('ChooseProduct', () => {
  test('TODO: update this test', () => {})
  // let testRender
  // let historyMock
  // let selectedProduct
  // let handleChange
  // let productsSkip
  // let mockClient

  // beforeEach(() => {
  //   historyMock = {
  //     push: jest.fn(),
  //     listen: () => {
  //       return jest.fn()
  //     },
  //     location: { pathname: '' }
  //   }

  //   mockClient = createApolloMockClient({
  //     mocks: [mockSurveyQuery, mockBrandsQuery, mockQuestionQuery(false)]
  //   })
  //   mockClient.cache.writeQuery({
  //     query: SURVEY_PARTICIPATION_QUERY,
  //     data: {
  //       currentSurveyParticipation: {
  //         ...defaults.currentSurveyParticipation,
  //         surveyId: SURVEY_ID,
  //         products: ['product-1', 'product-2'],
  //         selectedProducts: ['product-1'],
  //         answers: []
  //       }
  //     }
  //   })

  //   selectedProduct = 'product-1'
  //   handleChange = jest.fn()
  //   productsSkip = []
  // })

  // afterEach(() => {
  //   testRender.unmount()
  // })
  // test('should render ChooseProduct', async () => {
  //   getAuthenticatedUser.mockImplementation(() => ({
  //     isSuperAdmin: true
  //   }))

  //   testRender = mount(
  //     <ApolloProvider client={mockClient}>
  //       <Router history={historyMock}>
  //         <ChooseProduct
  //           selectedProduct={selectedProduct}
  //           handleChange={handleChange}
  //           productsSkip={productsSkip}
  //         />
  //       </Router>
  //     </ApolloProvider>
  //   )

  //   await wait(100)
  //   testRender.update()

  //   expect(testRender).toMatchSnapshot()
  // })

  // test('should render ChooseProduct onchange event', async () => {
  //   saveCurrentSurveyParticipation.mockImplementation(() => null)
  //   getAuthenticatedUser.mockImplementation(() => ({
  //     isSuperAdmin: true
  //   }))

  //   testRender = mount(
  //     <ApolloProvider client={mockClient}>
  //       <Router history={historyMock}>
  //         <ChooseProduct
  //           selectedProduct={selectedProduct}
  //           handleChange={handleChange}
  //           productsSkip={productsSkip}
  //         />
  //       </Router>
  //     </ApolloProvider>
  //   )

  //   await wait(100)
  //   testRender.update()

  //   expect(handleChange).toHaveBeenCalledWith(null, null)
  //   expect(saveCurrentSurveyParticipation).toHaveBeenCalledWith(
  //     expect.anything(),
  //     { selectedProduct: null },
  //     expect.anything(),
  //     expect.anything()
  //   )

  //   testRender
  //     .find(ChooseProductComponent)
  //     .props()
  //     .onChange('product-1')
  //   await wait(0)

  //   expect(handleChange).toHaveBeenCalledWith('product-1', null)
  //   expect(saveCurrentSurveyParticipation).toHaveBeenCalledWith(
  //     expect.anything(),
  //     { selectedProduct: 'product-1' },
  //     expect.anything(),
  //     expect.anything()
  //   )
  // })

  // test('skipTo specs', async () => {
  //   saveCurrentSurveyParticipation.mockImplementation(() => null)
  //   getAuthenticatedUser.mockImplementation(() => ({
  //     isSuperAdmin: true
  //   }))

  //   testRender = mount(
  //     <ApolloProvider client={mockClient}>
  //       <Router history={historyMock}>
  //         <ChooseProduct
  //           selectedProduct={selectedProduct}
  //           handleChange={handleChange}
  //           productsSkip={[
  //             {
  //               productIndex: 0,
  //               skipTo: 1
  //             }
  //           ]}
  //         />
  //       </Router>
  //     </ApolloProvider>
  //   )

  //   await wait(100)
  //   testRender.update()

  //   expect(handleChange).toHaveBeenCalledWith(null, null)
  //   expect(saveCurrentSurveyParticipation).toHaveBeenCalledWith(
  //     expect.anything(),
  //     { selectedProduct: null },
  //     expect.anything(),
  //     expect.anything()
  //   )

  //   testRender
  //     .find(ChooseProductComponent)
  //     .props()
  //     .onChange('product-1')
  //   await wait(0)

  //   expect(handleChange).toHaveBeenCalledWith('product-1', 1)
  //   expect(saveCurrentSurveyParticipation).toHaveBeenCalledWith(
  //     expect.anything(),
  //     { selectedProduct: 'product-1' },
  //     expect.anything(),
  //     expect.anything()
  //   )
  // })
})
