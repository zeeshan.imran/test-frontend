import React, { useState, useRef } from 'react'
import moment from 'moment'
import gql from 'graphql-tag'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { useMutation } from 'react-apollo-hooks'
import Text from '../../components/Text'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import { CSVLink } from 'react-csv'
import {
  curry,
  values,
  lift,
  evolve,
  add,
  reduce,
  map,
  groupBy,
  prop,
  head,
  tail
} from 'ramda'

const ExportProductIncentives = ({ survey, onMenuItemClick }) => {
  const { t } = useTranslation()
  const csvLinkRef = useRef()
  const getProductIncentivesMutation = useMutation(GET_SURVEY_SHARES)
  const [downloadData, setDownloadData] = useState([])
  const [filename, setFilename] = useState(
    `${survey.uniqueName}-paypal-incentives-${moment(new Date()).format(
      'YYYYMMDD-HHmmss'
    )}.csv`
  )

  const filterData = async () => {
    if (!survey.isPaypalSelected) {
      displayErrorPopup(
        t('containers.exportSharingContainer.popups.paypalDisabled')
      )
      return
    }

    const {
      data: { getProductIncentives }
    } = await getProductIncentivesMutation({
      variables: {
        surveyId: survey.id
      }
    })
    if (getProductIncentives.length) {
      downloadCSVFile(getProductIncentives, false) //Download sheet For Paypal
      const name = `internal-${filename}`
      setFilename(name)
      downloadCSVFile(getProductIncentives, true) //Download sheet For Internal Use
      setFilename(filename)
    } else {
      displayErrorPopup(t('containers.exportSharingContainer.popups.error'))
    }
    setDownloadData([])
  }

  const downloadCSVFile = async (getProductIncentives, internal = false) => {
    displaySuccessMessage(
      t('containers.exportSharingContainer.popups.downloading')
    )
    let finalData = []
    if (getProductIncentives.length) {
      finalData = getProductIncentives.map(productIncentive => {
        if (internal) {
          const internalDocument = {
            id: productIncentive.id,
            email: productIncentive.email,
            amount: productIncentive.amount,
            currency: productIncentive.currency,
            referral: productIncentive.referral,
            fullName: productIncentive.fullName
          }
          return {
            ...internalDocument,
            ...(Number(productIncentive.amountInDollar) > 0 && {
              amountInDollar: productIncentive.amountInDollar
            })
          }
        } else {
          const externalDocument = {
            email: productIncentive.email,
            amount: productIncentive.amount,
            currency: productIncentive.currency,
            fullName: productIncentive.fullName
          }
          return {
            ...externalDocument,
            ...(Number(productIncentive.amountInDollar) > 0 && {
              amountInDollar: productIncentive.amountInDollar
            })
          }
        }
      })

      if (!internal) {
        const sumBy = prop =>
          lift(
            reduce((current, val) =>
              evolve({ [prop]: add(val[prop]) }, current)
            )
          )(head, tail)

        const groupSumBy = curry((groupOn, sumOn, vals) =>
          values(map(sumBy(sumOn))(groupBy(prop(groupOn), vals)))
        )

        finalData = groupSumBy('email', 'amount', finalData)
      }

      setDownloadData(finalData)
      displaySuccessMessage(
        t('containers.exportSharingContainer.popups.downloaded')
      )
      // Trigger CSVLink automatically
      csvLinkRef.current.link.click({})
    } else {
      displayErrorPopup(t('containers.exportSharingContainer.popups.error'))
    }
  }

  return (
    <React.Fragment>
      <Text
        onClick={() => {
          onMenuItemClick()
          filterData()
        }}
      >
        {t('tooltips.exportProductIncentivesButton')}
      </Text>
      {downloadData.length ? (
        <CSVLink
          filename={filename}
          target='_self'
          ref={csvLinkRef}
          data={downloadData}
        />
      ) : null}
    </React.Fragment>
  )
}

export default withRouter(ExportProductIncentives)

const GET_SURVEY_SHARES = gql`
  mutation getProductIncentives($surveyId: ID!) {
    getProductIncentives(surveyId: $surveyId) {
      id
      fullName
      email
      amount
      currency
      referral
      amountInDollar
    }
  }
`
