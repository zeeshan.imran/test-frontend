import React, { useState } from 'react'
import { path, filter } from 'ramda'
import debounce from 'lodash.debounce'
import { useQuery } from 'react-apollo-hooks'
import ExportSurveyModalComponent from '../../components/ExportSurveyModal'
import {
  getImpersonateUserId,
  getAuthenticatedUser
} from '../../utils/userAuthentication'
import gql from 'graphql-tag'

const ExportSurveyModal = ({
  tKey,
  surveyName,
  onCancel,
  onSubmit,
  showScreenerOption,
  compulsorySurvey,
  includeCompulsorySurveyDataInStats
}) => {
  const authUser = getAuthenticatedUser()

  const [keyword, setKeyword] = useState('')
  const [saving, setSaving] = useState(false)
  const handleKeywordChange = debounce(setKeyword, 250)

  // Impersonating USER ID
  const isImpersonating = !!getImpersonateUserId()
 
  const { data, loading } = useQuery(QUERY_USERS, {
    fetchPolicy: 'cache-and-network',
    variables: {
      keyword: keyword || ''
    }
  })
  const users = path(['users'])(data) || []
  const filteredUsers = filter(u => u.id !== authUser.id, users)

  const handleSubmit = async values => {
    try {
      setSaving(true)
      await onSubmit(
        values.userId,
        values.includeScreener,
        values.dateFilter,
        values.includeCompulsorySurvey,
        values.reportList
      )
      setSaving(false)
    } catch (ex) {
      setSaving(false)
    }
  }

  return (
    <ExportSurveyModalComponent
      tKey={tKey}
      surveyName={surveyName}
      isImpersonating={isImpersonating}
      users={filteredUsers}
      loading={loading}
      saving={saving}
      onSearch={handleKeywordChange}
      showScreenerOption={showScreenerOption}
      onCancel={onCancel}
      onOk={handleSubmit}
      compulsorySurvey={compulsorySurvey}
      includeCompulsorySurveyDataInStats={includeCompulsorySurveyDataInStats}
    />
  )
}

const QUERY_USERS = gql`
  query users($keyword: String) {
    users(input: { keyword: $keyword, type: ["operator", "power-user"] }) {
      id
      fullName
      emailAddress
      type
    }
  }
`
export const QUERY_IMPERSONATE_USER = gql`
  query impersonateUser {
    adminUser: me(impersonate: false) {
      id
      fullName
      emailAddress
      isSuperAdmin
    }
  }
`

export default ExportSurveyModal
