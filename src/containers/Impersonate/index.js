import React, { useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom'
import qs from 'qs'
import {
  setImpersonateUserId,
  deleteImpersonateUserId,
  setAuthenticatedUser,
  setAuthenticatedOrganization
} from '../../utils/userAuthentication'
import LoadingModal from '../../components/LoadingModal'
import usePurgeCache from '../../hooks/usePurgeCache'
import gql from 'graphql-tag'
import { useApolloClient } from 'react-apollo-hooks'
import { useSlaask } from '../../contexts/SlaaskContext'

const Impersonate = ({ location, match }) => {
  const { impersonateUserId } = match.params
  const { reloadSlaask } = useSlaask()
  const [loading, setLoading] = useState(true)
  const parsedQuery = qs.parse(location.search.substring(1))
  const redirect = parsedQuery.redirect || '/operator/users'
  const purgeCache = usePurgeCache()
  const client = useApolloClient()

  useEffect(
    () => {
      async function enterImpersonate () {
        if (impersonateUserId) {
          setImpersonateUserId(impersonateUserId)
        } else {
          deleteImpersonateUserId()
        }

        await purgeCache()

        const {
          data: { me: user }
        } = await client.query({
          query: QUERY_ME,
          fetchPolicy: 'network-only'
        })

        setAuthenticatedOrganization(user.organization && user.organization.id)
        setAuthenticatedUser({
          ...user,
          fullName: user.fullName || user.emailAddress
        })
        await reloadSlaask()

        setLoading(false)
      }

      enterImpersonate()
    },
    [purgeCache, setLoading, impersonateUserId, client, reloadSlaask]
  )

  if (loading) {
    return <LoadingModal visible text='Impersonating' />
  }

  return <Redirect to={redirect} />
}

export const QUERY_ME = gql`
  query me {
    me {
      id
      initialized
      emailAddress
      type
      isSuperAdmin
      fullName
      country
      language
      isProfileCompleted
      isCompulsorySurveyTaken
      isDummyTaster
      organization {
        id
      }
    }
  }
`

export default Impersonate
