import React from 'react'
import { mount } from 'enzyme'
import wait from '../../utils/testUtils/waait'
import mockLocalStorage from '../../utils/testUtils/mockLocalStorage'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { Redirect } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo-hooks'
import usePurgeCache from '../../hooks/usePurgeCache'
import Impersonate, { QUERY_ME } from './index'
import LoadingModal from '../../components/LoadingModal'
import {
  FLAVORWIKI_IMPERSONATE,
  FLAVORWIKI_USER_DETAILS,
  getAuthenticatedOrganization
} from '../../utils/userAuthentication'
import { useSlaask } from '../../contexts/SlaaskContext'

jest.mock('../../hooks/usePurgeCache')
jest.mock('../../contexts/SlaaskContext')
jest.mock('react-router-dom')

const mockQueryMe = {
  FullInfo: {
    request: {
      query: QUERY_ME
    },
    result: {
      data: {
        me: {
          id: 'operator-1',
          initialized: true,
          emailAddress: 'operator-1@flavourwiki.com',
          type: 'operator',
          isSuperAdmin: false,
          fullName: 'Operator 1',
          country: 'US',
          language: 'en',
          isProfileCompleted: true,
          isCompulsorySurveyTaken: true,
          isDummyTaster: false,
          organization: {
            id: 'oragnization-1'
          }
        }
      }
    }
  },
  MissingFullName: {
    request: {
      query: QUERY_ME
    },
    result: {
      data: {
        me: {
          id: 'operator-1',
          initialized: true,
          emailAddress: 'operator-1@flavourwiki.com',
          type: 'operator',
          isSuperAdmin: false,
          fullName: null,
          country: 'US',
          language: 'en',
          isProfileCompleted: true,
          isCompulsorySurveyTaken: true,
          isDummyTaster: false,
          organization: {
            id: 'oragnization-1'
          }
        }
      }
    }
  }
}

describe('impersonate route', () => {
  let render
  let purgeCache
  let reloadSlaask

  beforeEach(() => {
    mockLocalStorage()
    purgeCache = jest.fn()
    usePurgeCache.mockImplementation(() => purgeCache)
    reloadSlaask = jest.fn()
    useSlaask.mockImplementation(() => ({ reloadSlaask: reloadSlaask }))
    Redirect.mockReset()
  })

  afterEach(() => {
    render.unmount()
  })

  test('should show loading before while cleaning cache', async () => {
    const client = createApolloMockClient({ mocks: [mockQueryMe.FullInfo] })
    render = mount(
      <ApolloProvider client={client}>
        <Impersonate
          match={{ params: { impersonateUserId: 'operator-1' } }}
          location={{ search: '' }}
        />
      </ApolloProvider>
    )

    expect(render.find(LoadingModal)).toHaveLength(1)
  })

  test('should call purge cache', async () => {
    const client = createApolloMockClient({ mocks: [mockQueryMe.FullInfo] })
    render = mount(
      <ApolloProvider client={client}>
        <Impersonate
          match={{ params: { impersonateUserId: 'operator-1' } }}
          location={{ search: '' }}
        />
      </ApolloProvider>
    )

    await wait(0)

    expect(purgeCache).toHaveBeenCalled()
  })

  test('should setAuthenticatedUser', async () => {
    const client = createApolloMockClient({ mocks: [mockQueryMe.FullInfo] })
    render = mount(
      <ApolloProvider client={client}>
        <Impersonate
          match={{ params: { impersonateUserId: 'operator-1' } }}
          location={{ search: '' }}
        />
      </ApolloProvider>
    )

    await wait(100)

    expect(Redirect).toHaveBeenCalledWith(
      { push: false, to: '/operator/users' },
      { router: undefined }
    )

    expect(window.localStorage.setItem).toHaveBeenCalledWith(
      FLAVORWIKI_USER_DETAILS,
      JSON.stringify(mockQueryMe.FullInfo.result.data.me)
    )
    expect(getAuthenticatedOrganization()).toBe('oragnization-1')
    expect(reloadSlaask).toHaveBeenCalled()
  })

  test('should redirect to / if the redirect is passed into component', async () => {
    const client = createApolloMockClient({ mocks: [mockQueryMe.FullInfo] })
    render = mount(
      <ApolloProvider client={client}>
        <Impersonate
          match={{ params: { impersonateUserId: 'operator-1' } }}
          location={{ search: '' }}
        />
      </ApolloProvider>
    )

    await wait(100)

    expect(Redirect).toHaveBeenCalledWith(
      { push: false, to: '/operator/users' },
      { router: undefined }
    )
    expect(reloadSlaask).toHaveBeenCalled()
  })

  test('should redirect to /operator/surveys if ?redirect=/operator/surveys is passed into query string', async () => {
    const client = createApolloMockClient({ mocks: [mockQueryMe.FullInfo] })
    render = mount(
      <ApolloProvider client={client}>
        <Impersonate
          match={{ params: { impersonateUserId: 'operator-1' } }}
          location={{ search: '?redirect=/operator/surveys' }}
        />
      </ApolloProvider>
    )

    await wait(100)

    expect(Redirect).toHaveBeenCalledWith(
      { push: false, to: '/operator/surveys' },
      { router: undefined }
    )
    expect(reloadSlaask).toHaveBeenCalled()
  })

  test('should delete impersonate id if impersonateUserId is not passed', async () => {
    const client = createApolloMockClient({
      mocks: [mockQueryMe.MissingFullName]
    })
    render = mount(
      <ApolloProvider client={client}>
        <Impersonate
          match={{ params: {} }}
          location={{ search: '?redirect=/operator/surveys' }}
        />
      </ApolloProvider>
    )

    await wait(100)

    expect(window.localStorage.removeItem).toHaveBeenCalledWith(
      FLAVORWIKI_IMPERSONATE
    )
    expect(reloadSlaask).toHaveBeenCalled()
  })
})
