import React, { useRef, useEffect } from 'react'
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo-hooks'
import SurveyQuestionLayoutComponent from '../../components/SurveyQuestionLayout'
import FillerLoader from '../../components/FillerLoader'
import { surveyBasicInfo } from '../../fragments/survey'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'

const SurveyQuestionLayout = ({
  questionId,
  surveyId,
  setIsTimerLocked,
  isTimerLocked,
  setTimeCompletedFor,
  timeCompletedFor,
  children
}) => {
  const { data, loading } = useQuery(SURVEY_QUERY, {
    variables: { id: surveyId }
  })
  
  const handleAdvance = useRef(useMutation(ADVANCE_IN_SURVEY))
  const { data: questionData, loading: questionLoading } = useQuery(
    QUESTION_QUERY,
    {
      variables: { id: questionId }
    }
  )

  let products = []
  if (data && data.survey && data.survey.products) {
    products = data.survey.products
  }

  const { data: currentSurveyParticipationData } = useQuery(
    SURVEY_PARTICIPATION_QUERY
  )
  const saveRewards = useMutation(SAVE_REWARDS)

  const {
    currentSurveyParticipation: { savedRewards = [], surveyEnrollmentId } = {}
  } = currentSurveyParticipationData || { currentSurveyParticipation: {} }

  const { type, displayOn } =
    questionData && questionData.question ? questionData.question : {}

  const hideProductPicture =
    [
      'info',
      'upload-picture',
      'choose-product',
      'show-product-screen'
    ].includes(type) || displayOn !== 'middle'

  const areRewardsSaved = useRef(false)
  const availableProducts = products.filter(product => product.isAvailable)
  useEffect(() => {
    if (availableProducts.length > 1) {
      areRewardsSaved.current = true
    } else if (
      surveyEnrollmentId &&
      !hideProductPicture && // the product must be visible
      !areRewardsSaved.current &&
      availableProducts.length === 1
    ) {
      areRewardsSaved.current = true
      if (savedRewards && savedRewards.length === 0) {
        let rewards = availableProducts.map(product => ({
          id: product.id,
          reward: product.reward
        }))
        saveRewards({
          variables: {
            rewards: rewards,
            surveyEnrollment: surveyEnrollmentId
          }
        })
      }
    }
  })

  if (loading || questionLoading) {
    return <FillerLoader fullScreen loading />
  }

  const {
    survey: { title }
  } = data

  return (
    <SurveyQuestionLayoutComponent
      survey={data && data.survey ? data.survey : {}}
      question={(questionData && questionData.question) || {}}
      title={title}
      children={children}
      currentSurveyParticipation={
        currentSurveyParticipationData &&
        currentSurveyParticipationData.currentSurveyParticipation
      }
      isTimerLocked={isTimerLocked}
      setIsTimerLocked={setIsTimerLocked}
      timeCompletedFor={timeCompletedFor}
      setTimeCompletedFor={setTimeCompletedFor}
      handleAdvance={handleAdvance}
    />
  )
}

export const SURVEY_QUERY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      ...surveyBasicInfo
    }
  }

  ${surveyBasicInfo}
`

export const QUESTION_QUERY = gql`
  query question($id: ID) {
    question(id: $id) {
      type
      displayOn
      displaySurveyName
      currentTime
      delayToNextQuestion
      extraDelayToNextQuestion
    }
  }
`

const SAVE_REWARDS = gql`
  mutation saveRewards($rewards: JSON, $surveyEnrollment: ID) {
    saveRewards(rewards: $rewards, surveyEnrollment: $surveyEnrollment)
  }
`
const ADVANCE_IN_SURVEY = gql`
  mutation advanceInSurvey($rejectUserEnrollment: Boolean) {
    advanceInSurvey(rejectUserEnrollment: $rejectUserEnrollment) @client
  }
`

export default SurveyQuestionLayout
