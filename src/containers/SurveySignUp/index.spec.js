import React from 'react'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import { create } from 'react-test-renderer'
import gql from 'graphql-tag'
import { surveyInfo } from '../../fragments/survey'
import SurveySignUp from './index'
import { Router } from 'react-router-dom'
import history from '../../history'
import '../../utils/internationalization/i18n'
import { displayDraftWarningPopup } from '../../utils/displayDraftWarningPopup'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import defaults from '../../defaults'
import { LOGIN_TO_SURVEY } from '../../mutations/loginToSurvey'
import { detect } from 'detect-browser'

jest.doMock('./index.js')
jest.mock('../../utils/displayDraftWarningPopup')

const mockSurveySignUp = surveyId => ({
  request: {
    query: LOGIN_TO_SURVEY,
    variables: {
      email: '',
      survey: surveyId,
      isUserLoggedIn: undefined,
      password: '',
      country: '',
      browserInfo: detect()
    }
  },
  result: {
    data: {
      loginToSurvey: {
        id: 'surveyEnrollmentId',
        paypalEmail: 'flawor-wiki@test.com',
        surveyId: surveyId,
        state: 'completed',
        lastAnsweredQuestion: {
          id: 'question-1'
        },
        selectedProducts: [
          {
            id: 'product-1',
            name: 'product name',
            brand: 'brand'
          }
        ]
      }
    }
  }
})

const mockActiveSurvey = {
  __typename: 'Survey',
  name: 'active survey',
  uniqueName: 'uniqueName',
  state: 'active',
  minimumProducts: 1,
  maximumProducts: 1,
  surveyLanguage: 'en',
  settings: {
    recaptcha: false
  },
  authorizationType: 'enrollment',
  id: `survey-1`,
  surveyEnrollment: 'survey-enrollment-1',
  context: {},
  value: {
    value: 'value-1'
  },
  range: {},
  options: {},
  pairsOptions: {},
  pairs: {},
  productsSkip: {},
  products: [],
  exclusiveTasters: [],
  allowRetakes: false,
  forcedAccount: false,
  forcedAccountLocation: `start`,
  isScreenerOnly: false,
  showGeneratePdf: false,
  linkedSurveys: [],
  referralAmount: 5,
  recaptcha: false,
  maxProductStatCount: 6,
  savedRewards: [],
  emailService: false,
  allowedDaysToFillTheTasting: 5,
  isPaypalSelected: false,
  isGiftCardSelected: false,
  customButtons: {
    __typename: 'CustomButtons',
    continue: 'Continue',
    start: 'Start',
    next: 'Next',
    skip: 'Skip'
  },
  startedAt: Date.now(),
  selectedProduct: 'prod-1',
  instructionSteps: [],
  instructionsText: '',
  thankYouText: '',
  rejectionText: '',
  screeningText: '',
  customizeSharingMessage: '',
  loginText: '',
  pauseText: '',
  screeningQuestions: {},
  setupQuestions: {},
  productsQuestions: {},
  finishingQuestions: {},
  paymentQuestions: {}
}

const mockDraftSurvey = {
  ...mockActiveSurvey,
  state: 'draft',
  id: 'survey-2',
  name: 'drafted survey'
}

describe('Test SurveySignUp', () => {
  it('test', () => {})
  beforeAll(() => {
    const storageMock = (function () {
      let store = {}
      return {
        getItem: function (key) {
          return store[key] || null
        },
        setItem: function (key, value) {
          store[key] = value.toString()
        },
        removeItem: function (key) {
          delete store[key]
        },
        clear: function () {
          store = {}
        }
      }
    })()

    Object.defineProperty(window, 'localStorage', {
      value: storageMock
    })
    Object.defineProperty(window, 'sessionStorage', {
      value: storageMock
    })
  })

  test('The active survey should not have a notification popup', () => {
    let mockClient = createApolloMockClient({
      mocks: [mockSurveySignUp('survey-1')]
    })
    mockClient.cache.writeQuery({
      query: gql`
        query survey($id: ID) {
          survey(id: "survey-1") {
            ...surveyInfo
          }
        }
        ${surveyInfo}
      `,
      data: {
        survey: {
          ...mockActiveSurvey
        }
      }
    })
    mockClient.cache.writeQuery({
      query: SURVEY_PARTICIPATION_QUERY,
      data: {
        currentSurveyParticipation: {
          ...defaults.currentSurveyParticipation,
          surveyId: 'survey-1',
          surveyEnrollmentId: 'survey-enrollment-1',
          canSkipCurrentQuestion: false,
          state: 'active',
          paypalEmail: '',
          selectedProduct: null,
          selectedProducts: [],
          currentSurveyStep: null,
          currentSurveySection: null,
          isCurrentAnswerValid: true,
          currentAnswerSkipTarget: null,
          answers: [],
          savedRewards: [],
          country: 'Germany',
          lastAnsweredQuestion: null,
          productRewardsRule: {
            active: false,
            min: 0,
            max: 0,
            percentage: 0
          }
        }
      }
    })
    const testRender = create(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <SurveySignUp
            match={{ url: `/survey/survey-1/` }}
            surveyId='survey-1'
            isTesting
          />
        </Router>
      </ApolloProvider>
    )
    expect(displayDraftWarningPopup).not.toHaveBeenCalled()
    testRender.unmount()
  })
})
