import React, { useRef, useState, useEffect } from 'react'
import gql from 'graphql-tag'
import { withRouter } from 'react-router-dom'

import { useMutation, useQuery } from 'react-apollo-hooks'
import SurveyLoginComponent from '../../components/SurveyLogin'
import { SURVEY_QUERY } from '../../queries/Survey'

import useCustomButton from '../../hooks/useCustomButton'
import useIsUserRegistered from '../../hooks/useIsUserRegistered'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import { getAuthenticatedUser } from '../../utils/userAuthentication'

const SurveySignUp = ({ surveyId, isTesting }) => {
  const handleAdvance = useRef(useMutation(ADVANCE_IN_SURVEY))
  const handleSubmitForcedSignUp = useMutation(SUBMIT_FORCED_SIGNUP)
  const isUserRegistered = useIsUserRegistered()
  const [requireRegistration, setRequireRegistration] = useState(false)
  const [checkLogin, setCheckLogin] = useState(false)
  const loggedInUser = getAuthenticatedUser()

  const { data: { survey = {} } = {} } = useQuery(SURVEY_QUERY, {
    variables: { id: surveyId },
    fetchPolicy: isTesting ? 'cache-first' : 'network-only'
  })

  const {
    data: {
      currentSurveyParticipation: {
        surveyEnrollmentId,
        currentSurveyStep,
        email: userEmail = ''
      }
    }
  } = useQuery(SURVEY_PARTICIPATION_QUERY)

  const checkUserLoggedIn = async () => {
    if (loggedInUser) {
      
      // So that this runs only once
      setCheckLogin(true)

      await handleSubmitForcedSignUp({
        variables: {
          input: {
            surveyEnrollment: surveyEnrollmentId,
            email: loggedInUser.emailAddress,
            password: '',
            country: ''
          }
        }
      })
      await handleAdvance.current({
        variables: { from: currentSurveyStep }
      })
    }
  }

  const { buttonLabel } = useCustomButton(surveyId, 'continue')

  const { title: surveyTitle, settings, loginText } = survey
  

  useEffect(() => {
    if (!checkLogin) {
      checkUserLoggedIn()
    }
  }, [loggedInUser])

  return (
    <SurveyLoginComponent
      authorizationType={`forced-email`}
      surveyTitle={surveyTitle}
      requireRecaptcha={settings && settings.recaptcha}
      buttonLabel={buttonLabel}
      loginText={loginText}
      requireRegistration
      defaultEmailValue={userEmail}
      emailDisabled={userEmail ? true : false}
      handleSubmit={async (email, password, country) => {
        if (!requireRegistration) {
          const userRegistered = await isUserRegistered({ email: email })
          if (!userRegistered) {
            setRequireRegistration(true)
            return
          }
        }
        await handleAdvance.current({
          variables: { from: currentSurveyStep }
        })
      }}
    />
  )
}
export default withRouter(SurveySignUp)

const SUBMIT_FORCED_SIGNUP = gql`
  mutation submitForcedSignUp($input: SubmitForcedSignUpInput!) {
    submitForcedSignUp(input: $input)
  }
`

const ADVANCE_IN_SURVEY = gql`
  mutation advanceInSurvey($from: ID, $skipLoop: Boolean) {
    advanceInSurvey(fromStep: $from, skipLoop: $skipLoop) @client
  }
`
