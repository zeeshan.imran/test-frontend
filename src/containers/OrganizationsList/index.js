import React, { useState, useMemo } from 'react'
import OrganizationsListComponent from '../../components/OrganizationsList'
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo-hooks'
import AlertModal from '../../components/AlertModal'
import { DEFAULT_N_ELEMENTS_PER_PAGE } from '../../utils/Constants'
import useSearch from '../../hooks/useSearch'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import { useTranslation } from 'react-i18next'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import getErrorMessage from '../../utils/getErrorMessage'

const OrganizationsListContainer = ({ history, location }) => {
  const {
    keyword,
    orderBy,
    orderDirection,
    page,
    handleKeywordChange,
    handleTableChange
  } = useSearch({
    location,
    history
  })
  const { t } = useTranslation()
  /*
   * Noted: editOrganizationId
   * - false: modal form closed
   * - true: modal form open with new organization
   * - string: model form open with the organization id
   */
  const [editOrganizationId, setEditOrganizationId] = useState(false)
  const [newOrganizationData, setnewOrganizationData] = useState({})
  const [modalType, setModalType] = useState('edit')
  const userTypes = ['operator', 'taster', 'analytics', 'power-user']

  const addOrganization = useMutation(ADD_ORGANIZATION)
  const editOrganization = useMutation(EDIT_ORGANIZATION)
  const deleteOrganization = useMutation(DELETE_ORGANIZATION)
  const unarchivedOrganization = useMutation(UNARCHIVED_ORGANIZATION)

  const isArchivedList = useMemo(() => {
    return location.pathname === '/operator/archived-organizations'
  }, [location])

  let organizationsList = useQuery(ORGANIZATIONS, {
    variables: {
      input: {
        keyword,
        skip: (page - 1) * DEFAULT_N_ELEMENTS_PER_PAGE,
        orderBy,
        orderDirection,
        type: userTypes,
        inactive: isArchivedList
      }
    },
    fetchPolicy: 'cache-and-network'
  })
  let organizationsCount = useQuery(COUNT_ORGANIZATIONS, {
    variables: {
      input: {
        type: userTypes,
        keyword,
        inactive: isArchivedList
      }
    },
    fetchPolicy: 'cache-and-network'
  })

  const handleOrganizationIdChange = isOpen => {
    if (isOpen !== true && isOpen !== false) {
      const currentOrganization = organizationsList.data.organizations.find(
        organization => organization.id === isOpen
      )
      setnewOrganizationData({
        name: currentOrganization.name,
        uniqueName: currentOrganization.uniqueName
      })
    } else {
      setnewOrganizationData({})
    }
    setEditOrganizationId(isOpen)
  }

  const handleOrganizationSubmit = async values => {
    if (editOrganizationId === false) {
      return
    }

    if (editOrganizationId === true) {
      try {
        await addOrganization({
          variables: {
            name: values.name,
            uniqueName: values.uniqueName
          }
        })
        organizationsCount.refetch()
        organizationsList.refetch()
        displaySuccessMessage(
          t('components.organizationForm.addedSuccessfully')
        )
        setEditOrganizationId(false)
      } catch (e) {
        displayErrorPopup(t('errors.E_ORG_UNIQUE'))
      }
    } else {
      try {
        await editOrganization({
          variables: {
            name: values.name,
            id: editOrganizationId
          }
        })
        displaySuccessMessage(
          t('components.organizationForm.updateSuccessfully')
        )
        setEditOrganizationId(false)
      } catch (e) {
        displayErrorPopup(getErrorMessage(e, 'ORG_DEFAULT_UPDATE'))
      }
    }
  }

  const handleDeleteOrganization = async id => {
    await deleteOrganization({ variables: { id: id } })
    displaySuccessMessage(t('components.organizationForm.deletedSuccessfully'))
    organizationsCount.refetch()
    organizationsList.refetch()
  }
  const handleDeleteOrganizationModal = id => {
    AlertModal({
      title: t('containers.organizationsList.alertModalTitle'),
      description: t('containers.organizationsList.alertModalDescription'),
      okText: t('containers.organizationsList.alertModalOkText'),
      handleOk: () => handleDeleteOrganization(id),
      handleCancel: () => { }
    })
  }

  const confirmUnarchivedOrganization = async id => {
    await unarchivedOrganization({ variables: { id: id } })
    displaySuccessMessage(t('unarchivedOrganization.success'))
    organizationsCount.refetch()
    organizationsList.refetch()
  }
  const onUnarchivedOrganization = id => {
    AlertModal({
      title: t('unarchivedOrganization.modalConfirmTitle'),
      description: t('unarchivedOrganization.modalConfirmContent'),
      okText: t('unarchivedOrganization.unarchived'),
      handleOk: () => confirmUnarchivedOrganization(id)
    })
  }

  return (
    <OrganizationsListComponent
      loading={organizationsList && organizationsList.loading}
      isArchivedList={isArchivedList}
      editOrganizationId={editOrganizationId}
      onEditOrganizationIdChange={handleOrganizationIdChange}
      organizations={
        organizationsList && organizationsList.data
          ? organizationsList.data.organizations
          : []
      }
      handleChangeSelection={() => { }}
      onOrganizationFormSubmit={handleOrganizationSubmit}
      handleRemove={handleDeleteOrganizationModal}
      newOrganizationData={newOrganizationData}
      page={page - 1}
      organizationsCount={
        organizationsCount &&
        organizationsCount.data &&
        organizationsCount.data.countOrganizations
      }
      onTableChange={handleTableChange}
      handleSearch={handleKeywordChange}
      searchBy={keyword}
      orderBy={orderBy}
      orderDirection={orderDirection}
      modalType={modalType}
      setModalType={setModalType}
      setOrganizationData={setnewOrganizationData}
      onUnarchivedOrganization={onUnarchivedOrganization}
    />
  )
}

export const COUNT_ORGANIZATIONS = gql`
  query countOrganizations($input: CountOrganizationsInput!) {
    countOrganizations(input: $input)
  }
`

export const ORGANIZATIONS = gql`
  query organizations($input: OrganizationsInput!) {
    organizations(input: $input) {
      id
      uniqueName
      name
      usersNumber
      createdBy {
        id
        fullName
        emailAddress
      }
      modifiedBy {
        id
        fullName
        emailAddress
      }
    }
  }
`

export const ADD_ORGANIZATION = gql`
  mutation addOrganization($name: String!, $uniqueName: String!) {
    addOrganization(name: $name, uniqueName: $uniqueName)
  }
`

export const EDIT_ORGANIZATION = gql`
  mutation editOrganization($name: String!, $id: ID!) {
    editOrganization(name: $name, id: $id) {
      name
      id
      usersNumber
      createdBy {
        id
        fullName
        emailAddress
      }
      modifiedBy {
        id
        fullName
        emailAddress
      }
    }
  }
`

const DELETE_ORGANIZATION = gql`
  mutation deleteOrganization($id: ID!) {
    deleteOrganization(id: $id)
  }
`

const UNARCHIVED_ORGANIZATION = gql`
  mutation unarchivedOrganization($id: ID!) {
    unarchivedOrganization(id: $id)
  }
`

export default OrganizationsListContainer
