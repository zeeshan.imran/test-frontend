import React from 'react'
import { withRouter } from 'react-router-dom'
import { useQuery } from 'react-apollo-hooks'
import LoadingModal from '../../components/LoadingModal'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import useToggle from '../../hooks/useToggle/index'
import useUpdateSurvey from '../../hooks/useUpdateSurvey'
import useConfirmQuestionReorder from '../../hooks/useConfirmQuestionReorder'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import TooltipWrapper from '../../components/TooltipWrapper'

import { ButtonWithMargin } from './styles'
import useSurveyValidate from '../../hooks/useSurveyValidate'
import { useTranslation } from 'react-i18next'
import settingsSchema from '../../validates/settings'
import { emailsValidationSchema } from '../../components/OperatorSurveyCreateEmailSettings'

const UpdateSurvey = ({ history, match }) => {
  const { t } = useTranslation()
  const confirmQuestionReorder = useConfirmQuestionReorder()
  const [publishing, togglePublishing] = useToggle(false)
  const {
    data: {
      surveyCreation: { products, basics, questions, mandatoryQuestions } = {
        basics: {}
      }
    }
  } = useQuery(SURVEY_CREATION)

  const {
    params: { surveyId }
  } = match
  const updateDraft = useUpdateSurvey({
    surveyId,
    successMessage: t('containers.updateSurvey.successMessage'),
    errorMessage: t('containers.updateSurvey.errorMessage')
  })
  const surveyValidate = useSurveyValidate()

  let submitDisabled = true
  if (basics.isScreenerOnly) {
    submitDisabled =
      !basics.uniqueName ||
      questions.length + mandatoryQuestions.length === 0 ||
      basics.linkedSurveys.length === 0
  } else if (basics.compulsorySurvey) {
    submitDisabled = !basics.uniqueName || questions.length === 0
  } else {
    submitDisabled =
      !basics.uniqueName ||
      products.length === 0 ||
      questions.length + mandatoryQuestions.length === 0 ||
      !settingsSchema.isValidSync(basics.autoAdvanceSettings) ||
      products.reduce((result, product) => result && !product.isAvailable, true)
  }

  const onUpdateDraft = async () => {
    try {
      emailsValidationSchema.validateSync(basics)
    } catch (ex) {
      history.push(`./emails`)
      return
    }

    const isAdvanceSettingsValid = await surveyValidate.validateAdvanceSettings()
    if (!isAdvanceSettingsValid) {
      history.push(`./settings`)
      await surveyValidate.showAdvanceSettingsErrors()
      return false
    }

    const isProductsValid = await surveyValidate.validateProducts()
    if (!isProductsValid) {
      history.push(`./products`)
      await surveyValidate.showProductsErrors()
      return
    }

    const {
      isValid: isQuestionsValid,
      invalidQuestion
    } = await surveyValidate.validateQuestions()

    if (!isQuestionsValid && invalidQuestion.displayOn === 'payments') {
      history.push(`./financial?section=${invalidQuestion.displayOn}`)
      await surveyValidate.showQuestionsErrors()
      return
    }

    if (!isQuestionsValid) {
      history.push(`./questions?section=${invalidQuestion.displayOn}`)
      await surveyValidate.showQuestionsErrors()
      return
    }

    const duplicateTitles = await surveyValidate.validateChartTitles()
    
    if(duplicateTitles.length > 0) {
      await surveyValidate.showChartTitlesErrors(duplicateTitles)
      return
    }

    const onConfirm = async () => {
      togglePublishing()

      try {
        await updateDraft()
        togglePublishing()
        history.push(`/operator/survey/stricted-edit/${surveyId}`)
      } catch (error) {
        togglePublishing()
        displayErrorPopup(error && error.message)
      }
    }

    confirmQuestionReorder(onConfirm)
  }

  return (
    <TooltipWrapper helperText={t('tooltips.updateSurvey')} placement='leftTop'>
      <ButtonWithMargin
        type='secondary'
        disabled={submitDisabled}
        size='default'
        onClick={onUpdateDraft}
      >
        {t('containers.updateSurvey.buttonText')}
      </ButtonWithMargin>
      <LoadingModal
        visible={publishing}
        text={t('containers.updateSurvey.updatingText')}
      />
    </TooltipWrapper>
  )
}

export default withRouter(UpdateSurvey)
