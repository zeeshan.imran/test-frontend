import React from 'react'
import { mount } from 'enzyme'
import AdditionalInformation from './index'
import '../../utils/internationalization/i18n'
import wait from '../../utils/testUtils/waait'
import ETHNICITIES_QUERY from '../../queries/ethnicities'
import SMOKER_KINDS_QUERY from '../../queries/smokerKinds'
import PRODUCT_CATEGORIES_QUERY from '../../queries/productCategories'
import DIETARY_PREFERENCES_QUERY from '../../queries/dietaryPreferences'
import { MockedProvider } from 'react-apollo/test-utils'

// jest.mock('../../components/AdditionalInformationForm')

describe('AdditionalInformation', () => {
  let testRender
  let user
  let mocks
  let handleUpdateTesterInfo
  let t

  function fillForm () {
    testRender
      .find({ id: 'numberOfChildren' })
      .first()
      .prop('onChange')({ target: { value: 2 } })

    testRender
      .find({ id: 'hasChildren' })
      .first()
      .prop('onChange')({ target: { value: 'Yes' } })

    testRender
      .find({ id: 'ethnicity' })
      .first()
      .prop('onChange')({ target: { value: 'ethnicity' } })

    testRender
      .find({ id: 'incomeRange' })
      .first()
      .prop('onChange')({ target: { value: '10000' } })

    testRender
      .find({ id: 'smokerKind' })
      .first()
      .prop('onChange')({ target: { value: 'smokerKind' } })

    testRender
      .find({ id: 'productTypes' })
      .first()
      .prop('onChange')({ target: { value: 'en' } })

    testRender
      .find({ id: 'dietaryRestrictions' })
      .first()
      .prop('onChange')({ target: { value: 'F' } })
  }

  beforeEach(() => {
    t = jest.fn(key => key)

    user = {
      id: 'user-1',
      firstname: 'Test',
      lastname: 'User',
      email: 'test@gmail.com',
      language: 'en',
      testerInfo: {
        ethnicityId: 'ethnicity-id',
        incomeRangeId: 'income-range-id',
        incomeRange: {
          id: 'income-range-id',
          currency: '$'
        },
        childrenAges: '10,15,30',
        smokerKindId: 'smoker-kind-id',
        preferredProductCategories: [1, 2],
        dietaryPreferenceId: 'dietary-preference-id'
      }
    }

    handleUpdateTesterInfo = {
      ok: jest.fn(() => ({
        data: { updateTesterInfo: { ok: true } }
      })),
      error: jest.fn(() => ({
        data: { updateTesterInfo: { error: 'ERROR on updateTesterInfo' } }
      })),
      throw: jest.fn(() => {
        throw 'Error'
      })
    }

    mocks = [
      {
        request: { query: ETHNICITIES_QUERY },
        result: { data: { ethnicities: [] } }
      },
      {
        request: { query: SMOKER_KINDS_QUERY },
        result: { data: { smokerKinds: [] } }
      },
      {
        request: { query: PRODUCT_CATEGORIES_QUERY },
        result: { data: { productCategories: [] } }
      },
      {
        request: { query: DIETARY_PREFERENCES_QUERY },
        result: { data: { dietaryPreferences: [] } }
      }
    ]
  })
  afterEach(() => {
    testRender.unmount()
  })

  test('should render AdditionalInformation', async () => {
    testRender = mount(
      <MockedProvider mocks={mocks} addTypename>
        <AdditionalInformation t={t} desktop={''} user={user} />
      </MockedProvider>
    )
    expect(testRender).toMatchSnapshot()
  })

  test('skip process when nothing is touched', () => {
    testRender = mount(
      <MockedProvider mocks={mocks} addTypename>
        <AdditionalInformation t={t} desktop={''} user={user} />
      </MockedProvider>
    )
    testRender.find('form').simulate('submit')
  })

  test('should call handleUpdateUser, handleUpdateTesterInfo when the user touched fields', async () => {
    testRender = mount(
      <MockedProvider mocks={mocks} addTypename>
        <AdditionalInformation
          t={t}
          desktop={''}
          user={user}
          updateTesterInfo={handleUpdateTesterInfo.ok}
        />
      </MockedProvider>
    )

    fillForm()
    testRender.find('form').simulate('submit')

    await wait(0)

    expect(handleUpdateTesterInfo.ok).toHaveBeenCalledWith({
      variables: {
        childrenAges: '10, 15',
        dietaryPreferenceId: 'F',
        ethnicityId: 'ethnicity',
        id: 'user-1',
        incomeRangeId: '10000',
        preferredProductCategoriesIds: 'en',
        smokerKindId: 'smokerKind'
      }
    })
  })
  test('should display error when updateUser returns error', async () => {
    testRender = mount(
      <MockedProvider mocks={mocks} addTypename>
        <AdditionalInformation
          t={t}
          desktop={''}
          user={user}
          updateTesterInfo={handleUpdateTesterInfo.error}
        />
      </MockedProvider>
    )
    fillForm()
    testRender.find('form').simulate('submit')

    await wait(0)

    expect(handleUpdateTesterInfo.error).toHaveBeenCalledWith({
      variables: {
        childrenAges: '10, 15',
        dietaryPreferenceId: 'F',
        ethnicityId: 'ethnicity',
        id: 'user-1',
        incomeRangeId: '10000',
        preferredProductCategoriesIds: 'en',
        smokerKindId: 'smokerKind'
      }
    })
  })

  test('should display error when handleUpdateTesterInfo throws network error', async () => {
    testRender = mount(
      <MockedProvider mocks={mocks} addTypename>
        <AdditionalInformation
          t={t}
          desktop={''}
          user={user}
          updateTesterInfo={handleUpdateTesterInfo.throw}
        />
      </MockedProvider>
    )
    fillForm()
    testRender.find('form').simulate('submit')

    await wait(0)

    expect(handleUpdateTesterInfo.throw).toHaveBeenCalledWith({
      variables: {
        childrenAges: '10, 15',
        dietaryPreferenceId: 'F',
        ethnicityId: 'ethnicity',
        id: 'user-1',
        incomeRangeId: '10000',
        preferredProductCategoriesIds: 'en',
        smokerKindId: 'smokerKind'
      }
    })
  })
})
