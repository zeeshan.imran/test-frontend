import React, { useEffect } from 'react'
import { useQuery, useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { withRouter } from 'react-router-dom'
import { withTranslation } from 'react-i18next'
import { Desktop } from '../../components/Responsive'
import FillerLoader from '../../components/FillerLoader'
import SurveyRejectionComponent from '../../components/SurveyRejection'
import AuthenticatedLayout from '../../containers/AuthenticatedLayout'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import TermsAndPrivacyFooter from '../TermsAndPrivacyFooter'

import { Container } from './styles'
import { surveyInfo } from '../../fragments/survey'
import { getShareLink } from '../../utils/shareUtils'

const SurveyRejection = ({ match, surveyId, i18n, history }) => {
  const {
    data: {
      currentSurveyParticipation: { surveyEnrollmentId }
    }
  } = useQuery(SURVEY_PARTICIPATION_QUERY)

  const { data, loading, error } = useQuery(SURVEY_QUERY, {
    fetchPolicy: 'network-only',
    variables: { id: surveyId }
  })
  if (data.survey) {
    let surveyLanguage = data.survey.surveyLanguage || 'en'
    if (i18n.language !== surveyLanguage) {
      i18n.changeLanguage(surveyLanguage)
    }
  }

  const rejectSurvey = useMutation(REJECT_SURVEY)
  useEffect(() => {
    if (surveyEnrollmentId) {
      rejectSurvey({
        variables: { input: { surveyEnrollment: surveyEnrollmentId } }
      })
    }
  }, [surveyEnrollmentId])

  if (loading) return <FillerLoader fullScreen loading />

  if (error) return `Error! ${error.message}`

  const { survey } = data

  let shareSurvey = survey
  if (survey.screeners && survey.screeners.length > 0) {
    shareSurvey = survey.screeners[0]
  }

  let { uniqueName, rejectionText } = shareSurvey

  return (
    <Desktop>
      {desktop => (
        <AuthenticatedLayout mergeNavbarToContent>
          <Container desktop={desktop}>
            <SurveyRejectionComponent
              sharingButtons={survey.sharingButtons}
              text={rejectionText}
              shareLink={getShareLink(uniqueName)}
            />
            <TermsAndPrivacyFooter showMessage={true} />
          </Container>
        </AuthenticatedLayout>
      )}
    </Desktop>
  )
}

const SURVEY_QUERY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      ...surveyInfo
      screeners {
        id
        uniqueName
      }
    }
  }
  ${surveyInfo}
`

const REJECT_SURVEY = gql`
  mutation rejectSurvey($input: EnrollmentStateInput) {
    rejectSurvey(input: $input)
  }
`

export default withRouter(withTranslation()(SurveyRejection))
