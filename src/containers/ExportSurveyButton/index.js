import React, { useState } from 'react'
import { useMutation } from 'react-apollo-hooks'
import { withRouter } from 'react-router-dom'
import gql from 'graphql-tag'
import ExportSurveyButtonComponent from '../../components/ExportSurveyButton'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import { useTranslation } from 'react-i18next'
import ExportSurveyModal from '../ExportSurveyModal'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import getErrorMessage from '../../utils/getErrorMessage'

const ExportSurveyButton = ({
  surveyId,
  surveyName,
  onMenuItemClick,
  screeners,
  compulsorySurvey,
  includeCompulsorySurveyDataInStats
}) => {
  const { t } = useTranslation()
  const sendReport = useMutation(MUTATION_SEND_REPORT)
  const [modalVisible, setModalVisible] = useState(false)

  const handleSubmit = async (
    userId,
    includeScreener,
    dateFilter,
    includeCompulsorySurvey,
    reportList
  ) => {
    try {
      setModalVisible(false)
      await sendReport({
        variables: {
          surveyId,
          userId,
          dateFilter,
          screener:
            includeScreener && screeners && screeners.length
              ? screeners[0].id
              : '',
          includeCompulsorySurvey,
          reportList
        }
      })
      displaySuccessMessage(t(`containers.exportSurveyButton.emailSend`))
    } catch (ex) {
      displayErrorPopup(getErrorMessage(ex, 'E_EXPORT_DEFAULT'))
    }
  }

  return (
    <React.Fragment>
      <ExportSurveyButtonComponent
        onClick={() => {
          onMenuItemClick()
          setModalVisible(true)
        }}
        icon='cloud-upload'
        tKey='exportSurveyButton'
      />
      {modalVisible && (
        <ExportSurveyModal
          tKey='exportSurveyButton'
          surveyName={surveyName}
          showScreenerOption={screeners && screeners.length > 0}
          onCancel={() => setModalVisible(false)}
          onSubmit={handleSubmit}
          compulsorySurvey={compulsorySurvey}
          includeCompulsorySurveyDataInStats={includeCompulsorySurveyDataInStats}
        />
      )}
    </React.Fragment>
  )
}

const MUTATION_SEND_REPORT = gql`
  mutation sendReport(
    $surveyId: ID!
    $userId: ID!
    $screener: ID
    $dateFilter: [Date]
    $includeCompulsorySurvey: Boolean
    $reportList: [String!]!
  ) {
    sendSurveyReport(
      surveyId: $surveyId
      userId: $userId
      screener: $screener
      dateFilter: $dateFilter
      includeCompulsorySurvey: $includeCompulsorySurvey
      reportList: $reportList
    )
  }
`

export default withRouter(ExportSurveyButton)
