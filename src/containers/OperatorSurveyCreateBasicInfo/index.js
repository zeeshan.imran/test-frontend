import React from 'react'
import SurveyBasicInfoForm from '../../components/SurveyBasicInfoForm'
import { useQuery, useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { withTranslation } from 'react-i18next'
import AlertModal from '../../components/AlertModal'
import { trimSpaces } from '../../utils/trimSpaces'
import templates from '../../utils/questionTemplates'
import { getAuthenticatedUser, getAuthenticatedOrganization } from '../../utils/userAuthentication'
import defaults from '../../defaults'
import useFlavorwikiOrganization from '../../hooks/useFlavorwikiOrganization'

import { DUMMY_COUNTRY_PHONE_CODE } from '../../utils/Constants'

// this is a simple standardize HTML
// it does not provide absolute correct result
// for comparsion purpose only
// please don't use for the other purposes
const standardizeHtml = (html) => {
  // use HTML entities ("Ö" from translation is modified by tinyMCE to "&ouml")
  var txt = document.createElement('textarea')
  txt.innerHTML = html
  return txt.value
}

const formatText = (text) =>
  text
    .replace(/['"]/gimu, '')
    .replace(/(<p>)|(<\/p)>/gimu, '')
    .replace(/(<br>)/gimu, '')
    .replace(/(<span[^>]*>)|(<\/span[^>]*>)/gimu, '')
    .replace(/[ \n\s]/gimu, '')

const htmlEquals = (source, target) =>
  formatText(standardizeHtml(source || '')) ===
  formatText(standardizeHtml(target || ''))

const OperatorSurveyCreateBasicInfo = ({
  t,
  i18n,
  surveyId,
  isDraft = false
}) => {
  const authUser = getAuthenticatedUser()
  const getTranslatedKey = (defaultKey, tKey, fn, extraParameters) => {
    if (fn) {
      return fn(tKey, extraParameters) === tKey
        ? fn(defaultKey, extraParameters) !== defaultKey
          ? fn(defaultKey, extraParameters)
          : ''
        : fn(tKey, extraParameters)
    }

    return i18n.t(tKey, extraParameters) === tKey
      ? i18n.t(defaultKey, extraParameters) !== defaultKey
        ? i18n.t(defaultKey, extraParameters)
        : ''
      : i18n.t(tKey, extraParameters)
  }

  const {
    data: {
      surveyCreation: { basics, questions }
    }
  } = useQuery(SURVEY_CREATION)

  if (authUser && authUser.isDummyTaster) {
    basics.country = DUMMY_COUNTRY_PHONE_CODE.code
  }

  const updateBasics = useMutation(UPDATE_SURVEY_CREATION_BASICS)
  const updateQuestionMutation = useMutation(UPDATE_SURVEY_CREATION_QUESTION)
  const isFlavorwikiOrganization = useFlavorwikiOrganization()

  const userOrganizationId = getAuthenticatedOrganization()
  const {
    data: { organization: { name: userOrganizationName } = {} } = {}
  } = useQuery(GET_ORGANIZATION, {
    variables: { id: userOrganizationId }
  })

  const {
    data: { isUniqueNameDuplicated }
  } = useQuery(IS_UNIQUE_NAME_DUPLICATED, {
    variables: {
      surveyId: surveyId,
      uniqueName: basics.uniqueName && trimSpaces(basics.uniqueName)
    },
    fetchPolicy: 'cache-and-network'
  })

  const handleChange = (value) => {
    const basicsUpdated = { ...basics, ...value }

    updateBasics({
      variables: {
        basics: basicsUpdated
      }
    })
    if (value.country) {
      const newT = i18n.getFixedT(basics.surveyLanguage)
      handleTemplatesLanguageChange(newT, newT, value.country)
    }
  }

  // const handleReferralAmountChange = value => {
  //   const numberValue = parseInt(value, 10)
  //   if (numberValue) {
  //     requirePaypalEmailStep()
  //   } else {
  //     unrequirePaypalEmailStep()
  //   }
  //   handleChange({ referralAmount: numberValue })
  // }

  const handleCustomButtonChange = (key, value) => {
    handleChange({
      customButtons: {
        ...basics.customButtons,
        [key]: value
      }
    })
    return null
  }

  const callHandleChange = (defaultGroups, oldT, newT, enT, value) => {
    let updatedValues = {
      instructionSteps: [],
      customButtons: {}
    }

    let basicProps = {
      instructionsText: '',
      thankYouText: '',
      rejectionText: '',
      screeningText: '',
      customizeSharingMessage: '',
      loginText: '',
      pauseText: '',
      ...basics,
      instructionSteps: [...basics.instructionSteps, '', '', '', '', '', '']
    }

    const getTranslations = (defaultKey, tKey) => {
      if (tKey.includes('customizeSharingMessage')) {
        return {
          newKeyVal: getTranslatedKey(defaultKey, tKey, newT, {
            amount: '{{amount}}'
          }),
          oldKeyVal: getTranslatedKey(defaultKey, tKey, oldT, {
            amount: '{{amount}}'
          }),
          englishKeyVal: getTranslatedKey(defaultKey, tKey, enT, {
            amount: '{{amount}}'
          })
        }
      }
      return {
        newKeyVal: getTranslatedKey(defaultKey, tKey, newT),
        oldKeyVal: getTranslatedKey(defaultKey, tKey, oldT),
        englishKeyVal: getTranslatedKey(defaultKey, tKey, enT)
      }
    }

    defaultGroups.forEach((groupName) => {
      if (basicProps[groupName] && typeof basicProps[groupName] === 'object') {
        for (let key of Object.keys(basicProps[groupName])) {
          const tKey = `defaultValues.${process.env.REACT_APP_THEME}.${groupName}.${key}`
          const defaultKey = `defaultValues.default.${groupName}.${key}`
          const { newKeyVal, oldKeyVal, englishKeyVal } = getTranslations(
            defaultKey,
            tKey
          )
          if (
            htmlEquals(basicProps[groupName][key], oldKeyVal) ||
            basicProps[groupName][key] === '' ||
            basicProps[groupName][key] === null ||
            basicProps[groupName][key] === undefined
          ) {
            if (
              (englishKeyVal !== newKeyVal || value === 'en') &&
              newKeyVal !== tKey
            ) {
              updatedValues[groupName][key] = newKeyVal
            }
          } else if (basicProps[groupName][key] !== '') {
            updatedValues[groupName][key] = basicProps[groupName][key]
          }
        }
      } else {
        const tKey = `defaultValues.${process.env.REACT_APP_THEME}.${groupName}`
        const defaultKey = `defaultValues.default.${groupName}`
        const { newKeyVal, oldKeyVal, englishKeyVal } = getTranslations(
          defaultKey,
          tKey
        )
        if (
          htmlEquals(basicProps[groupName], oldKeyVal) ||
          basicProps[groupName] === '' ||
          basicProps[groupName] === null ||
          basicProps[groupName] === undefined
        ) {
          if (
            (englishKeyVal !== newKeyVal || value === 'en') &&
            newKeyVal !== tKey
          ) {
            updatedValues[groupName] = newKeyVal
          } else {
            updatedValues[groupName] = basicProps[groupName]
          }
        }
      }
    })

    handleChange({
      surveyLanguage: value,
      ...updatedValues
    })
  }

  const changeQuestionDataByTemplate = (
    values,
    oldTemplateValues,
    newTemplateValues
  ) => {
    if (!newTemplateValues || !oldTemplateValues) {
      return values
    }
    if (['string', 'number', 'boolean'].includes(typeof values)) {
      return values === oldTemplateValues ? newTemplateValues : values
    }
    if (Array.isArray(values)) {
      let newValues = []
      Object.keys(values).forEach((key) => {
        newValues = [
          ...newValues,
          changeQuestionDataByTemplate(
            values[key],
            oldTemplateValues[key],
            newTemplateValues[key]
          )
        ]
      })
      return newValues
    }
    if (typeof values === 'object') {
      let newValues = {}
      Object.keys(values).forEach((key) => {
        Object.assign(newValues, {
          [key]: changeQuestionDataByTemplate(
            values[key],
            oldTemplateValues[key],
            newTemplateValues[key]
          )
        })
      })
      return newValues
    }
    return values
  }

  const handleTemplatesLanguageChange = (
    oldT,
    newT,
    country = basics.country
  ) => {
    questions.map((question, index) => {
      let newQuestion = question
      if (question.hasDemographic && question.demographicField) {
        const templateIndex = templates.findIndex(
          (template) => template.name === question.demographicField
        )
        if (templateIndex === -1) {
          return null
        }
        const template = templates[templateIndex]
        if (!template || !template.getData) {
          return null
        }

        const getTemplateProps = (country) => {
          const availableCountry = defaults.countries.find(
            (c) => c.code === country
          )
          const rewardCurrency = availableCountry || {}
          const currencyPrefix =
            defaults.currencyPrefixes[rewardCurrency.currency] || ''
          const currency =
            defaults.currencySuffixes[rewardCurrency.currency] || ''

          return {
            suffix: currency,
            prefix: currencyPrefix,
            country: country
          }
        }

        const newTemplateValues = template.getData({
          t: newT,
          ...getTemplateProps(country)
        })
        const oldTemplateValues = template.getData({
          t: oldT,
          ...getTemplateProps(basics.country)
        })
        const newQuestionData = changeQuestionDataByTemplate(
          question,
          oldTemplateValues,
          newTemplateValues
        )
        updateQuestionMutation({
          variables: {
            questionField: {
              hasDemographic: question.hasDemographic,
              demographicField: question.demographicField,
              ...newQuestionData
            },
            questionIndex: index
          }
        })
      }
      return newQuestion
    })
  }

  // change here
  const handleLanguageChange = (value, setFieldValue, field) => {
    const oldT = i18n.getFixedT(basics.surveyLanguage)
    const newT = i18n.getFixedT(value)
    const enT = i18n.getFixedT('en')
    let isChangeLanguageAlert = false

    const defaultGroups = [
      'customButtons',
      'instructionSteps',
      'instructionsText',
      'thankYouText',
      'rejectionText',
      'screeningText',
      'customizeSharingMessage',
      'loginText',
      'pauseText'
    ]
    defaultGroups.forEach((groupName) => {
      for (let key in basics[groupName]) {
        if (key !== '__typename') {
          isChangeLanguageAlert = true
        }
      }
    })

    if (!isChangeLanguageAlert) {
      callHandleChange(defaultGroups, oldT, newT, enT, value)
      handleTemplatesLanguageChange(oldT, newT)
      setFieldValue(field, value)
    } else {
      AlertModal({
        title: t('containers.operatorSurveyInfo.alertModalLanTitle'),
        okText: t('containers.operatorSurveyInfo.alertModalLanOkText'),
        description: t(
          'containers.operatorSurveyInfo.alertModalkLanDescription'
        ),
        handleOk: () => {
          callHandleChange(defaultGroups, oldT, newT, enT, value)
          handleTemplatesLanguageChange(oldT, newT)
          setFieldValue(field, value)
        }
      })
    }
  }
  return (
    <SurveyBasicInfoForm
      authUser={authUser}
      isNewSurvey={!surveyId || isDraft}
      isUniqueNameDuplicated={isUniqueNameDuplicated}
      onChange={handleChange}
      handleCustomButtonChange={handleCustomButtonChange}
      handleLanguageChange={handleLanguageChange}
      country={basics.country}
      isDraft={isDraft}
      isFlavorwikiOrganization={isFlavorwikiOrganization}
      isFlavorwikiOperator={userOrganizationName === 'FlavorWiki'}
      {...basics}
    />
  )
}

const IS_UNIQUE_NAME_DUPLICATED = gql`
  query isUniqueNameDuplicated($surveyId: ID, $uniqueName: String) {
    isUniqueNameDuplicated(surveyId: $surveyId, uniqueName: $uniqueName)
  }
`

export const GET_ORGANIZATION = gql`
  query organization($id: ID!) {
    organization(id: $id) {
      id
      name
    }
  }
`

export const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      basics {
        name
        title
        coverPhoto
        instructionsText
        instructionSteps
        thankYouText
        rejectionText
        screeningText
        uniqueName
        authorizationType
        allowRetakes
        forcedAccount
        forcedAccountLocation
        referralAmount
        savedRewards
        recaptcha
        minimumProducts
        maximumProducts
        surveyLanguage
        country
        customizeSharingMessage
        loginText
        pauseText
        allowedDaysToFillTheTasting
        isPaypalSelected
        isGiftCardSelected
        isScreenerOnly
        showSurveyProductScreen
        productDisplayType
        showGeneratePdf
        linkedSurveys
        exclusiveTasters
        autoAdvanceSettings {
          active
          debounce
          hideNextButton
        }
        screenOutSettings {
          reject
          rejectAfterStep
        }
        pdfFooterSettings {
          active
          footerNote
        }
        promotionSettings {
          active
          showAfterTaster
          code
        }
        promotionOptions {
          promoProducts {
            label
            url
          }
        }
        maxProductStatCount
        customButtons {
          continue
          start
          next
          skip
        }
        productRewardsRule {
          active
        }
        referralAmountInDollar
        retakeAfter
        maxRetake
        maximumReward
      }
      questions
      mandatoryQuestions
      uniqueQuestionsToCreate
      editMode
    }
  }
`

export const UPDATE_SURVEY_CREATION_BASICS = gql`
  mutation updateSurveyCreationBasics($basics: basics) {
    updateSurveyCreationBasics(basics: $basics) @client
  }
`

const UPDATE_SURVEY_CREATION_QUESTION = gql`
  mutation updateSurveyCreationQuestion(
    $questionField: Question
    $questionIndex: Int
    $mandatoryQuestion: Boolean
  ) {
    updateSurveyCreationQuestion(
      questionField: $questionField
      questionIndex: $questionIndex
      mandatoryQuestion: $mandatoryQuestion
    ) @client
  }
`

export default withTranslation()(OperatorSurveyCreateBasicInfo)
