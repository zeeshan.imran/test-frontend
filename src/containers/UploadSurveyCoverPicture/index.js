import React, { useState } from 'react'
import axios from 'axios'

import { useApolloClient } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { displayErrorPopup } from '../../utils/displayErrorPopup/index'
import UploadSurveyCoverComponent from '../../components/UploadSurveyCover'
import FieldLabel from '../../components/FieldLabel'
import { useTranslation } from 'react-i18next'
import i18n from '../../utils/internationalization/i18n'

const GET_SIGNED_URL = gql`
  query getSignedUrl($type: String) {
    getSignedUrl(type: $type) {
      signedUrl
      fileLink
    }
  }
`

const ALLOWED_TYPES = ['image/png', 'image/jpeg', 'image/gif']

const handleUpload = async (
  file,
  client,
  onChange,
  setUploading,
  setUploadError
) => {
  setUploading(true)
  try {
    const { type } = file

    if (!ALLOWED_TYPES.includes(type)) {
      setUploading(false)
      setUploadError(true)
      return displayErrorPopup(i18n.t('errors.uploadProductPictureFailed'))
    }

    const {
      data: {
        getSignedUrl: { signedUrl, fileLink }
      }
    } = await client.query({
      query: GET_SIGNED_URL,
      variables: { type: file.type },
      fetchPolicy: 'no-cache'
    })

    const result = await axios.put(signedUrl, file, {
      headers: {
        'Content-Type': file.type
      }
    })
    if (result.status !== 200) {
      setUploadError(true)
      return displayErrorPopup(i18n.t('errors.uploadProductPictureFailed'))
    }
    setUploadError(false)
    onChange([fileLink])
  } catch (error) {
    setUploadError(true)
    onChange([''])
  }
  setUploading(false)
}
 
const UploadSurveyCoverPicture = ({
  tooltip,
  tooltipPlacement,
  onChange,
  value,
  isSurvey=false,
  label,
  ...rest
}) => {
  const { t } = useTranslation()
  const client = useApolloClient()
  const [uploading, setUploading] = useState(false)
  const [uploadError, setUploadError] = useState(false)
  const labelValue = label ? label : isSurvey ? "Survey Cover" : t('containers.uploadProductPicture.label')
  return (
    <FieldLabel
      label={labelValue}
      tooltip={tooltip}
      tooltipPlacement={tooltipPlacement}
    >
      <UploadSurveyCoverComponent
        {...rest}
        error={uploadError}
        loading={uploading}
        onFileUpload={({ file }) =>
          handleUpload(file, client, onChange, setUploading, setUploadError)
        }
        url={value}
      />
    </FieldLabel>
  )
}

export default UploadSurveyCoverPicture
