import React from 'react'
import { mount } from 'enzyme'
import OperatorRoute from '.'
import OperatorRouteComponent from '../../components/OperatorRoute'
import { getAuthenticatedUser } from '../../utils/userAuthentication'
import { MockedProvider } from 'react-apollo/test-utils'
import wait from '../../utils/testUtils/waait'
import gql from 'graphql-tag'
import { useSlaask } from '../../contexts/SlaaskContext'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'

jest.mock('../../contexts/SlaaskContext')

describe('OperatorRoute', () => {
  let testRender
  let reloadSlaask

  let user
  let client

  beforeEach(() => {
    user = getAuthenticatedUser()
    reloadSlaask = jest.fn()
    useSlaask.mockImplementation(() => ({ reloadSlaask: reloadSlaask }))

    client = createApolloMockClient()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render OperatorRouteComponent', async () => {
    const history = createBrowserHistory()

    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <OperatorRoute desktop={''} user={user} />
        </Router>
      </ApolloProvider>
    )
    await wait(0)
    expect(OperatorRouteComponent).toHaveLength(1)
  })
})
