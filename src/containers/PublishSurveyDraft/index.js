import React from 'react'
import { withRouter } from 'react-router-dom'
import { useQuery, useMutation } from 'react-apollo-hooks'
import { Modal } from 'antd'
import Button from '../../components/Button'
import LoadingModal from '../../components/LoadingModal'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import useToggle from '../../hooks/useToggle/index'
import useUpdateSurvey from '../../hooks/useUpdateSurvey'
import useConfirmQuestionReorder from '../../hooks/useConfirmQuestionReorder'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import gql from 'graphql-tag'
import useSurveyValidate from '../../hooks/useSurveyValidate'
import { useTranslation } from 'react-i18next'
import settingsSchema from '../../validates/settings'
import { emailsValidationSchema } from '../../components/OperatorSurveyCreateEmailSettings'
import TooltipWrapper from '../../components/TooltipWrapper'

const PublishSurveyDraft = ({ history, match }) => {
  const { t } = useTranslation()
  const confirmQuestionReorder = useConfirmQuestionReorder()
  const [publishing, togglePublishing] = useToggle(false)
  const {
    data: {
      surveyCreation: { products, basics, questions, mandatoryQuestions }
    }
  } = useQuery(SURVEY_CREATION)
  const activateSurvey = useMutation(RESUME_SURVEY)

  const {
    params: { surveyId }
  } = match
  const updateDraft = useUpdateSurvey({
    surveyId,
    successMessage: t('containers.publishSurveyDraft.successMessage'),
    errorMessage: t('containers.publishSurveyDraft.errorMessage')
  })
  const surveyValidate = useSurveyValidate()

  let submitDisabled = true
  if (basics.isScreenerOnly) {
    submitDisabled =
      !basics.uniqueName ||
      questions.length + mandatoryQuestions.length === 0 ||
      basics.linkedSurveys.length === 0
  } else if (basics.compulsorySurvey) {
    submitDisabled = !basics.uniqueName || questions.length === 0
  } else {
    submitDisabled =
      !basics.uniqueName ||
      products.length === 0 ||
      questions.length + mandatoryQuestions.length === 0 ||
      !settingsSchema.isValidSync(basics.autoAdvanceSettings) ||
      products.reduce((result, product) => result && !product.isAvailable, true)
  }

  const onClickPublish = () => {

    Modal.confirm({
      title: t('confirmRemoveDataFromData.confirmModalTitle'),
      content: t('confirmRemoveDataFromData.confirmModalContent'),
      onOk: () => {
        onPublish(true)
      },
      onCancel() {
        onPublish(false)
      },
    })
  }

  const onPublish = async (isRemoveDataFromDraft) => {
    try {
      emailsValidationSchema.validateSync(basics)
    } catch (ex) {
      history.push(`./emails`)
      return
    }

    const isAdvanceSettingsValid = await surveyValidate.validateAdvanceSettings()
    if (!isAdvanceSettingsValid) {
      history.push(`./settings`)
      await surveyValidate.showAdvanceSettingsErrors()
      return false
    }

    const isProductsValid = await surveyValidate.validateProducts()
    if (!isProductsValid) {
      history.push(`./products`)
      await surveyValidate.showProductsErrors()
      return
    }

    const {
      isValid: isQuestionsValid,
      invalidQuestion,
      noQuestionError
    } = await surveyValidate.validateQuestions(true)

    if (noQuestionError) {
      await surveyValidate.showOnlyMandatoryQuestionErrors()
      return
    }

    if (!isQuestionsValid && invalidQuestion.displayOn === 'payments') {
      history.push(`./financial?section=${invalidQuestion.displayOn}`)
      await surveyValidate.showQuestionsErrors()
      return
    }

    if (!isQuestionsValid) {
      history.push(`./questions?section=${invalidQuestion.displayOn}`)
      await surveyValidate.showQuestionsErrors()
      return
    }

    const duplicateTitles = await surveyValidate.validateChartTitles()
    
    if(duplicateTitles.length > 0) {
      await surveyValidate.showChartTitlesErrors(duplicateTitles)
      return
    }

    const onConfirm = async () => {
      togglePublishing()

      try {
        const survey = await updateDraft({ isRemoveDataFromDraft })
        await activateSurvey({ variables: { id: surveyId } })
        togglePublishing()
        history.push(`/operator/surveys${survey.folderId ? `?folderId=${survey.folderId}` : ''}`)
      } catch (error) {
        togglePublishing()
        displayErrorPopup(error && error.message)
      }
    }

    confirmQuestionReorder(onConfirm)
  }

  return (
    <TooltipWrapper
      helperText={t('tooltips.publishSurvey')}
      placement='leftTop'
    >
      <Button
        type={submitDisabled ? 'disabled' : 'primary'}
        size='default'
        onClick={onClickPublish}
      >
        {t('containers.publishSurveyDraft.publishButton')}
      </Button>
      <LoadingModal
        visible={publishing}
        text={t('containers.publishSurveyDraft.loadingText')}
      />
    </TooltipWrapper>
  )
}

export const RESUME_SURVEY = gql`
  mutation resumeSurvey($id: ID!) {
    resumeSurvey(id: $id) {
      id
      state
    }
  }
`

export default withRouter(PublishSurveyDraft)
