import React from 'react'
import { mount } from 'enzyme'
import { ApolloProvider } from 'react-apollo-hooks'
import wait from '../../utils/testUtils/waait'
import Button from '../../components/Button'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import useSurveyValidate from '../../hooks/useSurveyValidate'
import usePublishSurvey from '../../hooks/usePublishSurvey'
import PublishEditedSurvey, { DEPRECATE_SURVEY } from './index'
import { defaultSurvey } from '../../mocks'

const createSurveyCreationMock = secondProduct => ({
  ...defaultSurvey,
  products: [
    {
      name: 'the first product',
      brand: 'the brand'
    },
    secondProduct
  ]

})

jest.mock('../../hooks/usePublishSurvey')
jest.mock('../../hooks/useSurveyValidate')

describe('publish edited survey', () => {
  let publishSurvey
  let render
  let history
  let client
  let showProductsErrors
  const match = { params: { surveyId: 'survey-1' } }
  let showQuestionsErrors

  beforeEach(() => {
    client = createApolloMockClient({
      mocks: [
        {
          request: {
            query: DEPRECATE_SURVEY,
            variables: { id: 'survey-1' }
          },
          result: {
            data: {
              deprecateSurvey: ['authToken', 'isOperator'],
              invalidQuestion: { displayOn: true }
            }
          }
        }
      ]
    })
    console.log(createSurveyCreationMock({
      name: '',
      brand: 'the brand'
    }))
    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: createSurveyCreationMock({
          name: '',
          brand: 'the brand'
        })
      }
    })
    publishSurvey = jest.fn()
    showProductsErrors = jest.fn()
    showQuestionsErrors = jest.fn()
    usePublishSurvey.mockImplementation(() => publishSurvey)

    history = {
      push: jest.fn()
    }
  })

  afterEach(() => {
    render.unmount()
  })

  test('should switch to products tab and show errors when the survey has at least one invalid product', async () => {
    useSurveyValidate.mockImplementation(() => ({
      validateAdvanceSettings: () => true,
      validateProducts: () => false,
      validateQuestions: () => ({ isValid: true }),
      validateChartTitles: () => [],
      showProductsErrors
    }))

    render = mount(
      <ApolloProvider client={client}>
        <PublishEditedSurvey.WrappedComponent history={history} match={match} />
      </ApolloProvider>
    )

    await wait(0)

    render
      .find(Button)
      .first()
      .prop('onClick')()

    await wait(0)

    expect(history.push).toHaveBeenCalledWith('./products')
    expect(showProductsErrors).toHaveBeenCalled()
    expect(publishSurvey).not.toHaveBeenCalled()
  })

  test('should call publishSurvey then redirect to /surveys when the survey has all products is valid', async () => {
    useSurveyValidate.mockImplementation(() => ({
      validateAdvanceSettings: () => true,
      validateProducts: () => true,
      validateQuestions: () => ({ isValid: true }),
      validateChartTitles: () => [],
      showProductsErrors
    }))

    render = mount(
      <ApolloProvider client={client}>
        <PublishEditedSurvey.WrappedComponent history={history} match={match} />
      </ApolloProvider>
    )

    await wait(0)

    await render
      .find(Button)
      .first()
      .prop('onClick')()

    await wait(0)

    // expect(history.push).toHaveBeenCalledWith('/operator/surveys')
    expect(showProductsErrors).not.toHaveBeenCalled()
    // expect(publishSurvey).toHaveBeenCalled()
  })

  test('should call publishSurvey then redirect to /surveys when the survey has all products is invalid valid', async () => {
    useSurveyValidate.mockImplementation(() => ({
      validateAdvanceSettings: () => true,
      validateProducts: () => true,
      validateQuestions: () => ({ invalidQuestion: false }),
      validateChartTitles: () => [],
      showQuestionsErrors
    }))
    render = mount(
      <ApolloProvider client={client}>
        <PublishEditedSurvey.WrappedComponent history={history} match={match} />
      </ApolloProvider>
    )

    await wait(0)

    await render
      .find(Button)
      .first()
      .prop('onClick')()

    await wait(0)
  })

  test('should call publishSurvey then redirect on error of togglePublishing', async () => {
    useSurveyValidate.mockImplementation(() => ({
      validateAdvanceSettings: () => true,
      validateProducts: () => true,
      validateQuestions: () => ({ isValid: true }),
      validateChartTitles: () => [],
      showProductsErrors
    }))

    const matchParam = {
      params: {}
    }

    render = mount(
      <ApolloProvider client={client}>
        <PublishEditedSurvey.WrappedComponent
          history={history}
          match={matchParam}
        />
      </ApolloProvider>
    )

    await wait(0)

    await render
      .find(Button)
      .first()
      .prop('onClick')()

    await wait(0)
  })
})
