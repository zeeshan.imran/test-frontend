import React from 'react'
import { mount } from 'enzyme'
import QuestionCreationCardBody from '.'
import wait from '../../utils/testUtils/waait'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import gql from 'graphql-tag'
import { createBrowserHistory } from 'history'
import { Router } from 'react-router-dom'
import QuestionCreationCardBodyComponent from '../../components/QuestionCreationCardBody'
import sinon from 'sinon'

jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k,
  getFixedT: () => () => ''
}))
jest.mock('../../components/InputRichText', () => ({
  __esModule: true,
  default: function InputRichText () {
    return null
  }
}))

const UPDATE_SURVEY_CREATION_QUESTION = gql`
  mutation updateSurveyCreationQuestion(
    $questionField: Question
    $questionIndex: Int
    $mandatoryQuestion: Boolean
    $question: Question
  ) {
    updateSurveyCreationQuestion(
      questionField: $questionField
      questionIndex: $questionIndex
      mandatoryQuestion: $mandatoryQuestion
      question: $question
    ) @client
  }
`

describe('QuestionCreationCardBody', () => {
  let testRender
  let client
  let spyChange

  beforeEach(() => {
    client = createApolloMockClient()
    client.cache.writeQuery({
      query: UPDATE_SURVEY_CREATION_QUESTION,
      updateQuestionMutation: {
        variables: {
          questionField: 'email',
          id: 'question-1',
          questionIndex: 1,
          mandatoryQuestion: [],
          question: {id: 'question-1', type: 'email'}
        }
      },
      data: {
        updateSurveyCreationQuestion: {
          questionField: 'email',
          questionIndex: 1,
          mandatoryQuestion: [],
          question: {id: 'question-1', type: 'email'}
        }
      }
    })

    const stub = sinon.stub(client, 'mutate')
    spyChange = jest.fn()
    stub
      .withArgs(sinon.match({ mutation: UPDATE_SURVEY_CREATION_QUESTION }))
      .callsFake(spyChange)
    stub.callThrough()
  })

  test('should render QuestionCreationCardBody', async () => {
    const history = createBrowserHistory()

    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <QuestionCreationCardBody question={{ type: 'info' }} />
        </Router>
      </ApolloProvider>
    )

    await wait(0)

    expect(testRender).toMatchSnapshot()

    testRender.unmount()
  })

  test('should render QuestionCreationCardTypeSelect', async () => {
    const history = createBrowserHistory()

    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <QuestionCreationCardBody
            question={{
              id: 'question-1',
              type: 'info',
              prompt: 'What is your email?',
              secondaryPrompt: 'secondaryPrompt'
            }}
          />
        </Router>
      </ApolloProvider>
    )

    await wait(0)

    spyChange.mockReset()
    testRender
      .find(QuestionCreationCardBodyComponent)
      .prop('handleFieldChange')()
    expect(spyChange).toHaveBeenCalled()

    testRender.unmount()
  })
})
