import React from 'react'
import { mount } from 'enzyme'
import { act } from 'react-dom/test-utils'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import OperatorSurveyLogsContainer, {
  SURVEY_LOGS,
  COUNT_SURVEY_LOGS
} from './index'
import { Table } from 'antd'

jest.doMock('./index.js')
jest.mock('rc-util/lib/Portal')
jest.mock('formik')
jest.doMock('antd')

describe('check the operator interaction when adding, searching, and deleting survey logs', () => {
  let testRender
  let apolloClient
  let location
  let mockPush = jest.fn(({ search }) => {
    location = {
      ...location,
      search
    }
  })

  beforeEach(async () => {
    apolloClient = createApolloMockClient({
      mocks: [
        {
          request: {
            query: SURVEY_LOGS,
            variables: {
              input: {
                keyword: '',
                skip: 0,
                orderBy: undefined,
                orderDirection: undefined
              }
            }
          },
          result: {
            data: {
              organizations: []
            }
          }
        },
        {
          request: {
            query: COUNT_SURVEY_LOGS,
            variables: { input: { keyword: '' } }
          },
          result: {
            data: {
              countOrganizations: 0
            }
          }
        }
      ]
    })

    location = {
      path: '/operator/users'
    }

    testRender = mount(
      <ApolloProvider client={apolloClient}>
        <OperatorSurveyLogsContainer
          history={{
            push: mockPush
          }}
          location={location}
          match={{ params: { surveyId: 'survey01' } }}
        />
      </ApolloProvider>
    )
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('orderBy to history.push', async () => {
    const names = ['createdAt']
    const order = ['descend', 'ascend']
    const table = testRender.find(Table)
    for (let index = 0; index < names.length; index++) {
      act(() => {
        table.prop('onChange')(
          {
            current: 1
          },
          '',
          {
            field: names[index],
            order: order[index]
          }
        )
      })
      expect(location.search).toBe(
        `orderBy=${names[index]}&orderDirection=${order[index]}&page=1`
      )
    }
  })

  test('search bar to history.push', async () => {
    const searchBar = testRender.find({ placeholder: 'Search' }).first()
    act(() => {
      searchBar.prop('handleChange')('this is a search text')
    })
    expect(location.search).toBe('keyword=this%20is%20a%20search%20text&page=1')
  })
})
