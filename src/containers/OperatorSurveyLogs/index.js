import React from 'react'
import { useQuery } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import Presentation from '../../components/OperatorSurveyLogs'
import useSearch from '../../hooks/useSearch'

import { DEFAULT_N_ELEMENTS_PER_PAGE } from '../../utils/Constants'

export const SURVEY_LOGS = gql`
  query surveyLogs($surveyId: ID!, $input: SurveyLogsInput) {
    surveyLogs(surveyId: $surveyId, input: $input) {
      id
      createdAt
      action
      type
      collections
      values
      by {
        id
        fullName
        emailAddress
      }
      relatedEnrollment {
        id
        state
      }
      relatedProduct {
        id
        name
        photo
      }
      relatedQuestion {
        id
        prompt
        typeOfQuestion
      }
      relatedAnswer {
        id
        value
        timeToAnswer
      }
    }
  }
`

export const COUNT_SURVEY_LOGS = gql`
  query countSurveyLogs($surveyId: ID!, $input: CountSurveyLogsInput) {
    countSurveyLogs(surveyId: $surveyId, input: $input)
  }
`

const Page = ({ location, history, match, ...props }) => {
  const { surveyId } = match.params
  const {
    keyword,
    orderBy,
    orderDirection,
    page,
    handleKeywordChange,
    handleTableChange
  } = useSearch({
    location,
    history
  })

  const {
    data: { surveyLogs },
    loading
  } = useQuery(SURVEY_LOGS, {
    variables: {
      surveyId: surveyId,
      input: {
        keyword,
        skip: (page - 1) * DEFAULT_N_ELEMENTS_PER_PAGE,
        orderBy,
        orderDirection
      }
    },
    fetchPolicy: 'cache-and-network'
  })

  const {
    data: { countSurveyLogs }
  } = useQuery(COUNT_SURVEY_LOGS, {
    variables: {
      surveyId: surveyId,
      input: {
        keyword
      }
    },
    fetchPolicy: 'cache-and-network'
  })

  return (
    <Presentation
      loading={loading}
      keyword={keyword}
      page={page - 1}
      surveyLogs={surveyLogs}
      countSurveyLogs={countSurveyLogs}
      handleKeywordChange={handleKeywordChange}
      handleTableChange={handleTableChange}
    />
  )
}

export default Page
