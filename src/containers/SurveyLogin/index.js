import React, { useEffect, useState } from 'react'
import gql from 'graphql-tag'
import { isEmpty } from 'ramda'
import { useMutation, useQuery } from 'react-apollo-hooks'
import { withRouter, Redirect } from 'react-router-dom'
import SurveyLoginComponent from '../../components/SurveyLogin'
import FillerLoader from '../../components/FillerLoader'
import { SURVEY_QUERY } from '../../queries/Survey'

import useCustomButton from '../../hooks/useCustomButton'
import useUpdateLocale from '../../hooks/useUpdateSurveyLanguage'
import useResumeSurvey from '../../hooks/useResumeSurvey'
import useLoginToSurvey from '../../hooks/useLoginToSurvey'

import {
  getAuthenticatedUser,
  setCurrentSelectedSurvey
} from '../../utils/userAuthentication'

import {
  logout as logoutOfPreviousSurveys,
  getAuthenticationToken,
  removeSurveyRedirect
} from '../../utils/surveyAuthentication'
import {
  destroyDraftWarningPopup,
  displayDraftWarningPopup
} from '../../utils/displayDraftWarningPopup'
import getQueryParams from '../../utils/getQueryParams'

const retakeKey = 'retake'

const getShouldSkipLogin = survey => {
  const { authorizationType, settings = {}, state } = survey
  return (
    authorizationType === 'public' &&
    !settings.recaptcha &&
    state !== 'suspended'
  )
}

const SurveyLogin = ({ history, surveyId, location, isTesting }) => {
  const {
    data: { survey = {} } = {},
    loading: fetchingSurvey,
    error
  } = useQuery(SURVEY_QUERY, {
    variables: { id: surveyId },
    fetchPolicy: isTesting ? 'cache-first' : 'network-only'
  })

  const { data: { compulsorySurveyCountry = {} } = {} } = useQuery(
    COMPULSORY_SURVEY_QUERY,
    {
      fetchPolicy: isTesting ? 'cache-first' : 'network-only'
    }
  )

  const resetCache = useMutation(RESET_CACHE)
  const saveCurrentSurveyParticipation = useMutation(SAVE_SURVEY_PARTICIPATION)
  const loginToSurvey = useLoginToSurvey()
  const resumeSurveyProgress = useResumeSurvey(history.push)

  const updateLocale = useUpdateLocale()
  const { buttonLabel } = useCustomButton(surveyId, 'continue')
  const [isLoggingIn, setLoggingIn] = useState(false)
  const [isSelectedTaster, setIsSelectedTaster] = useState(false)
  const [surveyEnrollmentId, setSurveyEnrollmentId] = useState(null)
  const [lastAnsweredQuestion, setLastAnsweredQuestion] = useState(null)
  const [isRetake, setIsRetake] = useState(null)

  const loggedInUser = getAuthenticatedUser()
  const isLoggedIn = loggedInUser && !isEmpty(loggedInUser)
  const isAnExclusiveSurvey =
    survey &&
    survey.authorizationType &&
    (survey.authorizationType === 'selected' ||
      survey.authorizationType === 'enrollment')
  const [isSurveyExpired, setSurveyExpired] = useState(false)
  const [isSurveySuspended, setSurveySuspended] = useState(false)

  useEffect(() => {
    logoutOfPreviousSurveys()
    resetCache()
  }, [])

  useEffect(() => {
    const { referral, extsource, extid } = getQueryParams(location)
    const allowStates = ['active', 'draft', 'suspended']

    //emptying survey local storage
    removeSurveyRedirect(null)

    let redirector = ''

    if (referral) {
      redirector += `referral=${referral}&`
    }
    if (extsource) {
      redirector += `extsource=${extsource}&`
    }
    if (extid) {
      redirector += `extid=${extid}&`
    }

    if (survey && surveyId === survey.uniqueName) {
      const redirecter = redirector
        ? `/survey/${survey.id}?${redirector}`
        : `/survey/${survey.id}`

      history.push(redirecter)
      return
    }

    if (
      loggedInUser &&
      !loggedInUser.isCompulsorySurveyTaken &&
      Object.keys(survey).length > 0 &&
      !survey.compulsorySurvey &&
      Object.keys(compulsorySurveyCountry).length > 0 &&
      compulsorySurveyCountry.id !== survey.id &&
      allowStates.includes(compulsorySurveyCountry.state) &&
      surveyId !== survey.uniqueName &&
      loggedInUser.type === 'taster'
    ) {
      setCurrentSelectedSurvey(surveyId)
      const redirecter = redirector
        ? `/survey/${compulsorySurveyCountry.id}?referral=${referral}`
        : `/survey/${compulsorySurveyCountry.id}`

      history.push(redirecter)
      return
    }

    if (
      survey &&
      survey.state &&
      survey.state !== 'active' &&
      survey.state !== 'draft' &&
      survey.state !== 'suspended'
    ) {
      history.push(`/404`)
      return
    }

    if (survey.state === 'suspended') {
      setSurveySuspended(true)
    }

    if (loggedInUser && isAnExclusiveSurvey && isLoggedIn) {
      setLoggingIn(true)
      loginToSurvey({
        email: loggedInUser.emailAddress,
        referral,
        surveyId,
        isLoggedIn
      })
        .then(async (loggedInInfo = {}) => {
          setLoggingIn(false)
          setIsSelectedTaster(!!getAuthenticationToken)
          await saveCurrentSurveyParticipation({
            variables: {
              savedQuestions: loggedInInfo.savedQuestions,
              savedRewards: loggedInInfo.savedRewards,
              productRewardsRule: loggedInInfo.productRewardsRule
            }
          })
          setSurveyEnrollmentId(loggedInInfo.surveyEnrollmentId)
          setLastAnsweredQuestion(loggedInInfo.lastAnsweredQuestion)
        })
        .catch((err = { message: '' }) => {
          if (err.message && err.message.includes('rejected')) {
            setIsRetake('rejected')
          } else if (err.message && err.message.includes('finished')) {
            setIsRetake('finished')
          } else if (
            err.message &&
            err.message.toLowerCase().includes('expired')
          ) {
            setSurveyExpired(true)
            setIsRetake('expired')
          } else {
            setIsRetake(false)
          }
          setLoggingIn(false)
          err.message && err.message.toLowerCase().includes(retakeKey)
            ? setIsSelectedTaster(retakeKey)
            : setIsSelectedTaster(false)
          setSurveyEnrollmentId(null)
          setLastAnsweredQuestion(null)
        })
    } else if (getShouldSkipLogin(survey)) {
      setLoggingIn(true)
      loginToSurvey({
        surveyId,
        email: isLoggedIn && loggedInUser ? loggedInUser.emailAddress : '',
        referral,
        extsource,
        extid,
        isLoggedIn
      }).then(
        async ({
          surveyEnrollmentId,
          savedRewards,
          savedQuestions,
          lastAnsweredQuestion,
          lastSelectedProduct: lastSelectedProductDb,
          productRewardsRule
        }) => {
          setLastAnsweredQuestion(lastAnsweredQuestion)

          // Only If screener
          if (
            survey &&
            (survey.isScreenerOnly ||
              survey.compulsorySurvey ||
              (extid && extsource))
          ) {
            await resumeSurveyProgress({
              surveyId,
              lastAnsweredQuestionFromLogin: lastAnsweredQuestion
            })
            await saveCurrentSurveyParticipation({
              variables: {
                surveyId
              }
            })
            return
          }

          const availableProducts =
            (survey &&
              survey.products &&
              survey.products.filter(
                product => product && product.isAvailable
              )) ||
            []
          const productId = lastSelectedProductDb
            ? lastSelectedProductDb
            : availableProducts[0] && availableProducts[0].id

          if (productId) {
            await resumeSurveyProgress({
              surveyId,
              lastAnsweredQuestionFromLogin: lastAnsweredQuestion
            })
            await saveCurrentSurveyParticipation({
              variables: {
                surveyId,
                selectedProduct: productId,
                savedRewards: savedRewards,
                savedQuestions: savedQuestions,
                productRewardsRule: productRewardsRule
              }
            })
            if (
              survey &&
              survey.state !== 'suspended' &&
              !lastAnsweredQuestion
            ) {
              history.push('./instructions')
            }
          }
        }
      )
    }
    return destroyDraftWarningPopup
  }, [survey, surveyId, location])

  if (error) {
    return <Redirect to='/404' />
  }

  if (survey && survey.state === 'draft') {
    displayDraftWarningPopup()
  }

  saveCurrentSurveyParticipation({
    variables: {
      surveyId
    }
  })

  if (
    fetchingSurvey ||
    isLoggingIn ||
    getShouldSkipLogin(survey) ||
    (survey && surveyId === survey.uniqueName)
  ) {
    return <FillerLoader loading />
  }

  const {
    title: surveyTitle,
    state,
    authorizationType,
    settings,
    surveyLanguage,
    products: productsInSurvey,
    loginText,
    pauseText,
    country
  } = survey
  updateLocale(surveyLanguage)

  if (state !== 'active' && state !== 'draft' && state !== 'suspended') {
    return <Redirect to='/404' />
  }
  let eligibleCountryUser = true
  if (loggedInUser) {
    eligibleCountryUser =
      loggedInUser &&
      (loggedInUser.country === country ||
        loggedInUser.type === 'operator' ||
        loggedInUser.isSuperAdmin)
  }

  return (
    <SurveyLoginComponent
      authorizationType={authorizationType}
      authorizationState={
        isSelectedTaster === 'retake'
          ? isRetake
          : isLoggedIn
          ? isSelectedTaster
            ? 'valid'
            : 'invalid'
          : 'loggedOut'
      }
      surveyTitle={surveyTitle}
      isSurveyExpired={isSurveyExpired}
      isSurveySuspended={isSurveySuspended}
      handleSubmit={async (email, password, country) => {
        let loggedSurveyEnrollmentId = surveyEnrollmentId
        let loggedLastAnsweredQuestion = lastAnsweredQuestion
        let loggedLastSelectedProduct = ''
        let retake = isSelectedTaster
        if (authorizationType !== 'selected') {
          const { referral } = getQueryParams(location)
          if (isLoggedIn && loggedInUser) {
            email = loggedInUser.emailAddress
          }

          await loginToSurvey({
            email: email || '',
            surveyId,
            referral
          })
            .then((loggedInInfo = {}) => {
              loggedSurveyEnrollmentId =
                loggedInInfo && loggedInInfo.surveyEnrollmentId
              loggedLastAnsweredQuestion =
                loggedInInfo && loggedInInfo.lastAnsweredQuestion
              loggedLastSelectedProduct =
                loggedInInfo && loggedInInfo.lastSelectedProduct
              setSurveyEnrollmentId(loggedSurveyEnrollmentId)
              setLastAnsweredQuestion(loggedLastAnsweredQuestion)
            })
            .catch((err = { message: '' }) => {
              if (err && err.message.toLowerCase().includes('expired')) {
                retake = retakeKey // Adding it to stop retake condition
                setSurveyExpired(true)
              } else {
                setIsSelectedTaster(false)
                if (err && err.message.toLowerCase().includes(retakeKey)) {
                  retake = retakeKey
                  setIsSelectedTaster(retakeKey)
                } else {
                  setIsSelectedTaster(false)
                }
              }

              setSurveyEnrollmentId(null)
              setLastAnsweredQuestion(null)
              if (err && err.message.includes('rejected')) {
                setIsRetake('rejected')
              } else if (err && err.message.includes('finished')) {
                setIsRetake('finished')
              } else {
                setIsRetake(false)
              }
            })
        }
        if (retake !== retakeKey) {
          await saveCurrentSurveyParticipation({
            variables: {
              surveyId,
              selectedProduct: loggedLastSelectedProduct
                ? loggedLastSelectedProduct
                : productsInSurvey &&
                  productsInSurvey[0] &&
                  productsInSurvey[0].id
            }
          })
          await resumeSurveyProgress({
            surveyId,
            lastAnsweredQuestionFromLogin: loggedLastAnsweredQuestion
          })
        }
      }}
      requireRecaptcha={settings.recaptcha}
      buttonLabel={buttonLabel}
      loginText={loginText}
      pauseText={pauseText}
      country={country}
      eligibleCountryUser={eligibleCountryUser}
    />
  )
}

const SAVE_SURVEY_PARTICIPATION = gql`
  mutation saveCurrentSurveyParticipation($surveyId: String) {
    saveCurrentSurveyParticipation(surveyId: $surveyId) @client
  }
`

const RESET_CACHE = gql`
  mutation resetCache {
    resetCache @client
  }
`

const COMPULSORY_SURVEY_QUERY = gql`
  query compulsorySurveyCountry {
    compulsorySurveyCountry {
      id
      state
    }
  }
`

export default withRouter(SurveyLogin)
