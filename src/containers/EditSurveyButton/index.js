import React, { useState } from 'react'
import { withRouter } from 'react-router-dom'
import IconButton from '../../components/IconButton'
import { useTranslation } from 'react-i18next'
import ConfirmEditMode from './ConfirmEditMode'
import { ButtonWrapper } from './styles'

const EditSurveyButton = ({ surveyId, history, label, noActive }) => {
  const { t } = useTranslation()
  const [modalVisible, setModalVisible] = useState(false)
  const onClick = () => {
    setModalVisible(true)
  }

  return (
    <React.Fragment>
      <ButtonWrapper onClick={onClick}>
        <IconButton
          tooltip={!label && t('tooltips.editSurveyButton')}
          type='edit'
          noActive={noActive}
        />
        {label && t('tooltips.editSurveyButton')}
      </ButtonWrapper>
      <ConfirmEditMode
        visible={modalVisible}
        onOk={({ editMode }) => {
          if (editMode === 'stricted') {
            history.push(`/operator/survey/stricted-edit/${surveyId}`)
          } else {
            history.push(`/operator/survey/edit/${surveyId}`)
          }
        }}
        onCancel={() => {
          setModalVisible(false)
        }}
      />
    </React.Fragment>
  )
}

export default withRouter(EditSurveyButton)
