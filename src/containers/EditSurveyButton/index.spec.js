import React from 'react'
import { mount } from 'enzyme'
import EditSurveyButton from '.'
import AlertModal from '../../components/AlertModal'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import { Router } from 'react-router-dom'
import history from '../../history'
import IconButton from '../../components/IconButton'

jest.mock('../../components/AlertModal')

const mockSurveyCreation = {
  questions: [
    {
      type: 'vertical-rating',
      range: {
        labels: ['1', '2']
      }
    },
    {
      type: 'vertical-rating',
      range: {
        labels: ['1', '3']
      }
    }
  ]
}

const mockAlertModal = ({ handleOk, handleCancel }) => {
  handleOk()
  handleCancel()
}
AlertModal.mockImplementation(mockAlertModal)

describe('EditSurveyButton', () => {
  let testRender
  let surveyId
  let surveyState
  let mockClient

  beforeEach(() => {
    surveyId = 1
    surveyState = 'status'
    mockClient = createApolloMockClient()
    mockClient.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: mockSurveyCreation
      }
    })
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render EditSurveyButton', async () => {
    testRender = mount(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <EditSurveyButton
            surveyId={surveyId}
            surveyState={surveyState}
            history={history}
          />
        </Router>
      </ApolloProvider>
    )
    expect(testRender.find(EditSurveyButton)).toMatchSnapshot()
    // expect(testRender.find(EditSurveyButton)).toHaveLength(1)
  })

  test('should call on click on iconButton', async () => {
    testRender = mount(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <EditSurveyButton
            surveyId={surveyId}
            surveyState={'edit'}
            history={history}
          />
        </Router>
      </ApolloProvider>
    )

    testRender.find(IconButton).simulate('click')

    expect(global.document.querySelectorAll('.ant-modal')).not.toBeNull()
    expect(global.document.querySelector('.ant-modal-confirm-title')).toBeNull()
  })
})
