import React, { useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import Presentation from '../../components/OperatorActiveSurveys'
import useSurveys from '../../hooks/useSurveys'
import useAllOrganizations from '../../hooks/useAllOrganizations'
import getQueryParams from '../../utils/getQueryParams'
import OperatorPage from '../../components/OperatorPage'
import OperatorPageContent from '../../components/OperatorPageContent'

const OperatorSharedWithMe = ({ location, history, match }) => {
  const { t } = useTranslation()
  const { page = 0, keyword, country } = getQueryParams(location)
  const { isOutOfSize = false, surveys, total, loading } = useSurveys({
    page,
    keyword,
    country,
    onlyShared: true
  })
  const { loading: loadingOrg, organizations } = useAllOrganizations()

  useEffect(() => {
    if(isOutOfSize) {
      history.push(match.url)
    }
  }, [isOutOfSize])

  const onFilter = (key, value) => {
    const urlSearchParams = new URLSearchParams(location.search)

    if (value) {
      urlSearchParams.set(key, value)
    } else {
      urlSearchParams.delete(key)
    }

    history.push(`${match.url}?${urlSearchParams.toString()}`)
  }
  
  return (
    <OperatorPage>
      <OperatorPageContent>
        <Presentation
          pageTitle={t('components.operatorSurveys.sharedWithMe')}
          loading={loading || loadingOrg}
          keyword={keyword}
          country={country}
          onFilter={onFilter}
          surveys={surveys}
          organizations={organizations}
          total={total}
          page={Number(page)}
          onChangePage={pageNumber => {
            history.push(`${match.url}?page=${pageNumber || 0}`)
          }}
        />
      </OperatorPageContent>
    </OperatorPage>
  )
}

export default withRouter(OperatorSharedWithMe)
