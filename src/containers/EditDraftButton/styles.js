import styled from 'styled-components'

export const ButtonWrapper = styled.div `
    display: flex;
    flex-direction: row;
    cursor: pointer;
`