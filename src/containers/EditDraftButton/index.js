import React from 'react'
import IconButton from '../../components/IconButton'
import { withRouter } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { ButtonWrapper } from './styles'

const EditDraftButton = ({ surveyId, history, match, label, noActive }) => {
  const { t } = useTranslation()
  return (
    <ButtonWrapper
      onClick={() => history.push(`/operator/draft/edit/${surveyId}`)}
    >
      <IconButton
        tooltip={!label && t('tooltips.editDraftButton')}
        type='edit'
        noActive={noActive}
      />
      {label && t('tooltips.editDraftButton')}
    </ButtonWrapper>
  )
}

export default withRouter(EditDraftButton)
