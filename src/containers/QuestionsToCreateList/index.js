import React from 'react'
import { withRouter } from 'react-router-dom'
import QuestionCreationCard from '../../components/QuestionCreationCard'
import getQueryParams from '../../utils/getQueryParams'

const QuestionsToCreateList = ({
  location,
  questions,
  unsetMandatory,
  unsetDisplaySurveyNameOnAll,
  displaySurveyNameOnAll,
  unsetPicture,
  allToMandatory,
  allToPicture,
  compulsorySurvey,
  reduceRewardInTasting,
  showInPreferedLanguage,
  addDelayToSelectNextProductAndNextQuestion
}) => {
  let questionIndexInSection = 0
  const { section } = getQueryParams(location)

  return (
    <React.Fragment>
      {questions.map((question, index) => {
        if (section && question.displayOn !== section) {
          return null
        }
        
        return (
          <QuestionCreationCard
            key={index}
            questionIndex={index}
            questionIndexInSection={questionIndexInSection++}
            question={question}
            mandatory={question.required}
            unsetMandatory={unsetMandatory}
            unsetDisplaySurveyNameOnAll={unsetDisplaySurveyNameOnAll}
            allToMandatory={allToMandatory}
            displaySurveyNameOnAll={displaySurveyNameOnAll}
            unsetPicture={unsetPicture}
            allToPicture={allToPicture}
            compulsorySurvey={compulsorySurvey}
            reduceRewardInTasting={reduceRewardInTasting}
            showInPreferedLanguage={showInPreferedLanguage}
            addDelayToSelectNextProductAndNextQuestion={addDelayToSelectNextProductAndNextQuestion}
          />
        )
      })}
    </React.Fragment>
  )
}

export default withRouter(QuestionsToCreateList)
