import React from 'react'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import SURVEY_PARTICIPATION_QUERY from '../../queries/SurveyParticipationQuery'
import { surveyBasicInfo } from '../../fragments/survey'
import SurveySharingComponent from '../../components/SurveySharing'

const SurveySharing = ({ isDesktop, centerAlign }) => {
  const { REACT_APP_THEME } = process.env
  const {
    data: {
      currentSurveyParticipation: { surveyId, surveyEnrollmentId }
    }
  } = useQuery(SURVEY_PARTICIPATION_QUERY)

  const {
    data: { survey, surveyEnrollment }
  } = useQuery(SURVEY_QUERY, {
    variables: { id: surveyId, enrollmentId: surveyEnrollmentId }
  })
  if (
    survey &&
    surveyEnrollment &&
    surveyEnrollmentId &&
    REACT_APP_THEME === 'default'
  ) {
    return (
      <SurveySharingComponent
        isDesktop={isDesktop}
        survey={survey}
        surveyEnrollment={surveyEnrollmentId}
        centerAlign={centerAlign}
        enrollmentReferralAmount={
          surveyEnrollment.enrollmentReferralAmount &&
          surveyEnrollment.enrollmentReferralAmount
        }
      />
    )
  }
  return null
}

const SURVEY_QUERY = gql`
  query survey($id: ID, $enrollmentId: ID) {
    survey(id: $id) {
      ...surveyBasicInfo
      screeners {
        id
        uniqueName
        isPaypalSelected
        isGiftCardSelected
        customizeSharingMessage
        referralAmount
        showSharingLink
      }
    }
    surveyEnrollment(id: $enrollmentId) {
      enrollmentReferralAmount
      referral
    }
  }

  ${surveyBasicInfo}
`
export default SurveySharing
