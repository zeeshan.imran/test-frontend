import React, { useCallback } from 'react'
import gql from 'graphql-tag'
import { useMutation } from 'react-apollo-hooks'
import OperatorQrCodeDownloadComponent from '../../components/OperatorQrCodeDownload'

const OperatorQrCodeDownload = ({ qrCode, onCancel }) => {
  const downloadQrCode = useMutation(DOWNLOAD_QR_CODE)

  const handleDownload = useCallback(
    async (qrCode, format) => {
      const { data } = await downloadQrCode({
        variables: {
          qrCodeId: qrCode.id,
          format
        }
      })

      var link = document.createElement('a')
      link.href = data.imageUrl
      link.download = `${qrCode.uniqueName}.${format}`
      link.target = '_blank'
      document.body.appendChild(link)
      link.click()
      document.body.removeChild(link)
    },
    [downloadQrCode]
  )

  return (
    <OperatorQrCodeDownloadComponent
      qrCode={qrCode}
      onDownload={handleDownload}
      onCancel={onCancel}
    />
  )
}

const DOWNLOAD_QR_CODE = gql`
  mutation downloadQrCode($qrCodeId: ID!, $format: ImageFormat!) {
    imageUrl: downloadQrCode(id: $qrCodeId, format: $format)
  }
`

export default OperatorQrCodeDownload
