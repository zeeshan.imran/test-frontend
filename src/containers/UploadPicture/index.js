import React, { useState } from 'react'
import imageCompression from 'browser-image-compression'
import axios from 'axios'
import UploadPictureComponent from '../../components/UploadPicture'
import { useApolloClient } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { displayErrorPopup } from '../../utils/displayErrorPopup/index'
import i18n from '../../utils/internationalization/i18n'

const GET_SIGNED_URL = gql`
  query getSignedUrl($type: String, $surveyId: ID) {
    getSignedUrl(type: $type, surveyId: $surveyId) {
      signedUrl
      fileLink
    }
  }
`

const optionsForImageUpload = {
  maxSizeMB: 1,
  maxWidthOrHeight: 1920,
  useWebWorker: true
}

const ALLOWED_TYPES = ['image/png', 'image/jpeg', 'image/gif']

const handleUpload = async (
  file,
  client,
  onChange,
  setUploading,
  setUploadError, 
  surveyId
) => {
  setUploading(true)
  try {
    let fileToUpload = file
    const { type, size } = fileToUpload

    if (!ALLOWED_TYPES.includes(type)) {
      setUploading(false)
      setUploadError(true)
      return displayErrorPopup(i18n.t('errors.uploadProductPictureFailed'))
    }

    if (size / 1024 / 1024 > 1) {
      let compressedFile = await imageCompression(
        fileToUpload,
        optionsForImageUpload
      )
      while (
        compressedFile &&
        compressedFile.size &&
        compressedFile.size / 1024 / 1024 > 1
      ) {
        compressedFile = await imageCompression(
          compressedFile,
          optionsForImageUpload
        )
      }
      fileToUpload = compressedFile
    }
    
    const {
      data: {
        getSignedUrl: { signedUrl, fileLink }
      }
    } = await client.query({
      query: GET_SIGNED_URL,
      variables: { type: fileToUpload && fileToUpload.type, surveyId },
      fetchPolicy: 'no-cache'
    })

    const result = await axios.put(signedUrl, fileToUpload, {
      headers: {
        'Content-Type': fileToUpload && fileToUpload.type
      }
    }).catch((error) => {
      // handle error
      throw(error)
    })
    
    if (result.status !== 200) {
      setUploadError(true)
      return displayErrorPopup(i18n.t('errors.uploadPictureFailed'))
    }
    
    setUploadError(false)
    setUploading(false)
    onChange([fileLink])
  } catch (error) {
    setUploading(false)
    setUploadError(true)
    return displayErrorPopup(i18n.t('errors.uploadPictureFailed'))
  }
}

const UploadPicture = ({ onChange, surveyId }) => {
  const client = useApolloClient()
  const [uploading, setUploading] = useState(false)
  const [uploadError, setUploadError] = useState(false)
  return (
    <UploadPictureComponent
      error={uploadError}
      loading={uploading}
      onFileUpload={({ file }) =>
        handleUpload(file, client, onChange, setUploading, setUploadError, surveyId)
      }
    />
  )
}

export default UploadPicture
