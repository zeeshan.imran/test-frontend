import React, { useMemo } from 'react'
import gql from 'graphql-tag'
import { withRouter } from 'react-router-dom'
import { useQuery } from 'react-apollo-hooks'
import { useTranslation } from 'react-i18next'
import QuestionCreationCard from '../../components/QuestionCreationCard'
import OperatorSection from '../../components/OperatorSection'
import getQueryParams from '../../utils/getQueryParams'

const MandatoryQuestionsToCreate = ({ location }) => {
  const { t } = useTranslation()
  const { section } = getQueryParams(location)
  const {
    data: {
      surveyCreation: { mandatoryQuestions }
    }
  } = useQuery(SURVEY_CREATION)

  const tabMandatoryQuestions = useMemo(() => {
    if (!mandatoryQuestions || mandatoryQuestions.length === 0) {
      return []
    }

    return mandatoryQuestions.filter(mandatoryQuestion => {
      return mandatoryQuestion.displayOn === section
    })
  }, [section, mandatoryQuestions])

  if (tabMandatoryQuestions.length === 0) {
    return null
  }

  return (
    <React.Fragment>
      <OperatorSection title={t('surveyCreation.mandatoryQuestion')} />
      {tabMandatoryQuestions.map((question, index) => {
        return (
          <QuestionCreationCard
            mandatory
            key={index}
            questionIndex={index}
            question={{
              ...question,
              skipFlow: {
                ...question.skipFlow
              }
            }}
          />
        )
      })}
    </React.Fragment>
  )
}

const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      mandatoryQuestions
    }
  }
`

export default withRouter(MandatoryQuestionsToCreate)
