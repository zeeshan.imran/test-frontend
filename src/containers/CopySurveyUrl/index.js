import React from 'react'
import CopyToClipboard from '../../components/CopyToClipboard'
import { useTranslation } from 'react-i18next'
import { getShareLink } from '../../utils/shareUtils'

const CopySurveyUrlButton = ({ uniqueName, label, noActive }) => {
  const { t } = useTranslation()
  return (
    <CopyToClipboard
      target={getShareLink(uniqueName)}
      tooltip={!label && t('tooltips.copySurveyUrlButton')}
      label={label && t('tooltips.copySurveyUrlButton')}
      noActive={noActive}
    />
  )
}

export default CopySurveyUrlButton
