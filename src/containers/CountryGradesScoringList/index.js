import React, { useState } from 'react'
import gql from 'graphql-tag'
import { useQuery, useMutation } from 'react-apollo-hooks'
import { withTranslation, useTranslation } from 'react-i18next'
import { omit } from 'ramda'
import { displaySuccessMessage } from '../../utils/displaySuccessMessage'
import { displayErrorPopup } from '../../utils/displayErrorPopup'
import CountryGradesScoringComponent from '../../components/CountryGradesScoringList'

const getError = error => {
  return error && error.graphQLErrors && error.graphQLErrors[0]
    ? error.graphQLErrors[0]['message']
    : null
}

const CountryGradesScoringContainer = ({ history, location }) => {
  const { t } = useTranslation()
  const [scoringModal, setScoringModal] = useState(false)
  const [scoringEditionType, setScoringEditionType] = useState('')
  const [requestloading, setRequestloading] = useState(false)

  const { data, loading, refetch: refetchUpdated } = useQuery(
    GET_COUNTRY_GRADES_SCORING,
    {
      fetchPolicy: 'network-only'
    }
  )

  const addCountryGradesScoringMutation = useMutation(
    ADD_COUNTRY_GRADES_SCORING
  )
  const updateCountryGradesScoringMutation = useMutation(
    UPDATE_COUNTRY_GRADES_SCORING
  )

  const closeScoringModal = () => {
    setScoringEditionType('')
    setScoringModal(false)
  }

  const addCountryGradesScoring = () => {
    setScoringEditionType('add')
    setScoringModal(true)
  }

  const editCountryGradesScoring = countryGrades => {
    setScoringModal(true)
    setScoringEditionType(countryGrades)
  }

  const addGrading = async (country, grades) => {
    setRequestloading(true)
    try {
      const CreateCountrySurveyGradingInput = {
        country: country,
        grades: grades
      }
      await addCountryGradesScoringMutation({
        variables: {
          input: CreateCountrySurveyGradingInput
        }
      })
      displaySuccessMessage(t('containers.countryGradesScoring.sucessMessage'))
      setRequestloading(false)
      closeScoringModal()
      await refetchUpdated()
    } catch (error) {
      const e = getError(error)
      displayErrorPopup(t( e ? `errors.${e}` : 'containers.countryGradesScoring.errorMessage'))
      setRequestloading(false)
    }
  }

  const editGrading = async (id, country, grades) => {
    setRequestloading(true)
    try {
      const newGrades = grades.map(g => {
        const grade = omit(['__typename'], g)
        const score = omit(['__typename'], g.scoreRange)
        return {
          ...grade,
          scoreRange: score
        }
      })
      const UpdateCountrySurveyGradingInput = {
        id: id,
        country: country,
        grades: newGrades
      }
      await updateCountryGradesScoringMutation({
        variables: {
          input: UpdateCountrySurveyGradingInput
        }
      })
      displaySuccessMessage(t('containers.countryGradesScoring.updateMessage'))
      setRequestloading(false)
      closeScoringModal()
      await refetchUpdated()
    } catch (error) {
      const e = getError(error)
      displayErrorPopup(t( e ? `errors.${e}` : 'containers.countryGradesScoring.errorMessage'))
      setRequestloading(false)
    }
  }

  return (
    <CountryGradesScoringComponent
      t={t}
      loading={loading}
      countryGradesScoringList={(data && data.getCountrySurveyGrading) || []}
      addCountryGradesScoring={addCountryGradesScoring}
      editCountryGradesScoring={editCountryGradesScoring}
      scoringModal={scoringModal}
      closeScoringModal={closeScoringModal}
      scoringEditionType={scoringEditionType}
      addGrading={addGrading}
      editGrading={editGrading}
      requestloading={requestloading}
    />
  )
}

const GET_COUNTRY_GRADES_SCORING = gql`
  query getCountrySurveyGrading {
    getCountrySurveyGrading {
      id
      country
      grades {
        grade
        scoreRange {
          start
          end
        }
      }
      createdBy {
        id
        fullName
      }
    }
  }
`

const ADD_COUNTRY_GRADES_SCORING = gql`
  mutation addCountrySurveyGrading($input: CreateCountrySurveyGradingInput!) {
    addCountrySurveyGrading(input: $input) {
      id
    }
  }
`

const UPDATE_COUNTRY_GRADES_SCORING = gql`
  mutation updateCountrySurveyGrading($input: UpdateCountrySurveyGradingInput) {
    updateCountrySurveyGrading(input: $input) {
      id
    }
  }
`

export default withTranslation()(CountryGradesScoringContainer)
