import React from 'react'
import { mount } from 'enzyme'
import TermsOfUseContainer from '.'
import TermsOfUse from '../../components/TermsOfUse'
import { act } from 'react-dom/test-utils'
import wait from '../../utils/testUtils/waait'

describe('TermsOfUseContainer', () => {
  let testRender
  let location
  let handleChange
  let mockScrollTo = jest.fn()
  let goToTermsOfUse

  beforeEach(() => {
    location = {
      pathname: '/survey/privacy-policy'
    }
    handleChange = jest.fn()
    global.window.scrollTo = mockScrollTo
    goToTermsOfUse = () => {
      history.push('/privacy-policy')
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render TermsOfUseContainer', () => {
    const historyData = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      }
    }

    testRender = mount(
      <TermsOfUseContainer
        history={historyData}
        location={location}
        handleChange={handleChange}
        goToTermsOfUse={goToTermsOfUse}
      />
    )
    expect(TermsOfUseContainer).toHaveLength(1)
  })

  test('should change route and scroll to top', async () => {
    const historyData = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      }
    }

    testRender = mount(
      <TermsOfUseContainer history={historyData} location={location} />
    )
    act(() => {
      testRender.find(TermsOfUse).prop('goToPrivacyPolicy')()
    })
    testRender.update()
    await wait(0)
    expect(historyData.push).toHaveBeenCalledWith('/privacy-policy')
  })
})
