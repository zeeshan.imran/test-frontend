import React, { useCallback, useMemo } from 'react'
import { path } from 'ramda'
import { useQuery, useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import camelCase from 'lodash.camelcase'
import EmailSettings from '../../components/OperatorSurveyCreateEmailSettings'
import defaultSurveyCreation from '../../defaults/surveyCreation'

const getEnabledEmailTypes = path([
  'surveyCreation',
  'basics',
  'enabledEmailTypes'
])

const getEmails = path(['surveyCreation', 'basics', 'emails'])

const OperatorSurveyCreateSettings = () => {
  const { data } = useQuery(SURVEY_CREATION)
  const updateSurveyBasics = useMutation(UPDATE_SURVEY_CREATION_BASICS)

  const enabledEmailTypes = getEnabledEmailTypes(data) || []
  const emails = getEmails(data)

  const emailSettings = useMemo(
    () => ({
      enabledEmailTypes,
      emails
    }),
    [enabledEmailTypes, emails]
  )

  const handleEmailTypesChange = useCallback(
    async enabledEmailTypes => {
      await updateSurveyBasics({
        variables: {
          basics: {
            ...emailSettings,
            enabledEmailTypes,
            emails: enabledEmailTypes.map(camelCase).reduce(
              (acc, type) => ({
                ...acc,
                [type]:
                  emailSettings.emails[type] ||
                  defaultSurveyCreation.surveyCreation.basics.emails[type]
              }),
              emailSettings.emails
            )
          }
        }
      })
    },
    [updateSurveyBasics, emailSettings]
  )

  const handleEmailTemplateChange = useCallback(
    async (emailType, template) => {
      await updateSurveyBasics({
        variables: {
          basics: {
            emails: {
              ...emails,
              [emailType]: {
                ...emails[emailType],
                ...template
              }
            }
          }
        }
      })
    },
    [updateSurveyBasics, emails]
  )

  return (
    <EmailSettings
      emailSettings={emailSettings}
      onEmailTypesChange={handleEmailTypesChange}
      onEmailTemplateChange={handleEmailTemplateChange}
    />
  )
}

export const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      basics {
        enabledEmailTypes
        emails {
          surveyWaiting {
            subject
            html
            text
          }
          surveyCompleted {
            subject
            html
            text
          }
          surveyRejected {
            subject
            html
            text
          }
        }
      }
    }
  }
`

export const UPDATE_SURVEY_CREATION_BASICS = gql`
  mutation updateSurveyCreationBasics($basics: basics) {
    updateSurveyCreationBasics(basics: $basics) @client
  }
`

export default OperatorSurveyCreateSettings
