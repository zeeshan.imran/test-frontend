import styled from 'styled-components'
import { Modal as AntModal, Button } from 'antd'

export const StyledModal = styled(AntModal)`
  .ant-modal-body {
    padding: 0rem;
    min-width: 600px;
  }
  .ant-modal-content {
    min-width: 650px;
  }
`
export const StyledButton = styled(Button)`
  margin-left: 12px;
`
export const FooterBtns = styled.div`
  margin-bottom: 12px;
`
