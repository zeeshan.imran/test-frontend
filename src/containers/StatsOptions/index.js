import React, { useState, useEffect, useRef } from 'react'
import { Button, Icon, Dropdown } from 'antd'
import { StyledModal, StyledButton, FooterBtns } from './styles'
import StatsOptionsForm from '../../components/StatsOptionsForm'
import { withTranslation } from 'react-i18next'
import ChartDownloadMenu from '../../components/ChartDownloadMenu'
import {
  getDefaultChartSettingsByChart,
  getDefaultChartSettingsByStatistic
} from '../../utils/chartSettings'
import AlertModal from '../../components/AlertModal'
import {
  isUserAuthenticatedAsAnalytics
} from '../../utils/userAuthentication'

const StatsOptions = ({
  chartSettings,
  saveChartsSettings,
  resetSimilarSettings,
  chartData,
  isOwner,
  t,
  chartId,
  isAdditionalMenuShown = false,
  isStatisticsSettings = false
}) => {
  const isAnalytics = isUserAuthenticatedAsAnalytics()
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [settings, setSettings] = useState({})
  const chartSettingsRef = useRef()
  const [downloading, setDownloading] = useState(false)

  useEffect(() => {
    if (chartSettingsRef.current !== chartSettings) {
      setSettings(chartSettings)
      chartSettingsRef.current = chartSettings
    }
  })

  const setNewSettings = newSettings => {
    setSettings({ ...settings, ...newSettings })
  }

  const saveSettings = tag => {
    saveChartsSettings(
      `${chartData.question}-${chartData.chart_type}`,
      settings,
      tag
    )
    setIsModalVisible(false)
  }

  const resetSettings = () => {
    const newSettings = isStatisticsSettings
      ? getDefaultChartSettingsByStatistic(chartData.chart_type)
      : getDefaultChartSettingsByChart(chartData)
    setSettings({ ...newSettings })
    saveChartsSettings(
      `${chartData.question}-${chartData.chart_type}`,
      newSettings
    )
    setIsModalVisible(false)
  }

  const showResetConfirmationModal = () => {
    AlertModal({
      title: t('settings.charts.alertModalResetTitle'),
      description: t('settings.charts.alertModalResetIndividualDescription'),
      okText: t('settings.charts.resetDefaults'),
      handleOk: () => {
        resetSettings()
        setIsModalVisible(false)
      },
      handleCancel: () => {}
    })
  }

  const showResetSimilarConfirmationModal = () => {
    AlertModal({
      title: t('settings.charts.alertModalResetTitle'),
      description: t('settings.charts.alertModalResetSimilarDescription'),
      okText: t('settings.charts.resetDefaults'),
      handleOk: () => {
        resetSimilarSettings(chartData.chart_type)
        setIsModalVisible(false)
      },
      handleCancel: () => {}
    })
  }

  const showSetSimilarConfirmationModal = () => {
    AlertModal({
      title: t('settings.charts.alertModalSetSimilar'),
      description: t('settings.charts.alertModalSetSimilarDescription'),
      okText: t('settings.charts.setSimilar'),
      handleOk: () => {
        saveSettings(chartData.chart_type)
        setIsModalVisible(false)
      },
      handleCancel: () => {},
      hideIcon: true
    })
  }

  return (
    <React.Fragment>
      {!isAnalytics && isOwner && (
        <Button
          type='secondary'
          size='default'
          onClick={() => {
            setSettings(chartSettings)
            setIsModalVisible(true)
          }}
        >
          <Icon type='setting' />
        </Button>
      )}
      {isAdditionalMenuShown && (
        <Dropdown
          disabled={downloading}
          overlay={ChartDownloadMenu(
            chartId,
            chartData.title,
            t,
            setDownloading
          )}
          trigger={['click']}
        >
          <StyledButton type='secondary'>
            <Icon type='bars' />
          </StyledButton>
        </Dropdown>
      )}

      <StyledModal
        title={t('settings.charts.settingsPopupTitle', {
          chartName: chartData.title
        })}
        visible={isModalVisible}
        onCancel={() => {
          setIsModalVisible(false)
        }}
        footer={
          <div>
            {!isStatisticsSettings && (
              <FooterBtns>
                {resetSimilarSettings && (
                  <Button
                    key='0'
                    onClick={() => {
                      showResetSimilarConfirmationModal()
                    }}
                  >
                    {t('settings.charts.resetDefaultsSimilar')}
                  </Button>
                )}
                <Button
                  key='1'
                  type='primary'
                  onClick={() => {
                    showSetSimilarConfirmationModal()
                  }}
                >
                  {t('settings.charts.setSimilar')}
                </Button>
              </FooterBtns>
            )}
            <div>
              <Button
                key='2'
                onClick={() => {
                  setIsModalVisible(false)
                }}
              >
                {t('settings.charts.cancel')}
              </Button>
              <Button
                key='3'
                onClick={() => {
                  showResetConfirmationModal()
                }}
              >
                {t('settings.charts.resetDefaults')}
              </Button>
              <Button
                key='4'
                type='primary'
                onClick={() => {
                  saveSettings()
                }}
              >
                {t('settings.charts.set')}
              </Button>
            </div>
          </div>
        }
      >
        <StatsOptionsForm
          settings={settings}
          setSettings={setNewSettings}
          chartType={chartData.chart_type}
          questionType={chartData.question_type}
          hasProducts={chartData.question_with_products}
          isStatisticsSettings={isStatisticsSettings}
        />
      </StyledModal>
    </React.Fragment>
  )
}

export default withTranslation()(StatsOptions)
