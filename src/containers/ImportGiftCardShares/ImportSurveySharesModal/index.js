import React, { useState } from 'react'
import gql from 'graphql-tag'
import Modal from '../../../components/Modal'
import { Upload, Icon, message } from 'antd'
import { Title, ModalContent, Prompt, UploadContainer } from './styles'
import { withTranslation } from 'react-i18next'
import getFileData from '../../../utils/getFileData'
import csvTextToArray from '../../../utils/CsvTextToArray'
import { pluck } from 'ramda'
import { useMutation } from 'react-apollo-hooks'

const ImportSurveySharesModal = ({
  visible,
  state,
  survey,
  t,
  toggleModal
}) => {
  const [fileList, setFileList] = useState([])
  const [fileData, setFileData] = useState('')
  const [updatedCount, setUpdatedCount] = useState(0)
  // const [enrollmentIds, setEnrollmentIds] = useState([])
  const updateIncentives = useMutation(UPDATE_SURVEY_SHARE_STATUS)

  const beforeUpload = async file => {
    const isCsv = file.type === 'text/csv' ;
    if (!isCsv) {
      message.error('You can only upload CSV file!');
    }

    const isLt2M = file.size / 1024 / 1024 < 2
    if (!isLt2M) {
      message.error('File must smaller than 2MB!')
    }

    return isCsv && isLt2M
  }

  const handleChange = async info => {
    let fileList = [...info.fileList]
    fileList = fileList.slice(-1)
    if (info.file.status === 'done') {
      message.destroy()
      message.success(`${updatedCount} out of ${fileData.length} Records updated successfully`)
      fileList = []
    } else if (info.file.status === 'error') {
      message.error(`Import failed, Please check your file.`)
    }
    setFileList(fileList)
  }

  const updateData = async info => {
    const { onSuccess, onError, file } = info
    const data = await getFileData(file)
    const result = csvTextToArray(data)
    setFileData(result)
    const enrollments = pluck('id')(result)
    const {
      data: {
        updateSurveyShareStatus: { updatedRecords }
      }
    } = await updateIncentives({
      variables: {
        surveyID: survey.id,
        enrollments: enrollments
      }
    })
    if (updatedRecords > 0) {
      setUpdatedCount(updatedRecords)
      onSuccess(file)
      toggleModal()
    } else {
      onError(file)
    }
  }
  return (
    <Modal
      title={t('containers.importSurveyShares.title', { name: survey.name })}
      visible={visible}
      toggleModal={toggleModal}
    >
      <ModalContent>
        <Title>{t('containers.importSurveyShares.title', { name: survey.name })}</Title>
        <Prompt>{t('containers.importSurveyShares.prompt')}</Prompt>
        <UploadContainer>
          <Upload
            name={'hahah'}
            onChange={handleChange}
            multiple={false}
            accept={'.csv'}
            fileList={fileList}
            beforeUpload={beforeUpload}
            maskClosable
            customRequest={updateData}
          >
            <Icon type='upload' /> Click to Upload
          </Upload>
        </UploadContainer>
      </ModalContent>
    </Modal>
  )
}

export const UPDATE_SURVEY_SHARE_STATUS = gql`
  mutation updateSurveyShareStatus($surveyID: ID, $enrollments: [ID]) {
    updateSurveyShareStatus(surveyID: $surveyID, enrollments: $enrollments) {
      updatedRecords
    }
  }
`

export default withTranslation()(ImportSurveySharesModal)
