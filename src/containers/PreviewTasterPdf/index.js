import React, { useEffect, useMemo } from 'react'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import Page from './Page'
import * as pdfUtils from '../../utils/pdfUtils'
import useChartResults from '../../hooks/useChartResults'
import { getColWidth, getRowHeight, PAPER_SIZES } from '../../utils/paperSizes'
import useRenderingState from '../../hooks/useRenderingState'
import { TasterPdfContainer } from './styles'
import { ChartContextProvider } from '../../contexts/ChartContext'
import usePreviewPdf from '../PreviewPdf/usePreviewPdf'

const BACKEND_LAYOUT =
  localStorage.getItem('pdf-layout') &&
  JSON.parse(localStorage.getItem('pdf-layout'))

const PreviewTasterPdf = ({
  savedLayout = BACKEND_LAYOUT,
  jobGroupId,
  surveyId
}) => {
  const {
    loading,
    stats,
    settings,
    survey,
    tasterLayout: defaultLayout
  } = useChartResults({
    jobGroupId,
    surveyId
  })
  const { loading: loadingSurvey, data } = useQuery(QUERY_SURVEY, {
    variables: { surveyId }
  })
  const questions = useMemo(
    () =>
      data &&
      data.survey && [
        ...data.survey.setupQuestions,
        ...data.survey.productsQuestions,
        ...data.survey.paymentQuestions,
        ...data.survey.screeningQuestions,
        ...data.survey.finishingQuestions
      ],
    [data]
  )
  const { rendering, showRendering } = useRenderingState()
  const { layout, pages, applyLayout } = usePreviewPdf(
    survey,
    settings,
    'taster'
  )
  const paperSize = (layout && layout.paperSize) || 'A4'
  const paperSizeMeta = PAPER_SIZES[paperSize]
  const zoom = paperSizeMeta.printZoom
  const colWidth = getColWidth(paperSizeMeta) * zoom
  const rowHeight = getRowHeight(paperSizeMeta) * zoom

  useEffect(() => {
    if (!loading && !loadingSurvey) {
      const layout = savedLayout || defaultLayout

      const formatStats = pdfUtils.filterTasterStats(stats).map(chart => {
        const question = questions.find(q => q.id === chart.result.question)
        return {
          ...chart,
          result: {
            ...chart.result,
            title: (
              <span>
                <strong>Question:</strong>{' '}
                {(question && question.prompt) || chart.result.title}
              </span>
            )
          }
        }
      })

      applyLayout(formatStats, layout, null)
      showRendering(1000)
    }
  }, [
    loading,
    loadingSurvey,
    settings,
    stats,
    questions,
    paperSizeMeta,
    savedLayout,
    showRendering,
    defaultLayout,
    applyLayout
  ])

  window.__PAGE_HEIGHT = Math.ceil(rowHeight * paperSizeMeta.rows)
  return (
    <ChartContextProvider showAll printView>
      <TasterPdfContainer>
        {rendering && <div id='loading'>loading</div>}
        {pages.map((page, index) => (
          <Page
            key={`page-${index}`}
            paperSize={paperSize}
            colWidth={colWidth}
            rowHeight={rowHeight * 0.995}
            zoom={zoom}
            layout={page}
          />
        ))}
      </TasterPdfContainer>
    </ChartContextProvider>
  )
}

const QUERY_SURVEY = gql`
  query survey($surveyId: ID!) {
    survey(id: $surveyId) {
      id
      setupQuestions {
        id
        prompt
      }
      productsQuestions {
        id
        prompt
      }
      paymentQuestions {
        id
        prompt
      }
      screeningQuestions {
        id
        prompt
      }
      finishingQuestions {
        id
        prompt
      }
    }
  }
`

export default PreviewTasterPdf
