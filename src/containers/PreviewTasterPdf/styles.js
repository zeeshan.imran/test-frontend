import styled from 'styled-components'
import { fixChartPrint } from '../PreviewPdf/styles'

export const TasterPdfContainer = styled.div`
  .ant-card-head {
    font-weight: 400;
  }
  ${fixChartPrint}
`
