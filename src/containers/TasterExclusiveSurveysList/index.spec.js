import React from 'react'
import { mount } from 'enzyme'
import TasterExclusiveSurveysList from '.'
import gql from 'graphql-tag'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { Router } from 'react-router-dom'
import { getAuthenticatedUser } from '../../utils/userAuthentication'
import SurveysList from '../../components/SurveysList'

jest.mock('../../utils/userAuthentication')
jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k,
  getFixedT: () => () => ''
}))
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('TasterExclusiveSurveysList', () => {
  let testRender
  let client
  let historyMock

  beforeEach(() => {
    historyMock = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }

    client = createApolloMockClient()
    const EXCLUSIVE_SURVEYS_QUERY = gql`
      query user($userId: ID) {
        user(id: $userId) {
          surveyEnrollments {
            allowedDaysToFillTheTasting
            survey {
              allowedDaysToFillTheTasting
              id
            }
          }
          exclusiveSurveys {
            allowedDaysToFillTheTasting
            id
            name
            state
            allowRetakes
          }
        }
      }
    `

    client.cache.writeQuery({
      query: EXCLUSIVE_SURVEYS_QUERY,
      variables: { id: 1 },
      data: {
        user: {
          surveyEnrollments: [
            {
              survey: {
                id: 1
              }
            }
          ],
          exclusiveSurveys: [
            {
              id: 1,
              name: 'taster',
              state: 'draft',
              allowRetakes: false
            }
          ]
        }
      }
    })
  })

  afterEach(() => {
    testRender.unmount()
  })
  test('should render TasterExclusiveSurveysList', () => {
    getAuthenticatedUser.mockImplementation(() => ({
      isSuperAdmin: true
    }))

    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={historyMock}>
          <TasterExclusiveSurveysList history={historyMock} />
        </Router>
      </ApolloProvider>
    )
    expect(testRender).toMatchSnapshot()

    testRender.find(SurveysList).prop('handleNavigateToSurvey')({ id: '1' })
  })
})
