import React from 'react'
import { useQuery } from 'react-apollo-hooks'
import { pluck, pathEq, filter } from 'ramda'
import { withRouter } from 'react-router-dom'
import gql from 'graphql-tag'
import SurveysList from '../../components/SurveysList'
import ModalGetStarted from '../../components/SurveysList/ModalGetStarted'
import {
  getAuthenticatedUser,
  setAuthenticatedUser
} from '../../utils/userAuthentication'
import { SURVEY_AUTHORIZATION_TYPE } from '../../utils/Constants'

const moment = require('moment')
const TasterExclusiveSurveysList = ({ history, getCurrency }) => {
  let user = getAuthenticatedUser()

  const {
    data: {
      user: {
        exclusiveSurveys = [],
        surveyEnrollments = [],
        isCompulsorySurveyTaken
      } = {}
    } = {},
    loading
  } = useQuery(EXCLUSIVE_SURVEYS_QUERY, {
    variables: { userId: user.id },
    fetchPolicy: 'network-only'
  })
  user = {
    ...user,
    isCompulsorySurveyTaken
  }

  setAuthenticatedUser(user)

  let validSurveys = exclusiveSurveys.filter(
    survey =>
      survey.state === 'active' &&
      !survey.compulsorySurvey &&
      !survey.showInPreferedLanguage
  )

  let compulsorySurveys = exclusiveSurveys.filter(
    survey => survey.compulsorySurvey
  )

  let compulsorySurveysPrefLanguage =
    compulsorySurveys &&
    compulsorySurveys.length &&
    compulsorySurveys.filter(
      survey =>
        survey.showInPreferedLanguage && survey.surveyLanguage === user.language
    )

  if (compulsorySurveysPrefLanguage && compulsorySurveysPrefLanguage.length) {
    compulsorySurveys = compulsorySurveysPrefLanguage
  }

  const preferedLanguageSurveys = exclusiveSurveys.filter(
    survey =>
      !survey.compulsorySurvey &&
      survey.showInPreferedLanguage &&
      survey.surveyLanguage === user.language
  )

  validSurveys = [...validSurveys, ...preferedLanguageSurveys]

  let surveyJump = []
  let surveyList = []
  surveyList = validSurveys.map(item => {
    item.children = []
    if (item && item.linkedSurveys && item.linkedSurveys.length) {
      item.linkedSurveys.forEach(linkedSurvey => {
        let validSurvey = validSurveys.find(x => x.id === linkedSurvey.id)

        if (validSurvey) {
          validSurvey.hasScreener = true
          surveyJump.push(validSurvey.id)

          if (
            linkedSurvey.authorizationType ===
            SURVEY_AUTHORIZATION_TYPE.SELECTED
          ) {
            validSurvey = { ...validSurvey, ...{ key: validSurvey.id } }
            item.children.push(validSurvey)
          }
        }
      })
      return { ...item, ...{ key: item.id } }
    } else {
      return { ...item, ...{ key: item.id } }
    }
  })

  surveyList = surveyList.filter(i => !surveyJump.includes(i.id))
  const expandedRowKeys = pluck('id')(surveyList)

  const findClosedEnrollments = survey => {
    return surveyEnrollments.find(enrollment => {
      return (
        enrollment.survey.id === survey.id &&
        (enrollment.state === 'finished' || enrollment.state === 'rejected')
      )
    })
  }
  const computeTimeDifference = (createdAt, retakeAfter) => {
    const currentDate = moment()
    const upcomingDate = moment(createdAt).add(retakeAfter, 'days')
    const retakeAfterDays =
      upcomingDate > currentDate
        ? upcomingDate.diff(currentDate, 'day', true)
        : false
    return retakeAfterDays
      ? {
          retakeAfterString: `Retake after ${Math.round(
            retakeAfterDays
          )} day(s)`,
          disableRow: true
        }
      : { retakeAfterString: `Click to retake survey`, disableRow: false }
  }
  const saver = ({ savedRewards }) => {
    let sum = 0
    if (savedRewards.length) {
      sum = savedRewards
        .filter(({ answered }) => answered === true)
        .reduce((accumulator, product) => accumulator + product.reward, 0)
    }

    return sum
  }
  const findAllEnrollments = survey => {
    const isSurvey = pathEq(['survey', 'id'], survey.id)
    const newResult = filter(isSurvey, surveyEnrollments)

    if (newResult.length) {
      const totalRetake = newResult.length
      const totalEarned = newResult
        .filter(({ state }) => state === 'finished')
        .map(saver)
        .reduce((a, b) => a + b, 0)
      const { allowRetakes = false, maxRetake = 0, retakeAfter = 0 } = survey
      const { createdAt } = newResult[0] || {}
      const latestEnrollment = { ...newResult[0], ...{ totalEarned } }
      if (allowRetakes) {
        if (totalRetake < maxRetake) {
          const { retakeAfterString, disableRow } = computeTimeDifference(
            createdAt,
            retakeAfter
          )
          return {
            ...latestEnrollment,
            ...{
              totalRetake,
              retakeAfterString,
              disableRow
            }
          }
        } else {
          return { ...latestEnrollment, ...{ totalRetake, maxTimeTaken: true } }
        }
      }
      return { ...latestEnrollment, ...{ totalRetake } }
    }
  }

  const isExpired = survey => {
    if (!survey || survey.allowRetakes || !survey.allowedDaysToFillTheTasting) {
      return false
    }

    const enrollments = surveyEnrollments.filter(
      enrollment => enrollment.survey.id === survey.id
    )

    if (enrollments.length === 0) {
      return false
    }

    return enrollments.reduce((acc, enrollment) => {
      return (
        acc &&
        moment(enrollment.createdAt).add(
          enrollment.allowedDaysToFillTheTasting,
          'days'
        ) < moment()
      )
    }, true)
  }

  const checkEnrollment = survey => {
    const found = findClosedEnrollments(survey)
    if (!found || survey.allowRetakes) {
      history.push(`/survey/${survey.id}`)
    }
  }

  return (
    <React.Fragment>
      <SurveysList
        loading={loading}
        surveys={surveyList}
        allSurveyEnrollements={surveyEnrollments}
        expandedRowKeys={expandedRowKeys}
        handleNavigateToSurvey={checkEnrollment}
        findClosedEnrollments={findClosedEnrollments}
        findAllEnrollments={findAllEnrollments}
        getCurrency={getCurrency}
        compulsorySurveys={compulsorySurveys}
        isCompulsorySurveyTaken={isCompulsorySurveyTaken}
        isExpired={isExpired}
      />

      <ModalGetStarted
        loading={loading}
        isCompulsorySurveyTaken={isCompulsorySurveyTaken}
        compulsorySurveys={compulsorySurveys}
        onGetStarted={checkEnrollment}
      />
    </React.Fragment>
  )
}

const EXCLUSIVE_SURVEYS_QUERY = gql`
  query user($userId: ID) {
    user(id: $userId) {
      id
      initialized
      emailAddress
      type
      isSuperAdmin
      isTaster
      fullName
      isVerified
      dateofbirth
      country
      state
      language
      gender
      paypalEmailAddress
      ethnicity
      smokerKind
      foodAllergies
      marketResearchParticipation
      isProfileCompleted
      isCompulsorySurveyTaken
      compulsorySurveyId
      organization {
        name
        id
      }
      surveyEnrollments {
        id
        state
        savedRewards
        createdAt
        allowedDaysToFillTheTasting
        survey {
          id
          isScreenerOnly
          allowedDaysToFillTheTasting
          allowRetakes
          compulsorySurvey
          retakeAfter
          maxRetake
          maximumReward
        }
      }
      exclusiveSurveys {
        id
        name
        state
        coverPhoto
        allowRetakes
        retakeAfter
        maxRetake
        maximumReward
        isScreenerOnly
        compulsorySurvey
        showSurveyProductScreen
        showIncentives
        authorizationType
        country
        uniqueName
        title
        showSharingLink
        allowedDaysToFillTheTasting
        showInPreferedLanguage
        surveyLanguage
        screenOutSettings{
          reject
        }
        screenOut
        products {
          id
          reward
          isAvailable
        }
        linkedSurveys {
          id
          name
          authorizationType
          products {
            id
            reward
            isAvailable
          }
        }
        screeners {
          id
          uniqueName
          showSharingLink
        }
        productRewardsRule {
          active
          max
          min
          percentage
        }
        showUserProfileDemographics
      }
    }
  }
`

export default withRouter(TasterExclusiveSurveysList)
