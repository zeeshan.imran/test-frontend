import React, { useCallback } from 'react'
import { path } from 'ramda'
import { useQuery, useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { DEFAULT_TASTING_NOTES } from '../../utils/Constants'
import TastingNotes from '../../components/OperatorSurveyCreateTastingNotes'

const getTastingNotes = path([
  'surveyCreation',
  'basics',
  'tastingNotes'
])
const OperatorSurveyCreateTastingNotes = () => {
  const { data } = useQuery(SURVEY_CREATION)
  const updateSurveyBasics = useMutation(UPDATE_SURVEY_CREATION_BASICS)
  let tastingNotes = getTastingNotes(data) || DEFAULT_TASTING_NOTES

  const handleTastingNotesChange = useCallback(async (field, value) => {
    tastingNotes[field] = value;
    await updateSurveyBasics({
      variables: {
        basics: {
          tastingNotes
        }
      }
    })
  }, [updateSurveyBasics])

  return (
    <TastingNotes
      tastingNotes={tastingNotes}
      onFieldUpdate={handleTastingNotesChange}
    />
  )
}

export const SURVEY_CREATION = gql`
  query {
    surveyCreation @client {
      basics {
        tastingNotes {
          tastingId
          tastingLeader
          customer
          country
          dateOfTasting
          otherInfo
        }
      }
    }
  }
`

export const UPDATE_SURVEY_CREATION_BASICS = gql`
  mutation updateSurveyCreationBasics($basics: basics) {
    updateSurveyCreationBasics(basics: $basics) @client
  }
`

export default OperatorSurveyCreateTastingNotes
