import React, { useEffect } from 'react'
import { Redirect } from 'react-router-dom'
import { useQuery, useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import { surveyInfo } from '../../fragments/survey'
import FillerLoader from '../../components/FillerLoader'
import useProductsBrands from '../../hooks/useProductsBrands'
import useAllSurveyQuestions from '../../hooks/useAllQuestions'
import { getPathFromLocation } from '../../utils/getPathFromLocation'
import { ERRORS } from '../../utils/Constants'
import {
  isUserAuthenticatedAsOperator,
  isUserAuthenticatedAsPowerUser,
  isUserAuthenticatedAsSuperAdmin
} from '../../utils/userAuthentication'

const LoadSurvey = ({ match, location, history, stricted }) => {
  const resetCreationForm = useMutation(RESET_CREATION_FORM)
  const setOpenedAt = useMutation(SET_OPENED_AT)
  const loadSurveyBasicsToForm = useMutation(LOAD_SURVEY_BASICS_TO_FORM)
  const loadSurveyProductsToForm = useMutation(LOAD_SURVEY_PRODUCTS_TO_FORM)
  const loadSurveyQuestionsToForm = useMutation(LOAD_SURVEY_QUESTIONS_TO_FORM)
  const requireChooseProductStep = useMutation(REQUIRE_CHOOSE_PRODUCT)
  const unrequireChooseProductStep = useMutation(UNREQUIRE_CHOOSE_PRODUCT)
  const requirePaypalEmailStep = useMutation(REQUIRE_PAYPAL_EMAIL)
  const unrequirePaypalEmailStep = useMutation(UNREQUIRE_PAYPAL_EMAIL)

  const requirePaymentQuestion = useMutation(REQUIRE_PAYMENT_QUESTION)
  const unrequirePaymentQuestion = useMutation(UNREQUIRE_PAYMENT_QUESTION)

  const requireShowProductScreen = useMutation(REQUIRE_SHOW_PRODUCT_SCREEN)
  const unrequireShowProductScreen = useMutation(UNREQUIRE_SHOW_PRODUCT_SCREEN)

  const makeSurveyStrict = useMutation(MAKE_SURVEY_STRICT)
  const secondaryPath = getPathFromLocation(location)

  const {
    params: { surveyId }
  } = match
  setOpenedAt({ variables: { id: surveyId } })
  const { error, data: surveyResponse = {}, loading: loadingSurvey } = useQuery(
    GET_SURVEY,
    {
      variables: { id: surveyId, checkOrg: true },
      fetchPolicy: 'network-only'
    }
  )

  if (surveyResponse.survey && surveyResponse.survey.state === 'deprecated') {
    history.push('/operator/surveys')
  }

  if (
    surveyResponse.survey &&
    surveyResponse.survey.state === 'active' &&
    secondaryPath === 'draft'
  ) {
    history.push('/operator/survey/edit/' + surveyResponse.survey.id)
  }

  useEffect(() => {
    if (error && error.message.includes(ERRORS.NOT_AUTH_SURVEY)) {
      history.push('/operator/not-authorized')
    }
  }, [error])

  const {
    data: questionsResponse = {},
    loading: loadingQuestions
  } = useAllSurveyQuestions(surveyResponse.survey)

  const { products = [] } = surveyResponse.survey || {}
  const { data: brandsResponse, loading: loadingBrands } = useProductsBrands(
    products
  )


  if (
    !(
      isUserAuthenticatedAsOperator() ||
      isUserAuthenticatedAsPowerUser() ||
      isUserAuthenticatedAsSuperAdmin()
    )
  ) {
    history.push('/operator/dashboard')
  }

  if (loadingSurvey || loadingQuestions || loadingBrands) {
    return <FillerLoader loading />
  }

  const { survey = {} } = surveyResponse
  const { brands = [] } = brandsResponse
  const { questions = [] } = questionsResponse
  
  async function resetData() {
    await resetCreationForm()
    await loadSurveyBasicsToForm({ variables: { survey } })
    await loadSurveyProductsToForm({
      variables: { products, brands }
    })
    await loadSurveyQuestionsToForm({
      variables: {
        questions
      }
    })

    if (stricted) {
      await makeSurveyStrict()
    }

    const hasProducts = survey.products && survey.products.length > 0 //to show choose product question on single product as well
    if (hasProducts) {
      await requireChooseProductStep()
    } else {
      await unrequireChooseProductStep() 
    }

    // Is Paypal question required in screening ?
    const hasReferralAmount = survey.referralAmount

    if (survey.isPaypalSelected && survey.isGiftCardSelected) {
      await requirePaymentQuestion()
    } else {
      await unrequirePaymentQuestion()
    }

    if (survey.showSurveyProductScreen){
      await requireShowProductScreen()
    } else {
      await unrequireShowProductScreen()
    }

    if (survey.isPaypalSelected && parseInt(hasReferralAmount, 10)) {
      await requirePaypalEmailStep()
    } else {
      await unrequirePaypalEmailStep()
    }
  }
  
  resetData();

  return <Redirect to={`${match.url}/basics`} />
}

export const SET_OPENED_AT = gql`
  mutation setOpenedAt($id: ID) {
    setOpenedAt(id: $id)
  }
`

export const GET_SURVEY = gql`
  query survey($id: ID, $checkOrg: Boolean) {
    survey(id: $id, checkOrg: $checkOrg) {
      ...surveyInfo
      state
    }
  }

  ${surveyInfo}
`

export const RESET_CREATION_FORM = gql`
  mutation resetCreationForm {
    resetCreationForm @client
  }
`

export const LOAD_SURVEY_BASICS_TO_FORM = gql`
  mutation loadSurveyBasicsToForm($survey: Survey) {
    loadSurveyBasicsToForm(survey: $survey) @client
  }
`

export const LOAD_SURVEY_PRODUCTS_TO_FORM = gql`
  mutation loadSurveyProductsToForm($products: [Product], $brands: [Brand]) {
    loadSurveyProductsToForm(products: $products, brands: $brands) @client
  }
`

export const LOAD_SURVEY_QUESTIONS_TO_FORM = gql`
  mutation loadSurveyQuestionsToForm($questions: [Question]) {
    loadSurveyQuestionsToForm(questions: $questions) @client
  }
`

export const REQUIRE_CHOOSE_PRODUCT = gql`
  mutation requireChooseProduct {
    requireChooseProduct(id: "") @client
  }
`

export const UNREQUIRE_CHOOSE_PRODUCT = gql`
  mutation unrequireChooseProduct {
    unrequireChooseProduct(id: "") @client
  }
`

export const REQUIRE_PAYPAL_EMAIL = gql`
  mutation requirePaypalEmail {
    requirePaypalEmail(id: "") @client
  }
`

export const UNREQUIRE_PAYPAL_EMAIL = gql`
  mutation unrequirePaypalEmail {
    unrequirePaypalEmail(id: "") @client
  }
`

export const REQUIRE_PAYMENT_QUESTION = gql`
  mutation requirePaymentQuestion {
    requirePaymentQuestion(id: "") @client
  }
`

export const UNREQUIRE_PAYMENT_QUESTION = gql`
  mutation unrequirePaymentQuestion {
    unrequirePaymentQuestion(id: "") @client
  }
`

export const REQUIRE_SHOW_PRODUCT_SCREEN = gql`
  mutation requireShowProductScreen {
    requireShowProductScreen(id: "") @client
  }
`

export const UNREQUIRE_SHOW_PRODUCT_SCREEN = gql`
  mutation unrequireShowProductScreen {
    unrequireShowProductScreen(id: "") @client
  }
`


export const MAKE_SURVEY_STRICT = gql`
  mutation {
    setSurveyEditMode(editMode: "strict") @client
  }
`

export default LoadSurvey
