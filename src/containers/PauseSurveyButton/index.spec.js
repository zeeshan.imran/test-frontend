import React from 'react'
import { mount } from 'enzyme'
import PauseSurveyButton from '.'
import wait from '../../utils/testUtils/waait'
import gql from 'graphql-tag'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import PauseSurveyButtonComponent from '../../components/PauseSurveyButton'

jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k,
  getFixedT: () => () => ''
}))
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

const SURVEY_ID = 'survey-1'
const SURVEY_STATE_QUERY = gql`
  query survey($id: ID) {
    survey(id: $id) {
      id
      state
    }
  }
`
const RESUME_SURVEY = gql`
  mutation resumeSurvey($id: ID!) {
    resumeSurvey(id: $id) {
      id
      state
    }
  }
`

const PAUSE_SURVEY = gql`
  mutation suspendSurvey($id: ID!) {
    suspendSurvey(id: $id) {
      id
      state
    }
  }
`

const mockQuestionQuery = () => ({
  request: {
    query: SURVEY_STATE_QUERY,
    variables: {
      id: SURVEY_ID
    }
  },
  result: {
    data: {
      survey: {
        state: 'draft'
      }
    }
  }
})

const mockMutations = [
  {
    request: {
      query: RESUME_SURVEY,
      variables: {
        id: SURVEY_ID
      }
    },
    result: {
      data: {}
    }
  },
  {
    request: {
      query: PAUSE_SURVEY,
      variables: {
        id: SURVEY_ID
      }
    },
    result: {
      data: {}
    }
  }
]

describe('PauseSurveyButton', () => {
  window.localStorage = {
    getItem: () => '{}'
  }

  let testRender
  let surveyId
  let mockClient

  beforeEach(() => {
    surveyId = 'survey-1'
    mockClient = createApolloMockClient({
      mocks: [mockQuestionQuery(false), ...mockMutations]
    })
  })

  test('should render PauseSurveyButton', async () => {
    const history = createBrowserHistory()

    testRender = mount(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <PauseSurveyButton
            survey={{
              id: surveyId,
              state: 'active'
            }}
          />
        </Router>
      </ApolloProvider>
    )
    await wait(0)

    expect(testRender).toMatchSnapshot()

    testRender.find(PauseSurveyButtonComponent).prop('onClickPause')()

    await wait(0)

    testRender.find(PauseSurveyButtonComponent).prop('onClickResume')()

    await wait(0)

    testRender.unmount()
  })
})
