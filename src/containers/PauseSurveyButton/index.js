import React from 'react'
import { useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import PauseSurveyButtonComponent from '../../components/PauseSurveyButton'
import PubSub from 'pubsub-js'
import { PUBSUB } from '../../utils/Constants'

const PauseSurveyButton = ({ survey }) => {
  const pauseSurvey = useMutation(PAUSE_SURVEY)
  const resumeSurvey = useMutation(RESUME_SURVEY)

  const { state = '' } = survey

  const refreshPage = () => {
    PubSub.publish(PUBSUB.REFETCH_FOLDERS)
  }

  return (
    <PauseSurveyButtonComponent
      state={state}
      onClickPause={async () => {
        await pauseSurvey({ variables: { id: survey.id } })
        refreshPage()
      }}
      onClickResume={async () => {
        await resumeSurvey({ variables: { id: survey.id } })
        refreshPage()
      }}
    />
  )
}

const RESUME_SURVEY = gql`
  mutation resumeSurvey($id: ID!) {
    resumeSurvey(id: $id) {
      id
      state
    }
  }
`

const PAUSE_SURVEY = gql`
  mutation suspendSurvey($id: ID!) {
    suspendSurvey(id: $id) {
      id
      state
    }
  }
`

export default PauseSurveyButton
