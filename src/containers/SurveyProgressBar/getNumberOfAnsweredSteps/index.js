import { getIndexFromObject } from '../../../utils/getIndexFromObject'

const getNumberOfAnsweredSteps = ({
  step,
  section,
  nTimesSelectedAProduct,
  survey: {
    screeningQuestions,
    setupQuestions = [],
    productsQuestions = [],
    finishingQuestions = [],
    paymentsQuestions = [],
    maximumProducts
  } = {}
}) => {
  if (section === 'screening') {
    return getIndexFromObject(step, screeningQuestions, 'id')
  } else if (section === 'payments') {
    return getIndexFromObject(step, paymentsQuestions, 'id')
  } else {
    const linearPath = [
      ...setupQuestions,
      ...productsQuestions,
      ...finishingQuestions,
    ]
    const indexInLinearPath = getIndexFromObject(step, linearPath, 'id')
    const reachedLoop = indexInLinearPath >= setupQuestions.length

    if (!reachedLoop || maximumProducts === 1) {
      return indexInLinearPath
    }

    const isBeginningLoop = indexInLinearPath === setupQuestions.length

    const nLoopsDone = isBeginningLoop
      ? nTimesSelectedAProduct
      : nTimesSelectedAProduct - 1
    // at the start of the loop, the product is not registered as covered

    const totalStepsDoneInLoops = nLoopsDone * productsQuestions.length
    return indexInLinearPath + totalStepsDoneInLoops
  }
}

export default getNumberOfAnsweredSteps
