import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import matchMediaMock from 'match-media-mock'
import MutationObserver from 'mutation-observer'

global.window.matchMedia = matchMediaMock.create()
global.MutationObserver = MutationObserver 
configure({ adapter: new Adapter() })

let storage = {}
global.window.localStorage = {
  getItem: jest.fn(key => storage[key] || null),
  setItem: jest.fn((...args) => {
    if (args.length < 2) throw Error('2 argument required')
    const [key, value] = args
    storage[key] = value
  }),
  removeItem: jest.fn((...args) => {
    if (args.length < 1) throw Error('1 argument required')
    const [key] = args
    delete storage[key]
  }),
  clear: jest.fn(() => {
    storage = {}
  })
}

