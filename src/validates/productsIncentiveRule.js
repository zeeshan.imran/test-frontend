import * as Yup from 'yup'
import i18n from '../utils/internationalization/i18n'
import defaults from '../defaults'

const isFlavorwiki = process.env.REACT_APP_THEME === 'default'

const validationSchema = (isProductRewardsRule = true) =>
  !isFlavorwiki
    ? Yup.object()
    : Yup.object().shape({
        min: !isProductRewardsRule
          ? Yup.number().nullable()
          : Yup.number()
              .typeError(i18n.t('validation.products.rules.min.required'))
              .min(0, () => i18n.t('validation.products.rules.min.min'))
              .max(Yup.ref('max'), () =>
                i18n.t('validation.products.rules.min.max')
              )
              .required(i18n.t('validation.products.rules.min.required')),
        max: !isProductRewardsRule
          ? Yup.number().nullable()
          : Yup.number()
              .typeError(i18n.t('validation.products.rules.max.required'))
              .min(Yup.ref('min'), () =>
                i18n.t('validation.products.rules.max.min')
              )
              .max(defaults.MAX_LIMIT_REFERRAL, () =>
                i18n.t('validation.products.rules.max.max', {
                  max: defaults.MAX_LIMIT_REFERRAL
                })
              )
              .required(i18n.t('validation.products.rules.max.required')),
        percentage: !isProductRewardsRule
          ? Yup.number().nullable()
          : Yup.number()
              .typeError(
                i18n.t('validation.products.rules.percentage.required')
              )
              .min(0, () => i18n.t('validation.products.rules.percentage.min'))
              .max(100, () =>
                i18n.t('validation.products.rules.percentage.max')
              )
              .required(i18n.t('validation.products.rules.percentage.required'))
      })

export default validationSchema
