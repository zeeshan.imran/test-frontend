import * as Yup from 'yup'
import i18n from '../utils/internationalization/i18n'

const isFlavorwiki = process.env.REACT_APP_THEME === 'default'

const validationSchema = isProductRewardsRule =>
  Yup.object().shape({
    name: Yup.string()
      .test(
        'not-empty',
        i18n.t('validation.products.name'),
        value => !/^[ ]+$/.test(value)
      )
      .test(
        'not-empty',
        i18n.t('validation.products.nameWithoutSpaces'),
        value => /^(?! ).*/.test(value)
      )
      .required(i18n.t('validation.products.name')),
    brand: Yup.string()
      .test(
        'not-empty',
        i18n.t('validation.products.brand'),
        value => !/^[ ]+$/.test(value)
      )
      .test(
        'not-empty',
        i18n.t('validation.products.brandWithoutSpaces'),
        value => /^(?! ).*/.test(value)
      )
      .required(i18n.t('validation.products.brand')),
    reward: isProductRewardsRule || !isFlavorwiki
      ? null
      : Yup.number()
          .typeError(i18n.t('validation.products.rewardNumber'))
          .min(0, () => i18n.t('validation.products.reward'))
          .test(
            'not-empty',
            i18n.t('validation.products.rewardNumber'),
            value => !/^[ ]+$/.test(value)
          )
          .test(
            'not-empty',
            i18n.t('validation.products.rewardWithoutSpaces'),
            value => /^(?! ).*/.test(value)
          )
          .required(i18n.t('validation.products.required')),
    rejectedReward: isProductRewardsRule || !isFlavorwiki
      ? null
      : Yup.number()
          .typeError(i18n.t('validation.products.rewardNumber'))
          .min(0, () => i18n.t('validation.products.reward'))
          .max(Yup.ref('reward'), 'Min should be > Max')
          .test(
            'not-empty',
            i18n.t('validation.products.rewardNumber'),
            value => !/^[ ]+$/.test(value)
          )
          .test(
            'not-empty',
            i18n.t('validation.products.rewardWithoutSpaces'),
            value => /^(?! ).*/.test(value)
          )
          .required(i18n.t('validation.products.required')),
    photo: Yup.string().test(
      'is photo loading',
      i18n.t('validation.product.photoLoading'),
      photo => photo === '' || photo !== 'loading'
    ),
    amountInDollar: isProductRewardsRule || !isFlavorwiki
      ? null
      : Yup.number()
          .typeError(i18n.t('validation.products.rewardNumber'))
          .min(0, () => i18n.t('validation.products.reward'))
          .test(
            'not-empty',
            i18n.t('validation.products.rewardNumber'),
            value => !/^[ ]+$/.test(value)
          )
          .test(
            'not-empty',
            i18n.t('validation.products.rewardWithoutSpaces'),
            value => /^(?! ).*/.test(value)
          )
  })

export default validationSchema
