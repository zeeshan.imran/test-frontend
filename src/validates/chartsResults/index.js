import * as Yup from 'yup'
import { locationQuestionCountries } from '../../utils/Constants'

const basicSchema = Yup.object().shape({
  chart_id: Yup.string().required(),
  title: Yup.string().required(),
  labels: Yup.array().required(),
  totalRespondents: Yup.number()
})

const productResultSchema = Yup.object().shape({
  data: Yup.array()
    .of(
      Yup.object().shape({
        label: Yup.string().required(),
        value: Yup.number().required(),
        analytics_value: Yup.mixed()
      })
    )
    .required()
})

const avgSchema = Yup.object().shape({
  chart_avg: Yup.boolean().required(),
  avg: Yup.array()
    .of(
      Yup.object().shape({
        label: Yup.string().required(),
        value: Yup.number().required()
      })
    )
    .required()
})

const countryGeoDataSchema = Yup.object().shape({
  country_geo_data: Yup.object().shape({
    features: Yup.array()
      .of(Yup.object())
      .required()
  }),
  region: Yup.string()
    .oneOf(locationQuestionCountries)
    .required()
})

const polarResultSchema = Yup.object().shape({
  data: Yup.array()
    .of(Yup.number())
    .required()
})

export const defaultChartSchema = Yup.lazy(value =>
  Yup.object()
    .shape({
      results: Array.isArray(value.results)
        ? Yup.array().of(productResultSchema)
        : productResultSchema
    })
    .concat(basicSchema)
)

export const defaultChartSchemaGoupedByProducts = Yup.lazy(value =>
  Yup.object()
    .shape({
      results: Yup.array().of(
        Yup.object().shape({
          productName: Yup.string().required(),
          attributes: Yup.array().of(productResultSchema)
        })
      ),
      attributes: Yup.array().required()
    })
    .concat(basicSchema)
)

export const lineChartInputSchema = Yup.object()
  .shape({
    results: productResultSchema
  })
  .concat(basicSchema)

export const defaultWithAvgChartSchema = Yup.lazy(value =>
  Yup.object()
    .shape({
      results: Array.isArray(value.results)
        ? Yup.array().of(productResultSchema)
        : productResultSchema
    })
    .concat(basicSchema)
    .concat(avgSchema)
)

export const defaultWithCountryGeoData = Yup.lazy(value =>
  Yup.object()
    .shape({
      results: Array.isArray(value.results)
        ? Yup.array().of(productResultSchema)
        : productResultSchema
    })
    .concat(basicSchema)
    .concat(countryGeoDataSchema)
)

export const polarChartInputSchema = Yup.lazy(value =>
  Yup.object()
    .shape({
      range: Yup.array()
        .of(Yup.number())
        .min(2)
        .required(),
      series: Array.isArray(value.series)
        ? Yup.array().of(polarResultSchema)
        : polarResultSchema
    })
    .concat(basicSchema)
)

export const tableSchema = Yup.object()
  .shape({
    results: Yup.array()
      .of(Yup.string())
      .required()
  })
  .concat(basicSchema)
