import * as Yup from 'yup'
import i18n from '../../utils/internationalization/i18n'
import chartSection from './chart-section'
import matchRangeRules from './skipFlow/matchRangeRules'

const getPossibleLabelsAmount = (min, max) => {
  const range = max - min
  let res = [0, 2]
  for (let i = 3; i <= range + 1; i++) {
    if (range % (i - 1) === 0) {
      res.push(i)
    }
  }
  return res
}

export default Yup.lazy(values =>
  Yup.object()
    .shape({
      prompt: Yup.string()
        .test(
          'not-empty',
          i18n.t('validation.prompt'),
          value => !/^[ ]+$/.test(value)
        )
        .test('not-empty', i18n.t('validation.promptWithoutSpaces'), value =>
          /^(?! ).*/.test(value)
        )
        .required(i18n.t('validation.prompt')),
      range: Yup.object().shape({
        min: Yup.number()
          .required()
          .typeError(i18n.t('validation.vertical.range.min.greaterThan0'))
          .min(0, i18n.t('validation.vertical.range.min.moreThan'))
          .lessThan(Yup.ref('max'), ({ less }) =>
            i18n.t('validation.vertical.range.min.lessThan', {
              value: less || 'maximum value'
            })
          )
          .integer(i18n.t('validation.vertical.range.min.integer')),

        max: Yup.number()
          .required()
          .typeError(i18n.t('validation.vertical.range.max.greaterThanMin'))
          .moreThan(Yup.ref('min'), ({ more }) =>
            i18n.t('validation.vertical.range.max.moreThan', {
              value: more || 'minimum value'
            })
          )
          .integer(i18n.t('validation.vertical.range.max.integer')),

        skipTo: Yup.string().nullable(),
        labels: Yup.array()
          .of(
            Yup.string().test(
              'not-empty',
              i18n.t('validation.label'),
              value => !/^[ ]+$/.test(value)
            )
          )
          .test({
            name: 'check labels correspond to values',
            test: function (labels) {
              if (!labels) {
                return true
              }
              return (
                getPossibleLabelsAmount(
                  this.resolve(Yup.ref('min')),
                  this.resolve(Yup.ref('max'))
                ).indexOf(labels.length) !== -1
              )
            },
            message: function ({ max, min }) {
              const possibleLabels = getPossibleLabelsAmount(min, max)
              return i18n.t('validation.vertical.range.labels.canBeUsed', {
                range_1: possibleLabels.slice(0, -1).join(', '),
                range_2: possibleLabels[possibleLabels.length - 1]
              })
            },
            params: {
              min: Yup.ref('min'),
              max: Yup.ref('max')
            }
          })
      }),
      skipFlow: Yup.object({
        rules: matchRangeRules(values.range)
      })
    })
    .concat(chartSection)
)
