import validationChooseMultipleSchema from './choose-multiple.js'

describe('choose multiple validation', () => {
  const positiveChartSection = {
    chartTitle: 'Chart title',
    chartTopic: 'Chart topic',
    chartType: 'column',
    prompt: 'prompt',
    options: ['option-1', 'option-2']
  }
  const optionsType = {
    context: { chartTypes: ['column', 'pie'] }
  }
  test('should valid', async () => {
    test('should valid', async () => {
      expect(
        validationChooseMultipleSchema.validate(
          positiveChartSection,
          optionsType
        )
      ).resolves.toBe(positiveChartSection)
    })
  })

  test('options validation', async () => {
    expect(
      validationChooseMultipleSchema.validate(
        {
          ...positiveChartSection,
          options: []
        },
        optionsType
      )
    ).rejects.toMatchObject({ errors: ['Minimum of 2 options'] })

    expect(
      validationChooseMultipleSchema.validate(
        {
          ...positiveChartSection,
          options: [{ label: 'protien', value: 1 }, { label: '', value: 2 }]
        },
        optionsType
      )
    ).rejects.toMatchObject({ errors: ['Label is required'] })

    expect(
      validationChooseMultipleSchema.validate(
        {
          ...positiveChartSection,
          options: [
            { label: 'protien', value: 1 },
            { label: 'gums', value: '' }
          ]
        },
        optionsType
      )
    ).rejects.toMatchObject({ errors: ['Analytics value must be a positive number'] })
  })
})
