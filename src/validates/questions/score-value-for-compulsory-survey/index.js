import * as Yup from 'yup'

const emptyStringToNull = (value, originalValue) => {
  if (typeof originalValue === 'string' && originalValue === '') {
    return null
  }
  return value
}

export default Yup.string().when('$compulsorySurvey', compulsorySurvey => {
  if (!compulsorySurvey) {
    return Yup.string()
      .transform(emptyStringToNull)
      .nullable()
  } else {
    return Yup.string()
      .test('not-empty', 'Score value without spaces', value =>
        /^(?! ).*/.test(value)
      )
      .test('not-empty', 'Score value required', value => !/^[ ]+$/.test(value))
      .test(
        'is-positive',
        'Score value is positive',
        value => parseInt(value, 10) > -1
      )
      .transform(emptyStringToNull)
      .nullable()
      .required('Score value required')
  }
})
