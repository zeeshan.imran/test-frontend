import * as Yup from 'yup'
import i18n from '../../../utils/internationalization/i18n'

export default Yup.object().when('$chartTypes', (chartTypes, schema) => {
  if (!chartTypes || chartTypes.length === 0) {
    return schema
  }
  return schema.shape({
    chartType: Yup.string()
      .when('$products', (products, schema) => {
        if (products && products.length < 3) {
          return schema.test(
            'correlations-chart',
            i18n.t('validation.chartType.minProductsForCorrelation'),
            val => ['pearson-table', 'spearmann-table'].indexOf(val) === -1
          )
        } else {
          return schema
        }
      })
      .test(
        'is-null-undefined',
        i18n.t('validation.chartType.required'),
        value =>
          value !== null && value !== undefined && chartTypes.includes(value)
      ),
    chartTitle: Yup.string().when('chartType', {
      is: value => value === '' || value === 'no-chart',
      otherwise: Yup.string()
        .test(
          'not-empty',
          i18n.t('validation.chartTitle.required'),
          value => !/^[ ]+$/.test(value)
        )
        .test('not-empty', i18n.t('validation.chartTitle.whitespace'), value =>
          /^(?! ).*/.test(value)
        )
        .test(
          'unique',
          i18n.t(
            'components.countryGradesScoring.gradesModal.validations.withoutSpace'
          ),
          value => /^(?! ).*/.test(value)
        )
        .required(i18n.t('validation.chartTitle.required'))
    }),
    chartTopic: Yup.string().when('chartType', {
      is: value => value === '' || value === 'no-chart',
      otherwise: Yup.string()
        .required(i18n.t('validation.chartTopic.required'))
        .test('not-empty', i18n.t('validation.chartTopic.whitespace'), value =>
          /^(?! ).*/.test(value)
        )
        .test(
          'spaceless-chart-topic',
          i18n.t('validation.chartTopic.required'),
          value => !/^[ ]+$/.test(value)
        )
    })
  })
})
