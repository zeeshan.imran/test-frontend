import { uniqBy } from 'ramda'
import * as Yup from 'yup'
import { trimSpaces } from '../../utils/trimSpaces'
import { combinations } from '../../utils/combinations'
import i18n from '../../utils/internationalization/i18n'
import chartSection from './chart-section'
import { CHARTS_WITH_LIKING_QUESTION } from '../../utils/Constants'

const minPairs = 3

export default Yup.lazy(values => {
  const maxPossilbePairs = combinations(values.pairs.length)

  return Yup.object()
    .shape({
      prompt: Yup.string()
        .test(
          'not-empty',
          i18n.t('validation.prompt'),
          value => !/^[ ]+$/.test(value)
        )
        .test('not-empty', i18n.t('validation.promptWithoutSpaces'), value =>
          /^(?! ).*/.test(value)
        )
        .required(i18n.t('validation.prompt')),
      pairsOptions: Yup.object().shape({
        minPairs: Yup.number()
          .required(i18n.t('validation.paired.options.required'))
          .min(1, i18n.t('validation.paired.options.min', { min: 1 }))
          .max(
            values.pairsOptions.maxPairs,
            i18n.t('validation.paired.options.max', {
              max: values.pairsOptions.maxPairs
            })
          )
          .typeError(i18n.t('validation.paired.options.integer')),
        maxPairs: Yup.number()
          .required(i18n.t('validation.paired.options.required'))
          .min(
            values.pairsOptions.minPairs,
            i18n.t('validation.paired.options.min', {
              min: values.pairsOptions.minPairs
            })
          )
          .max(
            maxPossilbePairs,
            i18n.t('validation.paired.options.max', { max: maxPossilbePairs })
          )
          .typeError(i18n.t('validation.paired.options.integer'))
      }),
      hasFollowUpProfile: Yup.boolean(),
      pairs: Yup.array()
        .of(
          Yup.object().shape({
            pair: Yup.string()
              .test('not-empty', i18n.t('validation.label'), value =>
                /^(?! ).*/.test(value)
              )
              .required(i18n.t('validation.label'))
          })
        )
        .test(
          'unique-pairs',
          i18n.t('validation.paired.pairs.unique'),
          pairs => {
            return (
              pairs &&
              uniqBy(el => trimSpaces(el.pair).toLowerCase(), pairs).length ===
                pairs.length
            )
          }
        )
        .test(
          'length-limitation-for-all',
          i18n.t('validation.paired.pairs.length'),
          pairs =>
            pairs.reduce(
              (acc, value) =>
                acc && (value && value.pair ? value.pair.length < 75 : true),
              true
            )
        )
        .min(
          minPairs,
          i18n.t('validation.paired.pairs.min', { min: minPairs })
        ),
      profilePrompt: Yup.string()
        .nullable()
        .when('hasFollowUpProfile', {
          is: true,
          then: Yup.string()
            .test(
              'not-empty',
              i18n.t('validation.paired.profileTitle.required'),
              value => !/^[ ]+$/.test(value)
            )
            .test(
              'not-empty',
              i18n.t('validation.profileTitle.withoutSpaces'),
              value => /^(?! ).*/.test(value)
            )
            .typeError(i18n.t('validation.profileTitle.string'))
            .required(i18n.t('validation.profileTitle.required'))
        }),
      likingQuestion: Yup.string()
        .nullable()
        .when('chartType', {
          is: value => CHARTS_WITH_LIKING_QUESTION.includes(value),
          then: Yup.string().required(
            i18n.t('validation.paired.likingQuestion')
          )
        })
    })
    .concat(chartSection)
})
