import * as Yup from 'yup'
import { uniqBy } from 'ramda'
import { trimSpaces } from '../../utils/trimSpaces'
import i18n from '../../utils/internationalization/i18n'
import chartSection from './chart-section'

const multiplier = 10000
const selfGap = 2
export default Yup.lazy(values => {
  return Yup.object()
    .shape({
      rangeMin: Yup.ref('range.min'),
      rangeMax: Yup.ref('range.max'),
      prompt: Yup.string(i18n.t('validation.slider.promptString'))
        .test(
          'not-empty',
          i18n.t('validation.prompt'),
          value => !/^[ ]+$/.test(value)
        )
        .test('not-empty', i18n.t('validation.promptWithoutSpaces'), value =>
          /^(?! ).*/.test(value)
        )
        .required(i18n.t('validation.prompt')),
      secondaryPrompt: Yup.string(i18n.t('validation.slider.secondaryPrompt')),
      range: Yup.object().shape({
        min: Yup.number()
          .typeError(i18n.t('validation.slider.range.min.number'))
          .lessThan(values.range.max || Infinity, ({ less }) =>
            i18n.t('validation.slider.range.min.lessThan', {
              value: less || 'max'
            })
          )
          .test(
            'step-after-zero-digits',
            i18n.t('validation.slider.range.min.digits'),
            value => Math.floor(value * multiplier) / multiplier === value
          )
          .required(i18n.t('validation.slider.range.min.required')),
        max: Yup.number()
          .typeError(i18n.t('validation.slider.range.max.number'))
          .moreThan(values.range.min, ({ more }) =>
            i18n.t('validation.slider.range.max.greaterThan', {
              value: more || 'min'
            })
          )
          .test(
            'step-after-zero-digits',
            i18n.t('validation.slider.range.max.digits'),
            value => Math.floor(value * multiplier) / multiplier === value
          )
          .required(i18n.t('validation.slider.range.max.required')),
        step: Yup.number()
          .typeError(i18n.t('validation.slider.range.step.number'))
          .lessThan(values.range.max || Infinity, ({ less }) =>
            i18n.t('validation.slider.range.step.lessThan', {
              value: less || 'max'
            })
          )
          .moreThan(0, ({ more }) =>
            i18n.t('validation.slider.range.step.greaterThan', { value: 0 })
          )
          .test(
            'dividable-range',
            i18n.t('validation.slider.range.step.submultiple'),
            value =>
              !(
                (values.range.max * multiplier -
                  values.range.min * multiplier) %
                (values.range.step * multiplier)
              )
          )
          .required(i18n.t('validation.slider.range.step.required')),
        marks: Yup.array()
          .of(
            Yup.object().shape({
              value: Yup.number()
                .typeError(i18n.t('validation.slider.range.marks.value.number'))
                .test(
                  'marks-max-boundary',
                  i18n.t('validation.slider.range.marks.value.lessThan', {
                    value: values.range.max
                  }),
                  value => value <= values.range.max
                )
                .test(
                  'marks-max-boundary',
                  i18n.t('validation.slider.range.marks.value.moreThan', {
                    value: values.range.min
                  }),
                  value => value >= values.range.min
                )
                .required(
                  i18n.t('validation.slider.range.marks.value.required')
                ),
              label: Yup.string()
                .test(
                  'not-empty',
                  i18n.t('validation.slider.range.marks.label.withoutSpaces'),
                  value => /^(?! ).*/.test(value)
                )
                .test(
                  'not-empty',
                  i18n.t('validation.slider.range.marks.label.required'),
                  value => !/^[ ]+$/.test(value)
                )
                .required(
                  i18n.t('validation.slider.range.marks.label.required')
                ),
              isMajorUnit: Yup.string()
            })
          )
          .test('long-text-overlap', i18n.t('validation.slider.range.marks.label.overlap'), value => {
            if (!value) {
              return true
            }
            let maxLimiter = 250
            if(values.displayOn === 'middle'){
              maxLimiter = 125
            }
            
            let spacer =
              maxLimiter /
              (parseInt(values.range.max, 10) + parseInt(values.range.min, 10))
            
            const marks = [...value].sort((a, b) => a.value - b.value)

            for (let i = 0; i < marks.length; i++) {
              let prevEl = null
              let nextEl = null
              let currentEl = marks[i]

              if (i) prevEl = marks[i - 2]
              if (i !== marks.length - 2) nextEl = marks[i + 2]
              
              if (
                prevEl &&
                (parseInt(prevEl.value +
                  prevEl.label.length, 10) -
                parseInt(currentEl.value +
                  selfGap, 10)) >=
                  spacer
              )
                return false
                
              if (
                nextEl &&
                (parseInt(currentEl.value +
                  currentEl.label.length, 10) -
                  parseInt(nextEl.value +
                  selfGap, 10)) >=
                  spacer
              )
                return false
            }
            return true
          })
          .test(
            'uniq-mark-position',
            i18n.t('validation.slider.range.marks.unique'),
            value =>
              !value ||
              !value.length ||
              (uniqBy(el => (el.value ? el.value : Math.random()), value)
                .length === value.length &&
                uniqBy(
                  el =>
                    el.label
                      ? trimSpaces(el.label).toLowerCase()
                      : Math.random(),
                  value
                ).length === value.length)
          )
          .nullable()
      }),
      sliderOptions: Yup.object().shape({
        sliders: Yup.array()
          .of(
            Yup.object().shape({
              label: Yup.string()
                .test(
                  'not-empty',
                  i18n.t('validation.slider.range.sliders.withoutSpaces'),
                  value => /^(?! ).*/.test(value)
                )
                .test(
                  'not-empty',
                  i18n.t('validation.slider.range.sliders.required'),
                  value => !/^[ ]+$/.test(value)
                )
                .required(i18n.t('validation.slider.range.sliders.required'))
                .typeError(i18n.t('validation.slider.range.sliders.required'))
            })
          )
          .test(
            'uniq-sliders',
            i18n.t('validation.slider.range.sliders.unique'),
            value =>
              !value ||
              !value.length ||
              uniqBy(
                el =>
                  el.label ? trimSpaces(el.label).toLowerCase() : Math.random(),
                value
              ).length === value.length
          )
          .min(1, i18n.t('validation.slider.range.sliders.atLeast')),
        hasFollowUpProfile: Yup.boolean().nullable(),
        profilePrompt: Yup.string()
          .nullable()
          .when('hasFollowUpProfile', {
            is: true,
            then: Yup.string()
              .test(
                'not-empty',
                i18n.t('validation.profileTitle.withoutSpaces'),
                value => /^(?! ).*/.test(value)
              )
              .test(
                'not-empty',
                i18n.t('validation.profileTitle.required'),
                value => !/^[ ]+$/.test(value)
              )
              .typeError(i18n.t('validation.profileTitle.string'))
              .required(i18n.t('validation.profileTitle.required'))
          })
      })
    })
    .concat(chartSection)
})
