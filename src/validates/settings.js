import * as Yup from 'yup'
import { uniqBy } from 'ramda'
import { trimSpaces } from '../utils/trimSpaces'
import i18n from '../utils/internationalization/i18n'

const settingsSchema = Yup.object().shape({
  active: Yup.boolean(),
  debounce: Yup.mixed().when('active', {
    is: true,
    then: Yup.number()
      .min(0, i18n.t('surveySettings.validations.debounce.min'))
      .max(600, i18n.t('surveySettings.validations.debounce.max'))
      .required(i18n.t('surveySettings.validations.debounce.required'))
      .test(
        'wrong-step',
        i18n.t('surveySettings.validations.debounce.step'),
        value => value % 0.5 <= Number.EPSILON
      )
  }),
  promotionActive: Yup.boolean(),
  promotionCode: Yup.mixed().when('promotionActive', {
    is: true,
    then: Yup.string().required(
      i18n.t('surveySettings.validations.promotionCode.required')
    )
  }),
  promotionOptions: Yup.object()
    .nullable()
    .shape({
      promoProducts: Yup.array()
        .of(
          Yup.object().shape({
            label: Yup.string()
              .test(
                'not-empty',
                i18n.t(
                  'surveySettings.validations.promotionProducts.name.withoutSpaces'
                ),
                value => /^(?! ).*/.test(value)
              )
              .test(
                'not-empty',
                i18n.t(
                  'surveySettings.validations.promotionProducts.name.required'
                ),
                value => !/^[ ]+$/.test(value)
              )
              .required(
                i18n.t(
                  'surveySettings.validations.promotionProducts.name.required'
                )
              )
              .typeError(
                i18n.t(
                  'surveySettings.validations.promotionProducts.name.required'
                )
              ),
            url: Yup.string()
              .test(
                'is-url',
                i18n.t(
                  'surveySettings.validations.promotionProducts.url.isUrl'
                ),
                value =>
                  value &&
                  /(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]/.test(
                    value
                  )
              )
              .nullable()
          })
        )
        .test(
          'uniq-promoProducts',
          i18n.t('surveySettings.validations.promotionProducts.unique'),
          value =>
            !value ||
            !value.length ||
            uniqBy(
              el =>
                el.label ? trimSpaces(el.label).toLowerCase() : Math.random(),
              value
            ).length === value.length
        )
        .min(1, i18n.t('surveySettings.validations.promotionProducts.atLeast'))
    })
})

export default settingsSchema
