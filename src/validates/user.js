import * as Yup from 'yup'
import i18n from '../utils/internationalization/i18n'

const validationSchema = Yup.object().shape({
  emailAddress: Yup.string()
    .test(
      'max-length',
      i18n.t('validation.userForm.email.maxLength'),
      value => value && value.length <= 50
    )
    .email(i18n.t('validation.email.email'))
    .required(i18n.t('validation.userForm.email.required')),
  fullName: Yup.string()
    .test(
      'max-length',
      i18n.t('validation.userForm.fullName.maxLength'),
      value => value && value.length <= 50
    )
    .test(
      'no-spaces',
      i18n.t('validation.userForm.fullName.required'),
      value => !/^[ ]+$/.test(value)
    )
    .required(i18n.t('validation.userForm.fullName.required'))
    .test(
      'not-empty',
      i18n.t('validation.userForm.fullName.nameWithoutSpaces'),
      value => /^(?! ).*/.test(value)
    ),
  gender: Yup.string().nullable(),
  country: Yup.string().nullable(),
  state: Yup.string().nullable(),
  language: Yup.string().nullable(),
  paypalEmailAddress: Yup.string()
    .email(i18n.t('validation.email.email'))
    .nullable(),
  type: Yup.string(),
  organization: Yup.string()
    .typeError(i18n.t('validation.userForm.organization.typeError'))
    .nullable()
})

export default validationSchema
