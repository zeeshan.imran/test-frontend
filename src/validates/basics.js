import * as Yup from 'yup'
import i18n from '../utils/internationalization/i18n'
import { MAX_LIMIT_REFERRAL } from '../utils/Constants'

export default isValidate =>
  Yup.object().shape({
    uniqueName: isValidate
      ? Yup.string()
          .test(
            'not-empty',
            i18n.t('validation.basics.uniquName.withoutSpaces'),
            value => !/^[ ]+$/.test(value)
          )
          .required(i18n.t('validation.basics.uniquName.required'))
          .test(
            'not-empty',
            i18n.t('validation.basics.uniquName.withoutSpaces'),
            value => /^(?! ).*/.test(value)
          )
          .matches(
            /^[0-9a-zA-Z-]+$/,
            i18n.t('validation.basics.uniquName.restrict')
          )
          .nullable()
      : Yup.string().nullable(),
    name: isValidate
      ? Yup.string()
          .test(
            'not-empty',
            i18n.t('validation.basics.name.required'),
            value => !/^[ ]+$/.test(value)
          )
          .test(
            'not-empty',
            i18n.t('validation.basics.name.withoutSpaces'),
            value => /^(?! ).*/.test(value)
          )
          .required(i18n.t('validation.basics.name.required'))
      : Yup.string().nullable(),
    title: Yup.string()
      .test(
        'not-empty',
        i18n.t('validation.basics.title.required'),
        value => !/^[ ]+$/.test(value)
      )
      .test(
        'not-empty',
        i18n.t('validation.basics.title.withoutSpaces'),
        value => /^(?! ).*/.test(value)
      )
      .required(i18n.t('validation.basics.title.required')),
    exclusiveTasters: Yup.array().when('authorizationType', {
      is: 'selected',
      then: Yup.array()
        .of(
          Yup.string().email(({ value }) =>
            i18n.t('validation.basics.excluseiveTasters.email', {
              email: value
            })
          )
        )
        .min(1, i18n.t('validation.basics.excluseiveTasters.required'))
        .required(i18n.t('validation.basics.excluseiveTasters.required'))
    }),

    maxProductStatCount: Yup.number()
      .required(i18n.t('validation.basics.maxProductStatCount.required'))
      .max(12, i18n.t('validation.basics.maxProductStatCount.max'))
      .min(6, i18n.t('validation.basics.maxProductStatCount.min')),

    instructionSteps: Yup.array().of(
      Yup.string()
        .test(
          'not-empty',
          i18n.t('validation.basics.instructionWithoutSpaces'),
          value => /^(?! ).*/.test(value)
        )
        .test(
          'not-empty',
          i18n.t('validation.basics.instruction'),
          value => !/^[ ]+$/.test(value)
        )
    ),

    allowedDaysToFillTheTasting: Yup.number().when('authorizationType', {
      is: 'email',
      then: Yup.number()
        .typeError(
          i18n.t('validation.basics.allowedDaysToFillTheTasting.required')
        )
        .integer(
          i18n.t('validation.basics.allowedDaysToFillTheTasting.integer')
        )
        .min(1, i18n.t('validation.basics.allowedDaysToFillTheTasting.min'))
        .max(712, i18n.t('validation.basics.allowedDaysToFillTheTasting.max'))
        .required(
          i18n.t('validation.basics.allowedDaysToFillTheTasting.required')
        )
    }),

    referralAmount: Yup.number().when('isPaypalSelected', {
      is: true,
      then: Yup.number()
        .required(i18n.t('validation.basics.referralAmount.required'))
        .integer(
          i18n.t('validation.basics.allowedDaysToFillTheTasting.integer')
        )
        .min(1, i18n.t('validation.basics.referralAmount.min'))
        .max(
          MAX_LIMIT_REFERRAL,
          i18n.t('validation.basics.referralAmount.max', {
            max: MAX_LIMIT_REFERRAL
          })
        )
    }),

    maxProductStatCountError: Yup.number().min(6, 'yolo'),
    customButtonsContinue: Yup.string()
      .test(
        'not-empty',
        i18n.t('validation.basics.customButtons.continueRequired'),
        value => !/^[ ]+$/.test(value)
      )
      .test(
        'not-empty',
        i18n.t('validation.basics.customButtons.continueEmptySpaces'),
        value => /^(?! ).*/.test(value)
      )
      .required(i18n.t('validation.basics.customButtons.continueRequired')),
    customButtonsStart: Yup.string()
      .test(
        'not-empty',
        i18n.t('validation.basics.customButtons.startRequired'),
        value => !/^[ ]+$/.test(value)
      )
      .test(
        'not-empty',
        i18n.t('validation.basics.customButtons.startEmptySpaces'),
        value => /^(?! ).*/.test(value)
      )
      .required(i18n.t('validation.basics.customButtons.startRequired')),
    customButtonsNext: Yup.string()
      .test(
        'not-empty',
        i18n.t('validation.basics.customButtons.nextRequired'),
        value => !/^[ ]+$/.test(value)
      )
      .test(
        'not-empty',
        i18n.t('validation.basics.customButtons.nextEmptySpaces'),
        value => /^(?! ).*/.test(value)
      )
      .required(i18n.t('validation.basics.customButtons.nextRequired')),
    customButtonsSkip: Yup.string()
      .test(
        'not-empty',
        i18n.t('validation.basics.customButtons.skipRequired'),
        value => !/^[ ]+$/.test(value)
      )
      .test(
        'not-empty',
        i18n.t('validation.basics.customButtons.skipEmptySpaces'),
        value => /^(?! ).*/.test(value)
      )
      .required(i18n.t('validation.basics.customButtons.skipRequired')),
    retakeAfter: Yup.number().when('allowRetakes', {
      is: true,
      then: Yup.number()
        .required('Retake After X Days required')
        .integer(
        'It must be integer'
        )
        .min(1, 'Minimum days cannot be less than 1')
       
    }),
    maxRetake: Yup.number().when('allowRetakes', {
      is: true,
      then: Yup.number()
        .required('Maximum Number of Retakes required')
        .integer(
        'It must be integer'
        )
        .min(0, 'Max Retake cannot be less than 0')
       
    }),
  })
