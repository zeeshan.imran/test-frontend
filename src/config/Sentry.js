import * as Sentry from '@sentry/browser'

const dsn = process.env.REACT_APP_SENTRY_URI

const init = {
  beforeSend (event, hint) {
    if (hint.originalException === 'Timeout') return null
    return event
  }
}

if (dsn) {
  init.dsn = dsn
}

Sentry.init(init)
