import bootstrapApollo from './ApolloClient'
import gql from 'graphql-tag'
import {
  setAuthenticationToken,
  setImpersonateUserId
} from '../utils/userAuthentication'

describe('test apollo bootstrap', () => {
  beforeAll(() => {
    const localStorageMock = (function () {
      let store = {}
      return {
        getItem: function (key) {
          return store[key] || null
        },
        setItem: function (key, value) {
          store[key] = value.toString()
        },
        removeItem: function (key) {
          delete store[key]
        },
        clear: function () {
          store = {}
        }
      }
    })()
    const sessionStorageMock = (function () {
      let store = {}
      return {
        getItem: function (key) {
          return store[key] || null
        },
        setItem: function (key, value) {
          store[key] = value.toString()
        },
        removeItem: function (key) {
          delete store[key]
        },
        clear: function () {
          store = {}
        }
      }
    })()
    Object.defineProperty(window, 'localStorage', {
      value: localStorageMock
    })
    Object.defineProperty(window, 'sessionStorage', {
      value: sessionStorageMock
    })
    const fetch = jest.fn(() =>
      Promise.resolve({
        text: () => Promise.resolve('{ "data": { "fakeQuery": null } }')
      })
    )
    Object.defineProperty(window, 'fetch', {
      value: fetch
    })

    setAuthenticationToken('faked-access-token')
  })

  it('bootstrap with localStorage', async () => {
    await bootstrapApollo(true)
  })

  it('bootstrap with sessionStorage', async () => {
    await bootstrapApollo(false)
  })

  it('should purge when restoring cache failure', async () => {
    window.localStorage.setItem('DEV', 'apollo-schema-version')
    window.localStorage.setItem('flavorwiki-apollo-cache', 'wrongJSON')
    await bootstrapApollo(true)
  })

  describe('authorization', () => {
    it('should send header while querying', async () => {
      const client = await bootstrapApollo(true)
      await client.query({
        query: gql`
          query {
            fakeQuery
          }
        `
      })
      expect(fetch).toHaveBeenCalledWith('http://test.local:8000/graphql', {
        body:
          '{"operationName":null,"variables":{},"query":"{\\n  fakeQuery\\n}\\n"}',
        credentials: undefined,
        headers: {
          Authorization: 'Bearer faked-access-token',
          accept: '*/*',
          'content-type': 'application/json'
        },
        method: 'POST'
      })
    })

    it('should send impersonate while querying', async () => {
      setImpersonateUserId('user-1')
      const client = await bootstrapApollo(true)
      await client.query({
        query: gql`
          query {
            fakeQuery
          }
        `
      })
      expect(fetch).toHaveBeenCalledWith('http://test.local:8000/graphql', {
        body:
          '{"operationName":null,"variables":{},"query":"{\\n  fakeQuery\\n}\\n"}',
        credentials: undefined,
        headers: {
          Authorization: 'Bearer faked-access-token',
          Impersonate: 'user-1',
          accept: '*/*',
          'content-type': 'application/json'
        },
        method: 'POST'
      })
    })
  })
})
