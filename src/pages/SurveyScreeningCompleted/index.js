import React from 'react'
import AuthenticatedLayout from '../../containers/AuthenticatedLayout'
import SurveyScreeningCompletedContainer from '../../containers/SurveyScreeningCompleted'
import { Row, Col } from 'antd'

const SurveyScreeningCompleted = ({
  match: {
    params: { surveyId }
  },
  history
}) => (
  <AuthenticatedLayout mergeNavbarToContent>
    <Row type='flex' justify='center'>
      <Col xs={{ span: 20 }} lg={{ span: 12 }}>
        <SurveyScreeningCompletedContainer surveyId={surveyId} />
      </Col>
    </Row>
  </AuthenticatedLayout>
)

export default SurveyScreeningCompleted
