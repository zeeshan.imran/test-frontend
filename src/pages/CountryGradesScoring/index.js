import React from 'react'
import OperatorPage from '../../components/OperatorPage'
import OperatorPageContent from '../../components/OperatorPageContent'
import CountryGradesScoringContainer from '../../containers/CountryGradesScoringList'

const CountryGradesScoring = ({ ...props }) => {
  return (
    <OperatorPage>
      <OperatorPageContent>
        <CountryGradesScoringContainer {...props} />
      </OperatorPageContent>
    </OperatorPage>
  )
}

export default CountryGradesScoring
