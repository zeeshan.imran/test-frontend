import React from 'react'
import {StyledTitle, StyledSubTitle} from '../styles'
import PersonalInformationForm from '../../../containers/CreateTasterAccount/PersonalInformationForm'

const PersonalInformationStep = ({ step, setForm, currentStep, desktop, t }) => {

  return (
    <React.Fragment>
      <StyledTitle desktop={desktop}>{t(step.title)}</StyledTitle>
      <StyledSubTitle desktop={desktop}>{t(step.description)}</StyledSubTitle>
      <PersonalInformationForm
        setForm={setForm}
        stepNumber={currentStep}
        initialValid={step.isValid}
        initialValues={step.formData}
      />
    </React.Fragment>
  )
}

export default PersonalInformationStep
