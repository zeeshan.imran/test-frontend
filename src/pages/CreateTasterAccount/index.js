import React, { useEffect, useState } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import { withTranslation } from 'react-i18next'
import { Col, Row } from 'antd'
import {
  getTasterRegistrationSteps,
  getStartingStep
} from '../../utils/getTasterRegistrationSteps'
import { getNavEntries } from '../../utils/getNavigationEntries'
import { Desktop } from '../../components/Responsive'
import { useMutation } from 'react-apollo-hooks'
import WelcomeStep from './WelcomeStep'
import PersonalInformationStep from './PersonalInformationStep'
import AdditionalInformationStep from './AdditionalInformationStep'
import BottomNavbar from '../../components/CreateTasterAccount/BottomNavbar'
import { getPathFromLocation } from '../../utils/getPathFromLocation/'
import getQueryParams from '../../utils/getQueryParams'
import { getIndexPointFromArray } from '../../utils/getIndexPointFromArray/'
import useUpdateLocale from '../../hooks/useUpdateSurveyLanguage'
import { useSlaask } from '../../contexts/SlaaskContext'
import Navbar from '../../components/Navbar'
import Modal from '../../components/Modal'

import {
  setAuthenticatedUser,
  getAuthenticatedUser,
  logout
} from '../../utils/userAuthentication'
import {
  SideBySideLayout,
  Side,
  Aligner,
  AlignerBottomNav,
  ContainerWithIncreaseWidth,
  ContainerWithIncreaseWidthNoPadding,
  Logo,
  StepsText,
  ErrorText
} from './styles'
import { getImages } from '../../utils/getImages'
import { imagesCollection } from '../../assets/png'
const { desktopImage } = getImages(imagesCollection)
const getEntryName = ({ title }) => title

// Main Function
const CreateTasterAccount = ({ history, match, location, t }) => {
  // States
  const updateLocale = useUpdateLocale()
  const [loading, setLoading] = useState(false)
  const { reloadSlaask } = useSlaask()
  const [userType] = useState('taster')
  const [errorData, setErrorData] = useState('')
  const [hasError, setHasError] = useState(false)
  const [completeForm, setCompleteForm] = useState({})
  const [loggedInUser, setLoggedInUser] = useState(getAuthenticatedUser())
  const STEPS = getTasterRegistrationSteps(loggedInUser, t)
  const [tasterSteps, setTasterSteps] = useState(STEPS)
  const [showHamburger, setShowHamburger] = useState(false)
  // Values required
  const startingPath = getStartingStep(STEPS)
  if (!startingPath) history.push('/taster')
  const currentPath = getPathFromLocation(location, 3)
  const currentStep = getIndexPointFromArray(tasterSteps, 'path', currentPath)
  const totalSteps = tasterSteps.length
  // const isLastStep = currentStep === totalSteps - 1
  // Getting and Setting up Mutation Calls - based on steps
  const getCurrentMutation =
    currentStep >= 0 ? tasterSteps[currentStep].mutation : null
  const mutationCall = useMutation(getCurrentMutation)

  // Redirecting to start page, if start step skipped.
  if (startingPath !== currentPath && !tasterSteps[0].isValid) {
    history.push(match.path + '/' + tasterSteps[0].path)
  }

  if (currentPath === STEPS[0].path && STEPS[0].isValid && STEPS.length > 1) {
    history.push(match.path + '/' + STEPS[1].path)
  }

  useEffect(() => {
    if (loggedInUser && loggedInUser.language) {
      updateLocale(loggedInUser.language || 'en')
    }
  }, [loggedInUser])

  // Navigation Handler
  const handleClickEntry = entry => {
    entry.action ? entry.action() : history.push(entry.route)
  }

  // SET Form updates all the values and maintains form data for each step
  function setForm (valid, values, stepNumber) {
    if (tasterSteps[stepNumber].isValid !== valid) {
      setTasterSteps(
        tasterSteps.map((step, index) => {
          if (step.isValid !== valid && index === stepNumber) {
            step.isValid = valid
            return step
          } else return step
        })
      )
    }
    // Setting values in the main STEPS to used to later
    if (tasterSteps[stepNumber].isValid) STEPS[stepNumber]['formData'] = values
  }

  // When clicked on next step and form submit
  async function submitForm (currentStep) {
    const nextStep = currentStep + 1

    setLoading(true)

    try {
      // In case user comes back to the step
      await setTasterSteps(
        tasterSteps.map((step, index) => {
          if (step.isValid && index === currentStep) {
            step.formData = STEPS[currentStep].formData
            return step
          } else return step
        })
      )

      // submitting current form - need to make it better
      switch (currentStep) {
        case 0:
          await submitSignUpForm(currentStep)
          break
        default:
          await submitPersonalInformationForm(currentStep)
          break
      }
      if (currentStep) {
        // Complete form without steps
        await setCompleteForm(...completeForm, STEPS[currentStep].formData)

        // If last step, currently redirects the logged in user to main screen
        if (nextStep >= totalSteps) goToTasterProfile()
        else history.push(match.path + '/' + tasterSteps[nextStep].path)
      } else {
        goToLogin()
      }
    } catch (error) {
      let errorMessage = error.message.replace('GraphQL error: ', '')

      if (errorMessage.includes('email.invalid')) {
        errorMessage = t('email.invalid')
      }

      if (errorMessage.includes('Operator Account')) {
        errorMessage = t('errors.operatorEmailExist')
      }

      setErrorData(<ErrorText>{errorMessage}</ErrorText>)

      setHasError(true)
    } finally {
      setLoading(false)
    }
  }

  // When clicked on prev step
  function goBack (currentStep) {
    const prevStep = currentStep - 1

    // if no prev step available, jump to dashboard screen
    if (prevStep < 0) goToDashboard()
    else {
      // In case user comes back to the step
      setTasterSteps(
        tasterSteps.map((step, index) => {
          if (step.isValid && index === currentStep) {
            step.formData = STEPS[currentStep].formData
            return step
          } else return step
        })
      )
      setCompleteForm(...completeForm, STEPS[currentStep].formData)
      history.push(match.path + '/' + tasterSteps[prevStep].path)
    }
  }

  // Going back to the dashboard
  function goToDashboard () {
    history.push('/')
  }

  function goToLogin () {
    history.push({
      pathname: '/onboarding/login',
      state: { fromDashboard: true }
    })
  }

  // Going to the profile page
  function goToTasterProfile () {
    history.push('/taster')
  }

  // Submitting first form
  async function submitSignUpForm (currentStep) {
    let formData = tasterSteps[currentStep].formData
    const { referral, source } = getQueryParams(location)
    // const {
    //   data: {
    //     createTasterAccount: { token }
    //   }
    // } =

    await mutationCall({
      variables: {
        input: {
          emailAddress: formData.emailAddress,
          password: formData.password,
          language: formData.language,
          country: formData.country,
          state: formData.state,
          type: userType,
          isDummyTaster:
            location.pathname === '/create-demo-account/taster/welcome',
          referral,
          source
        }
      }
    })

    // Setting up the auth token - required for further transactions
    // setAuthenticationToken(token)
    // if (!user.fullName) {
    //   user.fullName = user.emailAddress
    // }
    // if (token) {
    //   displayMessage(t('containers.loginForm.verifyEmail'))
    // }
    // steps  to authenticate and goto profile
    // setting the logged in user
    // setAuthenticatedUser(user)
    // setLoggedInUser(user)

    // reloading slassk help tool
    await reloadSlaask()
  }

  // Submitting rest of the forms
  async function submitPersonalInformationForm (currentStep) {
    let formData = tasterSteps[currentStep].formData
    formData['id'] = loggedInUser.id
    const {
      data: { updateTasterAccount }
    } = await mutationCall({
      variables: {
        input: formData
      }
    })

    // updating current object with new values
    let keys = Object.keys(updateTasterAccount)
    keys.map(index => {
      loggedInUser[index] = updateTasterAccount[index]
      // returning to avoid return error
      return index
    })

    // updating the logged in user
    setAuthenticatedUser(loggedInUser)
    setLoggedInUser(loggedInUser)
  }

  return (
    <Desktop>
      {desktop => {
        return (
          <SideBySideLayout desktop={desktop} loggedInUser={loggedInUser}>
            <Side>
              <Aligner desktop={desktop} loggedInUser={loggedInUser}>
                {loggedInUser ? (
                  <Navbar
                    user={loggedInUser}
                    entriesData={getNavEntries(
                      t,
                      '/create-account/taster/personal-information',
                      logout
                    )}
                    getEntryName={getEntryName}
                    onClickEntry={handleClickEntry}
                    setShowHamburger={() => {
                      setShowHamburger(!showHamburger)
                    }}
                    showHamburger={showHamburger}
                  />
                ) : (
                  <Logo
                    onClick={() => history.push('/')}
                    src={desktopImage}
                    desktop={desktop}
                  />
                )}
                <ContainerWithIncreaseWidth
                  desktop={desktop}
                  loggedInUser={loggedInUser}
                >
                  <Modal
                    visible={hasError}
                    children={errorData}
                    toggleModal={() => {
                      setHasError(false)
                      setErrorData('')
                    }}
                  />
                  <Row>
                    <Col
                      xs={{ span: 0 }}
                      lg={{ offset: 0, span: 16 }}
                      xl={{ offset: 0, span: 8 }}
                    >
                      <StepsText desktop={desktop}>
                        {t(`components.createTasterAccount.main.stepsText`, {
                          currentStep: currentStep + 1,
                          totalSteps: totalSteps
                        })}
                      </StepsText>
                    </Col>
                  </Row>
                  <Switch>
                    <Redirect
                      exact
                      path={`${match.path}`}
                      to={`${match.path}/${startingPath}`}
                    />
                    <Route
                      path={`${match.path}/welcome`}
                      render={() => (
                        <WelcomeStep
                          setForm={setForm}
                          currentStep={currentStep}
                          step={tasterSteps[currentStep]}
                          completeForm={completeForm}
                          desktop={desktop}
                        />
                      )}
                    />
                    <Route
                      path={`${match.path}/personal-information`}
                      render={() => (
                        <PersonalInformationStep
                          setForm={setForm}
                          currentStep={currentStep}
                          step={tasterSteps[currentStep]}
                          completeForm={completeForm}
                          desktop={desktop}
                          t={t}
                        />
                      )}
                    />
                    <Route
                      path={`${match.path}/additional-information`}
                      render={() => (
                        <AdditionalInformationStep
                          setForm={setForm}
                          currentStep={currentStep}
                          step={tasterSteps[currentStep]}
                          completeForm={completeForm}
                          desktop={desktop}
                          t={t}
                        />
                      )}
                    />
                  </Switch>
                </ContainerWithIncreaseWidth>
              </Aligner>
              <AlignerBottomNav>
                <ContainerWithIncreaseWidthNoPadding>
                  <BottomNavbar
                    loading={loading}
                    currentStep={desktop ? null : currentStep + 1}
                    totalSteps={desktop ? null : totalSteps}
                    okText={
                      tasterSteps[currentStep] !== undefined
                        ? t(tasterSteps[currentStep].okText)
                        : t(
                            'components.createTasterAccount.main.nextStepDefault'
                          )
                    }
                    onOk={() => submitForm(currentStep)}
                    okDisabled={
                      tasterSteps[currentStep] !== undefined
                        ? !tasterSteps[currentStep].isValid
                        : null
                    }
                    cancelText={t(
                      'components.createTasterAccount.main.cancelText'
                    )}
                    onCancel={() => goBack(currentStep)}
                    cancelDisabled={STEPS.length > 1 ? currentStep === 1 : true}
                    extraAction={t(
                      'components.createTasterAccount.main.extraActionText'
                    )}
                    onExtraAction={() => goToTasterProfile()}
                    hasExtraAction={false}
                    // hasExtraAction={STEPS.length > 1 ? isLastStep : false}
                  />
                </ContainerWithIncreaseWidthNoPadding>
              </AlignerBottomNav>
            </Side>
          </SideBySideLayout>
        )
      }}
    </Desktop>
  )
}

export default withTranslation()(CreateTasterAccount)
