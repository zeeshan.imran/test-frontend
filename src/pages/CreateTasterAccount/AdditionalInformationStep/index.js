import React from 'react'
import {StyledTitle, StyledSubTitle} from '../styles'
import AdditionalInformationForm from '../../../containers/CreateTasterAccount/AdditionalInformationForm'

const AdditionalInformationStep = ({ step, setForm, currentStep, completeForm, desktop, t }) => {
  return (
    <React.Fragment>
      <StyledTitle desktop={desktop}>{t(step.title)}</StyledTitle>
      <StyledSubTitle desktop={desktop}>{t(step.description)}</StyledSubTitle>
      <AdditionalInformationForm
        setForm={setForm}
        stepNumber={currentStep}
        initialValid={step.isValid}
        initialValues={step.formData}
        completeForm={completeForm}
      />
    </React.Fragment>
  )
}

export default AdditionalInformationStep
