import React from 'react'
import CategoryRedirect from '../../containers/CategoryRedirect'

const Category = ({
  match: {
    params: { categoryName }
  }
}) => <CategoryRedirect categoryName={categoryName} />

export default Category
