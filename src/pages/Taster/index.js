import React, { useState, useEffect } from 'react'
import { Switch } from 'react-router-dom'
import path from 'path'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import { Button, notification, Icon } from 'antd'
import TasterExclusiveSurveysList from '../../containers/TasterExclusiveSurveysList'
import AuthenticatedLayout from '../../containers/AuthenticatedLayout'
import TasterRoute from '../../components/TasterRoute'
import {
  Container,
  BaseContainer,
  Title,
  SubTitle,
  TitleContainer,
  TasterWrapper
} from './styles'
import { withTranslation } from 'react-i18next'
import {
  getImpersonateUserId,
  getAuthenticatedUser
} from '../../utils/userAuthentication'
import useUpdateLocale from '../../hooks/useUpdateSurveyLanguage'
import useResponsive from '../../utils/useResponsive'
import getCurrency from '../../utils/getCurrency'
import getQueryParams from '../../utils/getQueryParams'

const Taster = ({ match, t, history, location }) => {
  const user = getAuthenticatedUser()
  const isImpersonating = !!getImpersonateUserId()
  const [showNotification, setShowNotification] = useState(true)

  const { showhelp } = getQueryParams(location)

  const { data } = useQuery(QUERY_IMPERSONATE_USER, {
    fetchPolicy: 'no-cache',
    skip: !isImpersonating
  })

  const { data: { user: { language = 'en' } = {} } } = useQuery(QUERY_USER, {
    fetchPolicy: 'no-cache',
    variables: {
      userId: user && user.id
    }
  })
  
  if(user && (!user.country)) {
    history.push('/create-account/taster/personal-information')
  }

  const { mobile } = useResponsive()
  const updateLocale = useUpdateLocale()

  const openNotification = () => {
    const { alert } = getQueryParams(location)
    if(showNotification && !alert && user && user.country !== 'BR'){
      setShowNotification(false)
      notification.info({
        message: t('components.surveysList.notice'),
        description: t('components.surveysList.noticeMessage'),
        icon: <Icon type='notification' style={{ color: '#108ee9' }} />,
        duration: 10
      });
    }
  };

  useEffect(() => {
    openNotification()
  }, [])

  useEffect(() => {
    updateLocale(language)

    return () => {
      updateLocale('en')
    }
  }, [language])

  return (
    <AuthenticatedLayout mergeNavbarToContent showhelp={showhelp} >
      <BaseContainer>
        <Container mobile={mobile}>
          <Switch>
            <TasterRoute
              path={path.normalize(`${match.path}/`)}
              component={() => (
                <React.Fragment>
                  {data && data.adminUser && data.impersonateUser ? (
                    <TasterWrapper>
                      {t(`components.impersonate.impersonatedAs`, {
                        user:
                          data.adminUser.fullName ||
                          data.adminUser.emailAddress,
                        targetUser:
                          data.impersonateUser.fullName ||
                          data.impersonateUser.emailAddress
                      })}
                      <Button
                        type='link'
                        onClick={() => history.push('/impersonate/exit')}
                      >
                        {t('components.impersonate.toGoToAdmin')}
                      </Button>
                    </TasterWrapper>
                  ) : null}
                  <TitleContainer>
                    <Title>{t('containers.page.taster.title')}</Title>
                    <SubTitle>
                      <span
                        dangerouslySetInnerHTML={{
                          __html: t('containers.page.taster.promt')
                        }}
                      />
                    </SubTitle>
                    <SubTitle>
                      <span>
                        {t('containers.page.taster.earn', { amount: getCurrency(user && user.country, 50) })}
                      </span>
                    </SubTitle>
                  </TitleContainer>
                  <TasterExclusiveSurveysList getCurrency={getCurrency} />
                </React.Fragment>
              )}
            />
          </Switch>
        </Container>
      </BaseContainer>
    </AuthenticatedLayout>
  )
}

export const QUERY_USER = gql`
  query user($userId: ID) {
    user(id: $userId) {
      id
      initialized
      emailAddress
      type
      isSuperAdmin
      fullName
      country
      language
      dateofbirth
      isProfileCompleted
      isCompulsorySurveyTaken
      isDummyTaster
      organization {
        id
      }
    }
  }
`

export const QUERY_IMPERSONATE_USER = gql`
  query impersonateUser {
    impersonateUser: me {
      id
      initialized
      emailAddress
      type
      isSuperAdmin
      fullName
      country
      language
      dateofbirth
      isProfileCompleted
      isCompulsorySurveyTaken
      isDummyTaster
      organization {
        id
      }
    }
    adminUser: me(impersonate: false) {
      id
      initialized
      emailAddress
      type
      isSuperAdmin
      fullName
      country
      dateofbirth
      language
      isProfileCompleted
      isCompulsorySurveyTaken
      isDummyTaster
      organization {
        id
      }
    }
  }
`

export default withTranslation()(Taster)
