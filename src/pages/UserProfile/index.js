import React, { useState } from 'react'
import { Icon } from 'antd'
import { withRouter, Switch, Route, Redirect } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import AuthenticatedLayout from '../../containers/AuthenticatedLayout'
import TabBar from '../../components/TabBar'
import { getPathFromLocation } from '../../utils/getPathFromLocation/'
import { getUserProfileTabs } from '../../utils/getUserProfileTabs/'
import UserProfilePersonal from '../../containers/UserProfile/Personal'
import UserProfilePassword from '../../containers/UserProfile/Password'


import {
  Container,
  BaseContainer,
  Title,
  TitleContainer,
  TitleIconContainer,
  TitleText,
  TabContainer,
  TabTitleContainer
} from './styles'

const UserProfile = ({ history, match, location }) => {
  const { t } = useTranslation()
  const tabs = getUserProfileTabs(t)
  tabs.map(tab => {
    tab.title = (
      <TitleIconContainer>
        <Icon type={tab.iconType} /> <TitleText>{tab.title}</TitleText>
      </TitleIconContainer>
    )
    return tab
  })

  const currentPath = getPathFromLocation(location, 3)
  const [activeTab, setActiveTab] = useState(currentPath)

  const changeTab = active => {
    setActiveTab(active)
    history.push(active)
  }

  return (
    <AuthenticatedLayout mergeNavbarToContent>
      <BaseContainer>
        <Container>
          <React.Fragment>
            <TitleContainer>
              <Title>{t('containers.userProfile.mainTitle')}</Title>
            </TitleContainer>
            <TabTitleContainer>
              <TabBar
                tabs={tabs}
                activeTab={activeTab}
                onTabSelection={changeTab}
              />
            </TabTitleContainer>
            <TabContainer>
              <Switch>
                <Redirect
                  exact
                  path={`${match.path}`}
                  to={`${match.path}/personal`}
                />
                <Route
                  path={`${match.path}/personal`}
                  render={() => <UserProfilePersonal />}
                />
                <Route
                  path={`${match.path}/password`}
                  render={() => <UserProfilePassword />}
                />
                
              </Switch>
            </TabContainer>
          </React.Fragment>
        </Container>
      </BaseContainer>
    </AuthenticatedLayout>
  )
}

export default withRouter(UserProfile)
