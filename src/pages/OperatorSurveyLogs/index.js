import React from 'react'
import OperatorSurveyLogs from '../../containers/OperatorSurveyLogs'
import OperatorPage from '../../components/OperatorPage'
import OperatorPageContent from '../../components/OperatorPageContent'

const Page = ({ ...props }) => {

  return (
    <OperatorPage>
      <OperatorPageContent>
        <OperatorSurveyLogs {...props} />
      </OperatorPageContent>
    </OperatorPage>
  )
}

export default Page
