import React, { useEffect } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import gql from 'graphql-tag'
import { useMutation } from 'react-apollo-hooks'
import OperatorSurveyEditionTabBar from '../../containers/OperatorSurveyEditionTabBar'
import OperatorCreateSurveyProducts from '../OperatorCreateSurveyProducts'
import OperatorCreateSurveyBasics from '../OperatorCreateSurveyBasics'
import OperatorCreateTastingNotes from '../OperatorCreateTastingNotes'
import OperatorCreateSurveySettings from '../OperatorCreateSurveySettings'
import OperatorCreateSurveyQuestions from '../OperatorCreateSurveyQuestions'
import OperatorCreateSurveyPayments from '../OperatorCreateSurveyPayments'
import OperatorCreateSurveyLinkedSurveys from '../OperatorCreateSurveyLinkedSurveys'
import { Page, PageContent } from './styles'
// import OperatorCreateSurveyEmailSettings from '../OperatorCreateSurveyEmailSettings'

export const RESET_CREATION_FORM = gql`
  mutation resetCreationForm {
    resetCreationForm @client
  }
`

const EditSurvey = ({ match: { path, url }, stricted }) => {
  const resetCreationForm = useMutation(RESET_CREATION_FORM)

  useEffect(() => {

    return async () => {
      await resetCreationForm()
    }
  }, [])

  return (
    <React.Fragment>
      <Route
        path={`${path}/:step`}
        component={props => (
          <OperatorSurveyEditionTabBar {...props} stricted={stricted} />
        )}
      />
      <Page>
        <PageContent>
          <Switch>
            <Redirect exact path={`${path}`} to={`${url}/basics`} />
            <Redirect exact path={`${path}/`} to={`${url}/basics`} />
            <Route
              path={`${path}/basics`}
              component={OperatorCreateSurveyBasics}
            />
            {/* <Route
            path={`${path}/emails`}
            component={OperatorCreateSurveyEmailSettings}
          /> */}
            <Route
              path={`${path}/settings`}
              component={props => (
                <OperatorCreateSurveySettings
                  {...props}
                  editing
                  stricted={stricted}
                />
              )}
            />
            <Route
              path={`${path}/tasting-notes`}
              component={OperatorCreateTastingNotes}
            />
            <Route
              path={`${path}/products`}
              component={props => (
                <OperatorCreateSurveyProducts {...props} stricted={stricted} />
              )}
            />
            <Route
              path={`${path}/questions`}
              component={OperatorCreateSurveyQuestions}
            />
            <Route
              path={`${path}/financial`}
              component={props => (
                <OperatorCreateSurveyPayments {...props} stricted={stricted} />
              )}
            />
            <Route
              path={`${path}/linked-surveys`}
              component={OperatorCreateSurveyLinkedSurveys}
            />
          </Switch>
        </PageContent>
      </Page>
    </React.Fragment>
  )
}

export default EditSurvey
