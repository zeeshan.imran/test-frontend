import React from 'react'
import { mount } from 'enzyme'
import Section from '.'

describe('Section', () => {
  let testRender
  let title
  let dark
  let children

  beforeEach(() => {
    title = 'title'
    dark = 'dark'
    children = <div>Children content</div>
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Section', () => {
    testRender = mount(
      <Section title={title} dark={dark} children={children} />
    )
    expect(testRender).toMatchSnapshot()
  })
})
