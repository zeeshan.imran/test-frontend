import React, { useState, useRef, useCallback } from 'react'
import OperatorPage from '../../components/OperatorPage'
import OperatorPageContent from '../../components/OperatorPageContent'
import {
  Container,
  Title,
  SubTitle,
  ButtonGroup,
  LinkGroup,
  Link,
  ErrorMessage
} from './styles'
import Button from '../../components/Button'
import { withRouter } from 'react-router-dom'
import { Icon, Spin } from 'antd'
import { useTranslation } from 'react-i18next'
import gql from 'graphql-tag'
import { useQuery, useApolloClient } from 'react-apollo-hooks'
import getErrorMessage from '../../utils/getErrorMessage'

const Download = ({ history, match }) => {
  const id = match.params.id
  const linkRef = useRef()
  const client = useApolloClient()
  const { t } = useTranslation()
  const [downloading, setDownloading] = useState(false)
  const {
    data: { info },
    loading,
    error
  } = useQuery(GET_FILE_INFO, {
    variables: { id },
    fetchPolicy: 'network-only'
  })

  const handleDownload = useCallback(async () => {
    if (downloading) {
      return
    }

    setDownloading(true)

    const {
      data: { file }
    } = await client.query({
      query: GET_FILE_DOWNLOAD_LINK,
      variables: { id }
    })
    window.open(file.link, '_self')
  }, [client, id, downloading])

  return (
    <OperatorPage>
      <OperatorPageContent>
        <Container>
          <Title>{t('components.download.title')}</Title>
          {loading && (
            <div>
              <Spin size='small' /> {t('components.download.checking')}
            </div>
          )}
          {!loading && error && (
            <React.Fragment>
              <ErrorMessage>{getErrorMessage(error, 'NOT_FOUND')}</ErrorMessage>
              <ButtonGroup>
                <Button size='md' onClick={() => history.push('/')}>
                  {t('components.download.goToDashboard')}
                </Button>
              </ButtonGroup>
            </React.Fragment>
          )}
          {!loading && !error && (
            <React.Fragment>
              <SubTitle>{t('components.download.subTitle')}</SubTitle>
              <LinkGroup>
                <Icon type='file' />{' '}
                <Link
                  innerRef={linkRef}
                  download={info.file.fileName}
                  onClick={handleDownload}
                >
                  {info.file.fileName}
                </Link>
              </LinkGroup>
              <ButtonGroup>
                <Button
                  type={downloading && 'disabled'}
                  size='md'
                  onClick={handleDownload}
                >
                  {t('components.download.download')}
                </Button>
                <Button
                  type='secondary'
                  size='md'
                  onClick={() => history.push('/')}
                >
                  {t('components.download.cancel')}
                </Button>
              </ButtonGroup>
            </React.Fragment>
          )}
        </Container>
      </OperatorPageContent>
    </OperatorPage>
  )
}

const GET_FILE_INFO = gql`
  query getFileInfo($id: ID!) {
    info: getFileInfo(id: $id) {
      id
      file {
        type
        fileName
      }
      size
    }
  }
`

const GET_FILE_DOWNLOAD_LINK = gql`
  query getFileDownloadLink($id: ID!) {
    file: getFileDownloadLink(id: $id) {
      id
      link
    }
  }
`

export default withRouter(Download)
