import React from 'react'
import OperatorSection from '../../components/OperatorSection'
import OperatorSurveyCreateAutoAdvanceSettings from '../../containers/OperatorSurveyCreateAutoAdvanceSettings'
import { useTranslation } from 'react-i18next'

const OperatorCreateSurveySettings = ({ match, editing = false, stricted }) => {
  const { t } = useTranslation()
  return (
    <React.Fragment>
      <OperatorSection
        title={t('containers.page.operatorCreateSettings.title')}
      />
      <OperatorSurveyCreateAutoAdvanceSettings
        surveyId={match.params && match.params.surveyId}
        editing={editing}
        stricted={stricted}
      />
    </React.Fragment>
  )
}

export default OperatorCreateSurveySettings
