import merge from 'lodash.merge'
import React from 'react'
import { mount } from 'enzyme'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import Operator from './index'
import {
  getAuthenticatedUser,
  getUIState,
  updateUIState,
  isUserAuthenticatedAsOperator,
  isUserAuthenticatedAsPowerUser,
  isUserAuthenticatedAsSuperAdmin
} from '../../utils/userAuthentication'
import { MemoryRouter } from 'react-router'
import { useSlaask } from '../../contexts/SlaaskContext'
import { StyledMenu } from '../../components/OperatorLayout/Sider/Menu/styles'
import wait from '../../utils/testUtils/waait'

import PubSub from 'pubsub-js'
import { PUBSUB } from '../../utils/Constants'

jest.mock('../../contexts/SlaaskContext')

jest.mock('browser-image-compression', () => ({
  __esModule: true,
  default: async (file, option) => file
}))

jest.mock('../../utils/userAuthentication')

describe('pages -> Operator', () => {
  let testRender
  let uiState = {}
  let client
  getAuthenticatedUser.mockImplementation(() => ({
    emailAddress: 'test@flavorwiki.com'
  }))

  getUIState.mockImplementation(() => ({}))
  updateUIState.mockImplementation(updateState => merge(uiState, updateState))

  beforeEach(() => {
    useSlaask.mockImplementation(() => ({ reloadSlaask: jest.fn() }))
    client = createApolloMockClient()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('Should set correct active menu entry', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <MemoryRouter initialEntries={['/operator/dashboard']} initialIndex={0}>
          <Operator />
        </MemoryRouter>
      </ApolloProvider>
    )

    let menu = testRender.find(StyledMenu)

    expect(menu.props().selectedKeys).toEqual(['dashboard'])
  })

  test('should change the user dropdown when it is changed in UsersList', async () => {
    getAuthenticatedUser.mockImplementation(() => ({
      fullName: 'some full name',
      isSuperAdmin: true,
      fullName: 'some full name',
      emailAddress: 'emailAddress',
      type: 'power-user'
    }))
    isUserAuthenticatedAsOperator.mockImplementation(() => true)

    testRender = mount(
      <ApolloProvider client={client}>
        <MemoryRouter initialEntries={['/operator/dashboard']} initialIndex={0}>
          <Operator />
        </MemoryRouter>
      </ApolloProvider>
    )

    await wait(0)
    PubSub.publish(PUBSUB.UPDATE_OPERATOR_DROPDOWN)
    await wait(0)

    expect(getAuthenticatedUser).toBeCalled()
    expect(isUserAuthenticatedAsOperator).toBeCalled()
    expect(isUserAuthenticatedAsPowerUser).toBeCalled()
    expect(isUserAuthenticatedAsSuperAdmin).toBeCalled()
  })
})
