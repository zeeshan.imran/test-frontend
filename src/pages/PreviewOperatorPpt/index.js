import React from 'react'
import { withRouter } from 'react-router-dom'
import PreviewOperatorPpt from '../../containers/PreviewOperatorPpt'

export const PreviewOperatorPptPage = ({ match }) => {
  const { surveyId, jobGroupId } = match.params
  return <PreviewOperatorPpt surveyId={surveyId} jobGroupId={jobGroupId} />
}

export default withRouter(PreviewOperatorPptPage)
