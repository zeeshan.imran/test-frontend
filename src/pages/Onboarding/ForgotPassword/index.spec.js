import React from 'react'
import { mount } from 'enzyme'
import ForgotPassword from '.'
import { Router } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'

jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k,
  getFixedT: () => () => ''
}))
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = {
      ...Component.defaultProps,
      t: () => '',
      i18n: {
        t: () => ''
      }
    }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))
jest.mock(
  '../../../containers/ForgotPasswordForm',
  () => ({ handleGoBack }) => {
    handleGoBack()
    return <div />
  }
)

describe('ForgotPassword', () => {
  let testRender
  let history
  let client

  beforeEach(() => {
    history = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      createHref: jest.fn(),
      location: { pathname: '' }
    }
    client = createApolloMockClient()
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render ForgotPassword', () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <ForgotPassword />
        </Router>
      </ApolloProvider>
    )

    expect(testRender).toMatchSnapshot()
  })
})
