import React from 'react'
import { withRouter } from 'react-router-dom'
import ForgotPasswordForm from '../../../containers/ForgotPasswordForm'
import Title from '../Title'
import TermsAndPrivacyFooter from '../../../containers/TermsAndPrivacyFooter'

import { ONBOARD_ROOT_PATH } from '../../../utils/Constants'
import SubTitle from '../SubTitle'
import { useTranslation } from 'react-i18next';

const ForgotPassword = ({ history }) =>
{
  const {t} = useTranslation();
  return(
    <div>
      <Title>{t('containers.page.onboarding.forgotPassword.title')}</Title>
      <SubTitle>
        {t('containers.page.onboarding.forgotPassword.subTitle')}
      </SubTitle>
      <ForgotPasswordForm
        handleGoBack={() => history.push(`${ONBOARD_ROOT_PATH}/login`)}
      />
      <TermsAndPrivacyFooter />
    </div>
  )
}

export default withRouter(ForgotPassword)
