import React from 'react'
import { create, act } from 'react-test-renderer'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'
import { ApolloProvider } from 'react-apollo-hooks'
import { Router } from 'react-router-dom'

import RequestAccount from './index'

jest.doMock('./index.js')
jest.mock('rc-util/lib/Portal')

describe('RequestAccount', () => {
  let testRederer
  let mockClient

  const history = {
    location: '',
    match: {
      path: ''
    },
    listen: jest.fn(),
    createHref: jest.fn(),
    push: jest.fn()
  }

  const creating = () => {
    testRederer = create(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <RequestAccount userType='usery' />
        </Router>
      </ApolloProvider>
    )
  }
  const updating = () => {
    testRederer.update(
      <ApolloProvider client={mockClient}>
        <Router history={history}>
          <RequestAccount userType='usery' />
        </Router>
      </ApolloProvider>
    )
  }

  const destroy = () => {
    testRederer.unmount()
  }

  beforeAll(() => {
    mockClient = createApolloMockClient()
  })

  test('edit fields', () => {
    creating()
    const email = testRederer.root.findByProps({ name: 'email' })
    const firstName = testRederer.root.findByProps({ name: 'firstName' })
    const lastName = testRederer.root.findByProps({ name: 'lastName' })
    const companyName = testRederer.root.findByProps({ name: 'companyName' })
    const code = testRederer.root.findByProps({ placeholder: 'Country' })
    const number = testRederer.root.findByProps({
      numberName: 'number',
      placeholder: 'Your phone number'
    })
    act(() => {
      email.props.onChange({ target: { name: 'email', value: 'email@email.email' } })
      firstName.props.onChange({ target: { name: 'firstName', value: 'first_name' } })
      lastName.props.onChange({ target: { name: 'lastName', value: 'last_name' } })
      companyName.props.onChange({ target: { name: 'companyName', value: 'company_name' } })
      // number.props.onChangeCode({ target: { name: 'number', value: 'number' } })
      // code.props.onChange({ target: { name: 'code', value: 'code' } })
      updating()
    })
    expect(email.props.value).toBe('email@email.email')
    expect(firstName.props.value).toBe('first_name')
    expect(lastName.props.value).toBe('last_name')
    expect(companyName.props.value).toBe('company_name')
    // expect(number.props.value).toBe('number')
    // expect(code.props.value).toBe('code')
  })

  test('submit', () => {
    // const submit = testRederer.root.findByProps({ type: 'primary' })
    // act(() => {
    //   submit.props.onClick()
    // })(mockSubmit).toHaveBeCalledWith({
    //   email: 'email',
    //   firstName: 'first_name',
    //   lastName: 'last_name',
    //   companyName: 'company_name',
    //   code: 'code',
    //   number: 'number'
    // })
    // destroy()
  })
})
