import React from 'react'
import { mount } from 'enzyme'
import Login from '.'
import { Router } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../../utils/createApolloMockClient'
import {
  setAuthenticationToken,
  setAuthenticatedUser
} from '../../../utils/userAuthentication'
jest.mock('i18next', () => ({
  use: () => ({ init: () => {} }),
  init: () => {},
  t: k => k,
  getFixedT: () => () => ''
}))
jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: () => '' }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))
jest.mock(
  '../../../containers/LoginForm',
  () => ({
    onLoginOperator,
    onLoginTaster,
    handleNavigateToForgotPassword,
    handleNavigateToRequestAccount
  }) => {
    onLoginOperator(),
    onLoginTaster(),
    handleNavigateToForgotPassword(),
    handleNavigateToRequestAccount()
    return <div />
  }
)
describe('Login', () => {
  let testRender
  let history
  let client
  beforeEach(() => {
    history = {
      push: jest.fn(),
      createHref: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' },
      replace: jest.fn()
    }
    client = createApolloMockClient()
  })
  afterEach(() => {
    testRender.unmount()
  })
  test('should render Login', () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <Login />
        </Router>
      </ApolloProvider>
    )
    expect(testRender).toMatchSnapshot()
  })
  test('should render Login as operator', () => {
    history.location = {
      pathname: '/operator'
    }
    setAuthenticationToken('testTocken')
    setAuthenticatedUser({ type: 'operator' })
    
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <Login />
        </Router>
      </ApolloProvider>
    )
    expect(testRender.find('Router').prop('history').location.pathname).toEqual(
      '/operator'
    )
  })
  test('should render Login as power user', () => {
    history.location = {
      pathname: '/operator'
    }
    setAuthenticationToken('testTocken')
    setAuthenticatedUser({ type: 'power-user' })
    // isUserAuthenticatedAsPowerUser.mockImplementation(() => ({
    //   isUserAuthenticated: true
    // }))
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <Login />
        </Router>
      </ApolloProvider>
    )
    expect(testRender.find('Router').prop('history').location.pathname).toEqual(
      '/operator'
    )
  })
  test('should render Login as user taster', () => {
    history.location = {
      pathname: '/taster'
    }
    setAuthenticationToken('testTocken')
    setAuthenticatedUser({ type: 'taster' })
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={history}>
          <Login />
        </Router>
      </ApolloProvider>
    )
    expect(testRender.find('Router').prop('history').location.pathname).toEqual(
      '/taster'
    )
  })
})
