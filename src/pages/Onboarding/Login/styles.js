import styled from 'styled-components'
import colors from '../../../utils/Colors'
import { family } from '../../../utils/Fonts'

export const Title = styled.p`
  font-family: ${family.primaryLight};
  font-size: 4.2rem;
  line-height: 1.24;
  letter-spacing: normal;
  color: ${colors.BLACK};
  margin: 0;
`
