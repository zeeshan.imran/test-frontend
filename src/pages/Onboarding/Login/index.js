import React from 'react'
import { Redirect, withRouter } from 'react-router-dom'
import LoginForm from '../../../containers/LoginForm'
import Title from '../Title'
import {
  isUserAuthenticatedAsOperator,
  isUserAuthenticatedAsTaster,
  isUserAuthenticatedAsPowerUser,
  isUserAuthenticatedAsAnalytics
} from '../../../utils/userAuthentication'
import { ONBOARD_ROOT_PATH } from '../../../utils/Constants'
import TermsAndPrivacyFooter from '../../../containers/TermsAndPrivacyFooter'
import { useTranslation } from 'react-i18next'
import getQueryParams from '../../../utils/getQueryParams'

const Login = ({ history, location }) => {
  const { t } = useTranslation()
  const { state } = location
  const { referral, error, source } = getQueryParams(location)
  let params = referral ? '?referral=' + referral : '?'
  params += source ? "source="+ source : ''

  if (isUserAuthenticatedAsOperator()) {
    return <Redirect to='/operator' />
  }

  if (isUserAuthenticatedAsAnalytics()) {
    return <Redirect to='/operator' />
  }

  if (isUserAuthenticatedAsPowerUser()) {
    return <Redirect to='/operator' />
  }

  if (isUserAuthenticatedAsTaster()) {
    return <Redirect to='/taster?alert=false' />
  }

  return (
    <React.Fragment>
      <Title data-testid='login-page-title'>
        {t('containers.page.onboarding.login.title')}
      </Title>
      <LoginForm
        fromDashboard={!!state && state.fromDashboard}
        onLoginOperator={() => history.push('/operator')}
        onLoginTaster={() => history.push('/create-account/taster')}
        loginPersonalInfo={() =>
          history.push('/create-account/taster/personal-information')
        }
        loginProfile={() => history.push('/taster')}
        handleNavigateToForgotPassword={() =>
          history.push(`${ONBOARD_ROOT_PATH}/forgot-password`)
        }
        handleNavigateToRequestAccount={() =>
          history.push(`${ONBOARD_ROOT_PATH}/request-account/operator`)
        }
        // Taster Account Link
        handleNavigateToCreateTasterAccount={() =>
          history.push(`/create-account/taster/welcome` + params)
        }
        error={error}
      />
      <TermsAndPrivacyFooter />
    </React.Fragment>
  )
}

export default withRouter(Login)
