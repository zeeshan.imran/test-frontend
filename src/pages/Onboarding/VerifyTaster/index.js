import React, { useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import getQueryParams from '../../../utils/getQueryParams'
import { useMutation } from 'react-apollo-hooks'
import gql from 'graphql-tag'
import {
  setAuthenticationToken,
  setAuthenticatedUser,
  setAuthenticatedOrganization
} from '../../../utils/userAuthentication'

const VerifyTaster = ({ location, history }) => {
  const { token, email } = getQueryParams(location)
  const queryToken = token
  const verifyTasterMutation = useMutation(VERIFY_TASTER)

  useEffect(() => {
    const setVerifyTaster = async () => {
      try {
        const { data } = await verifyTasterMutation({
          variables: { input: { emailAddress: email, token: queryToken } }
        })
        let { verifyTaster } = data
        if (!verifyTaster.user.fullName) {
          verifyTaster.user.fullName = verifyTaster.user.emailAddress
        }
        setAuthenticatedUser(verifyTaster.user)
        setAuthenticatedOrganization(verifyTaster.user.organization && verifyTaster.user.organization.id)
        setAuthenticationToken(verifyTaster.token)
        history.push('/create-account/taster')
      } catch (error) {
        let errorMessage = error.message.replace('GraphQL error: ', '')
        history.push(`/onboarding/login?error=${errorMessage}`)
      }
      
    }
    setVerifyTaster()
  }, [])

  return <React.Fragment />
}

const VERIFY_TASTER = gql`
  mutation verifyTaster($input: VerifyTasterInput) {
    verifyTaster(input: $input) {
      user {
        id
        initialized
        emailAddress
        type
        isSuperAdmin
        isTaster
        isDummyTaster
        fullName
        isVerified
        dateofbirth
        country
        state
        language
        gender
        paypalEmailAddress
        ethnicity
        smokerKind
        foodAllergies
        marketResearchParticipation
        isProfileCompleted
        organization {
          name
          id
        }
      }
      token
    }
  }
`
export default withRouter(VerifyTaster)
