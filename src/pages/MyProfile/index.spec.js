import React from 'react'
import { mount } from 'enzyme'
import MyProfile from '.'

jest.mock('../../containers/AuthenticatedLayout', () => () => {
  return <div />
})

jest.mock('../../containers/ProfileLayout')

describe('MyProfile', () => {
  let testRender

  beforeEach(() => {})

  afterEach(() => {
    testRender.unmount()
  })

  test('should render MyProfile', () => {
    testRender = mount(<MyProfile />)
    expect(testRender).toMatchSnapshot()
  })
})
