import React from 'react'
import PersonalDetailsContainer from '../../../containers/PersonalDetails'

const PersonalDetails = props => <PersonalDetailsContainer {...props} />

export default PersonalDetails
