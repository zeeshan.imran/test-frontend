import React from 'react'
import { mount } from 'enzyme'
import NotFound from '.'
import history from '../../../history'

jest.mock('react-i18next', () => ({
  withTranslation: () => Component => {
    Component.defaultProps = { ...Component.defaultProps, t: text => text }
    return Component
  },
  useTranslation: () => ({ t: text => text })
}))

describe('NotFound', () => {
  let testRender

  afterEach(() => {
    testRender.unmount()
  })

  test('should render NotFound', () => {
    testRender = mount(<NotFound desktop={''} history={history} />)

    expect(testRender).toMatchSnapshot()
  })
})
