import React from 'react'
import { shallow } from 'enzyme'
import Survey from '.'

describe('Survey', () => {
  let testRender

  afterEach(() => {
    testRender.unmount()
  })

  test('should render Survey', () => {
    testRender = shallow(
      <Survey
        match={{ params: { surveyId: 'survey-1' } }}
        location={{ search: '' }}
      />
    )

    expect(testRender).toMatchSnapshot()
  })
})
