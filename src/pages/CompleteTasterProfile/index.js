import React, { useState } from 'react'
import Navbar from '../../components/Navbar'
import CompleteProfileForm from '../../containers/CompleteTasterProfileForm'
import useResponsive from '../../utils/useResponsive'
import { PageContainer, Container, Title, Subtitle } from './styles'
import { logout, getAuthenticatedUser } from '../../utils/userAuthentication'
import { useTranslation } from 'react-i18next'

const NAVIGATION_ENTRIES = [
  {
    title: 'containers.completeTasterProfile.home',
    route: '/'
  },
  {
    title: 'containers.completeTasterProfile.signOut',
    action: logout
  }
]

const CompleteTasterProfile = ({ history }) => {
  const { t } = useTranslation()
  const { mobile } = useResponsive()
  const [showHamburger, setShowHamburger] = useState(false)
  const user = getAuthenticatedUser()
  const handleClickEntry = entry =>
    entry.action ? entry.action() : history.push(entry.route)
  return (
    <PageContainer>
      <Navbar
        user={user}
        entriesData={NAVIGATION_ENTRIES}
        getEntryName={({ title }) => t(title)}
        onClickEntry={handleClickEntry}
        setShowHamburger={() => {
          setShowHamburger(!showHamburger)
        }}
        showHamburger={showHamburger}
      />
      <Container mobile={mobile}>
        <Title>{t('containers.completeTasterProfile.title')}</Title>
        <Subtitle>{t('containers.completeTasterProfile.subtitle')}</Subtitle>
        <CompleteProfileForm
          onCompleteProfile={() => history.push('/taster')}
        />
      </Container>
    </PageContainer>
  )
}

export default CompleteTasterProfile
