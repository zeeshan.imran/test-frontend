import React from 'react'
import { mount } from 'enzyme'
import SurveyLogin from '.'
import { Router } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'

jest.mock('../../containers/AuthenticatedLayout', () => ({ children }) => (
  <div>children</div>
))
jest.mock('../../containers/SurveyLogin')

describe('SurveyLogin', () => {
  let testRender
  let historyMock
  let client

  beforeEach(() => {
    historyMock = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }
    client = createApolloMockClient()
  })
  afterEach(() => {
    testRender.unmount()
  })

  test('should render SurveyLogin', () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={historyMock}>
          <SurveyLogin match={{ params: { surveyId: 'survey-1' } }} />
        </Router>
      </ApolloProvider>
    )

    expect(testRender).toMatchSnapshot()
  })
})
