import React, { useState } from 'react'
import AuthenticatedLayout from '../../containers/AuthenticatedLayout'
import SurveyQuestionLayout from '../../containers/SurveyQuestionLayout'
import SurveyQuestionContainer from '../../containers/SurveyQuestion'

const SurveyQuestion = ({
  match: {
    params: { stepId, surveyId }
  }
}) => {
  const [isTimerLocked, setIsTimerLocked] = useState(false)
  const [timeCompletedFor, setTimeCompletedFor] = useState([])
  
  return (
    <React.Fragment>
      <AuthenticatedLayout mergeNavbarToContent>
        <SurveyQuestionLayout
          questionId={stepId}
          surveyId={surveyId}
          isTimerLocked={isTimerLocked}
          setIsTimerLocked={setIsTimerLocked}
          timeCompletedFor={timeCompletedFor}
          setTimeCompletedFor={setTimeCompletedFor}
        >
          <SurveyQuestionContainer
            questionId={stepId}
            surveyId={surveyId}
            setIsTimerLocked={setIsTimerLocked}
            isTimerLocked={isTimerLocked}
            timeCompletedFor={timeCompletedFor}
            setTimeCompletedFor={setTimeCompletedFor}
          />
        </SurveyQuestionLayout>
      </AuthenticatedLayout>
    </React.Fragment>
  )
}

export default SurveyQuestion
