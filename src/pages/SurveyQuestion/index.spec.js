import React from 'react'
import { mount } from 'enzyme'
import SurveyQuestion from '.'
import wait from '../../utils/testUtils/waait'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'

jest.mock('../../containers/SurveyQuestion', () => () => {
  return <dev />
})
jest.mock('../../containers/SurveyQuestionLayout', () => ({ children }) =>
  children
)

describe('pages surveyQuestion', () => {
  let testRender
  let client
  beforeEach(() => {
    client = createApolloMockClient()
    window.localStorage = {
      getItem: () => '{}'
    }
  })

  afterEach(() => {
    testRender.unmount()
  })

  test('should render SurveyQuestion', async () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <SurveyQuestion
          match={{ params: { stepId: 'stepid-1', surveyId: 'survey-1' } }}
        />
      </ApolloProvider>
    )
    await wait(500)
    expect(testRender).toMatchSnapshot()
  })
})
