import styled from 'styled-components'
import { components } from '../../utils/Metrics'

export const Container = styled.div`
  margin-top: ${components.DEFAULT_PAGE_V_MARGIN}rem;
`
