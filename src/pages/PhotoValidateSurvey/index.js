import React, { useMemo } from 'react'
import gql from 'graphql-tag'
import { useQuery } from 'react-apollo-hooks'
import { useTranslation } from 'react-i18next'
import { withRouter } from 'react-router-dom'
import getQueryParams from '../../utils/getQueryParams'
import { SURVEY_QUERY } from '../../queries/Survey'
import { Page, PageContent, Marginer } from './styles'
import LoadingModal from '../../components/LoadingModal'
import PhotoValidationHeaderSection from './PhotoValidationHeaderSection'
import PhotoHub from '../../components/PhotoHub'

import { PHOTO_ANSWER_FILTERS } from '../../utils/Constants'

const PhotoValidateSurvey = ({ match, location, history }) => {
  const { page, status, productId, userId, keyword } = getQueryParams(location)
  const { t } = useTranslation()
  const { surveyId } = match.params

  const {
    data: { superAdminUsersByOrganization: operatorUsers }
  } = useQuery(USER_LIST, {
    fetchPolicy: 'network-only'
  })

  const {
    data: { survey }
  } = useQuery(SURVEY_QUERY, {
    variables: { id: surveyId }
  })

  const {
    loading,
    refetch,
    data: { surveyEnrollmentValidations = [] }
  } = useQuery(SURVEY_ENROLLMENT_VALIDATIONS, {
    variables: {
      surveyId,
      params: {
        status,
        productId,
        userId,
        keyword,
        skip: (page - 1) * 20,
        limit: 20
      }
    },
    fetchPolicy: 'no-cache'
  })

  const {
    data: { countSurveyEnrollmentValidations },
    refetch: refetchCount,
  } = useQuery(COUNT_SURVEY_ENROLLMENT_VALIDATIONS, {
    variables: {
      surveyId,
      params: {
        status,
        productId,
        userId,
        keyword
      }
    },
    fetchPolicy: 'no-cache'
  })

  const products = useMemo(() => {
    if(survey && survey.products && survey.products.length > 0) {
      const returnProducts = []

      survey.products.forEach((product) => {
        if(product) {
          returnProducts.push(product)
        }
      })

      return returnProducts
    }

    return []
  }, [survey])

  const updateUrlSearchParams = (key, value) => {
    const urlSearchParams = new URLSearchParams(location.search)

    if (value) {
      urlSearchParams.set(key, value)
    } else {
      urlSearchParams.delete(key)
    }

    if (key !== 'page') {
      urlSearchParams.delete('page')
    }

    history.push(`${location.pathname}?${urlSearchParams.toString()}`)
  }

  if (loading) {
    return <LoadingModal visible />
  }

  return (
    <React.Fragment>
      <Marginer />
      <Page>
        <PageContent>
          {survey && (
            <React.Fragment>
              <PhotoValidationHeaderSection
                title={t(`components.surveyPhotos.mainHeader`, {
                  surveyName: survey.name
                })}
                filters={PHOTO_ANSWER_FILTERS}
                countSurveyEnrollmentValidations={
                  countSurveyEnrollmentValidations
                }
                products={products}
                operatorUsers={operatorUsers}
                updateUrlSearchParams={updateUrlSearchParams}
              />
              <PhotoHub
                loading={loading}
                refetch={refetch}
                refetchCount={refetchCount}
                page={page}
                survey={survey}
                surveyEnrollmentValidations={surveyEnrollmentValidations}
                countSurveyEnrollmentValidations={
                  countSurveyEnrollmentValidations
                }
                updateUrlSearchParams={updateUrlSearchParams}
              />
            </React.Fragment>
          )}
        </PageContent>
      </Page>
    </React.Fragment>
  )
}

const USER_LIST = gql`
  query superAdminUsersByOrganization {
    superAdminUsersByOrganization {
      id
      fullName
    }
  }
`

export const SURVEY_ENROLLMENT_VALIDATIONS = gql`
  query surveyEnrollmentValidations(
    $surveyId: ID!
    $params: SurveyEnrollmentValidationParams
  ) {
    surveyEnrollmentValidations(surveyId: $surveyId, params: $params) {
      id
      savedRewards {
        id
        answered
        reward
        showOnCharts
        payable
        delayToNextProduct
        extraDelayToNextProduct
      }
      paypalEmail
      processed
      hiddenFromCharts
      validation
      comment
      escalation
      user {
        id
        emailAddress
        paypalEmailAddress
        fullName
        blackListed
      }
      answers {
        id
        value
        timeToAnswer
        product {
          id
          name
          photo
          reward
        }
        question {
          id
          type
        }
      }
    }
  }
`

export const COUNT_SURVEY_ENROLLMENT_VALIDATIONS = gql`
  query countSurveyEnrollmentValidations(
    $surveyId: ID!
    $params: CountSurveyEnrollmentValidationParams
  ) {
    countSurveyEnrollmentValidations(surveyId: $surveyId, params: $params)
  }
`

export default withRouter(PhotoValidateSurvey)
