import React, { useMemo } from 'react'
import { withRouter } from 'react-router-dom'
import { Input } from 'antd'
import { useTranslation } from 'react-i18next'
import Select from '../../../components/Select'

import {
  Container,
  RightContainerMain,
  LeftContainerMain,
  RightContainer,
  LeftContainer,
  Title,
  SubTitle,
  ClearFloats,
  SearchBarContainer,
  ProductContainerMain,
  ProductLeftContainer,
  ProductRightContainer
} from './styles'

const PhotoValidationHeaderSection = ({
  location,
  title,
  filters,
  countSurveyEnrollmentValidations,
  products,
  operatorUsers,
  updateUrlSearchParams
}) => {
  const { t } = useTranslation()

  const formData = useMemo(() => {
    const urlSearchParams = new URLSearchParams(location.search)
    const memoFormdata = {
      status: 'all',
      productId: '',
      userId: '',
      keyword: ''
    }

    if (urlSearchParams.has('status')) {
      memoFormdata.status = urlSearchParams.get('status')
    }

    if (urlSearchParams.has('productId')) {
      memoFormdata.productId = urlSearchParams.get('productId')
    }

    if (urlSearchParams.has('userId')) {
      memoFormdata.userId = urlSearchParams.get('userId')
    }

    if (urlSearchParams.has('keyword')) {
      memoFormdata.keyword = urlSearchParams.get('keyword')
    }

    return memoFormdata
  }, [location.search])

  return (
    <Container>
      <LeftContainerMain>
        <Title>{title}</Title>
      </LeftContainerMain>

      <RightContainerMain>
        <LeftContainer>
          <SubTitle>{t('components.surveyValidate.filters.filterBy')}</SubTitle>
        </LeftContainer>
        <RightContainer>
          <Select
            name='photo-filter'
            value={formData.status}
            options={filters}
            optionFilterProp='children'
            getOptionName={filter => filter.name}
            getOptionValue={filter => filter.code}
            onChange={filter => updateUrlSearchParams('status', filter)}
            size={`default`}
          />
        </RightContainer>
        <ClearFloats />
      </RightContainerMain>
      <ClearFloats />
      {products && products.length > 0 && (
        <ProductContainerMain>
          <ProductLeftContainer>
            <SubTitle>
              {t('components.surveyValidate.filters.filterByProduct')}
            </SubTitle>
          </ProductLeftContainer>
          <ProductRightContainer>
            <Select
              name='product-filter'
              value={formData.productId}
              options={products}
              optionFilterProp='children'
              getOptionName={product => product.name}
              getOptionValue={product => product.id}
              onChange={value => updateUrlSearchParams('productId', value)}
              size={`default`}
              placeholder={t(
                'components.surveyValidate.filters.filterByProductPlaceholder'
              )}
              allowClear
            />
          </ProductRightContainer>
        </ProductContainerMain>
      )}
      <ClearFloats />
      {operatorUsers && operatorUsers.length > 0 && (
        <ProductContainerMain>
          <ProductLeftContainer>
            <SubTitle>
              {t('components.surveyValidate.filters.validatedBy')}
            </SubTitle>
          </ProductLeftContainer>
          <ProductRightContainer>
            <Select
              name='user-filter'
              value={formData.userId}
              options={operatorUsers}
              optionFilterProp='children'
              getOptionName={user => user.fullName}
              getOptionValue={user => user.id}
              onChange={value => updateUrlSearchParams('userId', value)}
              size={`default`}
              placeholder={t(
                'components.surveyValidate.filters.validatedByPlaceholder'
              )}
              allowClear
            />
          </ProductRightContainer>
        </ProductContainerMain>
      )}
      <ClearFloats />
      <SearchBarContainer>
        <Input.Search
          defaultValue={formData.keyword}
          placeholder={t(`components.surveyValidate.filters.email`)}
          onChange={e => {
            if (e.target.value === '') {
              updateUrlSearchParams('keyword', '')
            }
          }}
          onSearch={value => updateUrlSearchParams('keyword', value)}
          enterButton
        />
      </SearchBarContainer>
      <SearchBarContainer>
        <SubTitle>
          {t(`components.surveyValidate.filters.total`, {
            records: countSurveyEnrollmentValidations
          })}
        </SubTitle>
      </SearchBarContainer>
    </Container>
  )
}

export default withRouter(PhotoValidationHeaderSection)
