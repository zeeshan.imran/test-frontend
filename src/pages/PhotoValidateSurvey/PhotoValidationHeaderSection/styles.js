import styled from 'styled-components'
import Text from '../../../components/Text'
import colors from '../../../utils/Colors'
import { family } from '../../../utils/Fonts'

export const Container = styled.div`
  background-color: ${colors.WHITE};
  padding: 3rem 3.5rem;
  margin-bottom: 2.5rem;
`

export const LeftContainerMain = styled.div`
  float: left;
  width: 60%
`
export const RightContainerMain = styled.div`
  float: right;
  width: 30%
`

export const LeftContainer = styled.div`
  float: left;
  line-height: 3rem;
  text-align: right;
  width: 30%
`

export const RightContainer = styled.div`
  float: right;
  width: 60%
`

export const Title = styled(Text)`
  font-size: 2rem;
  font-family: ${family.primaryBold};
  color: rgba(0, 0, 0, 0.85);
`

export const SubTitle = styled(Text)`
  font-size: 1.5rem;
  font-weight: normal;
  font-family: ${family.primaryBold};
`

export const ClearFloats = styled.div`
  clear: both;
`
export const SearchBarContainer = styled.div`
  margin: 1rem 0;
  text-align: center;
`

export const ProductContainerMain = styled.div`
  margin-top: 20px;
  float: right;
  width: 60%
`

export const ProductLeftContainer = styled.div`
  float: left;
  line-height: 3rem;
  text-align: right;
  width: 40%
`

export const ProductRightContainer = styled.div`
  float: right;
  width: 50%
`