import React from 'react'
import { mount } from 'enzyme'
import OperatorCreateSurveyBasics from '.'
import { Router } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import { defaultSurvey } from '../../mocks'

const mockSurveyCreation = {
  ...defaultSurvey,
  surveyId: 'survey-1',
  basics: {
    ...defaultSurvey.basics,
    name: 'Survey 1',
    uniqueName: 'survey-1',
  },
  products: [
    {
      id: 'product-1',
      name: 'product',
      brand: 'brand'
    }
  ],
  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?'
    }
  ]
}

describe('OperatorCreateSurveyBasics', () => {
  let testRender
  let t = jest.fn(key => key)
  let historyMock = {
    push: jest.fn(),
    listen: () => {
      return jest.fn()
    },
    location: { pathname: '' }
  }
  let client = createApolloMockClient()

  afterEach(() => {
    testRender.unmount()
  })

  test('should render OperatorCreateSurveyBasics', () => {
    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: mockSurveyCreation
      }
    })

    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={historyMock}>
          <OperatorCreateSurveyBasics
            t={t}
            match={{ params: { surveyId: 'survey-1' } }}
          />
        </Router>
      </ApolloProvider>
    )

    expect(testRender.find(OperatorCreateSurveyBasics)).toHaveLength(1)
  })
})
