import React from 'react'
import SurveyCompletedContainer from '../../containers/SurveyCompleted'

const SurveyCompleted = ({
  match: {
    params: { surveyId }
  }
}) => <SurveyCompletedContainer surveyId={surveyId} />

export default SurveyCompleted
