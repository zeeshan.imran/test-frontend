import React, { useState, useCallback } from 'react'
import AuthenticatedLayout from '../../containers/AuthenticatedLayout'
import SurveyQuestionLayout from '../../containers/SurveyQuestionLayout'
import SurveyQuestion from '../../containers/SurveyQuestion'
import SurveyLoading from '../../components/SurveyLoading'

const SurveyScreening = ({
  match: {
    params: { stepId, surveyId }
  }
}) => {
  const [loading, setLoading] = useState(true)
  const [isTimerLocked, setIsTimerLocked] = useState(false)
  const [timeCompletedFor, setTimeCompletedFor] = useState([])

  if (loading) {
    setTimeout(() => {
      setLoading(false)
    }, 500)
  }

  const handleSubmit = useCallback(() => {
    setLoading(true)
  }, [])
  
  return (
    <React.Fragment>
      <AuthenticatedLayout mergeNavbarToContent>
        {loading ? (
          <SurveyLoading />
        ) : (
          <SurveyQuestionLayout questionId={stepId} surveyId={surveyId}>
            <SurveyQuestion
              onSubmit={handleSubmit}
              questionId={stepId}
              surveyId={surveyId}
              setIsTimerLocked={setIsTimerLocked}
              isTimerLocked={isTimerLocked}
              timeCompletedFor={timeCompletedFor}
              setTimeCompletedFor={setTimeCompletedFor}
            />
          </SurveyQuestionLayout>
        )}
      </AuthenticatedLayout>
    </React.Fragment>
  )
}

export default SurveyScreening
