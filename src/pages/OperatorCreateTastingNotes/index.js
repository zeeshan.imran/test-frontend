import React from 'react'
import OperatorSection from '../../components/OperatorSection'
import OperatorSurveyCreateTastingNotes from '../../containers/OperatorSurveyCreateTastingNotes'
import { useTranslation } from 'react-i18next'

const OperatorCreateTastingNotes = ({ match, isDraft = false }) => {
  const { t } = useTranslation()
  return (
    <React.Fragment>
      <OperatorSection
        title={t('containers.page.operatorCreateTastingNotes.title')}
      />
      <OperatorSurveyCreateTastingNotes
        surveyId={match.params && match.params.surveyId}
        isDraft={isDraft}
      />
    </React.Fragment>
  )
}

export default OperatorCreateTastingNotes
