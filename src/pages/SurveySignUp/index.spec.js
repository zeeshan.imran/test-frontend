import React from 'react'
import { mount } from 'enzyme'
import SurveySignUp from '.'
import { Router } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'

jest.mock('../../containers/AuthenticatedLayout', () => ({ children }) => (
  <div>children</div>
))
jest.mock('../../containers/SurveySignUp')

describe('SurveySignUp', () => {
  let testRender
  let historyMock
  let client

  beforeEach(() => {
    historyMock = {
      push: jest.fn(),
      listen: () => {
        return jest.fn()
      },
      location: { pathname: '' }
    }
    client = createApolloMockClient()
  })
  afterEach(() => {
    testRender.unmount()
  })

  test('should render SurveySignUp', () => {
    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={historyMock}>
          <SurveySignUp match={{ params: { surveyId: 'survey-1' } }} />
        </Router>
      </ApolloProvider>
    )

    expect(testRender).toMatchSnapshot()
  })
})
