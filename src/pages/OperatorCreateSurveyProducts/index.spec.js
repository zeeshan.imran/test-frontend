import React from 'react'
import { mount } from 'enzyme'
import OperatorCreateSurveyProducts from '.'
import { Router } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo-hooks'
import { createApolloMockClient } from '../../utils/createApolloMockClient'
import { SURVEY_CREATION } from '../../queries/SurveyCreation'
import { defaultSurvey } from '../../mocks'

jest.mock('../../containers/AddProductToCreate', () => () => {
  return <div>Children</div>
})

jest.mock('browser-image-compression', () => ({
  __esModule: true,
  default: async (file, option) => file
}))

const mockSurveyCreation = {
  ...defaultSurvey,
  surveyId: 'survey-1',
  basics: {
    ...defaultSurvey.basics,
    name: 'Survey 1',
    authorizationType: 'public',
    exclusiveTasters: [],
    allowRetakes: false,
    forcedAccount: false,
    forcedAccountLocation: `start`,
    isScreenerOnly: false,
    showGeneratePdf: false,
    linkedSurveys: [],
    referralAmount: '5',
    recaptcha: false,
    maxProductStatCount: 6,
    savedRewards: [],
    minimumProducts: 1,
    maximumProducts: 1,
    surveyLanguage: 'en',
    country: 'United States of America',
    customizeSharingMessage: '',
    loginText: '',
    allowedDaysToFillTheTasting: 5,
    isPaypalSelected: false,
    isGiftCardSelected: false,
    productRewardsRule: {
      active: false,
      min: 0,
      max: 0,
      percentage: 0
    },
    maximumReward: 0
  },
  products: [
    {
      id: 'product-1',
      name: 'product',
      brand: 'brand'
    }
  ],
  questions: [
    {
      id: 'question-1',
      type: 'email',
      prompt: 'What is your email?'
    }
  ],
  mandatoryQuestions: [],
  uniqueQuestionsToCreate: []
}

describe('OperatorCreateSurveyProducts', () => {
  let testRender
  let t = jest.fn(key => key)
  let historyMock = {
    push: jest.fn(),
    listen: () => {
      return jest.fn()
    },
    location: { pathname: '' }
  }
  let client = createApolloMockClient()

  afterEach(() => {
    testRender.unmount()
  })

  test('should render OperatorCreateSurveyProducts', () => {
    client.cache.writeQuery({
      query: SURVEY_CREATION,
      data: {
        surveyCreation: mockSurveyCreation
      }
    })

    testRender = mount(
      <ApolloProvider client={client}>
        <Router history={historyMock}>
          <OperatorCreateSurveyProducts t={t} desktop={''} />
        </Router>
      </ApolloProvider>
    )

    expect(testRender).toMatchSnapshot()
  })
})
