// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('logout', () => {
  return cy
    .window()
    .its('localStorage')
    .invoke('removeItem', 'token')
})

Cypress.Commands.add('loginAsOperator', () => {
  const graphqlLoginRequest = {
    operationName: 'loginUser',
    variables: { email: 'operator@flavorwiki.com', password: 'operator' },
    query:
      'mutation loginUser($email: String!, $password: String!) {\n  loginUser(email: $email, password: $password) {\n    token\n    user {\n      id\n      initialized\n      emailAddress\n      type\n      fullName\n      organization{\n id\n }\n      __typename\n    }\n    __typename\n  }\n}\n'
  } 
  return cy
    .request('POST', `http://localhost:8000/graphql`, graphqlLoginRequest)
    .then(({ body: { data: { loginUser: { token, user } } } }) => { 
      window.localStorage.setItem('flavorwiki-operator-token', token)
      window.localStorage.setItem('flavorwiki-authenticated-org',user.organization.id)
      window.localStorage.setItem(
        'flavorwiki-user-details',
        JSON.stringify(user)
      )
    })
})

Cypress.Commands.add('assertRoute', (route, opts = undefined) => {
  return cy.url(opts).should('equal', `${window.location.origin}${route}`)
})

Cypress.Commands.add(
  'uploadFile',
  ({ selector, fixturePath, mimeType, fileName }) => {
    cy.get(selector).then(subject => {
      cy.fixture(fixturePath, 'base64').then(base64String => {
        Cypress.Blob.base64StringToBlob(base64String, mimeType).then(function (
          blob
        ) {
          var testfile = new File([blob], fileName, { type: mimeType })
          var dataTransfer = new DataTransfer()
          var fileInput = subject[0]

          dataTransfer.items.add(testfile)
          // This triggers the @change event on the input.
          fileInput.files = dataTransfer.files

          cy.wrap(subject).trigger('change', { force: true })
        })
      })
    })
  }
)
